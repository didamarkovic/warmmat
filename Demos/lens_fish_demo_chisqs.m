% 'chisq_test'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM
% version 1.2
% Using 1/m. Finding chisq too!
% 16.04.2010 KM
% version 1.3
% Testing new function chisq.m! -> chisq_test.m
% 16.04.2010 KM
% version 1.4
% plotting error of m_wdm from fish and probability from chisq
% 16.04.2010 KM
% Now doing a massive chisq run for all parameters! - USELESS!
% 14.05.2010 KM

clear %clc

filename = 'chisqs.txt';
filenameh = 'chisqs-halo.txt';

fprintf(['Starting the massive endeavour, see file ' filename '.\n'])

tic

l_max = 4;

halobool = 1;

% Set computational parameters
cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;
cospars.k_pivot = 0.05;

nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});
% had 1 bin

z_vals = 0.0001:0.05:5; % had 0.5
l_vals = 2*logspace(1,l_max,20); % had 5
M_vals = logspace(0,16,50); % had 5

nsigma = 1; % i.e. 3 sigma
P_limit = exp(-nsigma^2/2);

chipars = {'omega_m'; 'ns'; 'As'; 'Gamma'; 'invm'};%length(chipars)
nosteps = [5 5 5 5 20];
%nosteps = [2 2 2 2 2];
omega_m_steps = linspace(0.260,0.272,nosteps(1));
ns_steps = linspace(0.90,1.02,nosteps(2));
As_steps = linspace(10^5,1.6*10^5,nosteps(3));
Gamma_steps = linspace(0.165,0.190,nosteps(4));
ms_steps = 1./linspace(1/100000,1/500,nosteps(5));

c = clock;
dlmwrite(filename,['- Starting at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) ' of May 2010.'],'')
dlmwrite(filenameh,['- Starting at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) ' of May 2010.'],'')

dlmwrite(filename,'- Fiducial model is WMAP7.','-append','delimiter','')
dlmwrite(filenameh,'- Fiducial model is WMAP7.','-append','delimiter','')

%% How many chisq we need
noruns = nosteps(1); tmp = [chipars{1} ' '];
for int = 2:length(nosteps);
    noruns = noruns*nosteps(int);
    
    if int~=5
        tmp((length(tmp)+1):(length(tmp)+length(chipars{int})+1)) = [chipars{int} ' '];
    end
end
tmp((length(tmp)+1):(length(tmp)+6)) = 'chisqs';

dlmwrite(filename,['- We will do ' num2str(noruns) ' steps.'],'-append','delimiter','')
dlmwrite(filenameh,['- We will do ' num2str(noruns) ' steps.'],'-append','delimiter','')

dlmwrite(filename,'- Steps in 1/m_wdm are:','-append','delimiter','')
dlmwrite(filenameh,'- Steps in 1/m_wdm are:','-append','delimiter','')
dlmwrite(filename,ms_steps,'-append','delimiter',' ')
dlmwrite(filenameh,ms_steps,'-append','delimiter',' ')

dlmwrite(filename,tmp,'-append','delimiter','')
dlmwrite(filenameh,tmp,'-append','delimiter','')

%% Fiducial model
if halobool
    [CLS{1} CLSh{1} noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
        nzpars.nbin,NaN,M_vals);
else
    [CLS{1} xx noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
end

%% Theoretical models
storechisq = zeros(length(omega_m_steps),length(ns_steps),...
    length(As_steps),length(Gamma_steps),length(ms_steps));
storechisqh = storechisq;
for oint = 1:nosteps(1) % omega_m
    
    cospars.omega_m = omega_m_steps(oint);
    
    for nint = 1:nosteps(2) % ns
        
        cospars.ns = ns_steps(nint);
        
        for aint = 1:nosteps(3) % As
            
            %fprintf('.')
            
            cospars.As = As_steps(aint);
            
            for gint = 1:nosteps(4) % Gamma
                
                cospars.Gamma = Gamma_steps(gint);
                                
                for wint = 1:nosteps(5) % WDM
                    
                    cospars.m_wdm = ms_steps(wint);
                    
                    if halobool
                        [CLS{wint+1} CLSh{wint+1}] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
                            nzpars.nbin,NaN,M_vals);
                    else
                        CLS{wint+1} = lens_ps_get(cospars,nzpars,z_vals,l_vals);
                    end
                    
                end
                
                chis = chisq(CLS,nzpars,l_vals,nsigma,noise);
                dlmwrite(filename,[cospars.omega_m cospars.ns cospars.As cospars.Gamma chis],'-append','delimiter',' ')
                storechisq(oint,nint,aint,gint,:) = chis;

                if halobool
                    chish = chisq(CLSh,nzpars,l_vals,nsigma,noise);
                    dlmwrite(filenameh,[cospars.omega_m cospars.ns cospars.As cospars.Gamma chish],'-append','delimiter',' ')
                    storechisqh(oint,nint,aint,gint,:) = chish;
                end
                
            end
        end
    end
end

toc

c = clock;
dlmwrite(filename,['- Stopping at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) ' of May 2010.'],'-append','delimiter','')
dlmwrite(filenameh,['- Stopping at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) ' of May 2010.'],'-append','delimiter','')