% 'lens_methods_test.m'
% 
% Yet another testing script for comparing the halo vs the T&J method. 
% Another good one is cls_methods.m!
%
% Now modifying it to calculate the entire shear power spectrum (single source redshift)!
%
% version 2
% 17.11.09 KMarkovic

if 0
    tic
    clear

    [tmp tmp cospars] = setWMAPpars;
    nzpars = setnzpars([1 1],{'nbin' 'zs' 'smith2'});
    z_vals = 0.00001:0.1:1;
    l_vals = 2*logspace(0,4,20);
    M_vals = logspace(0,15,50);
  
    [cls halo] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);

    figure(1)
    plotlens(l_vals,cls,'b',1,1e-9,9e-5,16,2);
    plotlens(l_vals,halo,'g',1,1e-9,9e-4,16,2);
    axis([l_vals(1)/2 max(l_vals) 1e-9 8e-3])
    title('Weak Lensing Power Spectrum plotting methods');
    xlabel('multipole l','fontsize',15);
    ylabel('l(l+1)C_l/2\pi','fontsize',15);
    legend('Takada & Jain','Cooray et al',2)

    %ratio = squeeze(halo./cls); 
    %clf
    %loglog(l_vals,ratio)
    %xlabel('multipole l','fontsize',15);
    %ylabel('ratio = C_h_a_l_o / C_T_&_J','fontsize',15) ;
  

    toc
end

%% Here we are testing the lensing weight part:

if 1
    
    % SIGMAcrit

     clear
%     
     z_vals = 0.001:0.1:1.00001;
     nlintype = 'smith2';
     nzpars = setnzpars([1 1],{'nbin' 'zs' nlintype});
     [tmp tmp cospars] = setWMAPpars;

    consts;

    % Will be converting this to Sarah's units:
    %rho_crit_0 = rho_crit_0/(H0^2) * (H0_Mpc^2) *M_solar /(Mpc^3);
    
    % Distances and differential comoving volume
    %Dh = c/H0; % Hubble distance
    Dh = c_km/H0_Mpc; % in Mpc
    
    slbpars = convertpars(cospars);
    chim = D_wa_grid(z_vals,slbpars);

    [chis tmp tmp tmp volm] = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    %clear chis; chis = chim(1,:); % WRONG??!!!!!!
    %chilens = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
    
    %chimat = chim * Dh;
    chilens = chis * Dh;    chilens(chilens==0) = min( chilens(chilens~=0) ) * 1e-10;

    chiso = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    chisource = chiso * Dh;
    %chi_ls = chisource - chi; % This can't be right!!!!!
    %chisource = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
    chils = chisource-chilens;

    rho_crit_0 = 3 * H0_Mpc^2  / (8*pi*G_Mpc); % units of km^2 M_sol / Mpc^5
    rho_mean_0 = rho_crit_0 * cospars.omega_m;% M_solar /Mpc^3;

    SIGMAcrit = (c_km^2) .* chisource ./ ( 4*pi*G_Mpc .* chilens.* chils ) ./(1+z_vals); %kg/m^2
    % actually units of this one her are: km^2 M_sol / Mpc^4
    
    mypar = 3/2 * H0_Mpc^2 / c_km^2 * cospars.omega_m * (1 + z_vals) .* chilens .* chils / chisource;
    
    plotWbool=0;
    if plotWbool
    plot(z_vals, rho_mean_0./SIGMAcrit,'g','linewidth',5); hold on
    %myfavpar = rho_mean_0./SIGMAcrit*Mpc/cospars.h;
    %plot(z_vals, myfavpar,'g'); hold on

    plot(z_vals,mypar,'mo','linewidth',5); hold on
    end
   
   %%%%%%%%%%%%
   % Lensing weight:

    %z_vals = 0.000000001:0.1:0.9999999;
    %nzpars.zs = 1;
    %[tmp tmp cospars] = setWMAPpars;
    %consts
    %Dh = c/H0;

    % Find distances to and between all z_vals:
    %slbpars = convertpars(cospars);
    %chimat = D_wa_grid(z_vals,slbpars);
    chimat = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

    chisource = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
   
    % Lensing weight
    %nzpars = setnzpars([1 1],{'nbin' 'zs'});
    [n_z_mat niz] = get_nofz(nzpars,z_vals);
    Wi_chi = lens_weight(cospars,z_vals,chimat,n_z_mat,nzpars.zs,chisource);
    % Calculate it by hand:
    %c_km = 299792.5;
    %Dh = c_km/(100*cospars.h); % Matching this to have units of h for wi_z
    KO = 3/2 * cospars.omega_m * (Dh)^(-2); 
    %KO = 3/2 * cospars.omega_m * (100*cospars.h)^2;
    
    %chi = chimat(1,:)*Dh; % comov ag diam dists = comov dist
    chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;
    chi_source = chisource*Dh;
    chi_lss = chi_source-chi; % Yikes! Only for omega_k = 0!!!!!!
  
    a_vals = 1./(1+z_vals);
    %Wi_l = zeros(length(z_vals),1);

    ni_z = n_z_mat(:,1)';
    
    %for zlint = 1:(length(z_vals)-1)
    for zlint = 1:length(z_vals)


        dWi_l = 0; % clear the integral

        %chi_ls = chi_mat(zlint+1,:);
        % Since first line is obs->gal dist.
        % Actually obs redshift taken to be ~ first lens redshift, so there
        % is no special first line:
        %chi_ls = chimat(zlint,:);
        chi_ls = chi_lss(zlint);
        
        dWi_l = n_z_mat' .* chi(zlint) .* chi_ls ./ chi_source  ;%.* dchi;
        %Wi_l = chi .* chi_lss ./ chi_source  ;%.* dchi;
        
        % ni_z = 0 outside bin so can now just sum for integration!
        % no dchi because it is included in the nofz!!! since normalised!!!

        Wi_l(zlint) = sum(dWi_l);
        
        %or:
        
        Wi_l(zlint) = sum(ni_z) * chi(zlint).* chi_lss(zlint) ./ chi_source;


    end
    %Wi_l(1) = 0; % Make sure to delete what we add in line 34 above.
    %Wi_l(zlint+1) = 0;
    wiofz = KO * Wi_l ./ a_vals;

    if plotWbool
    plot(z_vals,Wi_chi*Dh*cospars.h^2,'y--','linewidth',1.1); hold on
    %plot(z_vals,Wi_chi,'b.'); hold on
    plot(z_vals,wiofz,'bx','linewidth',1.1); hold on
    xlabel('redshift','fontsize',15); ylabel('Wi(\chi)*D_h*h^2','fontsize',15)
    title('Lensing weight','fontsize',15)
    axis([z_vals(1) z_vals(length(z_vals)) 0 max(max(mypar),max(wiofz'))+max(mypar)/10])

    figure
    plot(z_vals,Wi_chi*Dh*cospars.h^2./(rho_mean_0./SIGMAcrit'))
    title('ratio of the two methods for getting lensing weight')
    xlabel('redshift','fontsize',15); ylabel('ratio','fontsize',15)
    end
end

%%

if 1
    
    %clear  ; figure
    
% Here we are testing the "MATTER POWER SPECTRUM PART":

% z_vals = [0.2 0.5 0.9]; %%% [no units]
%[tmp tmp cospars] = setWMAPpars; %%% [no units]
l_vals = 2*logspace(0,5,20); %%% [no units !!]


% First the constants:
consts %%% [lots of units!!!]
Dh = c_km/100/cospars.h; % unit fun:     [km/s]   /    [km/s/Mpc]    =    [Mpc/h] 
% ACTUALLY ITS [Mpc]!!
%Dh = c/H0; %%% [meters]


% Now the matter power spectrum - in & cf:
k_vals = logspace(-4,4,1000); %%% [h/Mpc]

[pk_nlin pk_lin_real] = pk_nlin_get(cospars,k_vals,z_vals,nlintype); 
%%% [Mpc^3/h^3] I guess...?


% Now the distances and such matters:
X_tmp = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000); 
%%% [Dh]
%X = X_tmp * Dh; %%% [meters]
X = X_tmp * Dh; %%% [Mpc]


% Now the mass integral for:
M_vals = logspace(0,16,10); %%% [M_solar]

M_scale = mascale(cospars,z_vals,k_vals,pk_lin_real); %%% [M_solar]


% % Initialize variables:
% IntM_C = zeros(length(z_vals),length(l_vals));
% IntM_P = IntM_C;
% tmp_pk_C = zeros(length(z_vals),length(k_vals));
% tmp_pk_P = tmp_pk_C;
% dndm = zeros(length(M_vals),length(z_vals));
% DELTA_C = tmp_pk_C;
% DELTA_P = tmp_pk_C;
% DELTA_nlin = tmp_pk_C;

bool = 1; % If =1 - broken, if =0 - works!
if bool
[rv_vals theta_v conc rho_s] = haloparams(cospars.omega_m,...
        cospars.h,M_scale,M_vals,z_vals,X*Mpc);%%% [meters][no units][no units][kg/m^3]
rv_vals = rv_vals/Mpc;
rho_s = rho_s/M_solar*Mpc^3;
end

dX = X(2:length(X))-X(1:(length(X)-1));
dX(length(X)) = interp1(z_vals(1:(length(z_vals)-1)),dX,max(z_vals),'spline','extrap');

for zint = 1:length(z_vals) 

    if ~bool
[rv_vals(:,zint) theta_v(:,zint) conc(:,zint) rho_s(:,zint)] = haloparams(cospars.omega_m,...
        cospars.h,M_scale(zint),M_vals,z_vals(zint),X(zint)*Mpc);%%% [meters][no units][no units][kg/m^3]
% This bit here doesn't work outside the loop properly!!!! 17.11.09 KM
rv_vals(:,zint) = rv_vals(:,zint)/Mpc;
rho_s(:,zint) = rho_s(:,zint)/M_solar*Mpc^3;
    end
     
% First Sigma_l
ONE = ones(length(z_vals),1);%%%[no units here!]
SIGMA_l(:,zint,:) = fourierkappa(z_vals(zint), X(zint), M_vals, l_vals,theta_v(:,zint),...
    rv_vals(:,zint), conc(:,zint), rho_s(:,zint), ONE,200);
%%%[kg/m^2]
%SIGMA_l = SIGMA_l*M_solar/Mpc^2;


% Now mass functions:
[dndm_un(:,zint) tmp M_stars sigma(:,zint)] = massfns_slb(M_vals*cospars.h,z_vals(zint),...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_real(zint,:)); % Pk today or at z????
%%% [(Mpc/h)^-3/(M_solar/h)] = [h^4/Mpc^3/M_solar] [same] [M_solar/h][unitless!]

dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]

% Bias:

% Okay lets try out this weird new delta_c2 (from Henry 2000):
delta_c(zint) = delta_c_alt(z_vals(zint),cospars.omega_m); %%%[no units]

a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]

%%%%%%%% for was here %%%%%%%%
    
    % Need to normalise the mass functions (units: (Mpc/h)^-3 / (M_solar/h) )!
    %norm = sum( dndm_unnorm(:,zint)' * cospars.h^4.* dM .* M_vals*M_solar / rho_mean_0 );
    
    rho_mean_Mpc = rho_crit_0_Mpc * cospars.omega_m*(1+z_vals(zint))^3; %%%[M_sol/Mpc^3]
    %rho_mean_Mpc = rho_crit_0_Mpc * cospars.omega_m; %%%[M_sol/Mpc^3]
    %rho_mean = rho_crit_0 * cospars.omega_m*(1+z_vals(zint))^3; %%%[kg/m^3]
    
    %norm = sum( dndm_un(:,zint)' .* dM*cospars.h^4 .* M_vals/ rho_mean_Mpc); %%% [no units]
    norm = sum( dndm_un(:,zint)' .* dM .* M_vals/ rho_mean_Mpc) ; %%% [no units]
 
    %dndm(:,zint) = dndm_un(:,zint) / norm / Mpc^3 * cospars.h^4;
    
    %dndm(:,zint) = dndm_un(:,zint) / norm * cospars.h^4; %%%[Mpc^-3 M_solar^-1]
    dndm(:,zint) = dndm_un(:,zint) / norm; %%%[h^4 Mpc^-3 M_solar^-1]

    
    % Now find the bias:
    nu = delta_c(zint) ./ sigma(:,zint).^2 ; %%% [unitless]

    % From Seljak 0001493v1 (after equn 9):
    bias_unnorm =  1  +  ( nu - 1 )/delta_c(zint)  +  2*p./( 1 + (a*nu).^p );
    
    % Need to normalise the bias:
    norm = sum( dndm(:,zint)'  .* dM .* M_vals  / rho_mean_Mpc .* bias_unnorm' ); %%%[no units]
    bias = bias_unnorm / norm; %%%[no units]

 
    % Now the mass integral:
    for lint = 1:length(l_vals)
        
        % THE UNITS ARE NOW DIFFERENT THAN WHAT IT SAYS HERE!!!!:

        dIntM_C = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint) .* bias; %%%[Mpc^-3 kg m^-2]

        %IntM_C(zint,lint) = sum(dIntM_C) / Mpc^3; %%%[kg m^-5]
        IntM_C(zint,lint) = sum(dIntM_C); %%%[M_sol Mpc^-5]
        
        dIntM_P = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint).^2; %%%[Mpc^-3 kg^2 m^-4]

        %IntM_P(zint,lint) = sum(dIntM_P)/ Mpc^3; %%%[kg^2 m^-7]
        IntM_P(zint,lint) = sum(dIntM_P); %%%[M_sol^2 Mpc^-7]

    end

    %tmp_Correlations = IntM_C(zint,:) .* X(zint)^2 / rho_mean_0; %%%[no units]
    
    %tmp_Correlations = IntM_C(zint,:) .* X(zint)^2 / rho_mean(zint); %%%[no units]
    tmp_Correlations = IntM_C(zint,:) .* X(zint)^2 / rho_mean_Mpc; %%%[no units]
    
    %tmp_Poisson = IntM_P(zint,:) .* X(zint)^4 / rho_mean_0^2;%%%[m^3]
    
    %tmp_Poisson = IntM_P(zint,:) .* X(zint)^4 / rho_mean(zint)^2;%%%[m^3]
    tmp_Poisson = IntM_P(zint,:) .* X(zint)^4 / rho_mean_Mpc^2;%%%[m^3]
    
    % The above makes no difference, clearly, at z = 0!
    
    %tmp_intrpl_C = interp1(l_vals/X(zint)*Mpc/cospars.h,tmp_Correlations,k_vals);%%%[no units]
    %tmp_intrpl_C = interp1(l_vals/X(zint)/cospars.h,tmp_Correlations,k_vals);%%%[no units]
    pk_lin_intrpl(zint,:) = interp1(k_vals*X(zint)*cospars.h,pk_lin_real(zint,:),l_vals);%%%[no units]
    pk_lin_intrpl(zint,isnan(pk_lin_intrpl(zint,:) )) =0;
    
    %tmp_intrpl_P = interp1(l_vals/X(zint)*Mpc/cospars.h,tmp_Poisson,k_vals);%%%[m^3]
    %tmp_intrpl_P = interp1(l_vals/X(zint)/cospars.h,tmp_Poisson,k_vals);%%%[m^3]
    tmp_intrpl_P = tmp_Poisson;%%%[m^3]

    % THE UNITS IN THE FOLLOWING TWO QUANTITIES ARE WRONG:
    
    %tmp_pk_C(zint,:) = tmp_intrpl_C.^2 .* pk_lin_real(zint,:); %%%[Mpc^3 h^-3]
    tmp_pk_C(zint,:) = tmp_Correlations.^2 .* pk_lin_intrpl(zint,:); %%%[Mpc^3 h^-3]
    
    tmp_pk_P(zint,:) = tmp_intrpl_P * cospars.h^3; %%% [m^3]

    %semilogx(l_vals/X(zint)*Mpc/cospars.h,tmp); hold on

    % Dimensionlass MPS:
    %DELTA_C(zint,:) = k_vals.^3.*tmp_pk_C(zint,:) /2/pi; %%%[no units]
    %DELTA_P(zint,:) = k_vals.^3.*tmp_pk_P(zint,:) /2/pi  * (cospars.h/Mpc)^3; %%%[no units]
    %DELTA_P(zint,:) = k_vals.^3.*tmp_pk_P(zint,:) /2/pi ; %%%[no units]
    % Real:
    %DELTA_nlin(zint,:) = k_vals.^3 .*pk_nlin(zint,:) /2/pi; %%%[no units]
    
    
    % Just for testing:
    pk_nlin_intrpl(zint,:) = interp1(k_vals*X(zint)*cospars.h,pk_nlin(zint,:),l_vals); 
    pk_nlin_intrpl(zint,isnan(pk_nlin_intrpl(zint,:) )) =0;

   
   % Lensing Power Spectrum:
%    if zint == 1
%         dX(zint)= X(zint) - 0;
%    else
%        dX(zint) = X(zint) - X(zint-1);
%    end
   dcl_P(zint,:) = tmp_pk_P(zint,:) * mypar(zint).^2 /X(zint)^2 *dX(zint)/cospars.h^3;
   dcl_C(zint,:) = tmp_pk_C(zint,:) * mypar(zint).^2 /X(zint)^2 *dX(zint)/cospars.h^3;
   
   dcl_tj_1(zint,:)  = pk_nlin_intrpl(zint,:)  * wiofz(zint)^2/X(zint)^2 *dX(zint) /cospars.h^3;%*2.68;%*(1+z_vals(zint))^3;%!!!!
   %dcl_tj_1(zint,:)  = pk_nlin_intrpl(zint,:)  * Wi_chi(zint)^2*Dh^2*cospars.h/X(zint)^2 *dX(zint) ;%*2.55;%!!!!

   
end

%pk_lin_out  = pk_lin_out * (Mpc/cospars.h)^(-3);

%loglog(k_vals([1 50 100*(1:10)]),tmpt)

% This should be = 1!:
%semilogx(k_vals,pk_lin_out);

% figure
% %fact = cospars.h^(-6);
% fact = 1;
% l=loglog(k_vals,fact*DELTA_C,'r'); hold on
% m=loglog(k_vals,fact*DELTA_P,'b'); hold on
% n=loglog(k_vals,fact*DELTA_P+fact*DELTA_C,'g'); hold on
% o=loglog(k_vals,k_vals.^3.*pk_lin_real/2/pi,'k:');
% axis([1e-2 9e2 1e-2 9e4])
% p=loglog(k_vals,DELTA_nlin,'k--'); 
% set([l m n o p],'linewidth',1.2);
% ylabel('\Delta^2(k)'); xlabel('k[h/Mpc]')
% legend([l m o p],'2H' ,'1H' ,'P_l_i_n(k)' ,'P_n_l_i_n(k)',2)
% title('z = 1')


% LPS LPS LPS LPS LPS LPS LPS LPS
% figure(1) %clf
% 
% [cls halo] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);
% e = plotlens(l_vals,cls,'k');
% f = plotlens(l_vals,halo,'g');
% set([e f],'linewidth',1.1)
% 
% cl_P = sum(dcl_P,1)';
% cl_C = sum(dcl_C,1)';
% cl = cl_P + cl_C;
% a = plotlens(l_vals,cl_P,'b:');
% b = plotlens(l_vals,cl_C,'r:');
% c = plotlens(l_vals,cl,'y--');
% cl_tj_1 = sum(dcl_tj_1,1)';
% d = plotlens(l_vals,cl_tj_1,'c-.');
% set([a b c d],'linewidth',1.1)
% 
% axis([l_vals(1) l_vals( length(l_vals) )  9e-10 9e-3]) ;

% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
figure
l=loglog(l_vals,tmp_pk_C,'r'); hold on
m=loglog(l_vals,tmp_pk_P,'b'); hold on
n=loglog(l_vals,tmp_pk_P+tmp_pk_C,'g'); hold on
o=loglog(l_vals,pk_lin_intrpl,'k:');
axis([2 2e4 1 9e4])
p=loglog(l_vals,pk_nlin_intrpl,'k--'); 
set([l m n o p],'linewidth',1.2);
ylabel('P(k)'); xlabel('l, multipole')
%legend([l m o p],'2H' ,'1H' ,'P_l_i_n(k)' ,'P_n_l_i_n(k)',2)
%title('z = 1')


% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
%figure
% l=loglog(k_vals,tmp_pk_C,'r'); hold on
% m=loglog(k_vals,tmp_pk_P,'b'); hold on
% n=loglog(k_vals,tmp_pk_P+tmp_pk_C,'g'); hold on
% o=loglog(k_vals,pk_lin_real,'k:');
% axis([1e-4 10 1 9e4])
% p=loglog(k_vals,pk_nlin,'k--'); 
% set([l m n o p],'linewidth',1.2);
% ylabel('P(k)'); xlabel('k[h/Mpc]')
% %legend([l m o p],'2H' ,'1H' ,'P_l_i_n(k)' ,'P_n_l_i_n(k)',2)
% title('z = 1')

% dtheta = 0.01;
% theta = 0:dtheta:2;
% l_ln = 10;
% 
% figure(2)
% Bessels = besselj(0,(l_ln+0.5)*theta);
% plot(theta,Bessels,'k'); hold on
% l_s  = 100;
% figure(3)
% integral = cumsum(Bessels*dtheta);
% plot(theta,integral,'k'); hold on
% 
% figure(2)
% Bessels = besselj(0,(l_s+0.5)*theta);
% plot(theta,Bessels,'r')
% l_nl = 1000;
% figure(3)
% integral = cumsum(Bessels*dtheta);
% plot(theta,integral,'r')
% 
% figure(2)
% Bessels = besselj(0,(l_nl+0.5)*theta);
% plot(theta,Bessels,'b')
% legend('lin','trans','nonlin')
% figure(3)
% integral = cumsum(Bessels*dtheta);
% plot(theta,integral,'b')
% 
% legend('lin','trans','nonlin')
%z = 0.00001;
%dist = Ds_wconst(0,z,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*c_km/100; %%% [Mpc/h]
%k_ln = l_ln/dist
%k_s = l_s/dist
%k_nl = l_nl/dist

%%%%% PART II - should be one at z = 0!

% zs = 9;
% Dh = c_km/H0_Mpc; % in Mpc
% chilens = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
% chisource = Ds_wconst(0,zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
% chiLS = chisource-chilens;
% 
% SIGMAcrit_tmp = (c_km^2) .* chisource ./ ( 4*pi*G_Mpc .* chilens.* chiLS ) ./(1+z_vals);
% % actually units of this one her are: km^2 M_sol / Mpc^4
% SIGMAcrit = SIGMAcrit_tmp * 1000^2 * M_solar /Mpc^4;
% 
% constant = 3/2*cospars.omega_m*H0^2/c^2;
% %constant = 4*pi*G*rho_mean_0/c^2;
% 
% %otherside = X * sum(dM'.*dndm.*bias.*SIGMA_l(:,1,1)/Mpc^3)/SIGMAcrit;
% otherside = X * sum(dM'.*dndm.*bias.*SIGMA_l(:,1,1)/Mpc^3)/SIGMAcrit;
% 
% only_for_z0 = constant/otherside
% 
% %%%% PART III
% for lint = 1:length(l_vals)
%     %shouldbeone(lint) = X ^2* sum(dM'.*dndm.*bias.*SIGMA_l(:,1,lint)/Mpc^3)/rho_mean;
%     shouldbeone(lint) = X ^2* sum(dM'.*dndm.*bias.*SIGMA_l(:,1,lint))/rho_mean_Mpc;
% end
% figure
% semilogx(l_vals,shouldbeone.^2); hold on
% %semilogx(k_vals*X(zint)/Mpc*cospars.h,DELTA_C./(k_vals.^3.*pk_lin_real/2/pi),'k')
% semilogx(k_vals*X(zint)*cospars.h,DELTA_C./(k_vals.^3.*pk_lin_real/2/pi),'k')
% xlabel('l, multipole')
    
end