% lens_ps_omegam_demo
%
% This script plots the Takada & Jain power spectrum with varying omega_m.
%
% version 4
% 28.01.10 KMarkovic

clear

%figure

cospars = setWMAPpars;
nzpars = setnzpars(10,{'nbin' 'smith2'}); dz = 0.2; z_vals = 0.0001:dz:5;
bin = 9; % to plot

l_vals = logspace(1,4,50);
%%

% omegam = 0.25
cospars.omega_m = 0.25;
Clsw1 = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    

% omega_m = 0.255
cospars.omega_m = 0.255;
Clsw2 = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    

% omega_m = 0.26
cospars.omega_m = 0.26;
Clsw3 = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    
%p7 = plotlens(l_vals,Clsw3(bin,bin,:),'g',1);

%% omega_m = 0.256499
clear cospars
cospars = setWMAPpars;
Clsc = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    

%% Ratios:
figure
pr1 = semilogx(l_vals,squeeze((Clsw1(bin,bin,:)-Clsc(bin,bin,:))./Clsc(bin,bin,:)),'m'); hold on
pr2 = semilogx(l_vals,squeeze((Clsw2(bin,bin,:)-Clsc(bin,bin,:))./Clsc(bin,bin,:)),'r');
pr3 = semilogx(l_vals,squeeze((Clsw3(bin,bin,:)-Clsc(bin,bin,:))./Clsc(bin,bin,:)),'g');
pr0 = semilogx(l_vals,zeros(size(l_vals)),'k'); 
xlabel('l, multipole'); ylabel('(C_\kappa-C_\kappa^f^i^d)/C_\kappa^f^i^d')
legend([pr1 pr2 pr0 pr3 ],'\Omega_m = 0.25','\Omega_m = 0.255','\Omega_m = 0.256','\Omega_m = 0.26',3)
lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
set([pr3 pr2 pr1],'linewidth',1.2);
axis([l_vals(1) l_vals(length(l_vals)) -0.15 0.07])
%title('Smith et al.')
%saveas(gcf,'lens_ps_omegam_ratios_2801','pdf')

%% Power spectra
figure
p1=plotlens(l_vals,squeeze(Clsw1(bin,bin,:)),'m'); hold on
p2=plotlens(l_vals,squeeze(Clsw2(bin,bin,:)),'r');
p3=plotlens(l_vals,squeeze(Clsw3(bin,bin,:)),'g');
p0=plotlens(l_vals,squeeze(Clsc(bin,bin,:)),'k');
xlabel('l, multipole'); ylabel('C_\kappa')
legend([p1 p2 p3 p0],'omega_m = 0.25','omega_m = 0.255','omega_m = 0.26',3)
set([p3 p2 p1 p0],'linewidth',1.2);
axis([3*l_vals(1) l_vals(length(l_vals)) 4e-6 2e-4])
%saveas(gcf,'lens_ps_wdm__2801','pdf')