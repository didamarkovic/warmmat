% 'lens_ps_cf_slb_demo'
%
% 12/08/08 KM

clear

k_vals = logspace(-2.5,4, 1000);

l_vals = logspace(1,4,50); plotfactor = l_vals.*(1+l_vals)/2/pi;

z_vals = 0.0001:0.03:3;

nzpars = setnzpars([1 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'});

type = 'smith2';

% Blue line: Omega_m = 0.3 & w = -1
cospars_b = setpars([0.047 0.7 0.9 0.3 0.7],{'omega_b'; 'h'; 'sigma8'; 'omega_m'; 'omega_de'});
pk_vals_b = pk_nlin_get(cospars_b,k_vals,z_vals,type);
[cls_blue noise] = lens_ps(cospars_b,z_vals,l_vals,k_vals,pk_vals_b,nzpars);

[l_vals_blue lens_cls_nl_vals_blue]=...
    lens_cls_doall(cospars_b.omega_m,cospars_b.omega_b,cospars_b.omega_de,...
    cospars_b.w0,cospars_b.h,cospars_b.sigma8,nzpars.alpha,nzpars.beta,...
    nzpars.z_0,l_vals(1),max(l_vals),length(l_vals));
 
% Red line: Omega_m = 0.35 & w = -1
cospars_r = cospars_b;
cospars_r.omega_m = 0.35;
cospars_r.omega_de = 0.65;
pk_vals_r = pk_nlin_get(cospars_r,k_vals,z_vals,type);
[cls_red noise] = lens_ps(cospars_r,z_vals,l_vals,k_vals,pk_vals_r,nzpars);

[l_vals_red lens_cls_nl_vals_red]=...
    lens_cls_doall(cospars_r.omega_m,cospars_r.omega_b,cospars_r.omega_de,...
    cospars_r.w0,cospars_r.h,cospars_r.sigma8,nzpars.alpha,nzpars.beta,...
    nzpars.z_0,l_vals(1),max(l_vals),length(l_vals));

% Green line: Omega_m = 0.3 & w = -0.7
cospars_g = cospars_b;
cospars_g.w0 = -0.7;
pk_vals_g = pk_nlin_get(cospars_g,k_vals,z_vals,type);
[cls_green noise] = lens_ps(cospars_g,z_vals,l_vals,k_vals,pk_vals_g,nzpars);

[l_vals_green lens_cls_nl_vals_green]=...
    lens_cls_doall(cospars_g.omega_m,cospars_g.omega_b,cospars_g.omega_de,...
    cospars_g.w0,cospars_g.h,cospars_g.sigma8,nzpars.alpha,nzpars.beta,...
    nzpars.z_0,l_vals(1),max(l_vals),length(l_vals));


figure; set(gca,'fontsize',15,'linewidth',0.7);
b = loglog(l_vals,plotfactor.*squeeze(cls_blue)','b'); hold on
tmp = l_vals_blue.*(1+l_vals_blue).*lens_cls_nl_vals_blue/(2*pi);
bs = loglog(l_vals_blue,tmp,'c:');

r = loglog(l_vals,plotfactor.*squeeze(cls_red)','r');
tmp = l_vals_red.*(1+l_vals_red).*lens_cls_nl_vals_red/(2*pi);
rs = loglog(l_vals_red,tmp,'m:');

g = loglog(l_vals,plotfactor.*squeeze(cls_green)','g');
tmp = l_vals_green.*(1+l_vals_green).*lens_cls_nl_vals_green/(2*pi);
gs = loglog(l_vals_green,tmp,'y:');

set([b bs r rs g gs],'linewidth',1.5)
axis([l_vals(1) max(l_vals) 1e-6 1e-3])
xlabel('l'); ylabel('C^\kappa_i_j(l) l(l+1) /2\pi')
title('convergence power spectrum - T&J vs SLB')
legend([b r g],'\Omega_m = 0.30 & w = -1','\Omega_m = 0.35 & w = -1',...
    '\Omega_m = 0.30 & w = -0.7',2)
%saveas(gcf,'Lensing_cf_slb_16-08_smith2...','bmp')