% lenshalo_wdm_demo
%
% This script plots the Takada & Jain power spectrum with varying m_wdm in
% the transfer function from Viel et al and with no mass cut-offs.
%
% version 3
% 26.11.09 KMarkovic
% version 3.1
% 22.12.2010 KM

clear

tic

%% figure % clc

cospars = setWMAP7pars;
%nzpars = setnzpars(10,{'nbin' 'smith2'}); 
nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});
dz = 0.03; z_vals = 0.0001:dz:3;
nzpars.noth = 400;
bin = 5; % to plot
bin2 = 5;

l_vals = 2*logspace(1,4,50);
%% WDM:
cospars.omega_wdm = cospars.omega_dm;

% m = 250 eV
cospars.m_wdm = 250;
Clsw1 = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    
%p3 = plotlens(l_vals,Clsw1(bin,bin,:),'m',1);

% m = 1000 eV
cospars.m_wdm = 500;
Clsw2 = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    
%p5 = plotlens(l_vals,Clsw2(bin,bin,:),'r',1);

% m = 2 keV
cospars.m_wdm = 1000;
Clsw3 = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    
%p7 = plotlens(l_vals,Clsw3(bin,bin,:),'g',1);

%% CDM
clear cospars
cospars = setWMAP7pars;
Clsc = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);    
% lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
% p1 = plotlens(l_vals,Clsc(bin,bin,:),'k',1);
% title('Weak Lensing Power Spectrum with WDM');
% set([p5 p7 p1],'linewidth',1.2);
% xlabel('l, multipole'); ylabel('l(l+1)C_\kappa/2\pi')
% legend([p3 p5 p7 p1],'200 eV','500 eV','1 keV','CDM',4)
% axis([l_vals(1) l_vals(length(l_vals)) 3e-7 3e-4])

toc

%% Ratios:
%scrsz = get(0,'ScreenSize');
%figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
% subplot(2,1,2)
% pr1 = semilogx(l_vals,squeeze((Clsw1(bin,bin,:)-Clsc(bin,bin,:))./Clsc(bin,bin,:)),'m'); hold on
% pr2 = semilogx(l_vals,squeeze((Clsw2(bin,bin,:)-Clsc(bin,bin,:))./Clsc(bin,bin,:)),'r');
% pr3 = semilogx(l_vals,squeeze((Clsw3(bin,bin,:)-Clsc(bin,bin,:))./Clsc(bin,bin,:)),'g');
% p0 = semilogx(l_vals,zeros(size(l_vals)),'k'); 
% xlabel('l, multipole'); ylabel('(C_\kappa^W^D^M-C_\kappa^C^D^M)/C_\kappa^C^D^M')
% legend([pr1 pr2 pr3],'400 eV','500 eV','1 keV',3)
% lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
% set([pr3 pr2 pr1],'linewidth',1.2);
% axis([l_vals(1) l_vals(length(l_vals)) -0.15 0.07])
% %title('Smith et al.')
% %saveas(gcf,'lens_ps_wdm_ratios_2801','pdf')

% Power spectra - wlps
figure(1)
%subplot(2,1,1)
p1=plotlens(l_vals,squeeze(Clsw1(bin,bin2,:)),'r--'); hold on
p2=plotlens(l_vals,squeeze(Clsw2(bin,bin2,:)),'b:');
p3=plotlens(l_vals,squeeze(Clsw3(bin,bin2,:)),'g');
p0=plotlens(l_vals,squeeze(Clsc(bin,bin2,:)),'k');
xlabel('l, multipole'); ylabel('C_\kappa^W^D^M')
legend([p1 p2 p0],'400 eV','500 eV','CDM',2)
set([p2 p1 p0],'linewidth',1.2);
%legend([p1 p2 p3 p0],'400 eV','500 eV','1 keV','CDM',2)
%set([p3 p2 p1 p0],'linewidth',1.2);
axis([20 l_vals(length(l_vals)) 2e-6 2e-4])
%axis([l_vals(1) l_vals(length(l_vals)) 1e-5 1e-4])
%saveas(gcf,'lens_ps_wdm__2801','pdf')

%samexaxis('xmt','on','ytac','join','yld',1)

%save 'lens_ps_wdm.mat'