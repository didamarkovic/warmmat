% lens_fish_tomo_demo_more
%
% Need Planck fisher matrix!
%
% 30/09/08 KM

% Now let's plot the error on WDM:

% Intialize the Planck Fisher matrix:
clear planck
fid = fopen('ftran.txt');
planck_tmp = cell2mat(textscan(fid,'%n'));
fclose(fid);
maxint = sqrt(length(planck_tmp));
for int = 1:maxint
    for jnt = int:maxint
        planck(int,jnt) = planck_tmp( (jnt-1)*maxint + int );
        planck(jnt,int) = planck(int,jnt);
    end
end
planck(:,maxint+1) = zeros(maxint,1);
planck(maxint+1,:) = zeros(1,maxint+1);
%%% Intialize the Deep Survey Fisher matrix!


% Initialise the 1st file!
FISH1a = fmat; FISH1b = fmath;
ELLS1 = l_vals;
l_max(1) = max(l_vals);
invfish = pinv(fmat);
error_WDM_noP(1) = sqrt(invfish(7,7));
invfish = pinv(fmat+planck);
error_WDM_planck(1) = sqrt(invfish(7,7));
%invfish = pinv(fmat+planck+deeps);
%error_WDM_PnDS(1) = sqrt(invfish(7,7));

% Initialise the 2nd file!
FISH2a = fmat; FISH2b = fmath; 
ELLS2 = l_vals;
l_max(2) = max(l_vals);
invfish = pinv(fmat);
error_WDM_noP(2) = sqrt(invfish(7,7));
invfish = pinv(fmat+planck);
error_WDM_planck(2) = sqrt(invfish(7,7));
%invfish = pinv(fmat+planck+deeps);
%error_WDM_PnDS(2) = sqrt(invfish(7,7));

% Initialise the 3rd file!
FISH3a = fmat; FISH3b = fmath; 
ELLS3 = l_vals;
l_max(3) = max(l_vals);
invfish = pinv(fmat);
error_WDM_noP(3) = sqrt(invfish(7,7));
invfish = pinv(fmat+planck);
error_WDM_planck(3) = sqrt(invfish(7,7));
%invfish = pinv(fmat+planck+deeps);
%error_WDM_PnDS(3) = sqrt(invfish(7,7));

% Initialise the 4th file!
FISH4a = fmat; FISH4b = fmath; 
ELLS4 = l_vals;
l_max(4) = max(l_vals);
invfish = pinv(fmat);
error_WDM_noP(4) = sqrt(invfish(7,7));
invfish = pinv(fmat+planck);
error_WDM_planck(4) = sqrt(invfish(7,7));
%invfish = pinv(fmat+planck+deeps);
%error_WDM_PnDS(4) = sqrt(invfish(7,7));


% Initialise the 5th file!
FISH5a = fmat; FISH45b = fmath; 
ELLS5 = l_vals;
l_max(5) = max(l_vals);
invfish = pinv(fmat);
error_WDM_noP(5) = sqrt(invfish(7,7));
invfish = pinv(fmat+planck);
error_WDM_planck(5) = sqrt(invfish(7,7));
%invfish = pinv(fmat+planck+deeps);
%error_WDM_PnDS(5) = sqrt(invfish(7,7));

% Now the plots
l_max_intp = logspace(3,5,20);
error_WDM_noP_intp = interp1(l_max,error_WDM_noP,l_max_intp);
error_WDM_planck_intp = interp1(l_max,error_WDM_planck,l_max_intp);
%error_WDM_PnDS_intp = interp1(l_max,error_WDM_PnDS,l_max_intp);
figure
set(gca,'fontsize',15)
a = semilogx(l_max_intp,error_WDM_noP_intp,'k'); hold on
b = semilogx(l_max_intp,error_WDM_planck_intp,'b');
%%c = 0;%plot(l_max_intp,error_WDM_PnDS_intp,'c');
%set([a b c],'linewidth',1.5)
set([a b],'linewidth',1.5)
xlabel('l_m_a_x'); ylabel('\Deltam_W_D_M')
%%legend('lensing survey','with Planck','with Planck & Deep Survey',1)
legend('lensing survey','with Planck',1)
%saveas(gcf,'figure5_v1','bmp')
%semilogx(l_max,error_WDM_noP,'ko')
%semilogx(l_max,error_WDM_planck,'bo')