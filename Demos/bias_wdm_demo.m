% BIAS_WDM_DEMO
% Reproducing plot 4 in arXiv:1103.2134
% 18.05.2011 KMarkovic

clear
clf

cospars = setWMAP7pars; 
cospars.omega_wdm = cospars.omega_dm;

consts

M_vals = logspace(0,16,50); 
k_vals = logspace(-4,4,1000);
z_vals = 1; 
m_wdm = [0 1000 500]; 

cols = {'k';'g';'r'};

a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]

D_plus = growth_factor(cospars,z_vals);

delta_c = delta_c_alt(z_vals,cospars.omega_m);

% For integration:
dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]

rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m; %%%[M_sol/Mpc^3]

Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);

for ii = 1:length(m_wdm)
    cospars.m_wdm = m_wdm(ii);

    pk_lwdm = wdm_transfn_pars(pk_lin,k_vals,cospars);

    [dndm_un tmp M_scale_tmp sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
        cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
        cospars.sigma8,k_vals,pk_lwdm);

    
    norm = sum( dndm_un' .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
    %dndm = dndm_un / norm /(1+z_vals)^3 /cospars.h^(-4) ; %%%[h^4 Mpc^-3 M_solar^-1]
    %norm = cospars.h^(-5) * (1+z_vals)^3; % HMMMMMMMM?????
    %norm = cospars.h^(-4); % Getting rid of h factors!
    %norm = 1;
    dndm = dndm_un / norm; %%%[h^4 Mpc^-3 M_solar^-1]

    % Now find the bias:
    nu = (delta_c ./ sigma ./D_plus).^2; % [unitless]
    % From Seljak 0001493v1 (after equn 9):
    bias_un =  1  +  (a* nu - 1 )/delta_c  +  2*p./( 1 + (a*nu).^p );

    % Need to normalise the bias:
    %norm = sum( dndm'  .* dM .* M_vals  / (rho_mean_Mpc_0*(1+z_vals)^3) .* bias_un' ); %%%[no units]
    norm = sum( dndm'  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias_un' ); %%%[no units]
    %norm = 1;
    bias = bias_un / norm; %%%[no units]

    % Now add WDM modifications: cospars.m_wdm = 1000; cospars.omega_wdm =
    % cospars.omega_dm;

    Mfs = freestreaming_comov(cospars,cospars.m_wdm);%,n00);
    M_vals_cut = M_vals.*(M_vals>Mfs);

    dndm_w = massfn_wdm(M_vals,dndm,Mfs,0.5); %

    numer = sum(dM .* M_vals_cut .* dndm_w') /rho_mean_Mpc_0;
    denom = sum(dM .* M_vals .* dndm'/rho_mean_Mpc_0);
    fr = numer/denom;%%%[Mpc^-3 kg m^-2]

    % Smooth component bias
    numer = sum( dM .* M_vals_cut .* dndm_w'  / rho_mean_Mpc_0 .* bias' );
    denom = sum( dM .* M_vals_cut .* dndm_w'  / rho_mean_Mpc_0);
    b_eff = numer/denom;

    bias_smooth = (1-fr*b_eff)/(1-fr);

    loglog(M_vals,bias,[cols{ii}]); hold on
    loglog(M_vals,bias_un,[cols{ii} '--']);
    loglog(M_vals,bias_smooth,[cols{ii} '.'])
    loglog(M_vals,b_eff,[cols{ii} ':'])
end
axis([2e8 2e15 0.5 5.2])
