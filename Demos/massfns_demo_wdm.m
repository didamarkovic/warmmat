% 'massfns_wdm_demo'
%BROKEN!!!
% 26/08/08 KM

clear;
makeplotnice;

z_vals = 0;

omega_m = 0.3;
omega_de = 0.67;
w = -1;
omega_dm = 0.25; omega_wdm = 0.25;
omega_b = 0.05;
h = 0.65;
sigma8 = 0.9;
nu_power = 1.2; % As Bode,Ostriker&Turok 0010389v3

m_dm = [1e20 200 500 1000 7000 10000];

k_vals = logspace(-4,4,500);
Gamma = exp(-2*omega_b*h)*omega_m*h;
pkCDM = pkf(Gamma,sigma8,k_vals);

M_vals = logspace(0,16,100);

cols = {'k' 'r' 'm' 'g' 'c' 'b'};

clf;
for int = 1:length(m_dm)
    [tf a pk(int,:)] = wdm_transfn(m_dm(int),omega_wdm,h,k_vals,nu_power,pkCDM,1,sigma8); 
    %dndm(int,:) = massfns_fast(M_vals,z_vals,omega_b,omega_m,omega_de,w,h,...
    %    sigma8,'null',k_vals,tf,pk(int,:));
dndm(int,:) = massfns_slb(M_vals,z_vals,omega_b,omega_m,omega_de,w,h,...
                                                  sigma8,k_vals,pk(int,:));
    pl = loglog(M_vals,M_vals.*dndm(int,:),cols{int}); hold on
    set(pl,'linewidth',1.5)
end
title('mass functions for WDM: 50 different M-vals & k: -4->4 in 10000 steps')
xlabel('M [M_s_o_l_a_r /h]')
ylabel('dn(M,z)/dlnM')
legend('CDM','m_w_d_m = 0.2 keV','m_w_d_m = 0.5 keV','m_w_d_m = 1 keV',...
    'm_w_d_m = 7 keV','m_w_d_m = 10 keV',3)
axis([1e0 1e16 1e-10 1e8])
makeplotnice;

%saveas(gcf,'dndM_WDM_3','bmp')