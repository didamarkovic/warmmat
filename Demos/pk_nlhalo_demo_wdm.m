% A script that plots \Delta(k)^2 for CDM & WDM
% It takes a while. (20 sec?)
%
% 04.03.2010 KM

tic

clear

figure

k_vals = logspace(-4,4,1000);

cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;

z_vals = 0.1;

m_wdm = [0 1000 400];


Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin_cdm_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);

linecol = 'krmbgy';
for int = 1:length(m_wdm) % clf

    cospars.m_wdm = m_wdm(int);

    pk_lin_wdm_0 = pk_wdmtransf(cospars,0,k_vals,pk_lin_cdm_0); % [Mpc^3/h^3]

    pk_halo = pk_nlhalo(k_vals,pk_lin_wdm_0',cospars,z_vals, logspace(0,16,100),0,512);

    pk_smith = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');

    %figure(3)
     %a(int) = loglog(k_vals,pk_smith,[linecol(int) '-']); hold on
     %b(int) = loglog(k_vals,pk_lin_wdm_0',[linecol(int) ':']);
     %c(int) = loglog(k_vals,pk_halo,'b--');
    %
    DELTA_smith = k_vals.^3 .*pk_smith /2/pi; %%%[no units]
    DELTA_lin = k_vals.^3 .*pk_lin_wdm_0'/2/pi; %%%[no units]
    DELTA_halo = k_vals.^3 .*pk_halo /2/pi;
    %
    a(int) = loglog(k_vals,DELTA_smith,[linecol(int) '-']); hold on
    b(int) = loglog(k_vals,DELTA_lin,[linecol(int) ':']);
    c(int) = loglog(k_vals,DELTA_halo,[linecol(int+length(linecol)/2) '-']);
    %c(int) = b(int);


end

set([a b c],'linewidth',1.2);
ylabel('\Delta^2(k)'); xlabel('k, waveno.')
%ylabel('P(k)'); xlabel('k, waveno.')
axis([k_vals(1) max(k_vals) 1e-10 9*1e7])
legend([a(int) b(int) c(int)],'smith et al','linear','halo',2)
titlestring = ['at z=' num2str(z_vals) ', CDM & WDM (' num2str(m_wdm(2)) ' & ' num2str(m_wdm(3)) ' eV) & my M_s'];
title(titlestring)

toc