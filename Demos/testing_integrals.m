% Testing the convergence of a Bessels function integral.
% Also testing the analytical integral for Kappa(Theta).
% Also testing the Fourier transform of Kappa(Theta) -> Kappa(l).
%
% Takes a couple of minutes to run!
%
% 14.10.2009 KMarkovic
% 16.11.2009 - added stuff

% First integrate the inbuilt function:
clear
tic
figure
Fn = @(x)besselj(0,x);
xx = 0:0.1:100;
Inte = zeros(length(xx),1);
for int = 2:length(xx)
    Inte(int) = quad(Fn,0,xx(int));
end
plot(xx,Inte,'b'); hold
plot(xx,Fn(xx),'m')
plot(xx,zeros(length(xx)),'k-')
toc

% Now try analytical integral of Bessel series (from 0->xx):
tic
k = 0:200;
besselint = zeros(length(xx),1);
for int = 2:length(xx)
    besselint(int) = 2*sum( (-1).^k ./ (factorial(k) .* gamma(k+1)) .* 1./(2*k+1) .* ...
        (xx(int)/2).^(2*k+1) );
end
plot(xx,besselint,'y--')
axis([xx(1) xx(length(xx)) -0.5 1.5])
toc
% USE QUADGK AND NOT JUST QUAD!

%% So let's take a look at the other integral, to get Kappa(theta):
tic
clear
c = 10.3;
r_perp = 1; r_v = 30.9;
rho_s = 15;
SIGMAcrit = 10;
%r_perp * c / r_v
r_parll = -sqrt(r_v^2-r_perp^2):0.00001:sqrt(r_v^2-r_perp^2);
%
rho_nfw = @(r_parll) rho_s ./ ( c/r_v * sqrt(r_parll.^2+r_perp^2) .* ...
    ( 1 + c/r_v * sqrt(r_parll.^2+r_perp^2) ).^2 );
SIGMA_nfw = quadgk(@(r_parll)density_nfw(r_parll,r_perp,rho_s,c,r_v),...
                                    r_parll(1),r_parll(length(r_parll)));
kappa1 = SIGMA_nfw/SIGMAcrit
toc
% This is rather fast, it seems.
% Let's see how fast this it:
tic
kappa2= nfwkappa(r_perp,c,r_v,r_v,rho_s,SIGMAcrit)
toc
% This is much faster.

% But is it correct?
%% The following finds the values of r_v, theta_v, con, rho_s, M_scale...
clear; %figure
clf(1)
% Set the values:
z_vals = 1;
[values names cospars] = setWMAPpars();
consts
M_vals = 1e5;%logspace(5,15,10);
consts
dl = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Mpc;
% Matter power spectrum:
k_vals = logspace(-4,4,1000);
 Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
pk_lin = pkf(Gamma,cospars.sigma8,k_vals);
% Now calculate:
M_scale = mascale(cospars,z_vals,k_vals,pk_lin);
[r_v theta_v con rho_s] = haloparams(cospars.omega_m,cospars.h,M_scale,M_vals,z_vals,dl);
%% And so:
% SO, IDEAL r_perp(1) DEPENDS ON dr_perp!!!:
dfact = 1/200; % - for calculating Sigma_l
%r_perp = dfact*r_v:dfact*0.5*r_v:dfact*20*r_v; - ideal for plotting
r_perp = dfact*r_v:dfact*r_v:r_v;
rho_nfw = @(r_parll) rho_s ./ ( con/r_v * sqrt(r_parll.^2+r_perp(int)^2) .* ...
    ( 1 + con/r_v * sqrt(r_parll.^2+r_perp(int)^2) ).^2 );
for int = 1:length(r_perp)
    dr_parll = (0.00001*sqrt(r_v^2-r_perp(int)^2));
    r_parll = -sqrt(r_v^2-r_perp(int)^2):dr_parll:sqrt(r_v^2-r_perp(int)^2);
    %SIGMA_nfw(int) = quadgk(@(r_parll)density_nfw(r_parll,r_perp(int),rho_s,c,r_v),...
    %                                r_parll(1),r_parll(length(r_parll));
    %SIGMA_nfw(int) = quadgk(@(r_parll)density_nfw(r_parll,r_perp(int),rho_s,c,r_v),...
    %                                -sqrt(r_v^2-r_perp(int)^2), sqrt(r_v^2-r_perp(int)^2));
    SIGMA_nfw(int) = sum(density_nfw(r_parll,r_perp(int),rho_s,c,r_v))*dr_parll;
end
SIGMA1 = SIGMA_nfw;
SIGMA2 = nfwkappa(r_perp,c,r_v,r_v,rho_s,1); % SIGMAcrit=1 - 13.11.09
%figure(1)
%semilogy(r_perp/Mpc,SIGMA1,'k'); hold on
%semilogy(r_perp/Mpc,SIGMA2,'c--')
%line([r_v/con/Mpc ; r_v/con/Mpc] , [0;1]); %clf
%line([r_v/con/Mpc ; r_v/con/Mpc],[min(SIGMA1(SIGMA1~=0));max(SIGMA1)],'Color','r')
%line([r_v/Mpc ; r_v/Mpc],[min(SIGMA1);max(SIGMA1)],'Color','g')
%xlabel('r_p_e_r_p_e_n_d_i_c_u_l_a_r[Mpc]'); ylabel('\Sigma(r_p_e_r_p_e_n_d_i_c_u_l_a_r)')
%title('integral of \rho_N_F_W')
%legend('numerical','analytical','scale radius', 'virial radius',3)
%legend('numerical','analytical','scale radius', 3)
% WHY THE ZEROS FOR LARGE M - it was a precision issue! - fixed!
% Ok, so this means this is a good way of solving the rho_nfw integral!

%% Okay, need to plot Kappa_l or Sigma_l for different M & l values:
% We need to make sure the 1H (Poisson) term of the power spectrum goes to
% zero for large l (or k). So, will need to make sure Sigma_l -> 0 for
% small masses and large l, since dn/dM -> Inf for M->0! For M->Inf, dn/dM
% goes to 0 anyway, so just need to make sure that Sigma_l doesn't -> Inf!
clf(1); tic
l_vals = logspace(1,4,20);
% Bessels function
theta_vals = r_perp/dl;
dtheta = theta_vals(2)-theta_vals(1);
for lint = 1:length(l_vals)
    J_0_vals = besselj(0,(l_vals(lint)+0.5)*theta_vals);
    for tint = 1:length(theta_vals)
        theta = theta_vals(tint);
        dK_l(tint) = dtheta.*theta.*J_0_vals(tint).*SIGMA2(tint) ;
    end
    K_lMZ(lint) = sum(dK_l)*2*pi;
    figure(1)
    plot(theta_vals,dK_l); hold on
end
figure(2)
loglog(l_vals,K_lMZ,'r'); hold on
xlabel('l, multipole')
ylabel('\Sigma_l(M,z)')
%% Problem is in quad(@(x)quad(x))... lets see:
clear
tic
Fn1 = @(x)x.^2;
Fn2 = @(y)quad(Fn1,0,y);
Fn3 = @(z)Fn2(z);
%w = quad(Fn3,0,1);
% This does not work because the limits of integration must be scalars!
% This probably does not solve anything, but also have:
Fn1 = @(x,y) x.^2+y.^2;
w = dblquad(Fn1,0,1,-1,2);
toc

%% Something from Jochen's Matlab book:
clear
tic
x = (0.00000000000001:0.1:1) *2*pi;
y = x.^(-3) + (x/2 + sqrt(x)).^(-x); %sin(x);                     % create rough data
pp = spline(x,y);            % pp-form fitting rough data
integrationconst = y(1);
ppi = mmppint(pp,integrationconst);    % pp-form of integral
xi = linspace(0,2*pi);    % finer points for interpolation
yi = ppval(pp,xi);          % evaluate curve
yyi = ppval(ppi,xi);        % evaluate integral - dodgy
%xi(2:length(xi))-xi(1:(length(xi)-1))
dxi = xi(2)-xi(1);
yyyi = zeros(size(xi));
yyyi(1)=yi(1)*dxi;
for int = 2:length(xi)      % evaluate integral - dodgier
    yyyi(int) = yyyi(int-1)+yi(int)*dxi;
end
plot(x,y,'o',xi,yi,xi,yyi,'--',xi,yyyi,':') % plot results
title('Figure 20.3: Spline Integration')
toc