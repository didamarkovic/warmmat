% 'parent code: lenshalo_tomo_updating.m'

clear

plotWbool=1; % Plot the lensing weigths
plotPlbool=0; % Plot the matter power spectrum vs l_vals
plotPkbool=0; % Plot the matter power spectrum vs k_vals
plotCbool=0; % Plot the convergence power spectrum

z_vals = 0.00001:0.1:1.00001;
%z_vals = 0.5;%[0.5 1 2]; % CAN ONLY USE THIS IF WANT ONLY Pk!!
nlintype = 'smith2';
nzpars = setnzpars([1 1],{'nbin' 'zs' nlintype});
cospars = setWMAPpars;

consts;


%% LENSING WEIGHT vs RHO_crit_0/SIGMA_crit:

if plotWbool || plotCbool

    % SIGMAcrit

    % Will be converting this to Sarah's units:
    %rho_crit_0 = rho_crit_0/(H0^2) * (H0_Mpc^2) *M_solar /(Mpc^3);

    % Distances and differential comoving volume
    %Dh = c/H0; % Hubble distance
    Dh = c_km/H0_Mpc; % in Mpc

    slbpars = convertpars(cospars);
    chim = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    chimat = chim * Dh;
    [n_z_mat niz] = get_nofz(nzpars,z_vals);

    [chis tmp tmp tmp volm] = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    %clear chis; chis = chim(1,:); % WRONG??!!!!!!
    %chilens = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;

    %chimat = chim * Dh;
    chilens = chis * Dh;    chilens(chilens==0) = min( chilens(chilens~=0) ) * 1e-10;

    chiso = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    chisource = chiso * Dh;
    %chi_ls = chisource - chi; % This can't be right!!!!!
    %chisource = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
    chils = chisource-chilens;

    rho_crit_0 = 3 * H0_Mpc^2  / (8*pi*G_Mpc); % units of km^2 M_sol / Mpc^5
    rho_mean_0 = rho_crit_0 * cospars.omega_m;% M_solar /Mpc^3;

    SIGMAcrit = (c_km^2) .* chisource ./ ( 4*pi*G_Mpc .* chilens.* chils ) ./(1+z_vals); %kg/m^2
    % actually units of this one her are: km^2 M_sol / Mpc^4

    mypar = 3/2 * H0_Mpc^2 / c_km^2 * cospars.omega_m * (1 + z_vals) .* chilens .* chils / chisource;

    if plotWbool
        figure(1)
        plot(z_vals, rho_mean_0./SIGMAcrit,'g','linewidth',5); hold on
        %myfavpar = rho_mean_0./SIGMAcrit*Mpc/cospars.h;
        %plot(z_vals, myfavpar,'g'); hold on

        plot(z_vals,mypar,'mo','linewidth',5); hold on
    end

    %%%%%%%%%%%%
    % Lensing weight:

    %z_vals = 0.000000001:0.1:0.9999999;
    %nzpars.zs = 1;
    %[tmp tmp cospars] = setWMAPpars;
    %consts
    %Dh = c/H0;

    % Find distances to and between all z_vals:
    %slbpars = convertpars(cospars);
    %chimat = D_wa_grid(z_vals,slbpars);
    %chimat = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

    chisource = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

    % Lensing weight
    %nzpars = setnzpars([1 1],{'nbin' 'zs'});
    %[n_z_mat niz] = get_nofz(nzpars,z_vals);
    Wi_chi = lens_weight(cospars,z_vals,chimat/Dh,n_z_mat,nzpars.zs,chisource);
    % Calculate it by hand:
    %c_km = 299792.5;
    %Dh = c_km/(100*cospars.h); % Matching this to have units of h for wi_z
    KO = 3/2 * cospars.omega_m * (Dh)^(-2);
    %KO = 3/2 * cospars.omega_m * (100*cospars.h)^2;

    %chi = chimat(1,:)*Dh; % comov ag diam dists = comov dist
    chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;
    chi_source = chisource*Dh;
    chi_lss = chi_source-chi; % Yikes! Only for omega_k = 0!!!!!!

    a_vals = 1./(1+z_vals);
    %Wi_l = zeros(length(z_vals),1);

    ni_z = n_z_mat(:,1)';

    %for zlint = 1:(length(z_vals)-1)
    for zlint = 1:length(z_vals)


        dWi_l = 0; % clear the integral

        %chi_ls = chi_mat(zlint+1,:);
        % Since first line is obs->gal dist.
        % Actually obs redshift taken to be ~ first lens redshift, so there
        % is no special first line:
        %chi_ls = chimat(zlint,:);
        chi_ls = chi_lss(zlint);

        dWi_l = n_z_mat' .* chi(zlint) .* chi_ls ./ chi_source  ;%.* dchi;
        %Wi_l = chi .* chi_lss ./ chi_source  ;%.* dchi;

        % ni_z = 0 outside bin so can now just sum for integration!
        % no dchi because it is included in the nofz!!! since normalised!!!

        Wi_l(zlint) = sum(dWi_l);

        %or:

        Wi_l(zlint) = sum(ni_z) * chi(zlint).* chi_lss(zlint) ./ chi_source;


    end
    %Wi_l(1) = 0; % Make sure to delete what we add in line 34 above.
    %Wi_l(zlint+1) = 0;
    wiofz = KO * Wi_l ./ a_vals;

    if plotWbool
        figure(1)
        plot(z_vals,Wi_chi*Dh*cospars.h^2,'y--','linewidth',1.1); hold on
        %plot(z_vals,Wi_chi,'b.'); hold on
        plot(z_vals,wiofz,'bx','linewidth',1.1); hold on
        xlabel('redshift','fontsize',15); ylabel('Wi(\chi)*D_h*h^2','fontsize',15)
        title('Lensing weight','fontsize',15)
        axis([z_vals(1) z_vals(length(z_vals)) 0 max(max(mypar),max(wiofz))+max(mypar)/10])

        figure
        plot(z_vals,Wi_chi*Dh*cospars.h^2./(rho_mean_0./SIGMAcrit'))
        title('ratio of the two methods for getting lensing weight')
        xlabel('redshift','fontsize',15); ylabel('ratio','fontsize',15)
    end

end
