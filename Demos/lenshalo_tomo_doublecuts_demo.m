% 'lens_wdm_double_demo'
%
% 26/08/08 KM, modified from cls_cooray_doublecuts: 25/09/08

clear

cospars = setpars([0.3 0.05 0.65 0.65],...
                  {'omega_wdm'; 'omega_b'; 'omega_de'; 'h'});
zs = 1; dz = 0.03; z_vals = 0.0001:dz:(zs-0.0000001);
m_vals = [200 1000 1000000];
nzpars = setnzpars([1 zs],{'nbin'; 'zs'});

l_vals = logspace(1,4,40); pf = l_vals.*(1+l_vals)/2/pi;

M_vals = logspace(0,16,50);
k_vals = logspace(-4,4,2000);
Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
pk = pkf(Gamma,cospars.sigma8,k_vals);
for int = 1:length(m_vals)
    cospars.m_wdm = m_vals(int);
    pk_wdm = wdm_transfn_pars(pk,k_vals,cospars);
    %Cls_halo_wdm(int,:) = halowlps_vielfit(k_vals,pk,m_vals(int),z_vals,...
    %                                            zs,dz,l_vals,M_vals);
    Cls_halo_wdm(int,:) = lenshalo_tomo(cospars,z_vals,l_vals,k_vals,...
                                                    pk_wdm,nzpars,M_vals);
    plotfactor(int,:) = pf;
end

%m_vals(m_vals==0) = 0.001;
[Ms Rvirs kfs] = freestreaming_comov(cospars,m_vals);

for int = 1:length(m_vals)
    M_vals = logspace(log10(Ms(int)),16,50);
    Cls_halo(int,:) = lenshalo_tomo(cospars,z_vals,l_vals,k_vals,pk,nzpars,M_vals);
end

figure
lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
p1 = loglog(l_vals,plotfactor.*Cls_halo,'k'); hold on
p2 = loglog(l_vals,plotfactor.*Cls_halo_wdm,'c--'); 
axis([l_vals(1) max(l_vals) 1e-7 1e-3])
title('Weak Lensing Power Spectrum - cutting M_m_i_n and k_f_s');
xlabel('multipole l','fontsize',fs); 
ylabel('l(l+1)C_l/2\pi','fontsize',fs);
set([p1 p2],'linewidth',lw);
%saveas(gcf,'Cls_doublecut_wdm...','bmp')