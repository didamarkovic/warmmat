% 'matter_power_spectrum_demo'
%
% Making a nice plot for the matter power spectum.
%
% version 1.1
% This is for my first talk at the Excellence Cluster.
% 26.11.09 KMarkovic
% version 1.2
% added possibility of using halo model
% 13.04.2010 KM

clear

tic

cospars = setEUCLIDpars;
k_vals = logspace(log10(0.0032),2,1000); %max(max(k_vals),100); min(min(k_vals),0.0032)

%method = 'smith2'; figure(1); clf; %figure('Position',[1 400 1000 400]); subplot(1,2,1)
method = 'readsmith'; figure(2); clf; %subplot(1,2,2)
titlestring = 'WDM halo model';

%% Now WDM:
cospars.omega_wdm = cospars.omega_dm;


% m = 250 eV
cospars.m_wdm = 250;

[pk_nlin pk_lin] = pk_nlin_get(cospars,k_vals,0,method);
%Delta_sq_nlin = k_vals.^3 .* pk_nlin /2/pi^2;
%Delta_sq_lin = k_vals.^3 .* pk_lin /2/pi^2;

%c = loglog(k_vals,Delta_sq_nlin,'m-','linewidth',1.1); hold on
%loglog(k_vals,Delta_sq_lin,'m--','linewidth',1.1);
c = loglog(k_vals,pk_nlin,'m-','linewidth',1.1); hold on
loglog(k_vals,pk_lin,'m--','linewidth',1.1);

% m = 500 eV
cospars.m_wdm = 500;

[pk_nlin pk_lin] = pk_nlin_get(cospars,k_vals,0,method);
%Delta_sq_nlin = k_vals.^3 .* pk_nlin /2/pi^2;
%Delta_sq_lin = k_vals.^3 .* pk_lin /2/pi^2;

%d = loglog(k_vals,Delta_sq_nlin,'r-','linewidth',1.1); hold on
%loglog(k_vals,Delta_sq_lin,'r--','linewidth',1.1);
d = loglog(k_vals,pk_nlin,'r-','linewidth',1.1); hold on
loglog(k_vals,pk_lin,'r--','linewidth',1.1);

% m = 1 keV
cospars.m_wdm = 1000;

[pk_nlin pk_lin] = pk_nlin_get(cospars,k_vals,0,method);
%Delta_sq_nlin = k_vals.^3 .* pk_nlin /2/pi^2;
%Delta_sq_lin = k_vals.^3 .* pk_lin /2/pi^2;

%e = loglog(k_vals,Delta_sq_nlin,'g-','linewidth',1.1); hold on
%loglog(k_vals,Delta_sq_lin,'g--','linewidth',1.1);
e = loglog(k_vals,pk_nlin,'g-','linewidth',1.1); hold on
loglog(k_vals,pk_lin,'g--','linewidth',1.1);


%% Now CDM:
%clear  cospars
cospars.m_wdm = 0;
%cospars = setWMAP7pars;
%k_vals = logspace(-4,4,1000);

[pk_nlin pk_lin] = pk_nlin_get(cospars,k_vals,0,method);
%Delta_sq_nlin = k_vals.^3 .* pk_nlin /2/pi^2;
%Delta_sq_lin = k_vals.^3 .* pk_lin /2/pi^2;

%a = loglog(k_vals,Delta_sq_nlin,'k-','linewidth',1.1); hold on
%b = loglog(k_vals,Delta_sq_lin,'k--','linewidth',1.1);
a = loglog(k_vals,pk_nlin,'k-','linewidth',1.1); hold on
b = loglog(k_vals,pk_lin,'k--','linewidth',1.1);
%set([a b],'fontsize',20)
%legend([a b],'non-linear','linear',2)
%title('The matter power spectrum at z = 0')
xlabel('k, wavenumber [h Mpc ^{-1}]','FontSize',12); %ylabel('\Delta^2(k)')
ylabel('P(k) [Mpc^3 h^{-3}]','FontSize',12);
%title('Matter power spectra with WDM','FontSize',12)
legend([c d e a],'250 eV', '500 eV', '1 keV', 'CDM')
axis([k_vals(1) 1e2 1e-1 1e5])
title(titlestring)
%saveas(gcf,'Pknonlin','pdf')

%set(gca,'LineWidth',1.5,'FontSize',12)

toc