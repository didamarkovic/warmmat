% plotsfortalk.m
%
% Modified for NAM2012.
% 28.03.2012

clear;

clear
cospars = setEUCLIDpars; %cospars.omega_wdm
k_vals = logspace(-2,4,1000);
cols = 'kbgr';
m_wdm = [0 4000 1000 500];
[Mfs Rfs kfs] = freestreaming_comov(cospars,m_wdm);
[gstar Tx_Tnu m_nu N_nu Tx] = degoffree(m_wdm,1,cospars.omega_dm,cospars.h);



%% Non-linear power spectra

model =  'smith2'; %'readsmith'; %'halo';

m_wdm = [0 1000 250];

redshifts = [1.1];

k_vals = logspace(-2,2,100);


zz = 1

    figure(1)
    clf

    pl = 0; pn = 0;
    for ii = 1:length(m_wdm)
        cospars.m_wdm = m_wdm(ii);

        [pk_nlin(ii,:) pk_lin(ii,:)] = pk_nlin_get(cospars,k_vals,redshifts(zz),model);

        pl(ii) = loglog(k_vals,pk_lin(ii,:),[cols(ii) '.'],'linewidth',1.2); hold on
        pn(ii) = loglog(k_vals,pk_nlin(ii,:),cols(ii),'linewidth',1.2); hold on
    end
    pl(1) = loglog(k_vals,pk_lin(1,:),'k.','linewidth',1.2); hold on
    pn(1) = loglog(k_vals,pk_nlin(1,:),'k','linewidth',1.2); hold on

    axis([min(k_vals) max(k_vals) 1e-5 1e5])
    xlabel('k [h/Mpc]'); ylabel('P(k)')
    legend(pn,'CDM',['m_w_d_m =' num2str(m_wdm(2)/1000) ' keV'],['m_w_d_m =' num2str(m_wdm(3)/1000) ' keV'],3)
    title(['z = ' num2str(redshifts(zz))])

    figure(2)
    clf

    for ii = 1:length(m_wdm)
        rl(ii) = semilogx(k_vals,pk_lin(ii,:)./pk_lin(1,:),[cols(ii) '.']); hold on
        rn(ii) = semilogx(k_vals,pk_nlin(ii,:)./pk_nlin(1,:),cols(ii),'linewidth',1.2);
    end
    axis([min(k_vals) max(k_vals) 0.9 1.04])
    legend(rn,'CDM',['m_w_d_m =' num2str(m_wdm(2)/1000) ' keV'],['m_w_d_m =' num2str(m_wdm(3)/1000) ' keV'],3)
    xlabel('k [h/Mpc]'); ylabel('ratio'); title(['z = ' num2str(redshifts(zz))]); %ylabel('P_w_d_m(k)/P_c_d_m(k)')
