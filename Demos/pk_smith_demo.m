% 'pk_smith_demo'
%
% 06/08/08 KM
% Sarah says to reproduce Smith et al (0207664v2) plot 14 (lower left)

clear

k_vals = logspace(-2,2,1000);

cospars = setWMAPpars; % LambdaCDM like in Fig.14 of Smith et el

z_vals = [0 0.5 1 2 3];

D_plus = growth_factor(cospars,z_vals);

cols = ['b' 'c' 'g' 'r' 'm' 'y' 'k'];

Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin = pkf(Gamma,cospars.sigma8,k_vals);
%cospars.O_m_wdm = 0.3/1000;
%pk_lin_w = wdm_transfn_pars(pk_lin,k_vals,cospars);

figure
for zint = 1:length(z_vals)

    pk_grown = pk_lin*D_plus(zint).^2;
    
    [pkn delta_qsq(:,zint) delta_hsq(:,zint)] = pk_nlin(k_vals,pk_grown,cospars);
    %cospars_S = convertpars(cospars);
    %pkn = pks_nonlin_halofit(k_vals,pk_lin,z_vals(zint),D_plus(zint),cospars_S);
    %delta_sq_NL = (k_vals.^3).*pkn'/(2*pi^2);
    
    delta_sq_NL = delta_qsq(:,zint)+delta_hsq(:,zint);
    
    loglog(k_vals,delta_sq_NL,cols(zint)); hold on
    
    pk_PD = pk_nonlin(k_vals,pk_grown,1);
    
    deltasq_PD(:,zint) = (k_vals.^3).*pk_PD/(2*pi^2);
     
end
axis([2e-2 1e2 2e-2 3e3])
title('Reproducing Fig.14 from Smith et al. 02076664v2 for LCDM')
xlabel('k/h/Mpc')
ylabel('\Delta�_N_L(k)')
legend('z=0.0','z=0.5','z=1.0','z=2.0','z=3.0',2)
    %loglog(k_vals,delta_qsq,'y:'); 
    %loglog(k_vals,delta_hsq,'y:'); 
    loglog(k_vals,deltasq_PD,'k--'); 
% saveas(gcf,'pknlin_smith_fig14...','bmp')