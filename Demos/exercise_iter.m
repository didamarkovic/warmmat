clear

%bobfn = @(x)exp(1./x);
bobfn = @(x)(exp(1./x/5000)-1)/700;
alice = 0.0001;
bob = bobfn(alice);
index = 0;

while abs(bob-alice)>10^(-5) && index<20
    alice = bob;
    bob = bobfn(alice);

    index = index + 1;
    eve(index,:) = [alice bob];
end

%% Ploting the iteration

a = eve(:,1);
b = eve(:,2);

figure(4)
%plot(b); hold on

% figure
plot(a,a,'k--'); hold on
plot(a,b,'c:'); hold on
plot(a,b,'k.'); hold on
xlabel('alice'); ylabel('bob');

%testy = 0.1:0.1:2;
%plot(testy,bobfn(testy)-1,'y--')

axis([0 index min(b) max(b)])