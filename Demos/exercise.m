% 'file writing exercise'
%
% trying to write data into file whilst cycling through loops
% takes 0.004436 sec without saving
% takes 2.827034 sec with saving
% 11.05.2010 KMarkovic

tic

int = 1:10;
jnt = 1:20;
knt = 1:15;

dlmwrite('exercise.txt','This is an exercise.','')

mymat = zeros(length(int),length(jnt),length(knt));
save exercisemat.mat mymat
for i=int
    for j=jnt
        for k=knt
            
            mymat(i,j,k) = i+j+k;
            
            %save exercisemat.mat mymat%(i,j,k)
                        
        end
        
        dlmwrite('exercise.txt',mymat(i,j,:),'-append','delimiter',' ')

        %pause
        
    end
end

toc

% So, with no saving this takes 0.005 seconds.
% With writing into a text file, it takes 0.16 seconds.
% With saving the entire matrix it takes 2.7 seconds.

% If the operation is terminated with ctrl-c, whilst writing the text file,
% we don't loose what has been written so far.