% lenshalo_wdm_demo
%
% This script plots the Cooray et al power spectrum with varying m_wdm in
% the transfer function from Viel et al and with no mass cut-offs.
%
% lenshalo_wdm_demo; lenshalo_wdmcuts_demo; lens_ps_wdm_demo
%
% version 3
% 26.11.09 KMarkovic

clear

figure

cospars = setWMAPpars;
nzpars = setnzpars(10,{'nbin' 'smith2'}); dz = 0.3; z_vals = 0.0001:dz:5;
bin = 9; % to plot

l_vals = logspace(1,4,20); pf = l_vals.*(1+l_vals)/2/pi;
M_vals = logspace(0,16,50);

%% WDM:
cospars.omega_wdm = cospars.omega_dm;

% m = 400 eV
cospars.m_wdm = 400;
[tmp Clsw1_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals,1,3);    

% m = 1000 eV
cospars.m_wdm = 1000;
[tmp Clsw2_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals,1,3);    

% m = 5 keV
cospars.m_wdm = 5000;
[tmp Clsw3_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals,1,3);    


%% CDM
clear cospars
cospars = setWMAPpars;
[tmp Clsc_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals,1,3);    

%% Ratios:
pr1 = semilogx(l_vals,squeeze((Clsw1_halo(bin,bin,:)-Clsc_halo(bin,bin,:))./Clsc_halo(bin,bin,:)),'m--'); hold on
pr2 = semilogx(l_vals,squeeze((Clsw2_halo(bin,bin,:)-Clsc_halo(bin,bin,:))./Clsc_halo(bin,bin,:)),'r--');
pr3 = semilogx(l_vals,squeeze((Clsw3_halo(bin,bin,:)-Clsc_halo(bin,bin,:))./Clsc_halo(bin,bin,:)),'g--');
pr0 = semilogx(l_vals,zeros(size(l_vals)),'k'); 
xlabel('l, multipole'); ylabel('(C_\kappa^W^D^M-C_\kappa^C^D^M)/C_\kappa^C^D^M')
legend([pr1 pr2 pr3],'400 eV','1 keV','5 keV',3)
lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
set([pr3 pr2 pr1],'linewidth',1.2);
title('Halo model')
axis([l_vals(1) l_vals(length(l_vals)) -0.2 0.07])