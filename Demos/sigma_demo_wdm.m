% Testing sigma(R) for WDM:
%
% 02.12.09 KMarkovic

clear; % clf

M_vals = logspace(0,16,1000);
dlogM = log(M_vals(2))-log(M_vals(1));
dM = dlogM*M_vals;
redshift = 1;
cospars = setWMAPpars;
k_vals = logspace(-5,5,1000);

consts
rho_mean_0 = rho_crit_0_Mpc * cospars.omega_m;
rho_mean = rho_mean_0 * (1 + redshift)^3;

[tmp pk_vals] = pk_nlin_get(cospars,k_vals,redshift,'smith2');
for im=1:length(M_vals)
    R(im) = (M_vals(im)/(rho_mean * 4 * pi/3))^(1/3); % in Mpc/h
    % Find sigma(R)
    sigma(im)=sigmar_frompk(k_vals,pk_vals,R(im));
end
loglog(M_vals,sigma,'k-', 'LineWidth',1.2); hold on
axis([1e0 1e17 2e-1 2e1])

linecol = ['mrygcb'];
for m_wdm = 500:500:3000
    % Linear matter powers  with WDM
    cospars.omega_wdm = cospars.omega_dm; cospars.m_wdm = m_wdm;
    [tmp pk_vals] = pk_nlin_get(cospars,k_vals,redshift,'smith2');

    for im=1:length(M_vals)
        sigma(im)=sigmar_frompk(k_vals,pk_vals,R(im));
    end
    
    loglog(M_vals,sigma,linecol(m_wdm/500), 'LineWidth',1.2);

end
title('\sigma(R) with WDM')
legend('CDM','WDM')
axis([1 max(M_vals) 0.2 10])
xlabel('M[M_s_o_l]'); ylabel('\sigma(R_v_i_r)')