% 'lens_fish_tomo_demo'
%
% need supercomp!
%
% version 2
% 07/08/08 KM

tic

setpath
%setnonlinpath
%sethalopath

clear

[cosparstmp valstmp namstmp] = setWMAPpars;
%cospars_fid.m_wdm = 200; % WDM
%cospars_fid.omega_wdm = cospars_fid.omega_dm; % WDM

cospars_fid = setpars([valstmp 0.25/1e20],{namstmp{:} 'WDM'});
%cospars_fid = setpars([valstmp 0.25/10000],{namstmp{:} 'WDM'});
%cospars_fid = setpars([valstmp 0.25/5000],{namstmp{:} 'WDM'});
%cospars_fid = setpars([valstmp 0.25/1000],{namstmp{:} 'WDM'});
%cospars_fid = setpars([valstmp],{namstmp{:}});

nzpars = setnzpars(3,{'nbin' 'smith2'});

z_vals = 0.0001:0.1:3; %nzpars.zs = 3;
%l_vals = logspace(0,4,30);
%exl = 3;
%exl = log10(2000); 
%exl = log10(5000); 
%exl = 4;
exl = log10(20000); 
%exl = log10(50000); 
%exl = 5;
l_vals = logspace(0,exl,20);
M_vals = logspace(0,16,50);

%fish.names = {'omega_m'; 'sigma8'; 'h'; 'w0'; 'O_m_wdm'};                       
%fish.steps = [0.03 0.09 0.07 0.1 ((0.25/200)-(0.25/1e20))];
fish.names = {'omega_m'; 'h'; 'sigma8'; 'omega_b'; 'w0'; 'ns'; 'O_m_wdm'};                       
fish.steps = [0.03  0.07  0.09 0.01 0.1 0.1 ((0.25/2000)-(0.25/1e20))];
%fish.names = {'omega_m'; 'h'; 'sigma8'; 'ns'; 'O_m_wdm'};                       
%fish.steps = [0.03  0.07  0.09 0.1 ((0.25/1000)-(0.25/1e20))];
                                        
[cls halo xx dcls dhalo] = lens_ps_get(cospars_fid,...
                        nzpars,z_vals,l_vals,nzpars.nbin,fish,M_vals,0);
%[cls halo xx dcls dhalo] = lens_ps_get(cospars_fid,...
%                        nzpars,z_vals,l_vals,nzpars.nbin,fish);
%[cls halo xx] = lens_ps_get(cospars_fid,...
%                        nzpars,z_vals,l_vals,nzpars.nbin,fish,M_vals,0);

[C_mat C_vect lens_vals_v] = covmat(l_vals,cls+xx,nzpars.fsky);

[fmat errs corrs] = fishmat(fish.names,dcls,C_mat);

[Ch_mat Ch_vect lensh_vals_v] = covmat(l_vals,halo+xx,nzpars.fsky);

[fmath errsh corrsh] = fishmat(fish.names,dhalo,Ch_mat);


%save(['run1_' num2str(round(1e5*now))],'*')


%fmat
%fmath

% Read out the values of the fiducial parameters for ellipse plotting:
%for int = 1:length(fish.names)
%    bfp(int) = getfield(cospars_fid,fish.names{int})';
%end

% now plot the error ellipses
%figure
%[hh,errvals] = plotellipses(bfp,pinv(fmath),fish.names,['b' 'r' 'g' 'k' '-']);

%hold on
%[h,errvals] = plotellipses(bfp,pinv(fmat),fish.names,['b' 'r' 'g' 'b' '-']);

%set(h,'linewidth',1.2)

%saveas(gcf,'errorellipses2509','bmp')
%print('-f3', '-dpsc', 'errorellipses1809_normpars...');

% Simple error on omega_b:
%erronOb = 1/sqrt(fmat(2,2));
% C.f. to
%invfish = pinv(fmat);
%erronOb2 = sqrt(invfish(4,4));
% These turn out to be about half the size of Sarah's from plotellipses

%figure
%plotfactor = l_vals.*(1+l_vals)/2/pi;
%lw = 2; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
%p1 = loglog(l_vals,plotfactor.*squeeze(cls)','b'); hold on
%p2 = loglog(l_vals,plotfactor.*squeeze(halo)','g'); 
%axis([l_vals(1)/2 max(l_vals) 1e-9 8e-3])
%title('Weak Lensing Power Spectrum plotting methods');
%xlabel('multipole l','fontsize',fs); 
%ylabel('l(l+1)C_l/2\pi','fontsize',fs);
%set([p1 p2],'linewidth',lw);
%legend([p1 p2], 'Takada & Jain','Cooray et al',2)

toc