% run lens_fish_demo_chifish many times for many lmax
%
% 25.06.2010 KMarkovic
% 03.02.2011 KM

lmaxs = [2.699 2.3979];

errs = zeros(size(lmaxs));
filestring = cell(size(lmaxs));
for int = 1:length(lmaxs)
    %[errs(int) filestring(int)] = lens_fish_demo_fishinterp(lmaxs(int));
    filestring{int} = lens_fish_demo_fishinterp(lmaxs(int));
end

%p = plot(lmaxs,errs,'k'); hold on
%xlabel('l_m_a_x'); ylabel('\sigma_w_d_m');
%set(p,'LineWidth',1.5)