% 'lenshalo_coorayfig'
%
% This tries to again reproduce fig.2b in Cooray,Hu&Miralda-Escude
% 25/08/08 KM, modified from halowlps_vielfit_coorayfig: 25/09/08

clear
tic

mmaxs = 11:15;

lvals = logspace(0,4,100);
dz = 0.1; zs = 1; zvals = 0.00001:dz:(zs-0.00000001);

cospars = setpars([0.65 0.3],{'omega_de'; 'omega_dm'});
nzpars = setnzpars([1 zs],{'nbin'; 'zs'});

k_vals = logspace(-4,4,200);

Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
pk = pkf(Gamma,cospars.sigma8,k_vals);

for int = 1:length(mmaxs)
    Mvals = logspace(0,mmaxs(int),50); 
    [Cls(int,:) pf(int,:) tmp Cl_P(int,:) Cl_C(int,:)] = ...
            lenshalo_tomo(cospars,zvals,lvals,k_vals,pk,nzpars,Mvals);
end

figure
lw = 2; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
p1 = loglog(lvals,squeeze(pf.*Cls),'k'); hold on
p2 = loglog(lvals,squeeze(pf.*Cl_P),'b--'); 
axis([lvals(1) lvals(length(lvals)) 1e-9 1e-3])
title('Weak Lensing Power Spectrum with different M_m_a_x & z_s = 1 ???');
xlabel('multipole l','fontsize',fs); 
ylabel('l(l+1)C_l/2\pi','fontsize',fs);
set([p1 p2],'linewidth',lw);
%saveas(gcf,'Coorayfig_Cls_25-08...','bmp')
toc