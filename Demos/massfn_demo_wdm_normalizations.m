% massfn_wdm_normalizations_demo
%
% Testing script to plot the mass functions for WDM universes with WDM  &
% CDM normalizations!
%
% 27.11.09 KMarkovic

clear

M_vals = logspace(0,16,1000);
dlogM = log(M_vals(2))-log(M_vals(1));
dM = dlogM*M_vals;
redshift = 0;
cospars = setWMAPpars;
k_vals = logspace(-5,5,1000);

% Linear matter powers pectrum
%Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
%pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals);

% Background density
consts
rho_mean_0 = rho_crit_0_Mpc * cospars.omega_m;
rho_mean = rho_mean_0 * (1 + redshift)^3;

% Mass functions

% CDM:
[tmp pk_lin] = pk_nlin_get(cospars,k_vals,0,'smith2'); % Pk today!
dndm = massfns_slb(M_vals*cospars.h,redshift, cospars.omega_b,cospars.omega_m,...
    cospars.omega_de,cospars.w0,cospars.h,cospars.sigma8,k_vals,pk_lin); 
%norm = 1;
norm_cdm = sum( dndm' .* dM .* M_vals / rho_mean );   
dndm_cdm = dndm / norm_cdm;

% WDM density
cospars.omega_wdm = cospars.omega_dm;
cospars.m_wdm = 5000;

%Mmin = freestreaming_comov(cospars,cospars.m_wdm);

[tmp pk_lin] = pk_nlin_get(cospars,k_vals,0,'smith2');
dndm = massfns_slb(M_vals*cospars.h,redshift, cospars.omega_b,cospars.omega_m,...
    cospars.omega_de,cospars.w0,cospars.h,cospars.sigma8,k_vals,pk_lin); 
%dndm_1 = dndm(1);
%dndm(M_vals<Mmin) = 0;
%dndm(1) = dndm_1;
norm_wdm = sum( dndm' .* dM .* M_vals / rho_mean );   
dndm_wdm = dndm / norm_wdm;

%figure; % clf
%loglog(M_vals,dndm_wdm,'r-',M_vals,dndm_cdm,'k','linewidth',1.5); hold on
semilogx(M_vals,(dndm_wdm-dndm_cdm)./dndm_cdm,'g','linewidth',1.5); hold on
xlabel('M[M_o]'); ylabel('(n_w_d_m-n_c_d_m)/n_c_d_m')
%title('Sheth-Tormen mass functions at z = 0 for CDM & WDM')
%axis([1e6 2e16 1e-30 max(dndm_cdm)])
axis([1e0 1e16 -1.1 1])
legend('1 keV mass','5 keV mass','CDM',2)
plot(M_vals,zeros(size(M_vals)),'k:')