% 'lens_methods_test.m'
%
% Yet another testing script for comparing the halo vs the T&J method.
% Another good one is cls_methods.m!
%
% version 1.75
% This version tries to test the source distribution in the lensing weight.
% 24.11.09 KMarkovic


% Here we are testing the lensing weight part:

clear

z_vals = 0.000000001:0.1:0.9999999;
%nzpars.zs = 1;
[tmp tmp cospars] = setWMAPpars;

consts;

nzpars = setnzpars(1,{'nbin'});
[n_z_mat niz] = get_nofz(nzpars,z_vals);

% Will be converting this to Sarah's units:
%rho_crit_0 = rho_crit_0/(H0^2) * (H0_Mpc^2) *M_solar /(Mpc^3);

% Distances and differential comoving volume
%Dh = c/H0; % Hubble distance
Dh = c_km/H0_Mpc; % in Mpc

%slbpars = convertpars(cospars);
%chim = D_wa_grid(z_vals,slbpars);
chim = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

[chis tmp tmp tmp volm] = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
%clear chis; chis = chim(1,:); % WRONG??!!!!!!
%chilens = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;

chimat = chim * Dh;
chi = chis * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;

%chiso = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
%chisource = chiso * Dh;
%chi_ls = chisource - chi; % This can't be right!!!!!
%chisource = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
%chils = chisource-chilens;

rho_crit_0 = 3 * H0_Mpc^2  / (8*pi*G_Mpc); % units of km^2 M_sol / Mpc^5
rho_mean_0 = rho_crit_0 * cospars.omega_m;% M_solar /Mpc^3 - NO! see above!

clear tmp
for zlint = 1:length(z_vals)
    SIGMAcrit(zlint,:) = (c_km^2) .* chi ./ ( 4*pi*G_Mpc .* chi(zlint).* chimat(zlint,:)) ./(1+z_vals(zlint)); %kg/m^2
    % actually units of this one her are: km^2 M_sol / Mpc^4
    %dmypar1 = rho_mean_0./dSIGMAcrit;
    for int = 1:nzpars.nbin
        %mypar1(zlint,int) = sum(n_z_mat(:,int) .* dmypar1');
        tmp(zlint,int) = sum(n_z_mat(:,int) ./SIGMAcrit(zlint,:)');
    end
    mypar1(zlint,:) = rho_mean_0.*tmp(zlint,:);
end

figure
plot(z_vals, mypar1,'g'); hold on
%SIGMAcrit = SIGMAcrit/c_Mpc^2*c_km^2;
%myfavpar = rho_mean_0./SIGMAcrit*Mpc/cospars.h;
%plot(z_vals, myfavpar,'g'); hold on

prefactor = 3/2 * H0_Mpc^2 / c_km^2 * cospars.omega_m * (1 + z_vals) ;

for zlint = 1:length(z_vals)
    dmypar =  prefactor(zlint) .* chi(zlint) .* chimat(zlint,:) ./ chi;
    for int = 1:nzpars.nbin
        mypar2(zlint,int) = sum(n_z_mat(:,int) .* dmypar');
    end
end
plot(z_vals,mypar2,'g.'); hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find distances to and between all z_vals:
%slbpars = convertpars(cospars);
%chimat = D_wa_grid(z_vals,slbpars);
%chimat = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

%chisource = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

% Lensing weight
%Wi_chi = lens_weight(cospars,z_vals,chimat,n_z_mat,nzpars.zs,chisource);
Wi_chi = lens_weight(cospars,z_vals,chimat/Dh,n_z_mat);
% Calculate it by hand:
%Dh = c_km/(100*cospars.h); % Matching this to have units of h for wi_z
KO = 3/2 * cospars.omega_m * (Dh)^(-2);
%KO = 3/2 * cospars.omega_m * (100*cospars.h)^2;

%chi = chimat(1,:)*Dh; % comov ag diam dists = comov dist
%chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
%chi(chi==0) = min( chi(chi~=0) ) * 1e-10;
%chi_source = chisource*Dh;
%chi_lss = chi_source-chi; % Yikes! Only for omega_k = 0!!!!!!

a_vals = 1./(1+z_vals);
%Wi_l = zeros(length(z_vals),1);

%chi = chimat(1,:); % THIS IS IT!!!

%for zlint = 1:(length(z_vals)-1)
for zlint = 1:length(z_vals)


    dWi_l = 0; % clear the integral

    %chi_ls = chi_mat(zlint+1,:);
    % Since first line is obs->gal dist.
    % Actually obs redshift taken to be ~ first lens redshift, so there
    % is no special first line:
    chi_ls = chimat(zlint,:);
    %chi_ls = chi_lss(zlint);

    for int = 1:nzpars.nbin
        dWi_l = n_z_mat(:,int)' .* chi(zlint) .* chimat(zlint,:) ./ chi  ;%.* dchi;
        %Wi_l = chi .* chi_lss ./ chi_source  ;%.* dchi;

        % ni_z = 0 outside bin so can now just sum for integration!
        % no dchi because it is included in the nofz!!! since normalised!!!

        Wi_l(zlint) = sum(dWi_l);
    end

    %or:

   %Wi_l(zlint) = sum(ni_z) * chi(zlint).* chi_lss(zlint) ./ chi_source;


end
%Wi_l(1) = 0; % Make sure to delete what we add in line 34 above.
%Wi_l(zlint+1) = 0;
wiofz = KO * Wi_l ./ a_vals;

plot(z_vals,Wi_chi*Dh*cospars.h^2,'bx'); hold on
%plot(z_vals,Wi_chi,'b.'); hold on
plot(z_vals,wiofz,'--m'); hold on

%figure
%plot(z_vals,Wi_chi*Dh*cospars.h^2./(rho_mean_0./SIGMAcrit'))