% lenshalo_wdm_demo
%
% This script plots the Cooray et al power spectrum with varying m_wdm with 
% mass cut-offs.
%
% version 3
% 27.11.09 KMarkovic

clear

figure

cospars = setWMAPpars;
nzpars = setnzpars(10,{'nbin' 'smith2'}); dz = 0.3; z_vals = 0.0001:dz:5;
bin = 9; % to plot

l_vals = logspace(1,4,20); pf = l_vals.*(1+l_vals)/2/pi;
M_vals = logspace(0,16,50);


%% WDM:
cospars.omega_wdm = cospars.omega_dm;
normbool = 1;

% m = 400 eV
m_wdm = 400;
Mmin = freestreaming_comov(cospars,m_wdm);
M_vals_wdm = logspace(log10(Mmin),16,50);
[tmp Clsw1_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals_wdm,1,normbool);    

% m = 500 eV
m_wdm = 500;
Mmin = freestreaming_comov(cospars,m_wdm);
M_vals_wdm = logspace(log10(Mmin),16,50);
[tmp Clsw2_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals_wdm,1,normbool);    

% m = 1 keV
m_wdm = 1000;
Mmin = freestreaming_comov(cospars,m_wdm);
M_vals_wdm = logspace(log10(Mmin),16,50);
[tmp Clsw3_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals_wdm,1,normbool);    


%% CDM
[tmp Clsc_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals,1,0);   

%% Ratios:
pr1 = semilogx(l_vals,squeeze((Clsw1_halo(bin,bin,:)-Clsc_halo(bin,bin,:))./Clsc_halo(bin,bin,:)),'m'); hold on
pr2 = semilogx(l_vals,squeeze((Clsw2_halo(bin,bin,:)-Clsc_halo(bin,bin,:))./Clsc_halo(bin,bin,:)),'r');
pr3 = semilogx(l_vals,squeeze((Clsw3_halo(bin,bin,:)-Clsc_halo(bin,bin,:))./Clsc_halo(bin,bin,:)),'g');
pr0 = semilogx(l_vals,zeros(size(l_vals)),'k'); 
xlabel('l, multipole'); ylabel('(C_\kappa^W^D^M-C_\kappa^C^D^M)/C_\kappa^C^D^M')
legend([pr1 pr2 pr3],'400 eV','500 eV','1 keV',3)
lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
set([pr3 pr2 pr1],'linewidth',1.2);
title('cut below free-streaming mass')
axis([l_vals(1) l_vals(length(l_vals)) -0.2 0.07])