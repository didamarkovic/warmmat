%function pknlin = pks_nonlin_readsmith(k_vals, z_vals, cospars)% P(k) [zint, kint]

% PKS_NONLIN_READSMITH reads files provided by Robert Smith containing
%  non-linear matter power spectra calculated using his WDM halo model
%  approach. The non-linear power spectra are stored in matrices.
%  The structure of Robert's files is:
%  a line with 2 numbers: [number of redshifts (50)] [number of kvals (100)]
%  a matrix of nonlinear factors in 50 rows (z) and with 100 columns (k)
%  then a matrix made of 2 column vectors with 50 entries each: [a factors] [growth fn.]
%  then a matrix made of 2 column vectors with 100 entries each: [kvals] [powref]
%
%  version 1
% script for playing around
% 09.12.2010 KMarkovic

clear; clc
k_vals = logspace(log10(0.0032),2,1000);
z_vals = 0:0.05:3;

%% Which WDM model:
iWDM = 5;
filename = ['DATA/Pow_rk_aa.' num2str(iWDM) '.dat.KM'];

% first read the file
nkvals = 100;
navals = 50;
[data header] = myread(filename,1,nkvals,' ');

% then check the header line for the size of the data
bool=0;
header = header{1};
header = [str2num(header{11}) str2num(header{20})];
if sum([navals nkvals]~=header) && bool
    warning('Yikes!!! Dimensions for Roberts file are wrong!!')
end

% now get the first matrix - nonlinear factors
[row col] = find(isnan(data)); % find the filling

logPowRatio = data(1:(row(1)-1),1:nkvals);

if sum(size(logPowRatio)~=header) && bool
    warning('Yikes!!! Dimensions for Roberts file are wrong!!')
end

% now get a factors and the growth function (at 50 different z)
logavals = data(row(1):(row(1)+navals-1),1);
growthf = data(row(1):(row(1)+navals-1),2);

% now get the kvals and the linear power spectrum
logkvals = data((row(1)+navals):max(row),1);  % max(kvals)
powref = data((row(1)+navals):max(row),2);

avals = 10.^logavals;
zvals = 1./avals - 1;
kvals = 10.^logkvals;
PowRatio = 10.^logPowRatio;

if iWDM==0
    PowRatioCDM = PowRatio;
    powrefCDM = powref;
    growthfCDM = growthf;
end

%% plotting cell
plotind = 2;

figure(1);
if plotind==50
    clf
    p1=loglog(kvals,powref*growthf(plotind).^2,'b:'); hold on
    p2=loglog(kvals,PowRatio(plotind,:),'r:'); hold on % size(powref) size(PowRatio(:,1))
    p3=loglog(kvals,PowRatio(plotind,:)'.*powref.*growthf(plotind).^2,'k'); hold on % size(powref) size(PowRatio(:,1))
    legend([p1 p2 p3],'reference ps','nonlin ratio','final nonlinear ps')
else
    p0=loglog(kvals,PowRatio(plotind,:)'.*powref.*growthf(plotind).^2,'k--'); hold on % size(powref) size(PowRatio(:,1))
    legend([p0 p1 p2 p3],'z = 2.1,1.3,0.8,0.3','reference ps','nonlin ratio','final nonlinear ps today')
end
xlabel('k, wavenumber'); ylabel('P(k)')
axis([min(kvals) max(kvals) 0.01 10^5])
%zvals(plotind)

%% now plot mine
cospars = setSMITHpars;
[pk_vals_km pk_grown] = pk_nlin_get(cospars,kvals',zvals(plotind),'smith');
loglog(kvals,pk_vals_km,'g--')
loglog(kvals,pk_grown,'k:')

%% plot the ratio
plotind = 50;
 
pspectrum=PowRatio(plotind,:)'.*powref.*growthf(plotind).^2;
pspectrumCDM=PowRatioCDM(plotind,:)'.*powrefCDM.*growthfCDM(plotind).^2;
C2Wratio = pspectrum./pspectrumCDM;

figure(2); clf
semilogx(kvals,powref./powrefCDM*(growthf(plotind)/growthfCDM(plotind)).^2,'b--'); hold on
semilogx(kvals,PowRatio(plotind,:)./PowRatioCDM(plotind,:),'r:'); hold on % size(powref) size(PowRatio(:,1))
semilogx(kvals,C2Wratio,'k'); hold on % size(powref) size(PowRatio(:,1))
legend('reference ps','nonlin ratio','final nonlinear ps',3)
xlabel('k, wavenumber'); ylabel('P_W_D_M(k) / P_C_D_M(k)')
title(['iWDM = ' num2str(iWDM) ' and z = ' num2str(zvals(plotind))])
%title(['iWDM = ' num2str(iWDM)])
axis([min(kvals) max(kvals) min(C2Wratio) max(PowRatio(plotind,:)./PowRatioCDM(plotind,:))])

%% plot growth factors against a
%clc
%figure(2); clf
%plot(zvals,growthf,'r')
%xlabel('z'); ylabel('D(z)')

%% still need to do the interpolation... this doesn't work at all...

%logratiotmp = interp1(logkvals,logPowRatio',log(k_vals),'spline'); %size(logPowRatio) length(logkvals)
%logratio = interp1(avals,logratiotmp',1./(1+z_vals)','spline');
%ratio = 10.^logratio;

%logreference = interp1(logkvals,log10(powref),log(k_vals),'spline');
%reference = 10.^logreference; 

%growth = interp1(avals,growthf,1./(1+z_vals),'spline');

%for zint = 1:length(z_vals)
%    pknlin(zint,:) = ratio(zint,:).*reference.*growth(zint)^2; % size(pknlin) size(k_vals) size(z_vals)
%end

%% interpolate the ratio

C2Wratio_km = interp1(logkvals,C2Wratio,log10(k_vals));
semilogx(k_vals,C2Wratio_km,'g--')

%% plotting cell
ind2 = find(min(abs(avals(plotind)-1./(1+z_vals)))==abs(avals(plotind)-1./(1+z_vals))); 
% z_vals(ind2) zvals(plotind)
figure(1); %clf
loglog(k_vals,reference*growth(plotind).^2,'b');
loglog(k_vals,10.^logratio(plotind,:),'r')
loglog(k_vals,pknlin(plotind,:),'k','LineWidth',1.2); hold on % size(powref) size(PowRatio(:,1))