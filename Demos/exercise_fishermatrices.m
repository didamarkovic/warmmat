% checking fisher matrices from run on Hal
%
% 25.05.2010 KMarkovic

clear

%clc
nosig = 3;

parsfact = 10^5;

%% My matrices:


%% 1) step = 0.0003

steps(1) = 0.0003;

%
halomat = [4.9801e+07 -3.3887e+07 -1.9108e+07 -64.888 -1.4219e+08;...
-3.3887e+07 4.8121e+07 2.4106e+07 95.13 2.0023e+08;...
-1.9108e+07 2.4106e+07 1.2447e+07 48.261 1.021e+08;...
-64.888 95.13 48.261 0.00019222 403.73;...
-1.4219e+08 2.0023e+08 1.021e+08 403.73 8.4965e+08];
 
halomat(4,:) = halomat(4,:)*parsfact;
halomat(:,4) = halomat(:,4)*parsfact;
invhalo = inv(halomat); 
errsh(1) = invhalo(1,1); 

%
smithmat = [5.6736e+05 -4.0084e+06 -2.5705e+06 -8.4473 -1.8782e+07;...
-4.0084e+06 5.0238e+07 2.6468e+07 104.52 2.2095e+08;...
-2.5705e+06 2.6468e+07 1.5019e+07 55.822 1.1999e+08;...
-8.4473 104.52 55.822 0.00021982 465.17;...
-1.8782e+07 2.2095e+08 1.1999e+08 465.17 9.8853e+08];

smithmat(4,:) = smithmat(4,:)*parsfact;
smithmat(:,4) = smithmat(:,4)*parsfact;
invsmith = inv(smithmat);
errs(1) = invsmith(1,1);


%% 2) step = 0.0005

steps(2) = 0.0005;

%
halomat = [6.1506e+07 -4.1605e+07 -2.284e+07 -79.361 -1.7273e+08;...
-4.1605e+07 4.8121e+07 2.4106e+07 95.13 2.0023e+08;...
-2.284e+07 2.4106e+07 1.2447e+07 48.261 1.021e+08;...
-79.361 95.13 48.261 0.00019222 403.73;...
-1.7273e+08 2.0023e+08 1.021e+08 403.73 8.4965e+08];
 
halomat(4,:) = halomat(4,:)*parsfact;
halomat(:,4) = halomat(:,4)*parsfact;
invhalo = inv(halomat); 
errsh(2) = invhalo(1,1); 

%
smithmat = [2.5886e+06 -8.5645e+06 -5.4919e+06 -18.049 -4.013e+07;...
-8.5645e+06 5.0238e+07 2.6468e+07 104.52 2.2095e+08;...
-5.4919e+06 2.6468e+07 1.5019e+07 55.822 1.1999e+08;...
-18.049 104.52 55.822 0.00021982 465.17;...
-4.013e+07 2.2095e+08 1.1999e+08 465.17 9.8853e+08];
 
smithmat(4,:) = smithmat(4,:)*parsfact;
smithmat(:,4) = smithmat(:,4)*parsfact;
invsmith = inv(smithmat);
errs(2) = invsmith(1,1);


%% 3) step = 0.00055

steps(3) = 0.00055;

%
halomat = [6.7006e+07 -4.3927e+07 -2.4031e+07 -83.756 -1.8213e+08;...
-4.3927e+07 4.8121e+07 2.4106e+07 95.13 2.0023e+08;...
-2.4031e+07 2.4106e+07 1.2447e+07 48.261 1.021e+08;...
-83.756 95.13 48.261 0.00019222 403.73;...
-1.8213e+08 2.0023e+08 1.021e+08 403.73 8.4965e+08];
 
halomat(4,:) = halomat(4,:)*parsfact;
halomat(:,4) = halomat(:,4)*parsfact;
invhalo = inv(halomat); 
errsh(3) = invhalo(1,1); 

errs(3) = errs(2)*0.999;


%% 4) step = 0.00085

steps(4) = 0.00085;

%
halomat = [1.1523e+08 -5.9989e+07 -3.2438e+07 -114.62 -2.4824e+08;...
-5.9989e+07 4.8121e+07 2.4106e+07 95.13 2.0023e+08;...
-3.2438e+07 2.4106e+07 1.2447e+07 48.261 1.021e+08;...
-114.62 95.13 48.261 0.00019222 403.73;...
-2.4824e+08 2.0023e+08 1.021e+08 403.73 8.4965e+08];
 
halomat(4,:) = halomat(4,:)*parsfact;
halomat(:,4) = halomat(:,4)*parsfact;
invhalo = inv(halomat); 
errsh(4) = invhalo(1,1); 

errs(4) = errs(3)*0.999;


%% 5) step = 0.001

steps(5) = 0.001;

%
halomat = [1.5496e+08 -6.9758e+07 -3.758e+07 -133.98 -2.8939e+08;...
-6.9758e+07 4.8121e+07 2.4106e+07 95.13 2.0023e+08;...
-3.758e+07 2.4106e+07 1.2447e+07 48.261 1.021e+08;...
-133.98 95.13 48.261 0.00019222 403.73;...
-2.8939e+08 2.0023e+08 1.021e+08 403.73 8.4965e+08];
 
halomat(4,:) = halomat(4,:)*parsfact;
halomat(:,4) = halomat(:,4)*parsfact;
invhalo = inv(halomat); 
errsh(5) = invhalo(1,1); 

errs(5) = errs(4)*0.999;


%% 6) step = 0.0011

steps(6) = 0.0011;

%
halomat = [1.933e+08 -7.6999e+07 -4.1394e+07 -148.59 -3.2027e+08;...
-7.6999e+07 4.8121e+07 2.4106e+07 95.13 2.0023e+08;...
-4.1394e+07 2.4106e+07 1.2447e+07 48.261 1.021e+08;...
-148.59 95.13 48.261 0.00019222 403.73;...
-3.2027e+08 2.0023e+08 1.021e+08 403.73 8.4965e+08];

halomat(4,:) = halomat(4,:)*parsfact;
halomat(:,4) = halomat(:,4)*parsfact;
invhalo = inv(halomat); 
errsh(6) = invhalo(1,1); 

% 
smithmat = [2.6796e+07 -2.7597e+07 -1.7691e+07 -58.169 -1.2931e+08;...
-2.7597e+07 5.0238e+07 2.6468e+07 104.52 2.2095e+08;...
-1.7691e+07 2.6468e+07 1.5019e+07 55.822 1.1999e+08;...
-58.169 104.52 55.822 0.00021982 465.17;...
-1.2931e+08 2.2095e+08 1.1999e+08 465.17 9.8853e+08];

smithmat(4,:) = smithmat(4,:)*parsfact;
smithmat(:,4) = smithmat(:,4)*parsfact;
invsmith = inv(smithmat);
errs(6) = invsmith(1,1);


%% now calculations and plots

errsh = sqrt(abs(errsh));
errs = sqrt(abs(errs));

1./errs
1./errsh

%plot(steps,errs,'k'); hold on
%plot(steps,errsh,'b--')
%axis([0 max(steps) 0 max(errs)])

dohalo = 1; dosmith = 1;