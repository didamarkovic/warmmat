% 'chisq_test'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM
% version 1.2
% Using 1/m. Finding chisq too!
% 16.04.2010 KM
% version 1.3
% Testing new function chisq.m! -> chisq_test.m
% 16.04.2010 KM
% version 1.4
% plotting error of m_wdm from fish and probability from chisq
% 16.04.2010 KM
% Now doing a massive chisq run for all parameters!
% 14.05.2010 KM

clear %clc

tic

l_max = 4;
linecolor = 'k';

suppressh = 0;
chibool = 1;
fishbool = 0;

% Set computational parameters
cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;
%cospars.m_wdm = 1500;

nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});
%plotbin = 1;

z_vals = 0.0001:0.05:5;
l_vals = 2*logspace(1,l_max,20);
M_vals = logspace(0,16,50);

nsigma = 1; % i.e. 3 sigma
P_limit = exp(-nsigma^2/2);
probints = [0.6827 0.9545 0.9973];

%tmp = linspace(1/15000,1/1000,50); % WDM % this ran for 34 hours!
tmp = linspace(1/15000,1/0.002,20); % WDM
%ms = 1./[tmp linspace(1/995,1/500,40)]; clear tmp
ms = 1./tmp; clear tmp

limit = NaN;
limith = NaN;

%steps = 0.00132;%
%steps = [0.00155 0.0005 0.00001 0.0000001];
%steps = [0.0001 0.00001];
%steps = 0.00060776; % 1-sigma
steps = 5.4724e-04;

c = clock;
fprintf(['Starting at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) ' of April 2010.\n'])

%%
if suppressh
    [CLS{1} CLSh{1} noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
        nzpars.nbin,NaN,M_vals);
else
    [CLS{1} xx noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
end


for wint = 1:length(ms)
    
    for int = 1:length(steps)
        
        fish.names = {'invm'};
        fish.steps = steps(int);
        
        cospars.m_wdm = ms(wint);
        
        % Find the power spectrum and the dCls/dparameter
        %[cls clsh xx dcls dclsh] = lens_ps_get(cospars_fid,nzpars,z_vals,l_vals,nzpars.nbin,fish,M_vals);
        if fishbool
            if suppressh
                [CLS{wint+1} CLSh{wint+1} xx dcls] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
                    nzpars.nbin,fish,M_vals);
            else
                [CLS{wint+1} xx xx dcls] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,fish);
            end
        else
            if suppressh
                [CLS{wint+1} CLSh{wint+1}] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
                    nzpars.nbin,NaN,M_vals);
            else
                CLS{wint+1} = lens_ps_get(cospars,nzpars,z_vals,l_vals);
            end
            
        end
        
        if fishbool
            % Get covariance matrices
            [C_mat C_vect lens_vals_v] = covmat(l_vals,CLS{wint+1}+xx,nzpars.fsky);
            %[Ch_mat Ch_vect lensh_vals_v] = covmat(l_vals,clsh+xx,nzpars.fsky);
            
            % Get Fisher matrices
            [fmat errs(wint,int) corrs] = fishmat(fish.names,dcls,C_mat);
            %[fmath errsh corrsh] = fishmat(fish.names,dclsh,Ch_mat);
        end
    end
    
end

save chifish-2010-12-07.mat

%return

% %% Analysis
% 
% clear chisqs probs P_lim tmp
% 
% stepint = 1; % The step-index
% if 1
%     nofid = 1; samebool=0; fishbool=1; chibool=0;
% elseif 0
%     lim = limit; %samebool = 1; % fishbool=1;
%     nofid = abs(newms-lim)==min(abs(newms-lim)); % newms(nofid)
% elseif 0
%     nofid = fishlimint;
%     %nofid=20; % 1/newms(450)
% else
%     def = 4000;
%     nofid = abs(newms-def)==min(abs(newms-def)); % newms(nofid)
% end
% newms = [10^10 ms]; % THIS IS REALLY JUST FOR PLOTTING: 10^10 is CDM!!!
% %newms=newms(1:25);
% %CLS=CLS(1:25);
% 
% % Find chisq and probabilities
% warning off
% tmp(1) = CLS(nofid);
% %tmp(2:length(CLS)) = CLS([1:(nofid-1) (nofid+1):length(CLS)]);
% tmp(2:(length(CLS)+1)) = CLS;
% [chisqs probs P_lim] = chisq(tmp,nzpars,l_vals,nsigma,noise);
% if suppressh
%     tmph(1) = CLSh(nofid);
%     tmph(2:length(CLSh)) = CLSh([1:(nofid-1) (nofid+1):length(CLSh)]);
%     [chisqsh probsh Ph_lim] = chisq(tmph,nzpars,l_vals,nsigma);
% end
% warning on
% 
% % Now find the limit:
% % limit_index = (probability-P_limit)<10^(-5);
% % improbable_ms = ms(limit_index);
% % limit = max(improbable_ms);
% %newms = [max(ms)*10 ms]; % THIS IS REALLY JUST FOR PLOTTING!!!
% probs = probs-(1:length(probs)).*(probs==0)*10^(-10);
% probs = probs+(length(probs):(-1):1).*(probs==1)*10^(-10);
% limit = interp1(probs,newms,P_lim)
% if suppressh
%     probsh = probs-(length(probsh):(-1):1).*(probsh==0);
%     limith = interp1(probsh,newms,Ph_lim)
% else
%     limith = NaN;
%     probsh = NaN;
%     Ph_lim = NaN;
% end
% 
% % Fisher error = 1/m:
% fishlimint = min(abs(nsigma*errs(:,stepint)'-1./ms))==abs(nsigma*errs(:,stepint)'-1./ms);
% fishlim = 1/ms(fishlimint);
% 
% % Integrate probability from chisq and find 99.73%/2 of the area:
% dx = 1./ms - 1./newms(1:length(ms));
% integral = cumsum(probs(1:length(ms)).*dx);
% integral = integral/max(integral);
% limindex = min(abs(integral-probints(nsigma)/2))==abs(integral-probints(nsigma)/2);
% limindex2 = min(abs(integral-probints(nsigma)))==abs(integral-probints(nsigma));
% limfromint = ms(limindex);
% limfromint2 = ms(limindex2);
% 
% % Plotting
% 
% if chibool
%     %figure(1); % clf
%     if fishbool subplot(2,1,1); end
%     
%     %     semilogx(1./ms,(probs-P_lim),linecolor); hold on
%     %     if nofid==1
%     %         area([1/min(ms) 1/limit],[1.1 1.1],'FaceColor','r'); hold on
%     %     end
%     %     semilogx(1./ms,(probsh-Ph_lim),[linecolor '--']); hold on
%     %     line([1/limith 1/limith],[min(probs) max(probs)],'Color','r')
%     %     legend('halofit',['excluded to ' num2str(nsigma) ' sigma'],'halo model',1)
%     %     axis([1/max(ms) 1/min(ms) 0 1])
%     %
%     %     p0 = semilogx(0,0,'LineStyle','none'); hold on
%     %     if nofid==1
%     %         p2 = line([limit limit],[0 1.1],'Color','r'); hold on
%     %     else
%     %         p2 = NaN;
%     %     end
%     %     p1 = line([min(ms) max(newms)],[P_lim P_lim],'Color','g'); hold on
%     %     %p3 = semilogx(newms,(probs-P_lim),linecolor); hold on
%     %     %p4 = semilogx(newms,(probsh-Ph_lim),[linecolor '--']); hold on
%     %     p3 = semilogx(newms,probs,linecolor); hold on
%     %     p4 = semilogx(newms,probsh,[linecolor '--']); hold on
%     %     line([limith limith],[min(probs) max(probs)],'Color','r')
%     %     legend([p2 p1 min(p3) min(p4)],['excluded to ' num2str(nsigma) ' sigma'],'detectability criterion','halofit','halo model',2)
%     %     axis([min(ms) max(ms) 0 1])
%     %     set([p1 p2],'LineWidth',1.5,'LineStyle','--')
%     %     %semilogx(newms,0.4,'ko')
%     %
%     %    ylabel('P(m_w_d_m^f^i^d)');
%     
%     
%     p0 = plot(0,0,'LineStyle','none'); hold on
%     if nofid==1
%         %p2 = line([1/limit 1/limit],[0 1.1],'Color','r','LineStyle','--'); hold on
%         %p1 = line([0 max(1./newms)],[P_lim P_lim],'Color','g','LineStyle','--'); hold on
%         %line([1/limith 1/limith],[min(probs) max(probs)],'Color','r','LineStyle','--')
%     end
%     %p3 = semilogx(newms,(probs-P_lim),linecolor); hold on
%     %p4 = semilogx(newms,(probsh-Ph_lim),[linecolor '--']); hold on
%     p3 = plot(1./newms,probs,linecolor); hold on
%     %plot(1./newms,probs,'y.'); hold on
%     %plot(-1./newms,probs,linecolor); hold on
%     p4 = plot(1./newms,probsh,[linecolor '--']); hold on
%     line([0 0],[min(probs) max(probs)],'Color','k','LineStyle','--')
%     %if fishbool
%     axis([0 max(1./ms) -0.01 1.01])
%     %else
%     %    axis([0 max(1./ms)/10 -0.01 1.01])
%     %end
%     %axis([-3*max(1./limit) 3*max(1./limit) 0 1.01])
%     
%     %line([fishlim fishlim],[0 1.1],'Color','c','LineStyle','--'); hold on
%     if 0
%         legno = 3;
%         if nofid(1)==1&&suppressh
%             %set([p1 p2],'LineWidth',1.5,'LineStyle','--')
%             legend([p2 p1 min(p3) min(p4)],['excluded to ' num2str(nsigma) ...
%                 ' sigma'],'detectability criterion','halofit','halo model',legno)
%         elseif nofid==1
%             %set([p1 p2],'LineWidth',1.5,'LineStyle','--')
%             legend([p2 min(p3) min(p4)],['excluded to ' num2str(nsigma) ...
%                 ' sigma'],'halofit',legno)
%         elseif suppressh
%             legend([min(p3) min(p4)],'halofit','halo model',legno)
%         else
%             legend([p2 p1 min(p3)],['excluded to ' num2str(nsigma) ...
%                 ' sigma'],'detectability criterion','halofit',legno)
%         end
%     end
%     %semilogx(newms,0.4,'ko')
%     %plot(1./ms,integral,'k-.')
%     %plot(1./limfromint,integral(limindex),'k.')
%     %plot(1./limfromint2,integral(limindex2),'k.')
%     %plot(1./limfromint,probs(limfromint==newms),'k.')
%     %plot(1./limfromint2,probs(limfromint2==newms),'k.')
%     
%     ylabel('P(1/m_w_d_m^f^i^d)');
%     
%     % Now fit the Gaussian from the Fisher calculation:
%     x = 1./newms - 1/newms(nofid);
%     fishprobs = exp(-x.^2 /2/errs(nofid,stepint)^2);
%     plot(1./newms,fishprobs/max(fishprobs),'b:') % clf
%     
%     fishintegral = cumsum(fishprobs(1:length(ms)).*dx);
%     fishintegral = fishintegral/max(fishintegral);
%     fishlimindex = min(abs(fishintegral-probints(nsigma)/2))==abs(fishintegral-probints(nsigma)/2);
%     fishlimindex2 = min(abs(fishintegral-probints(nsigma)))==abs(fishintegral-probints(nsigma));
%     fishlimfromint = ms(fishlimindex);
%     fishlimfromint2 = ms(fishlimindex2);
%     
%     %plot(1./fishlimfromint,fishprobs(fishlimindex)/max(fishprobs),'b.');
%     %plot(1./fishlimfromint2,fishprobs(fishlimindex2)/max(fishprobs),'b.');
%     
%     
%     
% end
% 
% 
% if fishbool
%     
%     %figure(2)
%     if chibool subplot(2,1,2); end
%     
%     p = plot(1./ms,errs,'k'); hold on
%     p1 = plot(1./ms,nsigma*errs,'k-.'); hold on
%     %plot(-1./ms,errs,'k'); hold on
%     r = plot(1./ms,1./ms,'k:');
%     line([0 0],[min(1./ms) max(1./ms)],'Color','k','LineStyle',':')
%     if nofid(1)==1
%         line([1/limit 1/limit],[min(1./ms) max(max(errs))],'Color','r','LineStyle','--')
%         %line([min(1./ms) max(1./ms)],[1/limit 1/limit],'Color','r','LineStyle',':')
%     end
%     axis([0 1/min(ms) min(min(errs)) max(max(errs))])
%     %axis([0 1/min(ms) min(min(1./10^6.4)) 1/limit*20])
%     xlabel('1/m_w_d_m^f^i^d [eV^-^1]')
%     ylabel('\Delta 1/m_w_d_m')
%     line([fishlim fishlim],[0 1.1],'Color','c','LineStyle','--');
%     %legend([p(int) r],'Fisher errors - different steps','1/m_w_d_m',2)
%     %legend(['fish.steps = ' num2str(steps)])
%     
%     %     p = semilogx(ms,errs,'k'); hold on
%     %     r = semilogx(ms,1./ms,'k--');
%     %     line([limit limit],[min(1./ms) max(1./ms)],'Color','r')
%     %     axis([min(ms) max(ms) min(min(1./ms)) max(max(1./ms))])
%     %     xlabel('m_w_d_m^f^i^d [eV]')
%     %     ylabel('\Delta 1/m_w_d_m')
%     %     legend([p(int) r],'Fisher errors - different steps','1/m_w_d_m',4)
%     
%     % for int = 1:length(steps)
%     %     msmat(:,int) = ms;
%     % end
%     % inverrs = msmat.^2.*errs;
%     % p = loglog(ms,inverrs,'k'); hold on
%     % r = loglog(ms,ms,'k--');
%     % line([limit limit],[min(ms) max(ms)],'Color','r')
%     % axis([min(ms) max(ms) min(ms) max(ms)])
%     % xlabel('m_w_d_m^f^i^d [eV^-^1]')
%     % ylabel('\Delta m_w_d_m')
%     % legend([p(int) r],'Fisher errors - different steps','1/m_w_d_m',4)
%     
%     %title(['fish.steps = ' num2str(steps)])
%     
% end
% 
% if fishbool&&samebool
%     samexaxis('xmt','on','ytac','join','yld',1);
% else
%     xlabel('1/m_w_d_m [eV^-^1]')
%     %xlabel('m_w_d_m [eV]')
% end
% 
% 
% toc
% 
% 
% return
%%

clear; load chifish-2010-07-07.mat

% Analysis 2 -> plot for paper

clear chisqs probs P_lim tmp

stepint = 1; % The step-index
nsigma = 1;

if 1
    nofid = 1; samebool=0; chibool=1;
elseif 0
    lim = limfromint; %samebool = 1; % fishbool=1;
    nofid = abs(newms-lim)==min(abs(newms-lim)); % newms(nofid)
elseif 0
    nofid = fishlimint;
    %nofid=20; % 1/newms(450)
else
    def = 1500;
    nofid = abs(newms-def)==min(abs(newms-def)); % newms(nofid)
end
newms = [10^10 ms]; % THIS IS REALLY JUST FOR PLOTTING: 10^10 is CDM!!!

% Find chisq and probabilities
warning off
tmp(1) = CLS(nofid);
tmp(2:(length(CLS)+1)) = CLS;
[chisqs probs P_lim] = chisq(tmp,nzpars,l_vals,nsigma,noise);
if suppressh
    tmph(1) = CLSh(nofid);
    tmph(2:length(CLSh)) = CLSh([1:(nofid-1) (nofid+1):length(CLSh)]);
    [chisqsh probsh Ph_lim] = chisq(tmph,nzpars,l_vals,nsigma);
end
warning on

% Integrate probability from chisq and find 68% (or whatever) of the area:
dx = 1./ms - 1./newms(1:length(ms));
integral = cumsum(probs(1:length(ms)).*dx);
integral = integral/max(integral);
limindex = min(abs(integral-probints(nsigma)))==abs(integral-probints(nsigma));
sequence = 1:length(limindex);
realindex = sequence(limindex);
limfromint = ms(realindex-1);

fiducialms = newms(nofid);

%% Ok, need to interpolate for the paper at this point:

oldprobs = probs; olddx = dx;

interpbool = 1;

if interpbool
    tmpnwms = 1./linspace(0.00000001,0.002,100);
    probs = interp1(newms,probs,tmpnwms,'spline'); probs(1:7) = ones(7,1);
    dx = [0 1./tmpnwms(2:100)-1./tmpnwms(1:99)];
    newms = tmpnwms;
end

%% Now fit the Gaussian from the Fisher calculation:
tmpx = 1./newms - 1/fiducialms; %1/newms(nofid); %1/newms(length(newms))
x = tmpx; %interp1(1:length(tmpx),tmpx,1:100,'spline');
fishprobs = exp(-x.^2 /2/errs(nofid,stepint)^2); % errs'

if interpbool
    fishintegral = cumsum(fishprobs(1:length(newms)).*dx);
else
    fishintegral = cumsum(fishprobs(1:length(ms)).*dx);
end
fishintegral = fishintegral/max(fishintegral);
fishlimindex = min(abs(fishintegral-probints(nsigma)))==abs(fishintegral-probints(nsigma));
sequence = 1:length(fishlimindex);
realfishindex = sequence(fishlimindex);
if interpbool
    fishlimfromint = newms(realfishindex-1);
else
    fishlimfromint = ms(realfishindex-1);
end

% Plotting
%figure%(1)

p0 = plot(0,0,'LineStyle','none'); hold on

%if nofid==1
    %p2 = line([1/limit 1/limit],[0 1.1],'Color','r','LineStyle','--'); hold on
    %p1 = line([0 max(1./newms)],[P_lim P_lim],'Color','g','LineStyle','--'); hold on
    %line([1/limith 1/limith],[min(probs) max(probs)],'Color','r','LineStyle','--')
%end

% First the real probability & the real limit in 1D
if nofid(1)==1
    p1 = plot(1./newms,probs,linecolor); hold on
    t1 = line([0 0],[min(probs) max(probs)],'Color','k');
    t2 = line([1/limfromint 1/limfromint],[0 oldprobs(limindex)],'Color','k','LineStyle','--'); hold on
    p1a = plot(1/limfromint,oldprobs(limindex),'kx'); %probs(limindex),'kx');
else
    t3 = plot(1./newms,probs,[linecolor ':']); hold on
end

if nofid(1)==1
    p2 = plot(x+1./newms(nofid),fishprobs/max(fishprobs),'b-.');
    t4 = line([1/fishlimfromint 1/fishlimfromint],[0 fishprobs(fishlimindex)],'Color','b','LineStyle','--'); hold on
    p2a = plot(1/fishlimfromint,fishprobs(fishlimindex),'bx');
else
    t5 = plot(x+1./newms(nofid),fishprobs/max(fishprobs),'b:');
end

ylabel('P(m_w_d_m^-^1)','FontSize',15); xlabel('m_w_d_m^-^1 [eV^-^1]','FontSize',15)
if nofid==1 axis([-0.00001 max(x) -0.01 1.01]); end
%legend([p1 p2 p1a p2a],'P(\chi^2)','Gaussian','68.3% for P(\chi^2)','68.3% for Gaussian')
legend([p1 p2 p1a p2a],'P(m^-^1)','Gaussian','68.3% for P(m^-^1)','68.3% for Gaussian')

%if nofid(1)~=1 set([p1 p1a p2 p2a t1 t2 t3 t4 t5],'LineWidth',1.5);
%else set([p1 p1a p2 p2a t1 t2 t4],'LineWidth',1.5);
%end

if nofid(1)~=1 set([p1 p1a p2 p2a],'LineWidth',1.5);
else set([p1 p1a p2 p2a],'LineWidth',1.5);
end

axis([0 0.002 0 1.01])
set(gca,'LineWidth',1.5,'FontSize',15)

%plot(1./ms,fishintegral,'b:')

return

%% Analysis - fisher plot for paper - NOT GOING IN!

crossing = interp1(nsigma*errs'-1./ms,1./ms,0)

% Plotting

figure(2)
p1 = plot(1./ms,nsigma*errs,'k'); hold on
p2 = plot(1./ms,1./ms,'k:');
p3 = plot(crossing,crossing,'ro');
set([p1 p2 p3],'LineWidth',1.5);
%axis([0 1/min(ms) min(min(errs)) max(max(errs))])
axis([0 0.002 0 nsigma*max(errs)])
xlabel('1/m_w_d_m^f^i^d [eV^-^1]','fontsize',15)
legend([p1 p2 p3],'Fisher error','x=y','d\theta = \sigma_w_d_m = 6.0776e-04')
ylabel('\Delta 1/m_w_d_m','fontsize',15)
set(gca,'fontsize',15)