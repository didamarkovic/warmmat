% 'pk_nlin_get_demo'
%
% 29/07/08 KMarkovic

clear

tic

%k_vals = logspace(-2.5,4,1000); % Need many values of k!
k_vals = logspace(-2.5,3,1000); %24.06.2011 KM

%z_vals = 0.0001:0.03:3;
z_vals = 0.0001:0.3:1; %24.06.2011 KM

%l_vals = logspace(1,5,500);
l_vals = logspace(1,5,20); %24.06.2011 KM
plotfactor = l_vals .* (1+l_vals) /2/pi;

% Cosmology
m_wdm = 0;
%cospars = setpars([0.05 0.7 0.25 0 0.7 0.9 -0.7 1 0.25 m_wdm],...
%    {'omega_b' 'omega_de' 'omega_dm' 'omega_nu' 'h' 'sigma8' 'w0' 'ns' ...
%    'omega_wdm' 'm_wdm'});
cospars = setEUCLIDpars; %24.06.2011 KM
nzpars = setnzpars(1,{'nbin'});


% Smith et al
%pk_vals_s = pk_nlin_get(cospars,k_vals,z_vals,'smith');
pk_vals_s = pk_nlin_get(cospars,k_vals,z_vals,'smith2'); % 24.06.2011 KM
figure(1); loglog(k_vals,pk_vals_s(1,:),'k'); hold on
lens_ps_vals_s = lens_ps(cospars,z_vals,l_vals,k_vals,pk_vals_s,nzpars);
figure(2); loglog(l_vals,plotfactor.*squeeze(lens_ps_vals_s(1,1,:))','k'); 
hold on

% Peacock & Dodds
pk_vals_pd = pk_nlin_get(cospars,k_vals,z_vals,'p&d');
figure(1); loglog(k_vals,pk_vals_pd(1,:),'b--'); hold on
lens_ps_vals_pd = lens_ps(cospars,z_vals,l_vals,k_vals,pk_vals_pd,nzpars);
figure(2); loglog(l_vals,plotfactor.*squeeze(lens_ps_vals_pd(1,1,:))','b--')

% WDM: sims
cospars.m_wdm = 0; %200; % keV
%pk_vals_w = pk_nlin_get(cospars,k_vals,z_vals,'smith2');
pk_vals_w = pk_nlin_get(cospars,k_vals,z_vals,'vielfit'); % 24.06.2011 KM
figure(1); loglog(k_vals,pk_vals_w(1,:),'r--'); hold on
lens_ps_vals_w = lens_ps(cospars,z_vals,l_vals,k_vals,pk_vals_w,nzpars);
figure(2); loglog(l_vals,plotfactor.*squeeze(lens_ps_vals_w(1,1,:))','r--')

% WDM: halo % 24.06.2011 KM
cospars.m_wdm = 200; % keV
pk_vals_w_h = pk_nlin_get(cospars,k_vals,z_vals,'warmhalo');
figure(1); loglog(k_vals,pk_vals_w_h(1,:),'m--'); hold on
lens_ps_vals_w_h = lens_ps(cospars,z_vals,l_vals,k_vals,pk_vals_w,nzpars);
figure(2); loglog(l_vals,plotfactor.*squeeze(lens_ps_vals_w_h(1,1,:))','m--')

figure(1)
xlabel('k'); ylabel('P_k')
title('matter power spectrum')
axis([k_vals(1) 1e2 1e-2 1e5])
legend('Smith','Peacock&Dodds','Smith + WDM: 200keV')
%saveas(gcf,'Matter_power_spectra_2...','bmp');

figure(2)
axis([1e1 1e4 1e-7 3e-4])
xlabel('l'); ylabel('C^\kappa_i_j(l) l(l+1) /2\pi')
title('convergence power spectrum')
legend('Smith','Peacock&Dodds','Smith + WDM: 200keV','Halo + WDM: 200keV',4)
%saveas(gcf,'Lensing_power_spectra_2...','bmp');

toc
