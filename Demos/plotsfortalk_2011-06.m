% plotsfortalk.m
%
% Written for the talk at the CIAS Meudon 2011 WDM Workshop.
% 09.06.2011

clear;

clear
cospars = setEUCLIDpars; %cospars.omega_wdm
k_vals = logspace(-2,4,1000);
cols = 'kbgr';
m_wdm = [0 4000 1000 500];
[Mfs Rfs kfs] = freestreaming_comov(cospars,m_wdm);
[gstar Tx_Tnu m_nu N_nu Tx] = degoffree(m_wdm,1,cospars.omega_dm,cospars.h);

%% From get_nofz_demo:
dz = 0.01;
z_vals = 0.00001:dz:3;

nzpars = setnzpars([10],{'nbin'});
[n_z_tot ngals_bin]=get_nofz(nzpars,z_vals);
pl = plot(z_vals, n_z_tot/nzpars.nbin/dz); hold on
%plot(z_vals,sum(n_z_tot'/nzpars.nbin),'k-');

nzpars = setnzpars([1],{'nbin'});
[n_z_tot ngals_bin]=get_nofz(nzpars,z_vals);
rl = plot(z_vals, n_z_tot/nzpars.nbin/dz,'k-'); hold on
%sum(n_z_tot)

set([rl],'linewidth',1.2)
xlabel('redshift')
ylabel('fraction of sources')


%% Linear matter power spectrum with WDM

% Linear matter power spectrum:
Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin_0_c = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
%D_plus = growth_factor(cospars,z_vals);
%pk_lin = pk_lin_0 * D_plus.^2; % Grow spectrum

figure
pl = 0; %rl = 0;
for ii = 2:length(m_wdm)
    cospars.m_wdm = m_wdm(ii);
    pk_lin_0(ii,:) = wdm_transfn_pars(pk_lin_0_c,k_vals,cospars);

    %index = min(abs(k_vals-kfs(ii)))==abs(k_vals-kfs(ii));

    pl(ii) = loglog(k_vals,pk_lin_0(ii,:),cols(ii),'linewidth',1.2); hold on
    %rl(ii) = loglog(k_vals(index),pk_lin_0(index),[cols(ii) 'x'],'linewidth',1.2);
    %rl(ii) = line([k_vals(index) k_vals(index)],[1e-10 1e10],'color',cols(ii),'linestyle',':');%,'linewidth',1.2);
end
axis([min(k_vals) 1e2 1e-5 1e5])
xlabel('k [h/Mpc]'); ylabel('P_l_i_n(k)')
pl(1) = loglog(k_vals,pk_lin_0_c,'k','linewidth',1.2); hold on
legend(pl,'CDM','m_w_d_m = 4 keV','m_w_d_m = 1 keV','m_w_d_m = 0.5 keV')


%% Non-linear power spectra
tic

model = 'readsmith'; %'halo'; %'smith2'

m_wdm = [0 1000 500];

redshifts = [0 1 2];
%gf = growth_factor(cospars,redshifts).^2;

k_vals = logspace(-2,2,1000);

for zz = 1:length(redshifts)
    
    scrsz = get(0,'ScreenSize');
    figure('Position',[1 scrsz(4) 2*scrsz(3)/5 scrsz(4)])

    subplot(2,1,1)

    pl = 0; pn = 0;
    for ii = 1:length(m_wdm)
        cospars.m_wdm = m_wdm(ii);

        [pk_nlin(ii,:) pk_lin(ii,:)] = pk_nlin_get(cospars,k_vals,redshifts(zz),model);

        pl(ii) = loglog(k_vals,pk_lin(ii,:),[cols(ii) '--'],'linewidth',1.2); hold on
        pn(ii) = loglog(k_vals,pk_nlin(ii,:),cols(ii),'linewidth',1.2); hold on
    end
    pl(1) = loglog(k_vals,pk_lin(1,:),'k','linewidth',1.2); hold on
    pn(1) = loglog(k_vals,pk_nlin(1,:),'k','linewidth',1.2); hold on

    axis([min(k_vals) max(k_vals) 1e-5 1e5])
    xlabel('k [h/Mpc]'); ylabel('P(k)')
    legend(pn,'CDM','m_w_d_m = 4 keV','m_w_d_m = 1 keV','m_w_d_m = 0.5 keV',3)
    title(['z = ' num2str(redshifts(zz))])

    subplot(2,1,2)

    for ii = 1:length(m_wdm)
        rl(ii) = semilogx(k_vals,pk_lin(ii,:)./pk_lin(1,:) -1,[cols(ii) ':']); hold on
        rn(ii) = semilogx(k_vals,pk_nlin(ii,:)./pk_nlin(1,:) - 1,cols(ii),'linewidth',1.2);
    end
    axis([min(k_vals) max(k_vals) -0.1 0.05])
    %legend(rn,'CDM','m_w_d_m = 4 keV','m_w_d_m = 1 keV','m_w_d_m = 0.5 keV',3)
    xlabel('k [h/Mpc]'); ylabel('( P_w_d_m(k)-P_c_d_m(k) )/P_c_d_m(k)')
    
    samexaxis('xmt','on','ytac','join','yld',1)
end

toc

%% Lensing

nzpars.meth = 'halo';

tic

cospars = setEUCLIDpars;
dz = 0.03; z_vals = 0.0001:dz:3;

M_vals = logspace(5,15,50);

l_vals = 2*logspace(1,4,50);

cospars.m_wdm = m_wdm(1);
[Clsw1 tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,nan,M_vals);    

cospars.m_wdm = m_wdm(2);
[Clsw2 tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,nan,M_vals);    

cospars.m_wdm = m_wdm(3);
[Clsw3 tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,nan,M_vals);    

toc


% Plot the lensing

bin = 10; % to plot
bjn = 10;

scrsz = get(0,'ScreenSize');
figure('Position',[1 scrsz(4) 2*scrsz(3)/5 scrsz(4)])
subplot(2,1,1)

[p1 plotfactor] = plotlens(l_vals,Clsw1(bin,bjn,:),[cols(1)],1); hold on
p2 = plotlens(l_vals,Clsw2(bin,bjn,:),[cols(2)],1);
p3 = plotlens(l_vals,Clsw3(bin,bjn,:),[cols(3)],1);
loglog(l_vals,plotfactor.*squeeze(noise(bin,bjn,:))','k:')

% lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
 set([p1 p2 p3],'linewidth',1.2);
 %xlabel('l, multipole'); 
 ylabel('l(l+1)C_\kappa/2\pi')
 legend([p1 p2 p3],'CDM','1 keV','0.5 keV',4)
 axis([l_vals(1) l_vals(length(l_vals)) 3e-7 3e-4])

% Ratios:
subplot(2,1,2)
pr1 = semilogx(l_vals,squeeze((Clsw2(bin,bin,:)./Clsw1(bin,bin,:)))-1,cols(2)); hold on
pr2 = semilogx(l_vals,squeeze((Clsw3(bin,bin,:)./Clsw1(bin,bin,:)))-1,cols(3));
p0 = semilogx(l_vals,zeros(size(l_vals)),'k:'); 
xlabel('l, multipole'); ylabel('(C_\kappa^W^D^M-C_\kappa^C^D^M)/C_\kappa^C^D^M')
legend([pr1 pr2],'1000 eV','500 eV',3)
%lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
set([pr1 pr2],'linewidth',1.2);
axis([l_vals(1) l_vals(length(l_vals)) -0.15 0.07])

samexaxis('xmt','on','ytac','join','yld',1)
