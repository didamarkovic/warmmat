% 'chisq_test'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM
% version 1.2
% Using 1/m. Finding chisq too!
% 16.04.2010 KM
% version 1.3
% Testing new function chisq.m! -> chisq_test.m
% 16.04.2010 KM

clear %clc

tic

l_max = 4;
linecolor = 'k';

suppressh = 0;

% Set computational parameters
cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;
%cospars.m_wdm = 1500;

nzpars = setnzpars([2 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});
%plotbin = 1;

z_vals = 0.0001:0.1:5;
l_vals = 2*logspace(1,l_max,20);
M_vals = logspace(0,16,50);

nsigma = 3; % i.e. 3 sigma

ms = 500:1000:20000; % WDM

%% CDM

if suppressh
    [CDM CDMh noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
        nzpars.nbin,NaN,M_vals);
else
    [CDM CDMh noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
end
    
CDM_plot = CDM;
CDM = CDM+noise;
CDMh_plot = CDMh;
CDMh = CDMh+noise;

CLS{1} = CDM;
CLSh{1} = CDMh;

%% WDMs

for wint = 1:length(ms)
    
    cospars.m_wdm = ms(wint);
    if suppressh
        
        [WDM WDMh] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);
    else
        [WDM WDMh] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
    end
    if wint==1; WDM_plot = WDM; WDMh_plot = WDMh; end
    WDM = WDM + noise;
    WDMh = WDMh + noise;
    
    CLS{wint+1} = WDM;
    CLSh{wint+1} = WDMh;
    
end

[chisqs probs P_lim] = chisq(CLS,nzpars,l_vals,nsigma);
[chisqsh probsh Ph_lim] = chisq(CLSh,nzpars,l_vals,nsigma);

%% Now find the limit:
% limit_index = (probability-P_limit)<10^(-5);
% improbable_ms = ms(limit_index);
% limit = max(improbable_ms);
limit = interp1(probs,ms,P_lim)
if suppressh    
    probsh(1) = -1;
    limith = interp1(probsh,ms,Ph_lim)
else
    limith = NaN;
end

%% Plotting

% plotbin = 2;
% 
% figure(1)
% plotlens(l_vals,CDM_plot(plotbin,plotbin,:),'k'); hold on
% plotlens(l_vals,WDM_plot(plotbin,plotbin,:),'r')
% plotlens(l_vals,CDM(plotbin,plotbin,:),'k--'); hold on
% plotlens(l_vals,WDM(plotbin,plotbin,:),'rx')

figure%(1); % clf
semilogx(1./ms,(probs-P_lim),linecolor); hold on
semilogx(1./ms,(probsh-Ph_lim),[linecolor '--']); hold on
area([1/min(ms) 1/limit],[1.1 1.1],'FaceColor','r')
%line([limit limit],[min(probability) max(probability)])
%plot(limit,P_limit,[linecolor 'x'])
%plot(limith,P_limit,[linecolor 'o'])
line([1/limith 1/limith],[min(probs) max(probs)])
ylabel('probability'); xlabel('1/m_w_d_m [eV^-^1]')
%semilogx(ms,0.5*ones(size(ms)),'go');
legend('halofit','halo model',['excluded to ' num2str(nsigma) ' sigma'], 3)
axis([1/max(ms) 1/min(ms) 0 1])
%title('m_f_i_d = 20000 eV')

toc