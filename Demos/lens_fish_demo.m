% 'lens_fish_demo'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM

clear

%logm_step = 0.0001;
%logm_fid = 3.5;
l_max = 4;
step_invm = 0.0014605;
invm_fid = 0;

% Set computational parameters
cospars_fid = setWMAP7pars;
%cospars_fid.As = 2.1386*10^(-9);
cospars_fid.As = 1.2733*10^5;
cospars_fid.k_pivot = 0.05;
cospars_fid.Gamma = exp(-2*cospars_fid.omega_b*cospars_fid.h) *cospars_fid.omega_m *cospars_fid.h;

%cospars_fid.m_wdm = 10^logm_fid;
cospars_fid.m_wdm = 1/invm_fid;
%cospars_fid.m_wdm = 500;
cospars_fid.omega_wdm = cospars_fid.omega_dm; 

nzpars = setnzpars([2 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});

z_vals = 0.0001:0.05:5; 
l_vals = 2*logspace(1,l_max,20);

%fish.names = {'omega_m'; 'h'; 'omega_b'; 'As'; 'logm'};                       
%fish.names = {'omega_m'; 'h'; 'omega_b'; 'sigma8'; 'logm'};                       
%fish.steps = [0.03  0.1  0.09 0.01 logm_step];
%fish.names = {'omega_m'; 'h'; 'omega_b'; 'sigma8'; 'm_wdm'};                       
%fish.steps = [0.03  0.1  0.09 0.01 500];
%fish.names = {'omega_m'; 'h'; 'omega_b'; 'sigma8'; 'ns'};                       
%fish.steps = [0.01  0.7  0.044 0.002 0.01];
%fish.names = {'As'; 'ns'; 'omega_m'; 'Gamma'; 'logm'};                       
%fish.steps = [10 0.00001 0.00001 0.00001 logm_step];
fish.names = {'As'; 'ns'; 'omega_m'; 'Gamma'; 'invm'};                       
fish.steps = [10 0.00001 0.00001 0.00001 step_invm];
                              
%% Find the power spectrum and the dCls/dparameter
tic
[cls tmp xx dcls] = lens_ps_get(cospars_fid,nzpars,z_vals,l_vals,nzpars.nbin,fish);
toc
%% Get covariance matrices
[C_mat C_vect lens_vals_v] = covmat(l_vals,cls+xx,nzpars.fsky);

% Get Fisher matrices
[fmat errs corrs] = fishmat(fish.names,dcls,C_mat);

%% Read out the values of the fiducial parameters for ellipse plotting:
for int = 1:length(fish.names)
    if strcmp(fish.names{int},'logm')
        bfp(int) = logm_fid;
    elseif strcmp(fish.names{int},'invm')
        bfp(int) = invm_fid;
    else
        bfp(int) = getfield(cospars_fid,fish.names{int})';
    end
end

% now plot the error ellipses
[h,errvals] = plotellipses_v1(bfp,inv(fmat),fish.names,['b' 'r' 'g' 'b' '-'],[0.68 0.95],3,20,1:length(bfp),0.68);
%set(h,'linewidth',1.2)

%m_wdm = 10^logm_fid;
%m_wdm = cospars_fid.omega_m/cospars_fid.O_m_wdm;
%deltam = abs(m_wdm^2/cospars_fid.omega_m*(errvals(1)/m_wdm - errvals(length(bfp))));
%fprintf(1,' Therefore:\n')
%fprintf(1,'m_wdm is %1.0f +/- %1.0f to 68 perc. conf.\n',m_wdm_fid,deltam)
%fprintf(1,'\n')

%% Plot vs WDM
% cospars = cospars_fid;
% cospars.omega_wdm = cospars.omega_dm;
% cospars.m_wdm = 1000;
% wdm = lens_ps_get(cospars,nzpars,z_vals,l_vals);
% 
% figure(2)
% plotlens(l_vals,cls(2,2,:),'k',1);
% plotlens(l_vals,wdm(2,2,:),'r--');


%figure
%mfid = [100 500 1000 2000 5000 10000 100000];
%merr = [2 9 72 440 3619 15000 1000000];
%plot(mfid,merr)
%axis([0 5000 0 5000])

%names = fish.names;

% save lens_fish_demo_mat_2010-06-07.mat