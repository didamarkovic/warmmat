% MASSFNS_DEMO
% Try to reproduce plots in the Sheth-Tormen paper (fig2).
% 18.05.2011 KMarkovic

clear

cospars = setWMAP7pars;
M_vals = logspace(6,16,100);
z_vals = [0,0.5,1,5,10];
cols = {'k','b','c','g','y','r','m'};

[dndms dndms_ps M_stars sigma] = massfns_slb(M_vals,z_vals,cospars.omega_b,...
    cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,cospars.sigma8);

consts
rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m;
delta_c = ones(size(z_vals))*1.686;%delta_c_alt(z_vals,cospars.omega_m); 
%[delta_vals_tmp, z_vals_tmp] = growth_wconst(cospars.omega_m,cospars.omega_de,cospars.w0,1000);
%delta_vals = interp1(z_vals_tmp, delta_vals_tmp, z_vals);
delta_vals = growth_factor(cospars,z_vals);

for ii = 1:length(z_vals)
    nu(:,ii) = (delta_c(ii)/delta_vals(ii)./sigma(:,ii)).^2;
    %nu(:,ii) = (delta_c(ii)./sigma(:,ii)).^2;

    tmp = diff(log(M_vals))./diff(log(nu(:,ii)))';
    dlnMdlnnu = [tmp(1) tmp];

    nufnu(:,ii) = dndms(:,ii)' .* M_vals.^2/rho_mean_Mpc_0 .* dlnMdlnnu;% /2;
end

%% Plot nu*f(nu) like in the Shet-Tormen paper - same at all redshifts!
for ii = 1:length(z_vals)
    r = loglog(nu(:,ii),nufnu(:,ii),[cols{ii} '.']); hold on
    %pause
end
axis([0.1 20 1e-3 1e0])
xlabel('\nu')
ylabel('\nuf(\nu)')


%% Mass functions

p = zeros(size(z_vals));
for ii = 1:length(z_vals)
    p(ii) = loglog(M_vals,dndms(:,ii),cols{ii}); hold on
    q(ii) = loglog(M_vals,dndms_ps(:,ii),[cols{ii} '--']); hold on
    if ~isnan(M_stars(ii))
        wheremin = abs(M_stars(ii)-M_vals)==min(abs(M_stars(ii)-M_vals));
        o(ii) = loglog(M_vals(wheremin),dndms(wheremin,ii),[cols{ii} '.']);
    end
end
legend(p,'z=0','z=0.5','z=1','z=5','z=10')
axis([min(M_vals) max(M_vals) 1e-25 1e-5])
title('Redshift evolution of mass functions')
xlabel('M_h[M_o/h]')
ylabel('dn/dM')

%% Now also growth factor

figure
%plot(1./(1+z_vals_tmp),delta_vals_tmp)
plot(1./(1+z_vals),delta_vals)