% 'parent code: lens_methods_test.m'
% 'lenshalo_tomo_updating.m'
%
% Trying to modify this script to perform the function of lenshalo_tomo.m,
% since can't get lenshalo_tomo.m to behave...
%
%
% version 4.2
% added mass function normalization options
% Feb.2010 KM
% version 4.1
% added loop in line 64 and played around with the factor of 1/(1+z)^6...
% 01.02.2010 KM
% version 4
% 19.11.09 KMarkovic

clear; %clf

plotWbool=0; % Plot the lensing weigths
plotPlbool=0; % Plot the matter power spectrum vs l_vals
plotPkbool=0; % Plot the matter power spectrum vs k_vals
plotCbool=1; % Plot the convergence power spectrum

normbool = 1; % Mass function normalisation

z_vals = 0.00001:0.1:5.00001;
if plotPkbool z_vals = 0.5; end%[0.5 1 2]; % CAN ONLY USE THIS IF WANT ONLY Pk!!
nlintype = 'smith2';
nzpars = setnzpars([200 1 max(z_vals)*1.0001],{'noth' 'nbin' 'zs' nlintype});
cospars = setWMAPpars;
%cospars.O_m_wdm = cospars.omega_m/500;

l_vals = 2*logspace(0,5,20); %%% [no units !!]

% Now the matter power spectrum - in & cf:
k_vals = logspace(-4,4,1000); %%% [h/Mpc]
[pk_nlin pk_lin pk_lin_0] = pk_nlin_get(cospars,k_vals,z_vals,nlintype);
%%% [Mpc^3/h^3] I guess...?

% Now the mass integral for:
M_vals = logspace(0,16,20); %%% [M_solar]

consts;

%% RHO_crit_0/SIGMA_crit:

% Galaxy distribution:
if length(z_vals)==1
    n_z_mat = 1;
else
    [n_z_mat niz] = get_nofz(nzpars,z_vals);
end

% Distances and differential comoving volume
Dh = c_km/H0_Mpc; % in Mpc

% If have z-distribution of sources:
slbpars = convertpars(cospars);
chim = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
chimat = chim * Dh;

% Distance to lenses + Comoving Volumes:
[chis tmp tmp tmp volm] = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
chi = chis * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;

% If have only one source redshift:
if isfield(nzpars,'zs')
    chiso = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    chisource = chiso * Dh;
    chi_ls = chisource - chi;
    chimat = chi;
end


SIGMAcrit = (c_km^2) .* chisource ./ ( 4*pi*G_Mpc .* chi.* chi_ls ) ./(1+z_vals);
% actually units of this one her are: km^2 M_sol / Mpc^4


%%%%%%%%%%%%
% C.F. Lensing weight: EXTRA FACTOR OF h^2???

Wi_chi = lens_weight(cospars,z_vals,chimat,n_z_mat,nzpars.zs,chisource);
if plotWbool || plotCbool

    rho_crit_0 = 3 * H0_Mpc^2  / (8*pi*G_Mpc); % units of km^2 M_sol / Mpc^5
    rho_mean_0 = rho_crit_0 * cospars.omega_m;% km^2 M_solar /Mpc^5;

    mypar = rho_mean_0./SIGMAcrit;

    if plotWbool
        figure(1)
        plot(z_vals, mypar,'b','linewidth',5); hold on
        plot(z_vals,Wi_chi*cospars.h^2,'g--','linewidth',1.1); hold on
        xlabel('redshift','fontsize',15); ylabel('Wi(\chi)*D_h*h^2','fontsize',15)
        title('Lensing weight','fontsize',15)
    end
end
%%%%%%%%%%%%


%% MATTER POWER SPECTRUM ETC:

%pk_lin_0 = pk_lin(1,:);


[dndm_un tmp M_scale_tmp sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_0); % Pk today or at z???? clf

dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]


M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0); %%% [M_solar]

[rv_vals theta_v conc rho_s n00] = haloparams(cospars.omega_m,...
    cospars.h,M_scale,M_vals,z_vals,chi*Mpc);%%% [meters][no units][no units][kg/m^3]
rv_vals = rv_vals/Mpc;
rho_s = rho_s/M_solar*Mpc^3;

if plotCbool
    dX = chi(2:length(chi))-chi(1:(length(chi)-1));
    dX(length(chi)) = interp1(z_vals(1:(length(z_vals)-1)),dX,max(z_vals),'spline','extrap');
end

% First Sigma_l
ONE = ones(length(z_vals),1);%%%[no units here!]
SIGMA_l = fourierkappa(z_vals, chi, M_vals, l_vals,theta_v,...
    rv_vals, conc, rho_s, ONE,nzpars.noth);


delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units]


% Growth
[delta_vals_tmp, z_vals_tmp] = growth_wconst(cospars.omega_m,cospars.omega_de,cospars.w0,1000);
growth_vals_tmp = delta_vals_tmp ./ ( max(delta_vals_tmp) ); 
growth_vals = interp1(z_vals_tmp, growth_vals_tmp, z_vals);


% Bias:

a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]

for zint = 1:length(z_vals)

    % Need to normalise the mass functions (units: (Mpc/h)^-3 / (M_solar/h) )!
    %norm = sum( dndm_unnorm(:,zint)' * cospars.h^4.* dM .* M_vals*M_solar / rho_mean_0 );

    rho_mean_Mpc = rho_crit_0_Mpc * cospars.omega_m*(1+z_vals(zint))^3; %%%[M_sol/Mpc^3]
    rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m; %%%[M_sol/Mpc^3]
    %rho_mean = rho_crit_0 * cospars.omega_m*(1+z_vals(zint))^3; %%%[kg/m^3]

    %norm = sum( dndm_un(:,zint)' .* dM*cospars.h^4 .* M_vals/ rho_mean_Mpc); %%% [no units]
    %norm = sum( dndm_un(:,zint)' .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
    if normbool
        norm = sum( dndm_un(:,zint)' .* dM .* M_vals/ rho_mean_Mpc) ; %%% [no units]
    elseif normbool==3
     Delta = 1 - norm;
     F = rho_mean_Mpc * Delta / M_vals(1) /dM(1); % ****
     dndm_add = zeros(size(dndm_un(:,zint)));
     dndm_add(1) = F;
    else
        norm = 1;
        %norm = 1/cospars.h^4/(z_vals(zint)+1)^4;
    end
    %dndm(:,zint) = dndm_un(:,zint) / norm / Mpc^3 * cospars.h^4;

    %dndm(:,zint) = dndm_un(:,zint) / norm * cospars.h^4; %%%[Mpc^-3 M_solar^-1]
    dndm(:,zint) = dndm_un(:,zint) / norm; %%%[h^4 Mpc^-3 M_solar^-1]
%     dndm(:,zint) = dndm_un(:,zint) + dndm_add;

    % Now find the bias:
    %nu = (delta_c(zint) ./ sigma(:,zint)).^2; %%% [unitless]
    nu = (delta_c(zint) ./ sigma(:,zint) ./growth_vals(zint)).^2; %%% [unitless]

    % From Seljak 0001493v1 (after equn 9):
    bias_unnorm =  1  +  (a* nu - 1 )/delta_c(zint)  +  2*p./( 1 + (a*nu).^p );

    % Need to normalise the bias:
    %norm = sum( dndm(:,zint)'  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias_unnorm' ); %%%[no units]
    norm = sum( dndm(:,zint)'  .* dM .* M_vals  / rho_mean_Mpc .* bias_unnorm' ); %%%[no units]
    bias = bias_unnorm / norm; %%%[no units]


    % Now the mass integral:
    for lint = 1:length(l_vals)

        % THE UNITS ARE NOW DIFFERENT THAN WHAT IT SAYS HERE!!!!:

        dIntM_C = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint) .* bias; %%%[Mpc^-3 kg m^-2]

        %IntM_C(zint,lint) = sum(dIntM_C) / Mpc^3; %%%[kg m^-5]
        IntM_C(zint,lint) = sum(dIntM_C); %%%[M_sol Mpc^-5]

        dIntM_P = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint).^2; %%%[Mpc^-3 kg^2 m^-4]

        %IntM_P(zint,lint) = sum(dIntM_P)/ Mpc^3; %%%[kg^2 m^-7]
        IntM_P(zint,lint) = sum(dIntM_P); %%%[M_sol^2 Mpc^-7]

    end

    %tmp_Correlations = IntM_C(zint,:) .* X(zint)^2 / rho_mean_0; %%%[no units]

    %tmp_Correlations = IntM_C(zint,:) .* chi(zint)^2 / rho_mean_Mpc(zint); %%%[no units]
    tmp_Correlations = IntM_C(zint,:) .* chi(zint)^2 / rho_mean_Mpc_0; %%%[no units]

    %tmp_Poisson = IntM_P(zint,:) .* X(zint)^4 / rho_mean_0^2;%%%[m^3]

    %tmp_Poisson = IntM_P(zint,:) .* chi(zint)^4 / rho_mean_Mpc(zint)^2;%%%[m^3]
    tmp_Poisson = IntM_P(zint,:) .* chi(zint)^4 / rho_mean_Mpc_0^2;%%%[m^3]

    % The above makes no difference, clearly, at z = 0!

    %tmp_intrpl_C = interp1(l_vals/X(zint)*Mpc/cospars.h,tmp_Correlations,k_vals);%%%[no units]
    if plotPkbool && ~plotPlbool
        tmp_intrpl_C = interp1(l_vals/chi(zint)/cospars.h,tmp_Correlations,k_vals);%%%[no units]
    else
        pk_lin_intrpl(zint,:) = interp1(k_vals*chi(zint)*cospars.h,pk_lin(zint,:),l_vals);%%%[no units]
        pk_lin_intrpl(zint,isnan(pk_lin_intrpl(zint,:) )) =0;
    end

    if plotPkbool && ~plotPlbool
        %tmp_intrpl_P = interp1(l_vals/X(zint)*Mpc/cospars.h,tmp_Poisson,k_vals);%%%[m^3]
        tmp_intrpl_P = interp1(l_vals/chi(zint)/cospars.h,tmp_Poisson,k_vals);%%%[m^3]
    else
        tmp_intrpl_P = tmp_Poisson;%%%[m^3]
    end

    % THE UNITS IN THE FOLLOWING TWO QUANTITIES ARE WRONG:

    if plotPkbool && ~plotPlbool
        tmp_pk_C(zint,:) = tmp_intrpl_C.^2 .* pk_lin(zint,:); %%%[Mpc^3 h^-3]
    else
        tmp_pk_C(zint,:) = tmp_Correlations.^2 .* pk_lin_intrpl(zint,:); %%%[Mpc^3 h^-3]
    end

    tmp_pk_P(zint,:) = tmp_intrpl_P * cospars.h^3; %%% [m^3]
    %tmp_pk_P(zint,:) = tmp_intrpl_P * cospars.h^6 * (1+z_vals(zint)).^4; %%% [m^3]
    %warning('h^3*(1+z)^6 factor') % this stuff makes it look better with
    %no norm, but with it looks stupid.
    
    % Dimensionlass MPS:
    %DELTA_C(zint,:) = k_vals.^3.*tmp_pk_C(zint,:) /2/pi; %%%[no units]
    %DELTA_P(zint,:) = k_vals.^3.*tmp_pk_P(zint,:) /2/pi  * (cospars.h/Mpc)^3; %%%[no units]
    %DELTA_P(zint,:) = k_vals.^3.*tmp_pk_P(zint,:) /2/pi ; %%%[no units]
    % Real:
    %DELTA_nlin(zint,:) = k_vals.^3 .*pk_nlin(zint,:) /2/pi; %%%[no units]

    if plotPlbool || plotCbool
        % Just for testing:
        pk_nlin_intrpl(zint,:) = interp1(k_vals*chi(zint)*cospars.h,pk_nlin(zint,:),l_vals);
        pk_nlin_intrpl(zint,isnan(pk_nlin_intrpl(zint,:) )) = 0;
    elseif plotPkbool

    end

    if plotCbool

        % Lensing Power Spectrum:
        %    if zint == 1
        %         dX(zint)= X(zint) - 0;
        %    else
        %        dX(zint) = X(zint) - X(zint-1);
        %    end
        dcl_P(zint,:) = tmp_pk_P(zint,:) * mypar(zint).^2 /chi(zint)^2 *dX(zint)/cospars.h^3;
        dcl_C(zint,:) = tmp_pk_C(zint,:) * mypar(zint).^2 /chi(zint)^2 *dX(zint)/cospars.h^3;

        dcl_tj_1(zint,:)  = pk_nlin_intrpl(zint,:)  * mypar(zint)^2/chi(zint)^2 *dX(zint) /cospars.h^3;%*2.68;%*(1+z_vals(zint))^3;%!!!!
        %dcl_tj_1(zint,:)  = pk_nlin_intrpl(zint,:)  * Wi_chi(zint)^2*Dh^2*cospars.h/X(zint)^2 *dX(zint) ;%*2.55;%!!!!

    end

end


%% FIGURES & LENSING POWER SPECTRUM:


% LPS LPS LPS LPS LPS LPS LPS LPS
if plotCbool
    figure(2) %clf
    [cls halo] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals,0,1);
    e = plotlens(l_vals,cls,'k');
    f = plotlens(l_vals,halo,'g');
    set([e f],'linewidth',1.1)

    cl_P = sum(dcl_P,1)';
    cl_C = sum(dcl_C,1)';
    cl = cl_P + cl_C;
    a = plotlens(l_vals,cl_P,'b:');
    b = plotlens(l_vals,cl_C,'r:');
    c = plotlens(l_vals,cl,'y--');
    cl_tj_1 = sum(dcl_tj_1,1)';
    d = plotlens(l_vals,cl_tj_1,'c-.');
    set([a b c d],'linewidth',1.1)
    axis([l_vals(1) l_vals( length(l_vals) )  9e-10 9e-3]) ;
    if normbool
        legend([e,f,a,b,c,d],'t&j fn','halo fn','1H raw','2H raw','halo raw','t&j raw',2)
    elseif normbool==3
        legend([e,f,a,b,c,d],'t&j fn','halo fn','1H raw','2H raw','halo raw - weird raw','t&j raw',2)
    else
        legend([e,f,a,b,c,d],'t&j fn','halo fn','1H raw','2H raw','halo raw - no norm','t&j raw',2)
    end
    title('CDM: z_s = 5')
    
    %     figure(4)
    %     p = semilogx(l_vals,cl./cl_tj_1,'k'); hold on
    %     r = semilogx(l_vals,cl_P./cl_tj_1,'b--'); hold on
    %     s = semilogx(l_vals,cl_C./cl_tj_1,'r--'); hold on
    %     set([p r s],'linewidth',1.1)
    %     axis([l_vals(1) l_vals( length(l_vals) )  0 2]) ;
    %     ylabel('ratio'); xlabel('l, multipole')
    %     title('ratio of convergence power spectra: halo/T&J')
end


% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
if plotPlbool
    figure(3)
    l=loglog(l_vals,tmp_pk_C,'r'); hold on
    m=loglog(l_vals,tmp_pk_P,'b'); hold on
    n=loglog(l_vals,tmp_pk_P+tmp_pk_C,'g'); hold on
    o=loglog(l_vals,pk_lin_intrpl,'k:');
    axis([2 2e4 1 9e4])
    p=loglog(l_vals,pk_nlin_intrpl,'k--');
    set([l m n o p],'linewidth',1.2);
    ylabel('P(k)'); xlabel('l, multipole')
    %legend([l m o p],'2H' ,'1H' ,'P_l_i_n(k)' ,'P_n_l_i_n(k)',2)
    title('z_s = 0.1')
end


% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
if plotPkbool && ~plotPlbool
    figure(3)
    l=loglog(k_vals,tmp_pk_C,'r'); hold on
    m=loglog(k_vals,tmp_pk_P,'b'); hold on
    n=loglog(k_vals,(tmp_pk_P+tmp_pk_C),'g'); hold on
    o=loglog(k_vals,pk_lin,'k:');
    %axis([0.001 10 0.1 2e4])
    %axis([k_vals(1) max(k_vals) min(tmp_pk_P+tmp_pk_C) max(tmp_pk_P+tmp_pk_C)])
    p=loglog(k_vals,pk_nlin,'k--');
    set([l m n o p],'linewidth',1.2);
    ylabel('P(k)'); xlabel('k, wavenumber')
end