% lenshalo_wdm_demo
%
% This script plots the Cooray et al power spectrum with varying m_wdm in
% the transfer function from Viel et al and with no mass cut-offs over the 
% Takada & Jain power spectrum for the same cosmologies.
%
% version 2
% 20.11.09 KMarkovic

clear

%figure

cospars = setWMAPpars;
cospars_cdm = cospars;
%cospars.omega_b = 0.00441;  % for testing omega_b dependence
%cospars.omega_m = cospars.omega_m - cospars.omega_b; % for testing omega_b dependence
%cospars.sigma8 = 0.9;
%nzpars = setnzpars([1 1],{'nbin' 'zs' 'smith2'}); dz = 0.1; z_vals = 0.0001:dz:nzpars.zs;
nzpars = setnzpars(1,{'nbin' 'smith2'}); dz = 0.3; z_vals = 0.0001:dz:3;
bin = 1; % to plot

%m_vals = [0 200 1500];
l_vals = logspace(1,4,20); pf = l_vals.*(1+l_vals)/2/pi;
M_vals = logspace(0,16,50);


% [Cls Cls_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);    
% 
% %figure
% lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
% p1 = plotlens(l_vals,Cls(bin,bin,:),'k',1);
% p2 = plotlens(l_vals,Cls_halo(bin,bin,:),'k--');
% title('Weak Lensing Power Spectrum with WDM');
%set(p1,'linewidth',lw);
%legend([p1 p2], 'Takada & Jain','Cooray et al',2)
%saveas(gcf,'Cls_cooray_wdm...','bmp')

%figure
%semilogx(l_vals,squeeze(Cls(bin,bin,:)./Cls_halo(bin,bin,:)),'b','linewidth',1.5)
%ylabel('ratio = T&J/HALO'); xlabel('l, multipole')

%% WDM:
cospars.omega_wdm = cospars.omega_dm;

% m = 400 eV
cospars.m_wdm = 400;
[Cls Cls_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);    
p3 = plotlens(l_vals,Cls(bin,bin,:),'m',1);
p4 = plotlens(l_vals,Cls_halo(bin,bin,:),'m--');
 Mmin = freestreaming_comov(cospars,cospars.m_wdm);
M_vals_wdm = logspace(log10(Mmin),16,50);
[tmp Cls_halo_Mmin]= lens_ps_get(cospars_cdm,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals_wdm,1);    
p9 = plotlens(l_vals,Cls_halo_Mmin(bin,bin,:),'m.');


% m = 500 eV
cospars.m_wdm = 500;
[Cls Cls_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);    
p5 = plotlens(l_vals,Cls(bin,bin,:),'r',1);
p6 = plotlens(l_vals,Cls_halo(bin,bin,:),'r--');
Mmin = freestreaming_comov(cospars,cospars.m_wdm);
M_vals_wdm = logspace(log10(Mmin),16,50);
[tmp Cls_halo_Mmin]= lens_ps_get(cospars_cdm,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals_wdm,1);    
p10 = plotlens(l_vals,Cls_halo_Mmin(bin,bin,:),'r.');

% m = 1 keV
cospars.m_wdm = 1000;
[Cls Cls_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);    
p7 = plotlens(l_vals,Cls(bin,bin,:),'g',1);
p8 = plotlens(l_vals,Cls_halo(bin,bin,:),'g--');
Mmin = freestreaming_comov(cospars,cospars.m_wdm);
M_vals_wdm = logspace(log10(Mmin),16,50);
[tmp Cls_halo_Mmin]= lens_ps_get(cospars_cdm,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals_wdm,1);    
p11 = plotlens(l_vals,Cls_halo_Mmin(bin,bin,:),'g.');


%% CDM
[Cls Cls_halo]= lens_ps_get(cospars_cdm,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);    
lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
p1 = plotlens(l_vals,Cls(bin,bin,:),'k',1);
p2 = plotlens(l_vals,Cls_halo(bin,bin,:),'k--');
title('Weak Lensing Power Spectrum with WDM');
%set([p3 p5 p7 p1],'linewidth',1.2);
set([p5 p7 p1],'linewidth',1.2);
xlabel('l, multipole'); ylabel('l(l+1)C_\kappa/2\pi')
%legend([p5 p7 p1],'500 eV','1 keV','CDM',4)
%legend([p3 p5 p7 p1],'200 eV','500 eV','1 keV','CDM',4)
legend([p4 p6 p8 p2],'400 eV','500 eV','1 keV','CDM',4)
axis([l_vals(1) l_vals(length(l_vals)) 3e-7 3e-4])