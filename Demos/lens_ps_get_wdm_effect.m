% 'lens_ps_get_wdm_effect'
%
% version 1
% Plotting Cls vs. m_wdm for single value of l.
% 01.05.2010 KMarkovic

clear

tic

cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;

nzpars = setnzpars([1 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});

z_vals = 0.000001:0.05:5;

M_vals = logspace(0,16,50);

l_vals = [2000 20000];

%ms = [logspace(2.1,3.5,20) 0];
ms = [logspace(2.1,3.2,20) 0];

effectsnoise = zeros(length(l_vals),length(ms));
effectshnoise = zeros(length(l_vals),length(ms));
for ind = 1:length(ms)
    
    cospars.m_wdm = ms(ind);
    
    %[cls(:,:,:,ind) halo(:,:,:,ind) noise] = lens_ps_get...
    %    (cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);
    [cls halo noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);
    
    %effectsline(:,ind) = squeeze(cls);
    effectsnoise(:,ind) = squeeze(cls+noise);
    effectshnoise(:,ind) = squeeze(halo+noise);
    
    fprintf([num2str(ind) '/' num2str(length(ms)) '\n'])
end

for lind = 1:length(l_vals)
    %effectsline(lind,:) = effectsline(lind,:)/effectsline(lind,length(ms));
    effectsnoise(lind,:) = effectsnoise(lind,:)/effectsnoise(lind,length(ms));
    effectshnoise(lind,:) = effectshnoise(lind,:)/effectshnoise(lind,length(ms));
end

ms = ms(1:(length(ms)-1));
effectsnoise = effectsnoise(:,1:length(ms));
effectshnoise = effectshnoise(:,1:length(ms));

%save 'effectwdm_halotoo_2010-06-08.mat'
%%
figure%('Position',[1 500 500 500]) %clc
c = plot(ms,effectsnoise); hold on
set(c(1),'Color','k')
set(c(2),'Color','r')
d = plot(ms,effectshnoise); hold on
set(d(1),'Color','k')
set(d(2),'Color','r')
set(d,'LineStyle','-.')
%semilogx(ms,halo(1,1,:,:),'r'); hold on
xlabel('m_w_d_m [eV^-^1]')
ylabel('C_\kappa(l) - normalised by CDM')
legend([c; d(1)],'l = 2000','l = 20000','halo model', 4)
axis([min(ms) max(ms) 0.8 1])
set([c d],'LineWidth',1.5)
%title('The effect of WDM suppresion on the C_\kappa for two different l')

toc