% 'lens_fish_demo_invm'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM
% version 1.2
% Using 1/m. Finding chisq too!
% 16.04.2010 KM

clear %clc

tic

l_max = 4;
linecolor = 'k';

suppressh = 0;

% Set computational parameters
cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;
%cospars.m_wdm = 1500;

nzpars = setnzpars([1 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});
%plotbin = 1;

z_vals = 0.0001:0.05:5;
l_vals = 2*logspace(1,l_max,20);
M_vals = logspace(0,16,50);

nsigma = 3; % i.e. 3 sigma
P_limit = exp(-nsigma^2/2);

ms = logspace(1.5,3.5,50); % WDM
%ms = 500:1000:20000; % WDM

limitcol = 'r';
limstyle = '--';

%% CDM

if suppressh
    [CDM CDMh noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
        nzpars.nbin,NaN,M_vals);
else
    [CDM CDMh noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
end
    
CDM_plot = CDM;
CDM = CDM+noise;
CDMh_plot = CDMh;
CDMh = CDMh+noise;

%% Covariance Matrix

%ibin = 1; jbin = 1;
CC = covmat(l_vals,CDM,nzpars.fsky);
CCh = covmat(l_vals,CDMh,nzpars.fsky);

%% WDMs

chisq = zeros(size(ms));
chisqh = zeros(size(ms));
for wint = 1:length(ms)
    
    cospars.m_wdm = ms(wint);
    if suppressh
        
        [WDM WDMh] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);
    else
        [WDM WDMh] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
    end
    if wint==1; WDM_plot = WDM; WDMh_plot = WDMh; end
    WDM = WDM + noise;
    WDMh = WDMh + noise;
    
    
    index = 1;
    CDM_vect = zeros(nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    WDM_vect = zeros(nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    CC_tmp = zeros(nzpars.nbin*(nzpars.nbin+1)/2,nzpars.nbin,nzpars.nbin,length(l_vals));
    CDMh_vect = zeros(nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    WDMh_vect = zeros(nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    CCh_tmp = zeros(nzpars.nbin*(nzpars.nbin+1)/2,nzpars.nbin,nzpars.nbin,length(l_vals));
    for ibin = 1:nzpars.nbin
        
        for jbin = ibin:nzpars.nbin
            
            CDM_vect(index,:) = CDM(ibin,jbin,:);
            WDM_vect(index,:) = WDM(ibin,jbin,:);
            CDMh_vect(index,:) = CDMh(ibin,jbin,:);
            WDMh_vect(index,:) = WDMh(ibin,jbin,:);
            
            CC_tmp(index,:,:,:) = CC(ibin,jbin,:,:,:);
            CCh_tmp(index,:,:,:) = CCh(ibin,jbin,:,:,:);
            
            index = index + 1;
            
        end
    end
    
    index = 1;
    CC_2Dmat = zeros(nzpars.nbin*(nzpars.nbin+1)/2,nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    CCh_2Dmat = zeros(nzpars.nbin*(nzpars.nbin+1)/2,nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    for ibin = 1:nzpars.nbin
        
        for jbin = ibin:nzpars.nbin
            
            CC_2Dmat(:,index,:) = CC_tmp(:,ibin,jbin,:);  
            CCh_2Dmat(:,index,:) = CCh_tmp(:,ibin,jbin,:);  
            
            index = index + 1;
            
        end
        
    end
    
    chitmp = zeros(length(l_vals),1);
    dC = zeros(nzpars.nbin*(nzpars.nbin+1)/2,1);
    chitmph = zeros(length(l_vals),1);
    dCh = zeros(nzpars.nbin*(nzpars.nbin+1)/2,1);
    for int = 1:length(l_vals)
        
        dC = CDM_vect(:,int) - WDM_vect(:,int);        
        dCh = CDMh_vect(:,int) - WDMh_vect(:,int);        
        
        chitmp(int) = dC'/CC_2Dmat(:,:,int)*dC;
        chitmph(int) = dCh'/CCh_2Dmat(:,:,int)*dC;
        
    end
    
    chisq(wint) = sum(chitmp);
    chisqh(wint) = sum(chitmph);
    
end


probability = exp(-chisq/2);
probabilityh = exp(-chisqh/2);

%% Now find the limit:
% limit_index = (probability-P_limit)<10^(-5);
% improbable_ms = ms(limit_index);
% limit = max(improbable_ms);
probability(probability==0) = min(probability(probability~=0))*...
    (0:(sum(probability==0)-1))/10/sum([probability==0 1]);
limit = interp1(probability,ms,P_limit)
if suppressh
    probabilityh(probabilityh==0) = min(probabilityh(probabilityh~=0))*...
        (0:(sum(probabilityh==0)-1))/10/sum([probabilityh==0 1]);
    limith = interp1(probabilityh,ms,P_limit)
else
    limith = NaN;
end

%% Plotting

% plotbin = 2;
% 
% figure(1)
% plotlens(l_vals,CDM_plot(plotbin,plotbin,:),'k'); hold on
% plotlens(l_vals,WDM_plot(plotbin,plotbin,:),'r')
% plotlens(l_vals,CDM(plotbin,plotbin,:),'k--'); hold on
% plotlens(l_vals,WDM(plotbin,plotbin,:),'rx')

%figure%(1); % clf
%semilogx(1./ms,(probability-P_limit),linecolor); hold on
%semilogx(1./ms,(probabilityh-P_limit),[linecolor '--']); hold on
semilogx(1./ms,probability,linecolor); hold on
semilogx(1./ms,probability,[linecolor '--']); hold on
%area([1/min(ms) 1/limit],[1.1 1.1],'FaceColor','r')
p=line([1/limit 1/limit],[min(probability) max(probability)],'Color',limitcol)
set(p,'LineWidth',1.5,'LineStyle',limstyle)
%plot(limit,P_limit,[linecolor 'x'])
%plot(limith,P_limit,[linecolor 'o'])
line([1/limith 1/limith],[min(probability) max(probability)])
ylabel('probability'); xlabel('1/m_w_d_m [eV^-^1]')
%semilogx(ms,0.5*ones(size(ms)),'go');
legend('halofit','halo model',['excluded to ' num2str(nsigma) ' sigma'], 3)
axis([1/max(ms) 1/min(ms) 0 1])
%title('m_f_i_d = 20000 eV')

toc

%save P(invm)-2010-04-20.mat

%%
fprintf('*****DONE WITH CHISQ!*****\n')
return
%% 
clc
c = clock;
fprintf(['Starting at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) ' of April 2010.\n'])

clear

%m1_step = 10^(-3);
%m1_fid = 0;
l_max = 4;

M_vals = logspace(0,16,50);

% Set computational parameters
cospars_fid = setWMAP7pars;
cospars_fid.As = 1.2733*10^5;
cospars_fid.k_pivot = 0.05;
cospars_fid.Gamma = exp(-2*cospars_fid.omega_b*cospars_fid.h) *cospars_fid.omega_m *cospars_fid.h;

%cospars_fid.omega_wdm = cospars_fid.omega_dm;

nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});

z_vals = 0.0001:0.05:5;
l_vals = 2*logspace(1,l_max,20);

%fish.names = {'omega_m'; 'h'; 'omega_b'; 'As'; 'logm'};
%fish.names = {'omega_m'; 'h'; 'omega_b'; 'sigma8'; '1/m'};
%fish.steps = [0.03  0.1  0.09 0.01 m1_step];
%fish.names = {'omega_m'; 'h'; 'omega_b'; 'sigma8'; 'm_wdm'};
%fish.steps = [0.03  0.1  0.09 0.01 500];
%fish.names = {'omega_m'; 'h'; 'omega_b'; 'sigma8'};
%fish.steps = [0.03  0.1  0.09 0.01 0.1 0.1];
fish.names = {'As'; 'ns'; 'omega_m'; 'Gamma'};                       
fish.steps = [10 0.00001 0.00001 0.00001];


% Find the power spectrum and the dCls/dparameter
tic
[cls clsh xx dcls dclsh] = lens_ps_get(cospars_fid,nzpars,z_vals,l_vals,nzpars.nbin,fish,M_vals);
toc
% Get covariance matrices
[C_mat C_vect lens_vals_v] = covmat(l_vals,cls+xx,nzpars.fsky);
[Ch_mat Ch_vect lensh_vals_v] = covmat(l_vals,clsh+xx,nzpars.fsky);

% Get Fisher matrices
[fmat errs corrs] = fishmat(fish.names,dcls,C_mat);
[fmath errsh corrsh] = fishmat(fish.names,dclsh,Ch_mat);

% Read out the values of the fiducial parameters for ellipse plotting:
for int = 1:length(fish.names)
    if strcmp(fish.names{int},'logm')
        bfp(int) = logm_fid;
    elseif strcmp(fish.names{int},'invm')
        bfp(int) = invm_fid;
    else
        bfp(int) = getfield(cospars_fid,fish.names{int})';
    end
end

% now plot the error ellipses
figure(2)
[h,errvals] = plotellipses_v1(bfp,inv(fmat),fish.names,'qbwk-',[0.68 0.95],3,20,1:length(bfp),0.68);
hold on
[k,errvals] = plotellipses_v1(bfp,inv(fmath),fish.names,'qbwg-',[0.68 0.95],3,20,1:length(bfp),0.68);
legend('halofit','full halo')
%m_wdm = 10^logm_fid;
%m_wdm = cospars_fid.omega_m/cospars_fid.O_m_wdm;
%deltam = abs(m_wdm^2/cospars_fid.omega_m*(errvals(1)/m_wdm - errvals(length(bfp))));
%fprintf(1,' Therefore:\n')
%fprintf(1,'m_wdm is %1.0f +/- %1.0f to 68 perc. conf.\n',m_wdm_fid,deltam)
%fprintf(1,'\n')

%save fisher-2010-04-23-invm.mat