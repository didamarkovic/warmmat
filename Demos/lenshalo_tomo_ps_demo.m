% 'lenshalo_ps_demo'
% 
% This plots the lensing power spectrum again with Cooray,Hu&Miralda-Escude
% Takes about 50 seconds to run at the set accuracy.
% 25/08/08 KM, modified from halowlps_vielfit_demo2: 25/09/08

clear
tic

lvals = logspace(1,4,20);
dz = 0.05; zs = 1; zvals = 0.00001:dz:0.9999999;
Mvals = logspace(0,16,100); 

%consts; omegas; sigma8 = 0.9;
cospars = setpars([0.65 0.3],{'omega_de'; 'omega_dm'});
nzpars = setnzpars([1 zs],{'nbin'; 'zs'});

k_vals = logspace(-2,2,1000);

%Gamma = exp(-2*omega_b*h)*omega_m*h;
%pk = pkf(Gamma,sigma8,k_vals);
Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
pk = pkf(Gamma,cospars.sigma8,k_vals);

%[Cls Cl_P Cl_C pf] = halowlps_vielfit(k_vals,pk,0,zvals,zs,dz,lvals,Mvals);
[Cls pf x Cl_P Cl_C] = lenshalo_tomo(cospars,zvals,lvals,k_vals,pk,nzpars,Mvals,NaN,NaN,3);

%figure
lw = 2; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
%p16 = loglog(lvals,pf.*Cls,'k'); hold on
%p16p = loglog(lvals,pf.*Cl_P,'b--'); 
%p16c = loglog(lvals,pf.*Cl_C,'r--');
p16 = loglog(lvals,squeeze(pf.*Cls),'k'); hold on
p16p = loglog(lvals,squeeze(pf.*Cl_P),'b--'); 
p16c = loglog(lvals,squeeze(pf.*Cl_C),'r--');
axis([lvals(1) lvals(length(lvals)) 1e-8 1e-3])
title('Weak Lensing Power Spectrum');
xlabel('multipole l','fontsize',fs); 
ylabel('l(l+1)C_l/2\pi','fontsize',fs);
set([p16 p16p p16c],'linewidth',lw);
legend([p16 p16p p16c],'total power spectrum','1-halo term','2-halo term',2)
%saveas(gcf,'Cooray_Cls_25-08...','bmp')
toc