% lens_fish_tomo_demo_plots
%
% 30/09/08 KM

% First need to open the run.mat file (by doubleclicking it)!

% Let's check the power spectra look right in tomography:
figure
plotfactor = l_vals.*(1+l_vals)/2/pi;
lw = 1.5; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
%p1a = loglog(l_vals,plotfactor.*squeeze(cls(1,1,:))','g'); hold on
%p2a = loglog(l_vals,plotfactor.*squeeze(halo(1,1,:))','b'); 
p1b = loglog(l_vals,plotfactor.*squeeze(cls(2,2,:))','g'); hold on
p2b = loglog(l_vals,plotfactor.*squeeze(halo(2,2,:))','b'); 
%p1c = loglog(l_vals,plotfactor.*squeeze(cls(3,3,:))','g'); 
%p2c = loglog(l_vals,plotfactor.*squeeze(halo(3,3,:))','b'); 
axis([l_vals(1)/2 max(l_vals) 1e-9 1e-3])
title('Weak Lensing Power Spectrum plotting methods - different bins');
xlabel('multipole l','fontsize',fs); 
ylabel('l(l+1)C_l/2\pi','fontsize',fs);
%set([p1a p2a p1b p2b p1c p2c],'linewidth',lw);
%legend([p1a p2a], 'Takada & Jain','Cooray et al',2)
legend([p1b p2b], 'Takada & Jain','Cooray et al',2)

% Write out the Fisher matrices
fmat
fmath

% Read out the values of the fiducial parameters for ellipse plotting:
for int = 1:length(fish.names)
    bfp(int) = getfield(cospars_fid,fish.names{int})';
end

% Now get the Planck error ellipse:
clear planck
%fid = fopen('ftran.txt');
%planck_tmp = cell2mat(textscan(fid,'%n'));
%fclose(fid);
planck_tmp = zeros((size(fmat,1)-1)^2);
maxint = sqrt(length(planck_tmp));
for int = 1:maxint
    for jnt = int:maxint
        planck(int,jnt) = planck_tmp( (jnt-1)*maxint + int );
        planck(jnt,int) = planck(int,jnt);
    end
end
planck(:,maxint+1) = zeros(maxint,1);
planck(maxint+1,:) = zeros(1,maxint+1);

% now plot the error ellipses
figure
[h,errvals] = plotellipses(bfp,pinv(fmat),fish.names,'qbwb-'); hold on
[pl,errvalspl] = plotellipses(bfp,pinv(fmath+planck),fish.names,'qbwk:');
[hh,errvalsh] = plotellipses(bfp,pinv(fmath),fish.names,'qbwg-');

set(h,'linewidth',1.5)
set(hh,'linewidth',1.5)
set(pl,'linewidth',1.5)

%saveas(gcf,'figure4','bmp')
%print('-f3', '-dpsc', 'figure4');

% Simple error on omega_b:
%erronOb = 1/sqrt(fmat(2,2));
% C.f. to
%invfish = pinv(fmat);
%erronOb2 = sqrt(invfish(4,4));
% These turn out to be about half the size of Sarah's from plotellipses