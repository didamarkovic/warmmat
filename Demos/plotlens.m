function [p plotfactor] = plotlens(ells,ps,linecol,bool,axmin,axmax,fs,ls)
%PLOTLENS(ells,ps,linecol,bool,axmin,axmax,fs,ls) plots the output 
%   from my lens_ps_get function.
%
%   ells - l_vals
%   ps - the power spectrum for 1 bin
%   fs - fontsize (default = 15)
%   ls - linewidth (default 1.5)
%   bool - 0 to do figure, 1 for hold on, 2 for clf (default = 3)
%   linecol - string with the line proberties (default = 'k-')
%
%   version 1.1
%   18/09/08 KMarkovic

if ~exist('fs','var'); fs = 15; end
if ~exist('ls','var'); ls = 1.0; end
if ~exist('bool','var'); bool = 3; end
if ~exist('linecol','var'); linecol='k-'; end

if bool==0; figure; set(gca,'fontsize',fs)
elseif bool==2; clf; end

plotfactor = ells.*(ells+1)/2/pi;

spectrum = plotfactor.*squeeze(ps)';

p = loglog(ells,spectrum,linecol);

set(p,'linewidth',ls);

%if ~exist('axmax','var'); axmax = max(2e-4,2*max(spectrum)); end
if ~exist('axmax','var'); axmax = 2*max(spectrum); end
%if ~exist('axmin','var'); axmin = min(2e-8,max(spectrum)/1e3); end
if ~exist('axmin','var'); axmin = max(spectrum)/1e2; end
axis([ells(1) max(ells) axmin axmax])

xlabel('multipole, l','FontSize',12)
ylabel('l(l+1)C_l/2\pi','FontSize',12)

if bool; hold on; end
