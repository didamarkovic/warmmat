% Testing the concentration parameter for WDM scenarios by c.f.ing c =
% r_v/r_s and Seljak's fitting formula with a(z), b(z) and M_s.
%
% version 1
% 16.12.09 KMarkovic

clear

% Set up:
cospars = setWMAPpars;
z_vals = 0.5;
k_vals = logspace(-4,4,1000);
M_vals = logspace(5,15,4);

% Distance to halo:
consts
Dh = c_km/H0_Mpc; % Hubble distance [Mpc]
chi_tmp = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000); % [Dh]
chi = chi_tmp * Dh;  % [Mpc]

% Matter power spectrum
[tmp tmp pk_lin_cdm_0] = pk_nlin_get(cospars,k_vals,z_vals);

% Growth factor:
D_plus = 1;%growth_factor(cospars,z_vals,1000,0); - need mps today for mass functions!

% WDM Matter power spectrum (still linear!)
[tmp tmp pk_lin_0] = wdm_transfn(200,cospars.omega_dm,cospars.h,k_vals,1.12,pk_lin_cdm_0,D_plus,cospars.sigma8);

% Mass scale
M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0); % [M_sol]

% Halo properties:
[rv_vals theta_v conc rho_s n00] = haloparams(cospars.omega_m,...
    cospars.h,M_scale,M_vals,z_vals,chi*Mpc); % [m][unitless][unitless][kg/m^3]
rv_vals = rv_vals/Mpc; %[Mpc]
rho_s = rho_s/M_solar*Mpc^3;

% figure
% loglog(M_vals,rv_vals./conc,'b'); hold on
% %rs_coor = (3* M_scale./ ( 4*pi.*n00*rho_crit_0_Mpc*cospars.omega_m)) .^(1/3) ;
% rs_coor = (3* M_scale./ ( 4*pi.*rho_crit_0_Mpc*cospars.omega_m)) .^(1/3) ;
% loglog(M_vals,ones(size(M_vals))*rs_coor,'r')
% ylabel('r_s'); xlabel('M[M_s_o_l]')
% % No, this is clearly not the same.

% Now let's try:
figure
dens1 = 3*rho_s.*( log(1+conc)-(conc./(1+conc)) ) ./ conc.^3;
dens2 = n00 * rho_crit_0_Mpc * cospars.omega_m;
semilogx(M_vals,dens1,'b'); hold on
semilogx(M_vals,ones(size(M_vals))*dens2,'r--')
ylabel('dummy density'); xlabel('M[M_s_o_l]')
% Ok, this clearly works for CDM & WDM! I think this is true by defintion.

%% Now fudge concentration parameter to see how it changes the power
% spectrum:

clear

figure

cospars = setWMAPpars;
%cospars.omega_b = 0.00441;  % for testing omega_b dependence
%cospars.omega_m = cospars.omega_m - cospars.omega_b; % for testing omega_b dependence
%cospars.sigma8 = 0.9;
nzpars = setnzpars([1 1],{'nbin' 'zs' 'smith2'}); dz = 0.1; z_vals = 0.0001:dz:nzpars.zs;
%nzpars = setnzpars(2,{'nbin' 'smith2'}); dz = 0.1; z_vals = 0.0001:dz:1;
bin = 1; % to plot

%m_vals = [0 200 1500];
l_vals = logspace(1,4,20); pf = l_vals.*(1+l_vals)/2/pi;
M_vals = logspace(0,16,50);


% [Cls Cls_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);    
% 
% %figure
% lw = 1; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
% p1 = plotlens(l_vals,Cls(bin,bin,:),'k',1);
% p2 = plotlens(l_vals,Cls_halo(bin,bin,:),'k--');
% title('Weak Lensing Power Spectrum with WDM');
%set(p1,'linewidth',lw);
%legend([p1 p2], 'Takada & Jain','Cooray et al',2)
%saveas(gcf,'Cls_cooray_wdm...','bmp')

%figure
%semilogx(l_vals,squeeze(Cls(bin,bin,:)./Cls_halo(bin,bin,:)),'b','linewidth',1.5)
%ylabel('ratio = T&J/HALO'); xlabel('l, multipole')

% WDM:
cospars.omega_wdm = cospars.omega_dm;

% m = 400 eV
cospars.m_wdm = 400;
[Cls Cls_halo]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);    
p3 = plotlens(l_vals,Cls(bin,bin,:),'m',1);
p4 = plotlens(l_vals,Cls_halo(bin,bin,:),'m--');
 Mmin = freestreaming_comov(cospars,cospars.m_wdm);
 cospars.m_wdm = 0;
M_vals_wdm = logspace(log10(Mmin),16,50);
[tmp Cls_halo_Mmin]= lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals_wdm,1);    
p9 = plotlens(l_vals,Cls_halo_Mmin(bin,bin,:),'m.');
