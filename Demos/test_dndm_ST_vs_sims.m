% test_dndm_ST_vs_sims
%
% Plotting the Shet-Tormen WDM mass functions vs. data from sims given by
% Jesus Zavala, for m_wdm = 1 keV in files: dndm & dndm2.txt.
%
% 27.01.2010 KM
%
% version 2
% for some reason I had to write a new function for reading
% 25.06.2010 KM

% clear
% 
% filename = 'icosmo_9-9-z5.txt';
% nrows = 200;
% ncolumns = 3;
% 
% % Read data file:
% [data headerrow] = txtread(filename,ncolumns,nrows); % headerrow'  size(data) data(1,:)
% 
% l_vals = data(:,1);
% cl_vals = data(:,2);
% errors = data(:,3);
% 
% figure
% loglog(l_vals,cl_vals,'k'); hold on
% loglog(l_vals,cl_vals-errors,'c')
% loglog(l_vals,cl_vals+errors,'c')
% 
% axis([l_vals(1) max(l_vals) 1e-7 4e-4])

%% FROM JESUS ZAVALA'S SIMULATION:

figure

clear

filename = 'dndm.txt';
%nrows = 22;
%ncolumns = 2;
% Read data file:
%[data headerrow] = txtread(filename,ncolumns,nrows); % headerrow'  size(data) data(1,:)
%M_vals = data(:,1);
%dndm_vals = data(:,2);
CDM = myread(filename);
M_vals = CDM.M_vals;
dndm_vals = CDM.dndm_vals;

filename = 'dndm2.txt';
%nrows = 23;
%ncolumns = 2;
% Read data file:
%[data headerrow] = txtread(filename,ncolumns,nrows); % headerrow'  size(data) data(1,:)
%M_vals2 = data(:,1);
%dndm_vals2 = data(:,2);
WDM = myread(filename);
M_vals2 = WDM.M_vals;
dndm_vals2 = WDM.dndm_vals;

a=loglog(M_vals2,dndm_vals2,'r',M_vals,dndm_vals,'b'); hold on
set(a,'LineWidth',1.2)
xlabel('M[M_o]'); ylabel('n(M,z=0)')
legend('WDM - 1 keV','CDM')
axis([M_vals(1) max(M_vals)*2.5 min(dndm_vals2) max(dndm_vals)*2])


%% SHETH TORMEN:

%figure

% cospars  = setWMAPpars;
% Where did I get these??? 25.03.2010 KM
omega_m = 0.3;
omega_de = 0.67;
w = -1;
omega_dm = 0.25; omega_wdm = 0.25;
omega_b = 0.05;
h = 0.65;
sigma8 = 0.9;
% omega_m = cospars.omega_m;
% omega_de = cospars.omega_de;
% w = cospars.w0;
% omega_dm = cospars.omega_dm; 
% omega_wdm = omega_dm;
% omega_b = cospars.omega_b;
% h = cospars.h;
% sigma8 = cospars.sigma8;
nu_power = 1.2; % As Bode,Ostriker&Turok 0010389v3

% Small logarithmic interval in M:
dlogM = abs(log(M_vals(2)) - log(M_vals(1)));
dM = dlogM .* M_vals; % [M_sol]

k_vals = logspace(-4,4,500);
Gamma = exp(-2*omega_b*h)*omega_m*h;
pkCDM = pkf(Gamma,sigma8,k_vals);

% S-T CDM
M_vals2 = logspace(0,16,1000);
dndmSTc = massfns_slb(M_vals2,0,omega_b,omega_m,omega_de,w,h,sigma8,k_vals,pkCDM);
% need to integrate!!!!
% Small logarithmic interval in M:
dlogM2 = abs(log(M_vals2(2)) - log(M_vals2(1)));
dM2 = dlogM2 * M_vals2; % [M_sol]
massfnSTc = cumsum(dndmSTc(end:-1:1)'.*dM2(end:-1:1));
c = loglog(M_vals2,massfnSTc(end:-1:1)/h,'b--'); hold on

% S-T WDM
M_vals2 = logspace(0,16,1000);
[tf a pk] = wdm_transfn(1000,omega_wdm,h,k_vals,nu_power,pkCDM,1,sigma8);
dndm2 = massfns_slb(M_vals2,0,omega_b,omega_m,omega_de,w,h,sigma8,k_vals,pk);
% need to integrate!!!!
% Small logarithmic interval in M:
dlogM2 = abs(log(M_vals2(2)) - log(M_vals2(1)));
dM2 = dlogM2 * M_vals2; % [M_sol]
massfnSTw = cumsum(dndm2(end:-1:1)'.*dM2(end:-1:1));
d = loglog(M_vals2,massfnSTw(end:-1:1)/h,'r--'); hold on

set([c d],'LineWidth',1.05)