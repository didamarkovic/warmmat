function [plot2D plot3D] = plotnfwshear(zlens, zsource, Mlens)

% This function returns a 2d and a 3D color plot of the convergence (kappa)
% vs angular position.
%
% function plot = plotnfwshear(zlens, zsource, Mlens)
%
% 26/01/08 KM

% Test: clear; zlens = 0.5; zsource = 1; Mlens = 10^13;

consts
omegas

[dn dnP Ms] = massfns(logspace(5,15,50),zlens,omega_b,omega_m,omega_l,w,h,sigma_8);
[rv_vals rs rho_s] = lensparams(Ms,Mlens,zlens);
[dl ds dls SIGMAcrit] = zparams(zlens,zsource);
theta_v_rad = rv_vals / dl; 
theta_vals = theta_v_rad/100:theta_v_rad/100:theta_v_rad;
max = length(theta_vals);

% 2D plot:
kappa = nfwkappa(zlens,dl,theta_vals,rs,rho_s,SIGMAcrit); % theta input in rad!
figure
plot2D = plot(theta_vals,kappa);
titlestring = ['plot of lensing shear for a lens with M=10^' num2str(log10(Mlens)) ...
    'M_o at z=' num2str(zlens) ' and source at z=' num2str(zsource)]
title(titlestring)
axis([theta_vals(1) theta_vals(length(theta_vals)) kappa(length(kappa)) kappa(1)])
xlabel('theta [arcmin]'); ylabel('kappa');

% 3D color plot:
thetax = -theta_vals(max):theta_vals(max)/32:theta_vals(max);
thetay = -theta_vals(max):theta_vals(max)/32:theta_vals(max);
for x = 1:length(thetax)
kappaC(x,:) = nfwkappa(zlens,dl,sqrt(thetay.^2 + thetax(x)^2),rs,rho_s,SIGMAcrit);
end
figure
plot3D = imagesc(thetax+1.2,thetay+2.4,kappaC); 
titlestring = ['lensing shear map of lens with M=10^' num2str(log10(Mlens)) ...
    'M_o at z=' num2str(zlens) ' and source at z=' num2str(zsource)]
title(titlestring); 
set(gca,'ydir','normal'); 
colorbar