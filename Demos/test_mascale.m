% TEST_MASCALE tests my function mascale.m versus Sarah's massfns_slb.m
% which has M_scale as 3rd output parameter.
%
% version 1
% 23.02.2010 KMarkovic

clear

cospars = setWMAPpars;
z_vals = 0:0.1:5;
k_vals = logspace(-4,4,1000);
M_vals = logspace(0,17,40);

[pk_nlin pk_lin pk_lin_0] = pk_nlin_get(cospars,k_vals,z_vals,'smith2');

M_scale_my = mascale(cospars,z_vals,k_vals,pk_lin_0,1); %%% [M_solar]

M_scale_sarahs = mascale(cospars,z_vals,k_vals,pk_lin_0,0); %%% [M_solar]

figure(1)
semilogy(z_vals,M_scale_my'/cospars.h,'k'); hold on
semilogy(z_vals,M_scale_sarahs'/cospars.h,'b-.')
legend('mine from scratch','Sarahs',3)
title('testing non-linear mass scale'); ylabel('M_*[h^-^1M_0]'); xlabel('redshift, z')