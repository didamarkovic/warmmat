% 'lens_fish_demo'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM

clear

tic

%wdmstep = 0.001;
%m_wdm_fid = 100000;

% Set computational parameters
[valstmp namstmp cospars_fid] = setWMAPpars;

cospars_fid = setpars([valstmp cospars_fid.omega_m/m_wdm_fid],{namstmp{:} 'WDM'});

nzpars = setnzpars([4],{'nbin' 'smith2'});

z_vals = 0.0001:0.02:2; 
exl = log10(20000); 
l_vals = logspace(0,exl,20);

fish.names = {'omega_m'; 'h'; 'sigma8'; 'omega_b'; 'w0'; 'ns'; 'O_m_wdm'};                       
fish.steps = [0.03  0.1  0.09 0.01 0.1 0.1 wdmstep];
                              
% Find the power spectrum and the dCls/dparameter
[cls tmp xx dcls] = lens_ps_get(cospars_fid,nzpars,z_vals,l_vals,nzpars.nbin,fish);

% Get covariance matrices
[C_mat C_vect lens_vals_v] = covmat(l_vals,cls+xx,nzpars.fsky);

% Get Fisher matrices
[fmat errs corrs] = fishmat(fish.names,dcls,C_mat);

% Read out the values of the fiducial parameters for ellipse plotting:
for int = 1:length(fish.names)
    bfp(int) = getfield(cospars_fid,fish.names{int})';
end

% now plot the error ellipses
[h,errvals] = plotellipses_v1(bfp,pinv(fmat),fish.names,['b' 'r' 'g' 'b' '-'],[0.68 0.95],3,20,1:7,0.68);
%set(h,'linewidth',1.2)

m_wdm = m_wdm_fid;
%m_wdm = cospars_fid.omega_m/cospars_fid.O_m_wdm;
deltam = abs(m_wdm^2/cospars_fid.omega_m*(errvals(1)/m_wdm - errvals(7)));
%fprintf(1,' Therefore:\n')
%fprintf(1,'m_wdm is %1.0f +/- %1.0f to 68 perc. conf.\n',m_wdm_fid,deltam)
%fprintf(1,'\n')

%toc

%figure
%mfid = [100 500 1000 2000 5000 10000 100000];
%merr = [2 9 72 440 3619 15000 1000000];
%plot(mfid,merr)
%axis([0 5000 0 5000])

names = fish.names;