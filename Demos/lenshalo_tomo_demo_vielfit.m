% lenshalo_tomo_demo_vielfit
%
% parent: lenshalo_tomo_demo, 15.10.09 KM
%
% 04.07.2011 KM


clear
%clf

tic

cospars = setEUCLIDpars;
cospars.omega_wdm = cospars.omega_dm;

mvals = [0 500 1000];

dz = 0.1; z_vals = 0.0001:dz:2;

l_vals = logspace(1,4,20);
k_vals = logspace(-4,4,1000);

%nzpars = setnzpars([0.1 100*(60*180/pi)^2 2],{'fsky','ng','nbin','vielfit'});
nzpars = setnzpars([8],{'nbin','vielfit'});
%nzpars.zsmin = 0.5;

tmp = min(z_vals):0.01:max(z_vals);
[n_z_mat niz] = get_nofz(nzpars,tmp);
bincents = 1:nzpars.nbin;
for ii = 1:nzpars.nbin
    bincents(ii) = tmp(max(n_z_mat(:,ii))==n_z_mat(:,ii));
end

ib = [5 5 8]; jb = [5 8 8];
zi = bincents(ib);
zj = bincents(jb);

forleg=''; forlegw = '';

C_l_ij = NaN;
for mm = 1:length(mvals)
    cospars.m_wdm = mvals(mm);
    
    if mm==2
        previous_C_l_ij = C_l_ij;
    end
    
    [C_l_ij tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
    
    if 0
        for ii = 1:length(ibin)
            n = noise(ibin(ii),ibin(ii),:);
            [h pf] = plotlens(l_vals,squeeze((C_l_ij(ibin(ii),ibin(ii),:))),col(mm),1); hold on
            loglog(l_vals,squeeze((C_l_ij(ibin(ii),ibin(ii),:))),col(mm)); hold on
            plot(l_vals,pf'.*squeeze(n),[col(mm) '--']);
            plot(l_vals,squeeze(n),[col(mm) '--']);
            axis([10 3000 1e-6 1e-3])
        end
    end
    
    % Ratios:
    if mm~=1
        ra = (C_l_ij)./ (previous_C_l_ij) - 1; % Within 1%
        ratio = (C_l_ij+noise)./ (previous_C_l_ij+noise) - 1; % Within 1%
        ns = - ra .* noise./ (previous_C_l_ij + noise); % Within 1%
    else
        % Work out the error bars for Euclid
        CC = covmat(l_vals,C_l_ij+noise,nzpars.fsky);
    end
    %end
    
    toc
    
    
    %% Plotting
    
    if mm~=1
        
        subplot(1,length(mvals)-1,mm-1)
        
        if mm==2
            %figure(1); clf
            col = 'yrm';
        elseif mm==3
            %figure(2); clf
            col = 'bcg';
        end
        
        semilogx(l_vals,0*l_vals,'k--'); hold on % clf
        %[p pf] = plotlens(l_vals,squeeze(C_l_ij(1,1,:)),'w--',1);
        for ii=1:length(ib)
            
            errors = squeeze(sqrt(CC(ib(ii),jb(ii),ib(ii),jb(ii),:)));
            p_spectr = squeeze(previous_C_l_ij(ib(ii),jb(ii),:));
            spectr = squeeze(C_l_ij(ib(ii),jb(ii),:));
            nois = squeeze(noise(ib(ii),jb(ii),:));
            
            %semilogx(l_vals,squeeze(ratio(ib(ii),jb(ii),:))*100,col(ii));
            %errorbar(l_vals,squeeze(ra(ib(ii),jb(ii),:))*100,errors./spectr,col(ii));
            
            if mm==2
                r(ii) = errorbar(l_vals,squeeze(ratio(ib(ii),jb(ii),:))*100,...
                    errors./spectr,[col(ii) '-']);
                set(r,'linewidth',1.05)
            elseif mm==3
                p(ii) = errorbar(l_vals,squeeze(ratio(ib(ii),jb(ii),:))*100,...
                    errors./spectr,[col(ii) '-']);
                set(p,'linewidth',1.2)
            end
            %semilogx(l_vals,-squeeze(ns(ib(ii),jb(ii),:))*100,[col(ii) ':']);
            
            %errorbar(l_vals,zeros(size(l_vals)),errors./spectr,'kx');
            
            %errorbar(l_vals,pf'.*spectr,pf'.*errors,pf'.*errors,col(ii));
            %p(ii) = plotlens(l_vals,spectr,col(ii));
            %errorbar(l_vals,pf'.*p_spectr,pf'.*errors,pf'.*errors,'k');
            %plotlens(l_vals,spectr+errors,[col(ii) ':']);
            %plotlens(l_vals,spectr-errors,[col(ii) ':']);
            %semilogx(l_vals,pf'.*nois,[col(ii) '--']);
            
            if mm==2
                forlegw{ii} = [num2str(cospars.m_wdm/1000) ...
                    ' keV: bin ' num2str(ib(ii)) '-' num2str(jb(ii))...
                    ', z ~' num2str(zi(ii)) '-' num2str(zj(ii))];
                legend(r,forlegw,3)
                axis([l_vals(1) l_vals(end) -0.5 0.05])
                ylabel('100(C_\kappa_,_i_j^C^D^M-C_\kappa_,_i_j^W^D^M)/C_\kappa_,_i_j^C^D^M')
                xlabel('multipole, l')
            elseif mm==3
                forleg{ii} = [num2str(cospars.m_wdm/1000) ...
                    ' keV: bin ' num2str(ib(ii)) '-' num2str(jb(ii))...
                    ', z ~' num2str(zi(ii)) '-' num2str(zj(ii))];
                legend(p,forleg,3)
                axis([l_vals(1) l_vals(end) -0.5 0.05])
                ylabel('100(C_\kappa_,_i_j^C^D^M-C_\kappa_,_i_j^W^D^M)/C_\kappa_,_i_j^C^D^M')
                xlabel('multipole, l')
            end
        end
        
        
    end
    
end
%legend([r p],[forlegw forleg],3)
%axis([l_vals(1) l_vals(end) -0.5 0.05])
%ylabel('100(C_\kappa_,_i_j^C^D^M-C_\kappa_,_i_j^W^D^M)/C_\kappa_,_i_j^C^D^M')
%xlabel('multipole, l')