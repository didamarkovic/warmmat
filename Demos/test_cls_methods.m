% cls_methods
%
%
%
% This script compares the halo to the nonlin methods of plotting the
% lensing power spectrum.
% 25/08/08 KM
%
% 08.04.2010 KM
% using the pk_nlhalo!
% version 2

clear

tic

l_vals = logspace(log10(2),4,20); %plotfactor = l_vals.*(1+l_vals)/2/pi;
zs = 5; dz = 0.05; z_vals = 0.000001:dz:(zs-0.0000001);

nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'});
nzpars.meth = 'smith2'; %????
%nzpars.meth = 'halo'; %????
%cospars = setpars([0.05 0.65 0.9 0.35 0.65],{'omega_b'; 'h'; 'sigma8'; 'omega_m'; 'omega_de'});
cospars = setWMAP7pars;
% NB here that if you set it to WMAP pars, then you get a bigger
% discrepancy!!!
%cospars.omega_m = 1;

M_vals = logspace(0,16,50);

%nzpars.zs = zs; % Pretend all sources are at z=1! Like in Cooray!
[Cls_nonlin tmp] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,NaN,M_vals);
%Cls_nonlin = lens_ps_get(cospars,nzpars,z_vals,l_vals);

k_vals = logspace(-4,4,1000);
Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals);

%[pk_vals tmp pk_lin] = pk_nlin_get(cospars,k_vals,z_vals,'halo',NaN,M_vals);
%[Cls_halo noise plotfactor] = lens_ps(cospars,z_vals,l_vals,k_vals,pk_vals,nzpars);
[Cls_halo_tmp pf noise Clp_tmp Clc_tmp] = lenshalo_tomo(cospars,z_vals,l_vals,k_vals,pk_lin_0,nzpars,M_vals,NaN,NaN,3,0);

%%
ibin = 5; % Which bin to plot.
jbin = 5; % clear nbin

Cls_halo = squeeze(Cls_halo_tmp(ibin,jbin,:))';
Clp = squeeze(Clp_tmp(ibin,jbin,:))';
Clc = squeeze(Clc_tmp(ibin,jbin,:))';
plotfactor = squeeze(pf(ibin,jbin,:))';

%figure % 
clf
p1 = loglog(l_vals,plotfactor.*squeeze(Cls_nonlin(ibin,jbin,:))','k'); hold on 
lw = 2; fs = 12; %set(gca,'fontsize',fs,'linewidth',lw);
%loglog(l_vals,plotfactor.*squeeze(tmp)','g.'); hold on
%p1 = loglog(l_vals,cospars.h*plotfactor.*squeeze(Cls_nonlin)','b'); hold on
p2 = loglog(l_vals,plotfactor.*Cls_halo,'b--'); hold on
loglog(l_vals,plotfactor.*Clp,'b-.'); 
loglog(l_vals,plotfactor.*Clc,'b-.'); 
axis([l_vals(1) max(l_vals) 1e-9 8e-3])
%title('Weak Lensing Power Spectrum plotting methods, zs = 2');
xlabel('multipole l','fontsize',fs); 
ylabel('l(l+1)C_l/2\pi','fontsize',fs);
set([p1 p2],'linewidth',lw);
legend([p1 p2], 'with Smith et al.','with full halo model',2)
%legend('with Smith et al.','with 2D halo model','with 3D halo model',2)
%saveas(gcf,'Cls_method_cf_1109...','bmp')

%save 'wlps.mat'

%%

% Ratio:
if 0
    figure
    ratio = real(Cls_halo./squeeze(Cls_nonlin(nbin,nbin,:))');
    ratio1H = real(Clp./squeeze(Cls_nonlin(nbin,nbin,:))');
    ratio2H = real(Clc./squeeze(Cls_nonlin(nbin,nbin,:))');
    p3 = semilogx(l_vals,ones(size(l_vals)),'c--'); hold on
    p4 = semilogx(l_vals,ratio,'k');
    p5 = semilogx(l_vals,ratio1H,'k.--');
    p6 = semilogx(l_vals,ratio2H,'kx--');
    %axis([l_vals(1) max(l_vals) 0 1.5])
    title('Weak Lensing Power Spectrum plotting methods - ratio halo/t&j');
    xlabel('multipole l','fontsize',fs);
    ylabel('l(l+1)C_l/2\pi','fontsize',fs);
    set([p3 p4],'linewidth',lw);
    %title('RATIO HALO/T&J with a0 = 10.3')
    %saveas(gcf,'Cls_methods_cf_ratio_1109...','bmp')
end

toc