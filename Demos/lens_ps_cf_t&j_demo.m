% 'lens_ps_demo'
% 'lens_ps_tomo_demo2eeek'
% Trying to reproduce Fig. 2 in Takada&Jain: astro-ph/0310125
% 30/07/08 KM
% The spectra are 30% to small (not multplied by h, I tried...) i.e. multiply by 1.3.

clear all
tic

nbin = 2;

k_vals = logspace(-2.5,4, 1000); % Need many values of k!
z_vals = 0.001:0.05:5;
l_vals = logspace(1,3.4771,500); plotfactor = l_vals .* (1+l_vals) /2/pi;

% Cosmology
%m_wdm = 0;
cospars = setpars([0.047 0.7],{'omega_b'; 'omega_de'});

% Constants
%consts = get_standard_pars;
consts.arcminsq_per_sr = (60*180/pi)^2;
consts.sky_deg2 = 4*pi*(180/pi)^2;

% Survey specs
nzpars.nbin = nbin;
nzpars.ng = 100*consts.arcminsq_per_sr; 
% no. of galaxies per unit steradian!!! - only need for shotnoise
nzpars.z_0 = 0.5;%1.13;%0.9/1.412; % Does Sarah  - ?!
nzpars.alpha = 2; 
nzpars.beta = 1; % fitting parameters
nzpars.deltaz = 0; nzpars.Deltaz = 0; nzpars.fcat = 0; % photo-z errors
nzpars.type = 'equal_tophat'; nzpars.verb = 0;
nzpars.fsky = 0.1;%20000/consts.sky_deg2; 
nzpars.sigma_gamma = 0.4;%0.16; % survey

nzpars.z_cuts = 1.13;


% Matter PS
pk_vals = pk_nlin_get(cospars,k_vals,z_vals,'p&d');

% Lensing PS with tomography
[lens_ps_vals noisemat] = ...
                lens_ps(cospars,z_vals,l_vals,k_vals,pk_vals,nzpars);
lens_ps_obs = lens_ps_vals + noisemat;


% Plot
figure;
cols = ['f';'r';'g';'b';'k';'m';'y';'c';'k';'b';'m';'r';'g';'y';'c'];
for int = 1:nbin
    for jnt = int:nbin
        pspktm = plotfactor.*squeeze(lens_ps_vals(int,jnt,:))';
        h = loglog(l_vals,pspktm,cols(jnt+int));  hold on
        set(h,'linewidth',2)
        %pspktm = plotfactor.*squeeze(lens_ps_obs(int,jnt,:))';
        %g = loglog(l_vals,pspktm,[cols(jnt+int) '--']);
        if jnt==2 && int==2
        g = loglog(l_vals,plotfactor.*squeeze(noisemat(int,int,:))','c--');
        set(g,'linewidth',1.5)
        end
    end
end
axis([l_vals(1) max(l_vals) 1e-6 1e-3])
xlabel('l'); ylabel('C^\kappa_i_j(l) l(l+1) /2\pi')
title('convergence power spectrum tomography - c.f. fig2 Takada&Jain')
%saveas(gcf,'Lensing_tomography_zmax5_14-08...','bmp');
legend('bin 11','bin 12','bin 22','noise 22',4)
set(gca,'fontsize',15,'linewidth',0.7);
%ax=get(gcf,'CurrentAxes');
%set(ax,'fontsize',15,'linewidth',0.7)


toc