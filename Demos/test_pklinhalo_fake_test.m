% further test of Cooray's halo way: pklinhalo_fake_test.m
%
% 27.10.09 KMarkovic

clear

tic

%z_vals = 0.1:0.4:2;
%z_vals = [0.0001 0.1 1];
%z_vals = 0.00001; %%% [no units]
z_vals = 0.5; %%% [no units]
%[tmp tmp cospars] = setWMAPpars; %%% [no units]
cospars = setWMAPpars; %%% [no units]
l_vals = 2*logspace(0,5,20); %%% [no units !!]
%l_vals = 0;
%cospars.h = 0.5;
%cospars.omega_m = 0.5;

% First the constants:
consts %%% [lots of units!!!]
%Dh = c_km/100; % unit fun:     [km/s]   /    [km/s/Mpc]    =    [Mpc/h]
Dh = c/H0; %%% [meters]


% Now the distances and such matters:
X_tmp = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000); %%% [Dh]

X = X_tmp * Dh; %%% [meters]


% Now the matter power spectrum - in:
k_vals = logspace(-4,4,1000); %%% [h/Mpc]
Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h; %%% [no units]

pk_lin_real_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run); %%% [Mpc^3/h^3] I guess...?

% Now let's draw the non-linear power spectrum just to compare:
[pk_nlin pk_lin_real_0] = pk_nlin_get(cospars,k_vals,z_vals,'smith2'); %%% [Mpc^3/h^3] I guess...?

% Now the mass integral for:
M_vals = logspace(10,15,50); %%% [M_solar]

M_scale = mascale(cospars,z_vals,k_vals,pk_lin_real_0); %%% [M_solar]
%M_scale = 1e14;

[rv_vals theta_v conc rho_s] = haloparams(cospars.omega_m,...
        cospars.h,M_scale,M_vals,z_vals,X);%%% [meters][no units][no units][kg/m^3]
    
 %theta_v = theta_v*cospars.h^2;
    
 rho_mean_0 = rho_crit_0 * cospars.omega_m; %%%[kg/m^3]
    
 
% First Sigma_l
SIGMAcrit = ones(length(z_vals),1);%%%[no units here!]
%SIGMA_l = fourierkappa(z_vals, X, M_vals, l_vals,theta_v, rv_vals, conc, rho_s, SIGMAcrit,200,rho_mean_0);
SIGMA_l = fourierkappa(z_vals, X, M_vals, l_vals,theta_v, rv_vals, conc, rho_s, SIGMAcrit,200);
%SIGMA_l = SIGMA_l * cospars.h^2;
%%%[kg/m^2]

% Now mass functions:
[dndm_unnorm dndms_ps M_stars sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_real_0); 
%%% [(Mpc/h)^-3/(M_solar/h)] = [h^4/Mpc^3/M_solar] [same] [M_solar/h][unitless!]

dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]


% Bias:

% Okay lets try out this weird new delta_c2 (from Henry 2000):
delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units]

a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]

IntM_C = zeros(length(z_vals),length(l_vals));
IntM_P = IntM_C;
tmp_pk_C = zeros(length(z_vals),length(k_vals));
tmp_pk_P = tmp_pk_C;
dndm = zeros(size(dndm_unnorm));
DELTA_C = tmp_pk_C;
DELTA_P = tmp_pk_C;
DELTA_nlin = tmp_pk_C;

for zint = 1:length(z_vals)

    
    % Need to normalise the mass functions (units: (Mpc/h)^-3 / (M_solar/h) )!
    %norm = sum( dndm_unnorm(:,zint)' * cospars.h^4.* dM .* M_vals*M_solar / rho_mean_0 );
    
    rho_mean_Mpc = rho_crit_0_Mpc * cospars.omega_m*(1+z_vals(zint))^3; %%%[M_sol/Mpc^3]
    rho_mean = rho_crit_0 * cospars.omega_m*(1+z_vals(zint))^3; %%%[kg/m^3]
    
    norm = sum( dndm_unnorm(:,zint)' * cospars.h^4.* dM .* M_vals/ rho_mean_Mpc); %%% [no units]
    %norm = sum( dndm_unnorm(:,zint)' .* dM .* M_vals/ rho_mean_Mpc); %%% [no units]
 
    %dndm(:,zint) = dndm_unnorm(:,zint) / norm / Mpc^3 * cospars.h^4;
    
    dndm(:,zint) = dndm_unnorm(:,zint) / norm * cospars.h^4; %%%[Mpc^-3 M_solar^-1]
    %dndm(:,zint) = dndm_unnorm(:,zint) / norm; %%%[h^4 Mpc^-3 M_solar^-1]

    
    % Now find the bias:
    nu = delta_c(zint) ./ sigma(:,zint).^2 ; %%% [unitless]

    %bias_unnorm(:,zint) = 1 + (  a*nu(:,zint).^2 - 1 ) ./ delta_c(zint) + ...
    %   2*p ./ (  delta_c(zint) * ( 1 + ( a * nu(:,zint)).^p )  );
    % From Seljak 0001493v1 (after equn 9):
    % This is totally wrong, no? Should use equn 6 from that paper:
    bias_unnorm =  1  +  ( nu - 1 )/delta_c(zint)  +  2*p./( 1 + (a*nu).^p );
    
    % Need to normalise the bias:
    %norm = sum( dndm(:,zint)' .* dM .* M_vals*M_solar   / rho_mean_0 .* bias_unnorm' );
    norm = sum( dndm(:,zint)'  .* dM .* M_vals  / rho_mean_Mpc .* bias_unnorm' ); %%%[no units]
    %norm = sum( dndm(:,zint)' .* dM .* M_vals  / rho_mean_Mpc .* bias_unnorm' ); %%%[no units]
    bias = bias_unnorm / norm; %%%[no units]
    %bias = 1;

 
    % Now the mass integral:
    for lint = 1:length(l_vals)

        dIntM_C = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint) .* bias; %%%[Mpc^-3 kg m^-2]

        IntM_C(zint,lint) = sum(dIntM_C) / Mpc^3; %%%[kg m^-5]
        
        dIntM_P = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint).^2; %%%[Mpc^-3 kg^2 m^-4]

        IntM_P(zint,lint) = sum(dIntM_P)/ Mpc^3; %%%[kg^2 m^-7]

    end

    %tmp_Correlations = IntM_C(zint,:) .* X(zint)^2 / rho_mean_0; %%%[no units]
    
    tmp_Correlations = IntM_C(zint,:) .* X(zint)^2 / rho_mean(zint); %%%[no units]
    
    %tmp_Poisson = IntM_P(zint,:) .* X(zint)^4 / rho_mean_0^2;%%%[m^3]
    
    tmp_Poisson = IntM_P(zint,:) .* X(zint)^4 / rho_mean(zint)^2;%%%[m^3]
    
    % The above makes no difference, clearly, at z = 0!
    
    tmp_intrpl_C = interp1(l_vals/X(zint)*Mpc/cospars.h,tmp_Correlations,k_vals);%%%[no units]

    tmp_intrpl_P = interp1(l_vals/X(zint)*Mpc/cospars.h,tmp_Poisson,k_vals);%%%[m^3]

    
    tmp_pk_C(zint,:) = tmp_intrpl_C.^2 .* pk_lin_real_0; %%%[Mpc^3 h^-3]
    
    tmp_pk_P(zint,:) = tmp_intrpl_P; %%% [m^3]

    %semilogx(l_vals/X(zint)*Mpc/cospars.h,tmp); hold on

    DELTA_C(zint,:) = k_vals.^3.*tmp_pk_C(zint,:) /2/pi; %%%[no units]
    DELTA_P(zint,:) = k_vals.^3.*tmp_pk_P(zint,:) /2/pi  * (cospars.h/Mpc)^3; %%%[no units]
    
   DELTA_nlin(zint,:) = k_vals.^3 .*pk_nlin(zint,:) /2/pi; %%%[no units]
    
end

%pk_lin_out  = pk_lin_out * (Mpc/cospars.h)^(-3);

%loglog(k_vals([1 50 100*(1:10)]),tmpt)

% This should be = 1!:
%semilogx(k_vals,pk_lin_out);

%figure
%fact = cospars.h^(-6);
fact = 1;
l=loglog(k_vals,fact*DELTA_C,'r'); hold on
m=loglog(k_vals,fact*DELTA_P,'b'); hold on
n=loglog(k_vals,fact*DELTA_P+fact*DELTA_C,'g'); hold on
o=loglog(k_vals,k_vals.^3.*pk_lin_real_0/2/pi,'k:');
axis([1e-2 9e2 1e-2 9e4])
p=loglog(k_vals,DELTA_nlin,'k--'); 
set([l m n o p],'linewidth',1.2);
ylabel('\Delta^2(k)'); xlabel('k[h/Mpc]')
legend([l m o p],'2H' ,'1H' ,'P_l_i_n(k)' ,'P_n_l_i_n(k)',2)
title('z = 1')

toc

% dtheta = 0.01;
% theta = 0:dtheta:2;
% l_ln = 10;
% 
% figure(2)
% Bessels = besselj(0,(l_ln+0.5)*theta);
% plot(theta,Bessels,'k'); hold on
% l_s  = 100;
% figure(3)
% integral = cumsum(Bessels*dtheta);
% plot(theta,integral,'k'); hold on
% 
% figure(2)
% Bessels = besselj(0,(l_s+0.5)*theta);
% plot(theta,Bessels,'r')
% l_nl = 1000;
% figure(3)
% integral = cumsum(Bessels*dtheta);
% plot(theta,integral,'r')
% 
% figure(2)
% Bessels = besselj(0,(l_nl+0.5)*theta);
% plot(theta,Bessels,'b')
% legend('lin','trans','nonlin')
% figure(3)
% integral = cumsum(Bessels*dtheta);
% plot(theta,integral,'b')
% 
% legend('lin','trans','nonlin')
%z = 0.00001;
%dist = Ds_wconst(0,z,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*c_km/100; %%% [Mpc/h]
%k_ln = l_ln/dist
%k_s = l_s/dist
%k_nl = l_nl/dist

%%%%% PART II - should be one at z = 0!

% zs = 9;
% Dh = c_km/H0_Mpc; % in Mpc
% chilens = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
% chisource = Ds_wconst(0,zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000)*Dh;
% chiLS = chisource-chilens;
% 
% SIGMAcrit_tmp = (c_km^2) .* chisource ./ ( 4*pi*G_Mpc .* chilens.* chiLS ) ./(1+z_vals);
% % actually units of this one her are: km^2 M_sol / Mpc^4
% SIGMAcrit = SIGMAcrit_tmp * 1000^2 * M_solar /Mpc^4;
% 
% constant = 3/2*cospars.omega_m*H0^2/c^2;
% %constant = 4*pi*G*rho_mean_0/c^2;
% 
% otherside = X * sum(dM'.*dndm.*bias.*SIGMA_l(:,1,1)/Mpc^3)/SIGMAcrit;
% 
% only_for_z0 = constant/otherside
% 
% %%%% PART III
 for lint = 1:length(l_vals)
     shouldbeone(lint) = X ^2* sum(dM'.*dndm.*bias.*SIGMA_l(:,1,lint)/Mpc^3)/rho_mean;
 end
 figure
 semilogx(l_vals,shouldbeone.^2); hold on
 semilogx(k_vals*X(zint)/Mpc*cospars.h,DELTA_C./(k_vals.^3.*pk_lin_real_0/2/pi),'k')
 xlabel('l, multipole')