% MASSFN_WDM_DEMO
% compare this to Smith & Markovic, arXiv:1103.2134.
% 18.05.2011 KMarkovic

clear

M_vals = 9*logspace(7,15,20);
z_vals = 0;
smoo = 0.5;

% Built object of cosmological parameters with values of WMAP7 + WDM
cospars = setEUCLIDpars;
cospars.m_wdm = [1000 500]; % 1 & 0.5 keV
cospars.omega_wdm = cospars.omega_dm;

% Calculate the free-streaming mass of chosen WDM model
%n00 = 150;
Mfs = freestreaming_comov(cospars,cospars.m_wdm);%,n00);

% Get Sheth-Tormen mass functions
cospars_slb = convertpars(cospars);
Gamma = exp(-2 * cospars.omega_b * cospars.h) *cospars.omega_m * cospars.h;
k_vals = logspace(-4,4,2000); 
pk_vals = pkf(Gamma, cospars.sigma8, k_vals);
[dndm_un M_scale_tmp bias] = massfn_new(k_vals,pk_vals,M_vals,z_vals,cospars_slb);
%[dndm_un tmp M_scale_tmp sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
%   cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
%   cospars.sigma8);

% Calculate WDM effect
dndm1 = massfn_wdm(M_vals,dndm_un',Mfs(1),smoo);
dndm2 = massfn_wdm(M_vals,dndm_un',Mfs(2),smoo);

% Now plot
loglog(M_vals,dndm_un'.*M_vals','k-'); hold on
loglog(M_vals,dndm1.*M_vals','m.');
loglog(M_vals,dndm2.*M_vals','r.');
%loglog(M_vals,flipud(cumsum(10^9*flipud(dndm_un))),'k-'); hold on
%loglog(M_vals,flipud(cumsum(10^9*flipud(dndm1))),'b--');
%loglog(M_vals,flipud(cumsum(10^9*flipud(dndm2))),'g--');
axis([1e8 1e15 1e-7 1e2])
set(gca,'XTick',logspace(8,15,8))

%% Test the effect of the error function
clf;

x=0:0.1:10;
smoo = [0.00000001 0.2 0.5 1 5 50];

for ii = 1:length(smoo)
    plot(x,erf(x/smoo(ii))); hold on
    plot(x,x/smoo(ii),'k:')
end
axis([-0.1 max(x) 0 1.01])