% Just converting the dark matter particle mass into temperature, radii etc
% 'freestreaming_demo.m'
%
% 10/03/08 KMarkovic

clear

z = 0.5;
f=1;
%omegas; consts;
Tcmb = 2.726;

omega_m = 0.3;
omega_de = 0.7;
h = 0.7;
omega_dm = 0.25;
cospars = setpars([ omega_m    omega_de    omega_dm    h    omega_dm],...
                  {'omega_m'; 'omega_de'; 'omega_dm'; 'h'; 'omega_wdm'});
n00 = 200;

m_vals = 300:100:8300;

% Let's write a table into a file:
fid = fopen('freestreaming_table_2509.dat','w');
fprintf(fid,'m_x [keV]\t');
fprintf(fid,'g*\t');
fprintf(fid,'T_x [K]\t');
fprintf(fid,'m_nu [keV]\t');
fprintf(fid,'k_fs\t');
fprintf(fid,'R_fs [Mpc]\t');
fprintf(fid,'M_fs [M_solar]\n');
for mint = 1:length(m_vals)

    m_x = m_vals(mint);
    [gstar Tx_Tnu m_nu N_nu Tx] = degoffree(m_x,f,omega_dm,h,Tcmb);
    %[M Rvir kfs] = freestreaming(m_x,z,f,h);
    [M Rvir kfs] = freestreaming_comov(cospars,m_x,n00);

    fprintf(fid,'%.1f\t',m_x/1000);
    fprintf(fid,'%.3f\t',gstar);
    fprintf(fid,'%.3f\t',Tx);
    fprintf(fid,'%.2f\t',m_nu/1000);
    fprintf(fid,'%.3f\t',kfs);
    fprintf(fid,'%.3f\t',Rvir);
    fprintf(fid,'%.1e\n',M);
    
    %fprintf(1,'\n')
    %fprintf(1,'For a warm dark matter particle with a mass of %.1f keV:\n',m_x/1000)
    %fprintf(1,'- g*(T_D) = %.3f\n- T_x = %.3f K\n',gstar,Tx)
    %fprintf(1,'- corresponding sterile neutrino mass, m_nu = %.2f keV\n',m_nu/1000)
    %fprintf(1,'- with a free-streaming scale, k = %.3f\n',kfs)
    %fprintf(1,'- with the free-streaming length of R_fs = %.3f Mpc\n',Rvir)
    %fprintf(1,'- corresponding to the radius of a halo with M_fs = %.1e M_solar\n',M)
    %fprintf(1,'\n')
    
    gstars(mint) = gstar;
    Txs(mint) = Tx;
    m_nus(mint) = m_nu;
    kfss(mint) = kfs;
    Ms(mint) = M;
    Rvirs(mint) = Rvir;
end
fclose(fid);
% This is 2 orders of magnitue bigger than I found last summer!

% Could do graphs for all these things!
figure; a = plot(m_vals/1000,gstars);xlabel('m_x [keV]');ylabel('g*(T_D)')
figure; b = plot(m_vals/1000,Txs);xlabel('m_x [keV]');ylabel('temperature of dark matter [K]')
figure; c = plot(m_vals/1000,m_nus);xlabel('m_x [keV]');ylabel('m_\nu')
figure; d = semilogy(m_vals/1000,kfss);xlabel('m_x [keV]');ylabel('free-streaming scale')
figure; e = semilogy(m_vals/1000,Ms);xlabel('m_x [keV]');ylabel('halo mass suppressed')
figure; f = plot(m_vals/1000,Rvirs);xlabel('m_x [keV]');ylabel('free-straming length')
set([a b c d e f],'linewidth',1.5')