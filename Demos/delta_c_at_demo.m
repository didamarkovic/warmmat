% delta_c_alt_demo
%
% Testing fitting function from astro-ph/9907024, Sheth, Mo & Tormen (1999)
%
% So in this version I changed delta_0 -> delta_0/growth_factor. And added
% the closed universe delta_c. Clearly need growth factor division to get
% real z dependence!!! See this by overplotting for different z!
% version 1.1
% 23.02.2010 KM
% version 1 - hmmmm...
% 01.02.2010 KMarkovic

clear
cospars = setPlanckpars;
z_vals = 0.005;


% First spherical
delta_spher_crit = delta_c_alt(z_vals,cospars.omega_m); % varies very little with redshift


% For closed:
[delta_vals_tmp, z_vals_tmp] = growth_wconst(cospars.omega_m,cospars.omega_de,cospars.w0,1000);
growth_vals_tmp = delta_vals_tmp ./ ( max(delta_vals_tmp) );
growth = interp1(z_vals_tmp, growth_vals_tmp, z_vals);
delta_c_closed = 1.686/growth;

% Find Pks & sigma(R):
M_vals = logspace(10,16,50);
kvals = logspace(-4,4,1000);
GAMMA = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
Pks = pkf(GAMMA,cospars.sigma8,kvals);
consts
Rs = haloparams(cospars,0,M_vals,z_vals,0)/Mpc;
for int = 1:length(Rs)
    sigma(int) = sigmar_frompk(kvals, Pks, Rs(int)) ;
end

% Now from ellipsoidal collapse:
beta = 0.47;
gamma = 0.615;
delta_ell_crit = delta_spher_crit *( 1 + beta*( sigma.^2 / delta_spher_crit^2 ).^gamma );

%figure
a = semilogx(M_vals,delta_spher_crit*ones(size(M_vals)),'k--'); hold on
b = semilogx(M_vals,delta_c_closed*ones(size(M_vals)),'b-.'); hold on
c = semilogx(M_vals,delta_ell_crit,'r');
axis([M_vals(1) max(M_vals) 1 max(delta_ell_crit)])%min(delta_spher_crit)*0.9 max(delta_ell_crit)*1.1])
xlabel('M'); ylabel('\delta_c')
legend([a b c],'spherical','spherical and closed','ellipsoidal collapse')

%%

z_vals = 0:0.1:1;
delta_c_flat = delta_c_alt(z_vals,cospars.omega_m); % varies very little with redshift

figure
d = plot(z_vals,delta_c_flat,'k'); hold on
e = plot(z_vals,1.686*ones(size(z_vals)),'b--'); hold on
axis([z_vals(1) max(z_vals) 1.65 1.7])%min(delta_spher_crit)*0.9 max(delta_ell_crit)*1.1])
xlabel('z'); ylabel('\delta_c')
legend([d e], 'flat','closed')
