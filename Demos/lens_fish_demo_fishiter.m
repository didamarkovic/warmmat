% 'chisq_test'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM
% version 1.2
% Using 1/m. Finding chisq too!
% 16.04.2010 KM
% version 1.3
% Testing new function chisq.m! -> chisq_test.m
% 16.04.2010 KM
% version 1.4
% plotting error of m_wdm from fish and probability from chisq
% 16.04.2010 KM
% Now doing a massive chisq run for all parameters!
% Called lens_fish_tomo_demo_chifish.m
% 14.05.2010 KM
% version 2
% Now going to use fisher only - do iterations to find the right step-size
% to fit gaussian to the chisq curve.
% Called lens_fish_tomo_demo_fishiter.m
% 20.05.2010 KM

clear %clc

tic

l_max = 4;
linecolor = 'k';

suppressh = 0;

% Set computational parameters
cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;
cospars.m_wdm = 10^8;

nzpars = setnzpars([2 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});

z_vals = 0.0001:0.5:5;
l_vals = 2*logspace(1,l_max,20);
%M_vals = logspace(0,16,50);

% NOTE THAT THE CODE BREAKS DOWN AT m = 500 eV!!!
err = 0.0001; % approx 1-sigma from 1d chisq
nosigma = 3;

fish.names = {'invm'};
fish.steps = [err]; % 1/m step must be first!!!

c = clock;
startstring = ['- Starting at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) ' of April 2010.'];
filestring = ['fishiter-' num2str(c(1)) '-' num2str(c(2)) '-' num2str(c(3)) '-' num2str(c(4)) 'h.txt'];

%% write file
dlmwrite(filestring,startstring,'')
dlmwrite(filestring,['- m_wdm = ' num2str(cospars.m_wdm)],'-append','delimiter','')
dlmwrite(filestring,['- no.bins = ' num2str(nzpars.nbin)],'-append','delimiter','')
dlmwrite(filestring,['- no.sigma = ' num2str(nosigma)],'-append','delimiter','')
dlmwrite(filestring,['- ' fish.names],'-append','delimiter','')
dlmwrite(filestring,fish.steps,'-append','delimiter',' ')
dlmwrite(filestring,'- step & error','-append','delimiter','')
dlmwrite(filestring,' ','-append','delimiter',' ')

%% Iterate
add = 0;%.0043; % title(['add=' num2str(add)])
maxiterations = 50;
index = 0;
iteration = zeros(maxiterations,2);
step = 0;
while abs(step-err)>10^(-10) && index <= maxiterations
    
    %if nosigma*err<=0.002
        step = nosigma*err+add;
    %else
    %    step = 0.002;
    %end
    
    fish.steps = [step fish.steps(2:length(fish.steps))];
    
    % Find the power spectrum and the dCls/dparameter
    if suppressh
        [CLS CLSh xx dcls] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
            nzpars.nbin,fish,M_vals);
        [Ch_mat Ch_vect lensh_vals_v] = covmat(l_vals,clsh+xx,nzpars.fsky);
        [fmath errsh corrsh] = fishmat(fish.names,dclsh,Ch_mat);
        
    else
        [CLS nn xx dcls] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,fish);
    end
    
    % Get covariance matrices
    [C_mat C_vect lens_vals_v] = covmat(l_vals,CLS+xx,nzpars.fsky);
    
    % Get Fisher matrices
    [fmat err corrs] = fishmat(fish.names,dcls,C_mat);
    
    dlmwrite(filestring,[step nosigma*err],'-append','delimiter',' ')
    
    index = index + 1;
    
    iteration(index,:) = [step nosigma*err];
    
end

toc

%% Ploting the iteration

x = iteration(:,1);
y = iteration(:,2);

if 1
    figure(1)
    plot(x,x,'k--'); hold on
    plot(x,x-add,'k:'); hold on
    plot(x,y,'c:'); hold on
    plot(x,y,'k.'); hold on
    plot(x(1),y(1),'ro');
    plot(x(1),y(1),'rx');
    xlabel('step'); ylabel([num2str(nosigma) '-sigma']);
    axis([0 max(max(iteration)) min(min(iteration)) max(max(iteration))])
    %axis([0 0.01 0 0.01])
%else
    figure(2)
    plot(y,'k:.'); hold on
    xlabel('number of iterations'); ylabel([num2str(nosigma) '-sigma']);
    axis([0 maxiterations min(y) max(y)])
end

return

%% interpolate
interptype = 'nearest';

[xx,inds] = sort(x);
yy = y(inds);
bingo = interp1(yy-xx,xx,0,interptype)

manyx = linspace(min(x),max(x),1000);
fit = interp1(yy-xx,xx,manyx,interptype);

figure(1)
%plot(xx,yy,'y--')
plot(manyx,fit);

return

%% sigma vs step
tic
index = 0;
steps = linspace(0.0001,0.01,100);
sigmas = zeros(size(steps));
for step = steps
     
    fish.steps = [step fish.steps(2:length(fish.steps))];
    
    % Find the power spectrum and the dCls/dparameter
    if suppressh
        [CLS CLSh xx dcls] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
            nzpars.nbin,fish,M_vals);
        [Ch_mat Ch_vect lensh_vals_v] = covmat(l_vals,clsh+xx,nzpars.fsky);
        [fmath errsh corrsh] = fishmat(fish.names,dclsh,Ch_mat);
        
    else
        [CLS nn xx dcls] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,fish);
    end
    
    % Get covariance matrices
    [C_mat C_vect lens_vals_v] = covmat(l_vals,CLS+xx,nzpars.fsky);
    
    % Get Fisher matrices
    [fmat err corrs] = fishmat(fish.names,dcls,C_mat);
            
    index = index + 1;
    sigmas(index) = err;    
end
toc
%% plot
figure(5)
plot(steps,sigmas,'k'); hold on
%plot(steps,(exp(1./steps/2000000)-1.00015),'g--'); hold on
plot(steps,(exp(1./steps/5000)-1)/700,'g--'); hold on
xlabel('step'); ylabel('\sigma')
%plot(1/cospars.m_wdm,ones(5,1),'r.')
%plot(steps,steps,'k--')
axis([0 0.01 0 0.01])