% test_sigma_result
% Plotting NFW projection I calculated vs. Takada&Jain (2003)
%
% 26.01.2010 KM

% take x > 1 & c = 2:

clear;figure;clf

c = 2;
x = 0.001:0.01:0.999999;

theta = sqrt(c^2-x.^2)./sqrt(1-x.^2);

myF = atanh(1./theta)-atanh(1./theta/c);
tjF = -acosh((x.^2+c)./(x.*(1+c)));
% Mine is more likely to diverge!!!!!

plot(x,myF,'b'); hold on
plot(x, tjF,'r--');