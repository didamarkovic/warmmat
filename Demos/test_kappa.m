% 'test_kappa.m'
%
% Plotting the solution to the integral over the line-of-sight of the NFW
% desnity profile. Solution I worked out is possible, but I use the one
% found in Takada & Jain 0209167.
% version 1
% 25.02.2010 KMarkovic

clear

concs = [0.2 1 2 10 100];

figure(1); clf(1)
line([1 1],[0.0000001 10],'Color','r')

linecol = 'kbcmgyk';

for int = 1:length(concs)

    conc = concs(int);

    x = linspace(0,conc,1000);

    % x == 0
    i0 = x==0;
    SIGMA(i0) = 0; % Ignore this due to divergence that is unimportant once Fourier integrated.

    i1 = x<1&x~=0;% sum(i1)
    SIGMA(i1) = -1./( (1+conc).*(1-x(i1).^2).^(3/2) ) .* ...
        (sqrt( (conc^2-x(i1).^2).*(1 - x(i1).^2) ) - (1+conc)*acosh( (x(i1).^2 + conc)./(x(i1).*(1+conc) ) ));

    % x == 1
    i2 = x==1; % sum(i2)
    SIGMA(i2) = (conc+2).*sqrt(conc-1)./(3*(conc+1).^(3/2));

    % x >1
    i3 = x>1; % sum(i3)
    SIGMA(i3) = 1./( (1+conc)*(-1+x(i3).^2).^(3/2) ) .*...
        (sqrt( (conc^2-x(i3).^2).*(x(i3).^2-1) ) - (1+conc)*acos( (x(i3).^2 + conc)./(x(i3).*(1+conc) ) ));

    % x == c
    i4 = x==conc;
    SIGMA(i4) = 0;

    loglog(x,real(SIGMA),linecol(int)); hold on
    %loglog(x,imag(SIGMA),linecol(int)); hold on
    %line([conc conc],[0.00000001 10],'Color',linecol(int))
    lstr{int} = ['concentration parameter = ' num2str(conc,'%10.1f') ];

end

plot(1,logspace(-6,1,100),'r')
axis([0 max(x) 0.00001 10])
xlabel('x'); ylabel('G(x)')
legend(lstr{1},lstr{2},lstr{3},lstr{4},lstr{5},'x=1',3)