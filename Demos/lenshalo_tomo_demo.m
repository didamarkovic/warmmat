% lenshalo_tomo_demo
%
% 16/09/08 KM
% 15.10.09 KM


clear

tic

%sethalopath;setnonlinpath

cospars = setWMAPpars; 
dz = 0.1; z_vals = 0.0001:dz:1;
l_vals = logspace(1,4,20);
k_vals = logspace(-4,4,1000);
 %Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
%pk = pkf(Gamma,cospars.sigma8,k_vals);
nzpars = setnzpars([2 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'});
M_vals = logspace(0,16,50);


%zs = 1;
%nzpars.zs = zs;
nzpars.meth = 'smith2';

% Halo-tomo
%[C_l_ij pf xx] = lenshalo_tomo(cospars,z_vals,...
%                                   l_vals,k_vals,pk,nzpars,M_vals);
%figure; loglog(l_vals,squeeze(pf(1,1,:).*(C_l_ij(1,1,:)+xx(1,1,:))),'k'); hold on
%loglog(l_vals,squeeze(pf(1,1,:).*(C_l_ij(1,2,:)+xx(1,2,:))),'k'); 
%loglog(l_vals,squeeze(pf(1,1,:).*(C_l_ij(2,2,:)+xx(2,2,:))),'k'); 
%figure; 
%loglog(l_vals,squeeze(pf(1,1,:).*(C_l_ij(1,1,:))),'k'); hold on
%loglog(l_vals,squeeze(pf(1,1,:).*(C_l_ij(1,2,:))),'k'); 
%h = loglog(l_vals,squeeze(pf(1,1,:).*(C_l_ij(2,2,:))),'k'); 

% T&J
%[Cls_nonlin C_l_ij yy] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin);
[Cls_nonlin C_l_ij] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,0,M_vals);
%loglog(l_vals,squeeze(pf(1,1,:).*(Cls_nonlin(1,1,:)+yy(1,1,:))),'b--'); 
%loglog(l_vals,squeeze(pf(1,1,:).*(Cls_nonlin(1,2,:)+yy(1,2,:))),'b--'); 
%loglog(l_vals,squeeze(pf(1,1,:).*(Cls_nonlin(2,2,:)+yy(2,2,:))),'b--'); 
%loglog(l_vals,squeeze(pf(1,1,:).*(Cls_nonlin(1,1,:))),'b--'); 
%loglog(l_vals,squeeze(pf(1,1,:).*(Cls_nonlin(1,2,:))),'b--'); 
%k = loglog(l_vals,squeeze(pf(1,1,:).*(Cls_nonlin(2,2,:))),'b--'); 

[h pf] = plotlens(l_vals,squeeze((C_l_ij(1,1,:))),'k',1);
plotlens(l_vals,squeeze(C_l_ij(1,2,:)),'k'); 
plotlens(l_vals,squeeze(C_l_ij(2,2,:)),'k'); 

plotlens(l_vals,squeeze(Cls_nonlin(1,1,:)),'b--'); 
plotlens(l_vals,squeeze(Cls_nonlin(1,2,:)),'b--'); 
k = plotlens(l_vals,squeeze(Cls_nonlin(2,2,:)),'b--'); 

legend([h k],'Cooray','T&J',2)

%print('-f4','-dpsc','tomography_bothmeth_1609_dz002...')

% Halo
%[Cls_halo plotfactor Clp Clc] = lenshalo_ps(cospars,l_vals,k_vals,pk,...
%        z_vals,M_vals,zs,dz);    
%loglog(l_vals,plotfactor.*Cls_halo,'y--')


% Ratios:
%figure
%semilogx(l_vals,squeeze(C_l_ij)'./Cls_halo) % Within 1%

toc