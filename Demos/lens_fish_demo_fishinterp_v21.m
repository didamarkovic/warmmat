% 'chisq_test'
%
% version 1
% Called lens_fish_tomo_demo_2.m
% 22.10.09 KMarkovic (modified SLB's script)
% version 1.1
% Changing the parameters a bit & making it into a script again.
% 14.04.2010 KM
% version 1.2
% Using 1/m. Finding chisq too!
% 16.04.2010 KM
% version 1.3
% Testing new function chisq.m! -> chisq_test.m
% 16.04.2010 KM
% version 1.4
% plotting error of m_wdm from fish and probability from chisq
% 16.04.2010 KM
% Now doing a massive chisq run for all parameters!
% Called lens_fish_tomo_demo_chifish.m
% 14.05.2010 KM
% version 2
% Now going to use fisher only - do iterations to find the right step-size
% to fit gaussian to the chisq curve.
% Called lens_fish_tomo_demo_fishiter.m
% 20.05.2010 KM
% version 2.1
% Ok, iteration doesn't work, try to simply interpolate.
% 21.05.2010 KM

%clear %clc

function [err0 filestring] = lens_fish_demo_fishinterp(l_max)

tic

if ~exist('l_max','var'); l_max = 4; end
linecolor = 'k';

dohalo = 0;
dosmith = 1;

% Set computational parameters
cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;
cospars.m_wdm = 10^8;
cospars.As = 1.2733*10^5;
cospars.k_pivot = 0.05;
cospars.Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;

nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});

z_vals = 0.0001:0.05:5;
l_vals = 2*logspace(1,l_max,20);
M_vals = logspace(0,16,50);

% NOTE THAT THE CODE BREAKS DOWN AROUND m = 500 eV!!!
%steps = 0.0008;%[0.00057 0.0014];%2*logspace(-2,-4,20);%[5.6993e-04 0.00115];%
% 24 hours for 3 steps for halo model
steps = 2*logspace(-5,-3,30);

%fish.names = {'invm'; 'omega_m'; 'ns'; 'As'; 'Gamma'};
%fish.steps = [NaN 0.00001 0.0001 10 0.00001]; % 1/m step must be first!!!
fish.names = {'invm'};
fish.steps = NaN; % 1/m step must be first!!!

c = clock;
startstring = ['Starting at ' num2str(c(4)) ':' num2str(c(5)) ' on the '  num2str(c(3)) '.' num2str(c(2)) '.' '2010.'];
filestring = ['fishinterp-' num2str(c(1)) '-' num2str(c(2)) '-' num2str(c(3)) '-' num2str(c(4)) 'h.txt'];

%% write file
dlmwrite(filestring,startstring,'')
dlmwrite(filestring,['m_wdm = ' num2str(cospars.m_wdm)],'-append','delimiter','')
dlmwrite(filestring,['no.bins = ' num2str(nzpars.nbin)],'-append','delimiter','')
%dlmwrite(filestring,['- no.sigma = ' num2str(nosigma)],'-append','delimiter','')
dlmwrite(filestring,'invm omega_m ns As Gamma','-append','delimiter','')
dlmwrite(filestring,fish.steps,'-append','delimiter',' ')
dlmwrite(filestring,'fisher matrices:','-append','delimiter','')
dlmwrite(filestring,' ','-append','delimiter',' ')

%% Work out for different steps

index = 1;
while index <= length(steps)
    
    step = steps(index);
    
    fish.steps = [step fish.steps(2:length(fish.steps))];
    
    % Find the power spectrum and the dCls/dparameter
    if dohalo && dosmith
        
        [CLS CLSh xx dcls dclsh] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
            nzpars.nbin,fish,M_vals);
        
        % Get covariance matrices
        [Ch_mat Ch_vect lensh_vals_v] = covmat(l_vals,CLSh+xx,nzpars.fsky);
        % Get Fisher matrices
        [fmath errh corrsh] = fishmat(fish.names,dclsh,Ch_mat);
        
        dlmwrite(filestring,'- halo:','-append','delimiter',' ')
        dlmwrite(filestring,step,'-append','delimiter',' ')
        dlmwrite(filestring,fmath,'-append','delimiter',' ')
        dlmwrite(filestring,' ','-append','delimiter',' ')
        
        errsh(index) = errh(1);
        
        [C_mat C_vect lens_vals_v] = covmat(l_vals,CLS+xx,nzpars.fsky);
        [fmat err corrs] = fishmat(fish.names,dcls,C_mat);
        
        dlmwrite(filestring,step,'-append','delimiter',' ')
        dlmwrite(filestring,fmat,'-append','delimiter',' ')
        dlmwrite(filestring,' ','-append','delimiter',' ')
        
        errs(index) = err(1);

    elseif dosmith
        [CLS nn xx dcls] = lens_ps_get(cospars,nzpars,z_vals,l_vals,nzpars.nbin,fish);
        
        [C_mat C_vect lens_vals_v] = covmat(l_vals,CLS+xx,nzpars.fsky);
        [fmat err corrs] = fishmat(fish.names,dcls,C_mat);
        
        dlmwrite(filestring,step,'-append','delimiter',' ')
        dlmwrite(filestring,fmat,'-append','delimiter',' ')
        dlmwrite(filestring,' ','-append','delimiter',' ')
        
        errs(index) = err(1);
        
    elseif dohalo
        
        [nn CLSh xx nn dclsh] = lens_ps_get(cospars,nzpars,z_vals,l_vals,...
            nzpars.nbin,fish,M_vals,1);
        
        [Ch_mat Ch_vect lensh_vals_v] = covmat(l_vals,CLSh+xx,nzpars.fsky);
        [fmath errh corrsh] = fishmat(fish.names,dclsh,Ch_mat);
        
        dlmwrite(filestring,'- halo:','-append','delimiter',' ')
        dlmwrite(filestring,step,'-append','delimiter',' ')
        dlmwrite(filestring,fmath,'-append','delimiter',' ')
        dlmwrite(filestring,' ','-append','delimiter',' ')
        
        errsh(index) = errh(1);
    end
    
    index = index + 1;
end

toc

% fish.steps(1)
% errh(1)
% fish.steps(2)
% errh(2)
% fish.steps(3)
% errh(3)
% fish.steps(4)
% errh(4)
% fish.steps(5)
% errh(5)

if dosmith
    step0 = interp1(errs - steps,steps,0);
    err0 = interp1(steps,errs,step0);
end

%save 'fishinterpp-2010-11-07.m'

return

%load 'fishinterpp-2010-07-07.m'
%
%%
if ~exist('jbojbljbmbj','var') 
    clear; 
    load 'fishinterp-2010-05-21h'; 
end
%
%% Plotting


dohalo = 0; dosmith = 1;

nosig = 1; 

figure
if dohalo && dosmith
    a = plot(steps,nosig*errs,'k.-'); hold on
    b = plot(steps,steps,'k:');
    h = plot(steps,nosig*errsh,'b.-'); hold on
elseif dohalo
    a = plot(steps,nosig*errsh,'b'); hold on
    b = plot(steps,steps,'k:');
elseif dosmith
    a = plot(steps,nosig*errs,'k'); hold on % THIS ONE!!
    %b = plot(steps,steps,'k:');
    b = plot([0 steps],[0 steps],'c');
end

% find the error = step point:
if dohalo && dosmith
    fn = nosig*errs - steps;
    step0 = interp1(fn,steps,0);
    err0 = interp1(steps,nosig*errs,step0)
    
    fn = nosig*errsh - steps;
    steph0 = interp1(fn,steps,0);
    errh0 = interp1(steps,nosig*errsh,steph0)
elseif dohalo
    fn = nosig*errsh - steps;
    steph0 = interp1(fn,steps,0);
    errh0 = interp1(steps,nosig*errsh,steph0)
elseif dosmith
    fn = nosig*errs - steps; % THIS ONE!!!
    step0 = interp1(fn,steps,0);
    err0 = interp1(steps,nosig*errs,step0)
end

% finish plot
if dohalo && dosmith
    c = plot(step0,err0,'ro');
    plot(steph0,errh0,'ro');
    axis([0 max(steps) 0 nosig*max(errs)])
elseif dohalo
    c = plot(steph0,errh0,'ro');
    axis([0 max(steps) 0 nosig*max(errsh)])
elseif dosmith
    c = plot(step0,err0,'ro'); % THIS ONE!!!!
    axis([0 max(steps) 0 nosig*max(errs)])
    %axis([0 0.002 0 nosig*max(errs)])
end
xlabel('d\theta [eV^-^1]' ,'FontSize',15); ylabel([num2str(nosig) '\sigma_w_d_m [eV^-^1]' ],'FontSize',15);
%legend([a b c], ['\sigma (d\theta)'],'x=y',['\sigma (d\theta_' num2str(nosig) ') = d\theta_' num2str(nosig)])
legend([a b c], ['\sigma (d\theta)'],'x=y','\sigma (d\theta) = d\theta')

set([a c],'Linewidth',1.5)
set(gca,'Linewidth',1.5,'FontSize',15)
%
%% Set the axis to be the same as the other figure (fig.6)
axis([0 0.002 0 0.008]); %nosig*max(errs)])

%% So now we better interpolate this: 12.07.2010 KM

stepsprec = 2*logspace(-5,-3,100); % linspace(0.000001,0.0002,30); %
errsprec = interp1(steps',errs',stepsprec','linear'); % min(steps)

fn = nosig*errsprec' - stepsprec; % THIS ONE!!!
%step0 = interp1(fn,stepsprec,0);
%err0 = interp1(stepsprec,nosig*errsprec,step0)
a = plot(stepsprec,nosig*errsprec,'k'); hold on % THIS ONE!!
b = plot([0 stepsprec],[0 stepsprec],'c');

c = plot(step0,err0,'ro'); % THIS ONE!!!!
axis([0 max(steps) 0 nosig*max(errs)])
%