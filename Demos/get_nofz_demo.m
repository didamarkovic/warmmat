% get_nofz_demo.m
%
% 09.12.2010 KMarkovic
% 08.06.2011: modifying for talk in Meudon, KM

clear; clf
dz = 0.01;
z_vals = 0.00001:dz:3;

nzpars = setnzpars([10],{'nbin'});
[n_z_tot ngals_bin]=get_nofz(nzpars,z_vals);  hold on
pl = plot(z_vals, n_z_tot/nzpars.nbin/dz); hold on
%plot(z_vals, n_z_tot/nzpars.nbin,'b--'); hold on
%plot(z_vals,sum(n_z_tot'/nzpars.nbin),'k-');

nzpars = setnzpars([1],{'nbin'});
[n_z_tot ngals_bin]=get_nofz(nzpars,z_vals);
rl = plot(z_vals, n_z_tot/nzpars.nbin/dz,'k-'); hold on
sum(n_z_tot)

set([rl],'linewidth',1.2)
xlabel('redshift')
ylabel('fraction of sources')
%line([0.5 0.5],[0 1],'linestyle','--','color','k')
%line([2 2],[0 1],'linestyle','--','color','k')

% nzpars = setnzpars([10],{'nbin'}); nzpars.type = 'equal_tophat';
% [n_z_tot ngals_bin]=get_nofz(nzpars,z_vals);
% plot(z_vals, n_z_tot/nzpars.nbin); hold on
% 
% nzpars = setnzpars([10],{'nbin'}); nzpars.type = 'equal_th_photoz';
% [n_z_tot ngals_bin]=get_nofz(nzpars,z_vals);
% plot(z_vals, n_z_tot/nzpars.nbin); hold on