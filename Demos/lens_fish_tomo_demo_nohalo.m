% 'lens_fish_tomo_demo_nohalo'
%
% Takes long to run!
%
% 16.10.09 KMarkovic

tic

clear

[cospars_fid valstmp namstmp] = setWMAPpars;

%cospars_fid = setpars([valstmp 0.25/1e20],{namstmp{:} 'WDM'});

nzpars = setnzpars([2 200],{'nbin' 'noth' 'smith2'});

z_vals = 0.0001:0.02:2; 
exl = log10(20000); 
l_vals = logspace(0,exl,20);
M_vals = logspace(0,16,50);

%fish.names = {'omega_m'; 'h'; 'sigma8'; 'omega_b'; 'w0'; 'ns'; 'O_m_wdm'};                       
%fish.steps = [0.03  0.1  0.09 0.01 0.1 0.1 ((0.25/1000)-(0.25/1e20))];
fish.names = {'omega_m'; 'h'; 'w0'};                       
fish.steps = [0.03  0.1  0.1];
                              
% Find the power spectrum and the dCls/dparameter
[cls tmp xx dcls] = lens_ps_get(cospars_fid,nzpars,z_vals,l_vals,nzpars.nbin,fish);

% Get covariance matrices
[C_mat C_vect lens_vals_v] = covmat(l_vals,cls+xx,nzpars.fsky);

% Get Fisher matrices
[fmat errs corrs] = fishmat(fish.names,dcls,C_mat);

%save(['run1_' num2str(round(1e5*now))],'*')

fmat

% Read out the values of the fiducial parameters for ellipse plotting:
for int = 1:length(fish.names)
    bfp(int) = getfield(cospars_fid,fish.names{int})';
end

% now plot the error ellipses
[h,errvals] = plotellipses_v1(bfp,pinv(fmat),fish.names,['b' 'r' 'g' 'b' '-']);
set(h,'linewidth',1.2)

%saveas(gcf,'errorellipses2509','bmp')
%print('-f3', '-dpsc', 'errorellipses1809_normpars...');

% Simple error on omega_m:
errorOm = 1/sqrt(fmat(1,1));
errorh = 1/sqrt(fmat(2,2));
errorw0 = 1/sqrt(fmat(3,3));
% C.f. to
invfish = pinv(fmat);
errorOm2 = sqrt(invfish(3,3));
errorh2 = sqrt(invfish(2,2));
errorw02= sqrt(invfish(1,1));
% These turn out to be about half the size of Sarah's from plotellipses
fprintf(1,'omega_m = %12.8f +/- %12.8f\n',cospars_fid.omega_m,errorOm)
fprintf(1,'omega_m = %12.8f +/- %12.8f\n',cospars_fid.omega_m,errorOm2)
fprintf(1,'h = %12.8f +/- %12.8f\n',cospars_fid.h,errorh)
fprintf(1,'h = %12.8f +/- %12.8f\n',cospars_fid.h,errorh2)
fprintf(1,'w0 = %12.8f +/- %12.8f\n',cospars_fid.w0,errorw0)
fprintf(1,'w0 = %12.8f +/- %12.8f\n',cospars_fid.w0,errorw02)

%figure
%plotfactor = l_vals.*(1+l_vals)/2/pi;
%lw = 2; fs = 12; set(gca,'fontsize',fs,'linewidth',lw);
%p1 = loglog(l_vals,plotfactor.*squeeze(cls(1,1,:))','b',l_vals,plotfactor.*squeeze(cls(2,2,:))','k'); hold on
%axis([l_vals(1)/2 max(l_vals) 1e-9 8e-3])
%title('Weak Lensing Power Spectrum plotting methods');
%xlabel('multipole l','fontsize',fs); 
%ylabel('l(l+1)C_l/2\pi','fontsize',fs);
%set(p1,'linewidth',lw);

toc