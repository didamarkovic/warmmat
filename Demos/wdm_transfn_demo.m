% Plotting m_x VS omega_x for a constant scale break from equn 7 from Viel
% 'scalebreak_demo.m' -> 'wdm_transfn_demo.m' (renamed 13.01.2010 by KM)
%
% 13/03/08 KMarkovic

clear

m_vals = 500:10:2000;
f_vals = 0:0.01:1; % f = omega_wdm/omega_dm
alpha = 0.049; % Scale break
h = 0.7;

omega_vals = 0.25*( (alpha/0.049) * ((m_vals/1000).^(1.11)) * ...
    ((h/0.7)^(-1.22)) ).^(1/0.11);
%plot(m_vals,omega_vals,'k'); xlabel('m_x [eV]'); ylabel('\Omega_x'); hold on
%axis([m_vals(1) m_vals(length(m_vals)) 0 0.25])
%legend('\alpha = 0.10','\alpha = 0.055','\alpha = 0.049','\alpha = 0.040')
%title('Scale break from Viel et al. equation for thermal relic')
%saveas(gcf,'scalebreakplot.jpg')
% This is pretty silly

% First calculate the possible values of the scale break:
alpha_vals = zeros(length(m_vals),length(f_vals));
for fint = 1:length(f_vals)
    alpha_vals(:,fint) = 0.049 * ((m_vals/1000).^(-1.11)) * ...
        (f_vals(fint)^(0.11)) * ((h/0.7)^(1.22));
end

% Let's try a 3D plot of alpha vs mx and omegax:
figure
imagesc(f_vals,m_vals,alpha_vals); hold on
colormap summer; colorbar
contour(f_vals,m_vals,alpha_vals,10,'k')
title('scale break'); xlabel('fraction of WDM'); ylabel('m_x [eV]');
axis([0.03 1 m_vals(1) m_vals(length(m_vals))])
% saveas(gcf,'scalebreakplot_contours_more.jpg')