% test_fourerkappa.m
%
% Plotting Kappa_l vs l for different masses, redshifts etc.
%
% 26.03.2010 KMarkovic

clear % clf
cospars = setWMAPpars;
M_vals = 10^16; %logspace(0,16,5);
k_vals = logspace(-4,4,1000);
z_vals = 0.5;
noth = 500;
consts; rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m;

% Growth factor:
D_plus = growth_factor(cospars,z_vals);

% Linear matter power spectrum:
if ~exist('pk_lin_0','var')
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
end
pk_lin = pk_lin_0 * D_plus.^2; % Grow spectrum

M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0,1); %%% [M_solar]

% Distances to halos:
clear consts; consts
Dh = c_km/H0_Mpc; % in Mpc
chis = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
chi = chis * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;

% Halo model parameters:
[rv_vals theta_v conc rho_s n00] = haloparams(cospars.omega_m,...
    cospars.h,M_scale,M_vals,z_vals,chi*Mpc);%%% [meters][no units][no units][kg/m^3]
rv_vals = rv_vals/Mpc;
rho_s = rho_s/M_solar*Mpc^3;

% For integration:
%dlogM = log(M_vals(2)) - log(M_vals(1));
%dM = dlogM.*M_vals; %%%[M_solar]

% Critical overdensity of collapse
delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units] % NOT THIS

l_vals = k_vals * cospars.h * chi;

% First Sigma_l
SIGMA_l = fourierkappa(z_vals, chi, M_vals, l_vals, theta_v,...
    rv_vals, conc, rho_s, 1, noth);

%% Plot
figure(1)
%pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
%l = loglog(k_vals,squeeze(sum(SIGMA_l(5:51,:,:))).^2.* chi^4 / rho_mean_Mpc_0^2,'y-.'); hold on
l = loglog(k_vals,k_vals'.^3 .*squeeze(SIGMA_l).* chi^2 / rho_mean_Mpc_0/2/pi,'r-'); hold on
%loglog(k_vals,squeeze(sum(SIGMA_l(5:51,:,:).^2)).* chi^4 / rho_mean_Mpc_0^2,'y--');
%o=loglog(k_vals,k_vals.^3 .*pk_lin/2/pi,'k-');
%set([l o],'linewidth',1.2);
ylabel('P(k)'); xlabel('k, waveno.')
%plot(1/rv_vals*cospars.h, k_vals(abs(k_vals-(1/rv_vals*cospars.h))==...
%    min(abs(k_vals-(1/rv_vals*cospars.h)))).^3 .*SIGMA_l(:,:, abs(k_vals-(1/rv_vals))==...
%    min(abs(k_vals-(1/rv_vals*cospars.h))) ).* chi^2 / rho_mean_Mpc_0/2/pi, 'gx')
%axis([k_vals(1) max(k_vals) 10000 10^6])