% 'test_kappaintegrand.m'
%
% Plotting all the elements of the integrand of the Fourier transform of
% the projected NFW density profile to see what's going on with the wiggles
% (yes, still...).
%
% 25.02.2010 KMarkovic
% version 1

clear

% Set free parameters
M_200 = 10^14; % h^-1 M_solar
z = 4.5;
z_s = 5; % Source redshift
cospars = setWMAPpars; % flat LCDM - WMAP5
nothet = 2^9;

% Multipoles
l_vals = [10 100 1000 10^4 5*10^4 10^5];

% Calculate constants
consts % Note: this is a script that sets constants
%Dh = c_km/H0_Mpc; % Hubble distance [Mpc]
Dh = c_km/100; % leave in units of h^-1

% Distances
chi_tmp = Ds_wconst(0,z,cospars.omega_m,cospars.omega_de,cospars.w0,1000); % [Dh]
chi = chi_tmp * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10; % [h^-1 Mpc]

% Corresponding multipoles in 3D matter power spectrum:
k_vals = l_vals/chi; % h Mpc^-1

% Scale mass
Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin_0 = pkf(Gamma,cospars.sigma8,logspace(-4,4,1000),cospars.ns,cospars.n_run);
M_scale = mascale(cospars,z,logspace(-4,4,1000),pk_lin_0); % Scale mass in M_solar

% Halo properties:
[r_v theta_v conc rho_s] = haloparams(cospars.omega_m,...
    cospars.h,M_scale,M_200,z,chi*Mpc); % [m][unitless][unitless][kg/m^3]
r_v = r_v/Mpc; %[Mpc]
rho_s = rho_s/M_solar*Mpc^3; %[M_sol/Mpc^3]
t_s = theta_v/conc;

% X-axis for plot
theta_vals = 0:theta_v/nothet:theta_v;
dtheta = theta_vals(2) - theta_vals(1);

% Projected density profile
chisource = Dh*Ds_wconst(0,z_s,cospars.omega_m,cospars.omega_de,cospars.w0,1000); %[Mpc]
chi_ls = chisource - chi; % This is only right for FLAT spacetime!  % [h^-1 Mpc]
SIGMAcrit = (c_Mpc^2) .* chisource ./ (4*pi*G_Mpc .*chi.*chi_ls) ./(1+z);% [M_sol /Mpc^2]
%SIGMAcrit =1;
kappa = nfwkappa(theta_vals,conc,theta_v,r_v,rho_s,SIGMAcrit);

% Loop over values of l or k:
bool = 1; figure(1); figure(2); clf(1); clf(2)
for int = 1:length(l_vals)

    % Bessel function
    J_0 = besselj(0,(l_vals(int)+0.5)*theta_vals);

    % Integrand
    dKl_dthet = theta_vals.*J_0.*kappa*dtheta;
    if int == 1 
        scaling = max(dKl_dthet); 
        sumscaling = max(cumsum(dKl_dthet));
    end % Decide on scaling in plots so its easier to see.
    
    if bool row = 0; bool = 0; else row = 1; bool = 1; end
    figure(1)
    subplot(2,length(l_vals)/2,int)
    %plot(theta_vals,theta_vals,'k:'); hold on
    %plot(theta_vals,kappa/max(kappa),'k--')
    %plot(theta_vals,J_0,'k:')
    %plot(theta_vals,dKl_dthet/scaling,'b')
    plot(theta_vals,dKl_dthet,'b'); hold on
    %line([t_s t_s],[min(dKl_dthet) max(dKl_dthet)],[0 0],'Color','r')
    plot(t_s,linspace(min(dKl_dthet),max(dKl_dthet),10),'r')
    titlestring = ['           z = ' num2str(z,'%10.2f') ', M = ' num2str(M_200,'%2.0e') ', l = ' num2str(l_vals(int),'%10.0f') ', k = ' num2str(k_vals(int),'%10.3f') ];
    axis([theta_vals(1) max(theta_vals) min(dKl_dthet) max(dKl_dthet)])
    title(titlestring); xlabel('\theta'); ylabel('integrand')
    
    figure(2)
    subplot(2,length(l_vals)/2,int)
    %plot(theta_vals,cumsum(theta_vals),'k:'); hold on
    %plot(theta_vals,cumsum(kappa)/max(cumsum(kappa)),'k--')
    %plot(theta_vals,cumsum(J_0),'k:')
    %plot(theta_vals,cumsum(dKl_dthet)/sumscaling,'b')
    plot(theta_vals,cumsum(dKl_dthet),'b'); hold on
    plot(theta_vals,cumtrapz(dKl_dthet),'r-.'); hold on % trapezoid integration
    %line([t_s t_s],[-1 1],[0 0],'Color','r')
    plot(theta_vals,zeros(size(theta_vals)),'k--')
    titlestringc = ['                      z = ' num2str(z,'%10.2f') ', M = ' num2str(M_200,'%2.0e') ', l = ' num2str(l_vals(int),'%1.0e') ', k = ' num2str(k_vals(int),'%10.3f') ];
    axis([theta_vals(1) max(theta_vals) min(cumsum(dKl_dthet)) max(cumsum(dKl_dthet))])
    title(titlestringc); xlabel('\theta'); ylabel('cumulative sum')
    plot(theta_vals(end),rombergint(dKl_dthet/dtheta,theta_vals),'r.')
    
    clear J_0 dKl_dthet
end