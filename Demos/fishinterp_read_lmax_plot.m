% fishinterp_read_lmax_plot
%
% I want to be able to read the files that are written by
% lens_fish_demo_fishinterp.m
% I'm doing this with low level commands and not textread.m because I want
% to read different lines differently!
%
% version 1
% 26.05.2010 KMarkovic
% version 2
% I'm slightly modifying this, so that I don't merge all the data, but
% instead to be able to plot erro vs lmax.
% 11.06.2010 KM

clear

files = {'6-9-16';'6-9-14';'6-9-18';'6-9-20';'5-21-21';};
lmaxs = [1000 2000 3000 5000 10000];

nopars = 5;
planckmat = zeros(nopars,nopars);

for fileint = 1:length(files)
    
    steps = NaN;
    errs = NaN;
    errsh = NaN;
    
    insertdatetime = files{fileint};
    
    filename = ['fishinterp-2010-' insertdatetime 'h.txt'];
    
    % open file
    fileid = fopen(filename);
    
    % read out of file line by line
    r = 1;
    x = 0;
    indfish = 1;
    step = NaN;
    % get the first line
    linetext = fgetl(fileid)
    
    while linetext~=-1
        
        if r<=7
            % skip the header of the file
        else
            
            if indfish~=1
                if  str2num(linetext) ~= step(indfish-1)
                    step(indfish) = str2num(linetext);
                    indfish = indfish + 1;
                end
            else
                step(indfish) = str2num(linetext);
                indfish = indfish + 1;
            end
            
            for int = 1:nopars
                linetext = fgetl(fileid);
                r = r+1;
                
                sithfish(:,int,indfish-1) = str2num(linetext);
            end
            
            mat = sithfish(:,:,indfish-1);
            mat = mat + planckmat;
            mat(4,:) = mat(4,:)*10^5;
            mat(:,4) = mat(:,4)*10^5;
            invmat = inv(mat);
            errs(indfish-1) = sqrt(abs(invmat(1,1)));
            
            
            
            % skip the empty line
            linetext = fgetl(fileid);
            r = r+1;
            
        end
        
        % must make sure that the end of file is caught by the while loop
        linetext = fgetl(fileid);
        r = r+1;
        
    end
    
    %frewind(fileid);
    fclose(fileid);
    
    
    [step order] = sort(step);
    errs = errs(order);
    
    [step indexvect] = unique(step);
    errs = errs(indexvect);
    
    nosig = 1;
    
    index = ~isnan(errs);
    serrs = errs(index); ssteps = step(index);
    
    fn = nosig*serrs - ssteps;
    step0(fileint) = interp1(fn,ssteps,0);
    err0(fileint) = interp1(ssteps,nosig*serrs,step0(fileint));
    
end

%%
figure
plot(lmaxs,err0,'k.'); hold on