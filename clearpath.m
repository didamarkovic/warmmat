% This command removes the paths to the mFiles folders i.e. KM, SLB and XC
% and to C:/CosmoRun

path(pathdef);

%warning off
%rmpath '/Users/katarina/Documents/matlab/WDM'
%rmpath '/Users/katarina/Documents/matlab/SLB'
%warning on