% ------------------------------------------------------------------------
% Copyright (C) 2008-2010
% Bruce Bassett Yabebal Fantaye  Renee Hlozek  Jacques Kotze
%
%
%
% This file is part of Fisher4Cast.
%
% Fisher4Cast is free software: you can redistribute it and/or modify
% it under the terms of the Berkeley Software Distribution (BSD) license.
%
% Fisher4Cast is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% BSD license for more details.
% ------------------------------------------------------------------------
function EXT_function_derivative_plot(deriv_flag, function_flag)
% This function generates a plot of the derivatives of the specific
% observables included in Fisher4Cast, H(data), d_A(data) and G(data). 
% It must be included in the directory in which Fisher4Cast is contained, 
% or that directory must be added to the Matlab path. 

% The flags deriv_flag and function_flag are set to 1 (0) if you do (dont)
% want to plot the function or derivatives. If no input is given these are
% both set to 1 and you get plots of all functions and derivatives.

% As a default example it calls the Seo_Eisenstein_2003 input structure,
% but then generates a redshift vector from 0.1:10;
% The colours for line plots must be specified as 1x3 RGB vectors,
% normalised to 1 (i.e. so each entry divided by 255). See the default
% colours as an example.
close all

% Flags to control what you want to plot, either derivatives only, of
% function only, or both
if nargin == 0
    deriv_flag = 1;
    function_flag = 1;
elseif nargin == 1;
    function_flag = 0;
end
%--------------------------------------------------------------------------
% Specify the colours of the derivatives
hcolour =[147 50 0]./255;
gcolour = [240 201 81]./255;
dacolour = [231 109 29]./255;
colourmat = [hcolour
            gcolour
            dacolour];
styles = {'-', '-.',  '--',':', '-.'};
legendmat{1} = {'dlnH/dH_0'  ' dlnH/dln\Omega_m' 'dlnH/d\Omega_k' 'dlnH/dw_0' 'dlnH/dw_a'};
legendmat{2} = {'dlnd_A/dH_0' ' dlnd_A/dln\Omega_m' 'dlnd_A/d\Omega_k' 'dlnd_A/dw_0'  'dlnd_A/dw_a'};
legendmat{3} = {'dlnG/dH_0' ' dlnG/dln\Omega_m' 'dlnG/d\Omega_k' 'dlnG/dw_0' 'dlnG/dw_a'};
%--------------------------------------------------------------------------
% Generate the Input data for the derivative plot
input = Seo_Eisenstein_2003; % initialise the input structure
data = 0.1:0.1:10; % the redshift range we want to consider
data = data(:);
input.growth_zn = 0; 
input.growth_zn_flag = 1;  % make sure the Growth is normalised at data = 0;
input.observable_index = [1 2 3]; % Use all three observables
input.num_observables = length(input.observable_index);
% Re-assign the redshift vectors in the input structure
input.data{1} = data;
input.data{2} = data;
input.data{3} = data;

% Re-assign the errors vectors in the input structure
input.error{1} = 0.1.*ones(1,length(input.data{1}));
input.error{2} = 0.1.*ones(1,length(input.data{2}));
input.error{3} = 0.1.*ones(1,length(input.data{3}));

% Use the analytical formula for H, d_A and numerical derivatives for G
input.numderiv.flag{1} = 0;
input.numderiv.flag{2} = 0;
input.numderiv.flag{3} = 1;
%--------------------------------------------------------------------------
% Run Fisher to get the parameter values and derivatives
output = FM_run(input);
close(1); % Close the figure of the Fisher Ellipse

%--------------------------------------------------------------------------
% PLOT THE DERIVATIVES
% We will plot dlnX/dtheta_i = dX/Xdtheta_i
%--------------------------------------------------------------------------
if deriv_flag == 1

    for i = 1:input.num_observables % Loop over the observable functions
    
        x = input.observable_index(i);
        % Set the figure properties    
        figure(x*100)
        axes( 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 14 , 'XScale', 'log', 'XTickLabel', {'0.1';'1';'10'} )
        hold on
        box on
        xlabel('Redshift', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
        ylabel(['Fisher Derivatives for ', input.observable_names{x}, '(z)'], 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16)
        
        for j = 1:5 
            if j==2     % The Omega_m derivative, this is actually dlnH/dlnOm
                semilogx(data, input.base_parameters(j).*output.function_derivative{x}(:,j)./output.function_value{x},...
                    'LineStyle', styles{j},'LineWidth', 2,  'Color', colourmat(x,:))             
            else
                plot(data, (output.function_derivative{x}(:,j)./output.function_value{x}),...
                'LineStyle', styles{j},'LineWidth', 2,   'Color', colourmat(x,:)) 
            end % end the check to see if we are plotting Omega_m derivatives
        end % end the loop over the parameters
        
        legend(legendmat{x}, 'Location','NorthWest') % plot the legend for the function according to the observable
    end % end the loop over the observable functions
    
end % end the if loop for plotting of derivs

%--------------------------------------------------------------------------
% PLOT THE FUNCTIONS
%--------------------------------------------------------------------------

if function_flag ==1
    
    for i = 1:input.num_observables
        x = input.observable_index(i);
        % Set the figure properties
        figure(100*x +1)
        axes( 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 14 ,'XScale', 'log', 'XTickLabel', {'0.1';'1';'10'} )
        hold on
        box on 
        xlabel('Redshift', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
        ylabel([input.observable_names{x}, '(z)'], 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
    
        % Plot the data
        semilogx(data, output.function_value{x}, 'LineWidth', 2,  'Color', colourmat(x,:));
        
    end % end the loop over observables
    
end  % end the if loop for function plotting
