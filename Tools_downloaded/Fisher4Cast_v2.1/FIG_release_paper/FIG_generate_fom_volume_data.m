% ------------------------------------------------------------------------
% Copyright (C) 2008-2010
% Bruce Bassett Yabebal Fantaye  Renee Hlozek  Jacques Kotze
%
%
%
% This file is part of Fisher4Cast.
%
% Fisher4Cast is free software: you can redistribute it and/or modify
% it under the terms of the Berkeley Software Distribution (BSD) license.
%
% Fisher4Cast is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% BSD license for more details.
% ------------------------------------------------------------------------
%This functions generates a volume matrix of FoM's (vol_res X vol_res)
%for the default observable indexs of H, Da and G. The resolution of the data 
%ie the number of rows and columns can be set by vol_res.
%
%The resulting matrix fom_grid_out can be used in the plotting function:
%
%>>FIG_plot_slice_fom_volume(fom_vol_out)
%
%This function requires the input structure fom_landscape_input.
%
%Example of using this function:
%
%>>fom_vol_out = FIG_generate_fom_volume_data(vol_res)
%
%if vol_res is not given then a default value is assumed.
%-------------------------------------------------------------------------
function fom_vol_out = FIG_generate_fom_volume_data(vol_res)

%check to see if a vol_res has been specified
if nargin<1
    %if no vol_res exists use a default value of 50
    vol_res = 10;
end

%select the redshift range for each bin of H
H = linspace(0.1,5,vol_res);
%select the redshift range for each bin of Da
Da = linspace(0.1,5,vol_res);
%select the redshift range for each bin of Da
G = linspace(0.1,5,vol_res);

%select the fom type selected to output eg 1 = DETF FoM is the default (refer to the
%manual on the differing FoM types)
choosen_fom=[1];

%set the input structure to be used
input = FIG_fom_landscape_input;
%set the parameters to plot (default is w0 vs wa)
input.parameters_to_plot = [4 5];
%set the observable index to be used (default is H Da and G)
input.observable_index = [1 2 3];

%loop over the redshift data range set for G
for k=1:length(G)
    input.data{3} = G(k);
    %loop over the redshift data range set for H
    for i=1:length(H)
        %set the redshift bin for the input structure
        input.data{1} = H(i);
        %loop over the redshift data range set for Da
        for j=1:length(Da)
            %set the redshift bin for the input structure        
            input.data{2} = Da(j);
            %run FM_run with the specified input and save data to the output
            %struture
            output = FM_run(input);
            %suppress any ellipse figures generated from FM_run
            close all;
            %save the choosen FoM to the FoM matrix where i and j correspond to
            %to the indexs of the range of data for H and Da
            FoM(i,j,k)=output.fom(choosen_fom);
        end
        %end loop over data range for Da
    end
    %end loop over data range for G    
end
%end loop over data range for H

%pass FoM matrix to fom_vol_out
fom_vol_out = FoM;


