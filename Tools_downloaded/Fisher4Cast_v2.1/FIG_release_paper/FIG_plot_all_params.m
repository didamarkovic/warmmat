% ------------------------------------------------------------------------
% Copyright (C) 2008-2010
% Bruce Bassett Yabebal Fantaye  Renee Hlozek  Jacques Kotze
%
%
%
% This file is part of Fisher4Cast.
%
% Fisher4Cast is free software: you can redistribute it and/or modify
% it under the terms of the Berkeley Software Distribution (BSD) license.
%
% Fisher4Cast is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% BSD license for more details.
% ------------------------------------------------------------------------
function EXT_plot_all_params
global input plot_spec axis_spec output
%  This function generates ellipses for all combinations of the observables
%  specified. 

% This code must be included in the directory in which Fisher4Cast is contained, 
% or that directory must be added to the Matlab path. 

% As a default example it calls the Seo_Eisenstein_2003 input structure,
% Colours are defined as an RGB vector, each entry normalised to 1 by
% dividing by 255. See the example colours for reference.

% NOTE That this code uses getfigdata.m by M.A. Hopcroft, which is code from the Matlab File
% Exchange (http://www.mathworks.co.uk/matlabcentral/fileexchange/14081).
% It is included in this package.

close all
%--------------------------------------------------------------------------
% Choose your color scheme
colour_vec = [
    181 181 251
    72 72 239 ]./255;
% Choose the params you are interested in plotting as combination
list1 = 4:5
list2 = 4:5

% Choose the number of sigma  levels you want to plot
sigmalevel = 1:2;
sigma_vec = [2.31 6.2 11.83]; % the vector of sigma
%--------------------------------------------------------------------------
% Set the Input structure for the survey you will use
input = Seo_Eisenstein_2003;
input.growth_zn_flag = 1; % Normalise growth at unity
input.growth_zn = 0;
input.data{3} = input.data{1}; % Take measurements of G(z) at the same places as H(z)
input.error{3} = input.error{1}; % Set errors on G(z) to be those of H(z)
input.observable_index = [1 2 3]; % we will use all 3 observables
input.prior_matrix = zeros(5,5);
input.fill_flag =1; 
input.guiRun = 1
%--------------------------------------------------------------------------
% Calling Fisher4Cast in a loop
for j =  list1(1):list1(end) % loop over the list of parameter 1
        
    for k = list2(1):list2(end) % loop over the list of parameter 2
            if k==j % we want 1D likelihood
                input.parameters_to_plot = [j];               
            else % we want 2D ellipse
                input.parameters_to_plot = [j k];
            end
           inputsav{j,k} = input; % save the input structure
           outputsav{j,k} = FM_run(input); % run Fisher4Cast
           close(1)  % close the figure produced
    end    
end

%--------------------------------------------------------------------------
% Now plot the ellipses in a systematic format
 for j =  list1(1):list1(end) % loop over the list of parameter 1
         
         for k = list2(1):list2(end) % loop over the list of parameter 2
             figure(100*k + j)   
             % The figures are numbered according to the parameter index e.g. 102 = H0-Om       
             hold on
             box on  
             for l = sigmalevel(1):sigmalevel(end)
                 % loop over the no of significance levels you want
                 output = outputsav{j,k}; % recall the input structure for this combination of j,k
                 input = inputsav{j,k};
                 count = mod(l,2)+1;
                 % Set the plotting specifics
                 plot_spec.resolution = max([1000, 1./input.base_parameters(1),1./input.base_parameters(2)]);
                 plot_spec.sb = max([10.*input.base_parameters(1), 10.*input.base_parameters(2)]);
                 cval = input.base_parameters(input.parameters_to_plot);
                 plot_spec.center_ellipse = cval;
                 plot_spec.sigma_level = sigma_vec(count);
                 plot_spec.sigma_linecolor = colour_vec(l,:);
                 plot_spec.fill_color = colour_vec(l,:); 
                 plot_spec.linecolor = colour_vec(l,:);
                 plot_spec.line_width = 2;
                 
                 if k==j % We have a 1D likelihood 
             
                     plot_spec.sigma_level = sigma_vec(count);                                        
                     hold on
                     FM_plot_likelihood(plot_spec);% call the likelihood plotting routine                    
                     % Set your axis specifications                     
                     set(gca, 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 25 )       
                     xlabel(input.parameter_names{inputsav{j,k}.parameters_to_plot(1)}, 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 30 )
                     ylabel('Normalised Likelihood' , 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 30 )
                              
                 else % We have a 2D likelihood                  
                     ellipsemat = output.marginalised_matrix;
                     input.fill_flag = 1;
                     FM_plot_ellipse(ellipsemat);
                     set(gca, 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 25 )
                     xlabel(input.parameter_names{inputsav{j,k}.parameters_to_plot(1)}, 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 30 )
                     ylabel(input.parameter_names{inputsav{j,k}.parameters_to_plot(2)}, 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 30 )

                 end 
             end 
         end
 
 end
 %-------------------------------------------------------------------------
 
