% ------------------------------------------------------------------------
% Copyright (C) 2008-2010
% Bruce Bassett Yabebal Fantaye  Renee Hlozek  Jacques Kotze
%
%
%
% This file is part of Fisher4Cast.
%
% Fisher4Cast is free software: you can redistribute it and/or modify
% it under the terms of the Berkeley Software Distribution (BSD) license.
%
% Fisher4Cast is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% BSD license for more details.
%-------------------------------------------------------------------------
%This functions generates a landscape matrix of FoM's (grid_res X grid_res) 
%for the default observable indexs of H and Da. The resolution of the data 
%ie the number of rows and columns can be set by grid_res.
%
%The resulting matrix fom_grid_out can be used in the plotting function:
%
%>>FIG_plot_pcolor_fom_landscape(fom_grid_out)
%
%>>FIG_plot_surf_fom_landscape(fom_grid_out)
%
%This function requires the input structure fom_landscape_input.
%
%Example of using this function:
%
%>> fom_grid_out = FIG_generate_fom_grid_data(grid_res)
%
%if grid_res is not given then a default value is assumed.
%-------------------------------------------------------------------------
function fom_grid_out = FIG_generate_fom_grid_data(grid_res)

%check to see if a grid_res has been specified
if nargin<1
    %if no grid_res exists use a default value of 50
    grid_res = 50;
end

%select the redshift range for each bin of H
H = linspace(0.1,5,grid_res);
%select the redshift range for each bin of Da
Da = linspace(0.1,5,grid_res);
%select the fom type selected to output eg 1 = DETF FoM is the default (refer to the
%manual on the differing FoM types)
choosen_fom=[1];

%set the input structure to be used
input = FIG_fom_landscape_input;
%set the parameters to plot (default is w0 vs wa)
input.parameters_to_plot = [4 5];
%set the observable index to be used (default is H and Da)
input.observable_index = [1 2];

%loop over the redshift data range set for H
for i=1:length(H)
    %set the redshift bin for the input structure
    input.data{1} = H(i);
    %loop over the redshift data range set for Da
    for j=1:length(Da)
        %set the redshift bin for the input structure        
        input.data{2} = Da(j);
        %run FM_run with the specified input and save data to the output
        %struture
        output = FM_run(input);
        %suppress any ellipse figures generated from FM_run
        close all;
        %save the choosen FoM to the FoM matrix where i and j correspond to
        %to the indexs of the range of data for H and Da
        FoM(i,j)=output.fom(choosen_fom);
    end
    %end loop over data range for Da
end
%end loop over data range for H

%pass FoM matrix to fom_grid_out
fom_grid_out = FoM;


