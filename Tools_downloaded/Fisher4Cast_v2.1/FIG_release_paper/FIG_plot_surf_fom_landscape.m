% ------------------------------------------------------------------------
% Copyright (C) 2008-2010
% Bruce Bassett Yabebal Fantaye  Renee Hlozek  Jacques Kotze
%
%
%
% This file is part of Fisher4Cast.
%
% Fisher4Cast is free software: you can redistribute it and/or modify
% it under the terms of the Berkeley Software Distribution (BSD) license.
%
% Fisher4Cast is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% BSD license for more details.
% ------------------------------------------------------------------------
%This function takes a two dimensional matrix, fom_grid_out, and plots a 
%slice plot for this data.
%
%fom_grid_out can be generated by calling
%
%>>fom_grid_out = FIG_generate_fom_grid_data(grid_res)
%
%If no grid_res is passed a default value of 50 is assumed.
%
%Example of using this function:
%
%>>FIG_plot_surf_fom_landscape(fom_grid_out)
%
%if no fom_grid_out is given then code checks to see if there is a default
%.mat file, default_fom_grid_out.mat, to load the data from, else
%the function FIG_generate_fom_landscape is called with a set of default values. 
%You can further specify the H_range and Da_range if none is given then an 
%assumed range of [0.1:0.1:5] is used for both:
%
%>>plot_surf_fom_landscape(fom_grid,H_range,Da_range)
%
%if neither of these ranges are given a default range is again assumed.
%--------------------------------------------------------------------------------

function FIG_plot_surf_fom_landscape(fom_grid,H_range,Da_range)

%check to see if a matrix with the FoM has been passed already, fom_grid,
%and the range of the data for H and Da
if nargin<1
    %see if the default .mat file exists and load the data
    if exist('default_fom_grid_out.mat')
        load default_fom_grid_out;
        fom_grid = default_fom_grid_out;
    else
        %if not then generate the fom_grid data
        fom_grid = FIG_generate_fom_grid_data(50);
    end
    %select the redshift range for each bin of H
    H_range = [0.1:0.1:5];
    %select the redshift range for each bin of Da
    Da_range = [0.1:0.1:5];
elseif nargin==1
    %select the redshift range for each bin of H
    H_range = [0.1:0.1:5];
    %select the redshift range for each bin of Da
    Da_range = [0.1:0.1:5];
elseif nargin ==2 
    errordlg('You are required to enter either one input or three ie FIG_plot_surf_fom_landscape(fom_grid) only or plot_surf_fom_landscape(fom_grid,H_range,Da_range)');
end

%select a colormap
colormap(jet);

%produce a surface plot dependent on the amount of input supplied
if nargin<=1 || nargin ==3
    %this plot will list the x and y tick data as being the H_range and
    %Da_range given
    s = surf(H_range,Da_range,fom_grid(:,:));
end

%specify labels for the x, y and z axes
ylabel('H Redshift');
xlabel('d_A Redshift');
zlabel('DETF   FoM');


