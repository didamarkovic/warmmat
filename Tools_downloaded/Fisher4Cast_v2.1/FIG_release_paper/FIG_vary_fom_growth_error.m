% ------------------------------------------------------------------------
% Copyright (C) 2008-2010
% Bruce Bassett Yabebal Fantaye  Renee Hlozek  Jacques Kotze
%
%
%
% This file is part of Fisher4Cast.
%
% Fisher4Cast is free software: you can redistribute it and/or modify
% it under the terms of the Berkeley Software Distribution (BSD) license.
%
% Fisher4Cast is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% BSD license for more details.
% ------------------------------------------------------------------------
function EXT_vary_fom_growth_error
global input plot_spec axis_spec
% This function generates a plot of the Fisher ellipse as one changes the error on
% the growth function G(z) relative to the error on the Hubble parameter
% H(z), and a plot of the Dark Energy Task Force Figure of Merit (FoM) as a
% function of the error on G relative to H. See the User's Manual for
% definitions of the FoM.

% This code must be included in the directory in which Fisher4Cast is contained, 
% or that directory must be added to the Matlab path. 

% As a default example it calls the Seo_Eisenstein_2003 input structure,
% The Matlab 'winter' colourmap is used to generate the line colours, and
% the number of iterations is given by N.

% NOTE That this code uses getfigdata.m by M.A. Hopcroft, which is code from the Matlab File
% Exchange (http://www.mathworks.co.uk/matlabcentral/fileexchange/14081).
% It is included in this package.

close all

%--------------------------------------------------------------------------
% PLOT SETTINGS
linecolor = colormap(winter);% Choose your color scheme
valinit = 35; % the starting colour for the plots
line_width = 2; % the line width for ellipses and FoM plot
%--------------------------------------------------------------------------
% Set the Input structure for the survey you will use
input = Seo_Eisenstein_2003;
input.observable_index = [1 2 3]; % we will use all 3 observables
input.fill_flag = 0; % do not fill the ellipse
%--------------------------------------------------------------------------
% Initialise the error on growth - set to the Hubble error
input.data{3} = input.data{1};
orig_error = 0.1.*ones(1,length(input.error{1}));
N = 20;
amp = logspace(-1, 1, N);
%--------------------------------------------------------------------------
% Call Fisher4Cast in a loop
for i=1:N 
    figure(1) 
    hold on 
    input.error{3} = orig_error.*amp(i); % Modify the fractional growth error 
    output = FM_run(input); % Call Fisher4Cast 
    outv(i,:) = output.fom; % Save the full FoM vector 
    out(i) = outv(i,1); % Save the DETF FoM for this run
    h = getfigdata(1); % Rip the data off the figure using getfigdata.m
    x{i} = h{1}.x;
    y{i} = h{1}.y;
    close(1) % close the figure
end

%--------------------------------------------------------------------------
% Initialise the Plots
figure(1001)
axes( 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 14 )
box on
hold on
xlabel('w_0', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
ylabel('w_a', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
    
figure(1002)
axes( 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 14,'XScale', 'log' );
box on
hold on
xlabel('Error on G(z) relative to error on H(z)', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
ylabel('DETF FOM', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
%--------------------------------------------------------------------------
% Plot the ellipses
count = 0;
for i = 1:N
    % Use increasing or decreasing colour to get a gradient 
    if i < ceil(N/2) 
        count = count +1;
    else 
        count = count -1;
    end
    
    cval = valinit+ 2*count;       
    figure(1001)
    plot(x{i}, y{i}, 'Color', linecolor(cval,:), 'LineWidth', line_width)     
end

figure(1002)
loglog(amp, out,  'Color', linecolor(cval,:), 'LineWidth', line_width)      
%--------------------------------------------------------------------------

