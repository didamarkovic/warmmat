% ------------------------------------------------------------------------
% Copyright (C) 2008-2010
% Bruce Bassett Yabebal Fantaye  Renee Hlozek  Jacques Kotze
%
%
%
% This file is part of Fisher4Cast.
%
% Fisher4Cast is free software: you can redistribute it and/or modify
% it under the terms of the Berkeley Software Distribution (BSD) license.
%
% Fisher4Cast is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% BSD license for more details.
% ------------------------------------------------------------------------
function EXT_vary_fom_curvature_prior
global input plot_spec axis_spec
% This function generates a plot of the Fisher ellipse as one changes the 
%  prior value on curvature, and a corresponding plot of the Dark Energy 
% Task Force Figure of Merit (FoM) as a function of the prior. 
% See the User's Manual for definitions of the FoM.

% This code must be included in the directory in which Fisher4Cast is contained, 
% or that directory must be added to the Matlab path. 

% As a default example it calls the Seo_Eisenstein_2003 input structure,
% The Matlab 'colormap' command is used to generate the line colours, specific to each 
% observable and the number of iterations is given by N.

% NOTE That this code uses getfigdata.m by M.A. Hopcroft, which is code from the Matlab File
% Exchange (http://www.mathworks.co.uk/matlabcentral/fileexchange/14081).
% It is included in this package.

close all
%--------------------------------------------------------------------------
% Choose your colour schemes for the various combinations
linecolor{1} = sort(colormap(copper), 'descend');
linecolor{2} = colormap(autumn);
linecolor{3} = colormap(copper);
linecolor{4} = colormap(winter);
linecolor{5} = colormap(summer);
input.fill_flag = 1;
valinit = 35; % the starting colour for the plots
num_obs = [1 2]; % The vector of combinations you want:
% 1 = Hubble
% 2 = Angular Diameter distance
% 3 = Growth Function
% 4 = Hubble parameter + Angular Diameter distance + Growth
% 5 = Hubble parameter + Angular Diameter distance

line_width = 2;
%--------------------------------------------------------------------------
% Set the Input structure for the survey you will use
input = Seo_Eisenstein_2003;
input.data{3} = input.data{1};
input.error{3} = 0.1.*ones(1,length(input.error{1}));
input.observable_index = [1 2 3]; % We will use all observables
input.fill_flag = 0;
input.numderiv.flag{3} = 1;
%--------------------------------------------------------------------------
% Set up the range you wish to consider
start_prior= 1e6;
range = 1e8; % No of orders of magnitude in the prior
N = 20; % Number of points
amp = (range)^(1/N); 

% Initialise the priors
prior_orig = input.prior_matrix; % this will be the default value
input.prior_matrix(3,3) = start_prior;
input.prior_matrix(2,2) = 0; % Initialise the prior on the matter density to zero
%--------------------------------------------------------------------------
% Initialise the global FOM plot
    figure(3000)
    axes( 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 14,'XScale', 'log','YScale', 'log' );
    hold on
    box on
    xlabel('Prior(\Omega_k)', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
    ylabel('Figure of Merit', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
%--------------------------------------------------------------------------    
% Call Fisher4Cast in a loop
for ni = num_obs(1):num_obs(end)
    input.prior_matrix(3,3) = start_prior;
    if ni ==4 % Compute the Fisher Ellipse for combination of Hubble, d_A and G
        input.observable_index = [1 2 3];    
    elseif ni ==5 % Compute the Fisher Ellipse for combination of Hubble and d_A  only
        input.observable_index = [1 2 ];
    else
        input.observable_index = ni; % use index value as specified
    end
    
    for i=1:N
        figure(1)
        hold on    
        input.prior_matrix(3,3) = input.prior_matrix(3,3)./amp; % modify the Prior
        output = FM_run(input); % Call Fisher4Cast 
        val(i) = input.prior_matrix(3,3); % save the value of the prior for plotting
        outv(i,:) = output.fom;  % Save the full FoM vector
        out(i) = outv(i,1); % Save the DETF FoM
         h = getfigdata(1); % call getfigdata.m to rip off the ellipse
         x{i} = h{1}.x;
         y{i} = h{1}.y;
         close(1) % close the figure
    end
    %--------------------------------------------------------------------------
    % Initialise the plotting figures with the axis specs etc
    figure(1000+ni)
    axes( 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 14 )
    hold on
    box on
    xlabel('w_0', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
    ylabel('w_a', 'FontName', 'Times', 'FontAngle', 'italic', 'FontSize', 16 )
    axis([-3 1 -10 10 ])
    count = 0;
        
%--------------------------------------------------------------------------
    % Plot the resulting ellipses
    for i = 1:N
        figure(1000+ni)
        hold on
        % Use increasing or decreasing colour to get a gradient
        if i < ceil(N/2)
            count = count +1;
        else 
            count = count -1;
        end
        cval = valinit+ 2*count;
        plot(x{i}, y{i}, 'Color', linecolor{ni}(cval,:), 'LineWidth',line_width)
    end
    
    figure(3000)
    hold on
    plot(val,out, 'Color', linecolor{ni}(20,:),  'LineWidth', line_width)
end
%--------------------------------------------------------------------------
