function [n_z_tot ngals_bin]=get_nofz_amarar06_biasbins(s,z_vals,verb);
% n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);
% Builds up Pr(z_spec,z_phot) using eqns in Amara & Refregier 2007
% Then it adds an outlier fraction.
% Then it shifts things according to zbias.
%
% based on get_nofz_amara06_biasbins
% extra pars are s.nbin_zbias, s.zbiass, s.deltazs
% based on get_nofz_equal_th_photoz 
% SLB 27 Feb 2007

% set defaults
if (~exist('verb')) verb=0; end % verbosity level
if (~isfield(s,'verb')) s.verb=0; end
verb=max([s.verb verb]);
if (~isfield(s,'ng')) s.ng=1; end % arbitrary normalisation if not reqd

% make overall Smail et al n(z)
% n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);
n_z=z_vals.^s.alpha .* exp(-(z_vals/s.z_0).^s.beta);

% find bin divisions for photoz cuts
% normalise in a way that makes it easier to find z bin divisions
n_z_tmp = s.nbin* n_z/sum(n_z);
cn_z=cumsum(n_z_tmp);
% plot(z_vals,cn_z)
eps=1e-5;
z_cuts=interp1(cn_z,z_vals,1:(s.nbin-1));
z_cuts_l = [0 z_cuts];
z_cuts_u = [z_cuts max(z_vals)];

% find bin divisions for zbias and deltaz bins
if (s.zbias_zcut_meth=='ngal')
    % normalise in a way that makes it easier to find z bin divisions
    n_z_tmp = s.nbin_zbias* n_z/sum(n_z);
    cn_z=cumsum(n_z_tmp);
    % plot(z_vals,cn_z)
    eps=1e-5;
    z_cuts=interp1(cn_z,z_vals,1:(s.nbin_zbias-1));
    z_bias_cuts_l = [0 z_cuts];
    z_bias_cuts_u = [z_cuts max(z_vals)];
elseif (s.zbias_zcut_meth=='z')
    z_cut_step=(max(z_vals)-min(z_vals))/(s.nbin_zbias);
    z_cuts=min(z_vals):z_cut_step:max(z_vals);
    z_bias_cuts_l=z_cuts(1:(length(z_cuts)-1));
    z_bias_cuts_u=z_cuts(2:length(z_cuts));
end

% make P(z_phot|z_spec) on a 2d grid
nz=length(z_vals);
dz=z_vals(2)-z_vals(1); % should really check they are evenly spaced
pzpzs=zeros(nz,nz);
vbx=3;
for iz=1:nz
    z_t=z_vals(iz);
    if (verb>=vbx) clf; end
    
    % z_bias and deltaz for this bin
    ibias_bin=find((z_t>=z_bias_cuts_l)&(z_t<=z_bias_cuts_u));
    if ((length(ibias_bin)~=1)&(length(ibias_bin)~=2))
        z_t
        z_bias_cuts_l
        z_bias_cuts_u
    end
    ibias_bin=ibias_bin(1);
    z_bias=s.zbiass(ibias_bin);
    deltaz=s.deltazs(ibias_bin);
    
    % P_stat
    z_m=z_t+z_bias;
    if (strcmp(s.zbias_deltaz_def,'sigmaz'))
        sigma_z=deltaz;
    else
        sigma_z=deltaz * (1+z_m); % red-shift errors are deltaz*(1+z)
    end
    p_stat=(1/(sqrt(2*pi)*sigma_z))*exp(-(z_vals-z_m).^2 / (2*sigma_z^2));    
    %z_m_test=sum(z_vals.*p_stat)*dz, % assumes equally spaced z values
    %sqrt(sum(z_vals.^2.*p_stat)*dz- z_m_test^2), % I assume A2 should be this!?
    if (verb>=vbx) plot(z_vals,p_stat); end
    
    % P_cat_minus
    z_cat=z_t-s.Deltaz+z_bias;
    if (strcmp(s.zbias_deltaz_def,'sigmaz'))
        sigma_z=deltaz;
    else
        sigma_z=deltaz * (1+z_cat); % red-shift errors are deltaz*(1+z)
    end
    p_cat_minus=(1/(sqrt(2*pi)*sigma_z))*exp(-(z_vals-z_cat).^2 / (2*sigma_z^2));
    %z_m_test=sum(z_vals.*p_cat_minus)*dz
    %sqrt(sum(z_vals.^2.*p_cat_minus)*dz-z_m_test^2)
    if (verb>=vbx)
        hold on
        plot(z_vals,p_cat_minus,'r-')
    end
    
    % P_cat_plus
    z_cat=z_t+s.Deltaz+z_bias;
    if (strcmp(s.zbias_deltaz_def,'sigmaz'))
        sigma_z=deltaz;
    else
        sigma_z=deltaz * (1+z_cat); % red-shift errors are deltaz*(1+z)
    end
    p_cat_plus=(1/(sqrt(2*pi)*sigma_z))*exp(-(z_vals-z_cat).^2 / (2*sigma_z^2));    
    %z_m_test=sum(z_vals.*p_cat_plus)*dz
    %sqrt(sum(z_vals.^2.*p_cat_plus)*dz-z_m_test^2)
    if (verb>=vbx) plot(z_vals,p_cat_plus,'g-'); end
    
    % add it all up
    p_tot=(1-s.fcat)*p_stat + s.fcat/2 * (p_cat_minus+p_cat_plus); % ?? must surely be a factor of 2 here?
    % normalise, in case e.g. p_cat_minus is outside range ????
    p_tot=p_tot/(sum(p_tot)*dz);
    if (verb>=vbx)
        plot(z_vals,p_tot,'k:')
        axis([0 3 0 6])
        pause(0.0001)
    end
    
    % get P(z_t,z_phot)=P(z_phot|z_t)*P(z_t)
    %    pzpzs(:,iz)=p_tot;
    pzpzs(:,iz)=p_tot*n_z(iz);

end

if (verb>=2)
    imagesc(z_vals,z_vals,pzpzs); colorbar; set(gca,'ydir','normal')
    n_z_mat=meshgrid(n_z,z_vals);
    %imagesc(z_vals,z_vals,n_z_mat); colorbar; set(gca,'ydir','normal')
    imagesc(z_vals,z_vals,pzpzs.*n_z_mat); colorbar; set(gca,'ydir','normal')
end


%% Now bin it

% cut up the z distribution
for ibin=1:s.nbin
    iz=find((z_vals>z_cuts_l(ibin))&(z_vals<z_cuts_u(ibin)));
    n_z_tot(:,ibin)=sum(pzpzs(iz,:),1);
    ngals_bin(ibin)=sum(n_z_tot(:,ibin));
    % normalise wrt chi later
end
ngals_bin=ngals_bin*s.ng/sum(ngals_bin);

% check everything
if (verb>=1)
    %clf
    for ibin=1:s.nbin
        if (mod(ibin,2)==1) col='r-'; else col='b--'; end
        plot(z_vals,n_z_tot(:,ibin),col)
        hold on
    end
end

% nomalise
for ibin=1:s.nbin
    norm_nz=sum(n_z_tot(:,ibin));
    n_z_tot(:,ibin)=n_z_tot(:,ibin)/norm_nz;
end
