function [dndm dndm_ps M_star sigma]=massfn(k_vals,pk_vals,M_vals,Omega_m,growth,z)
%
% Find the number of halos as a function of mass 
% given a matter power spectrum.
%
% DNDM = MASSFN(K,PK,M,OMEGA_M)
%
% K is the wavenumbers at which the power spectrum
% is defined (need good sampling for good results)
% in units of h/Mpc
%
% PK is the present day linear theory matter 
% power spectrum (e.g. from pkf)
%
% M are the mass values at which the mass function 
% is required (better sampling improves results)
%
% OMEGA_M is the matter density e.g. 0.3
%
% DNDM is the number of halos per unit mass as given
% by Sheth-Tormen. In units of (Mpc/h)^-3 / (M_solar/h)
%
%
% [DNDM DNDM_PS] = MASSFN(K,PK,M,OMEGA_M,GROWTH)
%
% Additionally gives the Press Schechter mass function,
% DNDM_PS
%
% Can additionally specify GROWTH, the growth factor
% at the redshift reqd. 
%
% SLB 14 July 2006
%
% Added the z-dependence of rho_m_z! 16/12/08 KM
% Added reshape in line 57, so that new version of sigma_frompk works!

if (~exist('growth')) growth=1; end % i.e. assume z=0
if (~exist('z')) z=0; 
    fprintf(1,'ASSUMING z=0 in massfn.m!!!\n')
end % i.e. assume z=0

% standard parameters
param.G = 6.673e-11; % SI units
param.c =  299792458; % m/s
param.Mpc_to_m = 1e6 * 3.0856e16; % number of meters in 1Mpc
param.H_0 = 100 * 1e3 / param.Mpc_to_m; % h/s
param.M_solar = 1.989e30; % kg
param.rho_crit = 3 * param.H_0^2 / (8 *pi * param.G) /param.M_solar *param.Mpc_to_m^3; % h^2 M_solar / Mpc^3

% Find sigma_M
% because rho_crit is h^2 M_solar / Mpc^3
rho_m=Omega_m*param.rho_crit; % NB. is not a fn of z
rho_m_z = rho_m*(1+z)^3;
%rho_m_z = rho_m; fprintf(1,'RHO_M not TIME DEPENDANT in massfn.m!\n')
clear sigma
pk_vals = reshape(pk_vals,size(k_vals));
for im=1:length(M_vals)
    R(im) = (M_vals(im)/(rho_m_z * 4 * pi/3))^(1/3); % in Mpc/h
    %R(im) = (M_vals(im)/(rho_m * 4 * pi/3))^(1/3); % in Mpc/h % 18.05.2011 KM
    % Find sigma_R - great, I already seem to have a function!
    sigma(im)=sigmar_frompk(k_vals,pk_vals,R(im));
end
%loglog(M_vals,sigma,'bx')
% Acccording to text after eq 2.1 of 9601088 (Eke et al)
% don't actually want sigmaR(z). sigma in PS eqn is the 
% present day rms fluctuation. Good.

% differentiate to get d_ln_sigma_by_d_ln_M
% This is where good sampling in M_vals is important
%clear d_ln_s_by_d_ln_M
for im=1:(length(M_vals)-1)
    % Natural log is reqd. NB log is ln in matlab (cf log10)
    d_ln_M = log(M_vals(im+1))- log(M_vals(im));
    d_ln_sigma = log(sigma(im+1)) - log(sigma(im));
    d_ln_s_by_d_ln_M(im) = d_ln_sigma / d_ln_M;
end
d_ln_s_by_d_ln_M(im+1) = d_ln_s_by_d_ln_M(im); % fill the last one

% subs into dn/dM formula (using eq 2.1 of 9601088)
%z_vals=0;
%delta_c=1.686*(1+z_vals); % "Conventional for Omega_m=1"
% "This is the value, extrapolated to the present using linear
% theory, of the overdensity of a uniform spherical overdense 
% region at the point at which the exact non-linear model predicts
% that it should collapse to a singularity."
% Looks like for Omega_m.ne.1 then it is better to use:
delta_0 = 1.686; % Looks like?
delta_c = delta_0 ./ growth;
dndm_ps = -(2/pi)^0.5 .* (rho_m./M_vals.^2) .* (delta_c./sigma) ...
    .* d_ln_s_by_d_ln_M .* exp(-delta_c^2./(2*sigma).^2);
% so must be in units (Mpc/h)^-3 / (M_solar/h)

% might as well try Sheth-Tormen too
% use eqn 6 of Vale and Ostriker astro-ph/0402500
A=0.5;%0.322; 
a=0.707; q=0.3;
% FROM JENKINS ET AL.!08.04.2010 KM!
%A=0.353; q=0.175; a=0.73; % got rid of this, 18.05.2011 KM!
%growth=1; % for z=0;
nu=sqrt(a)* delta_0 ./ (growth.*sigma);
d_nu_by_d_M = - nu./sigma .* d_ln_s_by_d_ln_M ./ (M_vals./sigma);
%dndm = A * (1+ 1./nu.^(2*q)).* sqrt(2/pi) .* (rho_m./M_vals) ...
%    .* d_nu_by_d_M .* exp(-nu.^2/2);
dndm = A * (1+ 1./nu.^(2*q)).* sqrt(2/pi) .* (rho_m./M_vals) ...
    .* d_nu_by_d_M .* exp(-nu.^2/2);% *a; % 19.05.2011 KM, hmmmm

% get M_star in case handy
% Seljak 0001493 after eqn 8, says this is where nu=1
if nargout>2 M_star = interp1(nu/sqrt(a),M_vals,1); end % Added if here KM
% removed the sqrt(a) fqctor here! 12.04.2010 KM