function [fish]=get_lens_fisher_doall(cospars_fid,nzpars,...
    ell_vals,z_vals,fish);
%    pars_step,parnames,verb,prior)
% e.g.
% fish.names={'Omega_m','w','wa','h','sigma_8','Omega_b','n_s','GI'};
% fish.steps=[0.01 0.01 0.01 0.01 0.01 0.005 0.005 0.01];
% fish.prior=inv(diag([10 10 10 10 10 10 10 10]).^2);
% fish.verb=1;
%

tstart_time=clock;

if (~isfield(fish,'verb')) fish.verb=0; end

% fiducial cls
[cls_vals_fid, lens_cls_fid, GI_cls_fid]=lens_GI_cls_doall(...
    cospars_fid,cospars_fid.nzpars,ell_vals,z_vals);
% get the covariances
[cov_cobs]=get_lenscl_cov(ell_vals,lens_cls_fid,cospars_fid.nzpars,z_vals);
%[cov_cobs]=get_lenscl_cov(ell_vals,cls_vals_fid,nzpars,z_vals);
% check the fiducial cls
if (fish.verb>0)
    %clf
    iplot=15;
    loglog(ell_vals,abs(GI_cls_fid(:,iplot))'.*ell_vals.^2/(2*pi),'k-')
    hold on
    loglog(ell_vals,lens_cls_fid(:,iplot)'.*ell_vals.^2/(2*pi),'r--')
    loglog(ell_vals,abs(cls_vals_fid(:,iplot))'.*ell_vals.^2/(2*pi),'b:')
end

% work out which parameters are varied
fish.ipuse=find(abs(fish.steps)>0);
npars=length(fish.ipuse);

% calculate derivatives wrt parameters
dcl_by_dpar=zeros(npars,length(ell_vals),length(lens_cls_fid(1,:)));
tstartp=clock;
for jp=1:npars
    ip=fish.ipuse(jp);
    if (fish.verb>1) progress(jp,npars,tstartp,1,'   Fisher steps loop'); end
    
    % Make the fisher step
    %* [cospars_step]=do_fish_step(cospars_fid,fish,jp);
    cospars_step=cospars_fid;
    cospars_step.vect(ip)=cospars_fid.vect(ip)+fish.steps(ip);
    cospars_step=change_cospars(cospars_step,0); % update values used from vect

    % Get new observables for the stepped parameters
    [cls_vals_step lens_cls_step GI_cls_step]=lens_GI_cls_doall(...
        cospars_step,cospars_step.nzpars,ell_vals,z_vals);
    
    % leave the option of artificially removing the dependence of lensing
    % on cosmology
    if (cospars_fid.dolens==0)
        cls_vals_step=GI_cls_step+lens_cls_fid;
    end
    
    % Optionally do plots
    if (fish.verb>1)
        loglog(ell_vals,abs(GI_cls_step(:,iplot))'.*ell_vals.^2/(2*pi),'k:')
        loglog(ell_vals,lens_cls_step(:,iplot)'.*ell_vals.^2/(2*pi),'r--')
        loglog(ell_vals,abs(cls_vals_step(:,iplot))'.*ell_vals.^2/(2*pi),'b-')
    end

    % Get derivative
    dcl_by_dpar(jp,:,:) = (cls_vals_step-cls_vals_fid)/fish.steps(ip);

end

% Get Fisher matrix
fish.matrix=get_lens_fisher(fish.steps(fish.ipuse),ell_vals,dcl_by_dpar,cov_cobs);

% store some things in case handy later
fish.runtime=etime(clock,tstart_time);
if (~isfield(fish,'prior')) fish.prior = inv(diag([fish.steps(fish.ipuse)*1e6]).^2); end
fish.matrixp=fish.matrix+fish.prior;
fish.margpars=sqrt(diag(inv(fish.matrixp)));
fish.dcl_by_dpar=dcl_by_dpar;

% optionally make a summary to cut out and keep
if (fish.verb>0)
    fprintf(1,'     z space = %5.4f; n ells = %i; runtime = %8.3f s\n',...
        z_vals(2)-z_vals(1),length(ell_vals),fish.runtime);
    for jp=1:npars
        ip=fish.ipuse(jp);
        fprintf(1,' d%s = %5.5f; ',cospars_fid.names{ip},fish.margpars(jp));
    end
    margpars_store=fish.margpars;
    fprintf(1,'\n');
end