function pk_nl_vals = pk_nonlin(k_vals,pk_lin,g)

clear PowerN
for i=1:(length(k_vals)-1)
    dP=pk_lin(i+1)-pk_lin(i);
    dk=k_vals(i+1)-k_vals(i);
    k_L=k_vals(i);
    P=pk_lin(i);
    n=(dP/dk)*(k_L/P); % PD96 eqn 22, but with k=k_L
    B=0.226*(1+n/3)^(-1.778); % PD96 eq24: "describes a second-order deviation from linear growth"
    A=0.482*(1+n/3)^(-0.947); % PD96 eq23: "A and alpha parameterise the power-law
    alpha=3.31*(1+n/3)^(-0.244); % which dominates the function in the quaisi-linear regime"
    V=11.55*(1+n/3)^(-0.287); % PD96 eq27: "virialization parameter giving the amplitude of the f_NL(x) \propto x^(3/2) asymptote"
    beta=0.862*(1+n/3)^(-0.287); % PD96 eq26: "softens the transition between these two regimes"
    %x=k_L^3*h*P/(2*pi^2); %Fergus says: Finding deltasqlin. Note factor of h to fix units
    % !!! understand factor of h/(2*pi^2); I cannot believe the h
    x=k_L^3*P/(2*pi^2); %Finding deltasqlin. Note factor of h to fix units
    % f_NL(x), PD96 eqn 21; relates to Delta_sq_NL using PD96 eq6
    Delta_sq_NL=x*((1+B*beta*x+(A*x)^(alpha*beta))/(1+((A*x)^alpha*g^3/V/sqrt(x))^beta))^(1/beta);
    %
    k_NL(i) =k_L*(1+Delta_sq_NL)^(1/3); % PD96 eq5; Modify k, as in Peacock pg 511 (eq 16.63)
    %PowerN(i)=Delta_sq_NL*(2*pi^2)/h/(k_NL(i))^3; %Fergus says: Extract the nonlinear power spectrum.
    % !!! understand factor of h/(2*pi^2)
    PowerN(i)=Delta_sq_NL*(2*pi^2)/(k_NL(i))^3; %Fergus says: Extract the nonlinear power spectrum.    
end
pk_nl_vals=interp1(k_NL,PowerN,k_vals);