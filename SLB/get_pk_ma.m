function [pk_vals]=get_pk_ma(cospars,k_vals);

% make a look-up table of power spectrum estimates
Gamma= exp(-2 * cospars.Omega_b * cospars.h) *cospars.Omega_m * cospars.h;
%Gamma= exp(- Omega_b*(1+sqrt(2*h)/Omega_m )) *Omega_m * h; % from Refregier et al
% !!! NB. I'm not sure this fits with the coefs in pkf??!
pk_vals=pkf(Gamma, cospars.sigma_8, k_vals,cospars.n_s,cospars.n_run,cospars.k_pivot);
