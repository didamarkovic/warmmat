function [p_delta_gammaI p_EE_gammaI]=get_p_delta_gammaI(cospars,z_vals,k_vals);
% Get p_delta_gammaI for GI term

if (strcmp(cospars.GI_method,'linear'))
    [p_delta_gammaI p_EE_gammaI]=get_pdgI_linear(cospars,z_vals,k_vals);
elseif (strcmp(cospars.GI_method,'linear_kzbin'))
    [p_delta_gammaI p_EE_gammaI]=get_pdgI_linear_kzbin(cospars,z_vals,k_vals);
else
    %fprintf(1,'ERROR! Unknown cospars.GI_method %s in get_p_delta_gammaI.m\n',cospars.GI_method);
    %p_delta_gammaI=NaN;
    string=sprintf('ERROR! Unknown cospars.GI_method: %s \nin get_p_delta_gammaI.m\n',cospars.GI_method);
    error(string,'Unknown GI method\n');
end

% plot z dependence
if (1==0)
    %clf
    %cospars.col='kx'
    subplot(1,2,1)
    loglog(1+z_vals,abs(p_delta_gammaI(500,:)),cospars.col)
    %plot([(1+z_vals(1)) (1+z_vals(length(z_vals)))],[p_delta_gammaI(500,1) p_delta_gammaI(500,length(z_vals))],'k-')
    ylabel('p_delta_gammaI')
    xlabel('1+z')
    hold on
    %
    subplot(1,2,2)
    loglog(1+z_vals,p_EE_gammaI(500,:),cospars.col)
    ylabel('p_EE_gammaI')
    xlabel('1+z')
    hold on
    %
    pause(0.0001)
    return
end