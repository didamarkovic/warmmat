function [chisq chisqmap npoints gamma_1_trial gamma_2_trial]=nfwechisq(...
    nfwepars,theta_x,theta_y,...
    gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad)
% Obtain the chisq between some shear data and a NFW model
%
% CHISQ = NFWECHISQ(NFWEPARS,THETA_X,THETA_Y,GAMMA_1,GAMMA_2,NOISE_G)
%
% CHISQ is the chisq value from the trial NFW model parameterised
% by NFWPARS=[M200 CONC THETA_CX THETA_CY,E1,E2]
% and the data given by THETA_X,THETA_Y,GAMMA_1_OBS, GAMMA_2_OBS
% and NOISE_G
%
% where
% SIGMAV is the velocity dispersion of the SIS in units of km/s
% THETA_CX, THETA_CY is the center of the lens
% E1, E2 describe the ellipticity of the lens
%        E1 = E cos(2*PHI), E2 = E sin(2*PHI)
%        where E is (a-b)/(a+b) of the lens, 
%        PHI is angle of major axis anticlockwise from +ve x axis
% THETA_X is angle in x direction on the sky, in arcmin, at each shear
% THETA_Y is angle in y direction on the sky, in arcmin
% GAMMA_1 and GAMMA_2 are the shears in the x, y coordinates
% NOISE_G is the error on GAMMA_1 or GAMMA_2
%
%
% [CHISQ CHISQMAP NPOINTS] = NFWECHISQ(NFWEPARS,THETA_X,THETA_Y,...
%                                GAMMA_1,GAMMA_2,NOISE_G1,NOISE_G2,MINRAD)
%
% NOISE_G1 is the noise on GAMMA_1
% NOISE_G2 is the noise on GAMMA_2 (by default assumed same as NOISE_G1)
%
% MINRAD enables you to specify to ignore all points within a radius
% of minrad arcmin from the center of the predicted SIS in the chisq.
%
% Where noise_g1=0 or noise_g2=0 the points will be ignored in the
% chisq.
%
% CHISQMAP is the contribution to the chisq from each theta_x, theta_y
% position
%
% NPOINTS= total number of points that were in practice used in the chisq.
%
% SLB 29 June 2006 (adapted from nfwchisq.m)

if (~exist('minrad')) minrad=0.001; end % just cut central point by default
if (~exist('noise_g2')) noise_g2=noise_g1; end
if (~exist('Omega_m')) Omega_m = 0.3; end 
if (~exist('n00')) n00 = 200; end % number of times the critical density to use
if (~exist('nsteps')) nsteps = 10000; end % number of steps to use


1/0;
warning off last

% unpack the data structures
M_200_trial=nfwepars(1);
conc_trial=nfwepars(2);
theta_cx_trial=nfwepars(3);
theta_cy_trial=nfwepars(4);
e1_trial=nfwepars(5);
e2_trial=nfwepars(6);
zclust = nfwepars(7);
zsource = nfwepars(8);
if (length(nfwepars)>=9)
    Omega_m = nfwepars(9);
else
    Omega_m = 0.3;
end
if (length(nfwepars)>=10);
    n00=nfwepars(10);
else
    n00=200;
end
if (length(nfwepars)>=11);
    nsteps=nfwepars(11);
else
    nsteps=10000;
end

% Get the predicted data for this model
[gamma_1_trial, gamma_2_trial, kappa_trial]=nfweshears(theta_x,theta_y,...
    M_200_trial,conc_trial,theta_cx_trial,theta_cy_trial,...
    e1_trial,e2_trial,zclust,zsource,Omega_m,n00,nsteps);

% Calculate chisq
chisqmap_g1 = zeros(size(gamma_1_obs));
chisqmap_g2 = zeros(size(gamma_1_obs));
% cut out shears with noise = 0 (as this is a flag to ignore)
iuse = (noise_g1>0)&(noise_g2>0);
% additionally cut out shears too close to the center 
theta = sqrt((theta_x-theta_cx_trial).^2 + (theta_y-theta_cy_trial).^2);
iuse = iuse .* (theta>minrad);
is = find(iuse);
npoints = length(is)*2;
%
chisqmap_g1(is)=((gamma_1_trial(is)-gamma_1_obs(is))./noise_g1(is)).^2;
chisqmap_g2(is)=((gamma_2_trial(is)-gamma_2_obs(is))./noise_g2(is)).^2;
% if there is a problem, it might be useful to look at the individual
% chisq maps for g1 and g2 separately to find anything odd.
chisqmap=chisqmap_g1+chisqmap_g2;
chisq=sum(sum(chisqmap));

