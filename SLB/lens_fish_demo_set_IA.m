% set some intrinsic alignments stuff for lens_fish_demo


% stuff on intrinsic alignments. Currently set not to use them.
cospars.GI_method='linear_kzbin'; % is assumed to be the same for GI and II
cospars.GI_interpmeth='linear';
cospars.GI_tieCI=0; % tie GI and II CI values to be the same
cospars.GI_fixpk=3; % doesn 't make sense to h#ave =1 if not doing Fisher
cospars.cov_include=1; % what to put in cov mat
cospars.GI_nl=1; % derive GI, II from nl matter power spectrum or not.
fish.verb=2;
% for my records:
tagplus=sprintf('_zstep%5.3f_nell%i_fcat%5.3f_fixpk%i_nl%i_nkv%i',...
    zstep,nell,fcat,cospars.GI_fixpk,cospars.GI_nl,length(k_vals));
cospars.GI_kmin=0.1; cospars.GI_kmax=2;
cospars.GI_zmin=zmin; cospars.GI_zmax=zmax;
cospars.dolens=1;
cospars.doGI=1;
cospars.doII=1;
cospars.GI_nk=2;
cospars.GI_nz=1;
cospars.GI_kzbins=zeros(cospars.GI_nk,cospars.GI_nz); % log of factor
cospars.II_kzbins=zeros(cospars.GI_nk,cospars.GI_nz); % log of factor
GI_steps=0*ones(cospars.GI_nk,cospars.GI_nz);
II_steps=0*ones(cospars.GI_nk,cospars.GI_nz);
