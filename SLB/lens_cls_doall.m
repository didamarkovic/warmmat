function [ell_vals lens_cls_nl_vals lens_cls_lin_vals ]=lens_cls_doall(...
    Omega_m,Omega_b,Omega_DE,w,h,sigma_8,...
    alpha,beta,z_0,ell_start,ell_stop,nell,z_plus,...
    xi_plus,z_minus,xi_minus,z_vals,chi,r_AR,k_vals,pk_nl_vals)
% wrapper for lens_cls which does all the preliminary calcs
% Good for general quick use. Should check resolutions of all grids below.
% SLB 2006

if ((~exist('z_plus'))|isempty(z_plus))
    z_plus = 0;
end
if ((~exist('xi_plus'))|isempty(xi_plus))
    xi_plus= 0; % will make f_plus = 1
end
if (~exist('z_minus')|isempty(z_minus))
    z_minus=0;
end
if (~exist('xi_minus')|isempty(xi_minus))
    xi_minus=0; % flag to make f_minus =1
end

% choose z values for integrals
if (~exist('z_vals'))
    z_vals=0.001:0.03:3; % should check higher resolutions to be sure..
end

% make SNAP n(z) with z_m = 1.23 (as mentioned in Fig 2 caption)
% First entry: bin 1 of 2; Second entry: bin 2 of 2
%* n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);              z= 0 0 1 0
clear n_z n_z_tot f_plus f_minus
for ibin=1:length(alpha)
    n_z(:,ibin)=z_vals.^alpha(ibin) .* exp(-(z_vals/z_0(ibin)).^beta(ibin));
    if (xi_plus(ibin)>0)
        f_plus(:,ibin)=1./(1+exp( +(z_plus(ibin)-z_vals)/xi_plus(ibin) ) );
    else
        f_plus(:,ibin) = ones(length(z_vals),1);
    end
    if (xi_minus(ibin)>0)
        f_minus(:,ibin)=1./(1+exp( -(z_minus(ibin)-z_vals)/xi_minus(ibin) ) );
    else
        f_minus(:,ibin) = ones(length(z_vals),1);
    end
    n_z_tot(:,ibin)=n_z(:,ibin).*f_plus(:,ibin).*f_minus(:,ibin);
    % normalise wrt chi later
end

% make a look-up table of distance measures
% see waffly notes on this from earlier versions of this code
c=3e5;  % in units of km s^-1
H_0=100; % in units of h km/s Mpc^-1
D_H=c/H_0; % So D_H is in units of Mpc /h
if ((~exist('r_AR'))|(isempty(r_AR)))
    fprintf(1,'\nCalculating distance measures in lens_cls_doall...');
    [chi r_AR]=Ds_wconst(z_vals,z_vals,Omega_m,Omega_DE,w,1000);
    chi=chi*D_H;
    r_AR=r_AR*D_H; % *comoving* ang diam distance = D_A/a = D_M
end


% choose ell and k values
ell_vals=logspace_ln(ell_start,ell_stop,nell);

if (~exist('k_vals')|(isempty(k_vals)))
    k_vals=logspace_ln(0.0001,2000,2000);
end

if (~exist('pk_nl_vals')|(isempty(pk_nl_vals)))
    fprintf(1,'\nCalculating non-linear matter power spectrum in lens_cls_doall...');
    % make a look-up table of growth factor stuff
    [delta_vals_tmp, z_vals_tmp]=growth_wconst(Omega_m,Omega_DE,w,1000);
    delta_vals=interp1(z_vals_tmp,delta_vals_tmp,z_vals);
    growth_vals=delta_vals./(max(delta_vals));
    growth_sup_vals=growth_sup_wconst(Omega_m,Omega_DE,w,z_vals);


    % make a look-up table of power spectrum estimates
    Gamma= exp(-2 * Omega_b * h) *Omega_m * h;
    %Gamma= exp(- Omega_b*(1+sqrt(2*h)/Omega_m )) *Omega_m * h; % from Refregier et al
    % !!! NB. I'm not sure this fits with the coefs in pkf??!
    %sigma_8=0.88;
    pk_vals=pkf(Gamma, sigma_8, k_vals);
    % convert to a non-linear power spectrum
    [pk_nl_vals pk_lin_vals]=pks_nonlin(k_vals,pk_vals,z_vals,growth_vals,growth_sup_vals);
end

if ((nargout>2)&(exist('pk_lin_vals')))
    lens_cls_lin_vals=lens_cls(ell_vals,z_vals,chi,r_AR,n_z_tot,...
        k_vals,pk_lin_vals,D_H,Omega_m,h);
end
lens_cls_nl_vals=lens_cls(ell_vals,z_vals,chi,r_AR,n_z_tot,...
    k_vals,pk_nl_vals,D_H,Omega_m,h);
