function [fish]=get_lens_fisher(pars_step,ell_vals,dcl_by_dpar,cov_cobs_m);

% check all parameters were varied
tmp=find(abs(pars_step)==0);
if (~isempty(tmp))
    fprintf(1,'ERROR: some parameter steps are zero in get_lens_fisher!\n');
end

% make fisher matrix
fish=zeros(length(pars_step),length(pars_step));
for ip1=1:length(pars_step);
    for ip2=1:length(pars_step);
        clear tmp
        for il=1:length(ell_vals)
            cov=squeeze(cov_cobs_m(il,:,:)); % covariance matrix at this ell
            dx1= squeeze(dcl_by_dpar(ip1,il,:));
            dx2= squeeze(dcl_by_dpar(ip2,il,:));
            tmp(il)=dx1'*inv(cov)*dx2;
        end
        fish(ip1,ip2)=sum(tmp);
        fish(ip2,ip1)=fish(ip1,ip2);
    end
end