%%lighten the current colormap
cmaptmp=colormap;
cmap(:,1)=0.5+0.5*cmaptmp(:,1);
cmap(:,2)=0.5+0.5*cmaptmp(:,2);
cmap(:,3)=0.5+0.5*cmaptmp(:,3);
colormap(cmap);
