function [gamma_1 gamma_2 kappa mod_gamma] = nfwshears(theta_x,theta_y,...
    M_200,conc,theta_cx,theta_cy,zclust,zsource,Omega_m,n00)
%
% Calculate shears for NFW model at given angular positions on sky.
%
% [GAMMA_1 GAMMA_2]=nfwshears(THETA_X,THETA_Y,M200,CONC,...
%                              THETA_CX, THETA_CY, ZCLUST, ZSOURCE)
%
% THETA_X is angle in x direction on the sky, in arcmin, at each shear reqd
% THETA_Y is angle in y direction on the sky, in arcmin
% M200 is the mass enclosed in a sphere of radius r200 centered on the
%      cluster, where r200 is defined such that the mean density in the 
%      sphere is 200 times the critical density.
%      In units of h^-1 M_solar
% CONC is the NFW concetration parameter 
% THETA_CX, THETA_CY is the center of the lens.
% ZCLUST is the redshift of the cluster 
% ZSOURCE is the redshift of the background galaxies
%
% GAMMA_1 and GAMMA_2 are the shears wrt the theta_x, theta_y coordinates
%
%
% [GAMMA_1 GAMMA_2 KAPPA]=nfwshears(THETA_X,THETA_Y,M200,CONC,...
%                                   THETA_CX, THETA_CY, ZCLUST, ZSOURCE, ...
%                                                  OMEGA_M, N00)
%
% OMEGA_M is the matter density (present day). Assumes flat universe, w=-1
% is 0.3 by default.
% N00 is 200 by default. If N00 = 500, this instead uses M500
% KAPPA is the convergence
%
% SLB 7 April 2006

% set defaults
if (~exist('theta_cx')) theta_cx =0; end 
if (~exist('theta_cy')) theta_cy =0; end 
if (~exist('Omega_m')) Omega_m = 0.3; end 
if (~exist('n00')) n00 = 200; end % number of times the critical density to use

% x position of each point on grid, relative to center
dtheta_x=theta_x-theta_cx; 

% y position of each point on grid, relative to center
dtheta_y=theta_y-theta_cy; 

% polar coords angle of each point on grid, from center, anticlockwise from +ve x axis
theta=atan2(dtheta_y,dtheta_x); 
% clf; imagesc(theta_x,theta_y,angle); set(gca,'ydir','normal'); colorbar

% polar coords distance of each point on grid, from center
mod_theta=sqrt( dtheta_x.^2 + dtheta_y.^2 );

% get the nfw quantities at all these positions
[mod_gamma kappa] = nfwshear(mod_theta, M_200,conc,zclust,zsource,Omega_m,n00);
 
% convert to gamma_1 and gamma_2
gamma_1=-mod_gamma.*cos(2*theta); % not sure how to justify minus sign..
gamma_2=-mod_gamma.*sin(2*theta);
% check
%imagesc(theta_x,theta_y,gamma_1); set(gca,'ydir','normal'); colorbar
%imagesc(theta_x,theta_y,gamma_2); set(gca,'ydir','normal'); colorbar

% Fudge the difficult bits.
% In practice won't have any galaxy shape estimates at the peak of the SIS
% anyway.
gamma_1(find(isnan(gamma_1)))=0;
gamma_2(find(isnan(gamma_2)))=0;
kappa(find(isnan(kappa)))=0;