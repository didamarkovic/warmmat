% Try to match Fig 2 of astro-ph/0304419v2 (SNAP3, Refregier et al)
% NB it won't match exactly because they are fixing to COBE 
% normalisation (a pain to code up and is anyway now out of date)
% whereas I am fixing sigma_8 (<-> present day amplitude of matter
% power spectrum)

clear
home='/home/ug_km/';
% e.g. if running on starlink machines:
%path(path,'/import/cyan/codeshare/sarah/gravlens/')
%path(path,'/import/cyan/codeshare/sarah/joint/')
% on my laptop I need:
%path(path,[home,'work/gravlens/mstuff/'])
%path(path,[home,'work/joint/mstuff/'])

% set parameters
Omega_b=0.047;
h=0.7;
sigma_8=0.9;
alpha=2; % parameters for n(z)
beta=2; 
z_0=1.13;
ell_start=10; % lowest ell value to use
ell_stop=2e5;
nell=500; % number of ell values at which to calculate power spectrum

% Do the calcs for the blue lines: Omega_m=0.3; w=-1
fprintf(1,'\n    Doing calculations for the blue lines...');
Omega_m=0.3;
Omega_DE=1-Omega_m;
w=-1;
sigma_8=0.9; % they don't specify this so is hard to match up.. guessing..
[ell_vals_blue lens_cls_nl_vals_blue lens_cls_lin_vals_blue ]=...
    lens_cls_doall(Omega_m,Omega_b,Omega_DE,w,h,sigma_8,...
    alpha,beta,z_0,ell_start,ell_stop,nell);
 
% Red line: Omega_m=0.35; w=-1  .. sigma8=?
fprintf(1,'\n    Doing calculations for the red line...');
Omega_m=0.35;
Omega_DE=1-Omega_m;
[ell_vals_red lens_cls_nl_vals_red]=...
    lens_cls_doall(Omega_m,Omega_b,Omega_DE,w,h,sigma_8,...
    alpha,beta,z_0,ell_start,ell_stop,nell);

% Green line: Omega_m=0.3; w=-0.7
fprintf(1,'\n    Doing calculations for the green line...');
Omega_m=0.3;
Omega_DE=1-Omega_m;
w=-0.7;
[ell_vals_green lens_cls_nl_vals_green]=...
    lens_cls_doall(Omega_m,Omega_b,Omega_DE,w,h,sigma_8,...
    alpha,beta,z_0,ell_start,ell_stop,nell);

% make a nice figure
clf
fs=16;
lw=1.5;
set(gca,'fontsize',fs,'linewidth',lw); % not essential but nicer
tmp=ell_vals_blue.*(1+ell_vals_blue).*lens_cls_lin_vals_blue/(2*pi);
h1=loglog(ell_vals_blue,tmp,'b-');
hold on
tmp=ell_vals_blue.*(1+ell_vals_blue).*lens_cls_nl_vals_blue/(2*pi);
h2=loglog(ell_vals_blue,tmp ,'b-');
tmp=ell_vals_red.*(1+ell_vals_red).*lens_cls_nl_vals_red/(2*pi);
h3=loglog(ell_vals_red,tmp,'r--');
tmp= ell_vals_green.*(1+ell_vals_green).*lens_cls_nl_vals_green/(2*pi);
h4=loglog(ell_vals_green,tmp,'g-.');
axis([10 2e5 1e-7 1e-3])
xlabel('l')
ylabel('l(l+1) C_l / (2\pi)')
set([h1 h2 h3 h4],'linewidth',lw)
legend([h1 h3 h4],'\Omega_m = 0.3  w=-1','\Omega_m = 0.35  w=-1',...
    '\Omega_m = 0.3  w=-0.7',4)
%get(gca), % see all the properties of the current axes
set(gca,'ticklength',[0.02 0.02])
% Note that we do not expect it to match exactly because we didn't
% use COBE normalisation. So they can all be shifted up or down by 
% a bit.

set(gcf,'PaperPositionMode','auto') % WYSIWYG (it doesn't mess up layout)
%print('-dpng','SNAP3_fig2.png'); % a nice compact file format
%print('-dpsc','SNAP3_fig2.ps'); % a format the printer understands

