function [kappa rho_s r_s] = nfwekappa(theta_x,theta_y,...
    M_200,conc,theta_cx,theta_cy,e1,e2,zclust,zsource,Omega_m,n00,nsteps,verbose,mn00type)
%
% Calculate kappa map for elliptical mass NFW model 
% at given angular positions on sky.
%
% [KAPPA]=nfwekappa(THETA_X,THETA_Y,DDS_OVER_DS,M200,CONC,...
%                              THETA_CX, THETA_CY, E1, E2, ZCLUST, ZSOURCE)
%
% THETA_X is angle in x direction on the sky, in arcmin, at each shear reqd
% THETA_Y is angle in y direction on the sky, in arcmin
% M200 is the mass enclosed in a sphere of radius r200 centered on the
%      cluster, where r200 is defined such that the mean density in the 
%      sphere is 200 times the critical density.
%      In units of h^-1 M_solar
% CONC is the NFW concetration parameter 
% THETA_CX, THETA_CY is the center of the lens.
% E1, E2 describe the ellipticity of the lens
%        E1 = E cos(2*PHI), E2 = E sin(2*PHI)
%        where E is (a-b)/(a+b) of the lens, 
%        PHI is angle of major axis anticlockwise from +ve x axis
% ZCLUST is the redshift of the lens
% ZSOURCE is the redshift of the background galaxies
% KAPPA is the convergence
%
% [KAPPA]=nfwshears(THETA_X,THETA_Y,DDS_OVER_DS,M200,CONC,...
%                                   THETA_CX, THETA_CY, E1, E2, ZCLUST, ZSOURCE, ...
%                                                  OMEGA_M, N00,NSTEPS,VERBOSE)
%
% OMEGA_M is the matter density (present day). Assumes flat universe, w=-1
% is 0.3 by default.
% N00 is 200 by default. If N00 = 500, this instead uses M500
%
% SLB 28 Aug 2006: is a tiny modification only of nfweshears

% set defaults
if (~exist('theta_cx')) theta_cx =0; end 
if (~exist('theta_cy')) theta_cy =0; end 
if (~exist('Omega_m')) Omega_m = 0.3; end 
if (~exist('n00')) n00 = 200; end % number of times the critical density to use
if (~exist('nsteps')) nsteps = 10000; end % number of steps to use
if (~exist('verbose')) verbose=1; end % be verbose by default
if (~exist('mn00type')) mn00type = 'critical'; end 

% Make coordinates aligned with major axis of ellipse (for making kappa)
dtheta_x = theta_x - theta_cx;
dtheta_y = theta_y - theta_cy;
e = sqrt(e1^2 + e2^2);
phi=0.5*atan2(e2,e1); % angle of lens to +ve x axis
f = (1-e)/(1+e); % minor to major axis ratio
r=sqrt( dtheta_x.^2 + dtheta_y.^2 );
angle= atan2(dtheta_y,dtheta_x);
x=r.*cos(angle-phi);
y=r.*sin(angle-phi);
theta = sqrt( x.^2 * f + y.^2 / f);

% Make 1d NFW profile kappa(theta)
theta_max = max(max(theta));
dtheta=theta_max / nsteps; 
theta_vals=dtheta:dtheta:(theta_max+dtheta); % need extra step for differentiating kappa 
[tmp kappa_vals rho_s r_s] = nfwshear(theta_vals,M_200,conc,zclust,zsource,Omega_m,n00,mn00type);
%clf
%plot(theta_vals,kappa_vals,'b-')

% Make 2d kappa map
indices = round(theta/dtheta); % nearest neighbor interpolation
null=find(indices==0);
indices(null)=1;
kappa=kappa_vals(indices);
kappa(null)=0;

