function [pk_nl_vals pk_lin_vals]=pks_nonlin(k_vals,pk_lin,z_vals,growth_vals,growth_sup_vals)
% 
% Find the non-linear matter power spectrum at a range of redshifts
% given a redshift zero linear matter power spectrum and the
% growth factor (and growth suppression factor) at the reqd
% redshifts.
%
% PK_NONLIN = PKS_NONLIN(K_VALS,PK_LIN,Z_VALS,GROWTH_VALS,GROWTH_SUP_VALS)
%
% K_VALS are the wavenumbers (in h Mpc^1) at which PK_LIN is given
%        these have to be reasonably closely spaced for the conversion
%        to non-linear to be accurate
% PK_LIN is the linear theory matter power spectrum 
% Z_VALS are the redshifts at which the PK_NONLIN is required
%        and at which GROWTH_VALS and GROWTH_SUP_VALS are specified
% GROWTH_VALS is the growth factor at redshifts Z_VALS
% GROWTH_SUP_VALS is the growth suppression factor at Z_VALS
%
% See PKS_NONLIN_DEMO for a demo
%
% SLB 12 July 2006

pk_lin_vals=zeros(length(k_vals),length(z_vals));
pk_nl_vals=zeros(length(k_vals),length(z_vals));
for iz=1:length(z_vals)
    %z_vals(iz);
    pk_lin_vals(:,iz) = pk_lin* growth_vals(iz).^2;
    g=growth_sup_vals(iz);
    pk_nl_vals(:,iz) = pk_nonlin(k_vals,pk_lin_vals(:,iz),g);
end
