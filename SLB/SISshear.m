clear

%this finds shear for Singular Isothermal Sphere model

consts;
sigma_v = 500;
theta = 0.1:0.5:10;
Dds_by_Ds = 1; %distant source approx.

shear = 2*pi*((sigma_v/c)^2)*(Dds_by_Ds).*(1./theta)

%this plots shear vs. ang rad

plot(theta,shear)