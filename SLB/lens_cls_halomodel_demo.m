%%%% Try to reproduce Fig 2 b of Cooray, Hu & Miralde-Escude
%
% Sarah Bridle, March 2008


clear
%home='d:/home/sarah/'; cd([home,'work/gravlens/mstuff/']); home='../../../';
%addpath([home,'work/gravlens/mstuff/']); addpath([home,'work/joint/mstuff/']);
%outdir =[home,'scratch/gravlens/halomodel/coding_20080227/'];
%fprintf(1,'--------------------------------------------------------------\n');
setoldpath
outdir = 'coding_20080227/';
outtag='_20080227_1930';
%close all
tic

% decide which ell values we want the power spectrum at
n_ell=20;
ell_min=2;
ell_max=1e4;
% number of theta values for FT of kappa for NFW profile
n_theta=400;
% set up for integral over source redshifts
z_s_vals = 1; % only one source redshift for now
% set up for integral over lens redshifts
z_l_step=0.1;
z_l_start=0.02;
% set up for mass integral
M_min=1e6;
%M_max=1e16;
M_max_vals=[1e11 1e12 1e13 1e14 1e15];
n_M=40;
n_k=200;
k_min=0.001;
k_max=200;

% cosmological parameters, for where needed
% use same as Cooray et al
cospars.Omega_b = 0.05;
cospars.Omega_m = 0.35; % SHOULD THIS NOT BE 0.3!
cospars.Omega_DE = 0.7;
cospars.w = -1;
cospars.h = 0.65;
cospars.sigma_8 = 0.9;

% calculate arrays based on settings above
ell_vals=logspace(0,4,n_ell);
z_l_vals=z_l_start:z_l_step:max(z_s_vals);
k_vals=logspace(log10(k_min),log10(k_max),n_k);

% calculate halo model power spectrum
for i_M=1:length(M_max_vals)
    M_max=M_max_vals(i_M);
    fprintf(1,'Calculating power spectrum for maximum mass %1.0e...\n',M_max);
    M_vals=logspace(log10(M_min),log10(M_max),n_M);
    [C_kappa_tots{i_M} C_kappa_1hs{i_M} C_kappa_2hs{i_M}] = lens_cls_halomodel(...
        ell_vals,z_l_vals,z_s_vals,M_vals,k_vals,n_theta,cospars);
end
toc

figure
f1h=0.05;
f2h=1;
clf
for i_M=1:length(M_max_vals)
    C_kappa_1h=C_kappa_1hs{i_M};
    C_kappa_2h=C_kappa_2hs{i_M};
    C_kappa_tot=C_kappa_1h*f1h + C_kappa_2h*f2h;
    loglog(ell_vals,f1h*C_kappa_1h.*ell_vals.*(1+ell_vals)/(2*pi),'k:')
    hold on
%    plot(ell_vals,f2h*C_kappa_2h.*ell_vals.*(1+ell_vals)/(2*pi),'r-')
    plot(ell_vals,C_kappa_tot.*ell_vals.*(1+ell_vals)/(2*pi),'b-')
end
axis([1 1e4 1e-9 1e-3])
title('Weak Lensing Power Spectrum - SLB''s halo code');
xlabel('multipole l'); ylabel('l(l+1)C_l/2\pi');
% saveas(gcf,'figure9-sarahsplot.jpg')
% cf Fig 2b of Cooray et al