function [nfwe_gg2pt_circs gamma_1_circs]=nfwecalcs_26jul06(...
    z_lens,M_200_vals,e1_vals,...
    z_source,theta_vals,...
    Omega_b,Omega_m,Omega_DE,w,h,sigma_8,M_star,angles);

% !!! NB. Some bits of the below assume flat and w=-1 !!

% Calculate c values for each M_200 value
c_0=10; % Take Seljak's advice that this is appropriate for a NFW (alpha=-1)
beta=-0.15; % am a bit unsure whether to use this or -0.2; Check effect
%* [tmp1 tmp2 M_star]=massfns(M_200_vals,z_lens,...
%[tmp1 tmp2 M_star]=massfns(logspace(1e-9,1e17,1000),z_lens,...
%    Omega_b,Omega_m,Omega_DE,w,h,sigma_8);
%    Omega_b,Omega_m,Omega_DE,w,h,sigma_8);
c_vals = c_0 * (M_200_vals/M_star).^beta; % eqn 12 of Seljak 0001493
%clf; semilogx(M_200_vals,c_vals,'rx'); hold on; plot(M_200_vals,c_vals,'r-')
% NB. this relation appears insensitive to z..

n00=200; % number of times critical density to define radius of cluster at
e2=0; % always keep lens aligned with x axis

% sources
if (~exist('angles'))
    angles=0:10:90; % only need to do one quadrant, if weight first and
    % last points half weight (see below average).
    % have checked this gives same answer as full 0:(360-eps), but
    % is faster.
    anglescheat=1;
else
    anglescheat=0;
end
theta_x_circ=theta_vals'*cos(angles*pi/180);
theta_y_circ=theta_vals'*sin(angles*pi/180);
theta_cx=0; % lens is at center
theta_cy=0;
nsteps=10000; % have checked this is enough for masses, scales of interest
% if e1=0.3, e2=0

tstarte=clock;
for ie=1:length(e1_vals)
    e1=e1_vals(ie);
    progress(ie,length(e1_vals),tstarte,1,'\n e1 loop');
    tstartm=clock;
    for im=1:length(M_200_vals)
        %im=1;
        %progress(im,length(M_200_vals),tstartm,1,'     M_200 loop');
        M_200=M_200_vals(im);
        conc=c_vals(im);

        gamma_1_circ=zeros(length(theta_vals),length(angles));
        tstartt=clock;
        % switch to looping over radii, to improve accuracy
        for it=1:length(theta_vals)
            %progress(it,length(theta_vals),tstartt,1,'theta loop');
            theta_x_circ_part=theta_vals(it)'*cos(angles*pi/180);
            theta_y_circ_part=theta_vals(it)'*sin(angles*pi/180);
            [gamma_1_circ_part gamma_2_circ_part] = nfweshears(...
                theta_x_circ_part,theta_y_circ_part,...
                M_200,conc,theta_cx,theta_cy,e1,e2,z_lens,z_source,...
                Omega_m,n00,nsteps,0); % NB. assumes flat, w=-1!!
            gamma_1_circ(it,:)=gamma_1_circ_part;
            gamma_2_circ(it,:)=gamma_2_circ_part;
        end
        gamma_1_circs{im,ie}=gamma_1_circ;
        gamma_2_circs{im,ie}=gamma_2_circ;
    end
end

% average to make corfn
for ie=1:length(e1_vals)
    e1=e1_vals(ie);
for im=1:length(M_200_vals)
    gamma_1_circ=gamma_1_circs{im,ie};
    na=length(angles);
    if (anglescheat==1)
      tmp=[(gamma_1_circ(:,1) + gamma_1_circ(:,na))/2 gamma_1_circ(:,2:(na-1))];
    else
        tmp=gamma_1_circ;
    end
    g1d_circ=mean(tmp,2);
    %gg2pt_circ=g1d_circ*e1/2; % factor of two becau-se e2*e2=0 - need to check!!!
    gg2pt_circ=g1d_circ*e1; 
    nfwe_gg2pt_circs{im,ie}=gg2pt_circ;
end
end