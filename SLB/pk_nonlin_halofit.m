function [pk_nl_smith] = pk_nonlin_halofit(k_vals,pk_lin,g,z,cospars,...
    replicatesmith,exp_k2r2_lookup,r_vals)
% modified version haloformula_halofit to be more matlabified
% SLB 14 Aug 2008

if (~exist('replicatesmith','var')) replicatesmith=0; end

% assume they are making Delta as written in eqn 5 of halofit
% and that they are using V=1
deltak2_to_pk=(4*pi * k_vals.^3 / (2*pi)^3);

% calculate the densities
[om_m tmp om_v]=densities_wa(cospars,z);

% calculate nonlinear wavenumber (rknl), effective spectral index (rneff) and
% curvature (rncur) of the power spectrum at the desired redshift, using method
% described in Smith et al (2002).
Deltasq=pk_lin.*deltak2_to_pk;
dk_vals=k_vals(2:length(k_vals))-k_vals(1:(length(k_vals)-1));
dk_vals(length(k_vals))=dk_vals(length(k_vals)-1);
dlnk_vals=dk_vals./k_vals;
% the above will work if there are enough k steps
%
% find r at which sigma (Smith et al eqn 54) is 1
%r_vals = 1./k_vals; % seems like a reasonable set of r values to try
sigma_vals=zeros(size(r_vals));
for ir=1:length(r_vals)
%    r=r_vals(ir);
    sigma_vals(ir)=sqrt(sum(Deltasq.*exp_k2r2_lookup(:,ir)'.*dlnk_vals));
end
% the following potentially clever method is actually much slower due to
% meshgrid:
%Deltasq_mesh=meshgrid(Deltasq,r_vals)';
%dlnk_mesh=meshgrid(dlnk_vals,r_vals)';
%sigma_vals=sqrt(sum(Deltasq_mesh.*exp_k2r2_lookup.*dlnk_mesh,1));
r_sig1=interp1(sigma_vals,r_vals,1);
%clf                                   %KM - commented, since didnt 
%plot(r_vals,sigma_vals,'bx')          %KM   want plot...
%hold on                               %KM
%v=axis;                               %KM
%plot([v(1) v(2)],[1 1],'k-')          %KM
%axis([0.9*r_sig1 1.1*r_sig1 0.9 1.1]) %KM
%
rknl=1/r_sig1; % nonlinear wavenumber
%
% find neff and rncur at the r where sigma=1
y=r_sig1*k_vals; % following what they did in wint function of halofit
rneff = -3 + 2*sum(dlnk_vals.*Deltasq.*y.^2.*exp(-y.^2)); % neff
rncur = (3+rneff)^2 + 4*sum(dlnk_vals.*Deltasq.*(y.^2-y.^4).*exp(-y.^2)); % C

if (replicatesmith==1)
    % overwrite with their way instead, copied from halofit.f
    sig8=cospars.sigma_8;    % z=0 normalisation of cdm power spectrum (sig8)
    gams=cospars.Gamma;     % shape parameter of cdm power spectrum (gams)
    amp=g;
    xlogr1=-2.0;
    xlogr2=3.5;
    diff=1e9; % initialise
    iloop=10;
    while (abs(diff)>=0.001)
        iloop=iloop+1;
        rmid=(xlogr2+xlogr1)/2.0;
        rmid=10^rmid;
        [sig d1 d2]=wint_halofit(rmid,amp,gams,sig8);
        diff=sig-1.0;
        if (diff>0.001)
            xlogr1=log10(rmid);
        elseif (diff<-0.001)
            xlogr2=log10(rmid);
        end
    end
    rknl=1./rmid;
    rneff=-3-d1;
    rncur=-d2;
end

%c now calculate power spectra for each wavenumber using Smith et al
ndat=length(k_vals);
rk_vals=zeros(size(k_vals));
rknl_vals=zeros(size(k_vals));
plin_vals=zeros(size(k_vals));
pnl_vals=zeros(size(k_vals));
for ik=1:ndat

    k_val=k_vals(ik);
    rk=k_val; % am guessing rk is the wavenumber in units [h Mpc^-1]
    %c linear power spectrum !! Remeber => plin = k^3 * P(k) * constant
    %c constant = 4*pi*V/(2*pi)^3
    %plin=g^2*p_cdm_halofit(rk,gams,sig8);
    plin=Deltasq(ik);
    %c calculate nonlinear power according to halofit: pnl = pq + ph,
    %c where pq represents the quasi-linear (halo-halo) power and
    %c where ph is represents the self-correlation halo term.
    %call halofit(rk,rneff,rncur,rknl,plin,pnl,pq,ph)  % ! halo fitting formula
    pnl=halofit_halofit(rk,rneff,rncur,rknl,plin,om_m,om_v);

    % store things
    rk_vals(ik)=rk;
    rknl_vals(ik)=rknl;
    plin_vals(ik)=plin;
    pnl_vals(ik)=pnl;

end

% convert to the P(k) that I usually use:
pk_nl_smith=pnl_vals./deltak2_to_pk;