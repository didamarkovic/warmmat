function [p_delta_gammaI p_EE_gammaI]=get_pdgI_linear_kzbin(cospars,...
    z_vals,k_vals);
% Get p_delta_gammaI for GI term
% Use the linear alignment model of Hirata & Seljak astro-ph/0406275
% Currently just returns the linear alignment model for the II part!

% Just modify this underlying function
[p_delta_gammaI p_EE_gammaI]=get_pdgI_linear(cospars,z_vals,k_vals);

% Gather infor for interpolation
method=cospars.GI_interpmeth; % is assumed same for GI and II
lkbins=log([k_vals(1) cospars.GI_kbins k_vals(length(k_vals))]);
lxbins=log(cospars.GI_zbins+1); % x=1+z
row=zeros(size(lxbins));
% Pad a line of zeros all the way round the matrix
GI_kzbins=[row' cospars.GI_kzbins' row']'; %This is the log of the multiplicative factor
II_kzbins=[row' cospars.II_kzbins' row']'; %This is the log of the multiplicative factor

if ((cospars.GI_nk>1) & (cospars.GI_nz>1))
    GI_ln_modulation=interp2(lxbins,lkbins,GI_kzbins,log(z_vals+1),log(k_vals)',method);
    II_ln_modulation=interp2(lxbins,lkbins,II_kzbins,log(z_vals+1),log(k_vals)',method);
elseif (cospars.GI_nk==1) & (cospars.GI_nz>1)
    GI_ln_modulation=meshgrid(interp1(lxbins,GI_kzbins,log(z_vals+1),method),k_vals);
    II_ln_modulation=meshgrid(interp1(lxbins,II_kzbins,log(z_vals+1),method),k_vals);
elseif (cospars.GI_nk>1) & (cospars.GI_nz==1)
    GI_ln_modulation=meshgrid(interp1(lkbins,GI_kzbins,log(k_vals),method),z_vals)';
    II_ln_modulation=meshgrid(interp1(lkbins,II_kzbins,log(k_vals),method),z_vals)';
else
    GI_ln_modulation=GI_kzbins;
    II_ln_modulation=II_kzbins;
end
%    end
%end
p_delta_gammaI=p_delta_gammaI.*exp(GI_ln_modulation);
p_EE_gammaI=p_EE_gammaI.*exp(II_ln_modulation);

% plot k dependence
if (1==0)
    clf
    subplot(2,2,1)
    loglog(k_vals,exp(GI_ln_modulation),'bx')
    hold on
    loglog(cospars.GI_kbins,exp(cospars.GI_kzbins),'rx')
    title('Modulation of GI source power spectrum')
    %
    subplot(2,2,3)
    tmp=abs(p_delta_gammaI./exp(GI_ln_modulation));
    loglog(k_vals,tmp(:,1),'bx')
    hold on
    loglog(k_vals,abs(p_delta_gammaI(:,1)),'ro')
    title('GI source power spectrum: P_{\delta \gammaI}')
    %
    subplot(2,2,2)
    loglog(k_vals,exp(II_ln_modulation),'bx')
    hold on
    loglog(cospars.GI_kbins,exp(cospars.II_kzbins),'rx')
    title('Modulation of II source power spectrum')
    %
    subplot(2,2,4)
    tmp=abs(p_EE_gammaI./exp(II_ln_modulation));
    loglog(k_vals,tmp(:,1),'bx')
    hold on
    loglog(k_vals,abs(p_EE_gammaI(:,1)),'ro')
    title('II source power spectrum: P^{EE}_{\gammaI}')
    pause(0.0001)
end

% plot z dependence