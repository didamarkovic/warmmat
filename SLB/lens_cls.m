function [lens_cls_vals, g_vals]=lens_cls(ell_vals,z_vals,chi,r_AR,n_z_tot,...
    k_vals,pk_vals,D_H,Omega_m,h)

% normalise n_z_tot wrt z
% NB. n(z) is shorthand for number per unit redshift, which would be better
% expressed as dn/dz
% The eqns in the books say to normalise int n(chi) dchi = 1
% But presumably when they write n(chi) they don't just mean n(z)
% where z=z(chi), they actually mean number per unit chi...
% i.e. n(chi) would be better written as dn/dchi 
% So, going back to the integral we're supposed to do, note that:
% n(chi) dchi  = dn/dchi dchi = dn/dz dz/dchi dchi = n(z) dz/dchi dchi 
% So int n(chi) dchi = int n(z) dz
nbin=size(n_z_tot,2);
for ibin=1:nbin
    %dchi_nz=zeros(size(z_vals));
    %for ic=2:(length(chi)) % assume n(chi)~=0 for last entry
    %    dchi_nz(ic)=chi(1,ic) - chi(1,ic-1);
    %end
%    norm_nz_tot=sum(n_z_tot(:,ibin).*dchi_nz);
    norm_nz_tot=sum(n_z_tot(:,ibin));
    n_z_tot(:,ibin)=n_z_tot(:,ibin)/norm_nz_tot;
end

% Proof that can't just say n(chi) = n(z(chi))
% p=n_z_tot(:,1);
% dx=dchi_nz';
% dz=z_vals(2)-z_vals(1);
% clf
% plot(z_vals,p.*dx / sum(p.*dx))
% hold on
% plot(z_vals,p.*dz / sum(p.*dz),'r--')


% make a look-up table of weighting function values, g(w)
% from CH eqn 2.59
% g(w_l) = int_(w_s=w_l)^wH dw_s n(w_s) Dds(w_s,w_l) / Ds(w_s)
%  if n(w_s) = delta(z-0.8) i.e. all sources at z=0.8
g_vals = zeros(length(z_vals),nbin);
for ibin=1:nbin
    for iz_l=1:length(z_vals)
        %dchi=zeros(size(z_vals)); 
        int=zeros(size(z_vals));
        for iz_s=(iz_l+1):length(z_vals)
            %dchi(iz_s)=chi(1,iz_s)-chi(1,iz_s-1);
            int(iz_s)=2*n_z_tot(iz_s,ibin) * ...
                r_AR(1,iz_l) * r_AR(iz_l,iz_s) / r_AR(1,iz_s); % iz_l+1 !!! KM
        end
        %% It is wrong, and different, to do n(z)dchi:
        %norm_nz_tot=sum(n_z_tot(:,ibin).*dchi_nz');
        %g_vals_ck(iz_l,ibin)=sum(int.*dchi/norm_nz_tot);
        %norm_nz_tot=sum(n_z_tot(:,ibin));
        %g_vals(iz_l,ibin)=sum(int/norm_nz_tot);
        g_vals(iz_l,ibin)=sum(int);
    end
end

% Fil does:
%  window_lens_fct value=(source_n_z(z_prime,i))*
%    (f_K(dist_com(z_prime)-dist_com(z))/dist_m(z_prime));
% window_lens
% value=2*misc_math::integrate((mo_triple)&cosmology::window_lens_fct,this,
%                               z,i,z+EPS,z_max,0.001,(char *)"2nd");


% find k spacing for manual interpolation
k_start=k_vals(1);
dlnk=log(k_vals(2))-log(k_vals(1));
% ! Should also check here whether is log spaced or not
% and use it to decide which sort of interp to do below.

% make P_k(ell) (eqn 2.67)
clear integrand window
for ibin=1:nbin
    for jbin=1:nbin
    zstart_time=clock;
    fprintf(1,'\nStarting redshift integral in lens_cls...\n');
    for iz=2:length(z_vals)
        progress(iz,length(z_vals),zstart_time,20,' redshift integral in lens_cls')
        dchi(iz)=chi(1,iz)-chi(1,iz-1); % should w be in units of Mpc/h or just Mpc?
        a=1/(1+z_vals(iz));
        window= dchi(iz)* g_vals(iz,ibin)*g_vals(iz,jbin)/(r_AR(1,iz)*a)^2;
        for il=1:length(ell_vals)
            ell_val = ell_vals(il);
            k_val = ell_val/ r_AR(1,iz);
            % if k_vals are log spaced then can use this line:
            %ik=round((log(k_val)-log(k_start))/dlnk) +1;
            % if not, then have to use this (v slow) line:
           [tmp ik]=min(abs(k_val-k_vals)); ik=ik(1);
           % Should really uncomment the next line, for info. But is slow.
           %if (ik>length(k_vals)) ik=length(k_vals); fprintf(1,'Overflowing at k_val=%5.3g\n',k_val); end % is v small here anyway
            P_delta_val(il,iz) = pk_vals(ik,iz); % Added (il,iz) for storage KM
            integrand(iz,il,ibin,jbin)= P_delta_val(il,iz) *window; % Added (il,iz) KM
        end
    end
    end
end
prefactor = 9  * Omega_m^2 / (16 * D_H^4);
%prefactor = prefactor; % 
%prefactor = prefactor * h^4; % !! changed on Thu 27 Jul 06
prefactor = prefactor; % changed back on 11 Aug 06
lens_cls_vals = prefactor * squeeze(sum(integrand,1)); 

% cf Fil:
%   k=l/((2997.9)*dist_m(z));
%  value=P_k_NL(k,z)*window_lens(z,i)*window_lens(z,j)/pow(1.0/(1+z),2.0)*
%    (2997.9)*func_dist_com(1.0/(1+z));
%  value=misc_math::integrate((mo_quad)&cosmology::c_l_fct,this,
%                             l,i,j,0+EPS,z_max,0.005,(char *)"2nd");
%  value*=(9./16)*pow(2997.9,(-4.0))*param.omega_m*param.omega_m;