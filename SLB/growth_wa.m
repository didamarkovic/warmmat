function [deltavals, zvals]=growth_wa(cospars,na)
%
% [DELTAVALS, ZVALS] = GROWTH_WA(OMEGA_M0,OMEGA_Q0,W,NA)
%
% Calculates the growth factor, DELTAVALS, as a function of redshift, ZVALS
% for a constant w model.
%
% NA is the number of steps to use in scale factor (and so Z). More is
% better!
%

% Supercedes growth_wconst by having cospars as structure

% perturbation amplitude for constant w
% normalised at zeq using Eisenstein + Hu 9710252
%
%


%fprintf(1,'growth_wa SLB:\n')


% unpack cospars
Omega_m=cospars.Omega_m;
Omega_DE=cospars.Omega_DE;
Omega_K=1-Omega_m - Omega_DE;
w0=cospars.w;
wa=cospars.wa;

h=0.7; % cancels, so just put in arbitrarily for now
%kmperMpc=3.0860e+19;
%H0=h*100/kmperMpc; % now in s^-1
%G=6.67e-11; % check!!
%rhoc0=3*H0^2/(8*pi*G);
theta2p7=2.728/2.7;
zeq=2.5e4*Omega_m*h^2*theta2p7^(-4);

%fprintf(1,'  %1.1f,%1.1f,%1.1f,%1.1f,%1.1f,%1.1f\n',Omega_m,Omega_DE,Omega_K,w0,wa,h)

zstart=zeq; % have to start at v high z as only here is delta amplitude zero
astart=1/(1+zstart);
astop=1;
da=(astop-astart)/(na-1);
avals=astart:da:astop; % ie start at zeq;
length(avals);
zvals=1./avals-1;

% set up initial conditions
ia=1;
delta=1; % arbitrary normalisation at z=1000
deltavals(ia)=delta;
deltadash=0; % erm, probs starts at zero?
a=avals(ia);
%* rhom=Omegam0*rhoc0 / a^3;
%* rhoQ=OmegaQ0*rhoc0 * a^(-3*(1+w)); % const w!!
x=1/a;
Omega_m_z=Omega_m .*x.^3;
Omega_K_z=Omega_K*x.^2;
Omega_DE_z= Omega_DE* (1./x) .^ (-3*(1+w0+wa)) .*exp(3*wa*(1./x -1));
H_z_sq =  Omega_m_z + Omega_K_z + Omega_DE_z;
%rhom=Omega_m_z * rhoc0;
%rho_DE_z=Omega_DE_z * rhoc0;

for ia=2:length(avals)
    deltaold=delta;
    deltadashold=deltadash;
    aold = a;
    a=avals(ia);

    % get densities
    x=1/a;
    Omega_m_z=Omega_m .*x.^3;
    Omega_K_z=Omega_K*x.^2;
    Omega_DE_z= Omega_DE* (1./x) .^ (-3*(1+w0+wa)) .*exp(3*wa*(1./x -1));
    %rhom=Omega_m_z * rhoc0;
    %rho_DE_z=Omega_DE_z * rhoc0;
    H_z_sq =  Omega_m_z + Omega_K_z + Omega_DE_z;
    w=w0+wa*(1-a);
    addot_over_a = -0.5 * (Omega_m_z + (1+3*w)*Omega_DE_z);

    % deltaddash=-3/2/(rhom+rhoQ) * (  ...
    %        (rhom+(1-w)*rhoQ)*deltadashold/a ...
    %       -rhom*deltaold/a^2 );
    deltaddash = 3/2 * Omega_m_z * deltaold / a^2 / H_z_sq -...
        (addot_over_a / H_z_sq + 2) * deltadashold / a;
    
    deltadash=deltadashold+(a-aold)*deltaddash;
    delta=deltaold + (a-aold)*deltadash;
    deltavals(ia)=delta;
end

%fprintf(1,'%1.1f %1.1f %1.1f\n',deltavals(1),deltavals(5),deltavals(length(avals)))