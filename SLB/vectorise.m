function vect=vectorise(matrix);
% 
% Turn an nd matrix into a vector 
% Is in order such that can get the matrix back as follows:
% s=size(matrix); % can be nd
% i=1
% for i4=1:s(4)
%     for i3=1:s(3)
%         for i2=1:s(2)
%             for i1=1:s(1)
%                 matrix(i1,i2,i3,i4)=cospars.vect(i);
%                 i=i+1;
%             end
%         end
%     end
% end

vect=reshape(matrix,1,prod(size(matrix)));
