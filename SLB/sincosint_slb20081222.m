function [Si Ci] = sincosint_slb20081222(x,verb)
%
% Calculates the sinx/x & cosx/x integrals numerically
% SLB 22 Dec 2008

% doesn't seem to work!! give up on this and go back to previous fn

if (~exist('verb','var')) verb=0; end

if (verb>0)
    fprintf(1,'Computing Si and Ci look-up tables...')
    tstart=clock;
end

dx=x(2:end) - x(1:(end-1)); 
dx(end+1) = dx(end);

% Ci(x) = - int_x^{\inf} cos(t) / t dt
dCi=-cos(x)./x .*dx;
Ci=zeros(size(x));
for ix=1:length(x)
    Ci(ix)=sum(dCi(ix:end));
end

% Si(x) = int_0^{x} sin(t) / t dt
dSi=sin(x)./x .*dx;
Si=zeros(size(x));
for ix=1:length(x)
    Si(ix)=sum(dSi(1:ix));
end

if (verb>0)
    tmp=etime(clock,tstart);
    fprintf(1,' done in %6.2f seconds\n',tmp)
end