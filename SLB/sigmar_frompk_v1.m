function sigmar=sigmar_frompk(kvals,pkvals,r)
% Finds rms fluctuation within R for a matter power spectrum, Pk_vals.
% sigma_r = sigmar_frompk(k_vals, Pk_vals, R) % this is dimensionless
%
% k_vals should be an array in logarithmic steps in units of h/Mpc!
% Pk_vals are in units of h^3/Mpc^3
% R = 8 Mpc/h for sigma_8
%
% comments added by KM: 11.11.09
% version 1

intdk = 0;

for ik=1:( length(kvals) - 1 )
    
  logkstep  =  log( kvals(ik+1) )  -  log( kvals(ik) ); % dlogk = dk/k
  
  k = kvals(ik);
  
  x = k * r; % dimensionless
  
  w  =  3*( sin(x) - x*cos(x) )  /  x^3;
  
  tmp = pkvals(ik) * w^2 * k^2 /2 /pi^2; % step
  
  intdk = intdk + tmp*logkstep*k; % integrate
  
end

sigmar=sqrt(intdk);