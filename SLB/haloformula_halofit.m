function [pk_nls pk_lins pk_nl_pds rn_pd_store]=haloformula_halofit(cospars,z,k_vals,verb,...
    generalamp)
% this version is as unchanged as possible cf halofit.f code
% see pk_nonlin_halofit for matlabified version

% cospars needs to have set:
% cospars.Omega_m
% cospars.Omega_DE
% cospars.sigma_8
% cospars.Gamma
% w0=cospars.w;
% wa=cospars.wa;
%
% z is required redshift
% k_vals is probably the k values at which to calculate things

if (~exist('generalamp')) generalamp=1; end
%generalamp=0; % use exactly the same eqns as in Smith et al for now
%generalamp=1; % must set generalamp=1 if varying w!!!


% c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% c PROGRAM: halofit
%
% c The `halofit' code models the nonlinear evolution of cold matter
% c cosmological power spectra. The full details of the way in which
% c this is done are presented in Smith et al. (2002), MNRAS, ?, ?.
% c
% c The code `halofit' was written by R. E. Smith & J. A. Peacock.
% c
% c Last edited 8/5/2002.
%
% c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%       real*8 rn,plin,pq,ph,pnl,rk,p_cdm,aexp,z
%       real*8 rknl,rneff,rncur,d1,d2,rad,sig,rknl_int_pow
%       real*8 om_m,om_v,om_b,h,p_index,gams,sig8,amp
%       real*8 om_m0,om_v0,omega_m,omega_v,grow,grow0,gg
%       real*8 rn_pd,rn_cdm,pnl_pd,rklin,f_pd
%       real*8 f1a,f2a,f3a,f4a,f1b,f2b,f3b,f4b,frac,f1,f2,f3,f4
%       real*8 diff,xlogr1,xlogr2,rmid
%
%       integer i,j,k,ndat
%
%       common/cospar/om_m,om_v,om_b,h,p_index,gams,sig8,amp

%c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (verb>=1)
fprintf(1, '****************************\n');
fprintf(1, '*** running halofit code ***\n');
fprintf(1, '****************************\n');
end

%c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%fprintf(1, '*** CDM fitting ***\n');

%c This section demonstrates how cdm spectra are generated

% extract the cosmological parameters
om_m0=cospars.Omega_m;  % z=0 matter density (om_m0)
om_v0=cospars.Omega_DE; % z=0 vacuum density (om_v0)
sig8=cospars.sigma_8;    % z=0 normalisation of cdm power spectrum (sig8)
gams=cospars.Gamma;     % shape parameter of cdm power spectrum (gams)
aexp=1./(1.+z);         % expansion factor

%c calculate matter density, vacuum density at desired redshift
%omega_t=1.0+(om_m0+om_v0-1.0)/(1-om_m0-om_v0+om_v0*aexp*aexp+om_m0/aexp);
%%% The above eqn is equivalent to 
%%% (om_m0 a^-3 + om_v0)/(om_m0 a^-3 + om_k0 a^-2 + om_v0)
%%% where om_k0 == 1 - om_m0 - om_v0
%om_m=omega_t*om_m0/(om_m0+om_v0*aexp^3)
%om_v=omega_t*om_v0/(om_v0+om_m0*aexp^(-3))
% NB these equations are wrong if w.ne.-1 !
[om_m tmp om_v]=densities_wa(cospars,z);
% yes, agrees with theirs to 4dp for z=1

%c calculate the amplitude of the power spectrum at desired redshift
if (generalamp==0)
    % check using their eqns
    grow=2.5*om_m/(om_m^(4./7.)-om_v+(1.0+om_m/2.)*(1.+om_v/70.));
    grow0=2.5*om_m0/(om_m0^(4./7.)-om_v0+(1.0+om_m0/2.)*(1.+om_v0/70.));
    amp=aexp*grow/grow0;
else
    % use my more accurate formula which is general for w0, wa models
    na=1000; % resolution on growth factor calculation
    [deltavals, zvals]=growth_wa(cospars,na);
    ampz=interp1(zvals,deltavals,z);
    amp0=interp1(zvals,deltavals,0);
    amp=ampz/amp0;
    % agrees fine with theirs (for z=1 amp theirs=0.6129; amp=0.6207)
end

%c calculate nonlinear wavenumber (rknl), effective spectral index (rneff) and
%c curvature (rncur) of the power spectrum at the desired redshift, using method
%c described in Smith et al (2002).
if (verb>=1)
fprintf(1, 'computing effective spectral quantities:\n')
end

xlogr1=-2.0;
xlogr2=3.5;
diff=1e9; % initialise
iloop=10;
while (abs(diff)>=0.001)
    iloop=iloop+1;
    rmid=(xlogr2+xlogr1)/2.0;
    rmid=10^rmid;
    [sig d1 d2]=wint_halofit(rmid,amp,gams,sig8);
    diff=sig-1.0;
    if (verb>=1)
    fprintf(1,'iloop=%i; diff=%10.5f; \n',iloop,diff);
    end
    if (diff>0.001)
        xlogr1=log10(rmid);
    elseif (diff<-0.001)
        xlogr2=log10(rmid);
    end
end
rknl=1./rmid;
rneff=-3-d1;
rncur=-d2;
%fprintf(1,'rknl [h/Mpc] = %12.6f; rneff= %12.6f rncur=%12.6f;\b',,rknl,rneff,rncur);

%c now calculate power spectra for a logarithmic range of wavenumbers (rk)
if (verb>=1)
fprintf(1, 'now computing halofit nonlinear power...\n');
end
%ndat=100;
ndat=length(k_vals);
rk_vals=zeros(size(k_vals));
rknl_vals=zeros(size(k_vals));
plin_vals=zeros(size(k_vals));
pnl_vals=zeros(size(k_vals));
%for i=1:ndat
%    rk=-2.0+4.0*(i-1)/(ndat-1.);
%    rk=-4.0+8.0*(i-1)/(ndat-1.);
%    rk=10^rk;
for ik=1:ndat
    k_val=k_vals(ik);
    rk=k_val; % am guessing rk is the wavenumber in units [h Mpc^-1]
    %c linear power spectrum !! Remeber => plin = k^3 * P(k) * constant
    %c constant = 4*pi*V/(2*pi)^3
    plin=amp*amp*p_cdm_halofit(rk,gams,sig8);
    %c calculate nonlinear power according to halofit: pnl = pq + ph,
    %c where pq represents the quasi-linear (halo-halo) power and
    %c where ph is represents the self-correlation halo term.
    %call halofit(rk,rneff,rncur,rknl,plin,pnl,pq,ph)  % ! halo fitting formula
    pnl=halofit_halofit(rk,rneff,rncur,rknl,plin,om_m,om_v);
    if (verb>=1)
    fprintf(1,'rk,plin,pnl = %10.6g,%10.6g,%10.6g; \n',...
        rk,plin,pnl);
    end
    
    % store things
    rk_vals(ik)=rk;
    rknl_vals(ik)=rknl;
    plin_vals(ik)=plin;
    pnl_vals(ik)=pnl;
end

%c comparison with Peacock & Dodds (1996)
if (verb>=1)
    fprintf(1,'halofit is computing PD96 nonlinear power...\n');
end
rn_pd_store=zeros(size(k_vals));
rknl_vals=zeros(size(k_vals));
p_rknl_pd_vals=zeros(size(k_vals));
for ik=1:ndat
    k_val=k_vals(ik);
    %c linear wavenumbers and power
    rklin=k_val;
    plin=amp*amp*p_cdm_halofit(rklin,gams,sig8);
    %c effective spectral index: rn_pd=dlogP(k/2)/dlogk
    %         rn_pd=rn_cdm(rklin,gams,sig8);
    y=p_cdm_halofit(rklin/2,gams,sig8);
    yplus=p_cdm_halofit(rklin*1.01/2.,gams,sig8);
    rn_pd=-3.+log(yplus/y)*100.5;
    %c nonlinear power from linear power
    %         pnl_pd=f_pd(plin,rn_pd)
    y=plin;
    rn=rn_pd;
    g=(5./2.)*om_m/(om_m^(4./7.)-om_v+(1+om_m/2.)*(1+om_v/70.));
    a=0.482*(1.+rn/3.)^(-0.947);
    b=0.226*(1.+rn/3.)^(-1.778);
    alp=3.310*(1.+rn/3.)^(-0.244);
    bet=0.862*(1.+rn/3.)^(-0.287);
    vir=11.55*(1.+rn/3.)^(-0.423);
    pnl_pd=y * ( (1.+ b*y*bet + (a*y)^(alp*bet)) / ...
        (1.+ ((a*y)^alp*g*g*g/vir/y^0.5)^bet ) )^(1./bet);
    %c scaling for nonlinear wavenumber
    %rk=rklin*(1+pnl_pd)^(1./3.);
    % surely the above line was a bug - should have been rknl?
    rknl=rklin*(1+pnl_pd)^(1./3.);
    % save things for later
    rknl_vals(ik)=rknl;
    p_rknl_pd_vals(ik)=pnl_pd;
    rn_pd_store(ik)=rn_pd;
end
% interpolate this onto the original k_vals
pnl_pd_vals=interp1(rknl_vals,p_rknl_pd_vals,k_vals);

% clf
% loglog(rk_vals,plin_vals,'b-')
% hold on
% loglog(rk_vals,pnl_vals,'r--')
% looks roughly like P(k)*k^3

% convert to the P(k) that I usually use:
% assume they are making Delta as written in eqn 5 of halofit
% and that they are using V=1
deltak2_to_pk=(4*pi * k_vals.^3 / (2*pi)^3);
pk_lins=plin_vals./deltak2_to_pk;
pk_nls=pnl_vals./deltak2_to_pk;
pk_nl_pds=pnl_pd_vals./deltak2_to_pk;