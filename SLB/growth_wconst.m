function [deltavals, zvals]=growth_wconst(Omegam0,OmegaQ0,w,na)
%
% [DELTAVALS, ZVALS] = GROWTH_WCONST(OMEGA_M0,OMEGA_Q0,W,NA)
% 
% Calculates the growth factor, DELTAVALS, as a function of redshift, ZVALS
% for a constant w model.
% 
% NA is the number of steps to use in scale factor (and so Z). More is
% better!
%
% Sarah Bridle

% perturbation amplitude for constant w 
% normalised at zeq using Eisenstein + Hu 9710252


%fprintf(1,'growth_wconst SLB\n')

h=0.7; % cancels, so just put in arbitrarily for now
kmperMpc=3.0860e+19;
H0=h*100/kmperMpc; % now in s^-1
G=6.67e-11; % check!!
rhoc0=3*H0^2/(8*pi*G);
theta2p7=2.728/2.7;
zeq=2.5e4*Omegam0*h^2*theta2p7^(-4);

zstart=zeq; % have to start at v high z as only here is delta amplitude zero
astart=1/(1+zstart);
astop=1;
da=(astop-astart)/(na-1);
avals=astart:da:astop; % ie start at zeq;
length(avals);
zvals=1./avals-1; % This is why it all comes out reversed! (KM)

% set up initial conditions
ia=1;
delta=1; % arbitrary normalisation at z=1000
deltavals(ia)=delta;
deltadash=0; % erm, probs starts at zero?
a=avals(ia);
rhom=Omegam0*rhoc0 / a^3;
rhoQ=OmegaQ0*rhoc0 * a^(-3*(1+w)); % const w!!

for ia=2:length(avals)
   deltaold=delta;
  deltadashold=deltadash;
  aold = a;
  a=avals(ia);
  rhom=Omegam0*rhoc0 / a^3;
  rhoQ=OmegaQ0*rhoc0 * a^(-3*(1+w)); % const w!!
  deltaddash=-3/2/(rhom+rhoQ) * (  ...
       (rhom+(1-w)*rhoQ)*deltadashold/a ...
      -rhom*deltaold/a^2 );
  deltadash=deltadashold+(a-aold)*deltaddash;
  delta=deltaold + (a-aold)*deltadash;
  deltavals(ia)=delta;
end