function xi_gamma_arr=lens_cls_2_xi(ell_vals,lens_cls_vals_arr,theta_vals)
% 
% Convert Cls to shear 2pt correlation function <gamma gamma^*>_theta
% 
% XI_GAMMA = LENS_CLS_2_XI(ELL_VALS, LENS_CLS_VALS, THETA_VALS)
%
% where e.g. ELL_VALS and LENS_CLS_VALS come out of
% lens_cls_doall
% THETA_VALS are angles at which to calculate xi, in arcmin

% Comparing Heymans eq 2.92 with SKS eq 7, 
% looks like k=ell. hmm.
k_vals = ell_vals;
warned =0;

% if lens_cls_vals is for >1 z bin
tmp=size(lens_cls_vals_arr);
if (tmp(1)==1) lens_cls_vals_arr=lens_cls_vals_arr'; end
nbin1=size(squeeze(lens_cls_vals_arr),2);
nbin2=size(squeeze(lens_cls_vals_arr),3);
for ibin1=1:nbin1
    for ibin2=1:nbin2
        lens_cls_vals = lens_cls_vals_arr(:,ibin1,ibin2);


% Shear correlation function from this:
theta_rad_vals = theta_vals /60 * pi/180; % convert armin to radians
for itheta=1:length(theta_vals)
    %itheta=30;
    clear int x
    for ik=1:(length(k_vals)-1)
        k_val = k_vals(ik);
        dk=k_vals(ik+1) - k_vals(ik);
        
        % check the spacing is good enough for bessels
        dx=dk*theta_rad_vals(itheta);
        if ((dx>pi/2)&(warned==0))
            fprintf(1,'Warning: Make sure finer ell sampling does not change your xi result\n')
            pause(0.1)
            warned=1;
        end
        
        x(ik) = k_val * theta_rad_vals(itheta);
        J_0_val = besselj(0,x(ik));
        int(ik) = dk * k_val * lens_cls_vals(ik) * J_0_val;
    end
    %plot(k_vals(1:(length(k_vals)-1)),int,'bx') 
    % v smooth for theta=1'; for theta=30', is not sampled enough really
    % when nell=200; With nell=2000 looks great.
    %plot(k_vals(1:(length(k_vals)-1)),x,'rx');
    % for theta=30', x goes up to 140
    xi_gamma(itheta) = sum(int) / (2*pi);
end

    xi_gamma_arr(:,ibin1,ibin2)=xi_gamma;
    end
end