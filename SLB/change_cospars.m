function [cospars]=change_cospars(cospars,verb)
% convert these into all the reqd cosmological parameters if necessary
% reqd pars are
% Omega_m
% Omega_b
% sigma_8
% h
% Omega_DE
% w

if (~exist('verb')) verb=0; end

if (~isfield(cospars,'parameterization')) cospars.parameterization='default'; end


if (strcmp(cospars.parameterization,'default'))
    % do nothing
    if (verb==1)
        fprintf(1,'Using default parameterization!\n')
    end

elseif (strcmp(cospars.parameterization,'fix_Omega_k'))

    cospars.Omega_DE = 1- cospars.Omega_k - cospars.Omega_m;

elseif (strcmp(cospars.parameterization,'21apr07'))
    
    % The standard parameters
    cospars.names={'Omega_m','w','wa','h','sigma_8','Omega_b','n_s',...
        'n_run',...
        'k_pivot','Omega_k','GI_A','II_A','GI_gamma','II_gamma','GI_zpivot'};
    nbase=length(cospars.names);
    for i=1:nbase
        cospars=setfield(cospars,cospars.names{i},cospars.vect(i));
    end
    cospars.Omega_DE = 1- cospars.Omega_k - cospars.Omega_m;

    % Parameters for the GI binning
    iv=nbase+1; % should be 16
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.GI_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('GI_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    cospars.GI_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % Might as well use log spacing in (1+z):
    cospars.GI_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    % Parameters for the II binning
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.II_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('II_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    % Is assumed the kbins and zbins are the same for GI and II
    %cospars.II_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % Might as well use log spacing in (1+z):
    %cospars.II_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    %% parameters for n(z) 
    cospars.names{iv}='nzpars.alpha';
    cospars.names{iv+1}='nzpars.beta';
    cospars.names{iv+2}='nzpars.z_0';
    cospars.names{iv+3}='nzpars.deltaz';
    cospars.names{iv+4}='nzpars.Deltaz';
    cospars.names{iv+5}='nzpars.fcat';
    for i=1:6
        string=sprintf('cospars.%s=%s',cospars.names{iv},'cospars.vect(iv);');
        eval(string);
        iv=iv+1;
    end
    
    %% parameters for zbias and deltaz binning
    for iz=1:cospars.nzpars.nbin_zbias
        cospars.nzpars.zbiass(iz)=cospars.vect(iv);
        cospars.names{iv}=sprintf('nzpars.zbiass(%i)',iz);
        iv=iv+1;
    end
    for iz=1:cospars.nzpars.nbin_zbias
        cospars.nzpars.deltazs(iz)=cospars.vect(iv);
        cospars.names{iv}=sprintf('nzpars.deltazs(%i)',iz);
        iv=iv+1;
    end

    if (verb==1)
        for i=1:nbase
            fprintf(1,'%s = %5.5f\n',cospars.names{i},getfield(cospars,cospars.names{i}));
        end
    end
    if (verb>=2)
        for i=1:(iv-1)
            string=sprintf('cospars.%s;',cospars.names{i});
            tmp=eval(string);
            fprintf(1,'%s = %5.5f\n',cospars.names{i},tmp);
        end
    end
    
elseif (strcmp(cospars.parameterization,'9apr07'))
    
    % The standard parameters
    cospars.names={'Omega_m','w','wa','h','sigma_8','Omega_b','n_s',...
        'n_run',...
        'k_pivot','Omega_k','GI_A','II_A','GI_gamma','II_gamma','GI_zpivot'};
    nbase=length(cospars.names);
    for i=1:nbase
        cospars=setfield(cospars,cospars.names{i},cospars.vect(i));
    end
    cospars.Omega_DE = 1- cospars.Omega_k - cospars.Omega_m;

    % Parameters for the GI binning
    iv=nbase+1; % should be 16
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.GI_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('GI_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    cospars.GI_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % Might as well use log spacing in (1+z):
    cospars.GI_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    % Parameters for the II binning
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.II_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('II_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    % Is assumed the kbins and zbins are the same for GI and II
    %cospars.II_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % Might as well use log spacing in (1+z):
    %cospars.II_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    %% parameters for n(z) 
    cospars.names{iv}='nzpars.alpha';
    cospars.names{iv+1}='nzpars.beta';
    cospars.names{iv+2}='nzpars.z_0';
    cospars.names{iv+3}='nzpars.deltaz';
    cospars.names{iv+4}='nzpars.Deltaz';
    cospars.names{iv+5}='nzpars.fcat';
    for i=1:6
        string=sprintf('cospars.%s=%s',cospars.names{iv},'cospars.vect(iv);');
        eval(string);
        iv=iv+1;
    end

    if (verb==1)
        for i=1:nbase
            fprintf(1,'%s = %5.5f\n',cospars.names{i},getfield(cospars,cospars.names{i}));
        end
    end
    if (verb>=2)
        for i=1:(iv-1)
            string=sprintf('cospars.%s;',cospars.names{i});
            tmp=eval(string);
            fprintf(1,'%s = %5.5f\n',cospars.names{i},tmp);
        end
    end
    
elseif (strcmp(cospars.parameterization,'27mar07'))
    
    % The standard parameters
    cospars.names={'Omega_m','w','wa','h','sigma_8','Omega_b','n_s',...
        'n_run',...
        'k_pivot','Omega_k','GI_A','GI_gamma','II_gamma','GI_zpivot'};
    for i=1:length(cospars.names)
        cospars=setfield(cospars,cospars.names{i},cospars.vect(i));
    end
    cospars.Omega_DE = 1- cospars.Omega_k - cospars.Omega_m;

    % Parameters for the GI binning
    iv=15;
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.GI_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('GI_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    cospars.GI_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % Might as well use log spacing in (1+z):
    cospars.GI_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    % Parameters for the II binning
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.II_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('II_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    % Is assumed the kbins and zbins are the same for GI and II
    %cospars.II_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % Might as well use log spacing in (1+z):
    %cospars.II_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    %% parameters for n(z) 
    cospars.names{iv}='nzpars.alpha';
    cospars.names{iv+1}='nzpars.beta';
    cospars.names{iv+2}='nzpars.z_0';
    cospars.names{iv+3}='nzpars.deltaz';
    cospars.names{iv+4}='nzpars.Deltaz';
    cospars.names{iv+5}='nzpars.fcat';
    for i=1:6
        string=sprintf('cospars.%s=%s',cospars.names{iv},'cospars.vect(iv);');
        eval(string);
        iv=iv+1;
    end

    if (verb==1)
        for i=1:14
            fprintf(1,'%s = %5.5f\n',cospars.names{i},getfield(cospars,cospars.names{i}));
        end
    end
    if (verb>=2)
        for i=1:(iv-1)
            string=sprintf('cospars.%s;',cospars.names{i});
            tmp=eval(string);
            fprintf(1,'%s = %5.5f\n',cospars.names{i},tmp);
        end
    end

elseif (strcmp(cospars.parameterization,'28feb07'))
    
    % The standard parameters
    cospars.names={'Omega_m','w','wa','h','sigma_8','Omega_b','n_s',...
        'n_run',...
        'k_pivot','Omega_k','GI_A','GI_gamma','GI_zpivot'};
    for i=1:length(cospars.names)
        cospars=setfield(cospars,cospars.names{i},cospars.vect(i));
    end
    cospars.Omega_DE = 1- cospars.Omega_k - cospars.Omega_m;

    % Parameters for the GI binning
    iv=14;
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.GI_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('GI_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    cospars.GI_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % Might as well use log spacing in (1+z):
    cospars.GI_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    %% parameters for n(z) 
    cospars.names{iv}='nzpars.alpha';
    cospars.names{iv+1}='nzpars.beta';
    cospars.names{iv+2}='nzpars.z_0';
    cospars.names{iv+3}='nzpars.deltaz';
    cospars.names{iv+4}='nzpars.Deltaz';
    cospars.names{iv+5}='nzpars.fcat';
    for i=1:6
        string=sprintf('cospars.%s=%s',cospars.names{iv},'cospars.vect(iv);');
        eval(string);
        iv=iv+1;
    end
    
    if (verb==1)
        %for i=1:length(cospars.names)
        for i=1:13
          fprintf(1,'%s = %5.5f\n',cospars.names{i},getfield(cospars,cospars.names{i}));
        end
    end
    
elseif (strcmp(cospars.parameterization,'15feb07'))
    
    % The standard parameters
    cospars.names={'Omega_m','w','wa','h','sigma_8','Omega_b','n_s',...
        'n_run',...
        'k_pivot','Omega_k','GI_A','GI_gamma','GI_zpivot'};
    for i=1:length(cospars.names)
        cospars=setfield(cospars,cospars.names{i},cospars.vect(i));
    end
    cospars.Omega_DE = 1- cospars.Omega_k - cospars.Omega_m;

    % Parameters for the GI binning
    iv=14;
    for iz=1:cospars.GI_nz
        for ik=1:cospars.GI_nk
            cospars.GI_kzbins(ik,iz)=cospars.vect(iv);
            cospars.names{iv}=sprintf('GI_kzbins(%i,%i)',ik,iz);
            iv=iv+1;
        end
    end
    cospars.GI_kbins=logspace_ln(cospars.GI_kmin,cospars.GI_kmax,cospars.GI_nk);
    % For linear spacing in z
    %if (cospars.GI_nz>1)
    %zstep=(cospars.GI_zmax-cospars.GI_zmin)/(cospars.GI_nz-1);
    %else
    %    zstep=1000;
    %end
    %cospars.GI_zbins=cospars.GI_zmin:zstep:cospars.GI_zmax;
    % Might as well use log spacing in (1+z):
    cospars.GI_zbins=logspace_ln(cospars.GI_zmin+1,cospars.GI_zmax+1,cospars.GI_nz)-1;
    
    if (verb==1)
        %for i=1:length(cospars.names)
        for i=1:13
          fprintf(1,'%s = %5.5f\n',cospars.names{i},getfield(cospars,cospars.names{i}));
        end
    end

elseif (strcmp(cospars.parameterization,'ma_hu_huterer'))

    cospars.Omega_m=1-cospars.Omega_DE; % assume their fiducial model is flat
    cospars.h=sqrt(cospars.Omega_mh2 / cospars.Omega_m);
    cospars.Omega_b=cospars.Omega_bh2/cospars.h^2;

    if (verb==1)
        fprintf(1,'Omega_m = %5.5f\n',cospars.Omega_m);
        fprintf(1,'Omega_b = %5.5f\n',cospars.Omega_b);
        fprintf(1,'h = %5.5f\n',cospars.h);
    end

else
    fprintf(1,'ERROR! Unrecognized parameterization %s in unpack_cospars\n',parameterization);
end
