function [D_C D_M D_L D_A V_C]=D_wconsts(z1,z2,cospars,nx)
%
% D_C_over_D_H = D_WCONST(Z1,Z2,OMEGA_M,OMEGA_DE,W)
% 
% calculate integrated comoving distance
% D_C / D_H in Hogg 1999 notation esp eq15 (but can vary limits of
% z integral in this function)
% 
% Z1 is lower limit for redshift integral (e.g. zero)
% Z2 is upper limit for redshift integral (i.e. z of interest)
% W is one number, assumed to be a constant
% D_C_over_D_H = D_C / D_H
%
% Z1 and Z2 are not vectors or matrices
%
% [D_C D_M D_L D_A V_C] = D_WCONST(Z1,Z2,OMEGA_M,OMEGA_DE,W)
% 
% Additionally returns D_M, D_L, D_A and V_C as given in Hogg 0005116
% In fact, these are all in units of D_H.
% i.e. it actually returns D_C/D_H, D_M/D_H etc
% NB. V_C is probably only right if z1=0
%
% D_C = D_WCONST(Z1,Z2,OMEGA_M,OMEGA_DE,W,NZ)
%
% Optionally specify number of steps for z integral 
%
% !! NB. I haven't tested w ne -1, or Omega_K ne 0 really
%
% modified from D_L_ok 
% checked for z1.ne.0 against David Sutton's Dist
% Supercedes D_wconst by using a structure for cospars SLB 12 Jan 2007
% SLB 7 Jul 2006

D_C=NaN; D_M=NaN; D_L=NaN; D_A=NaN; V_C=NaN;

if (~exist('nx')) nx=1000; end

if (z1>(z2-1e-6))
    %fprintf(1,'z1>z2 in D_wconst\n')
    D_C=0; D_M=0; D_L=0; D_A=0; V_C=0;
    return
end

% extract cosmological parameters from structure
Omega_m=cospars.Omega_m;
Omega_DE=cospars.Omega_DE;
w=cospars.w;

Omega_K=1-Omega_m-Omega_DE;
saok=sqrt(abs(Omega_K));

% set values for integral
x2=1+z2;
x1=1+z1;
xstep=(x2-x1)/(nx-1);
x_vals=x1:xstep:x2;

% find D_C
% Hogg eqn 14:
E_z = sqrt(Omega_m .* x_vals.^3+ Omega_K*x_vals.^2 + Omega_DE .* x_vals.^(3*(1+w)));
% Hogg eqn 15:
D_C = sum ( 1./E_z) * xstep;

% find D_M for use later
% Hogg eqn 16
if (Omega_K==0)
  D_M=D_C;
elseif (Omega_K<0)
  D_M=1/saok*sin(saok*D_C); % N.B. I'm using shorthand: D_i = D_i / D_H
else
  D_L(1)=1/saok*sinh(saok*D_C);
end

% find D_A
% Hogg eqn 18
% and BS99 eqn 2.44 for the general case when z1.ne.0
D_A = D_M / x2;

% find D_L
% Hogg eqn 21
% and BS99 eqn 2.45 for the general case when z1.ne.0
D_L = D_M * x2/x1^2;

% find V_C
% Hogg eqn 28
V_C = x2.^2*D_A.^2 ./ E_z(length(E_z)); % dV_C / (d Omega dz D_H)


% Where does x1 come in????
%V_proper= V_C ./x2.^3; % proper volume
%V_C = V_proper .* x1.^3; % No idea what comoving volume at z2 relative to z1
% means but maybe this is it?!
% Can't think why anyone would want to calculate this?
