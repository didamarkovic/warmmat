function growth_sup_vals=growth_sup_wconst(Omega_m,Omega_DE,w,z_vals,nz,znorm)
%
% Find growth supression factor for const w model
%
% SLB Wed 12 Jul 2006


%fprintf(1,'growth_sup_wconst SLB\n')

if (~exist('nz')) nz=1000; end
if (~exist('znorm')) znorm=20; end

[delta_vals, z_vals_tmp]=growth_wconst(Omega_m,Omega_DE,w,nz);
[delta_vals_SCDM, z_vals_tmp]=growth_wconst(1,0,-1,nz);

[tmp iz]=min(abs(z_vals_tmp-znorm)); iz=iz(1); % normalise at z=znorm

growth_sup_vals= delta_vals./delta_vals_SCDM / (delta_vals(iz) /delta_vals_SCDM(iz));

growth_sup_vals=interp1(z_vals_tmp,growth_sup_vals,z_vals);

