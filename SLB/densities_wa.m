function [Omega_m_z Omega_K_z Omega_DE_z]=densities_wa(cospars,z_vals);
% slb 20080814

% unpack inputs
Omega_m=cospars.Omega_m;
Omega_DE=cospars.Omega_DE;
w0=cospars.w;
if (isfield(cospars,'wa')) wa=cospars.wa; else wa=0; end
if (isfield(cospars,'Omega_K')) Omega_K=cospars.Omega_K; else Omega_K=1-Omega_m-Omega_DE; end
x_vals=(1+z_vals);

% calculate the contributions
Omega_m_part = Omega_m .*x_vals.^3;
Omega_K_part = Omega_K * x_vals.^2;
Omega_DE_part= Omega_DE* (1./x_vals) .^ (-3*(1+w0+wa)) .*exp(3*wa*(1./x_vals -1));
H_on_H0_sq = Omega_m_part + Omega_K_part + Omega_DE_part;

% calculate densities as a function of z
Omega_m_z= Omega_m_part / H_on_H0_sq;
Omega_DE_z=Omega_DE_part / H_on_H0_sq;
Omega_K_z=Omega_K_part / H_on_H0_sq;