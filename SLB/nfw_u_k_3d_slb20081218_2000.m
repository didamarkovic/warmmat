function [u_k cospars r_v]=nfw_u_k_3d_slb20081218_2000(...
    k_vals,M_val,z_val,M_star,cospars)

% set defaults
if (~isfield(cospars,'nx_SCi'))
    nx_SCi=50000;
else
    nx_SCi=cospars.nx_SCi;
end
if (~exist('type_SCi','var'))
%    type_SCi='fullnumint';
    type_SCi='fulllogint';
    acc_SCi=NaN; % not used if type='fullnumint'
end
if (~isfield(cospars,'type_conc'))
    type_conc='coorays02';
else
    type_conc=cospars.type_conc;
end

% get the concentration parameter from the mass
if (strcmp(type_conc,'coorays02'))
    % use eqn from Cooray & Sheth halo review astro-ph/0206508 eqn 78
    conc=9/(1+z_val) * (M_val/M_star)^(-0.13);
else
    fprintf(1,'ERROR in nfw_u_k_3d: type_conv = %s not recognized\n',...
        type_conv);
end

% work out standard NFW stuff from m_val
[r_s rho_s r_v]=nfw_rs_rhos_from_m_conc_slb20081218(M_val,conc,z_val,cospars);

% get look-up table of Si and Ci values if not already existing
if (~isfield(cospars,'Si'))
    x_min=min(k_vals)*r_s;
    x_max=(1+conc)*max(k_vals)*r_s;
%    dx=x_max/nx_SCi;
    dx=nx_SCi;
   % [Si Ci x_SCi] = sincosint_slb20081218_2000(x_max,acc_SCi,dx,type_SCi,x_min);
    [Si Ci x_SCi] = sincosint(x_max,acc_SCi,dx,type_SCi,x_min);
    cospars.Si=Si;
    cospars.Ci=Ci;
    cospars.x_SCi=x_SCi;
    % NB. best not to just use these blindly again, because the x_SCi
    % values need to be optimised for each halo mass separately.
else
    Si=cospars.Si;
    Ci=cospars.Ci;
    x_SCi=cospars.x_SCi;
end

% type in eqn 81 of Cooray & Sheth astro-ph/0206508,
% which seems to appear first in M. White 2001 halo model paper
u_norm=4*pi*rho_s*r_s^3/M_val;
Si_1st=interp1(x_SCi,Si,(1+conc)*k_vals*r_s);
Si_2nd=interp1(x_SCi,Si,k_vals*r_s);
Ci_1st=interp1(x_SCi,Ci,(1+conc)*k_vals*r_s);
Ci_2nd=interp1(x_SCi,Ci,k_vals*r_s);
curlybit=sin(k_vals*r_s).*(Si_1st-Si_2nd) ...
    - sin(conc*k_vals*r_s)./((1+conc)*k_vals*r_s) ...
    + cos(k_vals*r_s).*(Ci_1st - Ci_2nd);
u_k = u_norm * curlybit;







%