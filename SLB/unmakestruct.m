function varargout=unmakestruct(data)

nout=nargout;

for i=1:nout
    varargout(i)={getfield(data,data.names{i})};
end