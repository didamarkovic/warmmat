function [param]=get_standard_pars

% standard parameters
param.G = 6.673e-11; % SI units:  kg^-1 m^3 s^-2 
param.c =  299792458; % m/s
param.Mpc_to_m = 1e6 * 3.0856e16; % number of meters in 1Mpc
param.H_0 = 100 * 1e3 / param.Mpc_to_m; % h/s
param.M_solar = 1.989e30; % kg
param.rho_crit_SI = 3 * param.H_0^2 / (8 *pi * param.G); % kg m^-3
param.rho_crit = 3 * param.H_0^2 / (8 *pi * param.G) /param.M_solar *param.Mpc_to_m^3; % h^2 M_solar / Mpc^-3

%param.c=3e5;  % in units of km s^-1
param.H_0=100; % in units of h km/s Mpc^-1
param.D_H=(param.c / 1e3)/param.H_0; % So D_H is in units of Mpc /h

param.arcminsq_per_sr=(60*180/pi)^2;
param.sky_deg2=4*pi*(180/pi)^2;
