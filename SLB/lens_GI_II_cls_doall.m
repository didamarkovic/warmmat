function [cls_vals, lens_cls_vals, GI_cls_vals, II_cls_vals cospars]=lens_GI_II_cls_doall(...
    cospars,nzpars,ell_vals,...
    z_vals,chi,r_AR,k_vals,pk_nl_vals)
% adapted from lens_GI_cls_doall SLB 28 Feb 2007
% adapted from lens_cls_doall SLB 2006
% wrapper for lens_GI_cls which does all the preliminary calcs
% Good for general quick use. Should check resolutions of all grids below.

% assumes that the cospars which have been set properly are:
% For pk
% Omega_b
% h
% Omega_m
% sigma_8
% n_s
% n_run

param=get_standard_pars;

% choose values for integrals
if ((~exist('z_vals','var'))||isempty(z_vals))
    z_vals=0.001:0.03:3; % should check higher resolutions to be sure..
end
if (~exist('k_vals','var')||(isempty(k_vals)))
    k_vals=logspace_ln(0.0001,2000,1000);
end

% make a look-up table of distance measures
if ((~exist('r_AR','var'))||(isempty(r_AR)))
%*    [chi r_AR]=Ds_wconsts(z_vals,z_vals,cospars,1000);
    [chi r_AR]=D_wa_grid(z_vals,cospars);
    chi=chi*param.D_H;
    r_AR=r_AR*param.D_H; % *comoving* ang diam distance = D_A/a = D_M
end

% Get n(z) for galaxies
[n_z_tot]=get_nofz(cospars.nzpars,z_vals);

if (~exist('pk_nl_vals','var')||(isempty(pk_nl_vals)))
    % make a look-up table of growth factor stuff
    %[delta_vals_tmp, z_vals_tmp]=growth_wconsts(cospars,1000); % replaced
    %31 Jan 2007
    [delta_vals_tmp, z_vals_tmp]=growth_wa(cospars,1000);
    delta_vals=interp1(z_vals_tmp,delta_vals_tmp,z_vals);
    growth_vals=delta_vals./(max(delta_vals));
    %growth_sup_vals=growth_sup_wconsts(cospars,z_vals); % DOH 9 Feb 2006
    growth_sup_vals=growth_sup_wa(cospars,z_vals);
    
    % get non-linear power spectrum
    pk_vals=get_pk_ma(cospars,k_vals);
    [pk_nl_vals pk_lin_vals]=pks_nonlin(k_vals,pk_vals,z_vals,growth_vals,growth_sup_vals);
    pk_nl_vals(find(isnan(pk_nl_vals)))=1e-9; % need to fix it
    
    % store it for use in get_p_delta_gammaI
    cospars.growth_vals=growth_vals;
    cospars.pk_lin_vals=pk_lin_vals;
    cospars.pk_nl_vals=pk_nl_vals;
    if ( (isfield(cospars,'isfid')) & (cospars.isfid==1) )
        % i.e. if running Fisher code isfid is set
        cospars.fid.growth_vals=cospars.growth_vals;
        cospars.fid.pk_lin_vals=cospars.pk_lin_vals;
        cospars.fid.pk_nl_vals=cospars.pk_nl_vals;
    end

end

% Get p_delta_gammaI for GI term
[p_delta_gammaI p_EE_gammaI]=get_p_delta_gammaI(cospars,z_vals,k_vals);
if  ( (isfield(cospars,'isfid')) & (cospars.isfid==1) )
    cospars.fid.p_delta_gammaI=p_delta_gammaI;
    cospars.fid.p_EE_gammaI=p_EE_gammaI;
end
if (cospars.GI_fixpk==-1)
    p_delta_gammaI=cospars.fid.p_delta_gammaI;
    p_EE_gammaI=cospars.fid.p_EE_gammaI;
end

% Get lensing power spectrum and GI
[lens_cls_vals, GI_cls_vals, II_cls_vals]=lens_GI_II_cls(ell_vals,z_vals,chi,r_AR,n_z_tot,...
    k_vals,pk_nl_vals,p_delta_gammaI,p_EE_gammaI,cospars);
%loglog(ell_vals,lens_cls_vals(:,1)'.*ell_vals.^2/(2*pi),'r--')

cls_vals=lens_cls_vals+GI_cls_vals+II_cls_vals;
