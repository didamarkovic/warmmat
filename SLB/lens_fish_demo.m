%clearpath
%setoldpath
clear
%home='c:/home/sarah/'; 
%cd([home,'work/gravlens/im2shape']); 
%home='../../../';
%addpath([home,'work/gravlens/mstuff/']); 
%addpath([home,'work/joint/mstuff/']);
param=get_standard_pars;
%wkdir =[home,'scratch/gravlens/cospars/giii_photoz/'];
tag='fish_demo_slb_20070926_1209';
%fprintf(1,'--------------------------------------------------------------\n');

% Set computational parameters
%zstep=0.1; nell=5; nkv=100; % fast, for debugging
zstep=0.003; nell=20; nkv=500; % max practical I think
% Should use the above line for final results.

% Set survey parameters
% area of survey e.g. 20000 sq deg
cospars.nzpars.f_sky = 20000/param.sky_deg2; 
% galaxy density on sky e.g. 35 per sq arcmin
cospars.nzpars.ng=35*param.arcminsq_per_sr;
% intrinsic ellipticity of the galaxies
cospars.nzpars.sigma_gamma=0.16; 
cospars.nzpars.type='amarar06';
% number of redshift bins to divide the lensed galaxies into
cospars.nzpars.nbin=10; 
cospars.nzpars.verb=0; % whether to plot things while calculating
% Specify the shape of n(z) for the background galaxies:
z_0=0.9/1.412; 
alpha=2; 
beta=1.5;
% Specify photometric redshift quality:
deltaz=0.001; % photometric redshift error. 0.001 is ~ perfect photozs
% Specify catastrophic outliers (fcat=0 means there are none)
fcat=0; 
Deltaz=1; 

cospars.verb=1; % whether to write stuff to the screen lots
% Set base cosmological parameter values
% see function "change_cospars" to find out which is which.
% fiducial model of Amara & Refregier 2006
%cospars_vect_base=[0.28 -0.95 0    0.72 1.0  0.046 1     0 0.05 0 1    1    0    0    0];
% WMAP3
cospars_vect_base=[0.23 -0.95 0    0.73 0.74  (0.022/0.73^2) 0.95     0 0.05 0 1    1    0    0    0];
fish_steps_base=  [0.01  0.01 0.01 0.01 0.01 0.005 0.005 0 0    0 0.01 0.01 0.01 0.01 0];
cospars.parameterization='9apr07';

% The below lines should be fine. No need to change them.
zmin=0.001; zmax=3; 
ell_vals=logspace_ln(10,2e4,nell);
z_vals=zmin:zstep:zmax;
k_vals=logspace_ln(0.0001,2000,nkv); 

% put intrinsic alignments stuff together with the above
lens_fish_demo_set_IA % a cheat to make this code look simple 
cospars.vect=[cospars_vect_base ...
    vectorise(cospars.GI_kzbins) vectorise(cospars.II_kzbins)...
    alpha beta z_0 deltaz Deltaz fcat];
[cospars_fid]=change_cospars(cospars); % switch to my default parameters
cospars_fid.fid=cospars_fid;
fish.steps=[fish_steps_base vectorise(GI_steps) vectorise(II_steps) 0 0 0 0 0 0 ];
% a wide prior helps with stability sometimes:
prior=[1000*fish.steps(1:length(fish_steps_base)) 1000*vectorise(GI_steps) 1000*vectorise(II_steps) 10 10 10 0.001 10 0.001];
fish.prior=inv(diag(prior(find(abs(fish.steps)>0))).^2);

% get the fisher matrix
fish=get_lens_fisher_doall_ii(cospars_fid,[],ell_vals,z_vals,fish,k_vals);

% the Dark Energy Task force Figure of Merit:
%fish.fom

% the marginalised parameter values from the Fisher matrix:
%fish.margpars

clear tmp
for i=1:length(fish.ipuse)
    tmp{i}=cospars_fid.names{fish.ipuse(i)};
end

[h,errvals]=plotellipses_v1(cospars_fid.vect(fish.ipuse),inv(fish.matrixp),tmp,...
    'qbwb--',[0.68 0.95],3,20,[1 2 3],0.68);
