function x_vals=logspace_ln(x_min,x_max,nx)
%
% x_vals=logspace(x_min,x_max,nx)
% Equivalent of x_min:((x_max-x_min)/(nx-1)):x_max
% but for log spacing.

if (nx>1)
    d_log_x= (log(x_max)-log(x_min))./(nx-1);
else
    d_log_x=(log(x_max)-log(x_min))*1.1;
end
log_x_vals=log(x_min):d_log_x:log(x_max);
x_vals=exp(log_x_vals);
