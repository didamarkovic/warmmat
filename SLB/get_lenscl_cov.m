function [cov_cobs_m, c_obs_v]=get_lenscl_cov(ell_vals,lens_cls_vals_v,...
    nzpars,z_vals)
%
% Calculate covariances in lensing power spectra using  
% Takada & Jain 2003 eqn 13
%
% SLB 2006.

nell=length(ell_vals);
ell_start=ell_vals(1);
ell_stop=ell_vals(nell);

% could set a default z_vals here - doesn't have to be the same as in 
% rest of calc I think

sigma_gamma=nzpars.sigma_gamma;
f_sky=nzpars.f_sky;
[tmp ngals_bin]=get_nofz(nzpars,z_vals);
nbin=length(ngals_bin);

% % pack cls into handy vector form
% k=1;
% for i=1:nbin
%     for j=i:nbin
%         tmp_cl_vals_v(:,k)=lens_cls_vals(:,i,j);
%         k=k+1;
%     end
% end

if ndims(lens_cls_vals_v)==2 % Added this for compatibility with my code KM 05/08/08
    % unpack cls from handy vector form
    k=1;
    for i=1:nbin
        for j=i:nbin
            lens_cls_nl_vals(:,i,j)=lens_cls_vals_v(:,k);
            k=k+1;
        end
    end
else fprintf(1,'NOTE: flipping dimensions of cls matrix\n')
    for i=1:nbin
        for j=i:nbin
            lens_cls_nl_vals(:,i,j)=squeeze(lens_cls_vals_v(i,j,:));
        end
    end
end

% errors from Takada & Jain 2003 eqn 13
% take into account binning in ell 
ell_bins_tmp=logspace_ln(ell_start,ell_stop,nell+1);
nellperbin=ell_bins_tmp(2:(nell+1))-ell_bins_tmp(1:nell);
ident=eye(nbin);
for i=1:nbin
    for j=i:nbin
        c_obs(:,i,j) = ( lens_cls_nl_vals(:,i,j) + ...
            ident(i,j)*sigma_gamma^2 / ngals_bin(i) );
        c_obs(:,j,i)=c_obs(:,i,j); % symmetrical
    end
end
for i=1:nbin
    for j=1:nbin
        for m=1:nbin
            for n=1:nbin
                cov_cobs(:,i,j,m,n) = 1./( (2*ell_vals' + 1) ...
                    .* nellperbin' * f_sky) .*...
                    (c_obs(:,i,m).*c_obs(:,j,n) + c_obs(:,i,n).*c_obs(:,j,m));
            end
        end
    end
end
% pack the cls into a vector of cls, and pack cov into a matrix
k=1;
for i=1:nbin
    for j=i:nbin
        i_ind(k)=i;
        j_ind(k)=j;
        k_inds(i,j)=k;
        c_obs_v(:,k)=c_obs(:,i,j);
        cl_vals_v(:,k)=lens_cls_nl_vals(:,i,j);
        k=k+1;
    end
end
for k=1:length(i_ind)
    for l=1:length(i_ind)
        cov_cobs_m(:,k,l)=cov_cobs(:,i_ind(k),j_ind(k),i_ind(l),j_ind(l));
    end
end

