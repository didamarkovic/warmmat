% to put in nfwdemo.m


clear

home = '/home/ug_km/mFilesSJB';

%% Simulate some data
%
% Defind the x and y positions for each shear observation
% just put all the galaxies on a grid to keep it simple
dtheta=1;
theta_x_vect=-15:dtheta:15; % angle in x direction on the sky, in arcmin
theta_y_vect=theta_x_vect; % angle in y direction on the sky, in arcmin
% make a matrix of all the x values 
[theta_x theta_y]=meshgrid(theta_x_vect,theta_y_vect);
%
zclust=0.05;
zsource=1; 
M_200_true=5e14; % h^-1 M_solar
conc_true=10; % NFW concentration parameter
theta_cx_true=1.2;
theta_cy_true=2.4;
[gamma_1, gamma_2, kappa]=nfwshears(theta_x,theta_y,...
    M_200_true,conc_true,theta_cx_true,theta_cy_true,zclust,zsource);
% clf
%imagesc(theta_x_vect,theta_y_vect,gamma_1); set(gca,'ydir','normal'); colorbar
%imagesc(theta_x_vect,theta_y_vect,gamma_2); set(gca,'ydir','normal'); colorbar

%
clf; 
imagesc(theta_x_vect,theta_y_vect,kappa); 
set(gca,'ydir','normal'); 
colorbar
colormap jet
% Plot shear sticks using Keith Biner's function
hold on
PlotShearMap(gamma_1,gamma_2,theta_x_vect,theta_y_vect,10,'w-')
%
% Add some noise to make it like an observation:
% set noise level in each pixel
ngals_per_sqarcmin = 30;
noise_g1=0.3/sqrt(ngals_per_sqarcmin*dtheta^2)*ones(size(gamma_1));
noise_g2=0.3/sqrt(ngals_per_sqarcmin*dtheta^2)*ones(size(gamma_2));
% 0.3 is approx the noise on gamma_1 and gamma_2 estimates for a
% single galaxy
gamma_1_obs=gamma_1+noise_g1.*randn(size(gamma_1));
gamma_2_obs=gamma_2+noise_g2.*randn(size(gamma_2));
% hold on; plotsticks(theta_x,theta_y,gamma_1,gamma_2,'y-')
PlotShearMap(gamma_1_obs,gamma_2_obs,theta_x_vect,theta_y_vect,10,'r-')
%
% package up all the data into a structure, to simplify things
fprintf(1,'Done simulating some data\n')
fprintf(1,'Have plotted the kappa map\n');
fprintf(1,'Press any key to continue\n')
pause

%% Calculate the chisq between this data and some trial model
M_200_trial=8e14; 
nfwpars=[M_200_trial,conc_true,theta_cx_true,theta_cy_true,zclust,zsource];
% 
minrad=2; % an optional input to nfwchisq, which allows you to ignore all
% points within a radius of minrad arcmin from the center of the predicted
% SIS in the chisq.
% Also, where noise_g1=0 or noise_g2=0 the points will be ignored in the
% chisq. 
% For this reason nfwchisq now outputs a parameter npoints, which is the
% total number of points that were in practice used in the chisq. 
% see "help nfwchisq" for more info
[chisq_trial chisqmap npoints gamma_1_trial gamma_2_trial]=nfwchisq(nfwpars,...
    theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad);
PlotShearMap(gamma_1_trial,gamma_2_trial,theta_x_vect,theta_y_vect,10,'k-')
% check how sensible this is:
chisq_per_dof=chisq_trial/npoints
% Should be around 1 for the true model, although will always be around 1
% probably, because shear data is very noisy, and noisy data gives
% chisq_per_dof~1 anyway.
fprintf(1,'Just calculated the chisq for a trial SIS model\n')
fprintf(1,'For M_200=%5.3g, conc=%5.3f, x=%5.3f, y=%5.3f, find chisq=%f\n',...
    M_200_trial,conc_true,theta_cx_true,theta_cy_true,chisq_trial);
fprintf(1,'Press any key to continue\n')
pause


%% Plot chisq as a function of M_200
M_200_vals=1e14:1e14:2e15;
clear chisq_vals
for is=1:length(M_200_vals)
    nfwpars=[M_200_vals(is),conc_true,theta_cx_true,theta_cy_true,zclust,zsource];
    chisq_vals(is)=nfwchisq(nfwpars,...
            theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad);
end
chisq_norm=chisq_vals -min(chisq_vals);
clf
plot(M_200_vals,chisq_norm,'bx')
xlabel('M_{200}   /  h^{-1} M_{solar}')
ylabel('\chi^2')
% Or probability instead:
clf
prob=exp(-chisq_norm/2);
plot(M_200_vals,prob,'bx')
hold on
plot(M_200_vals,prob,'b-')
[lower,upper]=confreg(M_200_vals,prob,0.68,100,'n')
bfp=(upper+lower)/2; 
err=(upper-lower)/2;
xlabel('M_{200}   /  h^{-1} M_{solar}')
ylabel('Probability')
v=axis;
plot(lower*[1 1],[v(3) v(4)],'b:')
plot(upper*[1 1],[v(3) v(4)],'b:')
plot(M_200_true*[1 1],[v(3) v(4)],'r--')
% peak will not be at exactly sigmav_true due to noise realisation
fprintf(1,'Probability as a function of M_200 of NFW\n')
fprintf(1,' assuming that conc, x, y position is already known\n')
fprintf(1,'M_{200} = ( %5.3f +/- %5.3f )*1e14 h^-1 M_{solar} \n',bfp/1e14,err/1e14)
fprintf(1,'(M_{200}-truth)/truth *100 =  %5.3f +/- %5.3f per cent \n',...
    (bfp-M_200_true)/M_200_true*100,err/M_200_true*100)
fprintf(1,'Press any key to continue\n')
pause

% show degeneracy between M200 and c
%% Make a 3d cube of likelihoods and marginalise to find all pars
% Always try coarse grids first, before increasing the number of steps
fprintf(1,'Calculating probability on a 2d grid...\n')
M_200_vals=1e14:1e14:2e15;
conc_vals = 1:0.5:20; 
% could also vary x and y:
%cx_vals = theta_x_vect;
%cy_vals = theta_y_vect;
cx_vals = theta_cx_true;
cy_vals = theta_cy_true;
clear chisq_vals
for is=1:length(M_200_vals)
    fprintf(1,'%i of %i\n',is,length(M_200_vals));
    for ic = 1:length(conc_vals)
        for icx=1:length(cx_vals)
            for icy=1:length(cy_vals)
                nfwpars=[M_200_vals(is),conc_vals(ic),cx_vals(icx),cy_vals(icy),zclust,zsource];
                chisq_vals(is,ic,icx,icy)=nfwchisq(nfwpars,...
                    theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad);
            end
        end
    end
end
chisq_norm=chisq_vals-min(min(chisq_vals));
axiss={M_200_vals,conc_vals,cx_vals,cy_vals};
axislabels={'M_200','conc','x','y'}
plotoneset_cubec(chisq_norm,axiss,axislabels,[1 2],'qbwr-','r-',[0.68],0.68,10)
% If varying x and y, change [1 2] to [1 2 3 4] to plot them:
%plotoneset_cubec(chisq_norm,axiss,axislabels,[1 2 3 4],'qbwr-','r-',[0.68],0.68,10)
subplot(2,2,2)
hold on
plot(conc_true,M_200_true,'kx')
fprintf(1,'1 and 2d marginalised probability distributions\n')

% can also get constraints on x and y center of lens using the above

