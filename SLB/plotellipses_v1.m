function [h,errvals]=plotellipses_v1(bfp,covmat,namesuse,...
    col,conf,nsig,nsteps,iusenow,dotted1d,axlims)

%namesuse=names(iusepars+2)
%%% Compare marginalised ellipses from covmat and this Hess

if (~exist('conf')) conf=[0.68 .95]; end
if (~exist('nsig')) nsig=3; end % no of sigma to make grid
if (~exist('nsteps')) nsteps=30; end % no. steps in grid
if (~exist('iusenow')) iusenow=1:length(bfp); end % which pars
if (~exist('dotted1d')) dotted1d=max(conf); end % lines on 1d plots

% estimate good width of grid in parameter space
%covmat=inv(Hess);
pvals=zeros(length(bfp),nsteps);
for i=1:length(bfp)
  pe(i)=sqrt(covmat(i,i));
  pvals(i,:)=((-pe(i)*nsig):2*nsig*pe(i)/(nsteps-1):(pe(i)*nsig))...
    + bfp(i);
end


if (~exist('axlims'))
    for i=1:length(bfp)
        axlims(i,1)=min(pvals(i,:));
        axlims(i,2)=max(pvals(i,:));
    end
end

npars=length(iusenow);

washeld=ishold;

for ipar1=1:npars
  for ipar2=(ipar1+1):npars
      
    subplot(npars,npars,npars*(ipar1-1)+ipar2)
    if (washeld) hold on; end
    box on 
    
  % calculate chisq values for one pair of params
    clear chisq
    for ipx=1:nsteps
      p(ipar1)=pvals(ipar1,ipx);
      for ipy=1:nsteps
        p(ipar2)=pvals(ipar2,ipy);
        vec=[(p(ipar1)-bfp(ipar1)) (p(ipar2)-bfp(ipar2))];
%        chisq(ipx,ipy)=2*vec*...  !! bug: why the 2? 23 oct 2006
        chisq(ipx,ipy)=vec*...
            inv(covmat([ipar1 ipar2],[ipar1 ipar2]))*vec';
      end
    end

  % plot
  plot2dconfgeneral(pvals(ipar2,:),pvals(ipar1,:),...
      (chisq-min(min(chisq)))/2, ...
                    conf,1,100,col,0);
  axis([axlims(ipar2,1) axlims(ipar2,2) axlims(ipar1,1) axlims(ipar1,2)])

  xlabel(namesuse{ipar2})
  ylabel(namesuse{ipar1})
  
  end
end

% the 1d plots
for ipar=1:npars
  i=ipar;
  subplot(npars,npars,npars*(ipar-1)+ipar)
  if (washeld) hold on; end
  box on
  
  % calculate chisq values for one pair of params
  clear chisq p
  for ipx=1:nsteps
    p(ipar)=pvals(ipar,ipx);
    vec=[(p(ipar)-bfp(ipar))];
%*    chisq(ipx)=2*vec*inv(covmat(ipar,ipar))*vec';
% Why was this factor of 2 in here??!! Fixed 17 Jan 2007
    chisq(ipx)=vec*inv(covmat(ipar,ipar))*vec';
  end
  x=pvals(ipar,:);
  clear p
  p=exp(-(chisq-min(min(chisq)))/2);
  if (length(x)>1)
    xstep=x(2)-x(1);
    pnorm=p/max(p);
    if (length(col)>4)
      h(ipar)=plot(x,pnorm,col(4:length(col)));
    end
    [lower,upper]=confreg(x,p,dotted1d,10,'n');
    axis([axlims(ipar,1) axlims(ipar,2) 0 1.05])
    bfptmp=(upper+lower)/2;
    err=(upper-lower)/2;
%    [names(i,:),'= ',num2str(bfp),' +/- ',num2str(err)]
    fprintf(1,'%s = %12.8f +/- %12.8f at %3.1f perc. confidence\n', ...
        namesuse{i},bfptmp,err,dotted1d*100);
    errvals(i)=err;
  end
  
  xlabel(namesuse{ipar});
  ylabel('Probability');
end

