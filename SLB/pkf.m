function [pk primordial newA]=pkf(Gamma,sigma8,kvals,ns,n_run,k_pivot,A)
% 
% Calculate matter power spectrum using Ma 9605198 eqn 7
%
% PK=PKF(GAMMA,SIGMA_8,KVALS)
% 
% GAMMA = exp(-2 Omega_b h) Omega_m h
% SIGMA_8 is the sigma_8 value at the redshift in question
% KVALS are the values at which PK is returned
%       In addition KVALS are used for the sigma_8 integral so need to 
%       be reasonably fine and covering a wide enough range.
%       k is in units of h Mpc^-1
%
% Added ns, nrun, SLB 22 Jan 2007 
% PK=PKF(GAMMA,SIGMA_8,KVALS,NS,NRUN)
% 
% Optionally specify pivot wavenumber, and primordial amplitude A
% PK=PKF(GAMMA,SIGMA_8,KVALS,NS,NRUN,KPIVOT,A)
%
% version 2
% I'm modifying this to get rid of the for loop and have an array instead,
% because the time is reduced by factor of 10 at least.
% Also, added output "primordial" 
% 22.06.2010 KM

if (~exist('ns')) ns=1; end
if (~exist('n_run')) n_run=0; end
if (~exist('k_pivot')) k_pivot=0.05; end  % units of Mpc^-1. As in pinibins paper.
%if (exist('A')) norm_by_A=1; else norm_by_A=0; A=3E6; end
if (exist('A'))
    norm_by_A=1;
    warning('You probably shouldnt be setting As AND sig8!'); % 16.07.2013 KM
else
    norm_by_A=0;
    A=3E6*k_pivot;
end % 12.04.2010 KM

newA = A;

% get P(k) from fitting formula from Ma 9605198 eqn7
% c1 = 2.34; c2 = 3.89; c3 = 16.1^2; c4 = 5.46^3; c5 = 6.71^4; 
% for ik=1:length(kvals)
%   k=kvals(ik);
%   q = k/Gamma;
%   t(ik) = log(1+c1*q)/(c1*q*(1 + c2*q + c3*q^2 + c4*q^3 + c5*q^4)^0.25);
%   %primordial= A*k;
%   %primordial = A*k^n_s;
%   % ln P_chi = ln A + (ns-1) ln (k/kp) + nrun/2 (ln (k/kp))^2 
%   lnrat = log(k/k_pivot);
%   %primordial=A*exp((ns-1)*lnrat + n_run/2*lnrat^2)*k;
%   primordial=A*exp((ns-1)*lnrat + n_run/2*lnrat^2)*k/k_pivot; % 12.04.2010 KM
%   pk(ik) = primordial*t(ik)^2;
% end
%loglog(kvals,pk)

% Writing as an array -KM -22.06.2010
c1 = 2.34; c2 = 3.89; c3 = 16.1^2; c4 = 5.46^3; c5 = 6.71^4;
q = kvals/Gamma;
tfn = log(1+c1*q)./(c1*q.*(1 + c2*q + c3*q.^2 + c4*q.^3 + c5*q.^4).^0.25);
lnrats = log(kvals/k_pivot);
%primordial=A*exp((ns-1)*lnrat + n_run/2*lnrat^2)*k;
primordial=A*exp((ns-1)*lnrats + n_run./2*lnrats.^2).*kvals/k_pivot; % 12.04.2010 KM
pk = primordial.*tfn.^2;


if (norm_by_A==0)
    pk=pknorms8(kvals,pk,sigma8);
    % Now find A_s:
%     if n_run==0
%         prim = pk./t.^2;
%         newA_vals = prim.*exp((1-ns)*log(kvals/k_pivot))*k_pivot./kvals;
%         newA = interp1(kvals,newA_vals,k_pivot);
%     end
end

% Doing testing - KM
% if n_run==0
%     prim = pk./t.^2;
%     newA_vals = prim.*exp((1-ns)*log(kvals/k_pivot))*k_pivot./kvals;
%     %newA_vals = prim.*exp((1-ns)*log(kvals/k_pivot))./kvals;
%     newA = interp1(kvals,newA_vals,k_pivot);
%     fprintf(['\tThe new A_s = ' num2str(newA) '\n'])
% end

% Add this as a check (19.04.2010 KM):
% if norm_by_A
%     sig8 = sigmar_frompk(kvals,pk,8);
%     diff = abs(sigma8/sig8);
%     %if diff>10^(-5) 
%         fprintf(['\tThe old to new sigma8 ratio = ' num2str(1-diff) '\n']);
%     %end
% end