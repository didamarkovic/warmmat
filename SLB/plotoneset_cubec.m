function h=plotoneset_cubec(data,axiss,names,pars2plot, ...
           conf2dcol,col,clevels,dotted1d,smallfs,smw)
% data is now a cube of chisq values
% derived from plotoneset_cube but now using cell technology

% eg. pars2plot=[3,4,5,6]

nd=size(data)

if (~exist('conf2dcol')) conf2dcol='qbwr-'; end
if (~exist('col')) col='normr-.'; end
if (~exist('clevels')) clevels=[0.68 0.95]; end
if (~exist('dotted1d')) dotted1d=0.95; end
if (~exist('smallfs')) smallfs=5; end

washeld=ishold;

npars=length(pars2plot)

for ipar1=1:npars
  ix=pars2plot(ipar1)
  for ipar2=1:(ipar1-1)
    iy=pars2plot(ipar2);
    subplot(npars,npars,npars*(ipar2-1)+ipar1)
    if (washeld) hold on; end
    % marginalise over the other directions
    datamarg=exp(-data/2);
    datamarg(find(isnan(datamarg)))=0;
    for i=1:length(nd)
       if ((i~=ix)&(i~=iy))
         i
         datamarg=sum(datamarg,i);
       end
    end
    plt=squeeze(-log(datamarg));
    plot2dconfc(ix,iy,plt',clevels,nd,axiss,names,conf2dcol)
    set(gca,'FontSize',smallfs)
    set(get(gca,'XLabel'),'FontSize',smallfs)
    set(get(gca,'YLabel'),'FontSize',smallfs)
  end
  pause(0.1)
end

for ipar=1:npars
  i=pars2plot(ipar);
  subplot(npars,npars,npars*(ipar-1)+ipar)
  if (washeld) hold on; end
  % marginalise over the other directions
  datamarg=exp(-data/2);
  datamarg(find(isnan(datamarg)))=0;
  for imarg=1:length(nd)
    if (imarg~=i)
      imarg;
      datamarg=sum(datamarg,imarg);
    end
  end
  plt=squeeze(-log(datamarg));
  x=axiss{i};
  if (length(col)>4)
    h(ipar)=plot(x,exp(-plt),col(5:length(col)));
  end
  [lower,upper]=confreg(x,exp(-plt),dotted1d,100,col);
  bfp=(upper+lower)/2;
  err=(upper-lower)/2;
  fprintf(1,'%s = %8.4f +/- %8.4f\n',names{i},bfp,err)
  xlabel(names{i})
  ylabel('Likelihood')
  set(gca,'FontSize',smallfs)
  set(get(gca,'XLabel'),'FontSize',smallfs)
  set(get(gca,'YLabel'),'FontSize',smallfs)
  %j=j+1;
end
