function [chisq chisqmap npoints gamma_1_trial gamma_2_trial]=nfwchisq(...
    nfwpars,theta_x,theta_y,...
    gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad)
% Obtain the chisq between some shear data and a NFW model
%
% CHISQ = NFWCHISQ(NFWPARS,THETA_X,THETA_Y,GAMMA_1,GAMMA_2,NOISE_G)
%
% CHISQ is the chisq value from the trial NFW model parameterised
% by NFWPARS=[M200 CONC THETA_CX THETA_CY]
% and the data given by THETA_X,THETA_Y,GAMMA_1_OBS, GAMMA_2_OBS
% and NOISE_G
%
% where
% SIGMAV is the velocity dispersion of the SIS in units of km/s
% THETA_CX, THETA_CY is the center of the lens
% THETA_X is angle in x direction on the sky, in arcmin, at each shear
% THETA_Y is angle in y direction on the sky, in arcmin
% GAMMA_1 and GAMMA_2 are the shears in the x, y coordinates
% NOISE_G is the error on GAMMA_1 or GAMMA_2
%
%
% [CHISQ CHISQMAP NPOINTS] = NFWCHISQ(NFWPARS,THETA_X,THETA_Y,...
%                                GAMMA_1,GAMMA_2,NOISE_G1,NOISE_G2,MINRAD)
%
% NOISE_G1 is the noise on GAMMA_1
% NOISE_G2 is the noise on GAMMA_2 (by default assumed same as NOISE_G1)
%
% MINRAD enables you to specify to ignore all points within a radius
% of minrad arcmin from the center of the predicted SIS in the chisq.
%
% Where noise_g1=0 or noise_g2=0 the points will be ignored in the
% chisq.
%
% CHISQMAP is the contribution to the chisq from each theta_x, theta_y
% position
%
% NPOINTS= total number of points that were in practice used in the chisq.
%
% SLB 7 April 2006 (adapted from sisdemo.m)

if (~exist('minrad')) minrad=0.001; end % just cut central point by default
if (~exist('noise_g2')) noise_g2=noise_g1; end

1/0;
warning off last

% unpack the data structures
M_200_trial=nfwpars(1);
conc_trial=nfwpars(2);
theta_cx_trial=nfwpars(3);
theta_cy_trial=nfwpars(4);
zclust = nfwpars(5);
zsource = nfwpars(6);

% Get the predicted data for this model
[gamma_1_trial, gamma_2_trial, kappa_trial]=nfwshears(theta_x,theta_y,...
    M_200_trial,conc_trial,theta_cx_trial,theta_cy_trial,zclust,zsource);

% Calculate chisq
chisqmap_g1 = zeros(size(gamma_1_obs));
chisqmap_g2 = zeros(size(gamma_1_obs));
% cut out shears with noise = 0 (as this is a flag to ignore)
iuse = (noise_g1>0)&(noise_g2>0);
% additionally cut out shears too close to the center 
theta = sqrt((theta_x-theta_cx_trial).^2 + (theta_y-theta_cy_trial).^2);
iuse = iuse .* (theta>minrad);
is = find(iuse);
npoints = length(is)*2;
%
chisqmap_g1(is)=((gamma_1_trial(is)-gamma_1_obs(is))./noise_g1(is)).^2;
chisqmap_g2(is)=((gamma_2_trial(is)-gamma_2_obs(is))./noise_g2(is)).^2;
% if there is a problem, it might be useful to look at the individual
% chisq maps for g1 and g2 separately to find anything odd.
chisqmap=chisqmap_g1+chisqmap_g2;
chisq=sum(sum(chisqmap));

