% Script to demonstrate using elliptical nfw codes and estimating
% ellipticities.
% See nfwdemo.m for a demo of fitting M_200 and concentration.
% See siedemo.m and siedemo.m for fitting center of lens etc
%
% SLB 30 June 2006

clear

% set random number seed so get the same noise realisation each time
randn('state',1)

% add directories containing the matlab functions that are called.
path(path,'/import/cyan/codeshare/sarah/gravlens/')
path(path,'/import/cyan/codeshare/sarah/joint/')

%% Simulate some data
%
% Defind the x and y positions for each shear observation
% just put all the galaxies on a grid to keep it simple
dtheta=1;
theta_x_vect=-6:dtheta:6; % angle in x direction on the sky, in arcmin
theta_y_vect=theta_x_vect; % angle in y direction on the sky, in arcmin
% make a matrix of all the x values 
[theta_x theta_y]=meshgrid(theta_x_vect,theta_y_vect);
%
zclust=0.3;
zsource=0.8; 
Omega_m=0.3;
n00 = 200; % use 200 times the critical density to define the radius to find mass within
nsteps=100; % number of steps in mass integration. Check answer converges...!
%nsteps =1000; % 1000 is slightly better than 100, but 10 times slower!
M_200_true=9e14; % h^-1 M_solar
conc_true=5; % NFW concentration parameter
theta_cx_true=0.2;
theta_cy_true=-0.4;
e1_true=0.3; % ellipticity of the lens 
e2_true=0; 
[gamma_1, gamma_2, kappa]=nfweshears(theta_x,theta_y,...
    M_200_true,conc_true,theta_cx_true,theta_cy_true,...
    e1_true,e2_true,zclust,zsource,Omega_m,n00,nsteps);
% clf
%imagesc(theta_x_vect,theta_y_vect,gamma_1); set(gca,'ydir','normal'); colorbar
%imagesc(theta_x_vect,theta_y_vect,gamma_2); set(gca,'ydir','normal'); colorbar

%
figure(1);% clf; 
imagesc(theta_x_vect,theta_y_vect,kappa); 
set(gca,'ydir','normal'); 
colorbar
colormap jet
% Plot shear sticks using Keith Biner's function
hold on
PlotShearMap(gamma_1,gamma_2,theta_x_vect,theta_y_vect,10,'w-')
%
% Add some noise to make it like an observation:
% set noise level in each pixel
ngals_per_sqarcmin = 30;
noise_g1=0.3/sqrt(ngals_per_sqarcmin*dtheta^2)*ones(size(gamma_1));
noise_g2=0.3/sqrt(ngals_per_sqarcmin*dtheta^2)*ones(size(gamma_2));
% 0.3 is approx the noise on gamma_1 and gamma_2 estimates for a
% single galaxy
gamma_1_obs=gamma_1+noise_g1.*randn(size(gamma_1));
gamma_2_obs=gamma_2+noise_g2.*randn(size(gamma_2));
% hold on; plotsticks(theta_x,theta_y,gamma_1,gamma_2,'y-')
PlotShearMap(gamma_1_obs,gamma_2_obs,theta_x_vect,theta_y_vect,10,'r-')
axis equal
%
% package up all the data into a structure, to simplify things
fprintf(1,'Done simulating some data\n')
fprintf(1,'Have plotted the kappa map\n');
fprintf(1,'Overlaid noise-free prediction (white)\n');
fprintf(1,'Overlaid noisy simulated data (red)\n');
fprintf(1,'Press any key to continue\n')
pause

%% Calculate the chisq between this data and some trial model
e1_trial=0;
e2_trial=0;
nfwepars=[M_200_true,conc_true,theta_cx_true,theta_cy_true,...
    e1_trial,e2_trial,zclust,zsource,Omega_m,n00,nsteps];
% 
minrad=2; % an optional input to nfwchisq, which allows you to ignore all
% points within a radius of minrad arcmin from the center of the predicted
% SIS in the chisq.
% Also, where noise_g1=0 or noise_g2=0 the points will be ignored in the
% chisq. 
% For this reason nfwchisq now outputs a parameter npoints, which is the
% total number of points that were in practice used in the chisq. 
% see "help nfwchisq" for more info
[chisq_trial chisqmap npoints gamma_1_trial gamma_2_trial]=nfwechisq(nfwepars,...
    theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad);
PlotShearMap(gamma_1_trial,gamma_2_trial,theta_x_vect,theta_y_vect,10,'k-')
% check how sensible this is:
chisq_per_dof=chisq_trial/npoints
% Should be around 1 for the true model, although will always be around 1
% probably, because shear data is very noisy, and noisy data gives
% chisq_per_dof~1 anyway.
fprintf(1,'Just calculated the chisq for a trial SIS model\n')
fprintf(1,'For M_200=%5.3g, conc=%5.3f, x=%5.3f, y=%5.3f, e1=%5.3f, e2=%5.3f find chisq=%f\n',...
    M_200_true,conc_true,theta_cx_true,theta_cy_true,e1_trial,e2_trial,chisq_trial);
fprintf(1,'Press any key to continue\n')
pause

%% Plot chisq as a function of ellipticity
e_vals=0:0.05:0.9; % NB. it doesn't like e=1
e_true = sqrt(e1_true^2 + e2_true);
phi_true=0.5*atan2(e2_true,e1_true); % use true angle
clear chisq_vals
for is=1:length(e_vals)
    fprintf(1,'Calculating chisq for ellipticity %i of %i...\n',is,length(e_vals));
    e_trial = e_vals(is);
    e1_trial = e_trial * cos(2*phi_true);
    e2_trial = e_trial * sin(2*phi_true);
    nfwepars=[M_200_true,conc_true,theta_cx_true,theta_cy_true,e1_trial,e2_trial,zclust,zsource,Omega_m,n00,nsteps];
    chisq_vals(is)=nfwechisq(nfwepars,...
            theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad);
end
chisq_norm=chisq_vals -min(chisq_vals);
% Plot probability and find 68% limits
figure(2);% clf
prob=exp(-chisq_norm/2);
plot(e_vals,prob,'bx')
hold on
plot(e_vals,prob,'b-')
[lower,upper]=confreg(e_vals,prob,0.68,100,'n')
bfp=(upper+lower)/2; 
err=(upper-lower)/2;
xlabel('Ellipticity (a-b)/(a+b) of lens')
ylabel('Probability')
v=axis;
plot(lower*[1 1],[v(3) v(4)],'b:')
plot(upper*[1 1],[v(3) v(4)],'b:')
plot(e_true*[1 1],[v(3) v(4)],'r--')
% peak will not be at exactly M200_true due to noise realisation
fprintf(1,'Probability as a function of ellipticity of NFW\n')
fprintf(1,' assuming that M_200, conc, x, y position and angle is already known\n')
fprintf(1,'e =  %5.3f +/- %5.3f  \n',bfp,err)
fprintf(1,'(e-truth)/truth *100 =  %5.3f +/- %5.3f per cent \n',...
    (bfp-e_true)/e_true*100,err/e_true*100)
fprintf(1,'Press any key to continue\n')
pause

% show degeneracy between e1 and e2
%% Make a 3d cube of likelihoods and marginalise to find all pars
% Always try coarse grids first, before increasing the number of steps
fprintf(1,'Calculating probability on a 2d grid...\n')
M_200_vals=M_200_true;
M_200_vals= 5e14:2e14:16e14;
conc_vals = conc_true;
cx_vals = theta_cx_true;
cy_vals = theta_cy_true;
e1_vals = -0.6:0.1:0.6;
e2_vals = e1_vals;
clear chisq_vals
for is=1:length(M_200_vals)
    fprintf(1,'Mass value %i of %i ...\n',is,length(M_200_vals)); pause(0.0001);
    for ic = 1:length(conc_vals)
        for icx=1:length(cx_vals)
            for icy=1:length(cy_vals)
            for ie1=1:length(e1_vals)
            fprintf(1,'e1 value %i of %i ...\n',ie1,length(e1_vals)); pause(0.0001);
            for ie2=1:length(e2_vals)
                %fprintf(1,'  e2 value %i of %i ...\n',ie2,length(e2_vals)); pause(0.0001);
                nfwepars=[M_200_vals(is),conc_vals(ic),cx_vals(icx),cy_vals(icy),e1_vals(ie1),e2_vals(ie2),zclust,zsource,Omega_m,n00,nsteps];
                chisq_vals(is,ic,icx,icy,ie1,ie2)=nfwechisq(nfwepars,...
                    theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,minrad);
            end
            end
            end
        end
    end
end
chisq_norm=chisq_vals-min(min(min(min(min(chisq_vals)))));
axiss={M_200_vals,conc_vals,cx_vals,cy_vals,e1_vals,e2_vals};
axislabels={'M_200','conc','x','y','e1','e2'}
toplot=[1 5 6]; % which axes to plot. 5 and 6 are e1 and e2
figure(3); % clf
hold on
%plotoneset_cubec(chisq_norm,axiss,axislabels,toplot,'qcr-','r-',[0.68],0.68,10)
plotoneset_cubec(chisq_norm,axiss,axislabels,toplot,'qbwr-','r-',[0.68],0.68,10)
red
lightenn
%subplot(2,2,2); hold on; plot(e1_true,e2_true,'kx')
fprintf(1,'1 and 2d marginalised probability distributions\n')
