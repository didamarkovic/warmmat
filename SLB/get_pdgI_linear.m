function [p_delta_gammaI p_EE_gammaI]=get_pdgI_linear(cospars,z_vals,k_vals);
% Get p_delta_gammaI for GI term
% Use the linear alignment model of Hirata & Seljak astro-ph/0406275

if (~isfield(cospars,'II_A')) cospars.II_A=cospars.GI_A; end
if (isfield(cospars,'GI_tieCI') & (cospars.GI_tieCI==1) )
    cospars.II_A=cospars.GI_A;
end

% Unpack parameters associated only with GI term
%* CI=cospars.GI_A*(-100*20*2.5e-17); % so units are not crazy
%* NB. is negative since GI is an anticorrelation
factor=-100*20*2.5e-17; % so units are not crazy
zpivot=cospars.GI_zpivot;

% Cosmology dependence of p_delta_gammaI
% cospars_fid=cospars.fid; % unpack fiducial values for cospars
if (cospars.GI_fixpk>=3)
    Omega_m=cospars.fid.Omega_m;
else
    Omega_m=cospars.Omega_m; % use current value for Omega_m (?)
end

% Get dependence of matter density on redshift
% At z=0 Omega_m = rho_m_bar / rho_crit therefore
param=get_standard_pars;
rho_m_bar_z0 = Omega_m * param.rho_crit; % h^2 M_solar / Mpc^-3
% rho_m(z) = rho_m(z=0) * (1+z)^3  due to conservation of matter and a=1/(1+z)
rho_m_bar = rho_m_bar_z0 * (1+z_vals).^(3);

% Get the growth factor D
% [delta_vals_tmp, z_vals_tmp]=growth_wa(cospars,1000);
% delta_vals=interp1(z_vals_tmp,delta_vals_tmp,z_vals);
% growth_vals=delta_vals./(max(delta_vals));
if (cospars.GI_fixpk>=2)
    growth_vals=cospars.fid.growth_vals;
else
    growth_vals=cospars.growth_vals;
end

% Get matter power spectrum
if (cospars.GI_nl==1)
    if (cospars.GI_fixpk>=1)
        pk_vals=cospars.fid.pk_nl_vals;
    else
        pk_vals=cospars.pk_nl_vals;
    end
else
    if (cospars.GI_fixpk>=1)
        pk_vals=cospars.fid.pk_lin_vals;
    else
        pk_vals=cospars.pk_lin_vals;
    end
end

% Calculate linear alignment model as fn of k and z
p_delta_gammaI=zeros(length(k_vals),length(z_vals));
p_EE_gammaI=zeros(length(k_vals),length(z_vals));
for ik=1:length(k_vals)
    for iz=1:length(z_vals)
        % corrected a missing D on 21 Mar 2007
        rmb=rho_m_bar(iz);
        x=(1+z_vals(iz));
        xp=(1+zpivot);
        D=growth_vals(iz);
        p=pk_vals(ik,iz);
        Dbar=D*x; %constant of proportionality is absorbed in CI
        CI=factor*cospars.GI_A;
        gamma=cospars.GI_gamma;
        p_delta_gammaI(ik,iz)= (CI *rmb /Dbar)   *p *(x/xp)^gamma;
        % The following line only includes the first term of H&S eqn 16
        CI=factor*cospars.II_A;
        gamma=cospars.II_gamma;
        p_EE_gammaI(ik,iz)   = (CI *rmb /Dbar)^2 *p *(x/xp)^gamma;
    end
end
