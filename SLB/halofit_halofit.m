function pnl=halofit_halofit(rk,rn,rncur,rknl,plin,om_m,om_v)
% this version is as unchanged as possible cf halofit.f code

%c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%c halo model nonlinear fitting formula as described in
%c Appendix C of Smith et al. (2002)
%
%       subroutine halofit(rk,rn,rncur,rknl,plin,pnl,pq,ph)
%
%       implicit none
%
%       real*8 gam,a,amod,b,c,xmu,xnu,alpha,beta,f1,f2,f3,f4
%       real*8 rk,rn,plin,pnl,pq,ph
%       real*8 om_m,om_v,om_b,h,p_index,gams,sig8,amp
%       real*8 rknl,y,rncur,p_cdm
%       real*8 f1a,f2a,f3a,f4a,f1b,f2b,f3b,f4b,frac
%
%       common/cospar/om_m,om_v,om_b,h,p_index,gams,sig8,amp

gam=0.86485+0.2989*rn+0.1631*rncur;
a = 1.4861 + 1.83693*rn + 1.67618*rn*rn + 0.7940*rn*rn*rn + ...
    0.1670756*rn*rn*rn*rn - 0.620695*rncur;
a=10^a;
b=10^( 0.9463 + 0.9466*rn + 0.3084*rn*rn - 0.940*rncur);
c=10^( -0.2807 + 0.6669*rn + 0.3214*rn*rn - 0.0793*rncur);
xmu = 10^( -3.54419 + 0.19086*rn );
xnu = 10^( 0.95897 + 1.2857*rn );
alpha = 1.38848 + 0.3701*rn - 0.1452*rn*rn;
beta = 0.8291 + 0.9854*rn + 0.3400*rn^2;

if(abs(1-om_m)>0.01) % ! omega evolution
    f1a=om_m^(-0.0732);
    f2a=om_m^(-0.1423);
    f3a=om_m^(0.0725);
    f1b=om_m^(-0.0307);
    f2b=om_m^(-0.0585);
    f3b=om_m^(0.0743);
    frac=om_v/(1.-om_m);
    % !!! Not sure how the above line should be generalised to DE models!
    f1=frac*f1b + (1-frac)*f1a;
    f2=frac*f2b + (1-frac)*f2a;
    f3=frac*f3b + (1-frac)*f3a;
else
    f1=1.0;
    f2=1.;
    f3=1.;
end

y=(rk/rknl);

ph=a*y^(f1*3)/(1+b*y^(f2)+(f3*c*y)^(3-gam));
ph=ph/(1+xmu*y^(-1)+xnu*y^(-2));
pq=plin*(1+plin)^beta/(1+plin*alpha)*exp(-y/4.0-y^2/8.0);

pnl=pq+ph;