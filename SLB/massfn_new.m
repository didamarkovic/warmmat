function [dndm M_star b_M sigma_M_z cospars]=massfn_new(k_vals,pk_z_vals,M_vals,z_val,cospars)
%
% Find the number of halos as a function of mass
% given a matter power spectrum.
%
% DNDM = MASSFN(K,PK,M,OMEGA_M)
%
% K is the wavenumbers at which the power spectrum
% is defined (need good sampling for good results)
% in units of h/Mpc
%
% PK is the present day linear theory matter
% power spectrum (e.g. from pkf)
%
% M are the mass values at which the mass function
% is required (better sampling improves results)
%
% OMEGA_M is the matter density e.g. 0.3
%
% DNDM is the number of halos per unit mass as given
% by Sheth-Tormen. In units of (Mpc/h)^-3 / (M_solar/h)
%
%
% [DNDM DNDM_PS] = MASSFN(K,PK,M,OMEGA_M,GROWTH)
%
% Additionally gives the Press Schechter mass function,
% DNDM_PS
%
% Can additionally specify GROWTH, the growth factor
% at the redshift reqd.
%

% massfn_slb20081222
% modified massfn to be more transparent
% added the redshift dependence of sigma(M)
% SLB 22 Dec 2008

% massfn
% SLB 14 July 2006

% added output of sigma
% KM 02.06.2011

% see if the normalisation has been specified
if (isfield(cospars,'dndm_A'))
    dndm_A = cospars.dndm_A;
    donorm = 0;
else
    dndm_A = 1;
    donorm = 1; % then overwrite this by renormalising anyway
end
Omega_m = cospars.Omega_m;

% decide what type of mass function to calculate
if (isfield(cospars,'dndm_type'))
    dndm_type = cospars.dndm_type;
else
    dndm_type = 'ST';
end

% standard parameters
param.G = 6.673e-11; % SI units
param.c =  299792458; % m/s
param.Mpc_to_m = 1e6 * 3.0856e16; % number of meters in 1Mpc
param.H_0 = 100 * 1e3 / param.Mpc_to_m; % h/s
param.M_solar = 1.989e30; % kg
param.rho_crit = 3 * param.H_0^2 / (8 *pi * param.G) /param.M_solar ...
    *param.Mpc_to_m^3; % h^2 M_solar / Mpc^3

% Find sigma_M
rho_m_z=Omega_m*param.rho_crit*(1+z_val)^3; % see Seljak text after eqn 3 - is a fn of z
R=zeros(size(M_vals));
sigma_M_z=zeros(size(M_vals));
for im=1:length(M_vals)
    R(im) = (M_vals(im)/(rho_m_z * 4 * pi/3))^(1/3); % in Mpc/h
    sigma_M_z(im)=sigmar_frompk(k_vals,pk_z_vals,R(im));
end

% differentiate to get d_ln_sigma_by_d_ln_M
% This is where good sampling in M_vals is important
d_ln_s_by_d_ln_M=zeros(size(M_vals));
for im=1:(length(M_vals)-1)
    % Natural log is reqd (written 'log' in matlab)
    d_ln_M = log(M_vals(im+1))- log(M_vals(im));
    d_ln_sigma = log(sigma_M_z(im+1)) - log(sigma_M_z(im));
    d_ln_s_by_d_ln_M(im) = d_ln_sigma / d_ln_M;
end
d_ln_s_by_d_ln_M(im+1) = d_ln_s_by_d_ln_M(im); % fill the last one

% find delta_c using Henry 2000 fitting functions
x = (cospars.Omega_m.^(-1) -1)^(1/3) ./ (1+ z_val);
delta_c_z= 3 * (12 * pi)^(2/3) / 20 * ( 1- 0.0123 * log(1+x.^3) );

%% use eqns 2 to 4 of Seljak astro-ph/0001493

% set p and a according to the type of mass function needed
if (strcmp(dndm_type,'ST'))
    p = 0.3;
    a = 0.707;
elseif (strcmp(dndm_type,'PS'))
    p = 0;
    a = 1;
else
    fprintf(1,'ERROR: unknown dndm_type %s in massfn\n',dndm_type);
end

% find f_nu using Seljak eqn 4
A = dndm_A;
nu = (delta_c_z ./ sigma_M_z).^2;
nu_prime = a * nu;
nu_f_nu = A * (1 + nu_prime.^(-p)) .* nu_prime.^(1/2) .* exp(-nu_prime/2);
f_nu = nu_f_nu./nu;

% convert f_nu into dndm using Seljak eqn 2
d_sigma_by_d_M = d_ln_s_by_d_ln_M ./ (M_vals./sigma_M_z);
d_nu_by_d_sigma = - 2*nu./delta_c_z;
%d_nu_by_d_sigma = - 2*nu.^2./delta_c_z; %KM 06.06.2011
d_nu_by_d_M =  d_nu_by_d_sigma .* d_sigma_by_d_M;
dndm = (rho_m_z ./ M_vals) .* f_nu .* d_nu_by_d_M;

% get M_star in case handy
% Seljak 0001493 after eqn 8, says this is where nu=1
M_star = interp1(nu,M_vals,1);

% optionally normalise it
d_nu = nu(2:end) - nu(1:(end-1)); d_nu(end+1)= d_nu(end);
if (donorm==1)
    
    fprintf(1,'Normalizing mass functions...\n'); %KM
    
    % find the normalisation
    integrand = f_nu .* d_nu;
    norm = sum(integrand);
    
    % normalise it
    dndm = dndm / norm;
    f_nu = f_nu / norm;
    
    % store the normalising info 
    dndm_A_used = dndm_A / norm;
    cospars.dndm_A_used = dndm_A_used;
end

% might as well find the halo bias while we're about it
% use Seljak astro-ph/0001493 eqn 6 and text after eqn 4
%   for which he cites Sheth & Tormen 1999 MNRAS 308, 119
b_M = 1 + (nu-1)/delta_c_z + 2*p./(delta_c_z .* (1+ nu_prime.^p));
%b_M = 1 + (nu_prime-1)/delta_c_z + 2*p./(delta_c_z .* (1+ nu_prime.^p)); %KM 06.06.2011
% normalise b_M according to Seljak astro-ph/0001493 eqn 9
%integrand = f_nu .* d_nu .* b_M;
%dM_vals = M_vals(2:end) - M_vals(1:(end-1)); dM_vals(end+1)=dM_vals(end);
dlogM=log(M_vals(2)) - log(M_vals(1)); % assumes log spacing!
dM_vals=dlogM.*M_vals;
integrand = dndm .* (M_vals / rho_m_z) .* b_M .* dM_vals;
integral=sum(integrand);
b_M = b_M / integral;


%fnuint = sum( f_nu .* d_nu)  %KM
%mfnint = sum( dndm .* (M_vals  / rho_m_z) .* dM_vals) %KM

%