function [h,errvals]=plotellipses(bfp,covmat,namesuse,col,conf,...
    nsig,nsteps,iusenow,dotted1d,axlims)

% modified from plotellipses
% slb 20090114
% trying to fix a bug in chosing which parameters to plot

%namesuse=names(iusepars+2)
%% Compare marginalised ellipses from covmat and this Hess

if (~exist('conf')) conf=[0.68 .95]; end
if (~exist('nsig')) nsig=3; end % no of sigma to make grid
if (~exist('nsteps')) nsteps=30; end % no. steps in grid
if (~exist('iusenow')) iusenow=1:length(bfp); end % which pars
if (~exist('dotted1d')) dotted1d=max(conf); end % lines on 1d plots

% estimate good width of grid in parameter space
%covmat=inv(Hess);
pvals=zeros(length(iusenow),nsteps);
for j=1:length(iusenow)
    i=iusenow(j);
    pe(i)=sqrt(covmat(i,i));
    pvals(j,:)=((-pe(i)*nsig):2*nsig*pe(i)/(nsteps-1):(pe(i)*nsig))...
        + bfp(j);
end


if (~exist('axlims'))
    %for i=1:length(bfp)   
    for i=1:length(iusenow) % Changed this to make it run. 13.01.2010 KM
        axlims(i,1)=min(pvals(i,:));
        axlims(i,2)=max(pvals(i,:));
    end
end

npars=length(iusenow);

washeld=ishold;

for jpar1=1:npars
    ipar1=iusenow(jpar1);
    for jpar2=(jpar1+1):npars
        ipar2=iusenow(jpar2);
        subplot(npars,npars,npars*(jpar1-1)+jpar2)
        if (washeld) hold on; end
        box on

        % calculate chisq values for one pair of params
        clear chisq
        for jpx=1:nsteps
            p(jpar1)=pvals(jpar1,jpx);
            for jpy=1:nsteps
                p(jpar2)=pvals(jpar2,jpy);
                vec=[(p(jpar1)-bfp(jpar1)) (p(jpar2)-bfp(jpar2))];
                %        chisq(ipx,ipy)=2*vec*...  !! bug: why the 2? 23 oct 2006
                % I can't explain why, but I think the 2 was right...
                % 17.06.2010 KMarkovic
                chisq(jpx,jpy)=vec*...
                    inv(covmat([ipar1 ipar2],[ipar1 ipar2]))*vec';
            end
        end

        % plot
        plot2dconfgeneral(pvals(jpar2,:),pvals(jpar1,:),...
            (chisq-min(min(chisq)))/2, ...
            conf,1,100,col,0);
        axis([axlims(jpar2,1) axlims(jpar2,2) axlims(jpar1,1) axlims(jpar1,2)])

        xlabel(namesuse{jpar2})
        ylabel(namesuse{jpar1})

    end
end

% the 1d plots
for jpar=1:npars
    ipar=iusenow(jpar);
%    i=ipar;
    subplot(npars,npars,npars*(jpar-1)+jpar)
    if (washeld) hold on; end
    box on

    % calculate chisq values for one pair of params
    clear chisq p
    for jpx=1:nsteps
        p(jpar)=pvals(jpar,jpx);
        vec=[(p(jpar)-bfp(jpar))];
        %*    chisq(ipx)=2*vec*inv(covmat(ipar,ipar))*vec';
        % Why was this factor of 2 in here??!! Fixed 17 Jan 2007
        chisq(jpx)=vec*inv(covmat(ipar,ipar))*vec';
    end
    x=pvals(jpar,:);
    clear p
    p=exp(-(chisq-min(min(chisq)))/2);
    if (length(x)>1)
        xstep=x(2)-x(1);
        pnorm=p/max(p);
        if (length(col)>4)
            h(ipar)=plot(x,pnorm,col(4:length(col)));
        end
        [lower,upper]=confreg(x,p,dotted1d,10,'n');
        axis([axlims(jpar,1) axlims(jpar,2) 0 1.05])
        bfptmp=(upper+lower)/2;
        err=(upper-lower)/2;
        %    [names(i,:),'= ',num2str(bfp),' +/- ',num2str(err)]
        fprintf(1,'%s = %12.8f +/- %12.8f at %3.1f perc. confidence\n', ...
            namesuse{jpar},bfptmp,err,dotted1d*100);
        errvals(i)=err;
    end

    xlabel(namesuse{jpar});
    ylabel('Probability');
end
