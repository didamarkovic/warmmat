function [Dang]= Dist(z1,z2,Ho,OmegaM)
% 
% Determines radial distance in megaparsecs in a flat universe between two 
% red shifts.
% Angular diameter= Dist(z1,z2,Ho, OmegaM)
%
% Note: if input for redshifts are vectors, the resulting matrix of
% distances has same z2 within a column & increasing z1. And every column
% has a different z2.
% 
% Notes:
% i)z2<z1.  
% ii)Assumes flat universe.  As such, OmegaM+OmageL=1.
% iii)The user must define Ho, the Hubble constant (in kms-1Mpc-1) for the
% present day, and OmegaM, the mass density parameter.
%
% David Sutton, 17 Jan 2006.

fprintf('Using modified Dist.m! It is probably wrong!!!\n')

%Define constants and the red-shifts in question
az1 = 1./(1 + z1); 
az2 = 1./(1 + z2); % I added the matrix dots here, KM
c = 3*10^5; % c is in km/s
OmegaL = 1 - OmegaM; 

%Define variables and the function to be integrated 
% I deleted the lines defining z & a because they were not needed, KM
F = @(a)(c/Ho).*(a.^-2)./(((OmegaM.*(a.^-3))+OmegaL).^(1/2));

%Define Integral 
% I added the for loops, KM
for id = 1:length(az1)
    for jd = 1:length(az2)
        I(id,jd) = quad(F,az1(id),az2(jd));
        %fprintf(1,'calculated distance between z = %1.4f and z = %1.4f\n',...
        %    z1(id),z2(jd));
    end
end

%Define the function of the integral - made it a loop, KM
for elno = 1:length(az1)
    Dang(elno,:) = az1(elno).*I(elno,:);
end