function [pk_nl_smiths pk_lin_smiths pk_nl_pd_smiths]=pks_nonlin_halofits(...
    k_vals,pk_lin,z_vals,growth_vals,Omega_m_z,Omega_DE_z);

pk_nl_smiths=zeros(length(k_vals),length(z_vals));
pk_lin_smiths=zeros(length(k_vals),length(z_vals));
pk_nl_pd_smiths=zeros(length(k_vals),length(z_vals));

for iz=1:length(z_vals)

    z=z_vals(iz);

    % a version extremely similar to the halofit code:
    %[pk_nl_smith pk_lin_smith pk_nl_pd_smith]=haloformula_halofit(cospars,z,k_vals,verb,generalamp);
    
    % a version that is called like my old pk_nonlin code for P+D
    pk_lin_vals(:,iz) = pk_lin* growth_vals(iz).^2;
    g=growth_sup_vals(iz);
    [pk_nl_smith pk_lin_smith pk_nl_pd_smith] = pk_nonlin_halofit(k_vals,pk_lin,g);
    
    pk_nl_smiths(:,iz)=pk_nl_smith;
    pk_lin_smiths(:,iz)=pk_lin_smith;
    pk_nl_pd_smiths(:,iz)=pk_nl_pd_smith; % their P+D

end