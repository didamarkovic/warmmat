% demonstrate non-linear matter power spectrum
% type pk_nonlin_demo at the matlab prompt at sit back!

% (Need to copy files from /import/cyan/codeshare/sarah/gravlens
% and /import/cyan/codeshare/sarah/joint into current directory
% or can set your path e.g. type at the matlab prompt:
%path(path,'/import/cyan/codeshare/sarah/gravlens','/import/cyan/codeshare/sarah/gravlens')

clear; clf

% choose cosmological parameters
Omega_b=0.047;
Omega_m=0.3;
Omega_DE =1-Omega_m; % flat universe
w=-1;
h=0.70;
c=3e5;  % in units of km s^-1
H_0=100; % in units of h km/s Mpc^-1
D_H=c/H_0; % So D_H is in units of Mpc /h

% define z values at which to calculate everything
z_vals=[0.01:0.02:0.1 0.11:0.1:2 3:1:10 12:1:30]

% choose k valuse for integrals 
nk=2000;
k_start=0.0001;
k_stop=40;
dlnk= (log(k_stop)-log(k_start))./(nk-1);
lnk_vals=log(k_start):dlnk:log(k_stop);
k_vals=exp(lnk_vals);

% make a look-up table of power spectrum 
gamma= exp(-2 * Omega_b * h) *Omega_m * h
%gamma= exp(- Omega_b*(1+sqrt(2*h)/Omega_m )) *Omega_m * h; % from Refregier et al
% !!! NB. I'm not sure this fits with the coefs in pkf??!
% 0.1427
sigma_8=0.88;
pk_vals=pkf(gamma, sigma_8, k_vals);
% peaks at k=0.02; NB. k is in units of h Mpc^-1

% make a look-up table of growth factor estimates
[delta_vals_tmp, z_vals_tmp]=growth_wconst(Omega_m,Omega_DE,w,1000);
% switch to sensible zvals
delta_vals=interp1(z_vals_tmp,delta_vals_tmp,z_vals);
growth_vals=delta_vals./(max(delta_vals));

% find the growth supression factor needed by PD96
nz=1000;
[delta_vals, z_vals_tmp]=growth_wconst(Omega_m,Omega_DE,w,nz);
[delta_vals_SCDM, z_vals_tmp]=growth_wconst(1,0,-1,nz);
% Growth suppression factor seems to be normalised to 1 at high z
% All goes wrong if normalise at too high a z. Use z=20
[tmp iz]=min(abs(z_vals_tmp-20)); iz=iz(1);
%z_vals_tmp(iz)
growth_sup_vals= delta_vals./delta_vals_SCDM / (delta_vals(iz) /delta_vals_SCDM(iz));
g_general=interp1(z_vals_tmp,growth_sup_vals,z_vals);

clear F
for i=1:length(z_vals)
    iz=length(z_vals)-i+1;

    z_vals(iz);
    pk_lin= pk_vals* growth_vals(iz);
    g=g_general(iz);
    pk_nl_vals = pk_nonlin(k_vals,pk_lin,g);

    clf
    set(gcf,'Color',[1 1 1]); % want a white background for ppt movie
    subplot(2,1,1)
    loglog(k_vals,pk_lin,'b-')
    hold on
    plot(k_vals,pk_nl_vals,'r--')
    axis([1e-2 1 2e1 2e4])
    v=axis;
    xlabel('k / ( h Mpc^{-1})')
    ylabel('P(k) / units')
    title(['z=',num2str(z_vals(iz))]);
    legend('Linear','Non-linear',3)
    
    subplot(2,1,2)
    ratio=pk_nl_vals./pk_lin;
    semilogx(k_vals,ratio,'g-')
    [tmp ik_nonlin]=min(abs(ratio-1.5)); ik_nonlin=ik_nonlin(1); % in case there are more than 1
    hold on 
    axis([v(1) v(2) 0 5]); v=axis;
    plot([1 1]*k_vals(ik_nonlin),[v(3) v(4)],'k:')
    legend('Ratio','NL=1.5*linear',2)
    %title('Ratio')
    xlabel('k / ( h Mpc^{-1})')
    ylabel('Ratio')
    F(i)=getframe(gcf);
    pause(0.01)
end

! rm trash.avi
fprintf(1,'Compressing and saving movie to trash.avi ...\n')
if (ispc)
    movie2avi(F,'trash.avi','compression','None','fps',5)
else
    movie2avi(F,'trash.avi','fps',5); % unix/linux versions doesn't compress
end

% set back to the way it was
set(gcf,'Color',get(0,'defaultFigureColor'));