function [n_z_tot ngals_bin]=get_nofz_equal_tophat(s,z_vals,verb)
% n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);
%
% Adding notes (KM):
% s - object containing s.verb (whether or not to plot), s.ng
%     (normalisation), s.nbin (no. of z-bins), s.alpha (eg 2), s.beta (1.5),
%     s.z_0 (e.g. 0.5-0.7), s.Deltaz (~0.0001), s.fcat (?),
%     depending on your survey.
% z_vals - redshifts of the source galaxies to be binned
% verb - whether or not to plot (set either verb or s.verb to 1 to plot)

% set defaults
if (~exist('verb')) verb=0; end % verbosity level
if (~isfield(s,'verb')) s.verb=0; end
if (~isfield(s,'ng')) s.ng=1; end % arbitrary normalisation if not reqd

% make overall Smail et al n(z)
% n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);
n_z=z_vals.^s.alpha .* exp(-(z_vals/s.z_0).^s.beta);

% find bin divisions
% normalise in a way that makes it easier to find z bin divisions
n_z_tmp = s.nbin* n_z/sum(n_z);
cn_z=cumsum(n_z_tmp);
% plot(z_vals,cn_z)
%eps=1e-5; %removed since unused KM
z_cuts=interp1(cn_z,z_vals,1:(s.nbin-1));
z_cuts_l = [0 z_cuts];
z_cuts_u = [z_cuts max(z_vals)];

% cut up the z distribution
for ibin=1:s.nbin
    iz=find((z_vals>z_cuts_l(ibin))&(z_vals<z_cuts_u(ibin)));
    n_z_tot(:,ibin)=zeros(size(n_z));
    n_z_tot(iz,ibin)=n_z(iz);
    ngals_bin(ibin)=sum(n_z_tot(:,ibin));
    % normalise wrt chi later
end
ngals_bin=ngals_bin*s.ng/sum(ngals_bin);

% check everything
if ((verb>=1)|(s.verb>=1))
    clf
    for ibin=1:s.nbin
        if (mod(ibin,2)==1) col='r-'; else col='b--'; end
        plot(z_vals,n_z_tot(:,ibin),col)
        hold on
    end
end

% nomalise
nbin=size(n_z_tot,2);
for ibin=1:s.nbin
    norm_nz_tot=sum(n_z_tot(:,ibin));
    n_z_tot(:,ibin)=n_z_tot(:,ibin)/norm_nz_tot;
end
