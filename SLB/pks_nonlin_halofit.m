function [pk_nl_smiths pk_lin_vals]=pks_nonlin_halofit...
    (k_vals,pk_lin,z_vals,growth_vals,cospars,replicatesmith,exp_k2r2_lookup,r_vals)

if (~exist('replicatesmith','var')) replicatesmith=0; end

if (replicatesmith==1)
    % use their forumla for the growth factor:
    % NB this is wrong if w.ne.-1 or neutrinos etc
    om_m0=cospars.Omega_m; om_v0=cospars.Omega_DE;
    for iz=1:length(z_vals)
        z=z_vals(iz);
        [om_m tmp om_v]=densities_wa(cospars,z);
        grow=2.5*om_m/(om_m^(4./7.)-om_v+(1.0+om_m/2.)*(1.+om_v/70.));
        grow0=2.5*om_m0/(om_m0^(4./7.)-om_v0+(1.0+om_m0/2.)*(1.+om_v0/70.));
        growth_vals(iz)=grow/grow0 / (1+z);
    end
end

if ( (~exist('exp_k2r2_lookup','var')) || (~exist('r_vals','var')) )
    % make a lookup table of k and r values
    r_vals = 1./k_vals; % seems like a reasonable set of r values to try
    exp_k2r2_lookup=zeros(length(k_vals),length(r_vals));
    for ir=1:length(r_vals)
        r=r_vals(ir);
        exp_k2r2_lookup(:,ir)=exp(-k_vals.^2*r^2);
    end
end

pk_nl_smiths=zeros(length(k_vals),length(z_vals));
pk_lin_vals=zeros(length(k_vals),length(z_vals));
for iz=1:length(z_vals)

    z=z_vals(iz);

    % a alternative version extremely similar to the halofit code:
    % generalamp=0; % use same eqns as in Smith et al
    % generalamp=1; % use the more accurate eqns used in pk_nonlin_halofit
    %[pk_nl_smith pk_lin_smith pk_nl_pd_smith]=haloformula_halofit(...
    % cospars,z,k_vals,verb,generalamp);

    % a version that is called like my old pk_nonlin code for P+D
    growth=growth_vals(iz);
    pk_lin_z=pk_lin* growth.^2;
    [pk_nl_smith] = pk_nonlin_halofit(k_vals,pk_lin_z,growth,z,cospars,...
        replicatesmith,exp_k2r2_lookup,r_vals);

    pk_lin_vals(:,iz) = pk_lin_z;
    pk_nl_smiths(:,iz)=pk_nl_smith;
    %pk_lin_vals(:,iz)=pk_lin_smith;

end
