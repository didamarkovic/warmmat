function [lens_cls_vals_v, GI_cls_vals_v, II_cls_vals_v]=lens_GI_II_cls(...
    ell_vals,z_vals,chi,r_AR,n_z_tot,...
    k_vals,pk_nl_vals,p_delta_gammaI,p_EE_gammaI,cospars)
% Some tidying done Wed 21 Mar 2007. See backup_16mar07/lens_GI... for earlier version
% lens_GI_II_cls (supercedes lens_GI_cls as it adds the II term) SLB 28 Feb 2007
% lens_GI_cls (supercedes lens_cls as it adds the GI term and uses cospars
% struct)
% lens_cls SLB 12 Jan 2007

if (isempty('p_delta_gammaI'))
    p_delta_gammaI = zeros(size(pk_nl_vals));
end
if (isempty('p_EE_gammaI'))
    p_EE_gammaI = zeros(size(pk_nl_vals));
end
param=get_standard_pars;
if (sum(abs(p_delta_gammaI(:)))>0) use_GI=1; else use_GI=0; end
if (sum(abs(p_EE_gammaI(:)))>0) use_II=1; else use_II=0; end

nbin=size(n_z_tot,2);
%* [g_vals]=get_lens_weightfn(z_vals,n_z_tot,r_AR)*3*cospars.Omega_m /(4*param.D_H^2);
% modified on Wed 21 Mar 2007 to put in *(1+z) here rather than divide by a in main integral below
oneplusz=(1+z_vals')*ones(1,nbin);
[g_vals]=get_lens_weightfn(z_vals,n_z_tot,r_AR)*3.*oneplusz*cospars.Omega_m /(4*param.D_H^2);
% Note is 4 not 2 as in H&S eqn 2 because of the factor of 2 in get_lens_weightfn ( int_tmp(jz_s)=2*n_z_tot...)

% calculate logs first to save time
nk=length(k_vals);
lpk_nl_vals=log(pk_nl_vals);
if (logical(use_GI))
    lp_delta_gammaI=log(-p_delta_gammaI);
end
if (logical(use_II))
    lp_EE_gammaI=log(p_EE_gammaI);
end
lk_vals=log(k_vals);

% make P_k(ell) (eqn 2.67)
integrand_GG=zeros(length(z_vals),length(ell_vals),nbin,nbin);
integrand_GI=zeros(length(z_vals),length(ell_vals),nbin,nbin);
integrand_II=zeros(length(z_vals),length(ell_vals),nbin,nbin);

for iz=2:length(z_vals)
    dchi(iz)=chi(1,iz)-chi(1,iz-1); % should w be in units of Mpc/h or just Mpc?
    a=1/(1+z_vals(iz));

    for il=1:length(ell_vals)
        ell_val = ell_vals(il);
        k_val = ell_val/ r_AR(1,iz);
        
        % interpolate to get relevant power spectrum numbers
        lk_val=log(k_val);
        pk_nl_val=0;
        p_delta_gammaI_val=0;
        p_EE_gammaI_val=0;
        if ((k_val>k_vals(1))&(k_val<k_vals(nk)))
            pk_nl_val=exp(interp1(lk_vals,lpk_nl_vals(:,iz),lk_val));
            if (logical(use_GI)) p_delta_gammaI_val=-exp(interp1(lk_vals,lp_delta_gammaI(:,iz),lk_val)); end
            if (logical(use_II)) p_EE_gammaI_val=exp(interp1(lk_vals,lp_EE_gammaI(:,iz),lk_val)); end
        end

        % loop over z bins
        for ibin=1:nbin
            for jbin=ibin:nbin
                
                dc=dchi(iz);
                %riza=r_AR(1,iz)*a;
                % Modified on Wed 21 Mar 2007 to remove the a (but see new
                % *(1+z) in calc of g above).
                riza=r_AR(1,iz);
                %* ni=n_z_tot(iz,ibin);  % This is wrong. Wed 21 Mar 2007
                ni=n_z_tot(iz,ibin)/dc;  % Convert from n_z_tot=n(z)dz to n(chi) reqd (read the comments at the top of get_nofz and think very carefully!)
                nj=n_z_tot(iz,jbin)/dc;
                gi=g_vals(iz,ibin);
                gj=g_vals(iz,jbin);
                   
                i_GG = dc/riza^2 * gi*gj * pk_nl_val;
                i_GI = dc/riza^2 * (gi*nj + gj*ni) * p_delta_gammaI_val;
                i_II = dc/riza^2 * ni*nj * p_EE_gammaI_val;

                % use symmetry for speed (29 Jan 2007)
                integrand_GG(iz,il,ibin,jbin)= i_GG;
                integrand_GG(iz,il,jbin,ibin)= i_GG;
                integrand_GI(iz,il,ibin,jbin)= i_GI;
                integrand_GI(iz,il,jbin,ibin)= i_GI;
                integrand_II(iz,il,ibin,jbin)= i_II;
                integrand_II(iz,il,jbin,ibin)= i_II;
            end
        end
        
    end
end

lens_cls_vals = squeeze(sum(integrand_GG,1));
GI_cls_vals = squeeze(sum(integrand_GI,1));
II_cls_vals = squeeze(sum(integrand_II,1));
%loglog(ell_vals,lens_cls_vals(:,1)'.*ell_vals.^2/(2*pi),'r--')
if (nbin==1) lens_cls_vals=lens_cls_vals'; end
if (nbin==1) GI_cls_vals=GI_cls_vals'; end
if (nbin==1) II_cls_vals=II_cls_vals'; end

% pack into handy vector form
lens_cls_vals_v=zeros(length(ell_vals),nbin*(nbin+1)/2);
GI_cls_vals_v=zeros(length(ell_vals),nbin*(nbin+1)/2);
II_cls_vals_v=zeros(length(ell_vals),nbin*(nbin+1)/2);
k=1;
for i=1:nbin
    for j=i:nbin
        lens_cls_vals_v(:,k)=lens_cls_vals(:,i,j);
        GI_cls_vals_v(:,k)=GI_cls_vals(:,i,j);
        II_cls_vals_v(:,k)=II_cls_vals(:,i,j);
        k=k+1;
    end
end
% loglog(ell_vals,lens_cls_vals(:,1,1)'.*ell_vals.^2)
