function [fish]=get_lens_fisher_doall_ii(cospars_fid,nzpars,...
    ell_vals,z_vals,fish,k_vals);
%    pars_step,parnames,verb,prior)
% e.g.
% fish.names={'Omega_m','w','wa','h','sigma_8','Omega_b','n_s','GI'};
% fish.steps=[0.01 0.01 0.01 0.01 0.01 0.005 0.005 0.01];
% fish.prior=inv(diag([10 10 10 10 10 10 10 10]).^2);
% fish.verb=1;
%

tstart_time=clock;

if (~isfield(fish,'verb')) fish.verb=0; end

if (~exist('k_vals','var')||(isempty(k_vals)))
    k_vals=logspace_ln(0.0001,2000,1000);
end

% fiducial cls
cospars_fid.isfid=1; % let lens_GI_II_cls_doall know that it is the fidicial run
[cls_vals_fid, lens_cls_fid, GI_cls_fid, II_cls_fid cospars_fid]=lens_GI_II_cls_doall(...
    cospars_fid,cospars_fid.nzpars,ell_vals,z_vals,[],[],k_vals);
cospars_fid.isfid=0; % there are no more fiducial runs

% check the fiducial cls
if (fish.verb>2)
%     %clf
%     iplot=1;
%     loglog(ell_vals,abs(GI_cls_fid(:,iplot))'.*ell_vals.^2/(2*pi),'k-')
%     hold on
%     loglog(ell_vals,abs(II_cls_fid(:,iplot))'.*ell_vals.^2/(2*pi),'g-')
%     loglog(ell_vals,lens_cls_fid(:,iplot)'.*ell_vals.^2/(2*pi),'r--')
%     loglog(ell_vals,abs(cls_vals_fid(:,iplot))'.*ell_vals.^2/(2*pi),'b:')
plot_lens_GI_II_cls_nbyn(cospars_fid.nzpars.nbin,ell_vals,...
    cls_vals_fid,lens_cls_fid,GI_cls_fid,II_cls_fid,[1:5]);
%print('-dpng',['C:/Documents and Settings/sarah\My Documents\',...
%    'scratch/gravlens/cospars/giii_photoz/10by10_1to5only_28apr07.png'])
end

% get the covariances
cls_vals_used=zeros(size(cls_vals_fid));
if (cospars_fid.dolens==1)
    cls_vals_used=cls_vals_used+lens_cls_fid;
end
if (cospars_fid.doGI==1)
    cls_vals_used=cls_vals_used+GI_cls_fid;
end
if (cospars_fid.doII==1)
    cls_vals_used=cls_vals_used+II_cls_fid;
end
if (isfield(cospars_fid,'cov_include'))
    if (cospars_fid.cov_include==0)
        [cov_cobs]=get_lenscl_cov(ell_vals,lens_cls_fid,cospars_fid.nzpars,z_vals);
    elseif (cospars_fid.cov_include==1)
        [cov_cobs]=get_lenscl_cov(ell_vals,cls_vals_fid,cospars_fid.nzpars,z_vals);
    elseif (cospars_fid.cov_include==2)
        [cov_cobs]=get_lenscl_cov(ell_vals,cls_vals_used,cospars_fid.nzpars,z_vals);
    elseif (cospars_fid.cov_include==3)
        cls_tmp=zeros(size(cls_vals_fid));
        for i=1:size(cls_vals_fid,1)
            for j=1:size(cls_vals_fid,2)
                cls_tmp(i,j)=max([cls_vals_used(i,j) lens_cls_fid(i,j)]);
            end
        end
        [cov_cobs]=get_lenscl_cov(ell_vals,cls_tmp,cospars_fid.nzpars,z_vals);

    elseif (cospars_fid.cov_include==4)
        cls_tmp=zeros(size(cls_vals_fid));
        for i=1:size(cls_vals_fid,1)
            for j=1:size(cls_vals_fid,2)
                cls_tmp(i,j)=max([cls_vals_fid(i,j) lens_cls_fid(i,j)]);
            end
        end
        [cov_cobs]=get_lenscl_cov(ell_vals,cls_tmp,cospars_fid.nzpars,z_vals);
    end
else
    [cov_cobs]=get_lenscl_cov(ell_vals,lens_cls_fid,cospars_fid.nzpars,z_vals);
end
%[cov_cobs]=get_lenscl_cov(ell_vals,cls_vals_fid,nzpars,z_vals);

% avoid clutter
% % store some things
% fish.cls_vals_fid=cls_vals_fid;
% fish.cov_cobs=cov_cobs;

% work out which parameters are varied
fish.ipuse=find(abs(fish.steps)>0);
npars=length(fish.ipuse);

% calculate derivatives wrt parameters
dcl_by_dpar=zeros(npars,length(ell_vals),length(lens_cls_fid(1,:)));
tstartp=clock;
for jp=1:npars
    ip=fish.ipuse(jp);
    if (fish.verb>=2) progress(jp,npars,tstartp,1,'   Fisher steps loop'); end

    % Make the fisher step
    %* [cospars_step]=do_fish_step(cospars_fid,fish,jp);
    cospars_step=cospars_fid;
    cospars_step.vect(ip)=cospars_fid.vect(ip)+fish.steps(ip);
    cospars_step=change_cospars(cospars_step,0); % update values used from vect

    % Get new observables for the stepped parameters
    [cls_vals_step lens_cls_step GI_cls_step II_cls_step]=lens_GI_II_cls_doall(...
        cospars_step,cospars_step.nzpars,ell_vals,z_vals,[],[],k_vals);

    % leave the option of artificially removing the dependence of lensing
    % on cosmology
    cls_vals_step=zeros(size(cls_vals_step));
    if (cospars_fid.dolens==0)
        cls_vals_step=cls_vals_step+lens_cls_fid;
    else
        cls_vals_step=cls_vals_step+lens_cls_step;
    end
    if (cospars_fid.doGI==0)
        cls_vals_step=cls_vals_step+GI_cls_fid;
    else
        cls_vals_step=cls_vals_step+GI_cls_step;
    end
    if (cospars_fid.doII==0)
        cls_vals_step=cls_vals_step+II_cls_fid;
    else
        cls_vals_step=cls_vals_step+II_cls_step;
    end

    % Optionally do plots
    if (fish.verb>2)
        loglog(ell_vals,abs(GI_cls_step(:,iplot))'.*ell_vals.^2/(2*pi),'k:')
        loglog(ell_vals,abs(II_cls_step(:,iplot))'.*ell_vals.^2/(2*pi),'g:')
        loglog(ell_vals,lens_cls_step(:,iplot)'.*ell_vals.^2/(2*pi),'r--')
        loglog(ell_vals,abs(cls_vals_step(:,iplot))'.*ell_vals.^2/(2*pi),'b-')
    end

    % Get derivative
    dcl_by_dpar(jp,:,:) = (cls_vals_step-cls_vals_fid)/fish.steps(ip);

end

% Get Fisher matrix
fish.matrix=get_lens_fisher(fish.steps(fish.ipuse),ell_vals,dcl_by_dpar,cov_cobs);

% store some things in case handy later
fish.runtime=etime(clock,tstart_time);
if (~isfield(fish,'prior')) fish.prior = inv(diag([fish.steps(fish.ipuse)*1e6]).^2); end
fish.matrixp=fish.matrix+fish.prior;
fish.margpars=sqrt(diag(inv(fish.matrixp)));
% save on memory:
%fish.dcl_by_dpar=dcl_by_dpar;

% calculate fom
% adde slb 23 mar 2007
invfish=inv(fish.matrixp);
fishw=invfish(2:3,2:3);
fish.fom=1/(4*sqrt(det(fishw)));

% optionally make a summary to cut out and keep
if (fish.verb>0)
    fprintf(1,'fom = %5.2f; \n',fish.fom);
    for jp=1:npars
        ip=fish.ipuse(jp);
        fprintf(1,' d%s = %5.5f; \n',cospars_fid.names{ip},fish.margpars(jp));
    end
    fprintf(1,'     z space = %5.4f; \nn ells = %i; \nruntime = %8.3f s;\n',...
        z_vals(2)-z_vals(1),length(ell_vals),fish.runtime);
    margpars_store=fish.margpars;
    fprintf(1,'\n');
end