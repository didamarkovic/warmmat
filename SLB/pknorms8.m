function pknorm=pknorms8(kvals,pk,sigma8)
% PKNORM=PKNORMS8(KVALS,PK,SIGMA8)
% Normalise a matter power spectrum PK defined at k values
% KVALS so it has sigma8=SIGMA8
% (rms in top hat spheres of 8 h^-1 Mpc)
% assumes kvals are in (h^-1 Mpc)^-1
% assumes there are enough kvals that no need to interpolate

if (length(kvals)<100)
    fprintf(1,'NB. you may not have enough k values to get an accurate answer\nany key to continue\n')
    pause
end

% normalise so sigma8=1 by doing the integration to get sigma8
intdk=0; 
r=8;
for ik=2:length(kvals)
    k=kvals(ik);
    x=k*r;
    w=3*(sin(x)-x*cos(x))/x^3;
    tmp=pk(ik)*w^2*k^2/2/pi^2;
    logkstep=log(kvals(ik))-log(kvals(ik-1));
    intdk=intdk+tmp*logkstep*k;
end

% for this power spectrum sigma8^2=intdk
pknorm=pk*sigma8^2/intdk;
