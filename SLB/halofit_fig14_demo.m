% demonstrate making Fig 14 of Smith et al with matlab version of halofit

clear
%home='g:/'; cd([home,'work/gravlens/mstuff/']);
%addpath([home,'work/gravlens/mstuff/']); addpath([home,'work/joint/mstuff/']);
%fprintf(1,'----\n')
tic

% parameters as given by Smith et al Table 3 LCDM
cospars.Gamma=0.21;
cospars.sigma_8=0.9;
cospars.Omega_m=0.3;
cospars.Omega_DE=0.7;
% these parameters are used in my generalised versions of their eqns
cospars.w=-1;
cospars.wa=0;
cospars.n_s=1;
cospars.n_run=0;
cospars.k_pivot=0.05; % this value doesn't matter if n_run is 0
cospars.Omega_b=0.05;
cospars.h=0.7;
%z_vals=[0:0.3:3];
z_vals=[0 0.5 1 2 3];
verb=0;
k_vals=logspace_ln(1e-4,1e2,2000);
% NB. My P+D implementation doesn't converge until number of k steps is
% large

% precompute things for both methods
[delta_vals_tmp, z_vals_tmp]=growth_wa(cospars,1000);
delta_vals=interp1(z_vals_tmp,delta_vals_tmp,z_vals);
growth_vals=delta_vals./(max(delta_vals_tmp));
deltak2_to_pk=(4*pi * k_vals.^3 / (2*pi)^3);
%pk_vals=pkf(cospars.Gamma, cospars.sigma_8, k_vals,cospars.n_s,cospars.n_run,cospars.k_pivot);
%pk_vals=get_pk_ma(cospars,k_vals);
pk_vals=p_cdm_halofits(k_vals,cospars.Gamma,cospars.sigma_8)./deltak2_to_pk;

% make a lookup table of k and r values now to speed smith up
r_vals = 1./k_vals(1:(length(k_vals)-1)); % seems like a reasonable set of r values to try
exp_k2r2_lookup=zeros(length(k_vals),length(r_vals));
for ir=1:length(r_vals)
    r=r_vals(ir);
    exp_k2r2_lookup(:,ir)=exp(-k_vals.^2*r^2);
end
replicatesmith=0; % my own codes should match OK by now

% calculate non-linear matter power spectrum using halofit
[pk_nl_smiths pk_lin_smiths]=pks_nonlin_halofit(...
    k_vals,pk_vals,z_vals,growth_vals,cospars,...
    replicatesmith,exp_k2r2_lookup,r_vals);

% compare with my P+D code
growth_sup_vals=growth_sup_wa(cospars,z_vals);
[pk_nl_pds pk_lin_pds]=pks_nonlin(k_vals,pk_vals,z_vals,growth_vals,growth_sup_vals);

% plot power spectra normalised to today
cols={'k','r','m','b','g'};
clf
for iz=1:length(z_vals)
    jz=mod(iz-1,length(cols))+1;
    loglog(k_vals,pk_nl_smiths(:,iz)'/growth_vals(iz).^2,[cols{jz},'-'])
    hold on
    loglog(k_vals,pk_lin_pds(:,iz)'/growth_vals(iz)^2,'c--');
    loglog(k_vals,pk_nl_pds(:,iz)'/growth_vals(iz).^2,[cols{jz},'--']);
end
axis([1e-3 1e2 4e-4 1e5])
% can see the decrease in power just below k=0.1
% can see the non-linear scale moving to lower k at lower z

% plot delta as in Fig 14
cols={'k','r','m','b','g'};
clf
for iz=1:length(z_vals)
    jz=mod(iz-1,length(cols))+1;
    loglog(k_vals,pk_nl_smiths(:,iz)'.*deltak2_to_pk,[cols{jz},'-'])
    hold on
    %loglog(k_vals,pk_lin_pds(:,iz)'.*deltak2_to_pk,'c--');
    loglog(k_vals,pk_nl_pds(:,iz)'.*deltak2_to_pk,[cols{jz},'--']);
end
axis([2.5e-2 1e2 4e-2 3000])

% keep checking one number while tidying the code up
z=1; iz=interp1(z_vals,1:length(z_vals),z,'nearest');
k=50; ik=interp1(k_vals,1:length(k_vals),k,'nearest');
fprintf(1,'z= %5.3f; k=%5.3f; Pk_Smith=%10.5f; Pk_PD=%10.5f;\n',z_vals(iz),...
    k_vals(ik),pk_nl_smiths(ik,iz),pk_nl_pds(ik,iz))
% z= 1.000; k=50.101; Pk_Smith=   0.05224; Pk_PD=   0.06098;

toc
%