function [n_z_tot ngals_bin z_cuts_l]=get_nofz_volumelim(s,z_vals,verb);
% Assumes a volume limited survey between a max and min redshift.
% To first approx this therefore gives a result propto z^2.
% !! doesn't yet!: Actually it treats the volume integral more carefully.
% This replaces the underlying Smail et al formula.
% I include the code that allows division into bins, but
% this doesn't allow errors in photozs.
% i.e. this code is appropriate for a specz survey only.
% (e.g. for SDSS)
%
% SLB 13 Apr 2007

% set defaults
if (~exist('verb')) verb=0; end % verbosity level
if (~isfield(s,'verb')) s.verb=0; end
verb=max([s.verb verb]);
if (~isfield(s,'ng')) s.ng=1; end % arbitrary normalisation if not reqd

n_z=zeros(size(z_vals));
iuse_z=find((z_vals>s.z_min)&(z_vals<s.z_max));
for iz=iuse_z
    % zeroth order approx!
    n_z(iz)=z_vals(iz)^2; % arbitrary normalisation
end


%%% Now bin it in s.nbin bins in z, with equal nos of gals in each

% find bin divisions
% normalise in a way that makes it easier to find z bin divisions
n_z_tmp = s.nbin* n_z/sum(n_z);
cn_z=cumsum(n_z_tmp);
% plot(z_vals,cn_z)
eps=1e-5;
z_cuts=interp1(cn_z(iuse_z),z_vals(iuse_z),1:(s.nbin-1));
z_cuts_l = [0 z_cuts];
z_cuts_u = [z_cuts max(z_vals)];

% cut up the z distribution
n_z_tot=zeros(length(z_vals),s.nbin);
for ibin=1:s.nbin
    iz=find((z_vals>z_cuts_l(ibin))&(z_vals<z_cuts_u(ibin)));
    n_z_tot(iz,ibin)=n_z(iz);
    ngals_bin(ibin)=sum(n_z_tot(:,ibin));
    % normalise wrt chi later
end
ngals_bin=ngals_bin*s.ng/sum(ngals_bin);

% check everything
if (verb>=1)
    %clf
    for ibin=1:s.nbin
        if (mod(ibin,2)==1) col='r-'; else col='b--'; end
        plot(z_vals,n_z_tot(:,ibin),col)
        hold on
    end
end

% nomalise
for ibin=1:s.nbin
    norm_nz=sum(n_z_tot(:,ibin));
    n_z_tot(:,ibin)=n_z_tot(:,ibin)/norm_nz;
end