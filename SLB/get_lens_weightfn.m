function [g_vals]=get_lens_weightfn(z_vals,n_z_tot,r_AR);

% normalise n(z) just in case it wasn't done already
nbin=size(n_z_tot,2);
for ibin=1:nbin
    norm_nz_tot=sum(n_z_tot(:,ibin));
    n_z_tot(:,ibin)=n_z_tot(:,ibin)/norm_nz_tot;
end

% make a look-up table of weighting function values, g(w)
% from CH eqn 2.59
% g(w_l) = int_(w_s=w_l)^wH dw_s n(w_s) Dds(w_s,w_l) / Ds(w_s)
%  if n(w_s) = delta(z-0.8) i.e. all sources at z=0.8
g_vals = zeros(size(n_z_tot));
for ibin=1:nbin
    for iz_l=1:length(z_vals)
        %dchi=zeros(size(z_vals)); int=zeros(size(z_vals));
        iz_s_vals=(iz_l+1):length(z_vals);
        int_tmp=zeros(1,length(iz_s_vals));
        for jz_s=1:length(iz_s_vals)
            iz_s=iz_s_vals(jz_s);
            %dchi(iz_s)=chi(1,iz_s)-chi(1,iz_s-1);
            int_tmp(jz_s)=2*n_z_tot(iz_s,ibin) * ...
                r_AR(1,iz_l) * r_AR(iz_l,iz_s) / r_AR(1,iz_s);
        end
        %% It is wrong, and different, to do n(z)dchi:
        %norm_nz_tot=sum(n_z_tot(:,ibin).*dchi_nz');
        %g_vals_ck(iz_l,ibin)=sum(int.*dchi/norm_nz_tot);
        %norm_nz_tot=sum(n_z_tot(:,ibin));
        %g_vals(iz_l,ibin)=sum(int/norm_nz_tot);
        g_vals(iz_l,ibin)=sum(int_tmp);
    end
end
