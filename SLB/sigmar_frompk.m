function sigmar=sigmar_frompk(kvals,pkvals,R)
% Finds rms fluctuation within R for a matter power spectrum, Pk_vals.
% sigma_r = sigmar_frompk(k_vals, Pk_vals, R) % this is dimensionless
%
% k_vals should be an array in logarithmic steps in units of h/Mpc!
% Pk_vals are in units of h^3/Mpc^3
% R = 8 Mpc/h for sigma_8
%
% comments added by KM: 11.11.09
% version 2
% Rewriting it with an array, since this is the function that makes the
% whole thing slow, because I need to call it thousands of times.
% 21.06.2010 KMarkovic

nok = length(kvals);

logstep  =  log(kvals(2:nok))  -  log(kvals(1:(nok-1))); % dlogk = dk/k
x = kvals(1:(nok-1))*R; % dimensionless

w  =  3*(sin(x)-x.*cos(x)) ./ x.^3; % window function

tmp = pkvals(1:(nok-1)) .* w.^2 .* kvals(1:(nok-1)).^2 /2 /pi^2; % integrand

sigmar = sqrt(sum(tmp.*logstep.*kvals(1:(nok-1)))); % integrate