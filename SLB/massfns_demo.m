clear; figure

M_vals = logspace(11,15,100); % in units of M_solar/h
z_vals=0:0.2:3;

[dndms dndms_ps]=massfns(M_vals,z_vals,...
    0.047,0.3,0.7,-1,0.7,0.88);
%    Omega_b,Omega_m,Omega_DE,w,h,sigma_8);

clf
loglog(M_vals,dndms_ps(:,1),'r--')
hold on
loglog(M_vals,dndms(:,1),'b-')
for iz=2:length(z_vals)
    loglog(M_vals,dndms(:,iz),'c-')
end
loglog(M_vals,dndms(:,1),'b-'); % make sure it appears on top
axis([M_vals(1) M_vals(length(M_vals)) 1e-23 1e-12])
xlabel('Mass / (M_solar/h)')
ylabel('Mass function n(M) / ( h^4 Mpc^{-3} M_{solar}^{-1})')
legend('Press-Schechter','Sheth-Tormen z=0',...
    'Sheth-Tormen z=0.2','Sheth-Tormen z=0.4...',3)
