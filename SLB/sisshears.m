function [gamma_1 gamma_2 kappa]=sisshears(theta_x,theta_y,Dds_over_Ds,sigmav,...
    theta_cx,theta_cy)
% Calculate shears for SIS model at given angular positions on sky.
% [GAMMA_1 GAMMA_2 KAPPA]=sisshears(THETA_X,THETA_Y,DDS_OVER_DS,SIGMAV,...
%                                 THETA_CX, THETA_CY)
%
% THETA_X is angle in x direction on the sky, in arcmin, at each shear reqd
% THETA_Y is angle in y direction on the sky, in arcmin
% DDS_OVER_DS is D_ds / D_s where D_ds is lens, source distance
%             and D_s is observer to source distance. 
%             This approaches unity for nearby lens and distance sources. 
% SIGMAV is the velocity dispersion of the SIS in units of km/s
% THETA_CX, THETA_CY is the center of the lens
%
% GAMMA_1 and GAMMA_2 are the shears in the x, y coordinates
% KAPPA is the convergence
%
% e.g.
% theta_x_vect=-6:1:6; % angle in x direction on the sky, in arcmin
% theta_y_vect=-6:1:6; % angle in y direction on the sky, in arcmin
% make a grid from the x any y axis vectors
% [theta_x theta_y]=meshgrid(theta_x_vect,theta_y_vect);
% Dds_over_Ds=1; % D_ds/D_s. Assume distant source
% sigmav=500; % Velocity dispersion of SIS in units of km s^-1
% theta_cx=0;
% theta_cy=0;

% SLB 15 Dec 2005

c=3e5; % speed of light in km/s

% References to Schneider refer to his Saas Fee notes:
% "An Introduction to Gravitational Lensing and Cosmology"
% http://www.astro.uni-bonn.de/~peter/SaasFee.html

% x position of each point on grid, relative to center
if (~exist('theta_cx')) theta_cx = 0; end 
dtheta_x=theta_x-theta_cx; 

% y position of each point on grid, relative to center
if (~exist('theta_cy')) theta_cy = 0; end 
dtheta_y=theta_y-theta_cy; 

% polar coords angle of each point on grid, from center, anticlockwise from +ve x axis
angle=atan2(dtheta_y,dtheta_x); 
% clf; imagesc(theta_x,theta_y,angle); set(gca,'ydir','normal'); colorbar

% polar coords distance of each point on grid, from center
mod_theta=sqrt( dtheta_x.^2 + dtheta_y.^2 );
% clf; imagesc(theta_x,theta_y,mod_theta); set(gca,'ydir','normal'); colorbar

% Schneider eqn 52
arcmin_per_radian=60*180/pi;
theta_E=4*pi*(sigmav/c)^2 *Dds_over_Ds * arcmin_per_radian;

% Schneider eqn 53
kappa=theta_E./ (2*mod_theta);
mod_gamma=theta_E./(2*mod_theta);

% convert to gamma_1 and gamma_2
gamma_1=-mod_gamma.*cos(2*angle); % not sure how to justify minus sign..
gamma_2=-mod_gamma.*sin(2*angle);
% check
%imagesc(theta_x,theta_y,gamma_1); set(gca,'ydir','normal'); colorbar
%imagesc(theta_x,theta_y,gamma_2); set(gca,'ydir','normal'); colorbar

% Fudge the difficult bits.
% In practice won't have any galaxy shape estimates at the peak of the SIS
% anyway.
gamma_1(find(isnan(gamma_1)))=0;
gamma_2(find(isnan(gamma_2)))=0;
kappa(find(isnan(kappa)))=0;

