function u_k_m=nfw_u_k_3ds_slb20081218_2000(k_vals,M_vals,z_val,M_star,cospars)

u_k_m=zeros(length(M_vals),length(k_vals));
for iM=1:length(M_vals)
    M_val=M_vals(iM);
    % get FFT(NFW)
    [u_k]=nfw_u_k_3d_slb20081218_2000(k_vals,M_val,z_val,M_star,cospars);
    u_k_m(iM,:)=u_k;
end