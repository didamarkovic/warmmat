function [Dang]= Dist_sutton(z1,z2,Ho,OmegaM)
%Determines radial distance in megaparsecs in a flat universe between two 
%red shifts.
%            Angular diameter= Dist(z1,z2,Ho, OmegaM)
%Notes:
%i)z2<z1.  
%ii)Assumes flat universe.  As such, OmegaM+OmageL=1.
%iii)The user must define Ho, the Hubble constant (in kms-1Mpc-1) for the
%present day, and OmegaM, the mass density parameter.
%David Sutton, 17 Jan 2006.

%Define constants and the red-shifts in question
az1= 1/(1+z1);az2= 1/(1+z2);
c= 3*10^5; OmegaL=1-OmegaM;

%Define variables and the function to be integrated
z= 0:0.01:3;
a= 1./(1+z);
F = @(a)(c/Ho).*(a.^-2)./(((OmegaM.*(a.^-3))+OmegaL).^(1/2));

%Define Integral
I= quad(F,az1,az2);

%Define the function of the integral
Dang= az1*I;