function PlotShearMap(gamma1,gamma2,x,y,ScaleFactor,LineType)
% PlotShearMap  Plots a shear map.
%   PlotShearMap(GAMMA1,GAMMA2,X,Y,SCALEFACTOR,LINETYPE)
%   Overlays a stick shear map on any existing plot(e.g. mass distribution)
%   GAMMA1 & GAMMA2 are the two components of GAMMA
%   X & Y are the spatial axes
%   SCALEFACTOR is a fudge factor, just multiplies the stick length - change
%   this to get the correct size
%   LINETYPE is the line plot type, e.g. '-w', etc... See PLOT.
%Keith Biner's function

hold on;

Kappa=0;

gamma=gamma1 + i*gamma2;
gamma=gamma*ScaleFactor;

% Sometimes, because of the way that an Ifft transforms matrices differently depending on whether they are odd or even size, there is a
% discrepancy of 1 in the lengths of gamma and the x/y axis. fx / fy gives the total discrepency in the x/y direction.
% If the lengths are the same (as in virtually ALL cases when using this routine) this evaluates to zero.
fx=(length(x)-size(gamma,1))*x(2);
fy=(length(y)-size(gamma,2))*y(2);

for nx=1:size(gamma,1)  ;
    for ny=1:size(gamma,2)  ;

        % Only keep a random selection of galaxies
        %         if rand>0.5
        %             gamma1(nx,ny)=0;
        %             gamma2(nx,ny)=0;
        %             continue;
        %         end;

        % Find magnitude and phase in terms of real and imaginary components
        Gamma=abs(gamma(nx,ny));% complex    % sqrt(gamma1(nx,ny)^2+gamma2(nx,ny)^2);   %  N+B  58,59
        phi=0;


        if gamma1(nx,ny)~=0 phi=.5*(atan2(gamma2(nx,ny),gamma1(nx,ny))); end;     %  N+B  57   WITH NO KAPPA ONLY!!!!

        % When gamma1=0, phi is dependent on the points 'quadrant' of imaginary space
        if gamma2(nx,ny)<0 && gamma1(nx,ny)==0
            phi=-pi/4;
        end;
        if gamma2(nx,ny)>0 && gamma1(nx,ny)==0
            phi=pi/4;
        end;

        % Let the total line length = 0.5 * the calculated magnitude.
        halfwidth=Gamma;

        % ex/ey gives the cumulative discrepency in the x/y direction
        ex=fx*nx/size(gamma,1);
        ey=fy*ny/size(gamma,2);

        % If no shear, nothing to plot.
        if halfwidth==0 continue; end;  % take out to put in points

        %% angle = phi, length = halfwidth = sqrt((xpoint-nx)^2 + ...
        xoff=halfwidth*sin(phi);
        yoff=halfwidth*cos(phi);

        %Plot the shear taking in to account any axes discrepancy.
        plot([(y(ny)-yoff+ey), (y(ny)+yoff+ey)],[(x(nx)-xoff+ex), (x(nx)+xoff+ex)] ,LineType)

    end;
end;
hold off;
return
