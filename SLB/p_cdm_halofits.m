function ps=p_cdm_halofits(rks,gams,sig8)

for irk=1:length(rks)
    rk=rks(irk);
    ps(irk)=p_cdm_halofit(rk,gams,sig8);
end