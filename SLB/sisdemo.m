
clear

%% Simulate some data
%
% Defind the x and y positions for each shear observation
% just put all the galaxies on a grid to keep it simple
theta_x_vect=-6:1:6; % angle in x direction on the sky, in arcmin
theta_y_vect=-6:1:6; % angle in y direction on the sky, in arcmin
% make a matrix of all the x values 
[theta_x theta_y]=meshgrid(theta_x_vect,theta_y_vect);
%
Dds_over_Ds=1; % D_ds/D_s. Assume distant source
sigmav_true=500; % Velocity dispersion of SIS in units of km s^-1
theta_cx_true=1.2;
theta_cy_true=2.4;
[gamma_1, gamma_2, kappa]=sisshears(theta_x,theta_y,Dds_over_Ds,...
    sigmav_true,theta_cx_true,theta_cy_true);
%imagesc(theta_x_vect,theta_y_vect,gamma_1); set(gca,'ydir','normal'); colorbar
%imagesc(theta_x_vect,theta_y_vect,gamma_2); set(gca,'ydir','normal'); colorbar

%
clf; imagesc(theta_x_vect,theta_y_vect,kappa); set(gca,'ydir','normal'); colorbar
% Great if someone can provide the subroutine to do e.g.:
% hold on; plotsticks(theta_x,theta_y,gamma_1,gamma_2,length)
hold on
PlotShearMap(gamma_1,gamma_2,theta_x_vect,theta_y_vect,10,'w-')
% length= length in x, y units if |gamma|=1
%
% Add some noise to make it like an observation:
% set noise level in each pixel
noise_g1=0.05*ones(size(gamma_1));
noise_g2=0.05*ones(size(gamma_2));
% 0.3 is approx the noise on gamma_1 and gamma_2 estimates for a
% single galaxy
gamma_1_obs=gamma_1+noise_g1.*randn(size(gamma_1));
gamma_2_obs=gamma_2+noise_g2.*randn(size(gamma_2));
% hold on; plotsticks(theta_x,theta_y,gamma_1,gamma_2,'y-')
PlotShearMap(gamma_1_obs,gamma_2_obs,theta_x_vect,theta_y_vect,10,'r-')
%
% package up all the data into a structure, to simplify things
data=makestruct(Dds_over_Ds,theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,theta_x_vect,theta_y_vect);
fprintf(1,'Done simulating some data\n')
fprintf(1,'Have plotted the kappa map\n');
fprintf(1,'Press any key to continue\n')
pause

%% Calculate the chisq between this data and some trial model
sigmav=1000; 
theta_cx=theta_cx_true;
theta_cy=theta_cy_true;
sispars=[sigmav,theta_cx,theta_cy];
% 
minrad=2; % an optional input to sischisq, which allows you to ignore all
% points within a radius of minrad arcmin from the center of the predicted
% SIS in the chisq.
% Also, where noise_g1=0 or noise_g2=0 the points will be ignored in the
% chisq. 
% For this reason sischisq now outputs a parameter npoints, which is the
% total number of points that were in practice used in the chisq. 
% see "help sischisq" for more info
chisq_trial=sischisq(sispars,data,1,minrad);
PlotShearMap(gamma_1,gamma_2,theta_x_vect,theta_y_vect,10,'k-')
% check how sensible this is:
chisq_per_dof=chisq_trial/(2*prod(size(gamma_1_obs)))
% factor of two arises because we have both gamma1 and gamma2
% Should be around 1 for the true model, although will always be around 1
% probably, because shear data is very noisy, and noisy data gives
% chisq_per_dof~1 anyway.
fprintf(1,'Just calculated the chisq for a trial SIS model\n')
fprintf(1,'For sigmav=%f, x=%5.3f, y=%5.3f, find chisq=%f\n',...
    sigmav,theta_cx,theta_cy,chisq_trial);
fprintf(1,'Press any key to continue\n')
pause


%% Plot chisq as a function of sigmav
sigmav_vals=1:50:1000;
clear chisq_vals
for is=1:length(sigmav_vals)
    sispars=[sigmav_vals(is),theta_cx,theta_cy];
    chisq_vals(is)=sischisq(sispars,data);
end
clf
plot(sigmav_vals,chisq_vals,'bx')
xlabel('\sigma_v   /  km s^{-1}')
ylabel('\chi^2')
% Or probability instead:
clf
plot(sigmav_vals,exp(-chisq_vals/2),'bx')
xlabel('\sigma_v   /  km s^{-1}')
ylabel('Probability')
% peak will not be at exactly sigmav_true due to noise realisation
fprintf(1,'Probability as a function of sigma_v of SIS\n')
fprintf(1,' assuming that x, y position is already known\n')
fprintf(1,'Press any key to continue\n')
pause

%% Instead pretend we know sigmav and vary cx, cy
sigmav=sigmav_true;
cx_vals=-2:0.1:2;
cy_vals=-3:0.1:3;
clear chisq_vals
for icx=1:length(cx_vals)
    for icy=1:length(cy_vals)
    sispars=[sigmav,cx_vals(icx),cy_vals(icy)];
    chisq_vals(icx,icy)=sischisq(sispars,data);
    end
end
clf
imagesc(cx_vals,cy_vals,chisq_vals)
colorbar
clf
imagesc(cx_vals,cy_vals,exp(-chisq_vals/2))
colorbar
fprintf(1,'Probability as a function of x and y position of SIS center\n')
fprintf(1,' assuming that sigma_v is already known\n')
fprintf(1,'Press any key to continue\n')
pause

%% Make a 3d cube of likelihoods and marginalise to find all pars
% Always try coarse grids first, before increasing the number of steps
fprintf(1,'Calculating probability on a 3d grid...\n')
sigmav_vals=100:50:1000;
cx_vals=0:0.2:2;
cy_vals=0:0.2:3;
clear chisq_vals
for is=1:length(sigmav_vals)
    for icx=1:length(cx_vals)
        for icy=1:length(cy_vals)
            sispars=[sigmav_vals(is),cx_vals(icx),cy_vals(icy)];
            chisq_vals(is,icx,icy)=sischisq(sispars,data);
        end
    end
end
axiss={sigmav_vals,cx_vals,cy_vals};
axislabels={'\sigma_v','x','y'}
plotoneset_cubec(chisq_vals,axiss,axislabels,[1 2 3],'qbwr-','r-',[0.68],0.68,10)
fprintf(1,'1 and 2d marginalised probability distributions\n')
