function p=p_cdm_halofit(rk,gams,sig8)

%c%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%c Bond & Efstathiou (1984) approximation to the linear CDM power spectrum

%     function p_cdm(rk,gams,sig8)
%     implicit none
%     real*8 p_cdm,rk,gams,sig8,p_index,rkeff,q,q8,tk,tk8

p_index=1.;
rkeff=0.172+0.011*log(gams/0.36)*log(gams/0.36);
q=1.e-20 + rk/gams;
q8=1.e-20 + rkeff/gams;
tk=1/(1+(6.4*q+(3.0*q)^1.5+(1.7*q)^2)^1.13)^(1/1.13);
tk8=1/(1+(6.4*q8+(3.0*q8)^1.5+(1.7*q8)^2)^1.13)^(1/1.13);
p=sig8*sig8*((q/q8)^(3.+p_index))*tk*tk/tk8/tk8;

%     return
%     end


%