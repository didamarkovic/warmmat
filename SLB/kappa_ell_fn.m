function [kappa_ell r_v conc kappa0 theta_vals kappa_theta]=kappa_ell_fn(...
    ell_vals,M_val,z_l,...
    M_star_val,n_theta,D_d,D_ds_D_s,Omega_m);

% Fourier transform of convergence
%
% Sarah Bridle, March 2008

% calculate the halo concentration parameter
% a_z=10.3 * (1+z_l);
a_z=10.3 * (1+z_l)^(-0.3);
b_z=0.24 * (1+z_l)^(-0.3);
conc = a_z * (M_val/M_star_val)^(-b_z);

% % or use Seljak's eqns:
% c_0=10; % Take Seljak's advice that this is appropriate for a NFW (alpha=-1)
% beta=-0.15; % am a bit unsure whether to use this or -0.2; Check effect
% conc = c_0 * (M_val./M_star_val).^beta; % eqn 12 of Seljak 0001493

% calculate NFW parameters r_s and rho_s from M_val and conc
theta_vals_tmp=[1 2]; % not used
%Omega_m_tmp=0; % not used
n00=200;
%mn00type='critical';
mn00type='mean';
z_s=NaN; % means that it will use D_ds_D_s instead of using z_s
[tmp tmp2 rho_s r_s] = nfwshear(theta_vals_tmp,...
    M_val,conc,z_l,z_s,...
    Omega_m,n00,mn00type,D_ds_D_s);

% calculate virial radius (for theta_vals)
r_v = conc * r_s;
theta_v = r_v / D_d;

% get real space NFW profile
theta_vals=linspace(theta_v/n_theta,theta_v,n_theta);
dtheta=theta_vals(2)-theta_vals(1);
theta_vals_arcmin=theta_vals *180/pi *60;
[tmp kappa_theta] = nfwshear(theta_vals_arcmin,...
    M_val,conc,z_l,z_s,...
    Omega_m,n00,mn00type,D_ds_D_s);

% do Bessel integral
%theta_vals=theta_vals_arcmin; % see what happens
kappa_ell=zeros(size(ell_vals));
for iell=1:length(ell_vals)
    ell=floor(ell_vals(iell));
    tmp=besselj(0, (ell+0.5) * theta_vals); % use radians here
    kappa_ell(iell) = 2 * pi * sum ( ...
        theta_vals * dtheta .* kappa_theta ...
        .* tmp);
end
kappa0=2*pi * sum(theta_vals * dtheta .*kappa_theta);

% if (1>0)
%     clf
%     loglog(ell_vals,kappa_ell.*ell_vals.^2,'bx')
%     hold on
%     loglog(ell_vals,kappa_ell.*ell_vals.^2,'ro')
% end