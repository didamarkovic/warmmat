function [D_Cs D_Ms D_Ls D_As V_Cs]=Ds_wconst(z1_vals,z2_vals,Omega_m,Omega_DE,w,nx)
% 
% Call D_wconst for a range of z1 and z2 values
% See D_wconst
%
% output is the way round so can do:
% contour(z1_vals,z2_vals,D_A')
%
% SLB 7 July 2006

%fprintf(2,'\nCalculating distance measures...\n')

nz1=length(z1_vals);

for iz1=1:nz1
    
   %if (mod(iz1,floor(nz1/100))==1)
   %     fprintf(2,' %4.2f per cent complete\n',iz1/nz1*100);
   %     pause(0.01)
   %end

    z1_val=z1_vals(iz1);
    for iz2=1:length(z2_vals)
        z2_val=z2_vals(iz2);
        [D_C D_M D_L D_A V_C]=D_wconst(z1_val,z2_val,Omega_m,Omega_DE,w,nx);
        D_Cs(iz1,iz2)=D_C;
        D_Ms(iz1,iz2)=D_M;
        D_Ls(iz1,iz2)=D_L;
        D_As(iz1,iz2)=D_A;
        V_Cs(iz1,iz2)=V_C;
    end
end