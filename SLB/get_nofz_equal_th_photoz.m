function [n_z_sm ngals_bin]=get_nofz_equal_th_photoz(s,z_vals,verb);
% n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);
% This convolves the n(z) distributions from get_nofz_equal_tophat
% with a Gaussian corresonding to s.deltaz
% NB. convolving n(z) is not quite the right thing to do, since it
% does not conserve the total number of galaxies at a given z
% In fact, should do some backwards thing. (Probably index kernel
% opposite?)

% set defaults
if (~exist('verb')) verb=0; end % verbosity level
if (~isfield(s,'verb')) s.verb=0; end
if (~isfield(s,'ng')) s.ng=1; end % arbitrary normalisation if not reqd

% make overall Smail et al n(z)
% n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);
n_z=z_vals.^s.alpha .* exp(-(z_vals/s.z_0).^s.beta);

% find bin divisions
% normalise in a way that makes it easier to find z bin divisions
n_z_tmp = s.nbin* n_z/sum(n_z);
cn_z=cumsum(n_z_tmp);
% plot(z_vals,cn_z)
eps=1e-5;
z_cuts=interp1(cn_z,z_vals,1:(s.nbin-1));
z_cuts_l = [0 z_cuts];
z_cuts_u = [z_cuts max(z_vals)];

% % cut up the z distribution
% for ibin=1:s.nbin
%     iz=find((z_vals>z_cuts_l(ibin))&(z_vals<z_cuts_u(ibin)));
%     n_z_tot(:,ibin)=zeros(size(n_z));
%     n_z_tot(iz,ibin)=n_z(iz);
%     ngals_bin(ibin)=sum(n_z_tot(:,ibin));
%     % normalise wrt chi later
% end
% ngals_bin=ngals_bin*s.ng/sum(ngals_bin);

% convolve with photoz errors
%
% First make the smoothing kernel
kernel=zeros(length(z_vals),length(z_vals));
for iz=1:length(z_vals)
    z_m=z_vals(iz);
    sigma_z=s.deltaz * (1+z_m); % red-shift errors are deltaz(1+z)
    kernel(iz,:)=exp(-(z_vals-z_m).^2 / (2*sigma_z^2));
    kernel(iz,:)=kernel(iz,:)/sum(kernel(iz,:));
end
% Now smooth
n_z_th=zeros(length(n_z),s.nbin); % the top hat one
n_z_sm=zeros(length(n_z),s.nbin);
ngals_bin=zeros(s.nbin,1);
for ibin=1:s.nbin 
    iz=find((z_vals>z_cuts_l(ibin))&(z_vals<z_cuts_u(ibin)));
    %n_z_th(iz,ibin)=n_z(iz);
    % convolve with kernel (taking advantage of known zeros)
    for jiz=1:length(iz)
        iiz=iz(jiz);
        %n_z_sm(:,ibin)=n_z_sm(:,ibin) + (n_z_th(iiz,ibin)*kernel(iiz,:))';
        n_z_sm(:,ibin)=n_z_sm(:,ibin) + (n_z(iiz)*kernel(iiz,:))';
        %plot(z_vals,n_z_sm(:,ibin),'b-')
    end
    ngals_bin(ibin)=sum(n_z_sm(:,ibin));
    % normalise wrt chi later
end
ngals_bin=ngals_bin*s.ng/sum(ngals_bin);    

% check everything
if ((verb>=1)|(s.verb>=1))
    %clf
    for ibin=1:s.nbin
        if (mod(ibin,2)==1) col='r-'; else col='b--'; end
        plot(z_vals,n_z_sm(:,ibin),col)
        hold on
    end
end

% nomalise
for ibin=1:s.nbin
    norm_nz=sum(n_z_sm(:,ibin));
    n_z_sm(:,ibin)=n_z_sm(:,ibin)/norm_nz;
end
