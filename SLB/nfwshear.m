function [mod_gamma kappa rho_s r_s] = nfwshear(mod_theta,...
    M_200,conc,zclust,zsource,Omega_m,n00,mn00type,Dds_over_Ds)
%
% Calculate tangential shears for NFW model at given angular distance 
% from lens center.
%
% [GAMMA]=nfwshear(THETA,M200,CONC,ZLENS,ZSOURCE)
%
% THETA is angular distance from the lens center
%       i.e. lens center is at theta=0
% M200 is the mass enclosed in a sphere of radius r200 centered on the
%      cluster, where r200 is defined such that the mean density in the 
%      sphere is 200 times the critical density.
%      In units of h^-1 M_solar
% CONC is the NFW concetration parameter 
% ZLENS is the redshift of the lens 
% ZSOURCE is the redshift of the background galaxies
%
% GAMMA is the tangential shear
%
%
% [GAMMA KAPPA RHO_S R_S]=nfwshear(THETA,M200,CONC,ZLENS,ZSOURCE,OMEGA_M, N00,MNOOTYPE)
%
% OMEGA_M is the matter density (present day). Assumes flat universe, w=-1
% is 0.3 by default.
% N00 is 200 by default. If N00 = 500, this instead uses M500
% MNOOTYPE is 'mean' if MN00 is relative to the mean density at ZLENS
%             'critical'     "                 critical   "
% KAPPA is the convergence
%
% SLB 7 April 2006

% set defaults
if (~exist('Omega_m')) Omega_m = 0.3; end 
if (~exist('n00')) n00 = 200; end % number of times the critical density to use
if (~exist('mn00type')) mn00type = 'critical'; end 
if (~exist('Dds_over_Ds')) Dds_over_Ds = NaN; end 

% standard parameters
param.G = 6.673e-11; % SI units
param.c =  299792458; % m/s
param.Mpc_to_m = 1e6 * 3.0856e16; % number of meters in 1Mpc
param.H_0 = 100 * 1e3 / param.Mpc_to_m; % h/s
param.M_solar = 1.989e30; % kg
param.rho_crit = 3 * param.H_0^2 / (8 *pi * param.G) /param.M_solar *param.Mpc_to_m^3; % h^2 M_solar / Mpc^-3

% find cosmological critical density at redshift of the lens
%! DOH - bug. Changed to the below on 25 Aug 06 rho_c_zclust = param.rho_crit * (Omega_m /(1+zclust)^3 + (1-Omega_m)); 
rho_c_zclust = param.rho_crit * (Omega_m *(1+zclust)^3 + (1-Omega_m)); 
% assumes flat universe; Units are h^2 M_solar / Mpc^-3
rho_m_zclust = param.rho_crit * Omega_m *(1+zclust)^3;
if (strcmp(mn00type,'critical'))
    rho = rho_c_zclust;
elseif (strcmp(mn00type,'mean'))
    rho = rho_m_zclust;
else
    fprintf(1,'ERROR: MN00TYPE not recognised in nfwshear.m\n');
end

% get angular diameter distance to lens using David Sutton's Dist.m
% NB. assumes a flat universe
Dd= Dist(zclust,0,100,Omega_m); % in units of h^-1 Mpc
if (~isnan(zsource))
    Ds=Dist(zsource,0,100,Omega_m); % in units of h^-1 Mpc
    Dds=Dist(zsource,zclust,100,Omega_m); % in units of h^-1 Mpc
    Dds_over_Ds = Dds / Ds;
    % is zsource==NaN then Dds_over_Ds should be specified in arguments
else
    if (isnan(Dds_over_Ds)) 
        fprintf(1,'ERROR: Must have one of zsource or Dds_over_Ds set in nfwshear\n');
    end
end

% Navarro, Frenk & White 1996, ApJ 462, 563, eqn 3:
% rho(r) = rho_s / ( (r/r_s) *  (1+r/r_s)^2 )
% where I have defined rho_s = delta_c rho_c_zclust
% where delta_c = 200/3 * c^3 / ( ln(1+c) - c/(1+c) ), which I generalise
% to
delta_c = n00/3 * conc^3 / ( log(1+conc) - conc/(1+conc) );
rho_s = delta_c * rho_c_zclust; 
% also they say M_200 = 200 rho_c_zclust * 4/3 pi r_200^3 so:
r_n00 = ( M_200 / (n00 * rho * 4/3 * pi ) ).^(1/3);
% also they say r_s = r_200 / c
r_s = r_n00 / conc;
% Am not 100% sure I generalised it right to n00~=200 ..
%theta_200 = r_200 / Dd * 60 * 180/pi; % theta_200 in arcmin
%r_s = 10/0.7

% critical lens density
Sigma_crit = param.c^2 / (4* pi * param.G) / Dds_over_Ds / Dd /param.M_solar * param.Mpc_to_m; % M_solar / Mpc^2

% dimensionless lens distance parameter
x=mod_theta* 1/60 *pi/180 * Dd /r_s ; % x= theta * D_d / r_s

% For NFW equations, see Wright & Brainerd 2000
% N.B. I have defined rho_s = delta_c rho_c_zclust

% initialise arrays so they come out the right shape at the end
kappa = zeros(size(x));
mod_gamma = zeros(size(x));

is = find(x<1);
kappa(is) =  2.* r_s .* rho_s / Sigma_crit ...
    ./ (x(is).^2 -1) .* (1 - 2 ./ sqrt(1 - x(is).^2) .* atanh(sqrt( (1-x(is))./(1+x(is)) )) );
mod_gamma(is) = r_s * rho_s / Sigma_crit * ...
    ( 4./x(is).^2 .* log(x(is)/2) - 2./(x(is).^2-1) + ...
     4* atanh(sqrt((1-x(is))./(1+x(is)))) .* (2 - 3*x(is).^2) ...
     ./ ( x(is).^2 .* sqrt((1-x(is).^2).^3)) );

is = find(x==1);
kappa(is) = 2 * r_s * rho_s /Sigma_crit / 3;
mod_gamma(is) = r_s * rho_s / Sigma_crit * ( 10/3 + 4* log(0.5));

is = find(x>1);
kappa(is)= 2* r_s * rho_s /Sigma_crit ...
    ./ (x(is).^2 -1) .* (1 - 2 ./ sqrt(x(is).^2 -1) .* atan(sqrt( (x(is)-1)./(1+x(is)) )) );
%mod_gamma(is) = r_s * rho_s / Sigma_crit * ...
%    ( 4./x(is).^2 .* log(x(is)/2) - 2./(x(is).^2-1) + ...
%     4* atan(sqrt((x(is)-1)./(1+x(is)))) .* (2 - 3*x(is).^2) ...
%     ./ ( x(is).^2 .* sqrt((x(is).^2-1).^3)) );
% something doesn't seem to work right with atan, so use atanh
% and take real part (imaginary parts of the below are zero anyway).
 mod_gamma(is) = real (r_s * rho_s / Sigma_crit * ...
    ( 4./x(is).^2 .* log(x(is)/2) - 2./(x(is).^2-1) + ...
     4* atanh(sqrt((1-x(is))./(1+x(is)))) .* (2 - 3*x(is).^2) ...
     ./ ( x(is).^2 .* (1-x(is).^2).^1.5 ) ) );
% NB. this agrees with numerical integration of kappa values too