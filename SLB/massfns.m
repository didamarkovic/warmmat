function [dndms dndms_ps M_stars]=massfns(M_vals,z_vals,...
    omega_b,omega_m,omega_l,w,h,sigma_8);
%
% A packaged up version of massfn
%
% [dndms dndms_ps]=massfns(M_vals,z_vals,...
%    Omega_b,Omega_m,Omega_DE,w,sigma_8);
% 
% The mass function at a single redshift z_vals(iz) is
% dndm=dndms(:,iz)
%
% SLB 14 Jul 2006

% make a look-up table of power spectrum estimates 
%clf;
Gamma= exp(-2 * omega_b * h) *omega_m * h;
k_vals=logspace(-4,4,2000); % changed this to matlabs logspaec, KM
pk_vals=pkf(Gamma, sigma_8, k_vals);
%loglog(k_vals,pk_vals,'rx');
%title('linear mass power spectrum'); %I think it's linear, not sure... KM
%xlabel('k');
%ylabel('P_0');

%fprintf(1,'press any key to continue\n');
%pause

% make a look-up table of growth factors
[delta_vals_tmp, z_vals_tmp]=growth_wconst(omega_m,omega_l,w,1000);
delta_vals=interp1(z_vals_tmp,delta_vals_tmp,z_vals);
growth_vals=delta_vals./(max(delta_vals));

fprintf(1,'  Calculating mass functions')
% get the mass function
for iz=1:length(z_vals)
    fprintf(1,'.');%  Calculating mass function at redshift %5.3f...\n',z_vals(iz))
    growth=growth_vals(iz);
    if nargout==3 % Added this to avoid NaN errors KM
        [dndm dndm_ps M_star]=massfn(k_vals,pk_vals,M_vals,...
            omega_m,growth,z_vals(iz));
        M_stars(iz)=M_star;
    else
        [dndm dndm_ps]=massfn(k_vals,pk_vals,M_vals,...
            omega_m,growth,z_vals(iz));
    end
    dndms(:,iz)=dndm;
    dndms_ps(:,iz)=dndm_ps;
end
fprintf(1,'  \n');