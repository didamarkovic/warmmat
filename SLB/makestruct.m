function data=makestruct(varargin)

n=length(varargin);

data.names={};
for i=1:n
    data.names{i}=inputname(i);
    data=setfield(data,inputname(i),cell2mat(varargin(i)));
end