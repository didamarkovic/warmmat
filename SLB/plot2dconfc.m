function a=plot2dconfc(nx,ny,plt,clevels,nvals,axiss,axislabels,color)
% plot2dconf(3,2,plt,clevels,nvals,axiss,axislabels,'c'); 
% nx=3
% ny=2
% color='c'
% quick color ='qc'
% same as plot2dconf but using cell technology for axiss and axislabels
% and names

%%%%     look at plt, the parnx,parny plane
minplt=min(min(plt))
if (max(max(plt))==minplt)
  (['Minimum and maximum of plt are the same! = ',num2str(minplt)])
  return; end;
contst=minplt;
xlabs=zeros(nvals(nx),1); for j=1:nvals(nx) xlabs(j)=axiss{nx}(j); end
ylabs=zeros(nvals(ny),1); for j=1:nvals(ny) ylabs(j)=axiss{ny}(j); end
axis([xlabs(1) xlabs(nvals(nx)) ylabs(1) ylabs(nvals(ny))])

% are plots currently being held?
washeld=ishold;

plttmp=log(plt-min(min(plt))+1);
plttmp(find(isinf(plttmp)))=NaN;
plttmp(find(isnan(plttmp)))=max(max(plttmp));

if (color(1)=='c')
  contourf(xlabs,ylabs,plttmp',((max(max(plttmp)))-min(min(plttmp)))*(find(ones(64,1))-1)/64.+min(min(plttmp)))
%'
  shading flat
  if (washeld==0) hold on; end
elseif (color(2)=='c')
  contourf(xlabs,ylabs,plttmp',...
      ((max(max(plttmp)))-min(min(plttmp)))*(find(ones(20,1))-1)/20.+min(min(plttmp)))
%'
  shading flat
  if (washeld==0) hold on; end
end

%% get contour levels
% clevels=[0.68,0.90,0.95];
if (color(1)=='q')
  pltlevels=(-log(conf2dlevelz(xlabs,ylabs,exp(-plt'),clevels,1,100)))
  %'
elseif (color(1)=='c')
  if (color(2)~=' ')
    number=str2num(color(2:length(color)));
    pltlevels=(-log(conf2dlevelz(xlabs,ylabs,exp(-plt'),clevels,number,number*100)))
  else
    pltlevels=(-log(conf2dlevelz(xlabs,ylabs,exp(-plt'),clevels,4,400)))
  end
else
  pltlevels=(-log(conf2dlevelz(xlabs,ylabs,exp(-plt'),clevels,4,400)))
  %'
end
if ((length(color)>=3)&(color(1:3))=='qbw'&length(color)>3)
  contour(xlabs,ylabs,plt',pltlevels,deblank(color(4:length(color))))
else
  contour(xlabs,ylabs,plt',pltlevels,'k')
end
%'

% conf2dlevelz is the same as conf2dlevel but has an extra (the last) 
% argument which says the value to use for zlevelstep
% the value used by conf2dlevel is 100.
% xcoarse=xlabs; ycoarse=ylabs; zcoarse=exp(-plt');
% ninterp=1; zlevelstep=100

if (color(1:2)~='bw')
  if (washeld==0)
    hold off
  end
end

xlabel(axislabels{nx}); ylabel(axislabels{ny})









