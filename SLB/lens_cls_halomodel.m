function [C_kappa_tot C_kappa_1h C_kappa_2h] = lens_cls_halomodel(...
    ell_vals,z_l_vals,z_s_vals,M_vals,k_vals,n_theta,cospars);

% Calculate halo model lensing power spectrum.
%
% Sarah Bridle, March 2008

% calculate arrays based on settings
n_z_l=length(z_l_vals);
z_l_step=z_l_vals(2)-z_l_vals(1); % assumes linear spacing in z_l_vals!
dlogM=log(M_vals(2))-log(M_vals(1));
d_M_vals=dlogM.*M_vals;
n_M=length(M_vals);
n_k=length(k_vals);
n_ell=length(ell_vals);
k_min=min(k_vals);
k_max=max(k_vals);

% precalculate M_star for each redshift
[tmp1 tmp2 M_star]=massfns(logspace(9,17,100),z_l_vals,...
    cospars.Omega_b,cospars.Omega_m,cospars.Omega_DE,cospars.w,...
    cospars.h,cospars.sigma_8);
%dndms_array=massfns(M_vals,z_l_vals,...
%    cospars.Omega_b,cospars.Omega_m,cospars.Omega_DE,cospars.w,cospars.h,cospars.sigma_8);
% or using PS instead of ST for comparison with Cooray et al
[tmp1 dndms_array]=massfns(M_vals,z_l_vals,...
    cospars.Omega_b,cospars.Omega_m,cospars.Omega_DE,cospars.w,cospars.h,cospars.sigma_8);

% precompute delta_c as a function of z
x = (cospars.Omega_m.^(-1) -1)^(1/3) ./ (1+ z_l_vals);
delta_c_vals= 3 * (12 * pi)^(2/3) / 20 * ( 1- 0.0123 * log(1+x.^3) );
% check this against Eke, Cole and Frenk!!!
%plot(z_l_vals,delta_c_vals);

% precompute the volume at each redshift
D_H=3e5/100; % So D_H=c/H_0 is in units of Mpc /h
dOmega=(pi/180)^2; % 1 square degree
V_vals=zeros(size(z_l_vals));
d2VbydzdOmega_vals=zeros(size(z_l_vals));
for i_z_l=1:length(z_l_vals)
    [tmp1 tmp2 tmp3 tmp4 V_C]=D_wconst(0,z_l_vals(i_z_l),cospars.Omega_m,cospars.Omega_DE,cospars.w,1000);
    V_vals(i_z_l) = V_C * D_H^3 * z_l_step * dOmega;
    d2VbydzdOmega_vals(i_z_l)=V_vals(i_z_l) / (z_l_step * dOmega);
end
% has units of (h^-1 Mpc)^3 which will cancel with the halo mass fn

% precompute linear matter density power spectrum P(k,z) for each z_l
Gamma= exp(-2 * cospars.Omega_b *cospars.h) *cospars.Omega_m * cospars.h;
pk_vals=pkf(Gamma, cospars.sigma_8, k_vals);
% loglog(k_vals,pk_vals)
% get the growth factor as function of z
[deltavals, z_growth_vals]=growth_wconst(cospars.Omega_m,cospars.Omega_DE,cospars.w,10000);
growth = interp1(z_growth_vals,deltavals,z_l_vals);
growth_norm=growth/growth(1);
% plot(z_l_vals,growth_norm)
P_k_lookup = pk_vals'*growth_norm.^2;

% add in integral over z_s later
z_s=z_s_vals;
D_s=Dist_sutton(z_s_vals,0,100,cospars.Omega_m);
% in units of h^-1 Mpc

% step over z_l values
C_kappa_1h_part1=zeros(n_z_l,n_M,n_ell);
C_kappa_2h_part1=zeros(n_z_l,n_M,n_ell);
kappa_ell_store=zeros(n_z_l,n_M,n_ell);
sigma_M_store=zeros(n_z_l,n_M);
b_M_store=zeros(n_z_l,n_M);
tstartz=clock;
for i_z_l = 1:length(z_l_vals)
    progress(i_z_l,length(z_l_vals),tstartz,20,'   z_lens loop...');
    z_l=z_l_vals(i_z_l);
    d_z_l = z_l_step;
    d2VbydzdOmega= d2VbydzdOmega_vals(i_z_l); % comoving differential volume

    % calculate distances for this lens and source redshift
    D_d = Dist_sutton(z_l,0,100,cospars.Omega_m);
    D_ds = Dist_sutton(z_s,z_l,100,cospars.Omega_m);
    D_ds_D_s = D_ds / D_s;

    % look up power spectrum for ells reqd, at this z
    k_tries=ell_vals/D_d;
    iuse=find((k_tries>k_min)&(k_tries<k_max)); % (all others will be zero)
    P_k=zeros(1,n_ell);
    P_k(iuse)=interp1(k_vals,P_k_lookup(:,i_z_l),k_tries(iuse));

    % initialise for a new lens redshift z_l
    mass_int_1h_parts=zeros(n_M,n_ell);
    mass_int_2h_parts=zeros(n_M,n_ell);

    for i_M = 1:n_M
        M_val=M_vals(i_M);
        d_M=d_M_vals(i_M);

        % get the mass function for this mass and z
        dndM=dndms_array(i_M,i_z_l);

        % get the FFT of the halo profile for mass and z
        M_star_val=M_star(i_z_l);
        [kappa_ell r_v]=kappa_ell_fn(ell_vals,M_val,z_l,...
            M_star_val,n_theta,D_d,D_ds_D_s,cospars.Omega_m);
        kappa_ell_store(i_z_l,i_M,:)=kappa_ell;

        % find the halo bias b(M)
        delta_c = delta_c_vals(i_z_l);
        sigma_M = sigmar_frompk(k_vals,P_k_lookup(:,i_z_l),r_v);
        nu = delta_c / sigma_M;
        b_M = 1 + (nu^2-1)/delta_c;
        sigma_M_store(i_z_l,i_M)=sigma_M;
        b_M_store(i_z_l,i_M) = b_M;

        % do the integrals over mass (NB. is a fn of ell still)
        mass_int_1h_parts(i_M,:) = d_M * dndM * kappa_ell.^2;
        mass_int_2h_parts(i_M,:) = d_M * dndM * b_M * kappa_ell;

        C_kappa_1h_part1(i_z_l,i_M,:) = d_z_l * d2VbydzdOmega * ...
            mass_int_1h_parts(i_M,:);
    end
    
    mass_int_2h = sum(mass_int_2h_parts,1);
    C_kappa_2h_part1(i_z_l,1,:) = d_z_l * d2VbydzdOmega * P_k ...
        .* mass_int_2h.^2;

end
C_kappa_1h=squeeze(sum(sum(C_kappa_1h_part1,2),1))';
C_kappa_2h=squeeze(sum(sum(C_kappa_2h_part1,2),1))';
C_kappa_tot=C_kappa_1h+C_kappa_2h;