function [chisq chisqmap npoints]=sischisq(sispars,data,verb,minrad)
% Obtain the chisq between some shear data and a SIS model
%
% CHISQ = SISCHISQ(SISPARS,DATA)
%
% CHISQ is the chisq value from the trial SIS model parameterised
% by SISPARS_TRIAL=[SIGMA_V_TRIAL THETA_CX_TRIAL THETA_CY_TRIAL]
% and the data given by THETA_X,THETA_Y,GAMMA_1_OBS, GAMMA_2_OBS
% and NOISE_G1, NOISE_G2
%
% where
% SIGMAV is the velocity dispersion of the SIS in units of km/s
% THETA_CX, THETA_CY is the center of the lens
% THETA_X is angle in x direction on the sky, in arcmin, at each shear
% THETA_Y is angle in y direction on the sky, in arcmin
% GAMMA_1 and GAMMA_2 are the shears in the x, y coordinates
%
% Also
% DDS_OVER_DS is D_ds / D_s where D_ds is lens, source distance
%             and D_s is observer to source distance.
%             This approaches unity for nearby lens and distance sources.
%
% [CHISQ CHISQMAP NPOINTS] = SISCHISQ(SISPARS,DATA,VERB,MINRAD)
%
% is the same as above, except VERB says how verbose to be, e.g.
% VERB=1 gives a moderate amount of feedback
%
% MINRAD enables you to specify to ignore all points within a radius
% of minrad arcmin from the center of the predicted SIS in the chisq.
%
% Where noise_g1=0 or noise_g2=0 the points will be ignored in the
% chisq.
%
% NPOINTS= total number of points that were in practice used in the chisq.

if (~exist('minrad')) minrad=0.001; end % just cut central point by default

1/0;
warning off last

if (~exist('verb')) verb=0; end % by default don't plot lots of things

% unpack the data structures
sigmav_trial=sispars(1);
theta_cx_trial=sispars(2);
theta_cy_trial=sispars(3);
[Dds_over_Ds,theta_x,theta_y,gamma_1_obs,gamma_2_obs,noise_g1,noise_g2,...
    theta_x_vect,theta_y_vect]=unmakestruct(data);

% Get the predicted data for this model
[gamma_1_trial, gamma_2_trial, kappa_trial]=sisshears(theta_x,theta_y,Dds_over_Ds,...
    sigmav_trial,theta_cx_trial,theta_cy_trial);

% Calculate chisq
chisqmap_g1 = zeros(size(gamma_1_obs));
chisqmap_g2 = zeros(size(gamma_1_obs));
% cut out shears with noise = 0 (as this is a flag to ignore)
iuse = (noise_g1>0)&(noise_g2>0);
% additionally cut out shears too close to the center 
theta = sqrt((theta_x-theta_cx_trial).^2 + (theta_y-theta_cy_trial).^2);
iuse = iuse * find(theta>minrad);
is = find(iuse);
npoints = length(is);
%
chisqmap_g1(is)=((gamma_1_trial(is)-gamma_1_obs(is))./noise_g1(is)).^2;
chisqmap_g2(is)=((gamma_2_trial(is)-gamma_2_obs(is))./noise_g2(is)).^2;
% if there is a problem, it might be useful to look at the individual
% chisq maps for g1 and g2 separately to find anything odd.
chisqmap=chisqmap_g1+chisqmap_g2;
chisq=sum(sum(chisqmap));

if (verb==2)
    figure(1);
    clf
    imagesc(gamma_1_obs);
    set(gca,'ydir','normal');
    colorbar
    title('\gamma_1 obs')
    %
    figure(2);
    clf
    imagesc(gamma_2_obs);
    set(gca,'ydir','normal');
    colorbar
    title('\gamma_2 obs')
    %
    figure(3);
    clf
    imagesc(gamma_1_trial);
    set(gca,'ydir','normal');
    colorbar
    title('\gamma_1 pred')
    %
    figure(4);
    clf
    imagesc(gamma_2_trial);
    set(gca,'ydir','normal');
    colorbar
    title('\gamma_2 pred')
    %
    figure(5);
    clf
    imagesc(chisqmap_g1);
    set(gca,'ydir','normal');
    colorbar
    title('\chi^2 map for g_1')
    %
    figure(6);
    clf
    imagesc(chisqmap_g2);
    set(gca,'ydir','normal');
    colorbar
    title('\chi^2 map for g_2')
end

if (verb==1)
    figure(1);
    clf
    imagesc(theta_x_vect,theta_y_vect,kappa_trial);
    set(gca,'ydir','normal');
    colorbar
    title('Trial kappa and shears')
    PlotShearMap(gamma_1_obs,gamma_2_obs,theta_x_vect,theta_y_vect,10,'r-')
    PlotShearMap(gamma_1_trial,gamma_2_trial,...
        theta_x_vect,theta_y_vect,10,'g-')
end
