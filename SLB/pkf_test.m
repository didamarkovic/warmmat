% pkf_test
%
% Writing this script to test the meaning of A_s, so that it is comparable
% to the one Jochen uses in his Planck matrix.
% Will plot the matter power spectrum and possibly do other tests.
%
% 22.06.2010 KMarkovic

%clc

cospars = setWMAP7pars;
fprintf(['\tA_s = ' num2str(cospars.As) '\n'])
%cospars.As = cospars.As/cospars.k_pivot;

kvals = logspace(-4,4,1000);

[pk prim newA]= pkf(cospars.Gamma,cospars.sigma8,kvals,cospars.ns,cospars.n_run,cospars.k_pivot,cospars.As);
%[pk prim newA]= pkf(cospars.Gamma,cospars.sigma8,kvals,cospars.ns);

figure(1)
loglog(kvals,pk); hold on
loglog(kvals,prim,'k--')
plot(cospars.k_pivot,cospars.As,'ro')
plot(cospars.k_pivot,newA,'rs')