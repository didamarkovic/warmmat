function [D_C D_M D_L D_A]=D_wa_grid(z_vals,cospars)
%
% D_C_over_D_H = D_WA_GRID(ZVALS,COSPARS)
%
% calculate integrated comoving distance
% D_C / D_H in Hogg 1999 notation esp eq15 (but can vary limits of
% z integral in this function)
%
% Z1 is lower limit for redshift integral (e.g. zero)
% Z2 is upper limit for redshift integral (i.e. z of interest)
% W is one number, assumed to be a constant
% D_C_over_D_H = D_C / D_H
%
% Z1 is a vector
% values will be returned for a 2d grid of z1 values
% so size(D_C) will be MxM where M=length(z1)
% D_C(i,j) will be the distance from z1(i) to z1(j) ????
%
% [D_C D_M D_L D_A] =D_WA_GRID(ZVALS,COSPARS)
%
% Additionally returns D_M, D_L, D_A as given in Hogg 0005116
% In fact, these are all in units of D_H.
% i.e. it actually returns D_C/D_H, D_M/D_H etc
%
% !! NB. I haven't tested Omega_K ne 0 really
%
% modified from Ds_wconsts by SLB 29 Jan 2007

% Ds_wconsts was modified from Ds_wconst to take structure cospars as input
% Ds_wconst was modified from D_L_ok
% checked for z1.ne.0 against David Sutton's Dist
% SLB 7 Jul 2006

% extract cosmological parameters from structure
Omega_m=cospars.Omega_m;
Omega_DE=cospars.Omega_DE;
w0=cospars.w;
if (isfield(cospars,'wa')) wa=cospars.wa; else wa=0; end

nz=length(z_vals);
x_vals=1+z_vals;
x_step=x_vals(2:nz)-x_vals(1:(nz-1));
x_step(nz)=x_step((nz-1));

D_C=zeros(nz);
D_M=zeros(nz);
D_L=zeros(nz);
D_A=zeros(nz);

Omega_K=1-Omega_m-Omega_DE;
saok=sqrt(abs(Omega_K));

% find D_C
% Hogg eqn 14:
% E_z = sqrt(Omega_m .* x_vals.^3+ Omega_K*x_vals.^2 + Omega_DE .* x_vals.^(3*(1+w)));
%
% % From D_L_wa.m:    % use analytic eqn from Jiayu/Jochen
Omega_m_z=Omega_m .*x_vals.^3;
Omega_K_z=Omega_K*x_vals.^2;
Omega_DE_z= Omega_DE* (1./x_vals) .^ (-3*(1+w0+wa)) .*exp(3*wa*(1./x_vals -1));
E_z = sqrt( Omega_m_z + Omega_K_z + Omega_DE_z );


for iz1=1:nz
    %fprintf(1,'iz1= %i;\n',iz1); pause(0.001);
    for iz2=iz1:nz
%        fprintf(1,'iz1= %i; iz2= %i;\n',iz1,iz2);
        
        % Hogg eqn 15:
        %* D_C = sum ( 1./E_z) * xstep;
        D_C(iz1,iz2)=D_C(iz1,iz2)+sum(1./E_z(iz1:iz2).*x_step(iz1:iz2));

        % find D_M for use later
        % Hogg eqn 16
        if (Omega_K==0)
            D_M(iz1,iz2)=D_C(iz1,iz2);
        elseif (Omega_K<0)
            D_M(iz1,iz2)=1/saok*sin(saok*D_C(iz1,iz2)); % N.B. I'm using shorthand: D_i = D_i / D_H
        else
            D_M(iz1,iz2)=1/saok*sinh(saok*D_C(iz1,iz2));
        end

        % find D_A
        % Hogg eqn 18
        % and BS99 eqn 2.44 for the general case when z1.ne.0
        D_A(iz1,iz2) = D_M(iz1,iz2) / x_vals(iz2);

        % find D_L
        % Hogg eqn 21
        % and BS99 eqn 2.45 for the general case when z1.ne.0
        D_L(iz1,iz2) = D_M(iz1,iz2) * x_vals(iz2)/x_vals(iz1)^2;

    end
end