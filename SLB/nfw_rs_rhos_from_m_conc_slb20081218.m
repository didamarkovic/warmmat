function [r_s rho_s r_n00]=nfw_rs_rhos_from_m_conc_slb20081218(...
    M_n00,conc,zclust,cospars)
% find standard NFW stuff
% copied this stuff from nfwshear on Thu 18 Dec 2008
% assumes a flat universe

% extract parameters from cospars that are used
% - so that can see easily here which cospars fields are needed here
% - and to make sure that I don't edit cospars
Omega_m=cospars.Omega_m;
% set defaults
% Decide whether M_n00 is M_200 or e.g. M_500 etc
if (~isfield(cospars,'n00'))
    n00=200;
else
    n00=cospars.n00;
end
% the time at which the density is taken for calculating M200
if (~isfield(cospars,'mn00time'))
    mn00time='zclust';
else
    mn00time=cospars.mn00time;
end
% decide what density M_200 is defined relative to
if (~isfield(cospars,'mn00type'))
    mn00type='mean';
else
    mn00type=cospars.mn00type;
end

% standard parameters
param.G = 6.673e-11; % SI units
param.c =  299792458; % m/s
param.Mpc_to_m = 1e6 * 3.0856e16; % number of meters in 1Mpc
param.H_0 = 100 * 1e3 / param.Mpc_to_m; % h/s
param.M_solar = 1.989e30; % kg / M_solar
%param.M_solar_h = 1.989e30 * h; % kg/M_solar h^-1
param.rho_crit = 3 * param.H_0^2 / (8 *pi * param.G) /param.M_solar ...
    *param.Mpc_to_m^3; % h^2 M_solar / Mpc^-3

% find cosmological critical density at redshift of the lens
rho_c_zclust = param.rho_crit * (Omega_m *(1+zclust)^3 + (1-Omega_m));
% assumes flat universe; Units are h^2 M_solar / Mpc^-3
rho_m_zclust = param.rho_crit * Omega_m *(1+zclust)^3;
if (strcmp(mn00time,'zclust'))
    if (strcmp(mn00type,'critical'))
        rho = rho_c_zclust;
    elseif (strcmp(mn00type,'mean'))
        rho = rho_m_zclust;
    else
        fprintf(1,'ERROR: MN00TYPE not recognised in nfwshear.m\n');
    end
elseif (strcmp(mn00time,'today'))
    if (strcmp(mn00type,'critical'))
        rho = param.rho_crit;
    elseif (strcmp(mn00type,'mean'))
        rho =  param.rho_crit * Omega_m;
    else
        fprintf(1,'ERROR: MN00TYPE not recognised in nfw_rs_rhos_from_...\n');
    end
else
    fprintf(1,'ERROR: MN00TIME not recognised in nfw_rs_rhos_from_...\n');
end


% Navarro, Frenk & White 1996, ApJ 462, 563, eqn 3:
% rho(r) = rho_s / ( (r/r_s) *  (1+r/r_s)^2 )
% where I have defined rho_s = delta_c rho_c_zclust
% where delta_c = 200/3 * c^3 / ( ln(1+c) - c/(1+c) ), which I generalise
% to
delta_c = n00/3 * conc^3 / ( log(1+conc) - conc/(1+conc) );
rho_s = delta_c * rho; 
% also they say M_200 = 200 rho_c_zclust * 4/3 pi r_200^3 so:
r_n00 = ( M_n00 / (n00 * rho * 4/3 * pi ) ).^(1/3);
% also they say r_s = r_200 / c
r_s = r_n00 / conc;
% Am not 100% sure I generalised it right to n00~=200 ..


%