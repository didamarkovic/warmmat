function [n_z_tot ngals_bin]=get_snap_nofz(s,z_vals,verb);
% n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);


if (~exist('verb')) verb=0; end % verbosity level
if (~isfield(s,'verb')) s.verb=0; end
verb=max([s.verb verb]);

% Make z_minus the lower z and z_plus the higher z (i.e. make sense)
if (strcmp(s.convention,'19jan07'))
    tmp=s.z_plus;
    s.z_plus=s.z_minus;
    s.z_minus=tmp;
end

% If not using z_plus etc then set so they have no effect
if (~isfield(s,'z_plus'))
    s.z_plus = 0;
end
if ((~isfield(s,'xi_plus'))&(~isfield(s,'xi_minus')))
    s.xi_plus= 0; % will make f_plus = 1
    s.xi_minus=0; % flag to make f_minus =1
end
if (~isfield(s,'z_minus'))
    s.z_minus=0;
end
% if one of xi_plus or xi_minus have been specified, but not the other
% then make them equal
if (~isfield(s,'xi_minus'))
    s.xi_minus=s.xi_plus;
end
if (~isfield(s,'xi_plus'))
    s.xi_plus=s.xi_minus;
end


if (~isfield(s,'ng')) s.ng=1; end

% Usually want the same alpha, beta, z_0, xi_minus, xi_plus for all bins
% Therefore can just give one number for each parameter
% Here this number is copied for all bins
nbin=length(s.z_plus);
if (length(s.alpha   )==1) s.alpha    = s.alpha    * ones(nbin,1); end
if (length(s.beta    )==1) s.beta     = s.beta     * ones(nbin,1); end
if (length(s.z_0     )==1) s.z_0      = s.z_0      * ones(nbin,1); end
if (length(s.xi_minus)==1) s.xi_minus = s.xi_minus * ones(nbin,1); end
if (length(s.xi_plus )==1) s.xi_plus  = s.xi_plus  * ones(nbin,1); end

% make SNAP n(z) with z_m = 1.23 (as mentioned in Fig 2 caption)
% First entry: bin 1 of 2; Second entry: bin 2 of 2
%* n_z=z_vals.^alpha .* exp(-(z_vals/z_0).^beta);
clear n_z n_z_tot f_plus f_minus
for ibin=1:nbin
    n_z(:,ibin)=z_vals.^s.alpha(ibin) .* exp(-(z_vals/s.z_0(ibin)).^s.beta(ibin));
    if (s.xi_plus(ibin)>0)
        f_plus(:,ibin)=1./(1+exp( +(s.z_plus(ibin)-z_vals)/s.xi_plus(ibin) ) );
    else
        f_plus(:,ibin) = ones(length(z_vals),1);
    end
    if (s.xi_minus(ibin)>0)
        f_minus(:,ibin)=1./(1+exp( -(s.z_minus(ibin)-z_vals)/s.xi_minus(ibin) ) );
    else
        f_minus(:,ibin) = ones(length(z_vals),1);
    end
    n_z_tot(:,ibin)=n_z(:,ibin).*f_plus(:,ibin).*f_minus(:,ibin);
    ngals_bin(ibin)=sum(n_z_tot(:,ibin));
    % normalise wrt chi later
end
ngals_bin=ngals_bin*s.ng/sum(ngals_bin);
%ngals_bin/(60*180/pi)^2
% clf
%plot(z_vals,n_z_tot(:,1),'r-')
%hold on
%plot(z_vals,n_z_tot(:,2),'b-')
% This only makes sense if alpha, beta and z_0 values are the same for
% each bin!!!

% check everything
if (verb>=1)
    clf
    for ibin=1:nbin
        if (mod(ibin,2)==1) col='r-'; else col='b--'; end
        plot(z_vals,n_z_tot(:,ibin),col)
        hold on
    end
end

% nomalise
nbin=size(n_z_tot,2);
for ibin=1:nbin
    norm_nz_tot=sum(n_z_tot(:,ibin));
    n_z_tot(:,ibin)=n_z_tot(:,ibin)/norm_nz_tot;
end