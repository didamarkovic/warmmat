function [dndms dndms_ps M_stars sigma] = massfns_slb(M_vals,z_vals,...
    omega_b,omega_m,omega_l,w,h,sigma_8,k_vals,pk_vals)
%
% A packaged up version of massfn.m
%
% [dndms dndms_ps M_stars sigmas] = massfns(M_vals,z_vals,...
%    Omega_b,Omega_m,Omega_DE,w,sigma_8,cmbfast_output,kvals,trfn,pk);
% 
% The mass function at a single redshift z_vals(iz) is
% dndm=dndms(:,iz).
%
% version 1 by SLBridle - 14 Jul 2006
%
% Set cmbfast_output = 'null' and input kvals etc to avoid calculation 
% inside the function. To use Sarah's transfer function do not input any of
% the last 4 parameters.
%
% Also note sigma varies only through mass and is constant with redshift.
%
% Modified - 18/09/08 KM
% Deleted writing out - 16.10.09 KM
% Cleaned up - 19.11.09 KM
% version 2.1 (almost exactly the same as massfns.m)


% make a look-up table of power spectrum estimates 
if ~exist('k_vals','var')
    Gamma = exp(-2 * omega_b * h) *omega_m * h;
    k_vals = logspace(-4,4,2000); % changed this to matlabs logspace - KM
    pk_vals = pkf(Gamma, sigma_8, k_vals);
else % check that we have the right matter power spectrum - KM
    if min(size(pk_vals))>1
        fprintf(1,'ERROR: need P_0(k) to get mass functions!!!\n')
        return
    end
end


% make a look-up table of growth factors
[delta_vals_tmp, z_vals_tmp] = growth_wconst(omega_m,omega_l,w,1000);
%delta_vals = interp1(z_vals_tmp, delta_vals_tmp, z_vals);
%growth_vals = delta_vals ./ ( max(delta_vals) ); % Normalizing???? - KM:
growth_vals_tmp = delta_vals_tmp ./ ( max(delta_vals_tmp) ); 
growth_vals = interp1(z_vals_tmp, growth_vals_tmp, max(min(z_vals_tmp),z_vals));
    % Added this max/min check to avoid NA errors ar z = 0. KM 08.07.2013

% get the mass functions
dndms = zeros(length(M_vals),length(z_vals));
dndms_ps = dndms;
if nargout>2
    M_stars = zeros(1,length(z_vals));
    sigma = dndms;
end

for iz = 1:length(z_vals)
    
    growth = growth_vals(iz);
    
    if nargout>2 % Added this to avoid NaN errors )- KM
        
        [dndms(:,iz) dndms_ps(:,iz) M_stars(iz) sigma(:,iz)] = massfn(k_vals,pk_vals,M_vals,...
            omega_m,growth,z_vals(iz));
        
    else
        
        [dndms(:,iz) dndms_ps(:,iz)] = massfn(k_vals,pk_vals,M_vals,...
            omega_m,growth,z_vals(iz));
        
    end

end