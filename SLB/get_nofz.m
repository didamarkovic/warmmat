function [n_z_tot ngals_bin tmp]=get_nofz(nzpars,z_vals,verb)
%
% [N_Z_TOT NGALS_BIN] = GET_NOFZ(NZPARS,Z_VALS)
% 
% Returns N_Z_TOT, where N_Z_TOT(i) is the fraction of galaxies 
% with redshifts between Z_VALS(i) and Z_VALS(i+1).
% i.e. in terms of maths, N_Z_TOT is n(z)dz
% This whole thing assumes dz is a constant i.e. z_vals are
% linearly evenly spaced in z.
%
% If instead of this you wanted n(chi), then have
% to think really carefully...
% Can use the result n(z)dz = n(chi)dchi.
% e.g. this is used in get_lens_weightfn.m
%
% Or can use n(chi) = n(z)dz /dchi
% i.e. n(chi) = N_Z_TOT / dchi
%
% Adding notes (KM):
% s - object containing s.verb (whether or not to plot), s.ng
%     (normalisation), s.nbin (no. of z-bins), s.alpha (eg 2), s.beta (1.5),
%     s.z_0 (e.g. 0.5-0.7), s.Deltaz (~0.0001), s.fcat (?),
%     depending on your survey and nzpars.type for the different cutting
%     methods (choose from 'snap', 'equal_tophat', 'eqal_th_photoz', 
%     'amarar06', 'amarar06_biasbin', 'volumelim')...
% z_vals - redshifts of the source galaxies to be binned
% verb - whether or not to plot (set either verb or s.verb to 1 to plot)
%
% v2
% Added option to cut source redshifts differently to lenses.
% 06.07.2011 KM
    
% Possibly cut n(z) below zsmin! KM
if isfield(nzpars,'zsmin')
    ignore = z_vals<nzpars.zsmin;
    %old_z = z_vals;
    z_vals = z_vals(~ignore);
end

if (~exist('verb')) verb=0; end % be silent by default

if (strcmp(nzpars.type,'snap'))
    [n_z_tot ngals_bin]=get_snap_nofz(nzpars,z_vals,verb);
elseif (strcmp(nzpars.type,'equal_tophat'))
    [n_z_tot ngals_bin]=get_nofz_equal_tophat(nzpars,z_vals,verb);
elseif (strcmp(nzpars.type,'equal_th_photoz'))
    [n_z_tot ngals_bin]=get_nofz_equal_th_photoz(nzpars,z_vals,verb);
elseif (strcmp(nzpars.type,'amarar06'))
    [n_z_tot ngals_bin]=get_nofz_amarar06(nzpars,z_vals,verb);
elseif (strcmp(nzpars.type,'amarar06_biasbins'))
    [n_z_tot ngals_bin]=get_nofz_amarar06_biasbins(nzpars,z_vals,verb);
elseif (strcmp(nzpars.type,'volumelim'))
    [n_z_tot ngals_bin tmp]=get_nofz_volumelim(nzpars,z_vals,verb);
elseif (strcmp(nzpars.type,'takada_jain')) % Added 08/08/08 KM
    [n_z_tot ngals_bin] = get_nofz_td(nzpars,z_vals,verb);
else
    fprintf(1,'ERROR! Unknown nzpars.type in get_nofz.m\n')
end


% Add in zeros for at ignored redshifts! KM
if isfield(nzpars,'zsmin')
    n_z_tot = [zeros(max(find(ignore)),nzpars.nbin); n_z_tot];
end