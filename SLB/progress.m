function progress(ix,nx,tstart,npercent,string)
%
% PROGRESS(IX,NX,TSTART)
%
% Report to screen progress info for a parameter running
% from IX to NX, every 1% complete.
% Put TSTART=CLOCK; before you start the loop
%
% PROGRESS(IX,NX,TSTART,NPERCENT,STRING)
%
% Optionally specify % complete at which to report back
% By default NPERCENT is 1
%
% Optionally insert STRING before report

tmp=max(1,floor(nx*npercent/100));

if (mod(ix,tmp)==0)
    tmp=etime(clock,tstart);
    frac=(ix-1)/nx; % assumes PROGRESS is run at start of loop
    if (round(frac*100)>=(npercent))
        if (~exist('string')) string=''; end
        fprintf(1,[string,sprintf(': %4.1f percent complete.. about %4.2f minutes to go..\n',...
            frac*100,tmp/frac * (1-frac) /60)]);
        pause(0.0001)
    end
end

