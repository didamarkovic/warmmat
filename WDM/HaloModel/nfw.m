function [rhor rv ers] = nfw(cvir,Mvir,cospars,Del)
% NFW profile for many halo masses
%   16.07.2013 KMarkovic

if ~exist('Del','var') Del = 200; end % Overdensity of collapsed haloes
warning('Please input halo mass in M_sol, *not* M_sol/h!');

consts;
rhobar = cospars.omega_m * rho_crit_0_Mpc;

rv = (3*Mvir / (4*pi*Del*rhobar)).^(1/3);
rs = rv ./ cvir;

%TODO: is it log or log10??
dels = cvir.^3 * Del/3 ./ ( log(1+cvir) - cvir./(1+cvir) );

ers = 0:0.002:1;
rhor = zeros(length(ers),length(cvir));
for int = 1:length(ers)
	%rhor(int,:) = rhobar*dels ./ ( ers(int)./rs .* (1 + ers(int)./rs).^2 );
	rhor(int,:) = rhobar*dels ./ ( ers(int).*cvir .* (1 + ers(int).*cvir).^2 );
end


return