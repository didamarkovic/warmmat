function dndm = massfns_wrap(z_val,cospars,M_vals,alpha);



cospars_slb = convertpars(cospars);
Gamma = exp(-2 * cospars.omega_b * cospars.h) *cospars.omega_m * cospars.h;
k_vals = logspace(-4,4,2000);
pk_vals = pkf(Gamma, cospars.sigma8, k_vals);
[dndm_un M_scale_tmp bias] = massfn_new(k_vals,pk_vals,M_vals,z_vals,cospars_slb);


... unfinished...

