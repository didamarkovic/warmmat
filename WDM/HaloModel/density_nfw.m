function rho_nfw = density_nfw(r_parll,r_perp,rho_s,c,r_v)
% DENSITY_NFW calculates NFW density for a halo
% rho_nfw = density_nfw(r_parll,r_perp,rho_s,c,r_v)
%
% 14.10.09 KMarkovic

rho_nfw = rho_s ./ ( c/r_v * sqrt(r_parll.^2+r_perp^2) .* ...
    ( 1 + c/r_v * sqrt(r_parll.^2+r_perp^2) ).^2 );