function [mod_u_k r_v] = nfw_u_k_3d(k_vals,m_val,z_val,cospars,M_scale)

% Function to calculate the 3D Fourier transform of the NFW halo density
% profile.
%
% function [mod_u_k r_v] = nfw_u_k_3d(k_vals,m_val,z_val,cospars,M_scale)
%
% mod_u_k is a 1D aray at a specified redshift and mass of halo 
%         corresponding the k_vals array. 
% r_v is the virial radius
%
% k_vals - array of wavenumbers
% m_val is the mass of the halo (in solar masses)
% z_val is the redshift
% cospars is an object produced with setpars.m for example - it can
%         contain lookup tables of cos and sin integrals called as
%         cospars.Si and cospars.Ci
% M_scale is the scale mass - where nu = 1 - see SLB's function massfn.m
%
% 18/12/2008 KMarkovic

consts

[tmp tmp tmp conc] = lensparams(cospars.Omega_m,cospars.h,M_scale,m_val,z_val);
%%%%%%%%%%%%%%%%
% From nfwshear.m by SLB:
% standard parameters
G_ = 6.673e-11; % SI units
%c_ =  299792458; % m/s
Mpc_to_m_ = 1e6 * 3.0856e16; % number of meters in 1Mpc
H_0_ = 100 * 1e3 / Mpc_to_m_; % h/s
rho_crit_ = 3 * H_0_^2 / (8 *pi * G_) /M_solar *Mpc_to_m_^3; % h^2 M_solar / Mpc^-3
% find cosmological critical density at redshift of the lens
%rho_c_zclust = rho_crit_ * (cospars.Omega_m *(1+z_val)^3 + (1-cospars.Omega_m)); 
% assumes flat universe; Units are h^2 M_solar / Mpc^-3
rho_m = rho_crit_ * cospars.Omega_m *(1+z_val)^3;
% Navarro, Frenk & White 1996, ApJ 462, 563, eqn 3:
% rho(r) = rho_s / ( (r/r_s) *  (1+r/r_s)^2 )
% where I have defined rho_s = delta_c rho_c_zclust
% where delta_c = 200/3 * c^3 / ( ln(1+c) - c/(1+c) ), which I generalise
% to
n00 = 200;
delta_c = n00/3 * conc^3 / ( log(1+conc) - conc/(1+conc) );
rho_s = delta_c * rho_m; 
% also they say M_200 = 200 rho_c_zclust * 4/3 pi r_200^3 so:
r_v = ( m_val / (n00 * rho_m * 4/3 * pi ) ).^(1/3);
% also they say r_s = r_200 / c
rs = r_v / conc;
% Am not 100% sure I generalised it right to n00~=200 ..
%theta_200 = r_200 / Dd * 60 * 180/pi; % theta_200 in arcmin
%r_s = 10/0.7
%[r_v_ rs_ rho_s_ conc_] = lensparams(cospars.Omega_m,cospars.h,M_scale,m_val,z_val);
%%%%%%%%%%%%%%%%%%%%%%%%%%

% Now the option to calculate the sin/cos integrals:
if ~isfield('Si',cospars) && ~isfield('Ci',cospars)
    x = logspace(-5,5,5000);
    [Si Ci] = sincosint(x,500); %???
else
    Si = cospars.Si; Ci = cospars.Ci;
end

Si_1 = interp1(x,Si,(1+conc)*k_vals*rs,'spline');
Ci_1 = interp1(x,Ci,(1+conc)*k_vals*rs,'spline');
Si_2 = interp1(x,Si,k_vals*rs,'spline');
Ci_2 = interp1(x,Ci,k_vals*rs,'spline');

Apar = 4*pi*rho_s*rs.^3 / m_val;
Bpar = sin(k_vals*rs).*(Si_1-Si_2) - sin(conc*k_vals*rs)/(1+conc);
Cpar = cos(k_vals*rs).*(Ci_1-Ci_2);
mod_u_k = Apar+Bpar+Cpar;

% testing
if 0
    clear; clc; figure
    M_scale = 1e13;
    k_vals = logspace(-1,4,100);
    z_val = 0.75;
    [vals nams cospars] = setWMAPpars;
    cospars = convertpars(cospars);
    for m_val = logspace(11,16,6);
        [mod_u_k r_v] = nfw_u_k_3d(k_vals,m_val,z_val,cospars,M_scale);
        loglog(k_vals,mod_u_k); hold on
    end
    axis([k_vals(1) max(k_vals) 1e-4 2])
end