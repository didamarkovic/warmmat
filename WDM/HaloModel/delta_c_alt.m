function delta_c = delta_c_alt(z_vals,Omega_m)

% This is the fitting function for delta_c from Henry 2000 (like they use
% in Cooray et al. for a flat universe.
%
% function delta_c = delta_c_alt(z_vals,Omega_m)
% [unitless]
%
% 02/02/08 KMarkovic

% to test: z_vals = 0.1:0.1:0.9;

if ~exist('Omega_m','var') Omega_m = 0.3; end
x = ((1/Omega_m)-1)^(1/3) ./ (1+z_vals);
delta_c = 3* (12*pi)^(2/3) * (1-0.0123*log(1+x.^3)) /20;

%plot(z_vals,delta_c)
% Has this got anything at all to do with the delta_c that I find in
% lensparams.m ??? NO

%%
% clear
% cospars = setEUCLIDpars;
% redshifts = 0:1:5;
% growthfcs = growth_factor(cospars,redshifts);
% delta_c_1 = 1.686./growthfcs;
% delta_c_2 = delta_c_alt(redshifts,cospars.omega_m);
% figure
% plot(redshifts,delta_c_1,'k'); hold on
% plot(redshifts,delta_c_2,'b');

