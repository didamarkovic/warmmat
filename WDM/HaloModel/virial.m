function rvir = virial(M,cospars,Del)
% Function to calculate the virial radius explicitely.
%   16.07.2013 KMarkovic

if ~exist('Del','var') Del = 200; end % Overdensity of collapsed haloes
warning('Please input halo mass in M_sol, *not* M_sol/h!');

consts;
rhobar = cospars.omega_m * rho_crit_0_Mpc;

rvir = (3*M / (4*pi*rhobar*Del)).^(1/3);

return