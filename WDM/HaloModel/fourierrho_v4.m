function rho_kMZ = fourierrho(z_vals, M_vals, k_vals, r_v, conc, rho_s, cospars)
%FOURIERKAPPA(zvals, dlvals, Mvals, lvals, theta_v, r_v,conc, rho_s, SIGMAcrit, no_theta)
%   This function finds the Fourier transform Kappa_L of the lensing shear
%   and returns a 3D array with redshifts in x-direction, masses in
%   y-direction and l in the z-direction.
%
%   It uses the embedded function nfwkappa.m.
%
%   The default value of no_theta is 2^9. This is the number of steps
%   required in the integral. Note: need to calculate a lot of parameters
%   before running this. Suggested script to run before lensparams.m
%
%   The output is unitless and independent of units as long as input is
%   consistent!
%
%   09.10.09 KMarkovic
%   version 3.3 - implementing romberg
%   09.10.09 KMarkovic
%   version 3.2 - deleted the quad stuff
%
%   31.03.2010 KM
%   version 4 - rewriting in 3D



% Now the option to calculate the sin/cos integrals:
if ~isfield('Si',cospars) && ~isfield('Ci',cospars)
    %x = logspace(-10,5,50000);
    %warning off
    %[Si Ci] = sincosint(x); %???
    %warning on
    struct = load('SiCi.mat');
    x = struct.x; Si = struct.Si; Ci = struct.Ci;
    clear struct
else
    Si = cospars.Si; Ci = cospars.Ci; x = cospars.x;
end % clf; plot(x, Si,'k', x, Ci, 'b')

rho_kMZ = zeros(length(M_vals),length(z_vals),length(k_vals));
for mint = 1:length(M_vals)
    for zint = 1:length(z_vals)
        rs(mint,zint) = r_v(mint,zint)./conc(mint,zint);
        
        nu = k_vals*rs(mint,zint);
        
        Si_1 = interp1(x,Si,(1+conc(mint,zint))*nu,'spline');
        Ci_1 = interp1(x,Ci,(1+conc(mint,zint))*nu,'spline');
        Si_2 = interp1(x,Si,nu,'spline');
        Ci_2 = interp1(x,Ci,nu,'spline');
        
        % plot(nu,Si_2,'k',nu,Ci_2,'b'); hold on
        % plot(x,Si,'g--',x,Ci,'c--'); axis([nu(1) max(x) -20 5])
        
        %Apar = 4*pi*rho_s*rs.^3 / m_val;
        %Bpar = sin(k_vals*rs).*(Si_1-Si_2) - sin(conc*k_vals*rs)/(1+conc);
        Bpar = sin(nu).*(Si_1-Si_2) - sin(conc(mint,zint)*nu)/(1+conc(mint,zint))./nu;
        Cpar = cos(nu).*(Ci_1-Ci_2);
        %mod_u_k = Apar+Bpar+Cpar;
        f = (log(1+conc(mint,zint)) - conc(mint,zint)./(1+conc(mint,zint))).^(-1);
        rho_kMZ(mint,zint,:) = M_vals(mint)*f*(Bpar+Cpar);
    end
end



%%
% rho_kMZ = zeros(length(Mvals),length(zvals),length(kvals));
% rv = zeros(length(Mvals),length(zvals));
%
% for mint = 1:length(Mvals)
%     for zint = 1:length(zvals)
%
%         % THis is too crap, do it again!
%         [rho_kMZ(mint,zint,:) rv(mint,zint)] = ...
%             nfw_u_k_3d(kvals,Mvals(mint),zvals(zint),cospars,M_scale);
%
%     end
% end


%%
% % Step in theta needed for different z and M corresponding to theta_v:
% if ~exist('no_theta','var')
%
%     no_theta = 2^10; % = 512
%
% end
%
% %dtheta = theta_v ./ no_theta;
% dr = r_v ./ no_theta;
%
%
% % All the posible theta vals over which we must integrate the shear for
% % all z and M:
% r_vals = zeros(length(Mvals),length(zvals),no_theta+1);
%
% for zint = 1:length(zvals)
%
%     for mint = 1:length(Mvals)
%
%
%         %theta_vals(mint,zint,:) = 0:dtheta(mint,zint):theta_v(mint,zint);
%         r_vals(mint,zint,:) = 0:dr(zint):r_v(zint);
%
%     end
%
% end
%
%
%
% %%%%%%%%%%%%%%%%%%
% % Calculations kappa:
% %%%%%%%%%%%%%%%%%%
%
% % Now must do the lensing shear (kappa) integral:
% rho = zeros(length(Mvals), length(zvals), size(r_vals,3));
% for zint = 1:length(zvals)
%
%     for mint = 1:length(Mvals)
%
%         if 1
%
%             rho(mint,zint,:) = nfwrho(theta_vals(mint,zint,:),conc(mint,zint),...
%                 theta_v(mint,zint),r_v(mint,zint),rho_s(mint,zint),SIGMAcrit(zint)); %[unitless]
%
%             %kappa(mint,zint,:) = kappa(mint,zint,:)  / (1+zvals(zint))^3;
%
%         else
%
%             warning('using SIS lens!!!!')
%
%             SIGMA = Mvals(mint)/ (2*pi* r_v(mint,zint) ) ...
%                 / ( dlvals * theta_vals(mint,zint,:) );
%             SIGMA(isinf(SIGMA)) = 0;
%             kappa(mint,zint,:) = SIGMA./SIGMAcrit(zint);
%
%             clear SIGMA
%
%         end
%
%         %figure
%         %semilogy(squeeze(theta_vals),squeeze(abs(theta_vals)).*squeeze(kappa),'r'); hold on
%         %semilogy(squeeze(theta_vals),squeeze(kappa),'r');
%         %semilogy(squeeze(theta_vals),ones(size(squeeze(theta_vals))).*...
%         %    sum(theta_vals.*kappa*dtheta*dlvals^2*2*pi),'b--')
%         %semilogy(squeeze(theta_vals),ones(size(squeeze(theta_vals))).*Mvals,'rx')
%         %semilogy([-theta_v theta_v],[Mvals Mvals],'rx')
%         % all integrate up to the total halo mass!
%
%
%     end
%
% end
% %fprintf(1,'\nWARNING: Random a^3 factor in fourierkappa: line 106!\n')
%
% %%%%%%%%%%%%%%%%%
% % Fourier transforming kappa:
% %%%%%%%%%%%%%%%%%
% % Using realy simple integration inspired by 'lens_cls_2_xi.m':
%
% dK_l = zeros(length(Mvals),length(zvals),size(theta_vals,3));
% K_lMZ = zeros(length(Mvals),length(zvals),length(lvals));
%
% for lint = 1:length(lvals)
%
%     J_0_val_mat =  besselj(0,(lvals(lint)+0.5)*theta_vals);
%     %J_0_val_mat = besselj(0,(lvals(lint))*theta_vals); % nodiff 30.03.2010 KM
%
%     for tint = 1:size(theta_vals,3)
%
%         theta = theta_vals(:,:,tint);
%
%         dK_l(:,:,tint) = theta.*J_0_val_mat(:,:,tint).*kappa(:,:,tint);
%         %dK_l(:,:,tint) = theta.*J_0_val_mat(:,:,tint).*kappa(:,:,tint);
%
%     end
%
%     K_lMZ(:,:,lint) = dtheta.*sum(dK_l,3)*2*pi; %[unitless]
%     %K_lMZ(:,:,lint) = rombergint(dK_l,theta_vals)*2*pi; %[unitless] % 02.03.2010 KM
%
% end
% %plot(squeeze(theta_vals(50,1,:)),squeeze(dK_l(50,1,:)),'k'); hold on;
% %title('integrand of \kappa_l for M = 10^1^6 M_o and for z = 0 (see fourerkappa.m)')
% %xlabel('\theta'); ylabel('d\kappa_l')
% %plot(squeeze(theta_vals(490,10,:)),squeeze(dK_l(490,10,:)),'r--'); hold on;
% %clf; figure