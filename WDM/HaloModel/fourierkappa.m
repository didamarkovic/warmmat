function K_lMZ = fourierkappa(zvals, dlvals, Mvals, lvals, theta_v, r_v,...
    conc, rho_s, SIGMAcrit, no_theta)
%FOURIERKAPPA(zvals, dlvals, Mvals, lvals, theta_v, r_v,conc, rho_s, SIGMAcrit, no_theta)
%   This function finds the Fourier transform Kappa_L of the lensing shear
%   and returns a 3D array with redshifts in x-direction, masses in
%   y-direction and l in the z-direction.
%
%   It uses the embedded function nfwkappa.m.
%
%   The default value of no_theta is 2^9. This is the number of steps
%   required in the integral. Note: need to calculate a lot of parameters
%   before running this. Suggested script to run before lensparams.m
%
%   The output is unitless and independent of units as long as input is
%   consistent!
%
%   09.10.09 KMarkovic
%   version 3.3 - implementing romberg
%   09.10.09 KMarkovic
%   version 3.2 - deleted the quad stuff


% Step in theta needed for different z and M corresponding to theta_v:
if ~exist('no_theta','var')
    
    no_theta = 2^10; % = 512
    
end

dtheta = theta_v ./ no_theta;


% All the posible theta vals over which we must integrate the shear for
% all z and M:
theta_vals = zeros(length(Mvals),length(zvals),no_theta+1);

for zint = 1:length(zvals)
    
    for mint = 1:length(Mvals)
        
        
        theta_vals(mint,zint,:) = 0:dtheta(mint,zint):theta_v(mint,zint);
        
    end
    
end



%%%%%%%%%%%%%%%%%%
% Calculations kappa:
%%%%%%%%%%%%%%%%%%

% Now must do the lensing shear (kappa) integral:
kappa = zeros(length(Mvals), length(zvals), size(theta_vals,3));
cylindrical = 0;
for zint = 1:length(zvals)
    
    for mint = 1:length(Mvals)
        
        if 1
            
            kappa(mint,zint,:) = nfwkappa(theta_vals(mint,zint,:),conc(mint,zint),...
                theta_v(mint,zint),r_v(mint,zint),rho_s(mint,zint),SIGMAcrit(zint)); %[unitless]
            
            %kappa(mint,zint,:) = kappa(mint,zint,:)  / (1+zvals(zint))^3;
            
        elseif 0
            
            warning('using cylindrical lens!!!!')
            
            SIGMA(mint,zint,:) = Mvals(mint)/ (pi* r_v(mint,zint)^2 ) * ones(size(theta_vals(mint,zint,:)));
            kappa(mint,zint,:) = SIGMA(mint,zint,:)./SIGMAcrit(zint);
            
            cylindrical = 1;
            
        else
            
            warning('using SIS lens!!!!')
            
            warning off
            SIGMA = Mvals(mint)/ (2*pi* r_v(mint,zint) ) ...
                / ( dlvals * theta_vals(mint,zint,:) );
            warning on
            SIGMA(isinf(SIGMA)) = 0;
            kappa(mint,zint,:) = SIGMA./SIGMAcrit(zint);
            
            clear SIGMA
            
        end
        
        %figure
        %semilogy(squeeze(theta_vals),squeeze(abs(theta_vals)).*squeeze(kappa),'r'); hold on
        %semilogy(squeeze(theta_vals),squeeze(kappa),'r');
        %semilogy(squeeze(theta_vals),ones(size(squeeze(theta_vals))).*sum(theta_vals.*kappa*dtheta*dlvals^2*2*pi),'b-.')
        %semilogy(squeeze(theta_vals),ones(size(squeeze(theta_vals))).*Mvals,'rx')
        %semilogy([-theta_v theta_v],[Mvals Mvals],'ro'); hold on
        % all integrate up to the total halo mass!
        
        
    end
    
end
%fprintf(1,'\nWARNING: Random a^3 factor in fourierkappa: line 106!\n')

%%%%%%%%%%%%%%%%%
% Fourier transforming kappa:
%%%%%%%%%%%%%%%%%
% Using realy simple integration inspired by 'lens_cls_2_xi.m':

dK_l = zeros(length(Mvals),length(zvals),size(theta_vals,3));
K_lMZ = zeros(length(Mvals),length(zvals),length(lvals));

for lint = 1:length(lvals)
    
    J_0_val_mat =  besselj(0,(lvals(lint)+0.5)*theta_vals);
    %J_0_val_mat = besselj(0,(lvals(lint))*theta_vals); % nodiff 30.03.2010 KM
    
    for tint = 1:size(theta_vals,3)
        
        theta = theta_vals(:,:,tint);
        
        dK_l(:,:,tint) = theta.*J_0_val_mat(:,:,tint).*kappa(:,:,tint);
        %dK_l(:,:,tint) = theta.*J_0_val_mat(:,:,tint).*kappa(:,:,tint);
        
    end
    
    K_lMZ(:,:,lint) = dtheta.*sum(dK_l,3)*2*pi; %[unitless]
    %K_lMZ(:,:,lint) = rombergint(dK_l,theta_vals)*2*pi; %[unitless] % 02.03.2010 KM
    
    if cylindrical
        
        K_lMZ_analytical(:,:,lint) = 2*pi*kappa(:,:,tint).*theta_v/(lvals(lint)+0.5) .* besselj(1,(lvals(lint)+0.5)*theta_v);
        
    end
    
end

if cylindrical 
    
    bob=sum(sum(abs(K_lMZ - K_lMZ_analytical))); 

    figure
    mint = 1;
    loglog(lvals,squeeze(K_lMZ(mint,:,:)),'b'); hold on
    loglog(lvals,squeeze(K_lMZ_analytical(mint,:,:)),'g--')
    %axis([lvals(1) max(lvals) 10^(-8) 10^(-7)])
    % THIS IS EXACTLY THE SAME!
    
end
%plot(squeeze(theta_vals(50,1,:)),squeeze(dK_l(50,1,:)),'k'); hold on;
%title('integrand of \kappa_l for M = 10^1^6 M_o and for z = 0 (see fourerkappa.m)')
%xlabel('\theta'); ylabel('d\kappa_l')
%plot(squeeze(theta_vals(490,10,:)),squeeze(dK_l(490,10,:)),'r--'); hold on;
%clf; figure