function cvir = concentration(z,zc,K)
% Schneider et al. 2012 simulation fit:
%   16.07.2013 KMarkovic

if ~exist('K','var') K = 3.4; end

cvir = K*(zc+1)*(z+1);

return