function [M_scale sigma nu] = mascale(cospars,z_vals,k_vals,pk_lin,fromscratchbool)
% 
% This function finds the non-linear mass scale from the power spectrum.
%
% function M_s = mascale(cospars,z_vals,k_vals,pk_lin,fromscratchbool)
% ouput units [M_solar]
%
% pk_lin should supposedly be TODAY!! - 19.11.09 - YES!!! 05.03.2010
% This is because we multiply the sigma with the growth factor below!
%
% Mass scale increases with redshift (at ~0 M_scale ~10^13, at ~1 M_scale
% ~10^12...).
%
% The below statement is a lie. My code was different (and looked more
% wrong) because I was using delta_c for a flat universe and Sarah is uing
% the closed one (this doesn't make so much difference), also she has a
% factor of sqrt(a), which means she's using nu' and not nu, I think. Also
% she divided her delta_c/growth_factor, which made the big difference. I'm
% not sure why.
% version 6
% 23.02.2010 KM
% In this version I've made the "from scratch" part default, because with
% Sarah's code I was getting M_s = 10^7 ish at z = 2, so I used mine, where 
% M_s = 10^11 ish at z = 2. But I'm not sure which is right. For today I
% got 10^13 ish, Sarah got 10^10 ish.
% version 4
% 02.02.2010 KM
% version 3
% 19.11.09 KMarkovic

if ~exist('fromscratchbool','var') fromscratchbool = 1; end

if fromscratchbool % This seems to be bad!!! 19.11.09
    
    warning('Calculating M_s from scratch in mascale.m!')

    M_vals = logspace(5,16,100);
    
    % First find the radius of region:
    
    consts;
    rho_mean_0 = rho_crit_0_Mpc * cospars.omega_m; % M_sol/Mpc^3
    rho_mean = rho_crit_0_Mpc * cospars.omega_m * (1+z_vals).^3; % M_sol/Mpc^3

    % From Henry (2000)
    %x = ( ( 1 / cospars.omega_m ) - 1 )^(1/3) ./ ( 1 + z_vals );
    %n00 = 18 * pi^2 * ( 1 + 0.4093 * x.^2.71572 );

    R_vals = zeros(length(M_vals),length(z_vals));
    for zint = 1:length(z_vals)

        %R_vals(:,zint) = (3* M_vals/ ( 4*pi.*n00(zint)*rho_mean_0 )) .^(1/3);
        %R_vals(:,zint) = (3* M_vals/ ( 4*pi.*rho_mean(zint) )) .^(1/3); % This is how it is defined, apparently!
        R_vals(:,zint) = (3* M_vals/ ( 4*pi.*rho_mean_0 )) .^(1/3); % This is how it is defined, apparently!
        % Mpc
        % The reason why you need COMOVING R is because k is comoving!

    end


    % Now find sigma_R & then nu(R):

	growth_vals = growth_factor(cospars,z_vals);
    %[delta_vals_tmp, z_vals_tmp] = growth_wconst(cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    %growth_vals_tmp = delta_vals_tmp ./ ( max(delta_vals_tmp) );
    %growth_vals = interp1(z_vals_tmp, growth_vals_tmp, z_vals);
	%if sum(isnan(growth_vals))>0 error('Growth is not being calculated!'); end

    delta_c = delta_c_alt(z_vals,cospars.omega_m);

    sigma = zeros(length(M_vals),length(z_vals));
    nu = sigma;
    M_scale = zeros(length(z_vals),1);
    for zint = 1:length(z_vals)
        
        for rint = 1:length(M_vals)

            %sigma(rint,zint) = sigmar_frompk(k_vals,pk_lin',R_vals(rint,zint)*cospars.h);
            sigma(rint,zint) = sigmar_frompk(k_vals,pk_lin,R_vals(rint,zint)*cospars.h);

            %nu(rint,zint) = delta_c(zint) ./ sigma(rint,zint).^2 ; 
            % I'm pretty sure the above was wrong 02.02.2010 KM
            %nu(rint,zint) = delta_c(zint) ./ sigma(rint,zint);
            nu(rint,zint) = delta_c(zint) ./ sigma(rint,zint)./growth_vals(zint); 

        end

        
        % Now interpolate to find nonlinear mass scale:
        M_scale(zint) = interp1(nu(:,zint),M_vals,1);
        
    end

else
    % OLD CODE:
    % In massfn.m Matlab must find the M value at 'nu' = 1 and if we dont
    % include M values high enough this nu = 1 is not included and then
    % M_scale at that redsift is NaN. So, should really precalculate M_scale
    % with a wide range of M:
    %tmpM = logspace(0,17,100)*cospars.h;
    tmpM = logspace(0,17,100)*cospars.h;

    % Use Sarah Bridle's function to find the scale mass.
    [tmpout1 tmpout2 M_scale] = massfns_slb(tmpM,z_vals,...
        cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
        cospars.sigma8,k_vals,pk_lin);

    M_scale = M_scale / cospars.h; % M_scale from massfns is in units of h^-1
end