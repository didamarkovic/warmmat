function dndm = massfn_wdm(Mvals,dndm,Mfs,smooth)
% MASSFN_WDM(Mvals,dndm,Mfs,smooth)
%
%   Mvals - halo masses at which we have the mass function
%   dndm - mass functions
%   Mfs - free-straming mass for the WDM model
%   smoot - for the smoothing of the step in the error function
%
%   This function multiplies given mass functions by a step to simulate the
%   effect of Warm Dark Matter (equation 45 in arXiv:1103.2134).
%
%   version 1
%   18.05.2011 KMarkovic
%   OBSOLETE!!!!!!

warning('This function is obsolete! Use massfns_wrap!');

if ~exist('smooth','var') smooth = 0.5; end

warning('off','MATLAB:divideByZero')
factor = 0.5 * ( 1 + erf( log10(Mvals/Mfs)/smooth ) );
warning('on','MATLAB:divideByZero')

dndm = dndm .* factor';