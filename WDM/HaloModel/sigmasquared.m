function [sigma dlogs2dlogm] = sigmasquared(k_vals,pk_vals,R_vals)
% DLOGSIG2(k_vals,pk_linear,sigmas) is a function that calculates the
%   derivative dlogsigma^2/dlogM from equations 41-44 in Smith & Markovic
%   2011 (arXiv:1103.2134).
%
%   04.06.2011 KMarkovic
%   version 1

k_vals = horizontal(k_vals);
pk_vals = horizontal(pk_vals);
R_vals = horizontal(R_vals);

dlogk = diff(log10(k_vals));
warning off
if sum( dlogk(1:end/2)-dlogk(1:end/2) ) > 1e-10
    error('Log(k_vals) must be equally spaced!')
else
    dlogk = dlogk(1);
end
warning on

x = R_vals'*k_vals; % get a matrix: size(x)

window = 3./x.^3 .* ( sin(x) - x.*cos(x) ); % size(window)
windowdash = 3./x.^3 .* ( (x.^2 - 3).*sin(x) + 3*x.*cos(x) );

sigma = sqrt(  (k_vals.^3 .* pk_vals) * (window'.^2) *dlogk /  (2*pi^2)  );  %  size(pk_vals')
dlogs2dlogm = (2./(3*sigma.^2)) .* ((k_vals.^3 .* pk_vals) * (window'.* windowdash')) ...
    * dlogk/ (2*pi^2);

%return
%% Test
nok = length(k_vals);
logstep  =  log(k_vals(2:nok))  -  log(k_vals(1:(nok-1))); % dlogk = dk/k

sigma2 = zeros(size(sigma));
sigma3 = sigma2;
for rint = 1:length(R_vals)
    sigma2(rint) = sigmar_frompk(k_vals,pk_vals,R_vals(rint));

    %sigma3(rint) = sqrt(  sum((k_vals.^3 .* pk_vals) .* (window(rint,:).^2) *dlogk /  (2*pi^2))  );

    %tmp = pk_vals .* window(rint,:).^2 .* k_vals.^2 /2 /pi^2.*dlogk.*k_vals; % integrand
    %sigma3(rint) = sqrt(sum(tmp(1:end-1))); % integrate

    x = k_vals(1:(nok-1))*R_vals(rint); % dimensionless
    w  =  3*(sin(x)-x.*cos(x)) ./ x.^3; % window function

    tmp = pk_vals(1:(nok-1)) .* w.^2 .* k_vals(1:(nok-1)).^2 /2 /pi^2; % integrand

    sigmar = sqrt(sum(tmp.*logstep.*k_vals(1:(nok-1)))); % integrate

end

clf
loglog(R_vals,sigma2,'y.'); hold on
loglog(R_vals,sigma3,'bd'); hold on
loglog(R_vals,sigma,'mx');

%% Try to see analytical

syms a b c d e f g h i j 

mata = [a, b, c; d, e, f]; matb = [g, h, i];

matb*mata'

a = 5; conj(a)
