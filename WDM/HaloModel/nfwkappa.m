function kappa = nfwkappa(theta,conc,theta_vir,r_vir,rho_s,SIGMAcrit)
% NFWKAPPA(theta, conc, theta_vir, r_vir, rho_s, SIGMAcrit)
%
% Finds kappa parameter for an NFW profile. Note: kappa is dimensionless.
% It involves an analytic integral to get the surface density profile.
%
% THETA is the distance from mass centre in radians
% CONC, RHO_S and SIGMACRIT are all obtainable by using either the
% 'nfwparamsfn.m' or the 'lensparams.m' code.
% THETA_VIR is the angular virial radius
% R_VIR is the actual virial radius
%
% This used to be from Wright&Brainerd but this is wrong! Now using
% an equivalent to equation 26 from Takada & Jain 0209167 worked out in
% Mathematica. Based on SLB's code.
%
% Analytical integral from Takada & Jain (2003) - astro-ph/0209167, eqn (27).
% version 4.0
% 02.02.2010 KM
% For this version I calculated the analytical integral more accurately and
% deleted the old remnants that work less well (i.e. Wright & Brainerd and
% Takada & Jain versions). But note that this is equivalent to (and somewhat
% more messy than) Takada & Jain (2003) - astro-ph/0209167.
% version 3.0
% 22.01.2010 KMarkovic

rs = r_vir/conc;

%x = squeeze(single(theta*conc./theta_vir))';
%x = squeeze(theta*conc./theta_vir)'; % 25.02.2010 KM
x = squeeze(theta*conc./theta_vir)'; % 30.03.2010 KM - playing around...


% x<1
i1 = (x<1)&(x~=0);% sum(i1)
%i1 = x<1;% sum(i1)
SIGMA(i1) = -2*rs*rho_s./( (1+conc).*(1-x(i1).^2).^(3/2) ) .* ...
    (sqrt( (conc^2-x(i1).^2).*(1 - x(i1).^2) ) - (1+conc)*acosh( (x(i1).^2 + conc)./(x(i1).*(1+conc) ) ));

% x == 1
%i2 = (theta*conc)/theta_vir==1; % sum(i2)
i2 = x==1; % sum(i2)
SIGMA(i2) = 2.*rs.*rho_s.*(conc+2).*sqrt(conc-1)./(3*(conc+1).^(3/2));

% x >1
i3 = x>1; % sum(i3)
SIGMA(i3) = 2*rs*rho_s./( (1+conc)*(-1+x(i3).^2).^(3/2) ) .*...
    (sqrt( (conc^2-x(i3).^2).*(x(i3).^2-1) ) - (1+conc)*acos( (x(i3).^2 + conc)./(x(i3).*(1+conc) ) ));

% x == c
i4 = theta==theta_vir;
SIGMA(i4) = 0;

% x == 0 % Put this at the end so it overwrites i1!
i0 = x==0; 
SIGMA(i0) = 0; % Ignore this due to divergence that is unimportant once Fourier integrated.


kappa = SIGMA./SIGMAcrit;