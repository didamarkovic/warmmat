function [rv_vals theta_v conc rho_s n00] = ...
                         haloparams(omega_m_0,h,M_scale,M_vals,zl_vals,dl)
%
% This function returns all the parameters needed to do a convergence
% calculation and then finds the NFW density profile. 
%
% function [rv_vals theta_v conc rho_s] = ...
%                         haloparams(omega_m,h,M_scale,M_vals,z_vals,dl)
%
% M_scale - cut off in power spectrum between power law and exp
% M_vals - halo masses
% z_vals - redshift of lens halo - can be a vector
% dl - distance to the lens halo
%
% This works for flat universe only!
%
% This is from the papers Schneider; Henry 2000 & Cooray et al. 2000
% Also see SLB's nfwshear.m where this is adapted from.
%
% This code uses one external function: Dist.m by SLB. - DELETING THIS!!!
%
% version 3.0
% 08.10.09 KMarkovic
%
% Just a comment: all of this code would be much easier if it was more
% object oriented i.e. treat halos as objects and they have properties
% like: virial radius, viriat angular radius, dencity, concentration, mass
% etc. then put them into an array, tag them by mass and redshift.
% Everything else is dependant on M & z. So, ideally the output of this
% function would be an object "halo" and you would then have
% halo.rv, halo.thetav, halo.conc, halo.rs, halo.rhos etc. 
% Possibly it would be easier, since this is Matlab to have an object
% "halos", where you would have elements: halos.rv_vals, halos.theta_v_vals
% etc., where these are matrices varying in redshift/mass along
% rows/colums.
%
% version 3.1
% Added the possibility to omit theta_v and dl, if don't need for lensing.
% 18.05.11 KMarkovic

consts % Initialize the constants with this script.
rho_mean_Mpc_0 = rho_crit_0_Mpc * omega_m_0; %%%[M_sol/Mpc^3]


%rho_mean_0 = rho_crit_0 * omega_m_0; % kg/m^3


% Overdensity from Henry (2000)
x = ( ( 1 / omega_m_0 ) - 1 )^(1/3) ./ ( 1 + zl_vals ); % no units
% N.B. this, supposedly is only for LCDM! 01.02.2010 KM

%for mint = 1:length(M_vals)
    
    %n00(mint,:) = 18 * pi^2 * ( 1 + 0.4093 * x.^2.71572 ); % no units
    %n00 = 18 * pi^2 * ( 1 + 0.4093 * x.^2.71572 ); % no units
    n00 = 180*ones(size(zl_vals));
    % BUG!!!!!!!!! 17.11.09 KM
    
%end


% Concentration parameter
conc = zeros(length(M_vals),length(zl_vals));

a = 10.3 .* ( ( 1 + zl_vals ).^(-0.3) ); % no units
%a = 6*ones(size(zl_vals)); % no units

b = -0.24 .* ( ( 1 + zl_vals ).^(-0.3) ); % no units
%b = -0.1*ones(size(zl_vals)); 

for zint = 1:length(zl_vals)
    
    %conc(:,zint) = min(a(zint) .* ( M_vals ./ M_scale(zint) ).^b(zint) ,1e20);    
    conc(:,zint) = ( a(zint) .* ( M_vals ./ M_scale(zint) ).^b(zint) )' ; % no units
    %conc(:,zint) = ( a(zint)  .* ( M_vals ./ M_scale(zint) ).^(b(zint)/3.5)  )' ; % no units

    %%% WDM form Schneider et al 2012:
    Mhm = halfmodemass(cospars);
    conc (:,zint) = conc(:,zint) .* (1 + gamma1 * Mhm./M_vals).^(-gamma2);

end
% now conc is a matrix with the rows with different redshifts and columns 
% with different mass i.e. a column will have same redshift but increasing 
% mass downwards.



% Virial radius = rv = r_n00 is the radius containig mean mass density: 
% rho_mean = n00*rho_crit. - wrong! Its rho_mean_of_cluster = n00 * rho_backgrd!
rv_vals = zeros(length(M_vals),length(zl_vals));
for zint = 1:length(zl_vals)
    
    %rv_vals(:,zint) = (3* M_vals'*M_solar./ ( 4*pi.*n00(:,zint)*rho_mean_Mpc_0*(1+zl_vals(zint))^3 )) .^(1/3);
    rv_vals(:,zint) = (3* M_vals'./ ( 4*pi.*n00(:,zint)*rho_mean_Mpc_0*(1+zl_vals(zint))^3 )) .^(1/3);
    % UP OR DOWN?
    %rv_vals(:,zint) = (3* M_vals'*M_solar./ ( 4*pi.*n00(zint)*rho_mean_0 )) .^(1/3) ;
    %rv_vals(:,zint) = (3* M_vals'*M_solar./ ( 4*pi.*n00(zint)*rho_crit_0 )) .^(1/3) ;

end
%warning('Random a^3 factor!\n')
% Using rho_mean_0 here is what they say in Cooray et al. (2000) and in
% Henry (2000) they say nothing.
% Also I think that the redshift used here should be the redshift at which
% the object collapsed, not the redshift at which the object is observed!



% Now find rho_s: equate both equations for M in Cooray around eq. (2) and
% express rho_s!
rho_s = zeros(size(conc));

tmp = conc.^3 ./ ( log(1+conc) - conc./(1+conc) ) ;

for zint = 1:length(zl_vals) 
    
    %rho_s(:,zint) = n00(zint) * omega_m_0 * rho_mean_0 / 3 * tmp(:,zint);
    %rho_s(:,zint) = n00(zint)  * rho_mean_0 / 3 * tmp(:,zint);
    %rho_s(:,zint) = n00(zint)  * rho_mean_0 / (1+zl_vals(zint))^3/ 3 * tmp(:,zint);
    rho_s(:,zint) = n00(zint)  * rho_mean_Mpc_0 * (1+zl_vals(zint))^3/ 3 * tmp(:,zint);
    % Had a bug here! Fixed 10.11.09 KM!!!
    
end
%warning('Random a^3 factor!\n')


if ~exist('dl','var') 
    theta_v = pi;
    return; 
end

% rv converted to position on the sky:
% if ~exist('dl')
% 
%     dl = Mpc * Dist(zl_vals,0,H0_Mpc,omega_m_0)';% factor of h????
% 
%     dl(dl==0) = Mpc/10^6;
%     % Just to avoid zeros and infinities (this is inside the Milky Way).
% 
% end

theta_v = zeros(length(M_vals),length(zl_vals));

for zint = 1:length(zl_vals)

    theta_v(:,zint) = rv_vals(:,zint) ./ dl(zint) ; % in radians

end



% The following code may be activated if the function needs to be checked:

% %Do the other check Anze Slosar suggested: integrate over r:
% %figure
%  rs = rv_vals./conc;
%  for zint = 1:length(zl_vals)
%      for mint = 1:length(M_vals)
%          dr = (rv_vals(mint,zint)/10000);
%           r_vals(mint,zint,:) = dr:dr:(rv_vals(mint,zint));
%          % Now calculate the dM:
%          dM_vals(mint,zint,:) = dr*4*pi*rho_s(mint,zint)*r_vals(mint,zint,:).^2./ ...
%              ( (r_vals(mint,zint,:)/rs(mint,zint)) .* (1 + (r_vals(mint,zint,:)/rs(mint,zint))).^2 );
%     end
%      M_ratio_1(:,zint) = sum(dM_vals(:,zint,:),3)'/M_solar./M_vals;
%  
%      % Do this analytically:
%      tmp = (  log(1+conc(:,zint))  -  conc(:,zint)./(1+conc(:,zint))  );
%      M_integral(:,zint) = 4*pi*rho_s(:,zint).*rs(:,zint).^3.*tmp;    
%      M_ratio_2(:,zint) = M_integral (:,zint)'/M_solar./M_vals;  % bob
%  end
%  % Now integrate and do ratio:
%  %semilogx(M_vals',M_ratio(:,[1 10 25 30]))
%  semilogx(M_vals',M_ratio_1,'b'); hold on
%  semilogx(M_vals',M_ratio_2,'m--'),
%  title('integral over \rho_N_F_W // M at four different z')
%  xlabel('M[M_s_o_l_a_r]');ylabel('ratio')
%  axis([M_vals(1) max(M_vals) 0.99 1.01])
% %saveas(gcf,'int_rhonfw_vs_M...','bmp')
% % Okay there are problems on very small and very large scales. Small scales:
% % because the conc becomes in need of a lot of integrating steps?
%  
% % Do the check Anze suggested:
% for zint = 1:length(zl_vals)
%     rho_s_check(:,zint) = M_vals * M_solar/4/pi./(rs(:,zint)'.^3) .* ...
%       (log(1+conc(:,zint)')-(conc(:,zint)'./(1+conc(:,zint)'))).^(-1);
% end
% for int = 1:numel(rho_s_check)
%     img(int) = ~isreal(rho_s_check(int));
% end
% rho_s_check(img) = 0;
% ratio = sum(sum(rho_s_check./rho_s))/numel(rho_s);
% if (ratio-1)>1e-14
%    fprintf(1,'PROBLEM WITH RHO_s IN LENSPARAMS!!!\n');
%     rho_s = rho_s_check; % This makes no difference!!!
% end
% %sum(sum(tmp<0)); sum(img)