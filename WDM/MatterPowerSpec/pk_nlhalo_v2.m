function [pk_nlin pk_lin tmp_pk_C tmp_pk_P] = pk_nlhalo(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool)%,noth);
% PK_NLHALO(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool,noth)
%
% k_vals - logarithmic vector of multipoles
% pk_lin_0 - linear, present day matter power spectrum
% cospars - structure containing cosmology (use e.g. setWMAPpars.m)
% z_vals - vector of redshif values
% M_vals - halo masses present in universe
% normbool - 0 for no normalization, 1 for yes, 3 for weird way
% noth - number of steps in theta in the fourier transform of 2D NFW profile
%
% This function calculates the non-linear matter power spectra at redshift
% z_val and wavenumbers k_vals, where we integrate over M_vals in the HALO
% MODEL. It is calculated in 3D, see e.g. Seljak 0001493.
% parent codes: lens_methods_test.m & lenshalo_tomo_updating.m
%
% version 2
% 31.03.2010 KMarkovic

%% Set default parameters: clf; figure;
if exist('rho_s','var') clear; fprintf('Restarting calculations.\n'); end
if ~exist('cospars','var') cospars = setPlanckpars; end
if ~exist('M_vals','var') M_vals = logspace(5,16,50); end
if ~exist('z_vals','var') z_vals = 0; end
if ~exist('k_vals','var') k_vals = logspace(-3,2,1000); end
if ~exist('normbool','var') normbool = 0; end% Mass function normalisation
%if ~exist('noth','var') noth = 512; end

tic

% Growth factor:
D_plus = growth_factor(cospars,z_vals);

% Linear matter power spectrum:
if ~exist('pk_lin_0','var')
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
elseif isnan(pk_lin_0) % added 22.12.2010 KM
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
end
pk_lin = pk_lin_0 * D_plus.^2; % Grow spectrum


% Mass functions: PEACOCK & DODDS!!!!!!!!!!!!!!!!
[dndm_un tmp M_scale_tmp sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_0); % Pk today or at z??
M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0,1); %%% [M_solar]
%M_scale = M_scale_tmp;


% Distances to halos:
clear consts; consts
%Dh = c_km/H0_Mpc; % in Mpc
%chis = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
%chi = chis * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;


% Halo model parameters:
[rv_vals theta_v conc rho_s n00] = haloparams(cospars,M_scale,M_vals,z_vals);%,chi);%%% [Mpc][no units][no units][M_sol/Mpc^3]
%cospars.h,M_scale,M_vals,z_vals,chi*Mpc);%%% [meters][no units][no units][kg/m^3]
%rv_vals = rv_vals/Mpc;
%rho_s = rho_s/M_solar*Mpc^3;


% For integration:
dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]


% Critical overdensity of collapse
delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units] % NOT THIS


% CHECK UP TO HERE, FIX and CONTINUE... (05.03.2010 KM)
%%


% Normalisation, bias and mass integral:
a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]


% First Sigma_l
%rhoFT = fourierkappa(z_vals, chi, M_vals, l_vals,theta_v,rv_vals, conc, rho_s, ONE,noth);
%rhoFT = fourierrho(z_vals, M_vals, k_vals, rv_vals, conc, rho_s, cospars);
rhoFT = fourierrho(z_vals, M_vals, k_vals, M_scale, cospars); % 06.06.2011 KM

% Need to normalise the mass functions - units: (Mpc/h)^-3/(M_solar/h)
rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m; %%%[M_sol/Mpc^3]

if normbool % Standard normalisation
    norm = sum( dndm_un' .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
    %dndm = dndm_un / norm /(1+z_vals)^3 /cospars.h^(-4) ; %%%[h^4 Mpc^-3 M_solar^-1]
    dndm = dndm_un / norm; %%%[h^4 Mpc^-3 M_solar^-1]

elseif normbool==3 % Add missing mass into small halos
    norm = sum( dndm_un' .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
    Delta = 1 - norm;
    F = rho_mean_Mpc_0 * Delta / M_vals(1) /dM(1); % ****
    dndm_add = zeros(size(dndm_un));
    dndm_add(1) = F;
    dndm = dndm_un + dndm_add;
else % Do not normalise
    norm = cospars.h^(-5) * (1+z_vals)^3; % HMMMMMMMM?????
    %norm = cospars.h^(-4); % Getting rid of h factors!
    dndm = dndm_un / norm; %%%[h^4 Mpc^-3 M_solar^-1]

end

% Now find the bias:
nu = (delta_c ./ sigma ./D_plus).^2; % [unitless]

% From Seljak 0001493v1 (after equn 9):
bias_unnorm =  1  +  (a* nu - 1 )/delta_c  +  2*p./( 1 + (a*nu).^p );

% Need to normalise the bias:
%norm = sum( dndm'  .* dM .* M_vals  / (rho_mean_Mpc_0*(1+z_vals)^3) .* bias_unnorm' ); %%%[no units]
norm = sum( dndm'  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias_unnorm' ); %%%[no units]
%norm = 1;
bias = bias_unnorm / norm; %%%[no units]


% Now the mass integral:
Int_2H = zeros(size(k_vals));
Int_1H = Int_2H;
for kint = 1:length(k_vals)

    % THE UNITS ARE NOW DIFFERENT THAN WHAT IT SAYS HERE!!!!:

    %dPdM_2H = dM' .* dndm .* rhoFT(:,:,kint) .* bias / (rho_mean_Mpc_0*(1+z_vals)^3); %%%[Mpc^-3 kg m^-2]
    dPdM_2H = dM' .* dndm .* rhoFT(:,:,kint) .* bias / rho_mean_Mpc_0; %%%[Mpc^-3 kg m^-2]

    Int_2H(kint) = sum(dPdM_2H); %%%[M_sol Mpc^-5]

    %dPdM_1H = dM' .* dndm .* rhoFT(:,:,kint).^2 / rho_mean_Mpc_0^2 / (2*pi)^3; %%%[Mpc^-3 kg^2 m^-4]
    dPdM_1H = dM' .* dndm .* rhoFT(:,:,kint).^2 / (rho_mean_Mpc_0)^2; %%%[Mpc^-3 kg^2 m^-4]

    Int_1H(kint) = sum(dPdM_1H); %%%[M_sol^2 Mpc^-7]

end


tmp_pk_C = Int_2H.^2 .* pk_lin; %%%[Mpc^3 h^-3]
tmp_pk_P = Int_1H; %%% [Mpc^3 h^-3]

pk_nlin = tmp_pk_C + tmp_pk_P;

return
%% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
if 1
    figure(1)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    l=loglog(k_vals,tmp_pk_C,'r'); hold on
    m=loglog(k_vals,tmp_pk_P,'b'); hold on
    n=loglog(k_vals,pk_nlin,'g'); hold on
    o=loglog(k_vals,pk_lin,'k-.');
    p=loglog(k_vals,pk_vals,'k--');
    set([l m n o p],'linewidth',1.2);
    ylabel('P(k)'); xlabel('k, waveno.')
elseif 0
    % Dimensionless MPS:
    DELTA_C = k_vals.^3.*tmp_pk_C /2/pi;% / (1+z_vals)^6; %%%[no units]
    DELTA_P = k_vals.^3.*tmp_pk_P /2/pi;% / (1+z_vals)^3; %%%[no units]

    figure(3)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    %pk_vals_1 = pk_nlin_get(cospars,k_vals,z_vals, 'smith');
    %pk_vals_pd = pk_nlin_get(cospars,k_vals,z_vals, 'p&d');
    DELTA_nlin = k_vals.^3 .*pk_vals /2/pi; %%%[no units]
    %DELTA_nlin_1 = k_vals.^3 .*pk_vals_1 /2/pi; %%%[no units]
    %DELTA_nlin_pd = k_vals.^3 .*pk_vals_pd /2/pi; %%%[no units]
    DELTA_lin = k_vals.^3 .*pk_lin /2/pi; %%%[no units]

    l=loglog(k_vals,DELTA_C,'r'); hold on
    m=loglog(k_vals,DELTA_P,'b'); hold on
    n=loglog(k_vals,(DELTA_C+DELTA_P),'g'); hold on
    o=loglog(k_vals,DELTA_lin,'k-.');
    p=loglog(k_vals,DELTA_nlin,'k');
    %loglog(k_vals,DELTA_nlin_1,'m--');
    %loglog(k_vals,DELTA_nlin_pd,'k--');
    set([l m n o p],'linewidth',1.2);
    ylabel('\Delta^2(k)'); xlabel('k, waveno.')
    axis([0.01 100 1e-3 9*1e3])
    legend([l m n o p],'2H','1H','halo model','linear mps','halofit - smith et al',2)
    titlestring = ['at z=' num2str(z_vals) ', 3D, just summing & my scale mass'];
    title(titlestring)
end

%fprintf(1, 'Halo model calculation of non-linear Pk took %1.0f seconds\n', toc)
