function [pk_nlin pk_lin] = pk_nlwarmhalo(k_vals,pk_lin_0,cospars,z_vals,M_vals);
% PK_NLWARMHALO(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool,noth)
%
% k_vals - logarithmic vector of multipoles
% pk_lin_0 - linear, present day matter power spectrum
% cospars - structure containing cosmology (use e.g. setWMAPpars.m)
% z_vals - vector of redshif values
% M_vals - halo masses present in universe
%
% This function calculates the non-linear matter power spectra at redshift
% z_val and wavenumbers k_vals, where we integrate over M_vals in the HALO
% MODEL. It is calculated in 3D, see e.g. Seljak 0001493.
% parent codes: lens_methods_test.m & lenshalo_tomo_updating.m &
% pk_nlhalo.m.
% This function calculates the Warm Dark Matter effect on the halo
% components as in Smith & Markovic, arXiv:1103.2134.
%
% version 2: pk_nlhalo
% 31.03.2010 KMarkovic
% version 3: pk_nlwarmhalo
% 18.05.2011 KMarkovic

%% Set default parameters: clf; figure; clear; clc; 
if exist('rho_s','var') clear; end
if ~exist('cospars','var') cospars = setWMAP7pars; end
if ~exist('M_vals','var') M_vals = logspace(0,16,50); end
if ~exist('z_vals','var') z_vals = 2; end
if ~exist('k_vals','var') k_vals = logspace(-4,4,1000); end
% cospars.m_wdm = 1000; cospars.omega_wdm = 0;%cospars.omega_dm;

tic

% Growth factor:
D_plus = growth_factor(cospars,z_vals);

% Linear matter power spectrum:
if ~exist('pk_lin_0','var')
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
elseif isnan(pk_lin_0) % added 22.12.2010 KM
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
end
if cospars.m_wdm ~= 0
    fprintf(1,'WARNING: Linear matter power spectrum should not be suppressed!\n')
end
pk_lin = pk_lin_0 * D_plus.^2; % Grow spectrum


% Mass functions: PEACOCK & DODDS! - huh?
[dndm_un tmp M_scale_tmp sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_0); % Pk today or at z??
M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0,1); %%% [M_solar]


% Distances to halos:
clear consts; consts
Dh = c_km/H0_Mpc; % in Mpc
chis = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
chi = chis * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;


% Halo model parameters:
[rv_vals theta_v conc rho_s n00] = haloparams(cospars.omega_m,...
    cospars.h,M_scale,M_vals,z_vals,chi);%%% [Mpc][no units][no units][M_sol/Mpc^3]
%cospars.h,M_scale,M_vals,z_vals,chi*Mpc);%%% [meters][no units][no units][kg/m^3]
%rv_vals = rv_vals/Mpc;
%rho_s = rho_s/M_solar*Mpc^3;


% For integration:
dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]


% Critical overdensity of collapse
delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units] % NOT THIS


% CHECK UP TO HERE, FIX and CONTINUE... (05.03.2010 KM)
%%


% Normalisation, bias and mass integral:
a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]


% First Sigma_l: THIS TAKES VERY LONG!
%rhoFT = fourierkappa(z_vals, chi, M_vals, l_vals,theta_v,rv_vals, conc, rho_s, ONE,noth);
tic
rhoFT = fourierrho(z_vals, M_vals, k_vals, rv_vals, conc, rho_s, cospars);
toc

%% Need to normalise the mass functions - units: (Mpc/h)^-3/(M_solar/h)
rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m; %%%[M_sol/Mpc^3]
norm = sum( dndm_un' .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
%dndm = dndm_un / norm /(1+z_vals)^3 /cospars.h^(-4) ; %%%[h^4 Mpc^-3 M_solar^-1]
dndm = dndm_un / norm; %%%[h^4 Mpc^-3 M_solar^-1]


% Now find the bias:
nu = (delta_c ./ sigma ./D_plus).^2; % [unitless]

% From Seljak 0001493v1 (after equn 9):
bias_unnorm =  1  +  (a* nu - 1 )/delta_c  +  2*p./( 1 + (a*nu).^p );

% Need to normalise the bias:
%norm = sum( dndm'  .* dM .* M_vals  / (rho_mean_Mpc_0*(1+z_vals)^3) .* bias_unnorm' ); %%%[no units]
norm = sum( dndm'  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias_unnorm' ); %%%[no units]
%norm = 1;
bias = bias_unnorm / norm; %%%[no units]


%% Now add WDM modifications to the mass function:
Mfs = freestreaming_comov(cospars,cospars.m_wdm);%,n00);
dndm_w = massfn_wdm(M_vals,dndm,Mfs,0.5); %

clf; loglog(M_vals,dndm,'k'); hold on
loglog(M_vals,dndm_w,'r')
loglog(M_vals,dndm_un,'b')

% WDM smooth component modification:
M_vals_cut = M_vals.*(M_vals>Mfs);

fr = sum(dM .* M_vals_cut .* dndm') / rho_mean_Mpc_0; %%%[Mpc^-3 kg m^-2]

% Smooth component bias
numer = sum( dM .* M_vals_cut .* dndm'  / rho_mean_Mpc_0 .* bias' );
denom = sum( dM .* M_vals_cut .* dndm'  / rho_mean_Mpc_0);
b_eff = numer/denom;

bias_smooth = (1-fr*b_eff)/(1-fr);


% Now the mass integral:
Int_2H = zeros(size(k_vals));
Int_1H = Int_2H;
for kint = 1:length(k_vals)
    
    % THE UNITS ARE NOW DIFFERENT THAN WHAT IT SAYS HERE!:

    %dPdM_2H = dM' .* dndm .* rhoFT(:,:,kint) .* bias / (rho_mean_Mpc_0*(1+z_vals)^3); %%%[Mpc^-3 kg m^-2]
    dPdM_2H = dM' .* dndm .* rhoFT(:,:,kint) .* bias / rho_mean_Mpc_0; %%%[Mpc^-3 kg m^-2]

    Int_2H(kint) = sum(dPdM_2H); %%%[M_sol Mpc^-5]

    %dPdM_1H = dM' .* dndm .* rhoFT(:,:,kint).^2 / rho_mean_Mpc_0^2 / (2*pi)^3; %%%[Mpc^-3 kg^2 m^-4]
    dPdM_1H = dM' .* dndm .* rhoFT(:,:,kint).^2 / (rho_mean_Mpc_0)^2; %%%[Mpc^-3 kg^2 m^-4]

    Int_1H(kint) = sum(dPdM_1H); %%%[M_sol^2 Mpc^-7]

end


% Smooth component of density field due to WDM:
pk_ss = bias_smooth^2 * pk_lin;

% Smooth-Halo component
pk_sh = bias_smooth * Int_2H .* pk_lin;

% Halo component
pk_hh = (Int_1H) + (Int_2H.^2 .* pk_lin); %%%[Mpc^3 h^-3]


% Total non-linear density power spectrum from the WDM halo model
pk_nlin = (1-fr)^2.*pk_ss + 2*(1-fr)*pk_sh + fr^2*pk_hh;


%% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
if 1
    figure(2)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    
    k =loglog(k_vals,pk_hh,'b'); hold on
    l=loglog(k_vals,pk_ss,'r'); hold on
    m=loglog(k_vals,pk_sh .* pk_lin,'m'); hold on
    n=loglog(k_vals,pk_nlin,'k'); hold on
    
    o=loglog(k_vals,pk_lin,'k--');
    
    p=loglog(k_vals,pk_vals,'k:');
    %set([l m n o p],'linewidth',1.2);
    ylabel('P(k)'); xlabel('k, waveno.')
    legend([n,o,p],'Halo','Linear','Smith Non-Lin',3)
elseif 0
    % Dimensionless MPS:
    DELTA_C = k_vals.^3.*tmp_pk_C /2/pi;% / (1+z_vals)^6; %%%[no units]
    DELTA_P = k_vals.^3.*tmp_pk_P /2/pi;% / (1+z_vals)^3; %%%[no units]

    figure(3)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    %pk_vals_1 = pk_nlin_get(cospars,k_vals,z_vals, 'smith');
    %pk_vals_pd = pk_nlin_get(cospars,k_vals,z_vals, 'p&d');
    DELTA_nlin = k_vals.^3 .*pk_vals /2/pi; %%%[no units]
    %DELTA_nlin_1 = k_vals.^3 .*pk_vals_1 /2/pi; %%%[no units]
    %DELTA_nlin_pd = k_vals.^3 .*pk_vals_pd /2/pi; %%%[no units]
    DELTA_lin = k_vals.^3 .*pk_lin /2/pi; %%%[no units]

    l=loglog(k_vals,DELTA_C,'r'); hold on
    m=loglog(k_vals,DELTA_P,'b'); hold on
    n=loglog(k_vals,(DELTA_C+DELTA_P),'g'); hold on
    o=loglog(k_vals,DELTA_lin,'k-.');
    p=loglog(k_vals,DELTA_nlin,'k');
    %loglog(k_vals,DELTA_nlin_1,'m--');
    %loglog(k_vals,DELTA_nlin_pd,'k--');
    set([l m n o p],'linewidth',1.2);
    ylabel('\Delta^2(k)'); xlabel('k, waveno.')
    axis([0.01 100 1e-3 9*1e3])
    legend([l m n o p],'2H','1H','halo model','linear mps','halofit - smith et al',2)
    titlestring = ['at z=' num2str(z_vals) ', 3D, just summing & my scale mass'];
    title(titlestring)
end

fprintf(1, 'Halo model calculation of non-linear Pk took %1.0f seconds\n', toc)