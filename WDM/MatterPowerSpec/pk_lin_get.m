function [pk_lin pk_lin_0 growth] = pk_lin_get(k_vals,cospars,z)
% PK_LIN_GET(k_vals,cospars,z) is a function that can get
%	the linear matter power spectrum at redshift 'z' in the
%	cosmology described by the structure 'cospars'. The P(k)s
%	are calculated at wavenumbers 'k_vals', which are passed as
%	a vector. The default redshift is today (z=0).
%	This function uses Sarah Bridle's pkf function.
%
%	version 1
%	25.07.2013 KMarkovic

if ~exist('z','var') z = 0.0; end
if length(z) > 1 error('Cant handle multipe redshifts yet!\n'); end
if isfield('cospars','As') error('Dont set As in cospars!\n'); end

% Get P(k) today, no WDM yet
Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns);

% Suppress by WDM
pk_wdm_0 = wdm_transfn_pars(pk_lin_0,k_vals,cospars);

% Grow
growth = growth_factor(cospars,z);
pk_lin = pk_wdm_0 * growth^2;