%function pk_nlin = pk_nlhalo_demo(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool,noth);
% PK_NLHALO_DEMO(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool,noth)
%
% k_vals - logarithmic vector of multipoles
% pk_lin_0 - linear, present day matter power spectrum
% cospars - structure containing cosmology (use e.g. setWMAPpars.m) 
% z_vals - vector of redshif values
% M_vals - halo masses present in universe
% normbool - 0 for no normalization, 1 for yes, 3 for weird way
% noth - number of steps in theta in the fourier transform of 2D NFW profile
% 
% This function calculates the non-linear matter power spectra at redshift
% z_val and wavenumbers k_vals, where we integrate over M_vals in the HALO
% MODEL. Note that it is calculated in 2D, so I'm not sure how valid the
% conversion from Cl -> Pk_nlin is.
% parent codes: lens_methods_test.m & lenshalo_tomo_updating.m
%
% version 1
% 23.02.2010 KMarkovic
%
% NOT NORMALISING THE BIAS!!!

%% Set default parameters: clf; figure;
if exist('rho_s','var') clear; end
if ~exist('cospars','var') cospars = setWMAPpars; end
if ~exist('M_vals','var') M_vals = logspace(0,16,10); end
if ~exist('z_vals','var') z_vals = 0.5; end
if ~exist('k_vals','var') k_vals = logspace(-4,4,1000); end
if ~exist('normbool','var') normbool = 0; end% Mass function normalisation
if ~exist('noth','var') noth = 400; end

tic

% Growth factor:
D_plus = growth_factor(cospars,z_vals);

% Linear matter power spectrum:
if ~exist('pk_lin_0','var') 
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
end
pk_lin = pk_lin_0 * D_plus.^2; % Grow spectrum


% Mass functions:
[dndm_un tmp M_scale_tmp sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_0); % Pk today or at z??
M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0,1); %%% [M_solar]

% consts
% R_vals = ( 3*M_vals/ (4*pi*rho_crit_0_Mpc*cospars.omega_m) ).^(1/3); 
% figure; loglog(R_vals,sigma); xlabel('R[Mpc]'); ylabel('\sigma(R)')
% sigma8 = spline(R_vals,sigma,8/cospars.h);
% sigma8 - cospars.sigma8
% Check normalization!

% Distances to halos:
clear consts; consts
Dh = c_km/H0_Mpc; % in Mpc
chis = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
chi = chis * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10;


% Halo model parameters:
[rv_vals theta_v conc rho_s n00] = haloparams(cospars.omega_m,...
    cospars.h,M_scale,M_vals,z_vals,chi*Mpc);%%% [meters][no units][no units][kg/m^3]
rv_vals = rv_vals/Mpc;
rho_s = rho_s/M_solar*Mpc^3;


% For integration:
dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]


% Critical overdensity of collapse
delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units] % NOT THIS


% CHECK UP TO HERE, FIX and CONTINUE... (05.03.2010 KM)
%%


% Normalisation, bias and mass integral:
a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]

ONE = ones(length(z_vals),1);%%%[no units here!]
SIGMA_l = zeros(length(M_vals),length(z_vals),length(k_vals));
for zint = 1:length(z_vals)

    clear l_vals
    l_vals = k_vals * cospars.h * chi(zint);
    %l_vals = k_vals * chi(zint);
    
    % First Sigma_l
    SIGMA_l(:,zint,:) = fourierkappa(z_vals(zint), chi, M_vals, l_vals,theta_v,...
        rv_vals, conc, rho_s, ONE,noth);


    % Need to normalise the mass functions - units: (Mpc/h)^-3/(M_solar/h)
    %rho_mean_Mpc = rho_crit_0_Mpc * cospars.omega_m*(1+z_vals(zint))^3; %%%[M_sol/Mpc^3]
    rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m; %%%[M_sol/Mpc^3]

    %norm = sum( dndm_un(:,zint)' .* dM*cospars.h^4 .* M_vals/ rho_mean_Mpc); %%% [no units]
    %norm = sum( dndm_un(:,zint)' .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
    if normbool % Standard normalisation
        %norm = sum( dndm_un(:,zint)' .* dM .* M_vals/ rho_mean_Mpc) ; %%% [no units]
        norm = sum( dndm_un(:,zint)' .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
        dndm(:,zint) = dndm_un(:,zint) / norm; %%%[h^4 Mpc^-3 M_solar^-1]

    elseif normbool==3 % Add missing mass into small halos
        Delta = 1 - norm;
        %F = rho_mean_Mpc * Delta / M_vals(1) /dM(1); % ****
        F = rho_mean_Mpc_0 * Delta / M_vals(1) /dM(1); % ****
        dndm_add = zeros(size(dndm_un(:,zint)));
        dndm_add(1) = F;
        dndm(:,zint) = dndm_un(:,zint) + dndm_add;
    else % Do not normalise
        norm = cospars.h^(-4);
        dndm(:,zint) = dndm_un(:,zint) / norm; %%%[h^4 Mpc^-3 M_solar^-1]

    end

    %dndm(:,zint) = dndm_un(:,zint) * (1 + z_vals(zint))^3; % nope its not this...

    % Now find the bias:
    %nu = delta_c(zint).^2 ./ sigma(:,zint).^2 ; %%% [unitless]
    fprintf('\tNote: NOT SURE ABOUT SIGMA * D(z)!!\n')
    nu = (delta_c(zint) ./ sigma(:,zint) ./D_plus(zint)).^2; % [unitless]
    %nu = (1.686 ./ sigma(:,zint) ./D_plus(zint)).^2; % [unitless]

    % From Seljak 0001493v1 (after equn 9):
    bias_unnorm =  1  +  (a* nu - 1 )/delta_c(zint)  +  2*p./( 1 + (a*nu).^p ); 
    % added the a !!!!!! 23.02.2010

    % Need to normalise the bias:
    %norm = sum( dndm(:,zint)'  .* dM .* M_vals  / rho_mean_Mpc .* bias_unnorm' ); %%%[no units]
    %norm = sum( dndm(:,zint)'  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias_unnorm' ); %%%[no units]
    norm = 1;
    bias = bias_unnorm / norm; %%%[no units]


    % Now the mass integral:
    for lint = 1:length(l_vals)

        % THE UNITS ARE NOW DIFFERENT THAN WHAT IT SAYS HERE!!!!:

        dIntM_C = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint) .* bias; %%%[Mpc^-3 kg m^-2]

        IntM_C(zint,lint) = sum(dIntM_C); %%%[M_sol Mpc^-5]

        dIntM_P = dM' .* dndm(:,zint) .* SIGMA_l(:,zint,lint).^2; %%%[Mpc^-3 kg^2 m^-4]

        IntM_P(zint,lint) = sum(dIntM_P); %%%[M_sol^2 Mpc^-7]

    end

    tmp_Correlations = IntM_C(zint,:) .* chi(zint)^2 / rho_mean_Mpc_0; %%%[no units]
    %tmp_Poisson = IntM_P(zint,:) .* chi(zint)^4 / rho_mean_Mpc(zint)^2;%%%[m^3]
    tmp_Poisson = IntM_P(zint,:) .* chi(zint)^4 / rho_mean_Mpc_0^2;%%%[Mpc^3]

    %tmp_intrpl_C = interp1(l_vals/chi(zint)/cospars.h,tmp_Correlations,k_vals);%%%[no units]
    %tmp_intrpl_P = interp1(l_vals/chi(zint)/cospars.h,tmp_Poisson,k_vals);%%%[m^3]
    %tmp_pk_C(zint,:) = tmp_intrpl_C.^2 .* pk_lin(zint,:); %%%[Mpc^3 h^-3]
    %tmp_pk_P(zint,:) = tmp_intrpl_P * cospars.h^3; %%% [m^3]
    tmp_pk_C(zint,:) = tmp_Correlations.^2 .* pk_lin(zint,:); %%%[Mpc^3 h^-3]
    %tmp_pk_C(zint,:) = tmp_Correlations.^2; %%%[Mpc^3 h^-3]
    tmp_pk_P(zint,:) = tmp_Poisson * cospars.h^3; %%% [Mpc^3 h^-3]
    
    
     % Dimensionless MPS:
    DELTA_C(zint,:) = k_vals.^3.*tmp_pk_C(zint,:) /2/pi; %%%[no units]
    DELTA_P(zint,:) = k_vals.^3.*tmp_pk_P(zint,:) /2/pi ; %%%[no units]


end

%pk_nlin = tmp_pk_C + tmp_pk_P;
pk_nlin = tmp_pk_C + tmp_pk_P;


% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
if 0
    figure(2)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    %pk_vals2 = pk_nlin_get(cospars,k_vals,z_vals, 'p&d');
    pk_vals1 = pk_nlin_get(cospars,k_vals,z_vals, 'smith');
    l=loglog(k_vals,tmp_pk_C,'r'); hold on
    %loglog(k_vals,tmp_Correlations.^2,'r-.');
    %loglog(k_vals,squeeze(sum(SIGMA_l(90:100,zint,:))).^2.* chi(zint)^4 / rho_mean_Mpc_0^2,'c-.');
    %loglog(k_vals,squeeze(sum(SIGMA_l(90:100,zint,:).^2)).* chi(zint)^4 / rho_mean_Mpc_0^2,'c--');
    m=loglog(k_vals,tmp_pk_P,'b'); hold on
    n=loglog(k_vals,pk_nlin,'g'); hold on
    o=loglog(k_vals,pk_lin,'k-.');
    p=loglog(k_vals,pk_vals,'k--');
    p1=loglog(k_vals,pk_vals1,'k:');
    %p2=loglog(k_vals,pk_vals2,'b--');
    set([l m n o p],'linewidth',1.2);
    ylabel('P(k)'); xlabel('k, waveno.')
elseif 1
    figure(3)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    pk_vals2 = pk_nlin_get(cospars,k_vals,z_vals, 'p&d');
    pk_vals1 = pk_nlin_get(cospars,k_vals,z_vals, 'smith');
    DELTA_nlin = k_vals.^3 .*pk_vals /2/pi; %%%[no units]
    DELTA_nlin1 = k_vals.^3 .*pk_vals1 /2/pi; %%%[no units]
    DELTA_nlin2 = k_vals.^3 .*pk_vals2 /2/pi; %%%[no units]
    DELTA_lin = k_vals.^3 .*pk_lin /2/pi; %%%[no units]

    l=loglog(k_vals,DELTA_C,'r'); hold on
    m=loglog(k_vals,DELTA_P,'b'); hold on
    n=loglog(k_vals,(DELTA_C+DELTA_P),'g--'); hold on
    o=loglog(k_vals,DELTA_lin,'k-.');
    p=loglog(k_vals,DELTA_nlin,'k');
    p1=loglog(k_vals,DELTA_nlin1,'k:');
    p2=loglog(k_vals,DELTA_nlin2,'k--');
    %p2=loglog(k_vals,pk_vals2,'b--');
    %loglog(k_vals,k_vals'.^3 .*squeeze(sum(SIGMA_l(:,zint,:).^2))/2/pi.* chi(zint)^4 / rho_mean_Mpc_0^2,'c-.');
    set([l m n o p],'linewidth',1.2);
    ylabel('\Delta^2(k)'); xlabel('k, waveno.')
    axis([0.01 1000 1e-2 9*1e4])
    legend([l m n o p p2],'2H','1H','halo_t_o_t','linear mps','smith et al','p&d',2)
    titlestring = ['at z=' num2str(z_vals) ', ' num2str(noth) ' steps in theta, just summing & my scale mass'];
    title(titlestring)
end

fprintf(1, 'Halo model calculation of non-linear Pk took %1.0f seconds\n', toc)