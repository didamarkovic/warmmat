%function [pk_vals pk_grown pk_lin_0] = pk_nlin_get(cospars,k_vals,z_vals,typ,pk_lin_0,M_vals)
function [pk_vals pk_grown pk_wdm] = pk_nlin_get(cospars,k_vals,z_vals,typ,pk_lin_0,M_vals)
%PK_NLIN_GET  calculates the non-linear matter power spectrum for given
%    values of the wavenumber, k_vals and for a given cosmology (cospars).
%
%   [pk_vals pk_lin_grown pk_lin_0] = PK_NLIN_GET(cospars,k_vals,z_vals,typ,
%                                                                   pk_lin)
%   cospars - object containing cosmology - see setpars function
%   k_vals - a row array of wavenumbers needed [h Mpc^-1]
%   z_vals - redshifts (for growth factors)
%   typ - string either 'p&d' for Peacock and Dodds  (astro-ph/9311057v1)
%          nonlinear power spectrum or 'smith2' for Smith et al.(0207664v2)
%          - now added also 'readsmith' for Smith et al. 2011 (09.12.2010)
%   pk_lin - the linear power spectrum (don't need it, function can find it
%            itself from Sarah Bridle's code pkf.m)
%
%   pk_vals & pk_lin_grown - come out as a matrix with k varying between column and
%   different z in different rows
%   pk_lin is the straight result from Sarah's pkf.m -> no WDM & at z = 0!
%
%    version 1.2
%   29/07/08 KMarkovic
%   version 1.3
%   Added the full halo model & modified the help file.
%   08.04.2010 KM
%  version 1.4
%  Adding the possibility to read in files containing the non-lin power
%  spectra (readsmith). I'm using data files provided by Robert Smith.
%  09.12.2010 KM

% Add some parameters that can be varied:
if ~isfield(cospars,'ns') cospars.ns = 1; end
if ~isfield(cospars,'n_run') cospars.n_run = 0; end

if isfield(cospars,'Gamma')
    Gamma = cospars.Gamma;
    
    %fprintf('\tRECALCULATING h FROM GAMMA!\n')
    funct = @(x) Gamma - cospars.omega_m*x*exp(-2*cospars.omega_b*x);
    cospars.h = fzero(funct,[0 1.5]);
    
    boogamma = 1;
else
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    %Gamma = exp(-2*cospars.omega_b*0.7) *cospars.omega_m *0.7;
    
    boogamma = 0;
end

% First finding linear power spectrum if not inputted:
if ~exist('pk_lin_0','var')
    if isfield(cospars,'As')
        if ~isfield(cospars,'k_pivot') error('No k_pivot!'); end
        pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run,cospars.k_pivot,cospars.As);
    else
        pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
    end
elseif isnan(pk_lin_0)
    if isfield(cospars,'As')
        if ~isfield(cospars,'k_pivot') error('No k_pivot!'); end
        pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run,cospars.k_pivot,cospars.As);
    else
        pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
    end
elseif boogamma
    warning('Why do you use Gamma, if you are inputting the Pks???\n');
end


if  ~strcmp(typ,'readsmith')||nargout>1
    
    if ~exist('typ','var') typ = 'smith2'; end % default method
    
    % Possibility of WDM (iffy):
    bool = 0;
    if strcmp(typ,'p&d')
        if isfield(cospars,'m_wdm')
            if cospars.m_wdm~=0 bool = 1; end
        end
        if isfield(cospars,'O_m_wdm')
            if cospars.O_m_wdm~=0 bool = 1; end
        end
    end
    if bool
        fprintf('Cannod do WDM with Peacock & Dodds!\n');
        pk_wdm = pk_lin_0;
    else
        pk_wdm = wdm_transfn_pars(pk_lin_0,k_vals,cospars); % pk_lin; % changed back 14/08 KM
    end
    
end

% Now find growth and growth suppression factors:
D_plus = growth_factor(cospars,z_vals);
D_minus = growth_sup_factor(cospars,z_vals); %???%

% Now make it non-linear:
if strcmp(typ,'p&d') % PEACOCK & DODDS
    pk_vals = pks_nonlin(k_vals,pk_wdm,z_vals,D_plus,D_minus)';
    
    if nargout>1
        pk_grown = zeros(length(z_vals), length(k_vals));
        for zint = 1:length(z_vals)
            pk_grown(zint,:) = pk_wdm * D_plus(zint)^2;
        end
    end
    
elseif strcmp(typ,'smith') % SMITH ET AL. - K.Markovic & D.Kirk
    fprintf(1,'Growing the matter power spectrum')
    bob = 0;
    pk_grown = zeros(length(z_vals), length(k_vals));
    pk_vals = zeros(length(z_vals), length(k_vals));
    for zint = 1:length(z_vals)
        pk_grown(zint,:) = pk_wdm * D_plus(zint)^2;
        
        pk_vals(zint,:) = pk_nlin(k_vals,pk_grown(zint,:),cospars);
        
        bob = bob + 0.2;
        if bob==1 fprintf(1,'.'); bob = 0; end
    end
    fprintf(1,'\n')
    
elseif strcmp(typ,'smith2') % SMITH ET AL. i.e. HALOFIT - S.Bridle
    cospars_S = convertpars(cospars);
    tmp = pks_nonlin_halofit(k_vals,pk_wdm,z_vals,D_plus,cospars_S);
    pk_vals = tmp';
    
    if nargout>1
        pk_grown = zeros(length(z_vals), length(k_vals));
        for zint = 1:length(z_vals)
            pk_grown(zint,:) = pk_wdm * D_plus(zint)^2;
        end
    end
    
elseif strcmp(typ,'halo') % FULL HALO MODEL

	if ~exist('M_vals','var') M_vals = logspace(5,16,50); end
    if length(z_vals)==1 && z_vals==0; z_vals = 0.00001; end
    
    pk_grown = zeros(length(z_vals), length(k_vals));
    pk_vals = zeros(length(z_vals), length(k_vals));
    for zint = 1:length(z_vals)
        [pk_vals(zint,:) pk_grown(zint,:)] = pk_nlhalo(k_vals,pk_wdm,cospars,z_vals(zint),M_vals);
        
    end
    
elseif strcmp(typ,'readsmith') % Halo Model with WDM matrices
    
    cospars_S = convertpars(cospars);
    tmp = pks_nonlin_halofit(k_vals,pk_lin_0,z_vals,D_plus,cospars_S);
    
    pk_vals = pk_nlin_readsmith(k_vals, z_vals, cospars, tmp'); % P(k) [zint, kint]
    
    if nargout>1
        pk_grown = zeros(length(z_vals), length(k_vals));
        for zint = 1:length(z_vals)
            pk_grown(zint,:) = pk_wdm * D_plus(zint)^2;
        end
    end
    
elseif strcmp(typ,'vielfit') % Halo Model with WDM matrices
    
    cospars_S = convertpars(cospars);
    cospars_S.Omega_wdm = 0;
    cospars_S.m_wdm = 0;
    pk_vals = pks_nonlin_halofit(k_vals,pk_lin_0,z_vals,D_plus,cospars_S)';
    
    pk_vals = pk_nlin_simsfit(k_vals,pk_vals,z_vals,cospars);
    
    
    if nargout>1
        pk_grown = zeros(length(z_vals), length(k_vals));
        for zint = 1:length(z_vals)
            pk_grown(zint,:) = pk_wdm * D_plus(zint)^2;
        end
    end
    
elseif strcmp(typ,'coldhalo') % Halo Model
    
    if ~exist('M_vals','var') M_vals = logspace(5,16,50); end
    if length(z_vals)==1 && z_vals==0; z_vals = 0.00001; end
    
    pk_vals = zeros(length(z_vals), length(k_vals));
    pk_grown = zeros(length(z_vals), length(k_vals));
    for zint = 1:length(z_vals)
        [pk_vals(zint,:) pk_grown(zint,:)] = ...
            pk_nlhalo_v3(k_vals,pk_wdm,cospars,z_vals(zint),M_vals);
    end
    
elseif strcmp(typ,'warmhalo') % Halo Model with WDM matrices
    
    if ~exist('M_vals','var') M_vals = logspace(5,16,50); end
    if length(z_vals)==1 && z_vals==0; z_vals = 0.00001; end
    
    pk_vals = zeros(length(z_vals), length(k_vals));
    pk_grown = zeros(length(z_vals), length(k_vals));
    for zint = 1:length(z_vals)
        [pk_vals(zint,:) pk_grown(zint,:)] = ...
            pk_nlwarmhalo(k_vals,pk_wdm,cospars,z_vals(zint),M_vals);
    end
    
elseif strcmp(typ,'linear') % Halo Model with no non-linear correction!
    
    pk_grown = zeros(length(z_vals), length(k_vals));
    for zint = 1:length(z_vals)
        pk_grown(zint,:) = pk_wdm * D_plus(zint)^2;
    end
    
    pk_vals = pk_grown;
    
else
    error('Cannot recognise the type of non-linear model!')
end

%fprintf(['\tFinished with ' typ '!\n'])