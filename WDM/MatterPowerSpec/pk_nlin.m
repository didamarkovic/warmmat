function [pkn delta_qsq delta_hsq] = pk_nlin(k_vals,pkl_vals,cospars)

% 18/07/08
% Katarina Markovic & Donnacha Kirk
% k_vals = array of wavenumber values
% pkl_vals = matching array of linear matter power spectrum values
% cospars = cosmological paramters: omega_b, omega_de, omega_dm, omega_nu, h, sigma8, w, n
% Calculates Nonlinear power spectrum based on Smith et al. 2003 0207664v2
% NB - Some calculated values e.g. neff, C_vals are missing values for
% first and/or last k values- output knlin_vals are the k_vals with defined
% pkn
% modified by KM on 20/07/08
% version 2


omega_m = cospars.omega_m; % Changed this 06/08/08 KM

delta_lsq = (k_vals.^3).*pkl_vals/(2*pi^2);

% Building array of steps in integration
dk = k_vals(2:length(k_vals)) - k_vals(1:(length(k_vals)-1));
% Adding the last step to the k_value after last
dk(length(k_vals)) = interp1(k_vals(1:(length(k_vals)-1)), dk,...
    k_vals(length(k_vals)), 'spline' );

% Integrating for each individual scale over all k_vals!
for kint = 1:length(k_vals)
    sigsq(kint) = sum( delta_lsq.* ( exp( -(k_vals./k_vals(kint)).^2 ))...
        .* dk./k_vals );
end


ksig = interp1(sigsq,k_vals,1);

% Finding neff & C from analytical formulae in Smith paper:
neff = 2 * sum(  ( (k_vals./ksig).^2 ) .* delta_lsq .*...
    ( exp( -(k_vals./ksig).^2) ).* dk./k_vals ) - 3;
CS = (3+neff)^2  +  4*sum( ( (k_vals./ksig).^2 - (k_vals./ksig).^4) .*...
    delta_lsq .*  ( exp( -(k_vals./ksig).^2) ).* dk./k_vals);

% Coefficients
a_n = 10.^(1.4861+1.8369.*neff + 1.6762.*neff.^2 + 0.7940.*neff.^3 + 0.1670.*neff.^4 - 0.6206.*CS);
b_n = 10.^(0.9463+0.9466.*neff + 0.3084.*neff.^2 - 0.9400.*CS);
c_n = 10.^(-0.2807+0.6669.*neff + 0.3214.*neff.^2 - 0.0793.*CS);
gamma_n = 0.8649 + 0.2989.*neff + 0.1631.*CS; 
alpha_n = 1.3884 + 0.3700.*neff - 0.1452.*neff.^2;
beta_n = 0.8291 + 0.9854.*neff + 0.3401.*neff.^2;
mu_n = 10.^(-3.5442 + 0.1908.*neff);
nu_n = 10.^(0.9589 + 1.2857.*neff);


% f values, see smith et al for f_#a and explanation
f_1b = omega_m^(-0.0307);
f_2b = omega_m^(-0.0585);
f_3b = omega_m^(0.0743);

y_vals = k_vals/ksig;
fy_vals = y_vals/4 + (y_vals.^2)/8;


delta_qsq = delta_lsq.*(((1+delta_lsq).^beta_n)./(1+alpha_n.*delta_lsq)).*exp(-fy_vals);
delta_hdsq = a_n.*y_vals.^(3*f_1b)./(1 + b_n.*y_vals.^f_2b + (c_n*f_3b.*y_vals).^(3-gamma_n));
delta_hsq = delta_hdsq./(1 + mu_n.*y_vals.^(-1) + nu_n.*y_vals.^(-2));

delta_nlsq = delta_qsq + delta_hsq;

pkn = (2*pi^2).*delta_nlsq./(k_vals.^3);  