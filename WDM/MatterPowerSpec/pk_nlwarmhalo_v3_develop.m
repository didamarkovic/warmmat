%function [pk_nlin pk_lin] = pk_nlwarmhalo(k_vals,pk_lin_0,cospars,z_vals,M_vals);
% PK_NLHALO(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool,noth)
%
% k_vals - logarithmic vector of multipoles
% pk_lin_0 - linear, present day matter power spectrum
% cospars - structure containing cosmology (use e.g. setWMAPpars.m)
% z_vals - vector of redshif values
% M_vals - halo masses present in universe
% normbool - 0 for no normalization, 1 for yes, 3 for weird way
% noth - number of steps in theta in the fourier transform of 2D NFW profile
%
% This function calculates the non-linear matter power spectra at redshift
% z_val and wavenumbers k_vals, where we integrate over M_vals in the HALO
% MODEL. It is calculated in 3D, see e.g. Seljak 0001493.
% parent codes: lens_methods_test.m & lenshalo_tomo_updating.m
%
% version 2
% 31.03.2010 KMarkovic
% version 3.0 Beta
% Adding the Smith & Markovic 2011 modifiction for WDM: added smooth matter
% component to the bias and power spectrum, see arXiv:1103.2134.
% 24.05.2011 KM

%% Set default parameters:

%%% DELETE THIS!!!
if exist('D_plus','var') clear; warning('Resetting WDM!'); end % DELETE THIS!!!
%%% DELETE THIS!!!

if ~exist('cospars','var') cospars = setEUCLIDpars; end
if ~exist('M_vals','var') M_vals = logspace(1,15,50); end
if ~exist('z_vals','var') z_vals = 0; end
if ~exist('k_vals','var') k_vals = logspace(-2,2,500); end

%%% DELETE THIS!!!
cospars.omega_wdm = cospars.omega_dm; % DELETE THIS!!!
cospars.m_wdm = 0;%1000; % DELETE THIS!!!
%%% DELETE THIS!!!


% Growth factor:
D_plus = growth_factor(cospars,z_vals);

% Linear matter power spectrum:
if ~exist('pk_lin_0','var')
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0_c = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
    pk_lin_0 = wdm_transfn_pars(pk_lin_0_c,k_vals,cospars);
elseif isnan(pk_lin_0) % added 22.12.2010 KM
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0_c = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
    pk_lin_0 = wdm_transfn_pars(pk_lin_0_c,k_vals,cospars);
end
pk_lin = pk_lin_0 * D_plus.^2; % Grow spectrum


%% Halo Mass Function and Bias

approach = 3;

% For integration: M_vals = logspace(1,15,100000);
dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]
% sum(dM) % trapz(M_vals,dM)

consts
rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m; %%%[M_sol/Mpc^3] - comoving!

switch approach
    
    case 1 % Sarah's newest code from 2008
        
        cospars_slb = convertpars(cospars);
        cospars_slb.dndm_A = 0.3222;%/1.410282268324314;
        [dndm M_scale bias sigma] = massfn_new(k_vals,pk_lin_0,M_vals,z_vals,cospars_slb);
   
    case 2
        
        %[dndm_un tmp M_scale sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
        [dndm tmp M_scale sigma] = massfns_slb(M_vals,z_vals,...
            cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
            cospars.sigma8,k_vals,pk_lin_0); % Pk today or at z??
        dndm = horizontal(dndm);
        sigma = horizontal(sigma);

        % Critical overdensity of collapse
        delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units] % NOT THIS
               
        % From Seljak 0001493v1 (after equn 9):
        a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]
        
        nu = horizontal((delta_c ./ sigma' ./D_plus).^2); % [unitless]
        
        bias_unnorm =  1 +  (a* nu - 1 )/delta_c/D_plus  +  2*p./( 1 + (a*nu).^p )/delta_c/D_plus; %eeek! 18.05.11

        % Need to normalise the bias:
        %norm = sum( dndm'  .* dM .* M_vals  / (rho_mean_Mpc_0*(1+z_vals)^3) .* bias_unnorm' ); %%%[no units]
        norm = sum( dndm.* dM .* M_vals  / rho_mean_Mpc_0 .* bias_unnorm ); %%%[no units]
        %norm = 1;
        bias = bias_unnorm / norm; %%%[no units]
        % sum( dndm'  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias' ); %%%[no units]
        % sum( dndm  .* dM .* M_vals  / rho_mean_Mpc_0 ); %%%[no units]

%figure(1) %clf
%loglog(M_vals, dndm.*M_vals, 'B'); hold on
%axis([1e8 1e15 1e-7 1e2])
        
    case 3
        
        %M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0,1); %%% [M_solar]
        R_vals = (  M_vals / (4 * pi/3 * rho_mean_Mpc_0*(1+z_vals).^3)  ).^(1/3); % [Mpc/h]
        %R_vals = (  M_vals / (4 * pi/3 * rho_mean_Mpc_0)  ).^(1/3); % [Mpc/h]
        
        %sigma = zeros(size(M_vals));
        %for mint = 1:length(M_vals);
        %sigma(mint) = sigmar_frompk(k_vals,pk_lin_0,R_vals(mint));
        %end

        [sigma dlogs2dlogm] = sigmasquared(k_vals,pk_lin_0,R_vals);

        % Critical overdensity of collapse
        delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units] % NOT THIS
        delta_c_z = delta_c./D_plus;
        
        % Normalisation, bias and mass integral: S-H: A = 0.3222, KM: 0.3861
        A = 0.3222; a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]

        % Independent mass variable:
        nu = horizontal((delta_c_z ./ sigma).^2); % [unitless]
        
        % Mass scale
        M_scale = interp1(nu, M_vals, 1, 'cubic');
        
        % From Seljak 0001493v1 (after equn 9):
        bias_unnorm =  1 +  (a* nu - 1 )/delta_c_z  +  2*p./( 1 + (a*nu).^p )/delta_c_z; %eeek! 18.05.11
        bias_unnorm = horizontal(bias_unnorm);

        fofnu = A * sqrt(2*a*nu/pi).*( 1 + (a*nu).^(-p) ).*exp(-a*nu/2);
        fofnu = horizontal(fofnu);

        %Aofp = 1/(1+2^(-p)*gamma(0.5-p)/gamma(0.5));
        dnu = [nu(2)-nu(1) diff(nu)];
        shouldbeone_1 = sum(fofnu.*dnu./nu) 
        %fofnu = fofnu/shouldbeone_1;
        
        %         if 1
        %             [sigma2 dlogs2dlogm] = sigmasquared(k_vals,pk_lin_0,R_vals);
        %
        %             %figure
        %             %loglog(R_vals,sigma,'k--'); hold on
        %             %loglog(R_vals,sigma2,'go');
        %             %loglog(R_vals,sigma2./sigma,'b-')
        %         else
        %             %dlogs2dlogm = diff(log10(sigma.^2))./diff(log10(M_vals));
        %             dlogs2dlogm = diff(log(sigma.^2))./diff(log(M_vals));
        %             dlogs2dlogm = [dlogs2dlogm(1) dlogs2dlogm];
        %         end
        %dndm = -0.5 * rho_mean_Mpc_0./M_vals .* fofnu .* dlogs2dlogm ./ M_vals;
        dndm = -0.5 * rho_mean_Mpc_0./M_vals .* fofnu .* dlogs2dlogm ./ M_vals;
        dndm = horizontal(dndm);
        shouldbeone_2 =  sum( dndm  .* dM .* M_vals  / rho_mean_Mpc_0 )

        % Need to normalise the bias:
        %norm = sum( dndm'  .* dM .* M_vals  / (rho_mean_Mpc_0*(1+z_vals)^3) .* bias_unnorm' ); %%%[no units]
        norm = sum( dndm.* dM .* M_vals  / rho_mean_Mpc_0 .* bias_unnorm ); %%%[no units]
        %norm = 1;
        bias = bias_unnorm / norm; %%%[no units]
        % sum( dndm'  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias' ); %%%[no units]

end

% Normalizations. N.b. units of mass function: (Mpc/h)^-3/(M_solar/h)
% ...

%% Now add WDM modifications:

Mfs = freestreaming_comov(cospars,cospars.m_wdm);%,n00);
M_vals_cut = M_vals.*(M_vals>Mfs);

dndm_w = massfn_wdm(M_vals,dndm',Mfs,0.5); %

%loglog(M_vals,dndm,'k'); hold on
%loglog(M_vals,dndm_w,'r.')

numer = sum(dM .* M_vals_cut .* dndm_w') /rho_mean_Mpc_0;
denom = 1;%sum(dM .* M_vals .* dndm'/rho_mean_Mpc_0);
if 0%cospars.m_wdm ==0
    fr = 1;
    fprintf('Setting fraction of DM in haloes to 1!\n')
else
    fr = round2sf(numer'/denom',10);%%%[Mpc^-3 kg m^-2]
end

% Smooth component bias
numer = sum( dM .* M_vals_cut .* dndm_w'  / rho_mean_Mpc_0 .* bias );
denom = sum( dM .* M_vals_cut .* dndm_w'  / rho_mean_Mpc_0);
b_eff = numer/denom;

numer = max((1-fr*b_eff),1e-15) - 1e-15; % Avoid infinities!
bias_smooth = numer/ ((1-fr)+1e-15); % Avoid infinities!


%% Fourier transform of the halo density profile:

tic
rhoFT = fourierrho(z_vals, M_vals, k_vals, M_scale, cospars);
toc

%% Integration

% Now the mass integral:
Int_2H = zeros(size(k_vals));
Int_1H = Int_2H;
for kint = 1:length(k_vals) % k_vals(kint)

    % THE UNITS ARE NOW DIFFERENT THAN WHAT IT SAYS HERE!!!!:

    %dPdM_2H = dM' .* dndm .* rhoFT(:,:,kint) .* bias / (rho_mean_Mpc_0*(1+z_vals)^3); %%%[Mpc^-3 kg m^-2]
    dPdM_2H = dM .* dndm .* rhoFT(:,:,kint)' .* bias / rho_mean_Mpc_0; %%%[Mpc^-3 kg m^-2]
    % sum(rhoFT(:,:,kint)./M_vals')/length(M_vals)

    Int_2H(kint) = sum(dPdM_2H.*(M_vals>Mfs)); %%%[M_sol Mpc^-5]

    %dPdM_1H = dM' .* dndm .* rhoFT(:,:,kint).^2 / rho_mean_Mpc_0^2 / (2*pi)^3; %%%[Mpc^-3 kg^2 m^-4]
    dPdM_1H = dM .* dndm .* rhoFT(:,:,kint)'.^2 / (rho_mean_Mpc_0)^2; %%%[Mpc^-3 kg^2 m^-4]

    Int_1H(kint) = sum(dPdM_1H.*(M_vals>Mfs)); %%%[M_sol^2 Mpc^-7]

end

% Smooth component of density field due to WDM:
pk_ss = bias_smooth^2 * pk_lin;

% Smooth-Halo component
pk_sh = bias_smooth * Int_2H .* pk_lin;

% Halo component
pk_hh = (Int_1H) + (Int_2H.^2 .* pk_lin); %%%[Mpc^3 h^-3]

%fr = 1;
% Total non-linear density power spectrum from the WDM halo model
pk_nlin = fr^2*pk_hh + 2*(1-fr)*fr*pk_sh + (1-fr)^2.*pk_ss;


tmp_pk_C = Int_2H.^2 .* pk_lin; %%%[Mpc^3 h^-3]
tmp_pk_P = Int_1H; %%% [Mpc^3 h^-3]

%pk_nlin = tmp_pk_C + tmp_pk_P;


%% Plots
if 0
    fprintf(1,'Replotting...\n')
    figure(2)
    cospars.m_wdm = 0;
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');

    p=loglog(k_vals,pk_vals,'k'); hold on % Standard Smith et al.
    o=loglog(k_vals,pk_lin,'k:');

    n=loglog(k_vals,pk_nlin,'b'); % Halo model WDM

    k=loglog(k_vals,fr^2*pk_hh,'b:'); % Halo-halo term

    l=loglog(k_vals,(1-fr)^2.*pk_ss,'r--'); % Smooth-smooth term
    m=loglog(k_vals,2*(1-fr)*fr*pk_sh,'m-.'); % Smooth-halo term

    loglog(k_vals,Int_1H,'b--'); % 1-H term of the h-h term

    %set([l m n o p],'linewidth',1.2);
    ylabel('P(k)'); xlabel('k, waveno.')
    legend([o,p,n,k,l,m],'Linear','CDM: Smith Non-Lin','Halo:','   h-h term','   s-s term','   s-h term',3)
    axis([1e-2 1e2 1e-2 1e5])
    set(gca,'YTick',logspace(-2,5,8))
elseif 1
    % Dimensionless MPS:
    DELTA_C = k_vals.^3.*tmp_pk_C /2/pi;% / (1+z_vals)^6; %%%[no units]
    DELTA_P = k_vals.^3.*tmp_pk_P /2/pi;% / (1+z_vals)^3; %%%[no units]

    figure(3)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    %pk_vals_1 = pk_nlin_get(cospars,k_vals,z_vals, 'smith');
    %pk_vals_pd = pk_nlin_get(cospars,k_vals,z_vals, 'p&d');
    DELTA_nlin = k_vals.^3 .*pk_vals /2/pi; %%%[no units]
    %DELTA_nlin_1 = k_vals.^3 .*pk_vals_1 /2/pi; %%%[no units]
    %DELTA_nlin_pd = k_vals.^3 .*pk_vals_pd /2/pi; %%%[no units]
    DELTA_lin = k_vals.^3 .*pk_lin /2/pi; %%%[no units]

    l=loglog(k_vals,DELTA_C,'r'); hold on
    m=loglog(k_vals,DELTA_P,'b'); hold on
    n=loglog(k_vals,(DELTA_C+DELTA_P),'g'); hold on
    o=loglog(k_vals,DELTA_lin,'k-.');
    p=loglog(k_vals,DELTA_nlin,'k');
    %loglog(k_vals,DELTA_nlin_1,'m--');
    %loglog(k_vals,DELTA_nlin_pd,'k--');
    set([l m n o p],'linewidth',1.2);
    ylabel('\Delta^2(k)'); xlabel('k, waveno.')
    axis([0.01 100 1e-3 9*1e3])
    legend([l m n o p],'2H','1H','halo model','linear mps','halofit - smith et al',2)
    titlestring = ['at z=' num2str(z_vals) ', 3D, just summing & my scale mass'];
    title(titlestring)
end