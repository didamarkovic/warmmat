function pk = pk_wdmtransf(cospars,z_vals,k_vals,pk_lin_cdm)
%
% This function does the WDM transformation of the linear mater power
% spectrum using the embedded function wdm_transfn.m.
%
% pk = pk_wdmtransf(cospars,z_vals,k_vals,pk_lin_cdm)
%
% 23.02.10 KM
% This version has better parameter 1/m instead of omega_wdm/m_wdm. And is
% a separate function in it's own file.
% version 2
% 08.10.09 KMarkovic
% version 1

%D_plus = growth_factor(cospars,z_vals,1000,0); % Growth factors at all z:
%D_plus = ones(size(z_vals));
%warning('Not growing Pk_lin here!!!')

if isfield(cospars,'O_m_wdm')
        warning('O_m_wdm is no longer a parameter!')
end

%nu_power = 1.2; % As Bode,Ostriker&Turok 0010389v3
nu_power = 1.12; % As Viel et al.

pk = zeros(length(k_vals),length(z_vals));

tmp = wdm_transfn_pars(pk_lin_cdm,k_vals,cospars,nu_power);

for zint = 1:length(z_vals)

    %[tfvals a pk(:,zint)] = wdm_transfn(cospars.m_wdm,cospars.omega_wdm,...
    %    cospars.h,k_vals,nu_power,pk_lin_cdm,D_plus(zint),cospars.sigma8);
    %pk(:,zint) = wdm_transfn_pars(pk_lin_cdm,k_vals,cospars,nu_power);
    pk(:,zint) = tmp;
    
end