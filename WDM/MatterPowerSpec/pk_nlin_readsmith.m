function [pknlin C2Wratio] = pk_nlin_readsmith(k_vals, z_vals, cospars, pk_nlin_CDM)

% PK_NLIN_READSMITH reads files provided by Robert Smith containing
%  non-linear matter power spectra calculated using his WDM halo model
%  approach. The non-linear power spectra are stored in matrices.
%  The structure of Robert's files is:
%  a line with 2 numbers: [number of redshifts (50)] [number of kvals (100)]
%  a matrix of nonlinear factors in 50 rows (z) and with 100 columns (k)
%  then a matrix made of 2 column vectors with 50 entries each:
%		[a factors] [growth fn.]
%  then a matrix made of 2 column vectors with 100 entries each:
%		[kvals] [powref]
%
%  pknlin = PK_NLIN_READSMITH(k_vals, z_vals, cospars, pk_nlin_CDM)
%	- P(k) [zint, kint]
%
%  K_VALS can only be between 0.0032 to 2!
%  Z_VALS can only be from 0 to 3!
%  COSPARS should be set with setSMITHpars!
%  PK_NLIN_CDM can be found using Sarah's function pks_nonlin_halofit.m!
%
%  version 1
%  script for playing around
% 21.12.2010 KMarkovic
%  version 1.1
%  Only using the WDM/CDM ratio from Robert Smith otherwise using standard
%  halofit with Sarah's pks_nonlin_halofit.m function. Therefore if have
%  CDM, this is just halofit!
% 22.12.2010 KM

%clear; clc;

if ~exist('k_vals','var') 
    k_vals = logspace(log10(0.0032),2,1000); 
elseif round2dp(min(k_vals),4)<0.0032 || max(k_vals)>100
    error('Range of k must be between %f and %f!',0.0032,100)
end
if ~exist('z_vals','var') z_vals = 0000000001:0.05:3; end
if ~exist('cospars','var') cospars = setSMITHpars; cospars.m_wdm = 0.5*1000; end
if ~exist('pk_nlin_CDM','var')
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
    D_plus = growth_factor(cospars,z_vals);
    cospars_S = convertpars(cospars); cospars_S.m_wdm = 0;
    tmp = pks_nonlin_halofit(k_vals,pk_lin_0,z_vals,D_plus,cospars_S);
    pk_nlin_CDM = tmp';
end


%% Which WDM model: 13 models in [0.25,3.25] % 28.01.2011
if cospars.m_wdm==0||cospars.m_wdm>100000
    iWDM_tmp=0;
else
    masses = linspace(0.25,3.25,13);
    iWDM_tmp = find(min(abs(cospars.m_wdm/1000-masses))==abs(cospars.m_wdm/1000-masses));
    if abs(cospars.m_wdm/1000-masses(iWDM_tmp))>0.01
        warning('You should use one of the following for the WDM mass: [0.25, 0.5, 0.75, 1, 1.25] keV!')
    end
end

%% first read the files for CDM and the WDM

if iWDM_tmp
    
    for iWDM = [0 iWDM_tmp]
        filename = ['DATA-Smith/Pow_rk_aa.' num2str(iWDM) '.dat.Katarina'];
        
        nkvals = 100;
        navals = 50;
        [data header] = myread(filename,1,nkvals,' ');
        
        % then check the header line for the size of the data
        bool=0;
        

        %18.07.2013%header = header{1};
		%Octave header = [str2num(header{11}) str2num(header{20})];
		header = [str2num(header{1}) str2num(header{2})];
        if sum([navals nkvals]~=header)&&bool
            warning('Yikes!!! Dimensions for Roberts file are wrong!!')
        end
        
        % now get the first matrix - nonlinear factors
        [row col] = find(isnan(data)); % find the filling
        
        logPowRatio = data(1:(row(1)-1),1:nkvals);
        
        if sum(size(logPowRatio)~=header)
            warning('Yikes!!! Dimensions for Roberts file are wrong!!')
        end
        
        % now get a factors and the growth function (at 50 different z)
        logavals = data(row(1):(row(1)+navals-1),1);
        growthf = data(row(1):(row(1)+navals-1),2);
        
        % now get the kvals and the linear power spectrum
        logkvals = data((row(1)+navals):max(row),1);  % max(kvals)
        powref = data((row(1)+navals):max(row),2);
        
        avals = 10.^logavals;
        zvals = 1./avals - 1;
        kvals = 10.^logkvals;
        PowRatio = 10.^logPowRatio;
        
        if iWDM==0
            PowRatioCDM = PowRatio;
            powrefCDM = powref;
            growthfCDM = growthf;
            kvalsCDM = kvals;
            avalsCDM = avals;
        end
        
    end
    
    if sum(kvals~=kvalsCDM)&&sum(avals~=avalsCDM)
        warning('The files contain different k and a - this makes everything wrong!')
    end
    
    %% get the ratio
    
    pspectrum = zeros(length(avals),length(logkvals)); % size(avals) size(logkvals')
    pspectrumCDM = zeros(length(avals),length(logkvals)); % size(avals) size(logkvals')
    for zint = 1:length(avals)
        pspectrum(zint,:)=PowRatio(zint,:)'.*powref.*growthf(zint).^2;
        pspectrumCDM(zint,:)=PowRatioCDM(zint,:)'.*powrefCDM.*growthfCDM(zint).^2;
    end
    C2Wratio_tmp = pspectrum./pspectrumCDM; % size(pspectrum)
    
    [logkvals_mesh avals_mesh]=meshgrid(logkvals,avals);
	%clear avals_mesh logkvals_mesh
    %size(avals_mesh) size(logkvals_mesh) size(C2Wratio_tmp)
    C2Wratio = interp2(logkvals_mesh,avals_mesh,C2Wratio_tmp,log10(k_vals),1./(1+z_vals'));
    % size(z_vals) size(k_vals) size(C2Wratio)
    
else
    
    C2Wratio = 1; % if have CDM
    
end


%% Finally, get the power spectrum
pknlin = pk_nlin_CDM.*C2Wratio;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plotbool = 0; ratiobool = 1; mybool = 1; plotind = 1;%length(z_vals);
indind = plotind;%find(min(abs(avals(plotind)-1./(1+z_vals)))==abs(avals(plotind)-1./(1+z_vals)));

%% plot the ratio
if plotbool && iWDM_tmp && ratiobool
    figure(2); clf; %clc
    semilogx(kvals,powref./powrefCDM*(growthf(plotind)/growthfCDM(plotind)).^2,'b:'); hold on
    semilogx(kvals,PowRatio(plotind,:)./PowRatioCDM(plotind,:),'r:'); hold on % size(powref) size(PowRatio(:,1))
    semilogx(kvals,C2Wratio_tmp(plotind,:),'k'); hold on % size(powref) size(PowRatio(:,1))
    xlabel('k, wavenumber'); ylabel('P_W_D_M(k) / P_C_D_M(k)')
    title(['m_W_D_M = ' num2str(masses(iWDM)) ' [keV] and z = ' num2str(zvals(plotind))])
    %title(['iWDM = ' num2str(iWDM)])
    axis([min(kvals) max(kvals) min(C2Wratio(plotind,:)) 1.01*max(PowRatio(plotind,:)./PowRatioCDM(plotind,:))])
    
    % interpolated
    semilogx(k_vals,C2Wratio(indind,:),'g--')
    %legend('???');
end

%% plot the spectrum
if plotbool && iWDM_tmp
    figure(1); %clf; %clc
    loglog(k_vals,pknlin(plotind,:),'r'); hold on % size(powref) size(PowRatio(:,1))
    loglog(k_vals,pk_nlin_CDM(plotind,:),'k'); hold on % size(powref) size(PowRatio(:,1))
    xlabel('k, wavenumber'); ylabel('P(k)')
    axis([min(k_vals) max(k_vals) min(pknlin(plotind,:)) 2*max(pknlin(plotind,:))])
    legend('WDM','CDM',3)
    title(['z = ' num2str(z_vals(plotind))])
end

%% c.f. to my own WDM
if plotbool && mybool && iWDM_tmp
    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
    cospars.omega_wdm = cospars.omega_dm;
    pk_lin_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
    pk_wdm = wdm_transfn_pars(pk_lin_0,k_vals,cospars); % pk_wdm(1:10)./pk_lin_0(1:10)
    D_plus = growth_factor(cospars,z_vals);
    loglog(k_vals,pk_lin_0*D_plus(indind)^2,'k:'); hold on
    loglog(k_vals,pk_wdm*D_plus(indind)^2,'m:'); hold on
    cospars_S = convertpars(cospars);
    tmp = pks_nonlin_halofit(k_vals,pk_wdm,z_vals,D_plus,cospars_S);
    pk_nlin_WDM = tmp';
    loglog(k_vals,pk_nlin_WDM(indind,:),'c--');
    %axis([min(kvals) max(kvals) min(pknlin(indind,:)) max(pknlin(indind,:))])
end

%% now my ratio too
if plotbool && mybool && iWDM_tmp && ratiobool
    figure(2)
    semilogx(k_vals,pk_nlin_WDM(indind,:)./pk_nlin_CDM(indind,:),'c--')
    legend('reference ps','nonlin ratio','final nonlinear ps',['z = ' num2str(z_vals(indind)) ' interpolated'],'just halofit',3)
    %axis([min(kvals) max(kvals) min(C2Wratio(plotind,:)) max(pk_nlin_WDM(indind,:)./pk_nlin_CDM(indind,:))])
end