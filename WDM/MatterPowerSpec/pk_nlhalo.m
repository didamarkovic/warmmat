function [pk_nlin pk_lin tmp_pk_C tmp_pk_P] = pk_nlhalo(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool)%,noth);
% PK_NLHALO(k_vals,pk_lin_0,cospars,z_vals,M_vals,normbool,noth)
%
% k_vals - logarithmic vector of multipoles
% pk_lin_0 - linear, present day matter power spectrum
% cospars - structure containing cosmology (use e.g. setWMAPpars.m)
% z_vals - vector of redshif values
% M_vals - halo masses present in universe
% normbool - 0 for no normalization, 1 for yes, 3 for weird way
% noth - number of steps in theta in the fourier transform of 2D NFW profile
%
% This function calculates the non-linear matter power spectra at redshift
% z_val and wavenumbers k_vals, where we integrate over M_vals in the HALO
% MODEL. It is calculated in 3D, see e.g. Seljak 0001493.
% parent codes: lens_methods_test.m & lenshalo_tomo_updating.m
%
% version 3
% Implement WDM nicely.
% 25.07.2013 KMarkovic

%% Set default parameters: clf; figure;
if exist('rho_s','var') clear; fprintf('Restarting calculations.\n'); end
if ~exist('cospars','var') cospars = setPlanckpars; cospars.m_wdm = 1000; end
if ~exist('M_vals','var') M_vals = logspace(5,16,50); end
if ~exist('z_vals','var') z_vals = 1; end
if ~exist('k_vals','var') k_vals = logspace(-2,2,1000); end
if ~exist('normbool','var') normbool = 0; end% Mass function normalisation
%if ~exist('noth','var') noth = 512; end

%cospars.h = 1;

% Linear matter power spectrum:
if ~exist('pk_lin_0','var')
	[pk_lin pk_lin_0 D_plus] = pk_lin_get(k_vals,cospars,z_vals);
elseif isnan(pk_lin_0)
    [pk_lin pk_lin_0 D_plus] = pk_lin_get(k_vals,cospars,z_vals);
else
	D_plus = growth_factor(cospars,z_vals);
	pk_lin = pk_lin_0 * D_plus.^2;
end


% Mass functions:
[dndm_un tmp M_scale_tmp sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_0); % Pk today or at z??
M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0,1); %%% [M_solar]

% Viel et al. (2005), Schneider et al. (2012) parameter for WDM
alpha = 0.6;

Mhm = halfmodemass(cospars); % Multiplying by 20 makes this better at z=0...
dndm_un = dndm_un' .* (Mhm./M_vals+1).^(-alpha);


% Halo model parameters:
[rv_vals theta_v conc rho_s n00] = haloparams(cospars,M_scale,M_vals,z_vals);
	%%% [Mpc][no units][no units][M_sol/Mpc^3]


% For integration:
dlogM = log(M_vals(2)) - log(M_vals(1));
dM = dlogM.*M_vals; %%%[M_solar]


% Critical overdensity of collapse
delta_c = delta_c_alt(z_vals,cospars.omega_m); %%%[no units]


% Normalisation, bias and mass integral:
a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2 %%%[no units]


% First Sigma_l
rhoFT = fourierrho(z_vals, M_vals, k_vals, M_scale, cospars);
%rhoFT(:,:,1)


% Need to normalise the mass functions - units: (Mpc/h)^-3/(M_solar/h)
consts
rho_mean_Mpc_0 = rho_crit_0_Mpc * cospars.omega_m * (1+z_vals).^3; %%%[M_sol/Mpc^3]

if normbool % Standard normalisation
    norm = sum( dndm_un .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
    %dndm = dndm_un / norm /(1+z_vals)^3 /cospars.h^(-4) ; %%%[h^4 Mpc^-3 M_solar^-1]
    dndm = dndm_un / norm; %%%[h^4 Mpc^-3 M_solar^-1]

elseif normbool==3 % Add missing mass into small halos
    norm = sum( dndm_un .* dM .* M_vals/ rho_mean_Mpc_0) ; %%% [no units]
    Delta = 1 - norm;
    F = rho_mean_Mpc_0 * Delta / M_vals(1) /dM(1); % ****
    dndm_add = zeros(size(dndm_un));
    dndm_add(1) = F;
    dndm = dndm_un + dndm_add;
else % Do not normalise
    %normm = cospars.h^(-6) * (1+z_vals)^3; % HMMMMMMMM?????
    %norm = cospars.h^(-4); % Getting rid of h factors!

	tmppars = cospars; tmppars.m_wdm = 0;
	[tmp_lin tmp_lin_0] = pk_lin_get(k_vals,tmppars,z_vals);

	tmpdn = massfns_slb(M_vals*cospars.h,z_vals,...
		cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
		cospars.sigma8,k_vals,tmp_lin_0);

	norm = sum( tmpdn' .* dM .* M_vals/ rho_mean_Mpc_0); %%% [no units]
    dndm = dndm_un / norm;% /normm; %%%[h^4 Mpc^-3 M_solar^-1]


end


% Now find the bias:
nu = (delta_c ./ sigma ./D_plus).^2; % [unitless]

% From Seljak 0001493v1 (after equn 9):
bias_unnorm =  1  +  (a* nu - 1 )/delta_c  +  2*p./( 1 + (a*nu).^p );

bool=0;
if bool
	% Need to normalise the bias:
	fr = sum(dM .* M_vals .* dndm) /rho_mean_Mpc_0;
	
	numer = sum( dM .* M_vals .* dndm .* bias_unnorm' ) /rho_mean_Mpc_0;
	b_eff = numer/fr;
	
	numer = max( ( 1 - fr*b_eff ) ,1e-15 ) - 1e-15; % Avoid infinities!
	bias_smooth = numer / ( ( 1 - fr ) + 1e-15 ) % Avoid infinities!
end

%norm = sum( dndm  .* dM .* M_vals  / (rho_mean_Mpc_0*(1+z_vals)^3) .* bias_unnorm' ); %%%[no units]
norm = sum( dndm  .* dM .* M_vals  / rho_mean_Mpc_0 .* bias_unnorm' ); %%%[no units]
bias = bias_unnorm / norm; %%%[no units]


% Now the mass integral:
Int_2H = zeros(size(k_vals));
Int_1H = Int_2H;
for kint = 1:length(k_vals)

    % THE UNITS ARE NOW DIFFERENT THAN WHAT IT SAYS HERE!!!!:


    %dPdM_2H = dM' .* dndm' .* rhoFT(:,:,kint) .* bias / (rho_mean_Mpc_0*(1+z_vals)^3); %%%[Mpc^-3 kg m^-2]
    
    dPdM_2H = dM' .* dndm' .* rhoFT(:,:,kint) .* bias / rho_mean_Mpc_0; %%%[Mpc^-3 kg m^-2]
    Int_2H(kint) = sum(dPdM_2H); %%%[M_sol Mpc^-5]


    %dPdM_1H = dM' .* dndm' .* rhoFT(:,:,kint).^2 / rho_mean_Mpc_0^2 / (2*pi)^3; %%%[Mpc^-3 kg^2 m^-4]
    
    dPdM_1H = dM' .* dndm' .* rhoFT(:,:,kint).^2 / (rho_mean_Mpc_0)^2; %%%[Mpc^-3 kg^2 m^-4]
    Int_1H(kint) = sum(dPdM_1H); %%%[M_sol^2 Mpc^-7]

end

if bool
	% Smooth component of density field due to WDM:
	pk_ss = bias_smooth^2 * pk_lin;
	
	% Smooth-Halo component
	pk_sh = bias_smooth * Int_2H .* pk_lin;
end


% Halo-halo component
tmp_pk_C = Int_2H.^2 .* pk_lin; %%%[Mpc^3 h^-3]
tmp_pk_P = Int_1H; %%% [Mpc^3 h^-3]
pk_hh = tmp_pk_P + tmp_pk_C; %%%[Mpc^3 h^-3]

if bool
	% Total non-linear density power spectrum from the WDM halo model
	pk_nlin = fr^2*pk_hh + 2*(1-fr)*fr*pk_sh + (1-fr)^2.*pk_ss;
	%pk_nlin = fr^2*pk_lin + 2*(1-fr)*fr*pk_lin + (1-fr)^2.*pk_lin;
else
    pk_nlin = pk_hh;
    pk_ss = pk_lin; pk_sh = pk_lin; fr = 1;
end


%% MPS MPS MPS MPS MPS MPS MPS MPS MPS:
if 0
    figure(1); clf;
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals,'readsmith',pk_lin_0);
    %l=loglog(k_vals,tmp_pk_C,'r'); hold on
    %loglog(k_vals,tmp_pk_P,'b'); hold on
    o=loglog(k_vals,pk_nlin,'c-'); hold on
    l=loglog(k_vals,(1-fr)^2*pk_ss,'g:'); hold on
    m=loglog(k_vals,2*(1-fr)*fr*pk_sh,'g-.'); hold on
    n=loglog(k_vals,fr^2*pk_hh,'g--'); hold on
    o=loglog(k_vals,pk_lin,'k-.');
    p=loglog(k_vals,pk_vals,'k:');
    set([l m n o p],'linewidth',1.2);
    ylabel('P(k)'); xlabel('k, waveno.');
	axis([min(k_vals) max(k_vals) 0.01 2*max(pk_lin)]);
elseif 0
    % Dimensionless MPS:
    DELTA_C = k_vals.^3.*tmp_pk_C /2/pi;% / (1+z_vals)^6; %%%[no units]
    DELTA_P = k_vals.^3.*tmp_pk_P /2/pi;% / (1+z_vals)^3; %%%[no units]

    figure(3)
    pk_vals = pk_nlin_get(cospars,k_vals,z_vals, 'smith2');
    %pk_vals_1 = pk_nlin_get(cospars,k_vals,z_vals, 'smith');
    %pk_vals_pd = pk_nlin_get(cospars,k_vals,z_vals, 'p&d');
    DELTA_nlin = k_vals.^3 .*pk_vals /2/pi; %%%[no units]
    %DELTA_nlin_1 = k_vals.^3 .*pk_vals_1 /2/pi; %%%[no units]
    %DELTA_nlin_pd = k_vals.^3 .*pk_vals_pd /2/pi; %%%[no units]
    DELTA_lin = k_vals.^3 .*pk_lin /2/pi; %%%[no units]

    l=loglog(k_vals,DELTA_C,'r'); hold on
    m=loglog(k_vals,DELTA_P,'b'); hold on
    n=loglog(k_vals,(DELTA_C+DELTA_P),'g'); hold on
    o=loglog(k_vals,DELTA_lin,'k-.');
    p=loglog(k_vals,DELTA_nlin,'k');
    %loglog(k_vals,DELTA_nlin_1,'m--');
    %loglog(k_vals,DELTA_nlin_pd,'k--');
    set([l m n o p],'linewidth',1.2);
    ylabel('\Delta^2(k)'); xlabel('k, waveno.')
    axis([0.01 100 1e-3 9*1e3])
    legend([l m n o p],'2H','1H','halo model','linear mps','halofit - smith et al',2)
    titlestring = ['at z=' num2str(z_vals) ', 3D, just summing & my scale mass'];
    title(titlestring)
end

%fprintf(1, 'Halo model calculation of non-linear Pk took %1.0f seconds\n', toc)
