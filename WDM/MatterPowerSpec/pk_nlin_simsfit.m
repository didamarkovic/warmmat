function [pknl_wdm tks2] = pk_nlin_simsfit(k_vals,pknl_cdm,z_vals,cospars)
% PK_NLIN_SIMSFIT applies Matteo Viel's WDM transfer function to a
%    pre-calculated non-linear matter power spectrum to account for the
%    suppression due to WDM free-streaming.
%
%    [pknl_wdm tks] = pk_nlin_simsfit(k_vals,pknl_cdm,z_vals,cospars)
%
%    With no input, defaults are set: z = 0, m_wdm = 1 keV.
%
%    With no output we get 2 plots: T(k) and P_nl_cdm(k) & P_nl_wdm(k).
%
% version 1
% 24.06.2011 KMarkovic

if ~exist('cospars','var') cospars = setWMAP7pars; cospars.m_wdm = 1000; end
if ~exist('z_vals','var') z_vals = 1; end
if  ~exist('k_vals','var') k_vals = logspace(-3,2,1000); end

if ~exist('pknl_cdm','var')
        Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
        pklin = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run,cospars.k_pivot,cospars.As);
        D_plus = growth_factor(cospars,z_vals);
        cospars_S = convertpars(cospars);
        cospars_S.m_wdm = 0; cospars_S.omega_wdm = 0;
        tmp = pks_nonlin_halofit(k_vals,pklin,z_vals,D_plus,cospars_S);
        pknl_cdm = tmp';
end

nu = 3;
l = 0.6;
s = 0.45;

if cospars.m_wdm==0
    alpha=0*z_vals;
else
    alpha = 0.0476 * (1000/cospars.m_wdm)^1.85 * ((1 + z_vals)/2).^1.3;
end

for zint = 1:length(z_vals)

    tks2(zint,:) = (1 + (alpha(zint)*k_vals).^(nu*l)).^(-s/nu);

end

%pknl_wdm = tks.^2 .* pknl_cdm;
pknl_wdm = tks2 .* pknl_cdm;

if nargout==0
    figure
    semilogx(k_vals,tks2(1,:),'r'); hold on
    semilogx(k_vals,k_vals.^0,'k--');
    title('WDM suppression');
    xlabel('k[h/Mpc]');
    ylabel('T(k)');
    axis([min(k_vals) max(k_vals) 0 1.01]);
    
    figure
    loglog(k_vals,pknl_wdm(1,:),'r'); hold on
    loglog(k_vals,pknl_cdm(1,:),'k--');
    title('WDM suppression');
    xlabel('k[h/Mpc]');
    ylabel('P_n_l(k)');
    %axis([min(k_vals) max(k_vals) ]);

    pknl_wdm = 'See figures.';
end