function [gstar Tx_Tnu m_nu N_nu Tx] = degoffree(m_dm,f,omega_dm,h,Tcmb)

% Calculates the number of relativistic degrees of freedom at Warm Dark
% Matter decoupling for the thermalised case.
% 
% Equations 1 and 2 from Viel et al 0501562
%
% [gstar Tx_Tnu] = degoffree(mass,f,omega_dm,h,Tcmb)
%
% mass is the mass of the dark matter particle in eV (can be an array)
% omega_dm is the dark matter denstiy (all dark matter) [0.25]
% h is the Hubble parameter [0.65]
% f is the fraction of WDM out of all DM [1]
%
% gstar is the relativistic degrees of freedom at WDM decoupling
% Tx_Tnu is the ratio of the WDM temperature and neutrino temperature
% note: Tnu ~ Tphoton * (4/11)^(1/3)
%
% 15/02/08 KM

% clear; mass = [92 427 738 1000 1441];

if ~exist('omega_dm','var') omega_dm = 0.25; end
if ~exist('h','var') h = 0.65; end
if ~exist('f','var') f = 1; end
if ~exist('Tcmb','var') Tcmb = 2.726; end

omega_wdm = f * omega_dm;

% Calculate sterile neutrino mass that has the same effect as this WDM:
m_nu = 4430 * (m_dm./1000).^(4/3) * (0.25*(0.7^2)/(omega_wdm*h^2))^(1/3);

% Calculate number of massive neutrinos from this mass: 
omega_nu = omega_wdm;
N_nu = omega_nu * h^2 * (93.4./m_nu);

% Now calculate the number of relativistic degrees of freedom:
gstar = (m_nu/93.4) * (10.75/ (omega_nu*h^2));

Tx_Tnu = (10.75./gstar).^(1/3);

Tnu = Tcmb * (4/11)^(1/3); % From Dodelson book

Tx = Tx_Tnu .* Tnu; 