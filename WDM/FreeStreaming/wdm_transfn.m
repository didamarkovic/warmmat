function [TF a Pk_wdm] = wdm_transfn(m_x,omega_x,h,k_vals,nu,Pk_cdm,grow,sigma8)

% This calculates the matter power spectrum from the "transfer function"
% fit from Viel et al. 050156 and the given CDM matter power spectrum.
%
% [TF scale_break Pk_wdm] = wdm_transfn(m_x,omega_x,h,k_vals,nu,Pk_cdm)
%
% Note: m_x must be in eV
%
% 22/02/08 KMarkovic

if ~exist('k_vals','var') k_vals = logspace(-2,1,100); end
if ~exist('nu','var') nu = 1.12; end
if ~exist('Pk_cdm') Pk_cdm = 1; end
if ~exist('grow','var') grow = 1; end
if ~exist('sigma8','var') sigma8 = 0.9; end

if grow ~= 1 warning('Growing Pk_lin!!!'); end

% Scale break
a= 0.049*((m_x/1000)^(-1.11))*((omega_x/0.25)^(0.11))*((h/0.7)^(1.22));

% "Transfer function"
if m_x==0
    TF = 1;
else
    TF = (1 + (a*k_vals).^(2*nu) ).^(-5/nu);
end

% If want matter power spectrum:
%Pk_wdm = Pk_cdm.*(TF.^2)*grow^2; % no - No! CDM pk should be grown already!
% Omg I had grow^1! and the CDM should vary over z!   

Pk_wdm = Pk_cdm.*(TF.^2);

% Added the following 26/08/08 to normalise after WDM & before growing!
%Pk_norm = pknorms8(k_vals,Pk_wdm,sigma8);
Pk_norm = Pk_wdm;
Pk_wdm = Pk_norm*grow^2;