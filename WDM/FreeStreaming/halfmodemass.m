function [Mhm Rhm khm Mfs alpha] = halfmodemass(cospars,m_vals,mu)

% This function calculates the "half-mode" halo mass from
%   arXiv:1112.0330 (Schneider et al. 2012)
%   It gets it from the free-streaming length.
%   [Mhm Rhm khm Mfs-eff Rfs-eff] = halfmodemass(cospars,m_vals,mu)
%
% m_vals should be in eV!
%
% 05.07.2013 KMarkovic

if ~exist('m_vals','var') m_vals = cospars.m_wdm; end
if ~exist('mu','var') mu = 1.12; end

% If CDM:
if (m_vals==0&&length(m_vals)==1) m_vals=Inf; end

consts;
rho = 3*H0^2/8/pi/G_Mpc * cospars.omega_m;

if cospars.omega_wdm~=0
	omega_x = cospars.omega_wdm;
else
	omega_x = cospars.omega_dm;
%warning('Assuming you mean that all DM is warm, since you set Om_WDM = 0...')
end

% Viel et al fit:
alpha = 0.049 * (m_vals/1000).^(-1.11) * (omega_x/0.25)^(0.11) * (cospars.h/0.7)^1.22; %Mpc/h

Rhm = 2*pi * ( 2^(mu/5) - 1 )^(-1/2/mu) * alpha;
Mhm = (4*pi/3) * rho * (Rhm/2).^3 /cospars.h^3;
Mhm = Mhm * cospars.h; %Msol/h


if nargout >= 3
    khm = 2*pi./Rhm;

    Rfs = 0.11* (omega_x*cospars.h^2/0.15)^(1/3) * (m_vals/1000).^(-4/3);

    %Mfs = (4*pi/3) * rho * (alpha/2).^3 /cospars.h^3;
    Mfs = (4*pi/3) * rho * (Rfs/2).^3 /cospars.h^3;

    Mfs = Mfs * cospars.h; %Msol/h
end