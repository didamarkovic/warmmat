function [Mfs Rfs kfs] = freestreaming_comov(cospars,m_vals)

% This function calculates the lenght of free-streaming for dark matter
% particles of different masses, the masses of halos with the
% correspronding virial radius using comoving coordinates.
%
% [Mfs Rvir kfs] = freestreaming_pars(cospars,m_vals)
%
% m_vals should be in eV!
%
% 10/03/2008 KMarkovic
%
% version 3
% 17/09/08 KM
%
% version 4
% Wow, was using rho_crit and n00 = 200. Should be rho_m(z) and since linear
% regime, no n00!
% 27.07.2010 KM
% Modified so that the if statement can take a m_wdm array.
% 02.06.2011 KM

if ~exist('m_vals','var') m_vals = cospars.m_wdm; end

if length(m_vals)==1
    if cospars.omega_wdm ==0  || m_vals == 0
        Mfs = 0; Rfs = 0; kfs = Inf;
        return
    end
    fixbool = 0;
else
    fixbool = 1;
end

h = cospars.h;
consts
if cospars.omega_wdm~=0
	omega_x = cospars.omega_wdm;
else
	omega_x = cospars.omega_dm;
	warning('Assuming you mean that all DM is warm, since you set Om_WDM = 0...')
end
%if ~exist('n00','var') n00 = 200; end% Overdensity
z = 0; % Dumbass, it's all in comoving!!

% To find free-streaming lengths:
Rfs = 0.11* (omega_x*h^2/0.15)^(1/3) * (m_vals/1000).^(-4/3); %Mpc
% Zentner & Bullock

% To find M values: this is from Schneider:    tmp = sqrt(omegafn(z,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.omega_k));
%rho = n00 * rho_crit_0 *Mpc^3;
rho = omega_x*rho_crit_0_Mpc*(1+z)^3; % ASSUMING ONLY WDM!
%Mfs = rho * 4*pi * Rfs.^3 /3 /M_solar;
Mfs = rho * 4*pi * (Rfs/2).^3 /3;% 19.05.2011 KM !!!!!!!!!!

if nargout >= 3
    kfs = 5 * (m_vals/1000) .* (omega_x*h^2*94./m_vals).^(-1/3); % Viel et al
end

if fixbool
    ints = m_vals == 0;
    Mfs(ints) = 0; Rfs(ints) = 0; kfs(ints) = Inf;
end