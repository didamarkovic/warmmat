function [Pk_wdm TF a] = wdm_transfn_pars(Pk_cdm,k_vals,cospars,nu)

% This calculates the matter power spectrum from the "transfer function"
% fit from Viel et al. 050156 and the given CDM matter power spectrum.
%
% [Pk_wdm TF scale_break] = wdm_transfn(Pk_cdm,k_vals,cospars,nu)
%
% Notes: m_x must be in eV
%        This function doesn't grow the PS. It should be grown separately.
%        Pk_cdm (linear!) should match the k_vals.
%        cospars should have cospars.m_wdm,cospars.omega_wdm and cospars.h
%        specifying nu from Veil et al. is optional (nu = 1.12 by default)
%
% 22/02/08 KMarkovic, version 2: 21/07/08 KM
%
% version 2.2 
% 15/08/08 KM

if isfield(cospars,'O_m_wdm')
    if cospars.O_m_wdm~=0
    warning('Assuming only O_m_wdm matters for transfer function!')
        cospars.omega_wdm = cospars.omega_dm;
        cospars.m_wdm = cospars.omega_wdm/cospars.O_m_wdm;
    end
end

m_x = cospars.m_wdm;
omega_x = cospars.omega_wdm;
h = cospars.h;

if ~exist('nu','var')
    nu = 1.12; % From Viel et al.
end

% Scale break
a = 0.049*((m_x/1000)^(-1.11))*((omega_x/0.25)^(0.11))*((h/0.7)^(1.22));

if isnan(Pk_cdm)
    Pk_wdm = NaN; TF = NaN;
    return;
end


% "Transfer function"
if m_x==0
    TF = 1;
else
    TF = (1 + (a*k_vals).^(2*nu) ).^(-5/nu); % loglog(k_vals,TF,'m--') % added on the 22.12.2010 KM
end

% If want matter power spectrum:
Pk_wdm_tmp = Pk_cdm.*(TF.^2); % loglog(k_vals,Pk_cdm,'b--') 

if ~isfield(cospars,'As')
    Pk_wdm = pknorms8(k_vals,Pk_wdm_tmp,cospars.sigma8); % 12.04.2010 KM
    if Pk_wdm(1)/Pk_cdm(1)>(1+1e-5)
        warning('Not normalizing by sigma8, because it will increase WDM spectrum on large scales!')
        Pk_wdm = Pk_wdm * Pk_cdm(1)/Pk_wdm(1);
    end
else
    Pk_wdm = Pk_wdm_tmp;
end