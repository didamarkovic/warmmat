% TEST_MASCALE is the mommy
%
% 12.07.2013 KMarkovic

clear

m_vals = [Inf 10000 2000 1000 500 250];
cols = 'kbcgrm';

cospars = setPlanckpars;
consts;
Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;

kvals = logspace(-4,4,1000);
pkvals = pkf(Gamma,cospars.sigma8,kvals,cospars.ns);


[delta_vals_tmp, z_vals_tmp] = growth_wconst(cospars.omega_m,cospars.omega_de,cospars.w0,1000);
zvals = 0.00000001:0.2:2;
deltas = interp1(z_vals_tmp,delta_vals_tmp,zvals);
deltas = deltas/max(deltas);

M_vals = logspace(10,15,200);


figure(1); clf; makeplotslide;

% Now cycle through different LWDM cosmologies:
conc = zeros(length(M_vals),length(zvals),length(m_vals));
for int = 1:length(m_vals)

    cospars.m_wdm = m_vals(int);

    pkwdm = wdm_transfn_pars(pkvals,kvals,cospars);

    Msc = mascale(cospars,zvals,kvals,pkwdm);
    [rvs_vals theta_v conc(:,:,int)] = haloparams(cospars,Msc,M_vals,zvals);

    semilogx(M_vals,conc(:,1,int),cols(int)); hold on

end

makeplotslide;
xlabel('M [M_o/h]'); ylabel('c(M)');

legents = genleg(m_vals/1000,'keV');
legents{1} = '\LambdaCDM';
legend(legents);
axis([1e10 1e15 2 50]);


return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(2);

rtries = 0.05:0.05:10; %Mpc/h
for zint = 1:length(zvals)

for int = 1:length(rtries)
sigmas(int) = sigmar_frompk(kvals,deltas(zint)*pkvals,rtries(int));
end

plot(rtries,sigmas/deltas(zint)); hold on;

rstar(zint) = interp1(sigmas/deltas(zint),rtries,1.686/1);

end

plot(rtries,sigmas/deltas(zint),'r');
plot(rtries,1.686*ones(size(rtries)),'r');
xlabel('R [Mpc/h]'); ylabel('\sigma(R,z)');