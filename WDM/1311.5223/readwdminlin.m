% Gnuplot is too basic, so I need to do my plots in 
%   Octave/Matlab
%
%	Reading in the results produced by CLASS.
%
%	version 1
%	22.06.2012 KMarkovic

pth = '';

% Set the file names of data to plot:
fileC	=   'planck_pk.dat';
fileW1	=   'lwdm2000_pk.dat';
fileW2	=   'lwdm1000_pk.dat';
fileW3	=   'lwdm0500_pk.dat';

% Read in LCDM
[dataC headerC] = myread([pth fileC],4,2,'#');
k_vals = dataC(:,1); CPk_vals = dataC(:,2);
clear dataC headerC;

% Read in and interpolate nuLCDM
[tmp headerW1] = myread([pth fileW1],4,2,'#');
W1_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headernu;

% Read in and interpolate LWDM
[tmp headerW2] = myread([pth fileW2],4,2,'#');
W2_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headerW;

% Read in and interpolatenu LWDM
[tmp headerW3] = myread([pth fileW3],4,2,'#');
W3_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headerWnu;