function wdmcmb
%   parent: myread_demo, readPlanck
%
%   25.06.2010 KM
%   v?.? 14.06.2013 KM - in terms of functions
%   v?.? 14.06.2013 KM - WDM

clear; clf

makeplotslide;

nheads = 7;
myform = 2;
%[planck hdr] = myread('planck_cl.dat',nheads,myform,'#');
hdm1 = myread('lhdm05_cl.dat',nheads,myform,'#');
hdm2 = myread('lhdm10_cl.dat',nheads,myform,'#');

%plot(planck.l,planck.TT,'k'); hold on
plot(hdm2.l,hdm2.TT,'r'); hold on
plot(hdm1.l,hdm1.TT,'m')
%plot(hdm2(:,1),hdm2(:,2),'r'); hold on
%plot(hdm1(:,1),hdm1(:,2),'m')
PlotPlanck('k',gcf);

legend('LHDM, m = 10 eV','LHDM, m = 5 eV','LCDM','Planck data');

%title(['l = 3000 is at k = ' num2str(3000/D_wconst(0,1000,0.3,0.7,-1)/3000*0.7)])
% n.b. CMB is at 10 Gpc approximately

axis([0 2000 5e-12 8.5e-10]);
%set(gca,'fontsize',12)
xlabel('l'); ylabel('C_l (10^-^1^0)');

makeplotslide;

%print('/Users/katarina/Desktop/Research/WDM/2013_Lahav_Abdalla/plots/HDM-CMB.eps','-depsc')
%print('/Users/katarina/Desktop/Research/WDM/2013_Review/Paper/Figs/Cl_hdm.eps','-depsc')

ylabe = scell2mat(get(gca,'yticklabel'));
set(gca,'yticklabel',mat2scell(ylabe*1e10));
set(gca,'yaxislocation','right');

endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [clspl pldt] = PlotPlanck(color,figno)
nheads = 7;
myform = 2;

filename = 'planck_cl.dat';
[planck hdr] = myread(filename,nheads,myform,'#');

figure(figno)
%clspl = plot(planck(:,1),planck(:,2),color); hold on
clspl = plot(planck.l,planck.TT,color); hold on


filename1 = 'Planck_COM_PowerSpect_CMB_R1.10_t1.txt';
header1 = {'l';'TT';'errup';'errdown'};
planckdat = myread(filename1,0,4,header1);

figure(figno)
plot(planckdat.l,planckdat.TT/10^12/(2.726)^2,[color '.']);
errorbar(planckdat.l,planckdat.TT/10^12/(2.726)^2,planckdat.errdown/10^12/(2.726)^2,planckdat.errup/10^12/(2.726)^2,[color '.']);


filename2 = 'Planck_COM_PowerSpect_CMB_R1.10_t2.txt';
header2 = {'l';'lmin';'lmax';'TT';'err'};
planckdat2 = myread(filename2,0,5,header2);

figure(figno)
pldt = plot(planckdat2.l(1),planckdat2.TT(1)/10^12/(2.726)^2,[color '.']);
errorbar(planckdat2.l,planckdat2.TT/10^12/(2.726)^2,planckdat2.err/10^12/(2.726)^2,[color '.']);

endfunction





%%%%%%%%%%%%%%%%% TRASH

%nheads = 7;
%myform = 2;
%%[planck hdr] = myread('planck_cl.dat',nheads,myform,'#');
%wdm1 = myread('lwdm1000_cl.dat',nheads,myform,'#');
%wdm2 = myread('lwdm2000_cl.dat',nheads,myform,'#');
%
%plot(planck.l,planck.TT./planck.TT,'k'); hold on
%plot(wdm1.l,planck.TT./wdm1.TT,'r')
%plot(wdm2.l,planck.TT./wdm2.TT,'m')
%
%axis([0 2000 5e-12 8.5e-10]);
%set(gca,'fontsize',14)
%legend('1keV','2kev')
%
%makeplotnice
%
%%print('WDM-CMB.eps','-depsc')
%
%endfunction