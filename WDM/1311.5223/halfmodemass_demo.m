% 05.07.2013 KMarkovic
% a la Schneider et al 2012 Fig.1
% Reproduced exactly - done!

clear; clf; %makeplotslide;

cospars = setPlanckpars;
cospars.omega_wdm = cospars.omega_dm;

mu = 1.12;

m_vals = logspace(1,4,50);

[Mhm Rhm khm Mfs] = halfmodemass(cospars,m_vals,mu);

clf;

%makeplotslide;

semilogy(m_vals/1000,Mhm,'k'); hold on
semilogy(m_vals/1000,Mfs,'b');
axis([0.06 10 5e2 5e14]);

legend('half-mode halo mass, M_h_m','free-streaming halo mass, M_f_s');
ylabel('M_x (M_o/h)'); xlabel('m_w_d_m (keV)');

%makeplotnice;
