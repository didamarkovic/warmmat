% mama: 'massfns_demo_wdm'
% 26/08/08 KM

clear;
makeplotslide;

simsbool = 0;

z_vals = 0;

cospars = setPlanckpars;
cospars.omega_wdm = cospars.omega_dm;

omega_m = cospars.omega_m;
omega_de = cospars.omega_de;
w = cospars.w0;
omega_dm = cospars.omega_dm; omega_wdm = cospars.omega_wdm;
omega_b = cospars.omega_b;
h = cospars.h;
sigma8 = cospars.sigma8;
%nu_power = 1.2; % As Bode,Ostriker&Turok 0010389v3

m_dm = [1e20 200 500 1000 7000 10000];
nu_power = 1.12;
alpha = 0.6;

k_vals = logspace(-4,4,500);
Gamma = exp(-2*omega_b*h)*omega_m*h;
pkCDM = pkf(Gamma,sigma8,k_vals);

M_vals = logspace(0,16,100);

cols = {'k' 'r' 'm' 'g' 'c' 'b'};

clf; %makeplotslide;
for int = 1:length(m_dm)

    [tf a pk(int,:)] = wdm_transfn(m_dm(int),omega_wdm,h,k_vals,nu_power,pkCDM,1,sigma8);
    [Mhm Rhm khm Mfs] = halfmodemass(cospars,m_dm(int),nu_power);

    dndm(int,:) = massfns_slb(M_vals,z_vals,omega_b,omega_m,omega_de,w,h,...
                                                  sigma8,k_vals,pk(int,:));

    dndm_wdm(int,:) = dndm(int,:) .* (Mhm./M_vals+1).^(-alpha);
    
    if int~=1
        pl(int-1) = loglog(M_vals,M_vals.*dndm(int,:),[cols{int} ':']); hold on
        wl(int-1) = loglog(M_vals,M_vals.*dndm_wdm(int,:),cols{int}); hold on

        plot(Mhm,Mhm*interp1(M_vals,dndm_wdm(int,:),Mhm),[cols{int} 'x']);
        plot(Mfs,Mfs*interp1(M_vals,dndm_wdm(int,:),Mfs),[cols{int} '*']);
    end
end
pl(int) = semilogx(M_vals,M_vals.*dndm(1,:),[cols{1} ':']);
wl(int) = semilogx(M_vals,M_vals.*dndm_wdm(1,:),cols{1});



%%% Zavala et al sims

if simsbool
    [cdmdat head] = myread('dndm.txt',1);
    [wdmdat head2] = myread('dndm2.txt',1);

    wl(int+1) = loglog(cdmdat.M_vals,cdmdat.dndm_vals,'ko');
    wl(int+2) = loglog(wdmdat.M_vals,wdmdat.dndm_vals,'go');
end


%title('mass functions for WDM: Press-Schechter (dots) and Schneider et al. 2012')
xlabel('M (M_oh^{-1})')
ylabel('dn(M,z)/dlnM')
if simsbool
    legend(wl,'0.2 keV','0.5 keV','1 keV','7 keV','10 keV','CDM','Zavala sims: CDM','Zavala sims: 1 keV',3)
else
    legend(wl,'0.2 keV','0.5 keV','1 keV','7 keV','10 keV','CDM',3)
end
text (1e6, 1e4,'* is M_f_s, X is M_h_m','fontsize',15);
axis([1e2 1e15 1e-6 1e5])
%makeplotslide;
makeplotnice;
set(pl,'linewidth',1.5)

%saveas(gcf,'dndM_WDM_3','bmp')

