%  
% parent: figureC, 01.08.2013 KM
% grandparent: lenshalo_tomo_demo_vielfit, 04.07.2011 KM
% greatgrandparent: lenshalo_tomo_demo, 15.10.09 KM


clear; 

tic


%% Set up cosmology:
cospars = setPlanckpars;
cospars.omega_wdm = cospars.omega_dm;

mvals = [0 1000 500];


%% Set up model
nlmod = 'vielfit';
C_l_ij = NaN;


%% Set up plotting
sty = '-';
col = 'bcg';
forleg=''; forlegw = '';
fontsz = 15;
figure(1); figure(2);
makeplotslide(1); makeplotslide(2);

if strcmp(sty,'-') clfall; end


%% Set up survey
dz = 0.1; z_vals = 0.0001:dz:5.0001;

l_vals = 2*logspace(2,5,20);
k_vals = logspace(-4,4,1000);

nzpars = setnzpars([8 0.5],{'nbin','zsmin',nlmod});


% Get the galaxy distribution:
tmp = min(z_vals):0.01:max(z_vals);
[n_z_mat niz] = get_nofz(nzpars,tmp);

% Bin the galaxy distribution
bincents = 1:nzpars.nbin;
for ii = 1:nzpars.nbin
    bincents(ii) = tmp(max(n_z_mat(:,ii))==n_z_mat(:,ii));
end

ib = [7 7 8]; jb = [7 8 8];
zi = bincents(ib);
zj = bincents(jb);



%% Cycle over m_WDM:
for mm = 1:length(mvals)


    cospars.m_wdm = mvals(mm); 

    % Save CDM
    if mm==2
        previous_C_l_ij = C_l_ij;
    end
    
    [C_l_ij tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);

    
    % Ratios:
    if mm~=1
         % Within 1%
        ra = (C_l_ij)./ (previous_C_l_ij);
        ratio = (C_l_ij+noise)./ (previous_C_l_ij+noise);
        ns = - ra .* noise./ (previous_C_l_ij + noise);

        % Work out the error bars for Euclid
        CC = covmat(l_vals,C_l_ij+noise,nzpars.fsky);
    end

    
    toc
    
    
    %% Plotting
    
    if mm~=1
        
        figure(mm-1);
        
        semilogx(1:1e4:1e5,ones(size((1:1e4:1e5))),'k--'); hold on

        [Mfs Rfs kfs] = freestreaming_comov(cospars);


        % Cycle over bins
        for ii=1:length(ib)
            
            errors = squeeze(sqrt(CC(ib(ii),jb(ii),ib(ii),jb(ii),:)));
            p_spectr = squeeze(previous_C_l_ij(ib(ii),jb(ii),:));
            spectr = squeeze(C_l_ij(ib(ii),jb(ii),:));
            nois = squeeze(noise(ib(ii),jb(ii),:));
            
            if strcmp(sty,'-')&&0
                r(ii) = errorbar(l_vals,squeeze(ratio(ib(ii),jb(ii),:)),errors./spectr,[col(ii) sty]);
            else
                r(ii) = semilogx(l_vals,squeeze(ratio(ib(ii),jb(ii),:)),[col(ii) sty]);
            end
            
            forleg{ii} = ['z = (' num2str(round2dp(zi(ii),1),'%1.1f') ',' num2str(round2dp(zj(ii),1),'%1.1f') ')'];
            if strcmp(sty,'-') && mm==2
				legend(r,forleg,'location','south');
			end
            axis([l_vals(1) l_vals(end) 0.92 1.02])
            ylabel('C_\kappa_,_i_j^W^D^M/C_\kappa_,_i_j^C^D^M')
            xlabel('multipole l')

            text(300,1.01,[num2str(mvals(mm)/1000) ' keV'],'fontsize',fontsz)

			lfs = findl(zi(ii),kfs);
			line([lfs lfs],[0.5 1.5],'color',col(ii),'linestyle',':');


        end %for:ib

        
    end %if:mm

    
end %for:mm

makeplotslide(1); makeplotslide(2);




%print(lens.eps,'depsc')






