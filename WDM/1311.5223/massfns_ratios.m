% mama: 'massfns_demo_wdm'
% 26/08/08 KM

clear;
makeplotslide;

z_vals = 0;

cospars = setPlanckpars;
cospars.omega_wdm = cospars.omega_dm;

omega_m = cospars.omega_m;
omega_de = cospars.omega_de;
w = cospars.w0;
omega_dm = cospars.omega_dm; omega_wdm = cospars.omega_wdm;
omega_b = cospars.omega_b;
h = cospars.h;
sigma8 = cospars.sigma8;
%nu_power = 1.2; % As Bode,Ostriker&Turok 0010389v3

m_dm = [1e20 200 500 1000 7000 10000];
nu_power = 1.12;
alpha = 0.6;

k_vals = logspace(-4,4,500);
Gamma = exp(-2*omega_b*h)*omega_m*h;
pkCDM = pkf(Gamma,sigma8,k_vals);

M_vals = logspace(0,16,100);

cols = {'k' 'r' 'm' 'g' 'c' 'b'};

clf;
for int = 1:length(m_dm)

    [tf a pk(int,:)] = wdm_transfn(m_dm(int),omega_wdm,h,k_vals,nu_power,pkCDM,1,sigma8);
    [Mhm Rhm khm Mfs] = halfmodemass(cospars,m_dm(int),nu_power);

    dndm(int,:) = massfns_slb(M_vals,z_vals,omega_b,omega_m,omega_de,w,h,...
                          sigma8,k_vals,pk(int,:));

    dndm_wdm(int,:) = dndm(int,:) .* (Mhm./M_vals+1).^(-alpha);

    if int ~= 1
        pl(int-1) = semilogx(M_vals/Mhm,dndm(int,:)./dndm(1,:),[cols{int} '--']); hold on
        wl(int-1) = semilogx(M_vals/Mhm,dndm_wdm(int,:)./dndm_wdm(1,:),cols{int});


        plot(1,interp1(M_vals,dndm_wdm(int,:)./dndm_wdm(1,:),Mhm),[cols{int} 'x']);
    end
end
pl(int) = semilogx(M_vals/Mhm,dndm(1,:)./dndm(1,:),'y--');
wl(int) = semilogx(M_vals/Mhm,dndm_wdm(1,:)./dndm_wdm(1,:),[cols{1} '--']);

%title('mass function ratios: pure Press-Schechter (yellow) vs. Schneider et al. 2012')
xlabel('M/M_h_m')
ylabel('ratio')
legend(wl, 'm_w_d_m = 0.2 keV','m_w_d_m = 0.5 keV','m_w_d_m = 1 keV','m_w_d_m = 7 keV','m_w_d_m = 10 keV','CDM','location','southeast')
%axis([1e2 1e16 0 1])
axis([1e-2 1e3 0.1 1.05])
makeplotslide;
set(pl,'linewidth',0.5);
set(gca,'yaxislocation','right');

%saveas(gcf,'dndM_WDM_3','bmp')