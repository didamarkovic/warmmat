% plotting bias
%
% version 1
% 27.01.2010 KMarkovic
% version 2
% 29.07.2013 KM

clear

normbool = 1; % Do we normalise the bias?

zzerobool = 1;

if zzerobool
	zval = 0;
	clf;
else
	zval = 2;
	error('This makes no sense!');
end

M_vals = logspace(0,16,50);
dlogM = abs(log(M_vals(2)) - log(M_vals(1)));
dM = dlogM * M_vals; % [M_sol]


% CDM
cospars = setPlanckpars;
consts

% lin mps
k_vals = logspace(-4,4,1000);
[pk_vals pk_grown pk_lin_0] = pk_nlin_get(cospars,k_vals,zval,'smith2');

% dndm
[dndm tmp1 M_scale sigma] = massfns_slb(M_vals*cospars.h,zval,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_0); % [h^4/Mpc^3/M_sol][same][M_sol/h][unitless]
clear tmp1 tmp2

% Okay lets try out this weird new delta_c2 (from Henry 2000):
delta_c = delta_c_alt(zval,cospars.omega_m);  % [unitless]

% Bias:
a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2
rho_mean = rho_crit_0_Mpc * cospars.omega_m;
nu = (delta_c ./ sigma).^2; % [unitless]

% From Seljak 0001493v1 (after equn 9):
bias_unnorm =  1  +  ( nu - 1 )./delta_c +  2*p./( 1 + (a*nu).^p ); %[unitless]

% Need to normalise the bias:
if normbool
    norm = sum( dndm' .* dM .* M_vals   / rho_mean .* bias_unnorm' );
else
    norm = 1;
end
bias = bias_unnorm/norm; % [unitless]
clear norm

%conc
a = 10.3 .* ( ( 1 + zval ).^(-0.3) );
b = -0.24 .* ( ( 1 + zval ).^(-0.3) );
conc = ( a.* ( M_vals ./ M_scale ).^b)' ; % no units


% WDM
cospars_wdm = cospars;
cospars_wdm.omega_wdm = cospars.omega_dm;
cospars_wdm.m_wdm = 100; %hotter

% lin mps
[pk_vals pk_lin_grown pk_lin_wdm_0] = pk_nlin_get(cospars_wdm,k_vals,zval,'smith2');

% dndm
[dndm_wdm tmp1 M_scale_wdm sigma_wdm] = massfns_slb(M_vals*cospars.h,zval,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_wdm_0); % [h^4/Mpc^3/M_sol][same][M_sol/h][unitless]
clear tmp1 tmp2

[Mhm tmp tmp Mfs] = halfmodemass(cospars_wdm);

% Bias:
nu_wdm = (delta_c ./ sigma_wdm).^2; % [unitless]

% From Seljak 0001493v1 (after equn 9):
bias_unnorm_wdm =  1  +  ( nu_wdm - 1 )./delta_c +  2*p./( 1 + (a*nu_wdm).^p ); %[unitless]

% Need to normalise the bias:
if normbool
    norm_wdm = sum( dndm_wdm' .* dM .* M_vals   / rho_mean .* bias_unnorm_wdm' );
else
    norm_wdm = 1;
end
bias_wdm = bias_unnorm_wdm/norm_wdm; % [unitless]
clear norm

%conc
conc_wdm = ( a.* ( M_vals ./ M_scale_wdm ).^b)' ; % no units


% WDMish
cospars_wdm2 = cospars_wdm;
cospars_wdm2.m_wdm = 1000; %cooler

% lin mps
[pk_vals pk_lin_grown pk_lin_wdm2_0] = pk_nlin_get(cospars_wdm2,k_vals,zval,'smith2');

% dndm
[dndm_wdm2 tmp1 M_scale_wdm2 sigma_wdm2] = massfns_slb(M_vals*cospars.h,zval,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_wdm2_0); % [h^4/Mpc^3/M_sol][same][M_sol/h][unitless]
clear tmp1 tmp2

[Mhm2 tmp tmp Mfs2] = halfmodemass(cospars_wdm2);

% Bias:
nu_wdm2 = (delta_c ./ sigma_wdm2).^2; % [unitless]

% From Seljak 0001493v1 (after equn 9):
bias_unnorm_wdm2 =  1  +  ( nu_wdm2 - 1 )./delta_c +  2*p./( 1 + (a*nu_wdm2).^p ); %[unitless]

% Need to normalise the bias:
if normbool
    norm_wdm2 = sum( dndm_wdm2' .* dM .* M_vals   / rho_mean .* bias_unnorm_wdm2' );
else
    norm_wdm2 = 1;
end
bias_wdm2 = bias_unnorm_wdm2/norm_wdm2; % [unitless]
clear norm

%conc
conc_wdm2 = ( a.* ( M_vals ./ M_scale_wdm2 ).^b)' ; % no units


% Plotting
% figure
% loglog(k_vals,pk_lin_0,'k'); hold on
% loglog(k_vals,pk_lin_wdm_0,'r')
% loglog(k_vals,pk_lin_wdm2_0,'g--')

% SIGMA:
figure(1); %clf;
%a=semilogx(M_vals,sigma,'k'); hold on
%b=semilogx(M_vals,sigma_wdm,'r--');
%c=semilogx(M_vals,sigma_wdm2,'g--');

makeplotslide;

if zzerobool
	b=loglog(M_vals,1./nu_wdm,'r'); hold on
	c=loglog(M_vals,1./nu_wdm2,'g');
	a=loglog(M_vals,1./nu,'k');
else
	loglog(M_vals,1./nu_wdm,'r--'); hold on
	loglog(M_vals,1./nu_wdm2,'g--');
	loglog(M_vals,1./nu,'k--');
end

line([1 1e16],[1 1],'linewidth',0.5,'linestyle',':');
xlabel('M (M_o/h)')
ylabel(['\sigma(R,z)/\delta_c_o_l_l(z)'])

if zzerobool
	legend([a b c],'CDM',[num2str(cospars_wdm.m_wdm/1000) ' keV'],[num2str(cospars_wdm2.m_wdm/1000) ' keV'])
end
axis([1e5 max(M_vals) 1/50 50])
makeplotnice
%title('solid: z=0, dashed: z=2')

line([Mfs Mfs],[1e-2 1/nu_wdm(isclose(M_vals,Mfs))],'color','r','linestyle',':','linewidth',1.5);
line([Mfs2 Mfs2],[1e-2 1/nu_wdm2(isclose(M_vals,Mfs2))],'color','g','linestyle',':','linewidth',1.5);

line([Mhm Mhm],[1e-2 1/nu_wdm(isclose(M_vals,Mhm))],'color','r','linestyle','-.','linewidth',1.5);
line([Mhm2 Mhm2],[1e-2 1/nu_wdm2(isclose(M_vals,Mhm2))],'color','g','linestyle','-.','linewidth',1.5);

text(Mhm2*0.15,2,'M_h_m','fontsize',15,'color','g');
text(Mfs2*0.2,2,'M_f_s','fontsize',15,'color','g');

text(Mhm*0.15,0.05,'M_h_m','fontsize',15,'color','r');
text(Mfs*0.2,0.05,'M_f_s','fontsize',15,'color','r');

makeplotslide;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
return

% BIAS:
figure(2); clf;
d=semilogx(M_vals,(bias_wdm-bias)./bias,'r'); hold on
e=semilogx(M_vals,(bias_wdm2-bias)./bias,'g--');
semilogx(M_vals,zeros(size(M_vals)),'k:');
xlabel('M[M_o]')
ylabel('(b_w-b_c)/b_c')
legend('1keV','2keV')
%axis([M_vals(1) max(M_vals) -0.15 0.35])
axis([M_vals(1) max(M_vals) min((bias_wdm2-bias)./bias)-0.1 max((bias_wdm-bias)./bias)+0.1])
%title('Bias ratios - normalised (thick) & unnormalised (thin)')
makeplotnice

figure(3); clf;
f=semilogx(M_vals,bias,'k'); hold on
g=semilogx(M_vals,bias_wdm,'r');
h=semilogx(M_vals,bias_wdm2,'g--');
%f=loglog(M_vals,bias,'k'); hold on
%g=loglog(M_vals,bias_wdm,'r');
%h=loglog(M_vals,bias_wdm2,'g--');
xlabel('M[M_o]')
ylabel('bias')
legend('CDM','1keV','2keV','location','north')
axis([1e5 max(M_vals) 0.0 max(bias)])
% figure % This is the same because it is bigger than the free-streaming mass!
% semilogx(M_vals,conc,'b'); hold on
% semilogx(M_vals,conc_wdm,'r')
% semilogx(M_vals,conc_wdm2,'g--')
set([a b c d e f g h],'LineWidth',1.5)
%figure(1)
%saveas(gcf,'sigma_bias_demo_talkfig1','pdf')
%figure(2)
%saveas(gcf,'sigma_bias_demo_talkfig2','pdf')
makeplotslide;

%samexaxis('xmt','on','ytac','join','yld',1)