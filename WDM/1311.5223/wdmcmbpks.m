%   parent: myread_demo, readPlanck, wdmcmb
%
%   25.06.2010 KM
%   v?.? 14.06.2013 KM - in terms of functions
%   v?.? 14.06.2013 KM - WDM
%   v?.? 05.07.2013 KM - Pks

clear; clf

makeplotslide;

nheads = 7;
myform = 2;
planck = myread('planck_pk.dat',nheads,myform,'#');
hdm1 = myread('lhdm05_pk.dat',nheads,myform,'#');
hdm2 = myread('lhdm10_pk.dat',nheads,myform,'#');

loglog(planck(:,1),planck(:,2),'k'); hold on
loglog(hdm2(:,1),hdm2(:,2),'r'); hold on
loglog(hdm1(:,1),hdm1(:,2),'m')

legend('LCDM','LHDM, m = 10 eV','LHDM, m = 5 eV');

axis([0.005 5 5 50000])
%set(gca,'fontsize',12)
xlabel('k (h/Mpc)'); ylabel('P_k (Mpc/h)^3');
%set(gca,'yaxislocation','right');

makeplotslide;

%print('/Users/katarina/Desktop/Research/WDM/2013_Review/Paper/Figs/Pk_hdm.eps','-depsc')