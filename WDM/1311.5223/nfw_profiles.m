% concentration is the mommy
%
% 29.07.2013 KMarkovic

clear

%m_vals = [Inf 10000 2000 1000 500 250];
%cols = 'kbcgrm';
m_vals = [250 500 1000 2000 10000 Inf];
cols = 'mrgcbk';

cospars = setPlanckpars;
consts;
rhom = cospars.omega_m * rho_crit_0_Mpc;

Mvir = 1e12;
zval = 0;

figure(1); clf; makeplotslide;

% Only need this for CDM, since Mscale doesn't change for WDM:
Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
k_vals = logspace(-4,4,1000);
pk_lin = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns);

% Now cycle through different LWDM cosmologies:
for int = 1:length(m_vals)

    cospars.m_wdm = m_vals(int);

	[M_scale sigma nu] = mascale(cospars,zval,k_vals,pk_lin);

	[rv_vals tmp cvir] = haloparams(cospars,M_scale,Mvir,zval);

	[rhor rv ers] = nfw(cvir,Mvir,cospars);

	h(int) = semilogx(ers,log10(rhor/rhom),cols(int)); hold on
	line([ers(isclose(ers,1/cvir)) ers(isclose(ers,1/cvir))],[-10 log10(rhor(isclose(ers,1/cvir))/rhom)],'linestyle','--','linewidth',0.5,'color',cols(int));

end

makeplotslide;

axis([0.02 max(ers) 1.01 5.99]);
xlabel('r/R_v_i_r'); ylabel('log_1_0(\rho_N_F_W(r)/\rho_m)');

if Mvir==1e12
    legents = genleg(m_vals/1000,'keV');
    legents{end} = '\LambdaCDM';
    legend(h,legents);
end

expon = ['10^' num2str(floor(log10(Mvir)/10)) '^' num2str(log10(Mvir)-floor(log10(Mvir)/10)*10)];
text(0.3,1.5,[expon ' M_o'],'fontsize',20);
