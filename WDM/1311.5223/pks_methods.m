% figureA
%  Comparing P_k calculations for WDM. We plot the fit to simulations,
%  modified halo model, halofit and the linear spectrum. This we do for 2
%  different m_wdm and z.
%
% parent: compare_sims, 23.05.2011 KM
%
% 07.07.2011 KMarkovic

clear; clf; makeplotslide;


k_vals = logspace(-2,2,1000);
zval = 0;
mval = 1000;

cospars = setPlanckpars;
%cospars.h = 1;


%%% CDM

%% Halo Model
[pk_nlin_CDM pk_lin_CDM pk_C pk_P] = pk_nlhalo(k_vals,NaN,cospars,zval);
loglog(k_vals,pk_nlin_CDM,'k'); hold on
loglog(k_vals,pk_C,'c:');
loglog(k_vals,pk_P,'c:');
%loglog(k_vals,pk_lin,'k:');
xlabel('k, wavenumber'); ylabel('P(k)');
axis([min(k_vals) max(k_vals) 0.01 2*max(pk_lin_CDM)]);

%% Halofit
[pknlin C2Wratio] = pk_nlin_readsmith(k_vals, zval, cospars, pk_nlin_CDM);
loglog(k_vals,pknlin,'k-.');


%%% WDM
cospars.m_wdm = mval; cospars.omega_wdm = cospars.omega_dm;

%% Halo Model
[pk_nlin pk_lin pk_C pk_P] = pk_nlhalo(k_vals,NaN,cospars,zval);
loglog(k_vals,pk_nlin,'r');
loglog(k_vals,pk_C,'r-.');
loglog(k_vals,pk_P,'r--');
%loglog(k_vals,pk_lin,'r:');
xlabel('k, wavenumber'); ylabel('P(k)');
axis([min(k_vals) max(k_vals) 0.01 2*max(pk_lin)]);

%% Readsmith
[pknlin C2Wratio] = pk_nlin_readsmith(k_vals, zval, cospars, pk_nlin_CDM);
loglog(k_vals,pknlin,'g');

%% Readsmith
[pk_vals pk_lin_grown pk_lin_0] = pk_nlin_get(cospars,k_vals,zval,'smith');
loglog(k_vals,pk_vals,'b--'); %Shiiit

makeplotslide