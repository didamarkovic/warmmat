% figureA
%  Comparing P_k calculations for WDM. We plot the fit to simulations,
%  modified halo model, halofit and the linear spectrum. This we do for 2
%  different m_wdm and z.
%
% parent: compare_sims, 23.05.2011 KM
%
% 07.07.2011 KMarkovic

clear; clf; makeplotslide;

zval = 0;
mval = 1000;


k_vals = logspace(-1,2.9,1000);

cospars = setPlanckpars;

dosims = 0; % Should use setEUCLIDpars



%cospars.h = 1; % <---- THIS MESSED IT UP QUITE A BIT!!!!!


%%% CDM

%% Halo Model
[pk_nlin_c pk_lin_c pk_C_c pk_P_c] = pk_nlhalo(k_vals,NaN,cospars,zval);
[pk_nlin_c_3 pk_lin_c_3 pk_C_c_3 pk_P_c_3] = pk_nlhalo_v3(k_vals,NaN,cospars,zval);

%% Readsmith
kvls = logspace(-2,2,1000);
[pknlin_c C2Cratio] = pk_nlin_readsmith(kvls, zval, cospars, pk_nlin_c);
semilogx(kvls,C2Cratio*ones(size(kvls)),'k:'); hold on
xlabel('k (h/Mpc)'); ylabel('ratio');
axis([min(k_vals) max(k_vals) 0.0 1.1]);


%%% WDM
cospars.m_wdm = mval; cospars.omega_wdm = cospars.omega_dm;

%% Halo Model
[pk_nl pk_l pk_C pk_P] = pk_nlhalo(k_vals,NaN,cospars,zval);
[pk_nl_3 pk_l_3 pk_C_3 pk_P_3] = pk_nlhalo_v3(k_vals,NaN,cospars,zval);
wp = semilogx(k_vals,pk_nl./pk_nlin_c,'r');
semilogx(k_vals,pk_C./pk_nlin_c,'r:');
semilogx(k_vals,pk_P./pk_nlin_c,'r:');
%semilogx(k_vals,pk_l./pk_lin_c,'r-.');
hp = semilogx(k_vals,pk_nl_3./pk_nlin_c_3,'g-.');

%% Smithread
[pknlin C2Wratio] = pk_nlin_readsmith(kvls, zval, cospars, pk_nlin_c);
fp = semilogx(kvls,C2Wratio,'c-.');

%% Simsfit
[pknl_wdm tks2] = pk_nlin_simsfit(k_vals,pknlin_c,zval,cospars);
sp = semilogx(k_vals,tks2,'m--'); hold on


%% Plotting fixes
if mval==1000 && zval==0
	leg = legend([wp hp fp sp], 'new halo WDM','old halo WDM','Smith \& Markovic (2012)','fit to sims');
	set(leg,'location','northeast')
end
axis([8e-2 0.9e2 0.61 1.16]);

text(1,0.72,['m_w_d_m = ' num2str(cospars.m_wdm/1000) ' keV'],'fontsize',20);
text(1,0.75,['z = ' num2str(zval)],'fontsize',20);

% BUT OF COURSE IT SHOULD'T BE LIKE READSMITH, SINCE I USED THE SCHNEIDER ET AL.!!!


%%%%%% SIMS
if dosims
	z_snaps = [10 5 3 2.5 2 1.8 1.6 1.4 1.2 1 0.9:-0.1:0];
	redshiftno = isclose(z_snaps,zval)-1;

	%path = '/Users/katarina/Desktop/Research/WDM/2011_Vieletal/Matteo-TABLES/';
	path = '/Users/dida/Work/Desktop/Research/WDM/2011_Vieletal/Matteo-TABLES/';

	simsize = '25_512';

	tbin = 30;
	mmod = 15;

	nulls = '000';
	extrazeros = nulls(1:(3-length(num2str(redshiftno))));

	%model = cospars.m_wdm /1000;


	%% WDM
	filename = [path,'WDM1','_',simsize,'/powerspec_',extrazeros,num2str(redshiftno),'.txt'];

	[data header] = ppowerread(filename);
	[kvalsw pkvalsw noise] = ppowergetspec(data,tbin,mmod);


	%% CDM
	filename = [path,'LCDM','_',simsize,'/powerspec_',extrazeros,num2str(redshiftno),'.txt'];

	[data header] = ppowerread(filename);
	[kvals pkvals noise] = ppowergetspec(data,tbin,mmod);

	semilogx(kvals.B,pkvalsw.B./pkvals.B,'m*');
end

makeplotslide;