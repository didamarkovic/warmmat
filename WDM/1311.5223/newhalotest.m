% Want to get the concentration for CDM and WDM haloes from
%   Schneider st al. 2012
%
% 15.07.2013 KMarkovic

cospars = setPlanckpars; %cospars.m_wdm = 100;
consts;
Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;

kvals = logspace(-4,4,1000);
pkvals = pkf(Gamma,cospars.sigma8,kvals,cospars.ns);

pkvals = wdm_transfn_pars(pkvals,kvals,cospars);

%sigma8=sigmar_frompk(kvals,pkvals,8)

M_vals = logspace(10,15,100);
zl_vals = 0.0000001;
[M_scale sigma nu] = mascale(cospars,zl_vals,kvals,pkvals);
[rv_vals theta_v conc rho_s n00] = haloparams(cospars,M_scale,M_vals,zl_vals);

%rv_new = virial(M_vals,cospars)';
%fprintf('\tvirial radius ratio = %g\n',sum(rv_new./rv_vals)/length(M_vals));

conc = conc';
[densnfw rvs rads] = nfw(conc,M_vals/cospars.h,cospars);

thisone = isclose(M_vals,9e11);

figure(1); clf;
denserat = densnfw(:,thisone)/rho_crit_0_Mpc/cospars.omega_dm;
loglog(rads/rv_vals(thisone),denserat); hold on
%plot(rv_vals(thisone),denserat(isclose(rads,rv_vals(thisone))),'rx');
%plot(rv_new(thisone),denserat(isclose(rads,rv_new(thisone))),'g+');
%plot(rvs(thisone),denserat(isclose(rads,rvs(thisone))),'co');
axis([0.05, 1.05 1e1 1e5]);
xlabel('r/r_v'); ylabel('\rho(r)/\rho_c');
legend(['for halo mass = ' num2str(M_vals(thisone))]);
title('NFW density profile');
makeplotnice


figure(2); %clf;
loglog(M_vals,conc); hold on
xlabel('M [M_o]'); ylabel('c(r)');
legend(['for halo mass = ' num2str(M_vals(thisone))]);
axis([5e10 2e15 3 35]);
title('concentration with Schneider 1keV WDM suppression');
makeplotnice



%% Now WDM
[Mhm Rhm khm Mfs alpha] = halfmodemass(cospars,1000);

%% Schneider
gamma1 = 15; gamma2 = 0.3;
concwdm = conc .* (1 + gamma1 * Mhm./M_vals).^(-gamma2);

loglog(M_vals,concwdm,'r');