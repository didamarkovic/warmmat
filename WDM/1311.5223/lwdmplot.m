% Gnuplot is too basic, so I need to do my plots in 
%   Octave/Matlab
%
%	Plotting the matter power spectra.
%	Need to run 'readin' first!
%
%	version 2 (1 was for Gnuplot)
%	22.06.2012 KMarkovic

if ~exist('W1_vals','var')
	fprintf('You need to run a readin[...].m script first, dimwit!\n');
	break
end

% Find the free-streaming scales:
cospars = setPlanckpars;
[tmp tmp kfs] = freestreaming_comov(cospars,[2000 1000 500]);
kfs = kfs/10; %!!!!!!!!!!!!!!!!!

Pfs(1) = interp1(k_vals,W1_vals,kfs(1));
Pfs(2) = interp1(k_vals,W2_vals,kfs(2));
Pfs(3) = interp1(k_vals,W3_vals,kfs(3));

clf
figure(1); makeplotslide;

Cp = loglog(k_vals,CPk_vals,'k'); hold on

W1 = loglog(k_vals,W1_vals,'c'); 
plot([kfs(1) kfs(1)],[0.001 Pfs(1)],'--c')
%plot(kfs(1),Pfs(1),'co')

W2 = loglog(k_vals,W2_vals,'r');
plot([kfs(2) kfs(2)],[0.001 Pfs(2)],'--r')
%plot(kfs(2),Pfs(2),'ro')

W3 = loglog(k_vals,W3_vals,'m');
plot([kfs(3) kfs(3)],[0.001 Pfs(3)],'--m')
%plot(kfs(3),Pfs(3),'mo')


axis([0.005 k_vals(end) 3e-2 5e4]);
xlabel('k (h/Mpc)','fontsize',20); ylabel('P(k) (Mpc/h)^3','fontsize',20);
legend([Cp W1 W2 W3], '\LambdaCDM','\LambdaWCDM 2000eV','\LambdaWDM 1000eV','\LambdaWDM 500eV',3);

%set([Cp W1 W2 W3],'linewidth',2);
makeplotslide;

%text(0.0007, 0.003, '*CLASS*', 'Color', 'k','FontSize',20);