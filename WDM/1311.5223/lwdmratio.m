% Gnuplot is too basic, so I need to do my plots in 
%   Octave/Matlab
%
%	Plotting the matter power spectra ratios.
%	Need to run 'readin' first!
%
%	version 2 (1 was for Gnuplot)
%	22.06.2012 KMarkovic

if ~exist('k_vals','var')
	fprintf('You need to run a readin[...].m script first, dimwit!\n');
	break
end

% Find the free-streaming scales:
cospars = setPlanckpars;
[tmp tmp kfs] = freestreaming_comov(cospars,[2000 1000 500]);
kfs = kfs/10; %!!!!!!!!!!!!!!!!!

Pfs(1) = interp1(k_vals,W1_vals./CPk_vals,kfs(1));
Pfs(2) = interp1(k_vals,W2_vals./CPk_vals,kfs(2));
Pfs(3) = interp1(k_vals,W3_vals./CPk_vals,kfs(3));

clf
figure(1)

Cp = semilogx(k_vals,CPk_vals./CPk_vals,':k'); hold on

W1 = semilogx(k_vals,W1_vals./CPk_vals,'c');
plot([kfs(1) kfs(1)],[Pfs(1) 1],'--c')
%plot(kfs(1),Pfs(1),'co')

W2 = semilogx(k_vals,W2_vals./CPk_vals,'r');
plot([kfs(2) kfs(2)],[Pfs(2) 1],'--r')
%plot(kfs(2),Pfs(2),'ro')

W3 = semilogx(k_vals,W3_vals./CPk_vals,'m');
plot([kfs(3) kfs(3)],[Pfs(3) 1],'--m')
%plot(kfs(3),Pfs(3),'mo')


axis([0.002 k_vals(end) 0 1.05]);
xlabel('k (h/Mpc)','fontsize',20); ylabel('ratio','fontsize',20);
legend([Cp W1 W2 W3], '\LambdaCDM','\LambdaWCDM 2000eV','\LambdaWDM 1000eV','\LambdaWDM 500eV','location','south');
%legend('right');
set(gca,'yaxislocation','right');

% Styling for slides
set([Cp W1 W2 W3],'linewidth',2);
makeplotslide;

%text(0.0007, 0.003, '*CLASS*', 'Color', 'k','FontSize',20);