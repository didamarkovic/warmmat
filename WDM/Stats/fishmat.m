function [fmat errors corr] = fishmat(paramnames,dCldO_obj,cmat)

% Finds the Fisher matrix of the lensing power spectrum for the
% cosmological parameters initialised as a cell array of strings in
% paramnames. Equation from Takada&Jain 0310125v4.
%
% paramnames - cell array of strings with parameter names, e.g. 'omega_dm'
% dCldO_obj - object containing the differentials for each parameter, for
%             e.g. omega_dm: dCldO_obj.omega_dm = dcls_dparam...
% cmat - the 5D covariance matrix from covmat
%
% [fmat errors corrs] = fishmat(paramnames,dCldO_obj,cmat)
%
% OUTPUT - 2D matrix with size of length(paramnames) - fmat
%          vector of errors corresponding to the paramnames - errors
%          matrix of how correlated the different parameters are - corrs
%
% 03/08/08 KMarkovic

nbin = size(cmat(:,1,1,1,1),1);

nop = length(paramnames);
nol = size(cmat(1,1,1,1,:),5);

fish_tmp = zeros(nop,nop,nol);
%i = zeros(nbin*nbin,1); j=i; dC_vect=zeros(nbin*nbin,1,nol); % Initialize arrays

for pint = 1:nop

    
    dC_mat = getfield(dCldO_obj,paramnames{pint});

    % Let's deal with the 4D l-matrices Sarah's way
    
    % Turn the dCdO matrix into a vector:
    kbinz = 1;
    for ibin = 1:nbin
        for jbin = ibin:nbin % Changing this to just do the non-zero!!!!!!
            i_vect(kbinz) = ibin;
            j_vect(kbinz) = jbin;
            dC_vect(kbinz,pint,:) = dC_mat(ibin,jbin,:);
            kbinz = kbinz+1;
        end
    end
    kbinz=kbinz-1;
    
    % Turn 5D matrix into 3D matrix - l dimension is last:
    if ~exist('Cov','var')
        Cov = zeros(kbinz,kbinz,nol);
        for kint = 1:kbinz
            for lint = 1:kbinz
        tmp = cmat(i_vect(kint),j_vect(kint),i_vect(lint),j_vect(lint),:);
                Cov(kint,lint,:) = tmp;
            end
        end
    end
    
    % Now cycle through parameters and values of l:
    for lint = 1:nol
        covinv = inv(Cov(:,:,lint)); % Using pinv for dodgy zeros!
        
        %covinv = 1e15 * inv(1e15*Cov(:,:,lint));
        
        for pjnt = 1:pint
            
            tmp = dC_vect(:,pint,lint)'*covinv*dC_vect(:,pjnt,lint);
            
            fish_tmp(pint,pjnt,lint) = tmp;
                        
            if pint~=pjnt % Since have symmetry of Fisher matrix
                
                tmp = dC_vect(:,pjnt,lint)'*covinv*dC_vect(:,pint,lint);

                fish_tmp(pjnt,pint,lint) = tmp;
                
            end
            
        end
    end
    
    
end

fmat = sum(fish_tmp,3);

% Get errors on the parameters
if nargout >= 2
    %invfish = pinv(fmat);
    %invfish = inv(fmat);
    invfish = safeinv(fmat);
    %fprintf(1,'\tWarning: using pinv in fishmat!\n')
    %errors = diag(sqrt(abs(invfish))); % Oh, this is terrible!
    errors = diag(sqrt(invfish)); 
end

% How correlated are the parameters?
if nargout==3
    for alpha = 1:length(paramnames)
        for beta = 1:length(paramnames)
            corr(alpha,beta) = invfish(alpha,beta) / ...
                sqrt(invfish(alpha,alpha)*invfish(beta,beta));
        end
    end
end