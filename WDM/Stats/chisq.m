function [chisq probs P_lim] = chisq(CLS,nzpars,l_vals,nsigma,noise)
%CHISQ(CLS,cospars,nzpars,l_vals,nsigma)
%   This function calculates the chi squared and the corresponding
%   probability for a weak lensing power spectrum. CLS is a structure.
%   The field names should be 'fid' for the fiducial (or observed) 
%   spectrum and for the rest it should be the values of the parameter.
%   Note: spectra should have shot noise added already!!
%
%   version 1
%   Called lens_fish_tomo_demo_2.m
%   22.10.09 KMarkovic (modified SLB's script)
%   version 1.1
%   Changing the parameters a bit & making it into a script again.
%   14.04.2010 KM
%   version 1.2
%   Using 1/m. Finding chisq too! - lens_fish_demo_invm.m
%   16.04.2010 KM
%   version 2
%   writing it into a chisq function
%   30.04.2010 KM

if ~exist('noise','var'); noise = 0; end

%% FIDUCIAL

FIDCLS = CLS{1} + noise; % length(CLS)
    
%% Covariance Matrix

COV = covmat(l_vals,FIDCLS,nzpars.fsky);

%% WDMs

chisq = zeros(size(CLS)-[1 1]);
for pint = 2:length(CLS)
    
    THEOCLS = CLS{pint} + noise;
    
    index = 1;
    FIDCLS_vect = zeros(nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    THEOCLS_vect = zeros(nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    COV_tmp = zeros(nzpars.nbin*(nzpars.nbin+1)/2,nzpars.nbin,nzpars.nbin,length(l_vals));
    for ibin = 1:nzpars.nbin
        
        for jbin = ibin:nzpars.nbin
            
            FIDCLS_vect(index,:) = FIDCLS(ibin,jbin,:);
            THEOCLS_vect(index,:) = THEOCLS(ibin,jbin,:);
            
            COV_tmp(index,:,:,:) = COV(ibin,jbin,:,:,:);
            
            index = index + 1;
            
        end
    end
    
    index = 1;
    COV_2Dmat = zeros(nzpars.nbin*(nzpars.nbin+1)/2,nzpars.nbin*(nzpars.nbin+1)/2,length(l_vals));
    for ibin = 1:nzpars.nbin
        
        for jbin = ibin:nzpars.nbin
            
            COV_2Dmat(:,index,:) = COV_tmp(:,ibin,jbin,:);  
            
            index = index + 1;
            
        end
        
    end
    
    chitmp = zeros(length(l_vals),1);
    for int = 1:length(l_vals)
        
        dC = FIDCLS_vect(:,int) - THEOCLS_vect(:,int);        
        
        chitmp(int) = dC'/COV_2Dmat(:,:,int)*dC;
        
    end
    
    chisq(pint-1) = sum(chitmp);
    %chisq(pint) = sum(chitmp);
    
end

probs = exp(-chisq/2);
P_lim = exp(-nsigma^2/2);