function [figid axes] = fisherellipse(fmat2D,fidvect,nosig,nosteps,linsty,linwid,col);
%   ELLIPSE plots an ellipse from a 2D Fisher matrix.
% 
% figid = ELLIPSE(2Dfmat,fidvect,nosig,nosteps,linestyle,linewidth,fillcolor)
%
% default nosig is 1
% nosteps is how many steps in phi we want
% fidvect is the position of the fiducial model in the 2D parameter space
%
% This is done by diagonalising the matrix by finding a rotationa matrix 
% U which has the eigenvectors of matrics as columns. For this using 
% Matlab inbuilt function for eigenvectors.
%
% We also need the eigenvalues for the thickness of ellipse.
%
% Then the distance from centre of ellipse at angle phi until a point on
% the line is $r^2 = \frac{nosig}{eigenval1} + \frac{nosig}{eigenval2}$, 
% where the terms are the x and the y coordinate of the point squared, 
% respectively. But this is in a coordinate system that is aligned to the
% axes of the ellipse. For this reason these still need to be rotated with
% matrix U.
%
% Then the fiducial value must be added, i.e. the position of the centre of
% the ellipse.
%
% Then cycle around ellipse in increments of angle, dphi and calculate
% value at each point. -> actually can do this bit with arrays in Matlab
%
% version 1
% 11.06.2010 KMarkovic

% clear; fmat2D = [1 2; 0 3]; fidvect=[9.9,1.1];

if ~exist('nosig','var') nosig = 1; end
if ~exist('fidvect','var') fidvect = [0 0]; end
if ~exist('nosteps','var')||isnan(nosteps) nosteps = 200; end
if ~exist('linsty','var') linsty = 'k-'; end
if ~exist('col','var') col = NaN; end

[U,lamda] = eig(fmat2D);

% It turns out that we need to multiply by this extra factor, alpha.
% See astro-ph/0906.4123 and my notes on the 24.06.2010.
alpha = sqrt(-2*log(erfc(nosig/sqrt(2))))/nosig;

phi = (1:nosteps)*2*pi/nosteps;

%posvect(:,1) = sqrt(nosig)/sqrt(lamda(1,1))*cos(phi);
%posvect(:,2) = sqrt(nosig)/sqrt(lamda(2,2))*sin(phi);
% Fixing this together with alpha -> to fit Sarah's ellipses.
posvect(:,1) = nosig/sqrt(lamda(1,1))*cos(phi);
posvect(:,2) = nosig/sqrt(lamda(2,2))*sin(phi);

ellpoints = zeros(size(posvect));
for int = 1:nosteps
    ellpoints(int,:) = fidvect' + alpha*U*posvect(int,:)';
end

ellpoints(int+1,:) = ellpoints(1,:);

if ~isnan(col) 
    figid = fill(ellpoints(:,2),ellpoints(:,1),col);
    %plot(ellpoints(:,2),ellpoints(:,1),'--','Color',col,'LineWidth',1.1);
else
    %figid = plot(ellpoints(:,1),ellpoints(:,2),linsty);
    if ischar(linsty)
        figid = plot(ellpoints(:,2),ellpoints(:,1),linsty,'linewidth',linwid);
    else
        figid = plot(ellpoints(:,2),ellpoints(:,1));
        set(figid,'linewidth',linwid,'color',linsty)
    end
    %set(figid,'linewidth',linwid)
end

% invmat = sqrt(inv(fmat2D));
% min1 = fidvect(1) - 1.1*nosig*invmat(1,1);
% max1 = fidvect(1) + 1.1*nosig*invmat(1,1);
% min2 = fidvect(2) - 1.1*nosig*invmat(2,2);
% max2 = fidvect(2) + 1.1*nosig*invmat(2,2);
% axis([min1 max1 min2 max2]); % figure
% axis([min(ellpoints(:,1)) max(ellpoints(:,1)) min(ellpoints(:,2)) max(ellpoints(:,2))]);