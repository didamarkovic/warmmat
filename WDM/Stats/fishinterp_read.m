% fishinterp_read
%
% I want to be able to read the files that are written by
% lens_fish_demo_fishinterp.m
% I'm doing this with low level commands and not textread.m because I want
% to read different lines differently!
%
% 26.05.2010 KMarkovic

clear

%files = {'5-21-21'; '5-24-15'; '5-25-17'; '5-26-20'; '5-31-17'; '6-1-19'};
%files = {'6-24-18'}%;'6-25-9';};%'6-25-17'};
%files = {'6-25-17'};
%files = {'7-11-22'};
%files = {'7-12-18'}; % 1D!!!
%files = {'7-13-15'}; % halo, half steps
%files = {'12-22-21'}; % using smithread
%files = {'2011-1-28-17h'}; % using smithread - second batch, l_max=20000
%files = {'2011-2-3-17h-10000'}; % using smithread - l_max = 10000, 5000, 1000, 500
%files = {'2011-2-3-18h-1000'}; % using smithread - l_max = 10000, 5000, 1000, 500
%files = {'2011-2-4-13h-500'}; % using smithread - l_max = 10000, 5000, 1000, 500
% title('l_m_a_x = 20000, 10000, 5000'); axis([0 max(steps) 0.0002 0.005]); %clf
%files = {'2011-2-3-17h-5000'}; % using smithread - l_max = 10000, 5000, 1000, 500
files = {'2011-2-25-12h-20000'}; % using smithread - l_max = 10000, 5000, 1000, 500

steps = NaN;
errs = NaN;
errsh = NaN;

nopars = 5;

% Planck
planckmat = zeros(nopars,nopars);

didhalo = 0; didsmith = 0;
didread = 1;

for fileint = 1:length(files)
    
    insertdatetime = files{fileint};
    dohcol = 'b.';
    
    filename = ['fishinterp-' insertdatetime '.txt'];
    
    % open file
    fileid = fopen(filename);
    
    % read out of file line by line
    dohalo = 0; dosmith = 0; forcesmithoff = 0;
    r = 1;
    x = 0;
    indfish = 1;
    step = NaN;
    % get the first line
    linetext = fgetl(fileid);
    
    while linetext~=-1
        
        if r<=7
            % skip the header of the file
            if r==4 % read off the parameter names
                parnames = textscan(linetext,'%s %s %s %s %s'); % parnames{1}
            end
        else
            
            % test if halo or  smith
            if strcmp(linetext,'-   h a l o :')
                
                dohalo = 1;
                
                % get first line of set - step size
                linetext = fgetl(fileid);
                r = r+1;
                
                % save the step and move the fisher index on only if we've gone
                % to a new step-size
                if indfish~=1
                    if  str2num(linetext) ~= step(indfish-1)
                        step(indfish) = str2num(linetext);
                        indfish = indfish + 1;
                    end
                else
                    step(indfish) = str2num(linetext);
                    indfish = indfish + 1;
                end
                
                % loop through remaining 5 lines and get fisher matrix elements
                for int = 1:nopars
                    linetext = fgetl(fileid);
                    r = r+1;
                    
                    halofish(:,int,indfish-1) = str2num(linetext); % 2D fisher matrices vs (z-direction=step size)
                end
                
                mat = halofish(:,:,indfish-1);
                mat = mat + planckmat;
                mat(4,:) = mat(4,:)*10^5;
                mat(:,4) = mat(:,4)*10^5;
                invmat = inv(mat);
                errshtmp(indfish-1) = sqrt(abs(invmat(1,1)));
                
            else
                
                dosmith = 1;
                
                if indfish~=1
                    if  str2num(linetext) ~= step(indfish-1)
                        step(indfish) = str2num(linetext);
                        indfish = indfish + 1;
                    end
                else
                    step(indfish) = str2num(linetext);
                    indfish = indfish + 1;
                end
                
                for int = 1:nopars
                    linetext = fgetl(fileid);
                    r = r+1;
                    
                    sithfish(:,int,indfish-1) = str2num(linetext);
                end
                
                mat = sithfish(:,:,indfish-1);
                mat = mat + planckmat;
                if nopars >= 4
                    mat(4,:) = mat(4,:)*10^5;
                    mat(:,4) = mat(:,4)*10^5;
                end
                invmat = inv(mat);
                errstmp(indfish-1) = sqrt(abs(invmat(1,1))); %#ok<*SAGROW>
                
            end
            
            % skip the empty line
            linetext = fgetl(fileid);
            r = r+1;
            
        end
        
        % must make sure that the end of file is caught by the while loop
        linetext = fgetl(fileid);
        r = r+1;
        
    end
    
    %frewind(fileid);
    fclose(fileid);
    
    % save the errors and the steps
    steps(length(steps):(length(steps)+length(step)-1)) = step;
    if dosmith
        errs(length(errs):(length(errs)+length(errstmp)-1)) = errstmp;
    else
        errs(length(errs):(length(errs)+length(errshtmp)-1)) = NaN*ones(size(errshtmp));
    end
    if dohalo
        errsh(length(errsh):(length(errsh)+length(errshtmp)-1)) = errshtmp;
    else
        errsh(length(errsh):(length(errsh)+length(errstmp)-1)) = NaN*ones(size(errstmp));
    end
    clear errstmp errshtmp step
    
    if dosmith&&~didsmith; didsmith=1; end
    if dohalo&&~didhalo; didhalo=1; end
    
end

[steps order] = sort(steps);
errsh = errsh(order); errs = errs(order);

[steps indexvect] = unique(steps);
errsh = errsh(indexvect); errs = errs(indexvect);

if dohalo && dosmith
    sithfish = sithfish(:,:,order);
    halofish = halofish(:,:,order);
    sithfish = sithfish(:,:,indexvect);
    halofish = halofish(:,:,indexvect);
elseif dohalo
    halofish = halofish(:,:,order);
    halofish = halofish(:,:,indexvect);
elseif dosmith
    sithfish = sithfish(:,:,order);
    sithfish = sithfish(:,:,indexvect);
end

%return

%% plotting

nosig = 1;
plotplease = 1;

if forcesmithoff; didsmith = 0; end

if didsmith
    index = ~isnan(errs);
    serrs = errs(index); ssteps = steps(index);
    if plotplease
        figure(1)
        plot(steps,nosig*errs,'k.'); hold on
        %loglog(steps,nosig*errs,'k.'); hold on
        %axis([0 max(nosig*errs) 0 max(nosig*errs)])
        %axis([0 max(steps) 0 max(nosig*errs)])
    end
end
if didhalo
    index = ~isnan(errsh);
    herrsh = errsh(index); hsteps = steps(index);
    if plotplease
        figure(2)
        plot(steps,nosig*errsh,dohcol); hold on
        axis([0 max(nosig*errsh) 0 max(nosig*errsh)])
    end
end

if plotplease
    %line([0 3*max(steps)],[0 max(steps)],'LineStyle',':','Color','c')
    line([0 max(steps)],[0 max(steps)],'LineStyle',':','Color','k')
    axis([min(steps)*7/10 max(steps)*11/10 min(nosig*errs)*9/10 max(nosig*errs)*11/10])
end

% find the error = step point:
if didhalo && didsmith
    fn = nosig*serrs - ssteps;
    index = min(abs(fn))==abs(fn);
    step0 = ssteps(index);
    %step0 = interp1(fn,ssteps,0);
    err0 = serrs(index)
    %err0 = interp1(ssteps,nosig*serrs,step0)
    if plotplease
        figure(1)
        plot(step0,err0/nosig,'ro')
    end
    fishfromsmith = sithfish(:,:,index)
    
    fn = nosig*herrsh - hsteps;
    index = min(abs(fn))==abs(fn);
    steph0 = hsteps(index);
    %steph0 = interp1(fn,hsteps,0);
    errh0 = herrsh(index)
    %errh0 = interp1(hsteps,nosig*herrsh,steph0)
    if plotplease
        figure(2)
        plot(steph0,errh0/nosig,'ro')
    end
    fishfromhalo = halofish(:,:,index)
elseif didhalo
    fn = nosig*herrsh - hsteps;
    index = min(abs(fn))==abs(fn);
    steph0 = hsteps(index);
    %steph0 = interp1(fn,hsteps,0);
    errh0 = herrsh(index)
    %errh0 = interp1(hsteps,nosig*herrsh,steph0)
    if plotplease
        figure(2)
        plot(steph0,errh0/nosig,'ro')
    end
    fishfromhalo = halofish(:,:,index)
elseif didsmith
    fn = nosig*serrs - ssteps;
    index = min(abs(fn))==abs(fn);
    step0 = ssteps(index);
    %step0 = interp1(fn,ssteps,0);
    err0 = serrs(index)
    %err0 = interp1(ssteps,nosig*serrs,step0)
    if plotplease
        figure(1)
        plot(step0,err0/nosig,'ro')
    end
    fishfromsmith = sithfish(:,:,index)
end

if plotplease
    xlabel('step size')
    ylabel('1\sigma marginalised error')
    legend('errors','x=y line','error=step')
end

%%

return

%save 'fishinterpp-2010-07-07.mat'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%                   --- End Programme ---

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%

if 1
    planckid = fopen('fishplanck.txt');
    tmp = planckmat;
    for pint = 1:nopars % pint = 2
        line = fgetl(planckid);
        tmp(:,pint) = str2num(line);
        %tmptmp = textscan(line,'%d %d %d %d %d');
        %tmp(:,pint) = tmptmp{1:5};
        %clear tmptmp
    end
    tmp = tmp([1 2 5 4 3],:);
    planckmat = tmp(:,[1 2 5 4 3]);
    fclose(planckid);
    clear line pint tmp
end
%
%%

%save 'fishfromhalo.mat' fishfromhalo
%load 'fishfromhalo.mat'

fishfromhalo = zeros(size(fishfromsmith));

%save 'fishfromsmith.mat' fishfromsmith
%load 'fishfromsmith.mat'

%save 'fishfromreadsmith.mat' fishfromsmith

%filestring = 'fishinterp-2010-7-15-final_3_v4.txt';
%filestring = 'tempfishsmith-07-07-2010.txt';
%filestring = 'fishinterp-2010-12-22-bestfishmat.txt';
%filestring = 'fishinterp-2011-2-3-best10000.txt';
%filestring = 'fishinterp-2011-2-3-best5000.txt';
%filestring = 'fishinterp-2011-2-4-best1000.txt';
%filestring = 'fishinterp-2011-2-4-best500.txt';
dlmwrite(filestring,'Attempt at Fisher matrix with Smiths files - 22.12.2010','')
dlmwrite(filestring,'1/m_wdm Omega_m n_s A_s Gamma','-append','delimiter','')
dlmwrite(filestring,' ','-append','delimiter',' ')
dlmwrite(filestring,'Smith et al.:','-append','delimiter','')
dlmwrite(filestring,fishfromsmith,'-append','delimiter',' ')
dlmwrite(filestring,' ','-append','delimiter',' ')
dlmwrite(filestring,'Halo:','-append','delimiter','')
dlmwrite(filestring,fishfromhalo,'-append','delimiter',' ')
dlmwrite(filestring,' ','-append','delimiter',' ')
dlmwrite(filestring,'Planck:','-append','delimiter','')
dlmwrite(filestring,planckmat,'-append','delimiter',' ')
dlmwrite(filestring,' ','-append','delimiter',' ')

warning off
invhalo = safeinv(fishfromhalo);
allerrshalo = sqrt(diag(invhalo));
invhalo = safeinv(fishfromsmith);
allerrssmith = sqrt(diag(invhalo));
invhalo = safeinv(planckmat+fishfromsmith);
allerrssnp = sqrt(diag(invhalo));
cospars = setSMITHpars;
fids = [0 cospars.omega_m cospars.ns cospars.As cospars.Gamma];

dlmwrite(filestring,' ','-append','delimiter',' ')
dlmwrite(filestring,'fiducial values:','-append','delimiter','')
dlmwrite(filestring,[],'-append','delimiter',' ')
dlmwrite(filestring,'errors from halo:','-append','delimiter','')
dlmwrite(filestring,allerrshalo','-append','delimiter',' ')
dlmwrite(filestring,' ','-append','delimiter',' ')
dlmwrite(filestring,'errors from smith','-append','delimiter','')
dlmwrite(filestring,allerrssmith','-append','delimiter',' ')
dlmwrite(filestring,' ','-append','delimiter',' ')
dlmwrite(filestring,'errors from smith+planck','-append','delimiter','')
dlmwrite(filestring,fids,'-append','delimiter',' ')
dlmwrite(filestring,'+/-','-append','delimiter','')
dlmwrite(filestring,allerrssnp','-append','delimiter',' ')

return
%
%% Here fiddling with the Fisher matrix to examine the A_k entry:
cospars = setWMAP7pars;

errAk_euclid = 1/sqrt(fishfromsmith(4,4)); % 1D error on Ak
invmat = safeinv(fishfromsmith(2:5,2:5));
errAk_euclid_4d = sqrt(invmat(3,3));
%
errAk_planck = 1/sqrt(planckmat(4,4)); % 1D error on Ak
fprintf('From Planck, the 1D As = %d +/- %d\n',cospars.As,errAk_planck)

invmatp = safeinv(planckmat(2:5,2:5));
errAk_planck_4d = sqrt(invmatp(3,3)); % 1D error on Ak
fprintf('From Planck, the 4D As = %d +/- %d\n',cospars.As,errAk_planck_4d)
%