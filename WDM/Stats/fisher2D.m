function fmat2D = fisher2D(fmat,int,jnt);
% FISHER2D(fmat,int,jnt) marginalises over all the parameters in the 
% Fisher matrix fmat, but the two in places int & jnt. This can then be 
% fed into FISHERELLIPSE to plot the contour.
%
% First invert the Fisher matrix, then delete all the rows and columns we
% want to marginalise over. Then invert again.
%
% version 1
% 15.06.2010 KMarkovic

invmat = safeinv(fmat);
%invmat = pinv(fmat);
%invmat = inv(fmat);

invmattmp = invmat([int,jnt],:);
invmat2D = invmattmp(:,[int,jnt]);

fmat2D = safeinv(invmat2D);
%fmat2D = inv(invmat2D);
%fmat2D = pinv(invmat2D);
