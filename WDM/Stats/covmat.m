function [CC CC_v lens_obs_v] = covmat(l_vals,lens_obs,fsky)

% This function calculates the covariance matrix for a lensing power
% spectrum between different redshift bins after adding the shot noise.
% For equation see Takada&Jain 0310125v4.
%
% l_vals
% lens_obs - the power spectrum format: nbin*nbin*length(l_vals) + noise!
% fsky - fraction of sky covered
% 
%
% CC = covmat(l_vals,lens_vals,nzpars)
%
% OUTPUT is a ibin*jbin*mbin*nbin*length(l_vals) 5D covariance matrix
%        NOTE: symmetric entries are 0
%
% version 1.4
% 03/08/08 KMarkovic

nnbin = size(lens_obs,1);

% Now find the covariance matrix:

nol = length(l_vals);
deltal = l_vals(2:nol)-l_vals(1:(nol-1)); % ???? is this okay ????
deltal(nol) = l_vals(nol); % ???? is this okay ????

covfact(1,1,:) = 1./( (2*l_vals+1) .* deltal *fsky );

CC = zeros(nnbin,nnbin,nnbin,nnbin,nol);

for ibin = 1:nnbin
    for mbin = 1:nnbin
        for jbin = 1:nnbin%ibin:nnbin
            for nbin = 1:nnbin%mbin:nnbin

                % On the side make all entries to the Cls matrix nonzero:
                if ibin==1 && mbin==1 && jbin~=nbin && jbin>nbin
                    lens_obs(jbin,nbin,:)=lens_obs(nbin,jbin,:);
                end

                CC_tmp_1 = lens_obs(ibin,mbin,:).*lens_obs(jbin,nbin,:);
                CC_tmp_2 = lens_obs(ibin,nbin,:).*lens_obs(jbin,mbin,:);

                CC(ibin,jbin,mbin,nbin,:) = covfact.*(CC_tmp_1+CC_tmp_2);

            end
        end
    end
end

% Vectorise - SLB's method:
if nargout>=2
    k=1;
    for i=1:nbin
        for j=i:nbin
            i_ind(k)=i;
            j_ind(k)=j;
            k_inds(i,j)=k;
            lens_obs_v(k,:)=lens_obs(i,j,:);
            %lens_vals_v(k,:)=lens_vals(i,j,:);
            k=k+1;
        end
    end
    for k=1:length(i_ind)
        for l=1:length(i_ind)
            CC_v(k,l,:)=CC(i_ind(k),j_ind(k),i_ind(l),j_ind(l),:);
        end
    end
end