function dCdO = dcls_dparam(deltas,names,lens_minus,lens_plus)
% DCLS_DPARAM(deltas,names,lens_minus,lens_plus)
% This function calculates the partial derivative of the convergence power
% spectrum with respect to parametres (cosparnames) around the fiducial
% values (cosparsfid) with steps of deltas.
%
% deltas - step sizes in parameters to vary as an array
% names - cell array of strings of names of the varying parameters with the
%         following sytax for e.g.: names = {'w0'; 'omega_m'; 'omega_b'}
% lens_fid - lensing power spectra for all bins at the fiducial values of
%            all the parameters
% lens_obj - object that matches the object 'deltas' and contains the
%            variations of the sprectrum over the step in each parameter
%            e.g. lens_obj.omega_m conains the power spectrum where the
%            omega_m parameter has changed by deltas.omega_m compared to
%            lens_fid
%
% dCdO = dCl_dparam(deltas,names,lens_fid,lens_obj)
%
% OUTPUT will be an object of 3D matrices: nbin*nbin*length(l_vals) that
% matches the deltas object
%
% NOTE: if want backward differential make step -ve!
%
% version 1
% 30/07/08 KMarkovic
% version 1.1
% Making it compatible with the new version of lens_ps_get.m, where I
% calculate the derivatives by taking steps in both directions now.
% 15.04.2010 KM
% version 1.15
% Updated dynamic field names.
% 21.05.2010 KM

dCdO.tmp = 0;

for int = 1:length(names)
    
   name = names{int};
   lensP = lens_plus.(name);
   lensM = lens_minus.(name);
   
   % No need to cycle through the different bin combinations:
   dCdO_tmp = (lensP - lensM) / deltas(int);

   dCdO.(name) = dCdO_tmp;
   
   clear dCdO_tmp lensP lensM name
end

dCdO = rmfield(dCdO,'tmp');

% Note: this is quite a useless little function - wrapper will be much
% better!