function errs = fishellipses(fmat,fish,cospars,fact,linsty,col,linewidth,sizebool)
% FISHELLIPSES(fmat,fish,cospars,fact,linsty,colfill,linewidth,sizebool)
%   wraps FISHERELLIPSE 
%   for many fisher parameters and plots the ellipses and the Gaussian 
%   probability curves in an array of figures.
%
%   *fmat is the Fisher matrix
%   *fish is the structure containing the names of the parameters used 
%   *cospars contains the fiducial values for all the parameters
%   fact is the number of sigma the axes limits should correspond to
%   linsty is the style for the ellipse line
%
%   input marked with * is obligatory
%
%   version 1
%   15.06.2010 KMarkovic
%   version 2
%   improving the axes
%   24.02.2011 KM

fonts = 12; %set the plot font size

%format short

% These are just stupid rounding factors
%r.m_wdm = 10000;
%r.omega_m = 1000;
%r.ns = 1000;
%r.As = 0.001;
%r.Gamma = 1000;

% Minimum of significant figures
r.m_wdm = 2;
r.omega_m = 3;
r.ns = 3;
r.As = 2;
r.Gamma = 2;
fact10 = 10^4; % this is the unit of A_s!

if ~exist('fact','var') fact = 3; end
if ~exist('sizebool','var') sizebool = 0; else changepos = [1 1 1.3 1.33]; end
if ~exist('linsty','var') linsty = 'k-'; end
if ~exist('col','var') col = 'w'; end
if ~exist('linewidth','var')||isnan(linewidth) linewidth = 1; end

nosteps = 500;

% I want the outer ellipse to be slightly lighter than the inner.
coltmp = col+0.7;
colless = coltmp/max(coltmp);
clear coltmp
 

invfish = safeinv(fmat);
%invfish = inv(fmat);
%invfish = pinv(fmat);

tot = length(fish.names);
errs = zeros(1,5);

for int = 1:tot;
        
    name1 = fish.names{int};
    if strcmp(name1,'invm') name1 = 'm_wdm'; end
    fid1 = cospars.(name1);
    
    sig1 = sqrt(invfish(int,int)); %1/sqrt(fmat(int,int)); %
    
    
    %% Getting the Gaussian
    
    %subplot(tot,tot,tot*tot-(int-1)*tot-int+1)
    subplot(tot,tot,(int-1)*tot+int)
    
    p0 = gaussian(sig1,fid1,linsty,nosteps,5); hold on
    
    %% AXES: Set the limits for the first parameter:
    
    %lim3 = round((fid1-fact*sig1)*r.(name1))/r.(name1);
    %lim4 = round((fid1+fact*sig1)*r.(name1))/r.(name1);
    limits = round2sf([fid1-fact*sig1 fid1 fid1+fact*sig1],r.(name1));
    lim3 = limits(1);
    centre1 = limits(2);
    lim4 = limits(3);
    
    %% Set the AXES and make them pretty in the Gaussian plot
    
    axis([lim3 lim4 0 1.01]); % figure
    
    if sizebool 
        pos = get(gca,'position'); 
        %pos(3) = pos(3)*1.03;
        %pos(4) = pos(4)*1.1;
        set(gca,'position',pos)
    end
    set(gca,'XTick',[lim3 centre1 lim4])
    set(p0,'linewidth',linewidth)
    set(gca,'linewidth',1.5,'fontsize',fonts,'ticklength',[0.03 0])
   
    switch name1
        case 'm_wdm'
            xlabel('m_w_d_m^-^1 [keV^-^1]');
            %axis([0 round((fid1+fact*sig1)*r.m_wdm)/r.m_wdm 0 1.01]); % figure
            axis([0 lim4 0 1.01]); % figure
            set(gca,'XTick',[0 lim4/2 lim4],'XTickLabel',{0, lim4/2*1000, lim4*1000})
            %set(gca,'XTick',[0 round((fid1+fact*sig1)*r.m_wdm)/r.m_wdm/2 round((fid1+fact*sig1)*r.m_wdm)/r.m_wdm])
        case 'omega_m'
            xlabel('\Omega_m');
            set(gca,'XTickLabel',num2str(get(gca,'xTick')'))
        case 'ns'
            xlabel('n_s');
            set(gca,'XTickLabel',num2str(get(gca,'xTick')'))
        case 'As'
            xlabel(['A_s[10^' num2str(log10(fact10)) ']']);
            centraltick = round2sf(centre1); %round(  fid1/fact10*10^r.(name1)  ) / 10^r.(name1);
            set(gca,'XTickLabel',{lim3/fact10, centraltick/fact10, lim4/fact10})
        case 'Gamma'
            xlabel('\Gamma');
            set(gca,'XTickLabel',num2str(get(gca,'xTick')'))
        otherwise
            xlabel(name1);
            set(gca,'XTickLabel',num2str(get(gca,'xTick')'))
    end
    
    % Need to slightly increase the ranges
    xrange = get(gca,'XLim');
    if strcmp(name1,'m_wdm')
        set(gca,'XLim',[xrange(1) xrange(2)+0.3*sig1])
    else
        set(gca,'XLim',[xrange(1)-0.6*sig1 xrange(2)+0.6*sig1])
    end
        
    set(gca,'fontsize',fonts)
    
    % Positions of gaussian plots
    if sizebool
        pos = get(gca,'position');
        set(gca,'position',pos.*changepos)
    end
    
    %% Now plot the ellipses
    
    for jnt = (int+1):tot;
        
        name2 = fish.names{jnt};
        if strcmp(name2,'invm') name2 = 'm_wdm'; end
        fid2 = cospars.(name2);
        
        sig2 = sqrt(invfish(jnt,jnt));
        
        %% Ellipse plotting
        
        %subplot(tot,tot,tot*tot-(jnt-1)*tot-int+1)
        subplot(tot,tot,(int-1)*tot+jnt)
        
        fmat2D = fisher2D(fmat,int,jnt);
        
        p2 = fisherellipse(fmat2D,[fid1 fid2],2,nosteps,linsty,linewidth,colless); hold on
        p1 = fisherellipse(fmat2D,[fid1 fid2],1,nosteps,linsty,linewidth,col); hold on
        %fisherellipse(fmat2D,[fid1 fid2],3,nosteps,'k--'); hold on
        
        %% AXES: Setting the limits and centres for second parameter and
        %% resetting ones for the first one if needed

        %centre2 = round((fid2)*r.(name2))/r.(name2);
        %centre1 = round((fid1)*r.(name1))/r.(name1);
        
        %axis([fid1-fact*sig1 fid1+fact*sig1 fid2-fact*sig2 fid2+fact*sig2]); % figure
        if strcmp(name1,'m_wdm')
            %lim1 = round((fid2-fact*sig2)*r.(name2))/r.(name2);
            %lim2 = round((fid2+fact*sig2)*r.(name2))/r.(name2);
            toround = [fid2-fact*sig2 fid2 fid2+fact*sig2];
            limits = round2sf(toround,r.(name2));
            lim1 = limits(1);
            centre2 = limits(2);
            lim2 = limits(3);
            
            lim3 = 0;
            %lim4 = round((fid1+fact*sig1)*10000)/10000;
            centre1 = lim4/2;
            %axis([fid2-fact*sig2 fid2+fact*sig2 0 fid1+fact*sig1]); % figure
        elseif strcmp(name2,'m_wdm')
            lim1 = 0;
            %lim2 = round((fid2+fact*sig2)*10000)/10000;
            lim2 = round2sf(fid2+fact*sig2,r.(name2));
            %lim3 = round((fid1-fact*sig1)*r.(name1))/r.(name1);
            %lim4 = round((fid1+fact*sig1)*r.(name1))/r.(name1);  
            centre2 = lim2/2;
            %axis([0 fid2+fact*sig2 fid1-fact*sig1 fid1+fact*sig1]); % figure
        else
            %lim1 = round((fid2-fact*sig2)*r.(name2))/r.(name2);
            %lim2 = round((fid2+fact*sig2)*r.(name2))/r.(name2);
            toround = [fid2-fact*sig2 fid2 fid2+fact*sig2];
            limits = round2sf(toround,r.(name2));
            lim1 = limits(1);
            centre2 = limits(2);
            lim2 = limits(3);
            %axis([fid2-fact*sig2 fid2+fact*sig2 fid1-fact*sig1 fid1+fact*sig1]); % figure
        end
         
%         if strcmp(name1,'omega_m')
%             lim1 = round2sf(fid1-fact*sig1,3);
%             lim2 = round2sf(fid1+fact*sig1,3);
%             centre2 = round2sf(fid1,3);
%         elseif strcmp(name2,'omega_m')
%             lim1 = round2sf(fid2-fact*sig2,3);
%             lim2 = round2sf(fid2+fact*sig2,3);
%             centre2 = round2sf(fid2,3);
%         end

        
        %% Setting the AXES
        
        axis([lim1 lim2 lim3 lim4])
        
        %% AXES: Setting the tick marks
        
        set(gca,'XTick',[lim1 centre2 lim2])
        set(gca,'YTick',[lim3 centre1 lim4])

        if strcmp(name2,'As')
            %set(gca,'XTick',[lim1 centre2 lim2])
            %set(gca,'YTick',[lim3 centre1 lim4])

            %set(gca,'XTickLabel',[lim1/1000 centre2/1000 lim2/1000])
            %set(gca,'XTickLabel',num2str(get(gca,'XTick')'))
            if ~strcmp(name1,'m_wdm')
                set(gca,'YTickLabel',num2str(get(gca,'yTick')'))
            end
        elseif strcmp(name1,'As')
            %set(gca,'XTick',[lim1 centre2 lim2])
            %set(gca,'YTick',[lim3 centre1 lim4])
            
            %set(gca,'YTickLabel',[lim3/1000 centre1/1000 lim4/1000])
            set(gca,'YTickLabel',{lim3/fact10, round2sf(centre1)/fact10, lim4/fact10})
            %if ~strcmp(name1,'m_wdm')
                %set(gca,'XTickLabel',num2str(get(gca,'xTick')'))
            %end
            %set(gca,'YTickLabel',num2str(get(gca,'YTick')'))           
        else
            %set(gca,'XTick',[lim1 centre2 lim2])
            %set(gca,'YTick',[lim3 centre1 lim4])
            %set(gca,'XTick',[fid2-fact*sig2 fid2 fid2+fact*sig2])
            %set(gca,'YTick',[fid1-fact*sig1 fid1 fid1+fact*sig1])
            
            %if ~strcmp(name1,'m_wdm')
                %set(gca,'XTickLabel',num2str(get(gca,'xTick')'))
            %end
            if ~strcmp(name1,'m_wdm')
                %set(gca,'YTickLabel',num2str(get(gca,'yTick')'))
            else
                set(gca,'YTickLabel',num2str(get(gca,'yTick')')/1000)
            end
            
            %line([fid2-sig2 fid2-sig2],[fid1-sig1 fid1+sig1]);
            %line([fid2+sig2 fid2+sig2],[fid1-sig1 fid1+sig1]);
        end   
        
        % Getting rid of tick numbers in non-border figures and setting
        % ylabels on outer ones        
        set(gca,'XTickLabel',[])
        if jnt==tot
            switch name1
                case 'm_wdm'
                    ylabel('m_w_d_m^-^1');
                case 'omega_m'
                    ylabel('\Omega_m');
                case 'ns'
                    ylabel('n_s');
                case 'As'
                    ylabel(['A_s[10^' num2str(log10(fact10)) ']']);
                case 'Gamma'
                    ylabel('\Gamma');
                otherwise
                    ylabel(name1);
            end
        else
            set(gca,'YTickLabel',[])
            ylabel('')
        end


        
        % Need to slightly increase the ranges
        xrange = get(gca,'XLim');
        yrange = get(gca,'YLim');
        if strcmp(name1,'m_wdm')
            axis([xrange(1)-0.6*sig2 xrange(2)+0.6*sig2 ...
                yrange(1) yrange(2)+0.1*sig1])
        elseif strcmp(name2,'m_wdm')
            axis([xrange(1) xrange(2)+0.1*sig2...
                yrange(1)-0.6*sig1 yrange(2)+0.6*sig1])
        else
            axis([xrange(1)-0.6*sig2 xrange(2)+0.6*sig2...
                yrange(1)-0.6*sig1 yrange(2)+0.6*sig1])
        end

        
        %% Overlay X-line in case of WDM
        if strcmp(name1,'m_wdm')
            line([0.261 lim2], [0 0],'Color','k','LineWidth',1.5)
        end
                
        % General plot settings
        set(gca,'linewidth',1.5,'fontsize',fonts,'YAxisLocation','right')

        % Positions of ellipse plots
        if sizebool
            pos = get(gca,'position');
            set(gca,'position',pos.*changepos)
        end

    end
    
    errs(int) = sig1;
        
end

%% Now cycle through subplots and enlarge the areas
% if sizebool
%     for int = 1:tot^2
%         int
%         subplot(tot,tot,int)
%         pos = get(gca,'position')
%         %pos(3) = 0.2;%pos(3)*1.03;
%         %pos(4) = 0.2;%pos(4)*1.1;
%         set(gca,'position',pos)
%     end
% end