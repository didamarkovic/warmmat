% mama: 'pk_nlin_get_demo'
%
% 07.03.2012 KMarkovic

clear

k_vals = logspace(-2,2,100);
z_vals = [0 1 2];

% Cosmology
m_wdm = 0;
cospars = setpars([0.25 0 0.72 1],{'omega_dm' 'omega_b' 'h' 'ns'});


% Smith et al
pk_halofit = pk_nlin_get(cospars,k_vals,z_vals,'smith2');

% WDM: sims
%pk_sims = pk_nlin_get(cospars,k_vals,z_vals,'vielfit');
%psims = loglog(k_vals,pk_sims(zplot,:),'r');

% WDM: halo
pk_halo = zeros(length(z_vals),length(k_vals));
pk_lin = pk_halo; pk_2H = pk_halo; pk_1H = pk_halo;
for zint = 1:length(z_vals)
[pk_halo(zint,:) pk_lin(zint,:) pk_2H(zint,:) pk_1H(zint,:)] = ...
	     pk_nlhalo(k_vals,NaN,cospars,z_vals(zint));
end
pk_halo(isnan(pk_halo)) = 0;
pk_2H(isnan(pk_2H)) = 0;
pk_1H(isnan(pk_1H)) = 0;


figure(1); clf
zplot = 1;

phfit = loglog(k_vals,pk_halofit(zplot,:),'k'); hold on
phalo = loglog(k_vals,pk_halo(zplot,:),'m');
loglog(k_vals,pk_1H(zplot,:),'m.'); 
loglog(k_vals,pk_2H(zplot,:),'m.'); 

xlabel('k'); ylabel('P_k')
title('matter power spectrum')
axis([k_vals(1) 1e2 1e-2 1e5])
%legend([phfit psims phalo], 'Halofit','Simulations','Halo model')
legend([phfit phalo], 'Halofit','Halo model')


save k_vals k_vals
save pk_lin pk_lin
save pk_halofit pk_halofit
save pk_halo pk_halo
save pk_1halo pk_1H
save pk_2halo pk_2H
