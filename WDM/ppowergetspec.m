function [kvals pkvals shotnoise] = ppowergetspec(data,targetbins,minmodes)%,cutbool)
% PPOWERGETSPEC(data,targetbins,minmodes) uses the output
%   of the reading function ppowerread.m to get the nonlinear matter power spectrum. 
%   For this it combines the A and B blocks of the P-Power output file by doing a
%   weighted average after excluding the lowest 1% of k-modes of block B.
%
%   "data" is an output of the function ppowerread.m in the form of a
%   structure, containing 2 substructures, A and B, each containing 10
%   vectors.
%
%   "targetbins" is the target number of bins of the k-modes
%   (logarythmically spaced equally)
%   "minmodes" is the minimum number of modes per bin
%
%   "kvals" is a structure containing three entries: A, B and total.
%   Equivalently for "pkvals" and "shotnoise".
%
%   The binning routine is translated from an IDL routine written by Marco
%   Baldi. Similarly for the merging routine.
%
% version 1
% 24.05.2011 KMarkovic

if ~exist('targetbins','var'); targetbins = 50; end 
if ~exist('minmodes','var'); minmodes = 40; end 
%if ~exist('cutbool','var'); cutbool = 1; end 

blocks = {'A'; 'B'};
    

%%%%%%
% Binning
%%%%%%

bins = length(data.A.kvals);

for ab = 1:2
    
    kvals.(blocks{ab}) = [];
    pkvals.(blocks{ab}) = [];
    counting.(blocks{ab}) = [];
    
    mindlogk = (log10(max(data.(blocks{ab}).kvals)) - log10(min(data.(blocks{ab}).kvals)))/targetbins;
    
    ind=1;
    for istart = 1:bins

        %fprintf(1,'%i\n',istart)
        
        % Ok, how many k-modes do we have so far that have not been binned:
        count = sum(data.(blocks{ab}).modecount(ind));

        % Find the range of these k-modes
        deltak =  (log10(max(data.(blocks{ab}).kvals(ind))) - log10(min(data.(blocks{ab}).kvals(ind))));

        % If range big enough and it containes enough modes, do binning
        if (deltak > mindlogk) && (count > minmodes)

            % No idea what this does:
            d2 = sum(data.(blocks{ab}).sumpower(ind))/sum(data.(blocks{ab}).modecount(ind));
            
            % No idea what this does
            b = fix(sum(ind*data.(blocks{ab}).modecount(ind))/sum(data.(blocks{ab}).modecount(ind)));
            
            kk = data.(blocks{ab}).kvals(b);
            
            d2 = data.(blocks{ab}).convfact(b)*d2*data.(blocks{ab}).specshape(b);
            
            kvals.(blocks{ab}) = [kvals.(blocks{ab}), kk*1e3];
            pkvals.(blocks{ab}) = [pkvals.(blocks{ab}), 2*pi^2*d2/(kk*1e3)^3];
            counting.(blocks{ab}) = [counting.(blocks{ab}), sum(data.(blocks{ab}).modecount(ind))];
            
            clear ind
            ind = istart;
        else
            ind = [ind,istart];
        end
        
    end
end


%%%%%
% Combining the blocks
%%%%% 

ind_keep = kvals.B < kvals.B(  length(kvals.B)  -  fix( length(kvals.B)/25 )  );

kvals.B = kvals.B(ind_keep);
pkvals.B = pkvals.B(ind_keep);

% now we overlap the top-level mesh power spectrum and the folded one                                                                                                                                  
ind_A = kvals.A < max(kvals.B);
ind_B = kvals.B > min(kvals.A);

tmp_k_AB = kvals.B(ind_B); % kvals.A(ind_A)
weights = (tmp_k_AB/max(tmp_k_AB)).^2; %0.1 % we give different weights, ideally they'd be k-dependent             
tmp_pk_A = interp1(kvals.A,pkvals.A,tmp_k_AB);
tmp_pk_AB = weights.*tmp_pk_A + (1-weights).*pkvals.B(ind_B);

kvals.total = [kvals.B(~ind_B),tmp_k_AB,kvals.A(~ind_A)];
pkvals.total = [pkvals.B(~ind_B),tmp_pk_AB,pkvals.A(~ind_A)];

% now the shot noise
shotnoise.A = (2*pi^2)./kvals.A.^3.*interp1(data.A.kvals*1e3,data.A.shot,kvals.A);
shotnoise.B = (2*pi^2)./kvals.B.^3.*interp1(data.B.kvals*1e3,data.B.shot,kvals.B);
shotnoise.total = (2*pi^2)./kvals.total.^3.*interp1(data.B.kvals*1e3,data.B.shot,kvals.total);

% now we cut the power spectrum where the shot noise reaches a given threshold, 10%                                                                                                                    
ind_shot = shotnoise.total./pkvals.total > 0.1;
if sum(ind_shot)>0
   ind_shot = kvals.total < min(kvals.total(ind_shot)); % If wiggly spectrum.

   kvals.total = kvals.total(ind_shot);
   pkvals.total = pkvals.total(ind_shot);
   shotnoise.total = shotnoise.total(ind_shot);
end