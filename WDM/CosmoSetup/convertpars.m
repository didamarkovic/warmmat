function cospars = convertpars(cospars_K)

% This function converts the cosmological parameters from my convention to
% Sarah's convention.
%
% cospars_S = convertpars(cospars_K)
%
% version 1
% 16/08/08 Katarina Markovic

names = fieldnames(cospars_K);

for int = 1:length(names)
    if strncmp(names(int),'omega_de',8) 
        cospars.Omega_DE = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'omega_m',7)
        cospars.Omega_m = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'omega_dm',8)
        cospars.Omega_dm = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'omega_b',7)
        cospars.Omega_b = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'omega_wdm',9)
        cospars.Omega_wdm = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'m_wdm',5) 
        cospars.m_wdm = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'O_m_wdm',7)
        cospars.O_m_wdm = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'omega_nu',8) 
        cospars.Omega_nu = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'h',1) 
        cospars.h = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'w0',2) 
        cospars.w = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'wa',2) 
        cospars.wa = getfield(cospars_K,names{int}); %?
    elseif strncmp(names{int},'sigma8',6)
        cospars.sigma8 = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'ns',2)
        cospars.ns = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'n_run',5)
        cospars.n_run = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'omega_k',7)
        cospars.Omega_K = getfield(cospars_K,names{int});
    elseif strncmp(names{int},'As',2)%||strncmp(names{int},'Gamma',5)||strncmp(names{int},'k_pivot',7)
        %fprintf(1,'\tIgnoring As, k_pivot and Gamma in Sarahs params\n')
    elseif strncmp(names{int},'Gamma',5)||strncmp(names{int},'k_pivot',7)
    else fprintf(1,'UNKNOWN PARAMETER no. %1.0f!\n',int)
    end
end