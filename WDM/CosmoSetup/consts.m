% standard values and constants
%
% c_km = 299792.5; % km/s
% c = c_km * 1000; % speed of light in meters
% G = 6.67300e-11; % m^3 kg^-1 s^-2
% M_solar = 1.98892e30; % kg
% H0 = h * 3.24086077e-18; % per second
% H0_Mpc = h * 100; %in km /s /Mpc
% Mpc = 3.08568025e22; % meters in a megaparsec
% rho_crit = 3 * H0^2  ./ (8*pi*G) ; 
% arcmin = 180*60/pi;
%
% version 2
% 10/09/08 KMarkovic

if ~exist('cospars','var') cospars.h = h; end

c_km = 299792.5; % km/s
c = c_km * 1000; % speed of light in meters
G = 6.67384e-11; % m^3 kg^-1 s^-2
M_solar = 1.9891e30; % kg
Mpc = 3.08567758e22; % meters in a megaparsec
H0_Mpc = cospars.h * 100; % in km /s /Mpc
H0 = H0_Mpc / Mpc*1000; % per second
c_Mpc = c / Mpc; % speed of light in Mpc s^-1
% critical density
rho_crit_0 = 3 * H0^2  / (8*pi*G); % kg / m^3 
% 2.0406 times smaller than SLB because she doesn't put h in.
% this is a few particles per qubic meter
arcmin = 180*60/pi;
% Now, need a G in the right units:
G_Mpc = G * M_solar / Mpc^3; % Mpc^3 M_solar^-1 s^-2
% and 
%rho_crit_0_Mpc = 3 * H0_Mpc^2  / (8*pi*G_Mpc); km M_solar / Mpc^4 - wrong
rho_crit_0_Mpc = 3 * H0^2  / (8*pi*G_Mpc); % M_solar/Mpc^3

% test:
%rho_crit_0_Mpc - rho_crit_0/M_solar*Mpc^3