function omega = omegafn(z,omega_m,omega_l,w,omega_k,omega_r)
%
% This function calculates the total density, omega, in the Friedmann 
% equation.
%
% function omega = omegafn(z,Omega_m,Omega_l,w,Omega_k,Omega_r)
%
% It can be used for for example for finding distances by taking a square
% root and integrating over its inverse.
%
% If parameter not input assumes flat universe & w = -1 
% i.e. omega_m = 0.3, omega_l = 0.7;
%
% 11/06/07 KMarkovic

if (~exist('omega_k')) omega_k = 0; end
if (~exist('omega_r')) omega_r = 0; end
if (~exist('omega_m')) omega_m = 0.3; end
if (~exist('omega_l')) omega_l = 0.7; end
if (~exist('w')) w = -1; end

omega = omega_r.*( (1+z).^4 )  +  omega_m.*( (1+z).^3 )  +  ...
    omega_k.*( (1+z).^2 )  +  omega_l.*( (1+z).^(3*(1+w)) );