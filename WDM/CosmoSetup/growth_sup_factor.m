function growth_sup_vals=growth_sup_factor(cospars,z_vals,nz,znorm)
%
% Find growth supression factor for const w model
%
% Supercedes growth_sup_wconst by using a structure for cospars
% SLB Wed 12 Jul 2006
%
% Made it compatible 29/07/08 KM

%fprintf(1,'growth_sup_factor KM\n')

if (~exist('nz')) nz=1000; end
if (~exist('znorm')) znorm=20; end

% Unpack structure
scospars.Omega_m=cospars.omega_m;
scospars.Omega_DE=cospars.omega_de;
scospars.w=cospars.w0;
scospars.wa = cospars.wa;

[delta_vals, z_vals_tmp]=growth_wa(scospars,nz);

cospars_SCDM=scospars;
cospars_SCDM.Omega_m=1;
cospars_SCDM.Omega_DE=0;
cospars_SCDM.w=-1;

[delta_vals_SCDM, z_vals_tmp]=growth_wa(cospars_SCDM,nz);

[tmp iz]=min(abs(z_vals_tmp-znorm)); iz=iz(1); % normalise at z=znorm

growth_sup_vals= delta_vals./delta_vals_SCDM / (delta_vals(iz) /delta_vals_SCDM(iz));

growth_sup_vals=interp1(z_vals_tmp,growth_sup_vals,z_vals);

