function [k_vals l_vals] = findk(z_vals,l_vals,cospars)
% FINDKL(z_vals,l_vals) finds the wavenumbers, k corresponding to
%   multipoles, l at redshifts z. Note that the k are in units of h/Mpc!
%
%   'l_vals' - multipoles
%   'k_vals' - wavenumbers; optional input
%   'z_vals' - can be an array, then outputs are matrices
%
%   version 1
%   10.07.2011 KMarkovic

% clear; z_vals = [0.5; 0.9; 2.0];

if ~exist('l_vals','var') l_vals = 2*logspace(1,4,20); end
if ~exist('cospars','var') cospars = setEUCLIDpars; end

l_vals = horizontal(l_vals); 
z_vals = horizontal(z_vals); 

% Get D_H
chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

% Get distance in Mpc/h
c_km = 299792.5; % [km s^-1]
D_H = c_km/100; % [Mpc/h] - This means Cls are in units of h if Pks unitless!!!
chi = chi * D_H; % [Mpc/h] - hopefully now the distances are only in units of Mpc

k_vals = 1./chi' * l_vals; % k_vals * chi(2)
