function [cospars values names] = setEUCLIDpars()
% SETEUCLIDPARS sets the cospars to the latest standard LCDM EUCLID values.
%   No input.
%   Output is the cospars object:
%    [cospars values names] = SETEUCLIDPARS()
%
%   Parameters (in order):
%       omega_b 
%       omega_cdm 
%       omega_nu 
%       omega_de 
%       omega_wdm 
%       m_wdm 
%       omega_k... 
%       h 
%       w0 
%       wa 
%       sigma_8 
%       n_s 
%       n_run 
%       omega_m
%
%   version 1.0
%   WMAP 3 years: direct from WMAP for LCDM: 
%   http://lambda.gsfc.nasa.gov/product/map/dr2/params/lcdm_all.cfm
%   version 1.1
%   WMAP 5 years: updated to:
%   http://lambda.gsfc.nasa.gov/product/map/dr3/params/lcdm_sz_lens_wmap5.cfm
%   06/08/08 KMarkovic
%   version 1.3
%   WMAP 7 years: updated to:
%   http://lambda.gsfc.nasa.gov/product/map/dr4/params/lcdm_sz_lens_wmap7.cfm
%   10.04.2010 KM
%   Euclid:
%   
%   25.05.2011 KM

h = 0.703; 
omega_b = 0; % omega_b*h^2 = 0.02227
omega_cdm = 0.2711; % omega_cdm*h^2 = 0.1116
omega_de = 0.7289; % omega_de = 0.729
omega_m = 0.2711;
n_s = 0.966;
sigma_8 = 0.809;

% Others that we need:
omega_nu = 0;
omega_wdm = omega_cdm;
m_wdm = 0;
omega_k = 0;
w0 = -1; % -0.95
wa = 0;
n_run = 0;

values = [omega_b omega_cdm omega_nu omega_de omega_wdm m_wdm omega_k... 
     h w0 wa sigma_8 n_s n_run omega_m];

names = {'omega_b';'omega_dm';'omega_nu';'omega_de';'omega_wdm';'m_wdm'; 
    'omega_k';'h';'w0';'wa';'sigma8';'ns';'n_run';'omega_m'};

cospars = setpars(values,names);

%fprintf('WARNING!! SETTING As AND GAMMA TOO!!\n')
%tau = 0.085
%z_re = 10.4
%cospars.As = 1.2733*10^5; % amplitude = 2.42e-9
%cospars.k_pivot = 0.05;
%cospars.Gamma = exp(-2*omega_b*h) *omega_m *h; % gamma = 0.545  ?? not the same??