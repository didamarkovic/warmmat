function cospars = setpars(vals,names)

% This function sets the cosmological parameters into an object 'cospars'
% made up of omega_b, omega_dm, omega_nu, omega_de, omega_wdm, m_wdm, h,
% w0, wa, sigma8, ns, n_run, omega_k
% 
% vals is an array of values for these parameters
% names is a matching array of cells of the parameter names as stated above
% e.g. names = {'omega_dm' 'omega_b' 'h' 'ns'};
% test:  clear; vals = [0.25 0.05 0.7 1];
%
% cospars = setpars(vals,names)
%
% MINIMUM INPUT e.g.: setpars([0.7 0.25],{'omega_de'; 'omega_dm'});
%
% version 2
% 15/08/08 Katarina Markovic

for int = 1:length(names)
    if strncmp(names(int),'omega_de',8) cospars.omega_de = vals(int);
    elseif strncmp(names(int),'omega_m',7)cospars.omega_m = vals(int);
    elseif strncmp(names(int),'omega_dm',8) cospars.omega_dm = vals(int);
    elseif strncmp(names(int),'omega_b',7) cospars.omega_b = vals(int);
    elseif strncmp(names(int),'omega_wdm',9) cospars.omega_wdm = vals(int);
    elseif strncmp(names(int),'m_wdm',5) cospars.m_wdm = vals(int);
    %elseif strncmp(names(int),'WDM',3) cospars.O_m_wdm = vals(int);
    elseif strncmp(names(int),'omega_nu',8) cospars.omega_nu = vals(int);
    elseif strncmp(names(int),'h',1) cospars.h = vals(int);
    elseif strncmp(names(int),'w0',2) cospars.w0 = vals(int);
    elseif strncmp(names(int),'wa',2) cospars.wa = vals(int); %?
    elseif strncmp(names(int),'sigma8',6)cospars.sigma8 = vals(int);
    elseif strncmp(names(int),'ns',2)cospars.ns = vals(int);
    elseif strncmp(names(int),'n_run',5)cospars.n_run = vals(int);
    elseif strncmp(names(int),'omega_k',7)cospars.omega_k = vals(int);
    elseif strncmp(names(int),'k_pivot',7)cospars.k_pivot = vals(int);
    elseif strncmp(names(int),'As',2)cospars.As = vals(int);
    elseif strncmp(names(int),'tau',3)cospars.tau = vals(int);
    else
    warning('UNKNOWN PARAMETER no. %1.0f!',int)
    end
end

% DEFAULTS (should I set them to WMAP?):
% Should I write a script that sets all the nonset pars to WMAP?

% Simple defaults:
bool=0;
if ~isfield(cospars,'omega_wdm') cospars.omega_wdm = 0; end
if ~isfield(cospars,'m_wdm') cospars.m_wdm = 0; end
%if ~isfield(cospars,'O_m_wdm') cospars.O_m_wdm = 0; end % deleting this 23.02.10
if ~isfield(cospars,'omega_nu') cospars.omega_nu = 0; end
if ~isfield(cospars,'h') cospars.h = 0.7; bool=1; end
if ~isfield(cospars,'w0') cospars.w0 = -1; bool=1; end
if ~isfield(cospars,'wa') cospars.wa = 0; end
if ~isfield(cospars,'sigma8') cospars.sigma8 = 0.9; bool=1; end
if ~isfield(cospars,'ns') cospars.ns = 1; bool=1; end
if ~isfield(cospars,'n_run') cospars.n_run = 0; end

% Interdependant defaults:
%
% Matter densities:
% Matter densities with omega_k = 0 & omega_dm = omega_wdm:
if ~isfield(cospars,'omega_m') && isfield(cospars,'omega_de')
    if isfield(cospars,'omega_b')
        cospars.omega_m = 1 - cospars.omega_de;
        cospars.omega_dm = cospars.omega_m - cospars.omega_b;
    elseif isfield(cospars,'omega_dm')
        cospars.omega_m = 1 - cospars.omega_de;
        cospars.omega_b = cospars.omega_m - cospars.omega_dm;
    elseif cospars.omega_wdm~=0 && ~isfield(cospars,'omega_dm')
        cospars.omega_m = 1 - cospars.omega_de;
        cospars.omega_dm = cospars.omega_wdm;
        cospars.omega_b = cospars.omega_m - cospars.omega_dm;
    else
        cospars.omega_m = 1 - cospars.omega_de;
    end
elseif ~isfield(cospars,'omega_m') && ~isfield(cospars,'omega_de')
    if isfield(cospars,'omega_b')
        cospars.omega_m = cospars.omega_dm + cospars.omega_b;
        cospars.omega_de = 1 - cospars.omega_m;
    end
end


% Test the curvature:
if ~isfield(cospars,'omega_k') 
    cospars.omega_k = 1 - cospars.omega_m - cospars.omega_de;
end
if cospars.omega_k ~= 0
    fprintf(1,'WARNING: most code does not work for nonzero curvature!\n')
    fprintf(1,'         must have omega_m + omega_de = 1!\n')
end

if bool
    fprintf(1,' NOTE: Using some default values in setpars!\n')
end
