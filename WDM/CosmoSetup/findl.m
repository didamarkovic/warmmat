function [l_vals k_vals] = findk(z_vals,k_vals,cospars)
% FINDL(z_vals,k_vals) finds the multipoles, l corresponding
%   to wavenumbers, k at redshifts z. Note that the k are in units of h/Mpc!
%
%   'l_vals' - multipoles
%   'k_vals' - wavenumbers; optional input
%   'z_vals' - can be an array, then outputs are matrices
%
%   version 1 (from findk)
%   02.08.2013 KMarkovic

% clear; z_vals = [0.5; 0.9; 2.0];

if ~exist('k_vals','var') k_vals = logspace(-2,2,50); end
if ~exist('cospars','var') cospars = setPlanckpars; end

k_vals = horizontal(k_vals);
z_vals = horizontal(z_vals); 

% Get D_H
chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);

% Get distance in Mpc/h
c_km = 299792.5; % [km s^-1]
D_H = c_km/100; % [Mpc/h] - This means Cls are in units of h if Pks unitless!!!
chi = chi * D_H; % [Mpc/h] - hopefully now the distances are only in units of Mpc

l_vals = chi' * k_vals;
