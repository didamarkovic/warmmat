function [cospars values names] = setWMAP7pars()
% SETWMAP7PARS sets the cospars to the latest standard LCDM WMAP7 values.
%
% No input.
%
% Output is the cospars object:
%
% [cospars values names] = SET WMAP7PARS()
%
% Parameters (in order):
% omega_b 
% omega_cdm 
% omega_nu 
% omega_de 
% omega_wdm 
% m_wdm 
% omega_k... 
% h 
% w0 
% wa 
% sigma_8 
% n_s 
% n_run 
% omega_m
%
% version 1.0
% WMAP 3 years
% version 1.1
% WMAP 5 years
% 06/08/08 KMarkovic
% version 1.3
% WMAP 7 years
% 10.04.2010 KM

% Direct from WMAP for LCDM: 
% http://lambda.gsfc.nasa.gov/product/map/dr2/params/lcdm_all.cfm

fprintf('\t *Setting WMAP5* fiducial values and parameter values!*\n')

% Updated to:
% http://lambda.gsfc.nasa.gov/product/map/dr3/params/lcdm_sz_lens_wmap5.cfm
h = 0.719;
omega_b = 0.0441;
omega_cdm = 0.214;
omega_de = 0.742;
omega_m = 0.1326/(h^2); % = 0.256
%H_0 = 100*h; % km s-1 Mpc-1
n_s = 0.963;
sigma_8 = 0.796;
%tau = 0.087;

% Others that we need:
omega_nu = 0;
omega_wdm = 0;
m_wdm = 0;
%O_m_wdm = 0;
omega_k = 0;
w0 = -1;
wa = 0;
n_run = 0; % ????

values = [omega_b omega_cdm omega_nu omega_de omega_wdm m_wdm omega_k... 
     h w0 wa sigma_8 n_s n_run omega_m];

names = {'omega_b';'omega_dm';'omega_nu';'omega_de';'omega_wdm';'m_wdm'; 
    'omega_k';'h';'w0';'wa';'sigma8';'ns';'n_run';'omega_m'};

cospars = setpars(values,names);