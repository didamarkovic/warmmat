function D_pluss = growth_factor(cospars,z_vals,nz,znorm)
%
% Interpolate growth factor from growth_wconst by SLB.
%
% function D_pluss = growth_factor(omega_m,omega_l,w,z_vals,nz,znorm)
%
% Modified SLB's growth_sup_wconst.m
%
%
% version 2
% 29/07/08 KMarkovic


%fprintf(1,'growth_factor KM\n')

omega_m = cospars.omega_m;
omega_l = cospars.omega_de;
w = cospars.w0;

if (~exist('nz')) nz=1000; end
if (~exist('znorm')) znorm=0; end

[delta_vals, z_vals_tmp]=growth_wconst(omega_m,omega_l,w,nz);
[tmp iz]=min(abs(z_vals_tmp-znorm)); iz=iz(1); % normalise at z=znorm

z_vals(z_vals==0) = min(z_vals_tmp);

D_pluss = interp1(z_vals_tmp,delta_vals,z_vals)/delta_vals(iz);

if sum(isnan(D_pluss))>0
	bugger = find(isnan(D_pluss));
	error(['The following redshifts are out of range: ' num2str(z_vals(bugger)) '!']);
end