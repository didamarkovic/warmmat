function [cospars values names] = setPlanckpars()
% SETPLANCKPARS sets the cospars to the latest standard LCDM Planck values.
%
% No input.
%
% Output is the cospars object:
%
% [cospars values names] = SETPLANCKPARS()
%
% Parameters (in order):
% omega_b 
% omega_cdm 
% omega_nu 
% omega_de 
% omega_wdm 
% m_wdm 
% omega_k... 
% h 
% w0 
% wa 
% sigma_8 
% n_s 
% n_run 
% omega_m
%
% version 1.0
% WMAP 3 years
% version 1.1
% WMAP 5 years
% 06/08/08 KMarkovic
% version 1.3
% WMAP 7 years
% 10.04.2010 KM
% version 1.4
% 30.04.2013 KM

% Direct from Planck for LCDM: 
% http://lambda.gsfc.nasa.gov/product/map/dr2/params/lcdm_all.cfm

fprintf('\t *Setting Planck* fiducial values and parameter values!*\n')

% Updated to:
% http://arxiv.org/pdf/1303.5076v1.pdf
h = 67.11/100.0; % km s-1 Mpc-1
Omega_b = 0.022068/h^2;
Omega_cdm = 0.12029/h^2;
Omega_de = 0.6825;
Omega_m = Omega_b + Omega_cdm;
n_s = 0.9624;
sigma_8 = 0.796;
tau = 0.0925;

% Setting these two ONLY FOR CHECKING!!!
%   Don't use them, because you are giving sig8 and this is used
%   most of the time! Use As in code only if you want to check 
%   your calculations!
%k_pivot = 0.002;
%A_s = log(10^10*2.215e-9);

% Others that we need:
Omega_nu = 0;
Omega_wdm = 0;
m_wdm = 0;
%O_m_wdm = 0;
Omega_k = 0;
w0 = -1;
wa = 0;
n_run = 0;

%values = [Omega_b Omega_cdm Omega_nu Omega_de Omega_wdm m_wdm Omega_k...
%     h w0 wa sigma_8 n_s n_run Omega_m k_pivot A_s tau];
values = [Omega_b Omega_cdm Omega_nu Omega_de Omega_wdm m_wdm Omega_k... 
     h w0 wa sigma_8 n_s n_run Omega_m];

%names = {'omega_b';'omega_dm';'omega_nu';'omega_de';'omega_wdm';'m_wdm';
%    'omega_k';'h';'w0';'wa';'sigma8';'ns';'n_run';'omega_m';'k_pivot';'As';'tau'};
names = {'omega_b';'omega_dm';'omega_nu';'omega_de';'omega_wdm';'m_wdm'; 
    'omega_k';'h';'w0';'wa';'sigma8';'ns';'n_run';'omega_m'};

cospars = setpars(values,names);