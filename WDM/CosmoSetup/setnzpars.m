function nzpars = setnzpars(vals,names)

% This function sets the parameters for the galaxy distribution and for
% survey specifications into an object 'nzpars'
% made up of nbin, omega_dm, omega_nu, omega_de, omega_wdm, m_wdm, h,
% w0, wa, sigma8, ns, n_run, omega_k
%
% nzpars = setnzpars(vals,names)
% 
% vals is an array of values for these parameters
% names is a matching array of cells of the parameter names as stated above
% e.g. names = {'nbin' 'ng' 'z_0'};
% test:  clear; vals = [3 1e8 0.5];
%
% only nbin has no default value, so always need to set this!
%
% 07/8/08 KMarkovic
% version 1.01
% added readsmith
% 22.12.2010 KM

for int = 1:length(names)
    if strcmp(names(int),'nbin') nzpars.nbin = vals(int);
    elseif strcmp(names(int),'ng') nzpars.ng = vals(int);
    elseif strcmp(names(int),'z_0') nzpars.z_0 = vals(int);
    elseif strcmp(names(int),'alpha') nzpars.alpha = vals(int);
    elseif strcmp(names(int),'beta') nzpars.beta = vals(int);
    elseif strcmp(names(int),'deltaz') nzpars.deltaz = vals(int);
    elseif strcmp(names(int),'Deltaz') nzpars.Deltaz = vals(int);
    elseif strcmp(names(int),'fcat') nzpars.fcat = vals(int);
    elseif strcmp(names(int),'verb') nzpars.verb = vals(int);
    elseif strcmp(names(int),'fsky') nzpars.fsky = vals(int);
    elseif strcmp(names(int),'sigma_gamma') nzpars.sigma_gamma = vals(int);
    elseif strcmp(names(int),'zs') nzpars.zs = vals(int);
    elseif strcmp(names(int),'noth') nzpars.noth = vals(int);
    elseif strcmp(names(int),'zsmin') nzpars.zsmin = vals(int);

	elseif length(vals)<int nzpars.meth = names(int);
    %elseif strcmp(names(int),'smith') nzpars.meth = 'smith';
    %elseif strcmp(names(int),'smith2') nzpars.meth = 'smith2';
    %elseif strcmp(names(int),'readsmith') nzpars.meth = 'readsmith';
    %elseif strcmp(names(int),'vielfit') nzpars.meth = 'vielfit';
    %elseif strcmp(names(int),'halo') nzpars.meth = 'halo';
    %elseif strcmp(names(int),'p&d') nzpars.meth = 'p&d';
    %elseif strcmp(names(int),'warmhalo') nzpars.meth = 'warmhalo';
    %elseif strcmp(names(int),'coldhalo') nzpars.meth = 'coldhalo';

    else warning('UNKNOWN PARAMETER no. %1.0f!',int)
    end
end

% Default values - Euclid:
if ~isfield(nzpars,'ng') nzpars.ng = 35*(60*180/pi)^2; end % 12.04.2010, 35 says Sarah!
if ~isfield(nzpars,'z_0') nzpars.z_0 = 0.9/1.412; end
if ~isfield(nzpars,'alpha') nzpars.alpha = 2; end
if ~isfield(nzpars,'beta') nzpars.beta = 1.5; end
if ~isfield(nzpars,'deltaz') nzpars.deltaz = 0.05; end
if ~isfield(nzpars,'Deltaz') nzpars.Deltaz = 1; end
if ~isfield(nzpars,'fcat') nzpars.fcat = 0; end
%if ~isfield(nzpars,'type') nzpars.type = 'equal_tophat'; end
if ~isfield(nzpars,'type') nzpars.type = 'amarar06'; end
if ~isfield(nzpars,'verb') nzpars.verb = 0; end
if ~isfield(nzpars,'fsky') nzpars.fsky = 0.5; end
if ~isfield(nzpars,'sigma_gamma') nzpars.sigma_gamma = 0.35/sqrt(2); end