% Gnuplot is too basic, so I need to do my plots in 
%   Octave/Matlab
%
%	Reading in the results produced by CLASS.
%	This is the daughter file, now reading nonlinear
%		power spectra.
%
%	version 2
%	17.09.2012 KMarkovic

pth = '../output/fnu0/';
%pth = '../output/';

% Set the file names of data to plot:
fileC	=   'LCDM_pk_nl.dat';
filenu	= 'nuLCDM_pk_nl.dat';
fileW	=   'LWDM_pk_nl.dat';
fileWnu	= 'nuLWDM_pk_nl.dat';

% Read in LCDM
[dataC headerC] = myread([pth fileC],4,2);
k_vals = dataC(:,1); CPk_vals = dataC(:,2);
clear dataC headerC;

% Read in and interpolate nuLCDM
[tmp headernu] = myread([pth filenu],4,2);
nuPk_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headernu;

% Read in and interpolate LWDM
[tmp headerW] = myread([pth fileW],4,2);
WPk_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headerW;

% Read in and interpolatenu LWDM
[tmp headerWnu] = myread([pth fileWnu],4,2);
WnuPk_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headerWnu;