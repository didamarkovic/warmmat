% Gnuplot is too basic, so I need to do my plots in 
%   Octave/Matlab
%
%	Reading in the results produced by CLASS.
%
%	version 1
%	22.06.2012 KMarkovic

%pth = '../output/fnu0/';
pth = '../../../output/';

% Set the file names of data to plot:
fileC	=   'LCDM_pk.dat';
filenu	= 'nuLCDM_pk.dat';
fileW	=   'LWDM_pk.dat';
fileWnu = 'nuLWDM_pk.dat';

% Read in LCDM
[dataC headerC] = myread([pth fileC],4,2);
k_vals = dataC(:,1); CPk_vals = dataC(:,2);
clear dataC headerC;

% Read in and interpolate nuLCDM
[tmp headernu] = myread([pth filenu],4,2);
nuPk_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headernu;

% Read in and interpolate LWDM
[tmp headerW] = myread([pth fileW],4,2);
WPk_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headerW;

% Read in and interpolatenu LWDM
[tmp headerWnu] = myread([pth fileWnu],4,2);
WnuPk_vals = interp1(tmp(:,1),tmp(:,2),k_vals);
clear tmp headerWnu;