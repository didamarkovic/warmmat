% Gnuplot is too basic, so I need to do my plots in 
%   Octave/Matlab
%
%	Plotting the power spectra ratios for Ofer!
%	Need to run 'readin' first!
%
%	version 1
%	22.06.2012 KMarkovic

if ~exist('k_vals','var')
	fprintf('You need to run a readin[...].m script first, dimwit!\n');
	break
end

clf 
figure(1)

line([k_vals(1) k_vals(end)],[1 1],'Color',[.8 .8 .8]); hold on
nuCC = semilogx(k_vals,nuPk_vals./CPk_vals,'c'); hold on
nuWW = semilogx(k_vals,WnuPk_vals./WPk_vals,'m'); hold on
WC = semilogx(k_vals,WPk_vals./CPk_vals,'r.');
nuWC = semilogx(k_vals,WnuPk_vals./nuPk_vals,'g');
line([k_vals(1) k_vals(end)],[0 0],'Color',[.8 .8 .8]);

axis([k_vals(1) k_vals(end) -0.1 1.2]);
xlabel('k'); ylabel('P_1(k)/P_2(k) ratios');
legend([nuCC nuWW WC nuWC], '\nu\LambdaCDM/\LambdaCDM',...
	   '\nu\LambdaWDM/\LambdaWDM','\LambdaWDM/\LambdaCDM',...
	   '\nu\LambdaWDM/\nu\LambdaCDM',"location","south");

% Styling for slides
set([nuCC nuWW WC nuWC],'linewidth',2);
h=gcf;
set (h,'papertype', '<custom>')
set (h,'paperunits','centimeters');
set (h,'papersize',[3 2.5])
set (h,'paperposition', [0,0,[3 2.5]])
set (h,'defaultaxesposition', [0.15, 0.15, 0.75, 0.75])
set (0,'defaultaxesfontsize', 20)

text(2, 1.08, '*CLASS*', 'Color', 'k','FontSize',20);