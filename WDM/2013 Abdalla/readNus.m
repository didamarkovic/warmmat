% myread_demo
%
% 26.03.2013 KM

clear;

nheads = 4;
myform = 2;

figure(1); clf;
%subplot(2,1,1);

% LCDM linear matter power spectrum
filename = 'michi-cdm_pk.dat';
lcdm = myread(filename,nheads,myform,{'k','Pklin'});
c = loglog(lcdm.k,lcdm.Pklin,'b.'); hold on

% nuLCDM linear matter power spectrum
filename = 'michi-nucdm_pk.dat';
lwdm = myread(filename,nheads,myform,{'k','Pklin'});
w = loglog(lwdm.k,lwdm.Pklin,'r.');

% nuLCDM-const linear matter power spectrum
filename = 'michi-nucdm-const_pk.dat';
lhdm = myread(filename,nheads,myform,{'k','Pklin'});
h = loglog(lhdm.k,lhdm.Pklin,'m.');

% LCDM nonlinear matter power spectrum
filename = 'michi-cdm_pk_nl.dat';
lcdmn = myread(filename,nheads,myform,{'k','Pklin'});
cn = loglog(lcdmn.k,lcdmn.Pklin,'b'); hold on

% nuLCDM nonlinear matter power spectrum
filename = 'michi-nucdm_pk_nl.dat';
lwdmn = myread(filename,nheads,myform,{'k','Pklin'});
wn = loglog(lwdmn.k,lwdmn.Pklin,'r');

% nuLCDM-const nonlinear matter power spectrum
filename = 'michi-nucdm-const_pk_nl.dat';
lhdmn = myread(filename,nheads,myform,{'k','Pklin'});
hn = loglog(lhdmn.k,lhdmn.Pklin,'m');

xlabel('k [h/Mpc]'); ylabel('Pk [Mpc/h]^3');
axis([0.001 10 1 30000])
legend('CDM linear','\nuCDM linear','\nuCDM-const linear',...
	'CDM halofit','\nuCDM halofit','\nuCDM-const halofit')
set([c w h cn wn hn], "linewidth", 2);
title('m_\nu = 0.5 eV')

%print(gcf,"plots/neutrinos.eps",'-depsc')


%%%%%%%%%%%%%%%%%%%% RATIOS

%subplot(2,1,2);
figure(2); clf;

p1 = semilogx(lwdm.k,lwdm.Pklin./lcdm.Pklin,'r.'); hold on
p2 = semilogx(lhdm.k,lhdm.Pklin./lcdm.Pklin,'m.');
p3 = semilogx(lwdmn.k,lwdmn.Pklin./lcdmn.Pklin,'r'); hold on
p4 = semilogx(lhdmn.k,lhdmn.Pklin./lcdmn.Pklin,'m');
set([p1 p2 p3 p4], "linewidth", 2);

legend([p1 p2 p3 p4],'m_\nu = 0.5 eV \& \omega_m is kept constant','m_\nu = 0.5 eV \& \omega_{cdm} is kept constant', 'same but with halofit','same, but with halofit')
xlabel('k [h/Mpc]'); ylabel('P_{\nu\LambdaCDM}(k)/P_{\LambdaCDM}(k)')
axis([0.001 10 0.65 1.02])


%screenSize = get(0,'ScreenSize')
%set(gcf,'Position',screenSize.*[1 1 0.5 1]);

%print(gcf,"plots/neutrinos-ratios.eps",'-depsc')