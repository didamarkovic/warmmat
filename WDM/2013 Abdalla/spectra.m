% Gnuplot is too basic, so I need to do my plots in 
%   Octave/Matlab
%
%	Plotting the matter power spectra.
%	Need to run 'readin' first!
%
%	version 2 (1 was for Gnuplot)
%	22.06.2012 KMarkovic

if ~exist('k_vals','var')
	fprintf('You need to run a readin[...].m script first, dimwit!\n');
	break
end

clf
figure(1)

Cp = loglog(k_vals,CPk_vals,'g.'); hold on
nup = loglog(k_vals,nuPk_vals,'c.');
Wp = loglog(k_vals,WPk_vals,'r');
Wnup = loglog(k_vals,WnuPk_vals,'m');

axis([5*k_vals(1) k_vals(end) 1e-3 5e4]);
xlabel('k','fontsize',20); ylabel('P(k)','fontsize',20);
legend([Cp nup Wp Wnup], '\LambdaCDM','\nu\LambdaCDM','\LambdaWDM','\nu\LambdaWDM',3);

% Styling for slides
set([Cp nup Wp Wnup],'linewidth',2);
h=gcf;
set (h,'papertype', '<custom>')
set (h,'paperunits','centimeters');
set (h,'papersize',[3 2.5])
set (h,'paperposition', [0,0,[3 2.5]])
set (h,'defaultaxesposition', [0.15, 0.15, 0.75, 0.75])
set (0,'defaultaxesfontsize', 20)

text(0.0007, 0.003, '*CLASS*', 'Color', 'k','FontSize',20);