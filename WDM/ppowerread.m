function [data header] = ppowerread(filename,nheads,names)
% GADGETREAD(filename, numheads,names) reads from the file "filename" and
%   saves it into a data structure with the entry names coming from the
%   string array "names". The number of multipoles in the file "filename"
%   is normally given in the second element of the header in files written
%   by P-Power (MPA). Note that the "data" structure first splits into two
%   blocks, A and B, where the B block is found as a standard Power
%   Spectrum method (don't really know how, P-Power does it) and A is found
%   using the folding method in P-Power. I also don't know how it works.
%
%   [data header] = myread(filename,nheads,names)
%   The output "header" is a four element array. The first entry, I think,
%   is the paricle type from Gadget 2, the second is the number of k-modes
%   in each, A and B blocks, and I have no idea what the other two elements
%   are. This is in Matteo Viel's version of P-Power (I don't now where
%   it's from). My version of P-Power gives only 2 header elements of which
%   I think the first is the Gadget 2 paricle type and the second is the
%   number of k-modes in each block.
%   The output "data" is in the form of a structure. It has two fields, "A" and "B",
%   each also a structure containing 10 fields. The two structures
%   represent the blocks A and B produced by P-Power. The 10 elements of
%   these two structures are 10 vectors, each a column from the files
%   produced by P-Power. I'm not sure what all these columns are, but I
%   have set a default "names" array, which contains the names of the
%   vectors for which I know what they are.
%   For example, for the k-modes on small scales: data.A.kvals
%
% version 1:
% myread.m
% 25.06.2010 KMarkovic
%
% version 2
% Modified specifically to read power spectra produced by P-Power from
% outputs of Gadget 2.
% 23.05.2011 KM

% fclose(fid); clear; filename = '/Users/katarina/Desktop/Cosmology/WDM-sims/Matteo-TABLES/LCDM_25_512/powerspec_019.txt';

if ~exist('nheads','var');  nheads = 4; end
if ~exist('names','var');
    names = {'kvals'; 'deltasq'; 'shot'; 'ps'; 'modecount'; 'deltasquncorrected'; 'psuncorrected'; ...
                    'specshape'; 'sumpower'; 'convfact'};
end

ncols = 10; % Standars in files from P-Power
blocks = {'A';'B'};

fid = fopen(filename);

for ab = 1:2

    % Get the header
    header = zeros(nheads,1);
    for ii = 1:nheads
        textline = fgetl(fid);
        header(ii) = str2double(textline);
    end

    % Get the block
    datamat = zeros(header(2),ncols);

    nlines=0;
    while textline(1)~=-1 && nlines<header(2)
        textline = fgetl(fid);

        nlines = nlines +1;

        datamat(nlines,:) = str2num(textline);
    end

    % Write datamat into the structure "data":
    for ii = 1:ncols
        tmpcol = datamat(:,ii);
        data.(blocks{ab}).(names{ii}) = tmpcol;
    end

end

fclose(fid);

%%
%ii = 2;
%loglog(data.A.kvals, data.A.(names{ii}),'r'); hold on
%loglog(data.B.kvals, data.B.(names{ii}),'k');