% FISHELLIPSES_TEST
%  need to run the following functions first: 
%       lens_fish_demo_fishinterp.m 
%       fishinterp_read.m
%
% c.f. to Sarah's function
%
% 15.06.2010 KM
% 24.02.2011 KM
% modifying to be better for second paper
% version 2

clear
cospars = setWMAP7pars;

%% Read the matrices from file:
%planckid = fopen('fishinterp-2010-6-16-final_3_v3.txt');
planckid = fopen('fishinterp-2011-2-3-best5000.txt');
%planckid = fopen('fishinterp-2011-2-3-best10000.txt');

nopars = 5;

% Set ellipse colors
fact = 2.7;
greensmith = [0.5 1 0.5];
redplanck = [0 0 1];

tmp = zeros(nopars);

fprintf([fgetl(planckid) '\n']); % Discard first line
line = fgetl(planckid); 
fish.names = textscan(line,'%s %s %s %s %s');
fgetl(planckid); 

types = {'smith'; 'halo'; 'planck'};

for tint = 1:length(types)
    
    fgetl(planckid); % Discard first line
    
    for pint = 1:nopars
        line = fgetl(planckid);
        tmp(:,pint) = str2num(line);
    end
    
    fish.(types{tint}) = tmp;
    
    tmp = zeros(nopars);
    
    fgetl(planckid);
    
end

fclose(planckid);

%% Find the errors and plot the ellipses
for int = 1:length(fish.names)

    if strcmp(fish.names{int},'1/m_wdm')   
        fish.vals(int) = cospars.m_wdm;
        fish.names{int} = 'invm';
    elseif strcmp(fish.names{int},'Omega_m')
        fish.vals(int) = cospars.omega_m;
        fish.names{int} = 'omega_m';
    elseif strcmp(fish.names{int},'n_s')
        fish.vals(int) = cospars.ns;
        fish.names{int} = 'ns';
    elseif strcmp(fish.names{int},'A_s')
        fish.vals(int) = cospars.As;
        fish.names{int} = 'As';
    elseif strcmp(fish.names{int},'Gamma')
        fish.vals(int) = cospars.Gamma;
        fish.names{int} = 'Gamma';
    else
        fish.vals(int) = cospars.(fish.names{int});
    end
    
end

%% Plot for Smith et al. 2011 paper
figure
errs = fishellipses(fish.smith,fish,cospars,fact,greensmith,greensmith,1.2);
errs_p = fishellipses(fish.smith+fish.planck,fish,cospars,fact,'b--',redplanck,1.2);
fishellipses(fish.smith,fish,cospars,fact,'g:',NaN,0.5,1);

fprintf('errs = %f keV, errs_p = %f keV\n',1/errs(1)/1000,1/errs_p(1)/1000)