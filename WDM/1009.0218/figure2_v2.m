% massfn_demo -> figure3 13.01.2010 KM
%
% 17/09/08 KM
% 25.03.2010 KM
%
% Here let's add the simulation results from Jesus Zavala. C.f.-ing S-T and
% 1keV simulation results could try to estimate fitting function. (?) 22.02.2010
%
% version 2
% Adding the mass function from Zavala's simulations. The problem is that
% in the differential mass function can't really see a difference -> not
% very illustrative. So really should plot the cumulative mass function.
% 25.06.2010 KM
% 
% version 3
% Simply plotting the cumulative mass functions instead, so that all is
% clear.
% 07.07.2010 KM

clear

%% Now the sims too
simstoo = 0;

if simstoo

    filename = 'dndm.txt';
    CDM = myread(filename);

    filename = 'dndm2.txt';
    WDM = myread(filename);

    %dlogM = abs(   log(  WDM.M_vals( 2:length(WDM.M_vals) )  ) - log(  WDM.M_vals( 1:(length(WDM.M_vals)-1) )  )   );
    dlogM = abs( log(WDM.M_vals(2)) - log(WDM.M_vals(1)) );
    dM =  dlogM * WDM.M_vals; % [M_sol]

    WDM.dndm_vals = WDM.dndm_vals./dM;

    scrsz = get(0,'ScreenSize');
    figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
    subplot(2,1,1)
    %loglog(CDM.M_vals,CDM.dndm_vals,'b'); hold on
    loglog(WDM.M_vals,WDM.dndm_vals,'co'); hold on

    M_vals = WDM.M_vals';
end

%% Now Sheth-Tormen

m_x = [1000 5000 1e6];
%m_x = [500 1000 1e6];

cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;

k_vals = logspace(-4,4,1000);
Gamma = exp(-2*cospars.omega_b*cospars.h) * cospars.omega_m*cospars.h;
pk_cdm = pkf(Gamma, cospars.sigma8, k_vals);

if ~simstoo M_vals = logspace(0,16,100); end

% First find the mass functions:
%nu = 1.12; ????????
nu = 1.2;
growthfact = 1;
for int = 1:length(m_x)
    cospars.m_wdm = m_x(int);
    pk(int,:) = wdm_transfn_pars(pk_cdm,k_vals,cospars,nu);
    pk(int,:) = pk(int,:)*growthfact^2;
    dndm(int,:) = massfn(k_vals,pk(int,:),M_vals,cospars.omega_m,growthfact);
end

Mfs = freestreaming_comov(cospars,m_x);
whichMval1 = find(abs(M_vals-Mfs(1))==min(abs(M_vals-Mfs(1))));
whichMval2 = find(abs(M_vals-Mfs(2))==min(abs(M_vals-Mfs(2))));

%% Plot the mass functions:
%figure
if ~simstoo 
    scrsz = get(0,'ScreenSize');
    figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)]);
end
%set(gca,'fontsize',15)
subplot(2,1,1)
a = loglog(M_vals,dndm(1,:),'r--'); hold on
b = loglog(M_vals,dndm(2,:),'g');
c = loglog(M_vals,dndm(3,:),'k');
%title('WDM universal mass functions')
if ~simstoo 
    axis([M_vals(1) max(M_vals) 1e-30 1e10])
else
    axis([M_vals(1) max(M_vals) 1e-20 1e-10])
end
xlabel('M [M_o]'); ylabel('dn/dM')
set([a b c],'linewidth',1.5)
legend(['m_W_D_M = ' num2str(m_x(1)) ' eV'],['m_W_D_M = ' num2str(m_x(2)) ' eV'],1)
%saveas(gcf,'WDM_massfns','pdf')

%% Plot the ratio & cut-offs:
%figure
%set(gca,'fontsize',15)
subplot(2,1,2)
ratio1 = (dndm(1,:)-dndm(3,:))./dndm(3,:);
d = semilogx(M_vals,ratio1,'r--'); hold on
ratio2 = (dndm(2,:)-dndm(3,:))./dndm(3,:);
e = semilogx(M_vals,ratio2,'g');
f = plot(Mfs(1),ratio1(whichMval1),'ro');
g = plot(Mfs(2),ratio2(whichMval2),'go');
%title('WDM universal mass function ratios')
axis([M_vals(1) max(M_vals) -1.1 0.1])
xlabel('M[M_o]'); ylabel('(n_w_d_m-n_c_d_m)/n_c_d_m')
set([d e f g],'linewidth',1.5)
legend(['m_W_D_M = ' num2str(m_x(1)) ' eV'],['m_W_D_M = ' num2str(m_x(2)) ' eV'],2)
plot(M_vals,zeros(size(M_vals)),'k:')

samexaxis('xmt','on','ytac','join','yld',1)

%saveas(gcf,'WDM_massfns_ratio','pdf')