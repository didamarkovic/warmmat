% figure1_plotbin
%
% Want to see what's going on between the different methods at different
% redshifts.
%
% 17/12/08 KM

%clear

% First need to load the file produced with the figure1.m script
%load 'runfig1_73375964096.mat' % 10 bins, z_max = 2

% First do a plot comparing bins 1,4,7,10 between TNJ & HALO1 (i.e.
% Takada&Jain and Halocode-nocuts) - with CDM
cols = 'kbcgmrykbcgmry';
figure
int = 0;
%for ibin = [1 4 5 6 7 10]%1:nzpars.nbin%
for ibin = 1:nzpars.nbin%
    int = int+1;
    col = cols(int);
    [a pf] = plotlens(l_vals,Cls_tnj(1,ibin,ibin,:),col,1); hold on 
    b = plotlens(l_vals,Cls_halo1(1,ibin,ibin,:),[col '--'],1);
    c = plotlens(l_vals,Cls_halo2(1,ibin,ibin,:),[col '--'],1);
    axis([1e1 5e3 8e-8 8e-4])
end
legend([a b], 'T&J','halo',2)
set(gca,'yscale','log') % get(gca) or set(gca) to see the options
set(gca,'xscale','log')
title('high-res')
% Clearly it's the redshift resolution!!! The T&J goes down with bad
% resolution and the halo goes up! It gets almost fixed for the 2-halo term
% quite quickly (dz = 0.05) but it even has some effect on the 1-halo term!
% So maybe with good ehough resolution they would totally coincide! BUT!!!
% It also reduces the effect of WDM!!!!!

% now with WDM:
%ibin = 6;
ibin = 2;
figure
int=0;
for iwdm = 1:length(m_vals)
    int=int+1;
    col = cols(int);
    [a pf] = plotlens(l_vals,Cls_tnj(int,ibin,ibin,:),col,1); hold on 
    b = plotlens(l_vals,Cls_halo1(int,ibin,ibin,:),[col '--'],1);
    c = plotlens(l_vals,Cls_halo2(int,ibin,ibin,:),[col ':'],1);
end
legend([a b c], 'T&J','halo - no cuts','halo - Mmin cuts',2)
title('WDM')
set(gca,'yscale','log')
set(gca,'xscale','log')