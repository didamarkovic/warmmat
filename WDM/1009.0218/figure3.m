% 'figure3'
%
% 29/09/08 KM

clear

m_x = 0:100:10000;

cospars = setWMAPpars;
cospars.omega_wdm = cospars.omega_dm;

k_vals = logspace(-4,4,1000);
Gamma = exp(-2*cospars.omega_b*cospars.h) * cospars.omega_m*cospars.h;
pk_cdm = pkf(Gamma, cospars.sigma8, k_vals);

M_vals = logspace(0,16,100);

% First find the mass functions:
%nu = 1.12;
nu = 1.2;
fprintf(1,'Calculating')
for int = 1:length(m_x)
    cospars.m_wdm = m_x(int);
    pk(int,:) = wdm_transfn_pars(pk_cdm,k_vals,cospars,nu);
    dndm(int,:) = massfn(k_vals,pk(int,:),M_vals,cospars.omega_m,1);
    if int~=1 Mhalf(int) = interp1(dndm(int,:)./dndm(1,:),M_vals,0.5); end
    if round(int/5)==int/5 fprintf(1,'.'); end
end
fprintf(1,'\n')
Mfs = freestreaming_comov(cospars,m_x);

%% Plot 

figure
set(gca,'fontsize',15)
p = semilogy(m_x,Mfs,'r'); hold on
f = semilogy(m_x,Mhalf,'g--');
xlabel('m_W_D_M [eV]','fontsize',12); ylabel('M [M_s_o_l_a_rh]','fontsize',12);
legend('Free-streaming mass','Mass function ratio falling to 0.5 ')
set([p f],'linewidth',1.5)
set(gca,'LineWidth',1.5,'fontsize',12)
%saveas(gcf,'figure3','bmp')