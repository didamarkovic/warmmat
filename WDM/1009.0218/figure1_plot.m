% FIGURE1_PLOT plots the results of running the command FIGURE1. It plots
%   four plots, two WL power spectra plots comparing different WDM scenarios
%   and one comparing the different methods i.e. Mmin, halofit and full halo
%   model. Then underneath it plots the fractional differences together with
%   Euclid error bars on one of them.
%
%   version 1.0
%   05/10/08 KM
%   version 1.1
%   included subplots
%   22.02.2010 KM
%   version 1.2
%   Cleaned up and fixed the plots to be max illustrative (I think)
%   09.04.2010 KM

% Must initiate runfig1.mat first! 
% load 'runfig1_{INSERT!!!}.mat'
clear

%load 'runfig1_73432716303.mat'
%load 'runfig1_73433092364.mat'
load 'runfig1_73434701722.mat' % Mmin cuts are wrong apart from 1 keV!!

if ~exist('Cls_halo2','var') fprintf('Must run FIGURE1 first!\n'); return; end

ibin = 5; jbin = 5; % Which bins to plot (must be ibin=jbin to be able to have errors!)

%% FIGURE 1:
%figure(1); clf 

figure('Position',[1 700 500 700])

% Work out the error bars for Euclid
CC = covmat(l_vals,shiftdim(Cls_halo2(1,:,:,:),1)+noise,nzpars.fsky);
errors = squeeze(sqrt(CC(ibin,jbin,ibin,jbin,:)));

% Upper left plot
subplot(2,1,1)

d = plotlens(l_vals, Cls_tnj(4,ibin,jbin,:) ,'r-.',1);
c = plotlens(l_vals, Cls_tnj(3,ibin,jbin,:) ,'g--',1);
b = plotlens(l_vals, Cls_tnj(2,ibin,jbin,:) ,'b.-',1);
%[c pf] = plotlens(l_vals, Cls_tnj(2,ibin,jbin,:) ,'g--',1);
a = plotlens(l_vals, Cls_tnj(1,ibin,jbin,:) ,'k',1);

%a        = errorbar(l_vals,squeeze(Cls_tnj(1,ibin,jbin,:)).*pf',errors.*pf','k');

legend(['m_W_D_M = ' num2str(m_vals(4)) ' eV'],['m_W_D_M = ' num2str(m_vals(3)) ' eV'],['m_W_D_M = ' num2str(m_vals(2)/1000) ' keV'],'CDM',4)
xlabel('')
xlim([l_vals(1) max(l_vals)])
ylim([3e-6 2e-4])
set(gca,'Linewidth',1.5,'FontSize',12)
set([d c b a],'Linewidth',1.5)

bubble = get(gca,'position'); bubble(1) = .14;
set(gca,'position',bubble)

%% Lower left plot : wdmeffect - diff masses with errorbars
figure(1)
subplot(2,1,2) 
%subplot(2,1,1) 
%figure(1)

e = semilogx(l_vals, squeeze(dCl_tnj(3,ibin,jbin,:))' ,'ro'); hold on
f = semilogx(l_vals, squeeze(dCl_tnj(2,ibin,jbin,:))' ,'gs');
g = semilogx(l_vals, squeeze(dCl_tnj(1,ibin,jbin,:))' ,'bd');

x = errorbar(l_vals,zeros(size(l_vals)),errors./squeeze(Cls_tnj(1,ibin,ibin,:)),'kx');

set([g f e x],'linewidth',1.5)
xlabel('multipole, l','FontSize',12)
ylabel('(C_l_,_W_D_M - C_l_,_C_D_M)/C_l_,_C_D_M','FontSize',12)
xlim([l_vals(1) max(l_vals)])
ylim([-0.05 0.05])
legend(['m_W_D_M = ' num2str(m_vals(4)) ' eV'],['m_W_D_M = ' num2str(m_vals(3)) ' eV'],['m_W_D_M = ' num2str(m_vals(2)/1000) ' keV'],'CDM',3)

set(gca,'Linewidth',1.5,'FontSize',12)
axis([l_vals(1) max(l_vals) -0.15 0.1])

%title(['bin ' num2str(ibin) '-' num2str(jbin)])

bubble = get(gca,'Position'); bubble(1) = .14; % bubble(1)
set(gca,'Position',bubble)
samexaxis('xmt','on','ytac','join','yld',1)

%% FIGURE 2:
% figure(2); clf; clc
%% Upper right plot - diff methods for 1 keV -> wlpsratios
scrsz = get(0,'ScreenSize');
figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
% figure(1)
%%
subplot(2,1,1)

h = plotlens(l_vals, Cls_tnj(2,ibin,jbin,:) ,'k-',1);
%i = plotlens(l_vals, Cls_halo1(2,ibin,jbin,:) ,'b:',1);
j = plotlens(l_vals, Cls_halo2(2,ibin,jbin,:) ,'b--',1);
cdm = plotlens(l_vals, Cls_tnj(1,ibin,jbin,:) ,'k--',1);
xlim([l_vals(1) max(l_vals)])
ylim([2e-6 2e-4])

%legend([h i j],'halofit','M_m_i_n cut','full halo',4)
legend([h j cdm],'WDM with halofit','WDM with halo model','CDM with halofit',4)

%set([h i j],'linewidth',1.5)
set([h j],'linewidth',1.5)
set(gca,'Linewidth',1.5,'FontSize',12)
bubble = get(gca,'position'); bubble(1) = .14;
set(gca,'position',bubble)

%% Lower right plot : wdmeffect -> ratios
% Need to look at the third entry of the Cl (i.e. the difference in this
% case between the 1st and 3rd entry and hence the 2nd entry in the dCl), 
% since we only calculated that one for the halo model, since we don't 
% need the others and so they are equal to 0!
%figure%(1)
subplot(2,1,2) % clf

k = semilogx(l_vals,squeeze(dCl_tnj(2,ibin,jbin,:))','k-'); hold on
%l = semilogx(l_vals,squeeze(dCl_halo1(2,ibin,jbin,:))','b:');
m = semilogx(l_vals,squeeze(dCl_halo2(2,ibin,jbin,:))','b--');
semilogx(l_vals,zeros(size(l_vals)),'k--')

%set([k l m],'linewidth',1.5)
set([k m],'linewidth',1.5)
xlabel('multipole, l','FontSize',12)
ylabel('(C_l_,_W_D_M - C_l_,_C_D_M)/C_l_,_C_D_M','FontSize',12)
%legend('T&J','Mmin-cut','Halo',3)
%legend('halofit','M_m_i_n cut','full halo',3)
legend('WDM with halofit','WDM with halo model','CDM with halofit',3)
xlim([l_vals(1) max(l_vals)])
axis([min(l_vals) max(l_vals) -0.15 0.015])
set(gca,'Linewidth',1.5,'FontSize',12)

bubble = get(gca,'Position'); bubble(1) = .14;
set(gca,'Position',bubble)
samexaxis('xmt','on','ytac','join','yld',1)