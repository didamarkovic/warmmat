% 'lens_WDM_demo'
%
% Need a big computer to run this.
%
% 17/12/08 KM

tic

clear; %clc

% WMAP
cospars = setWMAP7pars;

% Euclid:
%nzpars = setnzpars(5,{'nbin' 'smith2'});
nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'; 'smith2'});
%nzpars = setnzpars(2,{'nbin' 'smith2'});
% [n_z_mat niz] = get_nofz(nzpars,z_vals);

z_vals = 0.0001:0.05:5.0001; %nzpars.zs = 2; % Need to have fine grid od z!!!
%z_vals = 0.0001:0.2:1;
l_vals = 2*logspace(1,4,20);
nzpars.noth = 400;

%m_vals = [0 2000 1000 500];
m_vals = [0 1000 500 250];

k_vals = logspace(-4,4,1000);
Gamma = exp(-2*cospars.omega_b*cospars.h)*cospars.omega_m*cospars.h;
pk_lin = pkf(Gamma,cospars.sigma8,k_vals);

%% METHOD 1: Takada&Jain: WDM TF for Pk_lin:
%dCl_tnj = zeros(length(m_vals)-1, nzpars.nbin, nzpars.nbin, length(l_vals));
Cls_tnj = zeros(length(m_vals), nzpars.nbin, nzpars.nbin, length(l_vals));
for int = 1:length(m_vals)
    %cospars.O_m_wdm = cospars.omega_dm/m_vals(int);
    cospars.omega_wdm = cospars.omega_dm; %%%%% 09.04.2010 KM
    cospars.m_wdm = m_vals(int);
    pk_nl = pk_nlin_get(cospars,k_vals,z_vals,nzpars.meth,pk_lin);
    Cls_tnj(int,:,:,:) = lens_ps(cospars,z_vals,l_vals,k_vals,pk_nl,nzpars);
    clear pk_nl
    if int~=1
        %dCl_tnj(int-1,:,:,:) = Cls_tnj(int,:,:,:) - Cls_tnj(1,:,:,:);
        warning off
        dCl_tnj(int-1,:,:,:) = (Cls_tnj(int,:,:,:) - Cls_tnj(1,:,:,:))./Cls_tnj(1,:,:,:);
        warning on
    end
    %cospars.O_m_wdm = 0;
end
%%%%%

%% METHOD 2a: Cooray model: Mmin cuts:
cospars.omega_wdm = cospars.omega_dm;
Mmins = freestreaming_comov(cospars,m_vals);
cospars.omega_wdm = 0; %%%%%%%%%%% 16/12/08 KM
Mmins(Mmins==Inf) = 1;
Mmins = max(Mmins,1);
dCl_halo1 = zeros(length(m_vals)-1, nzpars.nbin, nzpars.nbin, length(l_vals));
Cls_halo1 = zeros(length(m_vals), nzpars.nbin, nzpars.nbin, length(l_vals));
for int = 1:(length(m_vals))%-2)
    M_vals = logspace(log10(Mmins(int)),16,50);
    [Cls_halo1(int,:,:,:) plotfactor noise] = lenshalo_tomo(cospars,z_vals,...
                                 l_vals,k_vals,pk_lin,nzpars,M_vals);
    if int~=1
        %dCl_halo1(int-1,:,:,:) = Cls_halo1(int,:,:,:) - Cls_halo1(1,:,:,:);
        dCl_halo1(int-1,:,:,:) = (Cls_halo1(int,:,:,:) - Cls_halo1(1,:,:,:))./Cls_halo1(1,:,:,:);
    end
end


%% METHOD 2b: Cooray model: WDM TF for Pk_lin:
M_vals = logspace(0,16,50);
cospars.omega_wdm = cospars.omega_dm; %%%%% 17/12/08 KM
dCl_halo2 = zeros(length(m_vals)-1, nzpars.nbin, nzpars.nbin, length(l_vals));
Cls_halo2 = zeros(length(m_vals), nzpars.nbin, nzpars.nbin, length(l_vals));
for int = 1:(length(m_vals))%-2)
    %cospars.O_m_wdm = cospars.omega_wdm/m_vals(int);
    cospars.m_wdm = m_vals(int);
    Cls_halo2(int,:,:,:) = lenshalo_tomo(cospars,z_vals,...
                                 l_vals,k_vals,pk_lin,nzpars,M_vals);
    if int~=1
        %dCl_halo2(int-1,:,:,:) = Cls_halo2(int,:,:,:) - Cls_halo2(1,:,:,:);
        dCl_halo2(int-1,:,:,:) = (Cls_halo2(int,:,:,:) - Cls_halo2(1,:,:,:))./Cls_halo2(1,:,:,:);
    end
    %cospars.O_m_wdm = 0;
end

save(['runfig1_' num2str(round(1e5*now))],'*')

toc

return

%% Just recalculating for 1 keV using the correct M_fs! 27.07.2010 KM
% First initialise runfig1_73433092364.mat.

clear
clc
load 'runfig1_73433092364.mat'

tic

cospars.omega_wdm = cospars.omega_dm;
Mmins = freestreaming_comov(cospars,m_vals);
cospars.omega_wdm = 0; cospars.m_wdm = 10^8; % Make sure we have CDM.
Mmins(Mmins==Inf) = 1;

dCl_halo1 = zeros(length(m_vals)-1, nzpars.nbin, nzpars.nbin, length(l_vals));
Cls_halo1(2:4,:,:,:) = zeros(length(m_vals)-1, nzpars.nbin, nzpars.nbin, length(l_vals));

M_vals = logspace(log10(Mmins(2)),16,50);
[Cls_halo1(2,:,:,:) plotfactor noise] = ...
    lenshalo_tomo(cospars,z_vals,l_vals,k_vals,pk_lin,nzpars,M_vals);

dCl_halo1(1,:,:,:) = (Cls_halo1(2,:,:,:) - Cls_halo1(1,:,:,:))./Cls_halo1(1,:,:,:);

save(['runfig1_' num2str(round(1e5*now))],'*')
toc