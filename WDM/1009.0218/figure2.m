% massfn_demo -> figure3 13.01.2010 KM
%
% 17/09/08 KM
% 25.03.2010 KM
%
% Here let's add the simulation results from Jesus Zavala. C.f.-ing S-T and
% 1keV simulation results could try to estimate fitting function. (?) 22.02.2010
%
% version 2
% Adding the mass function from Zavala's simulations. The problem is that
% in the differential mass function can't really see a difference -> not
% very illustrative. So really should plot the cumulative mass function.
% 25.06.2010 KM
% 
% version 3
% Simply plotting the cumulative mass functions instead, so that all is
% clear.
% 07.07.2010 KM

clear

%% Now the sims too
simstoo = 1;

if simstoo

    filename = 'dndm.txt';
    CDM = myread(filename);

    filename = 'dndm2.txt';
    WDM = myread(filename);

    %dlogM = abs(   log(  WDM.M_vals( 2:length(WDM.M_vals) )  ) - log(  WDM.M_vals( 1:(length(WDM.M_vals)-1) )  )   );
    dlogM = abs( log(WDM.M_vals(2)) - log(WDM.M_vals(1)) );
    dM =  dlogM * WDM.M_vals; % [M_sol]

    % First uncumulatify, then divide by dM
    nomw = length(WDM.dndm_vals);
    diff = WDM.dndm_vals(1:(nomw-1)) - WDM.dndm_vals(2:nomw);
    WDM.dndm_vals = [diff; WDM.dndm_vals(nomw)];
    WDM.dndm_vals = WDM.dndm_vals./dM;
    
    nomc = length(CDM.dndm_vals);
    diff = CDM.dndm_vals(1:(nomc-1)) - CDM.dndm_vals(2:nomc);
    CDM.dndm_vals = [diff; CDM.dndm_vals(nomc)];
    CDM.dndm_vals = CDM.dndm_vals./(dlogM.*CDM.M_vals);

    scrsz = get(0,'ScreenSize');
    figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)])
    subplot(2,1,1)
    s1 = loglog(CDM.M_vals(4:22),CDM.dndm_vals(4:22),'kx'); hold on
    s2 = loglog(WDM.M_vals(4:23),WDM.dndm_vals(4:23),'rx'); hold on

    set(gca,'linewidth',1.2)
    
    %M_vals = WDM.M_vals';
end

%% Now Sheth-Tormen

m_x = [1000 5000 1e6];
%m_x = [250 1000 1e6];

cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;

k_vals = logspace(-4,4,1000);
Gamma = exp(-2*cospars.omega_b*cospars.h) * cospars.omega_m*cospars.h;
pk_cdm = pkf(Gamma, cospars.sigma8, k_vals);

%if ~simstoo M_vals = logspace(0,16,100); end
M_vals = logspace(0,16,100);

% First find the mass functions:
%nu = 1.12; ????????
nu = 1.2;
growthfact = 1;
for int = 1:length(m_x)
    cospars.m_wdm = m_x(int);
    pk(int,:) = wdm_transfn_pars(pk_cdm,k_vals,cospars,nu);
    pk(int,:) = pk(int,:)*growthfact^2;
    dndm(int,:) = massfn(k_vals,pk(int,:),M_vals,cospars.omega_m,growthfact);
end

Mfs = freestreaming_comov(cospars,m_x);
whichMval1 = find(abs(M_vals-Mfs(1))==min(abs(M_vals-Mfs(1))));
whichMval2 = find(abs(M_vals-Mfs(2))==min(abs(M_vals-Mfs(2))));

%% Plot the mass functions:
%figure
if ~simstoo 
    scrsz = get(0,'ScreenSize');
    figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)]);
end
%set(gca,'fontsize',15)
subplot(2,1,1)
a = loglog(M_vals,dndm(1,:),'r--'); hold on
b = loglog(M_vals,dndm(2,:),'g');
c = loglog(M_vals,dndm(3,:),'k');
%title('WDM universal mass functions')
if ~simstoo 
    axis([M_vals(1) max(M_vals) 1e-30 1e10])
else
    %axis([M_vals(1) max(M_vals) 1e-20 1e-10])
    axis([10^5 max(M_vals) 1e-30 0.1])
end
xlabel('M [M_o]','FontSize',12); ylabel('dn/dM','FontSize',12)
set([a b c],'linewidth',1.5)
set(gca,'linewidth',1.5,'FontSize',12)
if simstoo
    legend([a b s1 s2],['m_W_D_M = ' num2str(m_x(1)/1000) ' keV'],...
        ['m_W_D_M = ' num2str(m_x(2)/1000) ' keV'],'CDM simulation','1 keV simulation' ,3)
else
    legend([a b],['m_W_D_M = ' num2str(m_x(1)) ' eV'],['m_W_D_M = ' num2str(m_x(2)) ' eV'],3)
end
%saveas(gcf,'WDM_massfns','pdf')

%% Plot the ratio & cut-offs:
%figure
%set(gca,'fontsize',15)
subplot(2,1,2)
ratio1 = (dndm(1,:)-dndm(3,:))./dndm(3,:);
d = semilogx(M_vals,ratio1,'r--'); hold on
ratio2 = (dndm(2,:)-dndm(3,:))./dndm(3,:);
e = semilogx(M_vals,ratio2,'g');
f0 = plot(Mfs(1),ratio1(whichMval1),'ro');
f = line([Mfs(1) Mfs(1) ], [-2 ratio1(whichMval1)],'color','r','LineStyle','--');
g0 = plot(Mfs(2),ratio2(whichMval2),'go');
g = line([Mfs(2) Mfs(2)], [-2 ratio2(whichMval2)],'color', 'g', 'LineStyle','--');
%title('WDM universal mass function ratios')
axis([10^5 max(M_vals) -1.1 0.1])
xlabel('M[M_o]','FontSize',12); ylabel('(dn/dm_w_d_m-dn/dm_c_d_m)/dn/dm_c_d_m','FontSize',12)
%set([d e f g f0 g0],'linewidth',1.5)
set([d e f0 g0],'linewidth',1.5)
set(gca,'linewidth',1.5,'FontSize',12)
legend(['m_W_D_M = ' num2str(m_x(1)/1000) ' keV'],['m_W_D_M = ' num2str(m_x(2)/1000) ' keV'],4)
plot(M_vals,zeros(size(M_vals)),'k:')

samexaxis('xmt','on','ytac','join','yld',1)

%saveas(gcf,'WDM_massfns_ratio','pdf')