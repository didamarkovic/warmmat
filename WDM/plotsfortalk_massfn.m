% massfn_demo -> figure2 13.01.2010 KM
%
% 17/09/08 KM
% 25.03.2010 KM
%
% Here let's add the simulation results from Jesus Zavala. C.f.-ing S-T and
% 1keV simulation results could try to estimate fitting function. (?) 22.02.2010
%
% version 2
% Adding the mass function from Zavala's simulations. The problem is that
% in the differential mass function can't really see a difference -> not
% very illustrative. So really should plot the cumulative mass function.
% 25.06.2010 KM
% 
% version 3
% Simply plotting the cumulative mass functions instead, so that all is
% clear.
% 07.07.2010 KM
%
% version 4
% Modified for NAM2012.
% 29.03.2012

clear

filename = 'dndm.txt';
CDM = myread(filename,1);

filename = 'dndm2.txt';
WDM = myread(filename,1);

dlogM = abs( log(WDM.M_vals(2)) - log(WDM.M_vals(1)) );
dM =  dlogM * WDM.M_vals; % [M_sol]

% First uncumulatify, then divide by dM
nomw = length(WDM.dndm_vals);
diff = WDM.dndm_vals(1:(nomw-1)) - WDM.dndm_vals(2:nomw);
WDM.dndm_vals = [diff; WDM.dndm_vals(nomw)];
WDM.dndm_vals = WDM.dndm_vals./dM;
   
nomc = length(CDM.dndm_vals);
diff = CDM.dndm_vals(1:(nomc-1)) - CDM.dndm_vals(2:nomc);
CDM.dndm_vals = [diff; CDM.dndm_vals(nomc)];
CDM.dndm_vals = CDM.dndm_vals./(dlogM.*CDM.M_vals);

figure(1); clf
s1 = loglog(CDM.M_vals(4:22),CDM.dndm_vals(4:22),'kx'); hold on
s2 = loglog(WDM.M_vals(4:23),WDM.dndm_vals(4:23),'rx'); hold on

set(gca,'linewidth',1.2)


%%%%%%%%%%%%%%%%%%%%%%% Now Sheth-Tormen

m_x = [1000 5000 1e6];

cospars = setWMAP7pars;
cospars.omega_wdm = cospars.omega_dm;

k_vals = logspace(-4,4,1000);
Gamma = exp(-2*cospars.omega_b*cospars.h) * cospars.omega_m*cospars.h;
pk_cdm = pkf(Gamma, cospars.sigma8, k_vals);

M_vals = logspace(0,16,100);

% First find the mass functions:
nu = 1.2;
growthfact = 1;
for int = 1:length(m_x)
    cospars.m_wdm = m_x(int);
    pk(int,:) = wdm_transfn_pars(pk_cdm,k_vals,cospars,nu);
    pk(int,:) = pk(int,:)*growthfact^2;
    dndm(int,:) = massfn(k_vals,pk(int,:),M_vals,cospars.omega_m,growthfact);
end

Mfs = freestreaming_comov(cospars,m_x);
whichMval1 = find(abs(M_vals-Mfs(1))==min(abs(M_vals-Mfs(1))));

%% Plot the mass functions:
a = loglog(M_vals,dndm(1,:),'r--'); hold on
c = loglog(M_vals,dndm(3,:),'k');
axis([1e9 max(M_vals) 1e-23 1e-5])
xlabel('M [M_o]','FontSize',12); ylabel('dn/dM','FontSize',12)
set([a c],'linewidth',1.5)
set(gca,'linewidth',1.5,'FontSize',12)
legend([a s1 s2],'1 keV Sheth-Tormen','CDM simulation','1 keV simulation' ,3)