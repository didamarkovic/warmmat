function [C_l_ij pf noisemat Cl_P_ij Cl_C_ij] = lenshalo_tomo(...
    cospars,z_vals,l_vals,k_vals,pk_lin_cdm_0,nzpars,M_vals,kmax,kmin,normbool,normboolb)
%
% This function calculates the weak lensing power spectrum for different
% redshift bins using the halo model equations from Cooray et al. 2000.
%
% [C_l_ij plotfactor noisemat Cl_P_ij Cl_C_ij] = lenshalo_tomo(
% cospars,z_vals,l_vals,k_vals,pk_lin_cdm_0,nzpars,M_vals,kmax,kmin,normbool)
%
% cospars - set with SETPARS or SETWMAPPARS
% z_vals - redshift range
% l_vals - multipoles in the Cls
% k_vals - waveno in the Pks
% pk_lin_cdm_0 - today standard LCDM linear Pks
% nzpars - lensing survey pars
% M_vals - range of mass values
% kmax,kmin - cuts to the Pks if need, otherwise ignore or set to NaN
% normbool - set to 0 if don't want CDM normalisation of massfns fudge (1 if you do), set
% to 3 if don't want to normalise mass fns at all! - default is 3
% normboolb - set to 1 if want to skip normalization of bias
%
% l_vals - unitless
% k_vals [h/Mpc]
% pk_lin_cdm [Mpc^3/h^3]
% M_vals [M_solar]
%
% M_vals exist by default
%
% version 4.0
% re-hashing, so that can normalise Pk_nlin at end instead of dndm & bias,
% hmmmmm
% 01.02.2010 KM
% version 3.5
% cleaning up a bit, so that I can see what's wrong with mass cut-offs
% 27.11.09 KMarkovic

%%

if ~exist('M_vals','var')
    M_vals = logspace(0,16,50); % [M_sol]
end

if ~exist('normbool','var') % default: no normalisation of mass functions
    normbool = 3; % [M_sol]
end
if ~exist('normboolb','var') % defult: do normalise the bias
    normboolb = 0; % [M_sol]
end


% Setting the constants:
consts % Note: this is a script


% Galaxy distribution:
[n_z_mat niz] = get_nofz(nzpars,z_vals);


% Distances and differential comoving volume
Dh = c_km/H0_Mpc; % Hubble distance [Mpc]

chimat_tmp = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000); % [Dh]
chimat = chimat_tmp * Dh; % [Mpc]

[chi_tmp tmp tmp tmp vol_tmp] = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000); % [Dh]
chi = chi_tmp * Dh;    chi(chi==0) = min( chi(chi~=0) ) * 1e-10; % [Mpc]

d2V_dzdA = vol_tmp * Dh^3; % [Mpc^3]
%d2V_dzdA = vol_tmp * Dh^3 ./ (1+z_vals).^6; % [Mpc^3] % Also see line 217!
%warning('Random a^6 factor!\n')

clear chimat_tmp chi_tmp tmp vol_tmp


%% Halos:

% Transforming the power spectrum to include WDM:
pk_lin = pk_wdmtransf(cospars,z_vals,k_vals,pk_lin_cdm_0); % [Mpc^3/h^3]


% First need the z = 0 matter power spectrum for finding mass functions:
pk_lin_0 = pk_lin(:,1)'; % today + WDM % [Mpc^3/h^3]


% Mass scale
M_scale = mascale(cospars,z_vals,k_vals,pk_lin_0); % [M_sol]


% Halo properties:
[rv_vals theta_v conc rho_s] = haloparams(cospars,M_scale,M_vals,z_vals,chi); % [m][unitless][unitless][kg/m^3]
%[rv_vals theta_v conc rho_s] = haloparams(cospars.omega_m,...
%    cospars.h,M_scale,M_vals,z_vals,chi); % [m][unitless][unitless][kg/m^3]
%cospars.h,M_scale,M_vals,z_vals,chi*Mpc); % [m][unitless][unitless][kg/m^3]
%rv_vals = rv_vals/Mpc; %[Mpc]
%rho_s = rho_s/M_solar*Mpc^3; %[M_sol/Mpc^3]


% Fourier transform the lensing shear values: integral up to the angular
% virial radius:

%fprintf(1,'Fourier transforming kappa...');

if ~isfield(nzpars,'noth')
    no_theta = 512;
    %fprintf('\tUsing default steps in theta = 512.\n')
else
    no_theta = nzpars.noth;
end

% Interval in z:
dz = z_vals(2)-z_vals(1);


SIGMAcrit = zeros(size(chimat'));

if isfield(nzpars,'zs')

    %% All sources at one redshift

    chisource = Dh*Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000); %[Mpc]
    chi_ls = chisource - chi; % This is only right for FLAT spacetime!  % [Mpc]


    if nzpars.nbin ~= 1

        fprintf(1,'WARNING: Cant have bins if all at one source z!!!!')
        return

    end

    warning off
    SIGMAcrit = (c_Mpc^2) .* chisource ./ ...
        ( 4*pi*G_Mpc .* chi.* chi_ls ) ./(1+z_vals);% [M_sol /Mpc^2]
    warning on


    Ks_lMZ_i = fourierkappa(z_vals, chi, M_vals, l_vals,...
        theta_v, rv_vals, conc, rho_s, SIGMAcrit, no_theta); % [unitess]


%    fprintf(1,'done.\n')

else

    %% Binning

%    fprintf(1,'be patient...');


    Ks_lMZ = zeros(length(M_vals),length(z_vals),length(l_vals),length(z_vals));
    Ks_lMZ_i = zeros(length(M_vals),length(z_vals),length(l_vals),length(z_vals),nzpars.nbin);

    for sint = 1:length(z_vals)

        warning off
        SIGMAcrit(:,sint) = (c_Mpc^2) .* chi(sint) ./ ...
            ( 4*pi*G_Mpc .* chi.* chimat(:,sint)' ) ./(1+z_vals); % [M_sol /Mpc^2]
        warning on


        Ks_lMZ(:,:,:,sint) = fourierkappa(z_vals,chi,M_vals,l_vals,...
            theta_v,rv_vals,conc,rho_s,SIGMAcrit(:,sint),no_theta); %[unitless]

        for int = 1:nzpars.nbin
            Ks_lMZ_i(:,:,:,sint,int) = n_z_mat(sint,int)*Ks_lMZ(:,:,:,sint);
        end

    end

%    fprintf(1,'done.\n')

end

K_lMZ_i = squeeze(sum(Ks_lMZ_i,4));


%% Structure:


% Mass functions:

% WDM:
[dndm_tmp tmp1 tmp2 sigma] = massfns_slb(M_vals*cospars.h,z_vals,...
    cospars.omega_b,cospars.omega_m,cospars.omega_de,cospars.w0,cospars.h,...
    cospars.sigma8,k_vals,pk_lin_0); % [h^4/Mpc^3/M_sol][same][M_sol/h][unitless]
clear tmp1 tmp2

% CDM needed for normalization: hmmmm...
if isfield(cospars,'O_m_wdm')||isfield(cospars,'m_wdm')|| isfield(cospars,'omega_wdm')
    dndm_cdm = massfns_slb(logspace(0,16,length(M_vals))*cospars.h,z_vals,cospars.omega_b,cospars.omega_m,...
        cospars.omega_de,cospars.w0,cospars.h,cospars.sigma8,k_vals,pk_lin_cdm_0);
end
%figure(2); loglog(M_vals,dndm_tmp,'b.'); hold on; loglog(logspace(0,16,50),dndm_cdm,('r--')); figure(1)


% Small logarithmic interval in M:
dlogM = abs(log(M_vals(2)) - log(M_vals(1)));
dM = dlogM * M_vals; % [M_sol]


% Okay lets try out this weird new delta_c2 (from Henry 2000):
[delta_vals_tmp, z_vals_tmp] = growth_wconst(cospars.omega_m,cospars.omega_de,cospars.w0,1000);
growth_vals_tmp = delta_vals_tmp ./ ( max(delta_vals_tmp) );
growth_vals = interp1(z_vals_tmp, growth_vals_tmp, z_vals);
% Added the above as an experiment after fiddling with test_mascale.m -
% 23.02.2010 KM - !!!!!!!! growth in nu later !!!!!!!!!!!!!!!!!!!!!!!!!!!!
delta_c = delta_c_alt(z_vals,cospars.omega_m);  % [unitless]


% Bias:
a = 0.707; p = 0.3; % Sheth-Tormen 9901122v2

bias = zeros(size(sigma));
dndm = bias;
%warning('Below changed rho_mean->rho_mean_0...')
bob = 1;
for zint = 1:length(z_vals)

    %rho_mean = rho_crit_0_Mpc * cospars.omega_m * (1 + z_vals(zint))^3; %???!! % [M_sol/Mpc^3]
    rho_mean = rho_crit_0_Mpc * cospars.omega_m; %???!! % [M_sol/Mpc^3] - connect to line 67
    % Need to normalise to density today as mass functions defined in
    % COMOVING volume!!

    %nu = (delta_c(zint) ./ sigma(:,zint)).^2; % [unitless]
    nu = delta_c(zint) ./ sigma(:,zint) ./growth_vals(zint);%.^2; % [unitless]

    % From Seljak 0001493v1 (after equn 9):
    bias_unnorm =  1  +  ( a*nu - 1 )./delta_c(zint)  +  2*p./( 1 + (a*nu).^p ); %[unitless]
    % added the a !!!!!! 23.02.2010


    % Normalising the mass functions:
    % The following is a fudge, hmmmmmm...
    if (isfield(cospars,'m_wdm')|| isfield(cospars,'omega_wdm'))&&normbool&&normbool ~= 3
        norm = sum( dndm_cdm(:,zint)' .* dM .* M_vals / rho_mean );
        if bob warning('Using CDM mass function normalization!'); bob = 0; end
    elseif ~normbool
        norm = sum( dndm_tmp(:,zint)' .* dM .* M_vals / rho_mean );
    elseif normbool == 3 
        norm=1/cospars.h^4*(1+z_vals(zint))^3; % UNITS!!!!! 01.02.2010
    end

    %if normbool == 3 norm=1; end
    dndm(:,zint) = dndm_tmp(:,zint)/norm; %[1/Mpc^3/M_sol]<-NO!<-[1/m^3/M_sol]
    clear norm
    % h???????? units?????????????????????????????????????????????


    % Need to normalise the bias:
    if normboolb
        norm=1;
    else
        norm = sum( dndm(:,zint)' .* dM .* M_vals   / rho_mean .* bias_unnorm' );
        % this is actually rho_mean_0 here.
    end
    %bias(:,zint) = ones(size(M_vals));
    bias(:,zint) = bias_unnorm/norm; % [unitless]
    clear norm

end
%warning('Randomly multiplying the mass functions by a^3 again!')
%figure(3); loglog(M_vals,dndm,'b.'); hold on;loglog(logspace(0,16,50),dndm_cdm,('r--')); figure(1)


% Now interpolate the linear matter power spectrum to the values of l that
% we need in order to calculate the lensing shear p.s. Cls at ls, k = l/dl.
% This is according to the Limber approximation.
% Also make a matrix out of the vector dM for easier multiplication:
dMmatrix = zeros(length(M_vals),length(z_vals));
kinterpol = zeros(length(l_vals),length(z_vals));
pk_ldl = zeros(size(kinterpol));

for zint = 1:length(z_vals)

    dMmatrix(:,zint) = dM;

    kinterpol(:,zint) = l_vals/chi(zint)/cospars.h;
    % to have units of k of h/Mpc - dl is in metres & l has no unit

    pk_ldl(:,zint) = interp1(k_vals,pk_lin(:,zint)*growth_vals(zint)^2,kinterpol(:,zint)); %[Mpc^3/h^3]

end
clear zint

% Now, there are issues with NaN entries for z ~ 0, because dl -> 0 and
% therefore k needed -> infinity for high l. But the matter power spectrum
% is ~ 0 at such high k (I think) so let's make all the NaN entries 0:
pk_ldl(isnan(pk_ldl)) = 0;

% Also the Pk is in units of (Mpc/h)^3 => must multiply to adapt to units
% of everything else - sadly and stupidly 25/08/08:
%pk_ldl = pk_ldl * (Mpc/cospars.h)^3; %[m^3]
pk_ldl = pk_ldl /cospars.h^3; %[Mpc^3]

% And make an option for cutting the matter power spectrum 26/08/08:
if exist('kmax','var'); if ~isnan(kmax); pk_ldl(kinterpol>kmax) = 0; end; end
if exist('kmin','var'); if ~isnan(kmin); pk_ldl(kinterpol<kmin) = 0; end; end

% Now find the values of the integrand of the Poisson and of the
% Correlations term of the shear power spectrum (like in Cooray et al.) for
% all M values and all z values at all l:
%fprintf(1,'Integrating over mass and redshift...\n')

bool=1;

dINT_Z_P_ij = zeros(length(z_vals),nzpars.nbin,nzpars.nbin,length(l_vals));
dINT_Z_C_ij = zeros(length(z_vals),nzpars.nbin,nzpars.nbin,length(l_vals));
pf = zeros(nzpars.nbin,nzpars.nbin,length(l_vals));

for lint = 1:length(l_vals)


    for int = 1:nzpars.nbin

        % Mass integrand in the Correlations term:
        dINT_M_C_i = K_lMZ_i(:,:,lint,int) .* bias .* dndm .* dMmatrix; %[Mpc^-3]
        SUM_C_i = sum(dINT_M_C_i); % Summing over masses.
        clear dINT_M_C_i

        for jnt = int:nzpars.nbin

            % Mass integrand in Poisson term:
            dINT_M_P_ij = ...
                K_lMZ_i(:,:,lint,int).*K_lMZ_i(:,:,lint,jnt) .* dndm .* dMmatrix; %[Mpc^-3]

            % Now the 2-halo (Correlations) term:
            dINT_M_C_j = K_lMZ_i(:,:,lint,jnt) .* bias .* dndm .* dMmatrix; %[Mpc^-3]

            SUM_P_ij = sum(dINT_M_P_ij); % Summing over masses.

            SUM_C_j = sum(dINT_M_C_j); % Summing over masses.

            % Now the redshift integand with the mass integral already done:
            dINT_Z_P_ij(:,int,jnt,lint) = dz * SUM_P_ij .* d2V_dzdA; %[unitless]
            dINT_Z_P_ij(:,jnt,int,lint) = dINT_Z_P_ij(:,int,jnt,lint);

            % Now the Correlations term:
            dINT_Z_C_ij(:,int,jnt,lint) = ...
                dz * SUM_C_i .* SUM_C_j .* d2V_dzdA .* pk_ldl(lint,:) ; %[unitless]
            dINT_Z_C_ij(:,jnt,int,lint) = dINT_Z_C_ij(:,int,jnt,lint);

            % For plotting
            if bool
                pf(int,jnt,:) = l_vals.*(l_vals+1)./(2*pi);
            end

            clear dINT_M_P_ij dINT_M_C_j SUM_P_ij SUM_C_j
        end

        clear SUM_C_i
    end


    bool=0;
end

% Summing the two terms before integrating over z:
dINT_Z_ij = dINT_Z_P_ij + dINT_Z_C_ij;

% Now integrating the two terms separately:
Cl_P_ij = shiftdim(sum(dINT_Z_P_ij,1),1);
Cl_C_ij = shiftdim(sum(dINT_Z_C_ij,1),1);
clear dINT_Z_P_ij dINT_Z_C_ij

% And finally integrating the sum of the terms:
C_l_ij = shiftdim(sum(dINT_Z_ij,1),1);

% Now add the shot noise - none between different bins:
if nargout>2
    noisemat = zeros(size(C_l_ij));
    for ibin = 1:nzpars.nbin
        shotnoise = (nzpars.sigma_gamma^2) / niz(ibin);
        noisemat(ibin,ibin,:) = noisemat(ibin,ibin,:) + shotnoise;
    end
end