% Euclid WDM Signal-to-Noise
% 
% This script plots the Euclid error bars. Need to still program in the
% ratio of fractional difference due to WDM VS error bars to get a
% signal-to-noise plot!
%
% version 1.2
% Didn't have shot noise - added it.
% 27.01.2010 KMarkovic

clear

cospars = setWMAPpars;

l_vals = 2*logspace(1,4,20); 
zs = 5; dz = 0.2; z_vals = 0.000001:dz:(zs-0.0000001);

nzpars = setnzpars([10 2 2 1.13],{'nbin'; 'alpha'; 'beta'; 'z_0'});
nzpars.meth = 'smith2';
cospars = setWMAPpars;
[lens_vals tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);

CC = covmat(l_vals,lens_vals+noise,nzpars.fsky);

nbin = 5;
errors = squeeze(sqrt(CC(nbin,nbin,nbin,nbin,:)));
%loglog(l_vals,errors,'co'); hold on

figure
plotlens(l_vals,lens_vals(nbin,nbin,:)); hold on
plotlens(l_vals,squeeze(lens_vals(nbin,nbin,:))-errors,'c');
plotlens(l_vals,squeeze(lens_vals(nbin,nbin,:))+errors,'c');