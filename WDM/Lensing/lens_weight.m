function wiofz = lens_weight(cospars,z_vals,chi_mat,nofz,zs,chi_ss)

% Lensing weight function with equations combined from Sarah's lens_cls code and 
% the Takada&Jain paper.
%
% wiofz = lens_weight(cospars,z_vals,chi_mat,nofz,zs)
%
% chi_mat is a matrix of distances between all the z_vals produced by
% SLB's d_wa_grid.m for example (in units of Dh^-1!).
% nofz is the number density of source galaxies at z_vals (get_nofz.m)
% n_norm is the no. of galaxies per arcmin?
% Note that in the case we have all sources at one redshift, chi_mat is
% just the chi - distances to the lenses (or sources)!!
% 
% output is a matrix different bins in different columns, z(lenses) in rows
% NOTE: output is in units of Dh*h^2 /Mpc^2 (or c*h/Mpc^2) if
% chi_mat & chi_ss are in [Dh]! (fixed this - 27.10.09) 
% If input in Mpc/h then output: h/Mpc!!! (16.11.09)
%
% version 1.3
% Only changed the chi - not the first line of chi_mat anymore. Also note
% that the inputs are slightly different!
% 24.11.09 KM

if ~exist('n_norm','var') n_norm = 1; end

consts.c = 299792.5; % km/s
%cospars.h=1; % NOTE: Need this to make it like Sarah's??!!
%Dh = consts.c/(100*cospars.h);
Dh = consts.c/100; % Matching this to have units of h for wi_z
KO = 3/2 * cospars.omega_m * (Dh)^(-2); 


% done outside:
%chi = chi_mat(1,:); % comov ag diam dists = comov dist % REMOVED THIS 24.11.09!!
%chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
%if chi_mat(1,:)/chi > 10
%    chi = chi * Dh;
%end
%chi = chi*Dh; % see D_wconst docs! - output in units of Dh!
%dchi = chi(2:length(chi))-chi(1:(length(chi)-1));
%dchi(length(chi)) = 0; % add one dchi at end
%chi_mat = chi_mat*Dh;

%tmp_z = [0 z_vals]; [chi_tmp rAR] = D_wa_grid_km(tmp_z,cospars);
%chi = chi_tmp(1,2:(length(z_vals)+1));

if exist('zs','var')
    
       chi = chi_mat;

            if ~exist('chi_ss','var')
                chi_ss = Dh*Ds_wconst(0,zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
                chi_lss = Dh*Ds_wconst(z_vals,zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
                fprintf(1,'Assuming units of [Mpc/h] for distances in lens_weight.m!!!\n')
            elseif exist('chi_ss','var')
                chi_lss = chi_ss-chi; % Yikes! Only for omega_k = 0!!!!!!
            end
            
else
    chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
    % Added above from below (line 85ish) 03.02.2011 KM
    
end


wiofz = zeros(size(nofz));
 
a_vals = 1./(1+z_vals);
 
for ibin = 1:size(nofz,2)

    %ni_z = nofz(:,ibin)'*n_norm/sum(nofz(:,ibin));
    ni_z = nofz(:,ibin)';
    % makes no difference really - they're already normailsed in get_nofz!

    Wi_l = zeros(length(z_vals),1);

    %for zlint = 1:(length(z_vals)-1)
    for zlint = 1:length(z_vals)

        % Adding the option to make all sources at one z:
        if ~exist('zs','var')
            
            %chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000);
            % Moved above line 03.02.2011 KM
            if chi_mat(1,:)/chi > 10
               chi = chi * Dh;
            end
            chi(chi==0) = min(chi(chi~=0))*1e-10; % Correcting for the zero

            dWi_l = 0; % clear the integral

            %chi_ls = chi_mat(zlint+1,:);
            % Since first line is obs->gal dist.
            % Actually obs redshift taken to be ~ first lens redshift, so there
            % is no special first line:
            chi_ls = chi_mat(zlint,:);

            dWi_l = ni_z .* chi(zlint) .* chi_ls ./ chi  ;%.* dchi;
            % ni_z = 0 outside bin so can now just sum for integration!
            % no dchi because it is included in the nofz!!! since normalised!!!
            % also, nb: n(z)dz = n(X)dX

            Wi_l(zlint) = sum(dWi_l);

            
        else

            
            Wi_l(zlint) = sum(ni_z) * chi(zlint).* chi_lss(zlint) ./ chi_ss;

        end

    end
    %Wi_l(1) = 0; % Make sure to delete what we add in line 34 above.
    %Wi_l(zlint+1) = 0;
    wiofz(:,ibin) = KO * Wi_l ./ a_vals';
end
%figure;plot(z_vals,wiofz); title('lensing weight')
%ylabel('W_i(z)');xlabel('redshift, z'); hold on
%plot(z_vals,2*g_vals*sqrt(prefactor).*(1+z_vals'),'--')

%%% c.f. to Sarah's lens_cls code (modified to fit here!):
%n_z_tot = nofz;
%consts.c = 3*10^5;
%Omega_m = cospars.omega_m;
%%Omega_DE = cospars.omega_de;
%%w = cospars.w0;
%D_H = consts.c/cospars.h/100;
%r_AR = chi_mat;
%g_vals = zeros(size(nofz));
%for ibin=1:size(nofz,2)
%    for iz_l=1:length(z_vals)
%        for iz_s=(iz_l+1):length(z_vals)
%            int(iz_s)=2*n_z_tot(iz_s,ibin) * ...
%               r_AR(1,iz_l) * r_AR(iz_l,iz_s) / r_AR(1,iz_s);%+1!!!!-no!
%        end
%        g_vals(iz_l,ibin)=sum(int);
%       int = zeros(size(z_vals)); % deleting int!!!!!!!
%    end
%    g_vals(:,ibin) = g_vals(:,ibin)'.*(1+z_vals);
%end
%prefactor = 3  * Omega_m / (4 * D_H^2);
%wiofz=g_vals*prefactor;

% Just for general debugging - totally meaningless:
wiofz=wiofz;