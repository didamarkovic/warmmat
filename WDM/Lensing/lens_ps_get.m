function [cls_fid halo_fid noise dcls dhalo bool] = lens_ps_get(cospars_fid,...
    nzpars,z_vals,l_vals,nbin,fish,M_vals,supnl,normbool,normboolb)

% This function wraps lens_ps and calculates the dCls/dparam for all the
% parameters given within the fish object.
%
% [cls_fid halo_fid noise dcls dhalo] = lens_ps_get(cospars,
%                            nzpars,z_vals,l_vals,nbin,fish,M_vals,supnl)
%
% cospars_fid - object from setpars - fiducial vales of cosmological pars
% nzpars - parameters for the galaxy distribution function and survey specs
% nbin - no. of bins
% fish - object - here only need the names and the steps entries for the
%        parameters one wants to vary
%      - can get the fish.names from fieldnames(cospars) - CELL ARRAY!
%      - can have default values for fish.steps
% M_vals - only input if want to run the halo model calcs
% supnl - set to 1 to suppress the non-halo calcs, then the result for it
%         is NaN & set to 0 or leave out for both calcs
% normbool - 0 for reguar normalisation, 1 for CDM normalisation, 3 for no
% normalisation of the mass functions - default is 3
%
% OUTPUT - cls + noisemat gives the observed fiducial Cls
%        - dcls is an object containing dCls/dpar for all names
%
% Note: can only vary the cospars!!!!
%
% version 2.1
% added options norbool & normboolb for normalisation of bias & dndm
% 01.02.2010 KM
% version 2
% 07/08/08 KMarkovic
% version 3
% So, now I use the 3D Pk_nl from the halo model instead of the
% strange 2D way from Cooray et al. (well it's an option).
% 08.04.2010 KM
% version 3.1
% possibility to suppress calculation of fiducial cls - if 6 outputs
% 30.04.2010 KM

%% Check
if isfield(nzpars,'zs')
    if  nzpars.nbin~=1
        fprintf('Need nbin = 1 if have zs!\n');
        nzpars.nbin = 1;
        nbin = 1;
        pause
    end
end

if z_vals(1) == 0
    fprintf(1,'Cannot set redshifts to 0, since then have singularity in critical surface density!\n')
    z_vals(1) = 10^(-4)*z_vals(2);
end

% Suppress halofit
boodn = 0;
if ~exist('supnl','var') 
    if nargout == 6 
        boon = 0;
        boodn = 1;
    else
        boon = 1; 
    end
elseif supnl
    boon = 0;
else
    boon = 1;
end
cls_fid = NaN; 

% Suppress full halo
boodh = 0; booh = 0;
if exist('M_vals','var')&&~strcmp(nzpars.meth,'halo') 
    if nargout == 6
        boodh = 1;
    else
        booh = 1;
    end
end
halo_fid = NaN;

if exist('nbin','var')
    %fprintf('\tRewriting number of bins in nzpars!\n')
    nzpars.nbin = nbin;
end

if ~isfield(nzpars,'meth')
    type = 'p&d'; % method to make matter power spectrum non-linear
    error('\t *Using Peacock & Dodds to do the nonlinear matter ps!*\n');
else
	type = nzpars.meth; %01.08.2013 KM
end

kmin = NaN; kmax = NaN;
if ~exist('normbool','var') normbool = 3; end % do not normalise mass function
if ~exist('normboolb','var') normboolb = 0; end % normalise bias

%% First calculate the fiducial spectrum:

% Non-halo way:
if ~exist('M_vals','var') M_vals = logspace(5,16,50); end
if boon
    % Matter power spectrum
    if strcmp(type,'readsmith') %07.07.2011 KM
        k_vals = logspace(-2.4,2,1000);
    else
        %k_vals = logspace(-4,4,1000);
        k_vals = logspace(-2.4,2,1000);
    end
    if strcmp(type,'halo')
        pk_vals = pk_nlin_get(cospars_fid,k_vals,z_vals,type,NaN,M_vals);
    else
        pk_vals = pk_nlin_get(cospars_fid,k_vals,z_vals,type);
        %pk_vals = pk_nlin_get(cospars_fid,k_vals,z_vals,type);
    end
    
    [cls_fid noise] = lens_ps(cospars_fid,z_vals,l_vals,k_vals,pk_vals,nzpars);
end
% Isn't this now in units of h??? 28.10.2009 KM - NO 16.11.09

% Halo way
if booh
    if strcmp(type,'halo') warning('Doing halo model twice!'); end
    if 0 % 3D way - takes longer - OFF
        nzparsH = nzpars;
        nzparsH.meth = 'halo';
        % Matter power spectrum
        k_vals = logspace(-4,4,1000);
        [pk_vals tmp pk_lin] = pk_nlin_get(cospars_fid,k_vals,z_vals,nzparsH.meth,NaN,M_vals);
		
        [halo_fid noise] = lens_ps(cospars_fid,z_vals,l_vals,k_vals,pk_vals,nzparsH);
        
    else % 2D way - quicker - THIS IS ON
        k_vals = logspace(-4,4,1000);
        if isfield(cospars_fid,'Gamma')
            Gamma = cospars_fid.Gamma;
 
            funct = @(x) Gamma - cospars_fid.omega_m*x*exp(-2*cospars_fid.omega_b*x);
            cospars_fid.h = fzero(funct,[0 1.5]);
        else
            Gamma = exp(-2*cospars_fid.omega_b*cospars_fid.h) *cospars_fid.omega_m *cospars_fid.h;
        end
        if isfield(cospars_fid,'As')
            if ~isfield(cospars_fid,'k_pivot') error('No k_pivot!'); end
            pk_lin = pkf(Gamma,cospars_fid.sigma8,k_vals,cospars_fid.ns,cospars_fid.n_run,cospars_fid.k_pivot,cospars_fid.As);
        else
            pk_lin = pkf(Gamma,cospars_fid.sigma8,k_vals,cospars_fid.ns,cospars_fid.n_run);
        end

		%figure(3); loglog(k_vals,pk_lin); hold on
       
        [halo_fid pf noise] = lenshalo_tomo(cospars_fid,z_vals,l_vals,k_vals,...
            pk_lin,nzpars,M_vals,kmin,kmax,normbool,normboolb);
        
        %    dz = (max(z_vals)-z_vals(1))/length(z_vals);
        %    halo_fid_tmp = lenshalo_ps(cospars_fid,l_vals,k_vals,pk_lin,z_vals,M_vals,...
        %        nzpars.zs,dz);
        %    % While we have no tomography need it all in the third-D:
        %    for int = 1:length(l_vals) halo_fid(:,:,int) = halo_fid_tmp(int); end
    end
end



%% Now find the dCls/dpar if it is requested
if nargout>3
    
    dcls = NaN; dhalo = NaN;
    
    % Initialise the objects:
%     cls_plus.names = fieldnames(cospars_fid);
%     halo_plus.names = fieldnames(cospars_fid);
%     cls_minus.names = fieldnames(cospars_fid);
%     halo_minus.names = fieldnames(cospars_fid);
    cls_plus.names = fish.names;
    halo_plus.names = fish.names;
    cls_minus.names = fish.names;
    halo_minus.names = fish.names;
    
    % If none specified make steps of a thousandth
    %if ~exist('fish','var') || ~isfield(fish,'names')
    %    fish.names = fieldnames(cospars_fid);
    %end
    
    % Read fiducial values of cosmological parameters:
    count = 0; bool = 0;
    for int = 1:length(fish.names)
        if strcmp(fish.names{int},'logm')
            fid_vals(int) = getfield(cospars_fid,'m_wdm');
        elseif strcmp(fish.names{int},'invm')
            fid_vals(int) = getfield(cospars_fid,'m_wdm');
        else
            fid_vals(int) = getfield(cospars_fid,fish.names{int});
            
        end
        if fid_vals(int)==0 bool = 1;
        else count = count + 1;
            new_names{count} = fish.names{int};
        end
    end
    % Remove the ones that are 0:
    if bool
        fprintf(1,'If fiducial value of parameter = 0, it is left out!\n')
        fish.names = new_names';
    end
    
    
    for int = 1:length(fish.names)
        
        name = fish.names{int}; % Which par we are varying
        cospars = cospars_fid; % Erase the previous change
        %val_fid = getfield(cospars_fid,name); % Fiducial value
        val_fid = fid_vals(int);
        
        %fprintf(1,'***Working out the Cls for step in %s:***\n',name)
        
        
        for plusminus = 1:2
            
            cancelbool = 0; % this is to prevent double calc of plus step
            
            % Set default step size for the derivative - this is not
            % recommended!
            if ~isfield(fish,'steps')
                fprintf(1,'Taking steps of %s/100 in lens_ps_get.\n',name);
                step(int) = val_fid/100;
            else
                % Otherwise extract the step sizes from object
                step(int) = fish.steps(int);
            end
            
            
            % Add or subtract the step from the fiducial value
            if plusminus==1 % Means: fid + step
                if strcmp(name,'logm')
                    % Logarithmic parameter:
                    % logm + dlog(m) => m = 10^logm*10^dlogm
                    val_new = val_fid*10^(step(int));
                elseif strcmp(name,'invm')
                    %val_new = val_fid; % Cannot be negative!
                    %step(int) = step(int)/2;
                    val_new = 1/(1/val_fid + step(int));
                    %val_new = val_fid + step(int);
                else
                    val_new = val_fid + step(int);
                end
            elseif plusminus==2 % Means: fid - step
                if strcmp(name,'logm')
                    % Logarithmic parameter:
                    % logm - dlog(m) => m = 10^logm/10^dlogm
                    val_new = val_fid/10^(step(int));
                elseif strcmp(name,'invm')
                    %if step(int)>=1/val_fid 
                        val_new = val_fid; % Cannot be negative!
                        step(int) = step(int)/2; 
                        % Since we have half the interval and later we 
                        % multiply by 2!
                    %else
                    %    val_new = 1/(1/val_fid - step(int));
                        %val_new = val_fid - step(int);
                    %end
                    cancelbool = 1;
                else
                    val_new = val_fid - step(int);
                end
            end
            
            
            % Set new value in the object:
            if strcmp(name,'logm')
                cospars = setfield(cospars,'m_wdm',val_new);
            elseif strcmp(name,'invm')
                cospars = setfield(cospars,'m_wdm',val_new);
            else
                cospars = setfield(cospars,name,val_new);
            end                
            
%             % Okay, need to make sure we have a flat universe at all times:
%             if abs(cospars.omega_m+cospars.omega_de-1)>1e-15
%                 if strcmp(name,'omega_m')
%                     cospars.omega_de = 1 - cospars.omega_m;
%                 elseif strcmp(name,'omega_de')
%                     cospars.omega_m = 1 - cospars.omega_de;
%                 else fprintf('Uh-oh: non-zero curvature!!!\n')
%                 end
%             end
%             % Also, need to make sure we have a omega_m = omega_dm + omega_b:
%             if abs(cospars.omega_m-cospars.omega_dm-cospars.omega_b)>1e-15
%                 if strcmp(name,'omega_m')
%                     cospars.omega_b = cospars.omega_m - cospars.omega_dm;
%                 elseif strcmp(name,'omega_b')
%                     cospars.omega_dm = cospars.omega_m - cospars.omega_b;
%                 else fprintf('Uh-oh: matter does not add up!!!\n')
%                 end
%             end
            
            
            % Now calculae the power spectra
            
            % First the non-halo way:
            %[pk_vals tmp pk_lin] = pk_nlin_get(cospars,k_vals,z_vals,type);
            
            if cancelbool
                if boon||boodn
                    tmpN = cls_fid;
                end
                if booh||boodh
                    tmpH = halo_fid;
                end
            else
                
            if boon||boodn
                % Added this here to speed up on the 24.06.2010 KM
                [pk_vals , tmp, pk_lin] = pk_nlin_get(cospars,k_vals,z_vals,type); %changed tilde to tmp 22.12.2010 KM ????
                
                tmpN = lens_ps(cospars,z_vals,l_vals,k_vals,pk_vals,nzpars);
            end
            
            % Then the halo way:
            
            if booh||boodh
                
                % Added this here to speed up on the 24.06.2010 KM
                if isfield(cospars,'Gamma')
                    Gamma = cospars.Gamma;
                    funct = @(x) Gamma - cospars.omega_m*x*exp(-2*cospars.omega_b*x);
                    cospars.h = fzero(funct,[0 1.5]);
                else
                    Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
                end
                if ~exist('pk_lin','var')                  
                    if isfield(cospars,'As')
                        pk_lin = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run,cospars.k_pivot,cospars.As);
                    else
                        pk_lin = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);
                    end
                end
                
                %warning('errrrrr, should update this first!!')
                %tmptmp = lenshalo_ps(cospars,l_vals,k_vals,pk_lin,z_vals,M_vals,...
                %    nzpars.zs,dz);
                %%% While have no tomography:
                %for int = 1:length(l_vals) tmpH(:,:,int) = tmptmp(int); end
                tmpH = lenshalo_tomo(cospars,z_vals,l_vals,k_vals,pk_lin,...
                    nzpars,M_vals);
            end
            
            end
            
            
            % Now put it into the object:
            if plusminus==1
                %if boon cls_plus = setfield(cls_plus,name,tmpN); end
                %if booh halo_plus = setfield(halo_plus,name,tmpH); end
                if boon cls_plus.(name) = tmpN; end
                if booh halo_plus.(name) = tmpH; end
            elseif plusminus==2
                %if boon cls_minus = setfield(cls_minus,name,tmpN); end
                %if booh halo_minus = setfield(halo_minus,name,tmpH); end
                if boon cls_minus.(name) = tmpN; end
                if booh halo_minus.(name) = tmpH; end
            end
            
            clear pk_vals tmp pk_lin tmpN tmpH
        end
    end
    %if ~isfield(fish,'steps') fish.steps = step'; end
    % Because step into plus and minus around fiducial model
    if ~isfield(fish,'steps')
        fish.steps = 2*step';
    else
        fish.steps = 2*fish.steps;
    end
    
    % Now can put the cls onject directly into the function I already have:
    if boon dcls = dcls_dparam(fish.steps,fish.names,cls_minus,cls_plus); end
    if booh dhalo = dcls_dparam(fish.steps,fish.names,halo_minus,halo_plus); end
end


%% 

if 0 && nargout>3
    figure(3);
    plotlens(l_vals,cls_plus.(name)(2,2,:),'r'); hold on
    plotlens(l_vals,cls_fid(2,2,:),'k.');
    val_fid
    val_new
end