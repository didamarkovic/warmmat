function [C_l_ij noisemat pf] = lens_ps(cospars,z_vals,l_vals,...
                                            k_vals,Pk_vals,nzpars)

% Lensing power spectrum from Takada&Jain 0310125v4
%
% cospars - use setpars
% Pk_vals - use pk_nlin_get, units: [h^3/Mpc^3]
% nzpars - object of parameters needed in get_nofz to find no. of gal
% nzpars.zs - input if want all sources to appear at one redshift
%
% [C_l_ij noisemat] = lens_ps(cospars,z_vals,l_vals,k_vals,Pk_vals,nzpars)
%
% To get C_l_ij_obs = C_l_ij + noisemat.
%
%
% version 3.2 - nothing is different from before, really
% 16.11.09 KMarkovic

if exist('nzpars','var')
    
    if ~isfield(nzpars,'nbin') nzpars.nbin = 1; end
    
    nbin = nzpars.nbin; 

else nbin = 1;
end


% Find distances to and between all z_vals:
chimat = Ds_wconst(z_vals,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000); % [Dh]

chi = Ds_wconst(0,z_vals,cospars.omega_m,cospars.omega_de,cospars.w0,1000); % [Dh]
if isfield(nzpars,'zs')
    chi_source = Ds_wconst(0,nzpars.zs,cospars.omega_m,cospars.omega_de,cospars.w0,1000); %[Dh]
end
dchi = chi(2:length(chi))-chi(1:(length(chi)-1));
dchi(length(chi)) = interp1(z_vals(1:(length(z_vals)-1)),dchi,max(z_vals),'spline','extrap');

% Note that all distances so far are in units of Dh so convert them:
c_km = 299792.5; % [km s^-1]
Dh = c_km/100; % [Mpc/h] - This means Cls are in units of h if Pks unitless!!!
chi = chi * Dh; % [Mpc/h] - hopefully now the distances are only in units of Mpc
chimat = chimat * Dh; % [Mpc/h]
dchi = dchi * Dh; % [Mpc/h]
if isfield(nzpars,'zs')
    chi_source = chi_source * Dh; % [Mpc/h]
end


% Find weighting functions for all z-bins at all chi (0 if not in bin):
[n_z_mat niz] = get_nofz(nzpars,z_vals);


% Lensing weight
if ~isfield(nzpars,'zs')
    
    
    Wi_chi = lens_weight(cospars,z_vals,chimat,n_z_mat);%,nzpars.ng); % [h/Mpc]
    % Iffy units...

else

    Wi_chi = lens_weight(cospars,z_vals,chi,n_z_mat,nzpars.zs,chi_source);% [h/Mpc]

end



% Interpolate the non-linear matter power spectrum (Limber approx.):
new_k = zeros(length(chi),length(l_vals));
new_Pk = zeros(length(chi),length(l_vals));
warning off
for index = 1:length(chi)
    new_k(index,:) = l_vals / ( chi(index) );%*cospars.h ); % [h/Mpc]
    new_Pk(index,:) = interp1(k_vals,Pk_vals(index,:),new_k(index,:)); %[h^3/Mpc^3]
    % Old test scripts don't work here, since Pk_vals must be grown!
end
warning on
%figure; loglog(new_k(20,:),new_Pk(20,:),'r--'); hold on
%loglog(k_vals,Pk_vals,'b--')
% Now have new_Pk matrix that has columns differing in l and rows in chi!

% Delete the bad ones out of the k_range
new_Pk(isnan(new_Pk))=0; % Don't know if this is good? - Well, values very small!

% Also use kmax&kmin if exist 28/08/08:
if isfield(nzpars,'kmax') new_Pk(new_k>nzpars.kmax) = 0; end
if isfield(nzpars,'kmin') new_Pk(new_k<nzpars.kmin) = 0; end


C_l_ij = zeros(nbin,nbin,length(l_vals));

for ibin = 1:nbin

    wibin = Wi_chi(:,ibin); % Extract Wi for ibin

    
    for jbin = 1:nbin
        if ibin <= jbin % Since symmetrical matrix
            
            wjbin = Wi_chi(:,jbin); % Extract Wj for jbin
            
            
            for lint = 1:length(l_vals)

                pkls = new_Pk(:,lint);

                window = ( dchi  .* (1 * chi.^-(2) ) )' .* wibin .* wjbin; % [h^3/Mpc^3]

                dInt = window .* pkls; % [unitless]
                
                %saveint(:,lint) = dInt;

                C_l_ij(ibin,jbin,lint) = sum(dInt);
                C_l_ij(jbin,ibin,lint) = C_l_ij(ibin,jbin,lint);
                %clear dInt
            end
            
            
        end
    end

    
end

% Now add the shot noise - none between different bins:
if nargout==2 || nargout==3
    noisemat = zeros(size(C_l_ij));
    for ibin = 1:nbin
        shotnoise = (nzpars.sigma_gamma^2) / niz(ibin);
        noisemat(ibin,ibin,:) = noisemat(ibin,ibin,:) + shotnoise;
    end
end

% Factor for plotting:
 pf = l_vals.*(l_vals+1)./(2*pi);