% figureA
%  Comparing P_k calculations for WDM. We plot the fit to simulations,
%  modified halo model, halofit and the linear spectrum. This we do for 2
%  different m_wdm and z.
%
% parent: compare_sims, 23.05.2011 KM
%
% 07.07.2011 KMarkovic

clear; %clf

type1 = 'vielfit';%'readsmith'; %'halo';
typename1 = 'Fit to simulations';

%type2 = 'readsmith'; %
type2 = 'warmhalo';
typename2 = 'Modified halo model';

type3 = 'smith2';
typename3 = 'Halofit';

% Redshifts
zchoose = [3 4];
zs = [5,2,1,0.5,0];
z_snaps = [10 5 3 2.5 2 1.8 1.6 1.4 1.2 1 0.9:-0.1:0];

modelnos = [1 4 5];

posmasses = [0,4,2,1,0.5];
posmodels = {'LCDM';'WDM4';'WDM2';'WDM1';'WDM0.5'};

masses = posmasses(modelnos);
models = posmodels(modelnos);

tbin = 30;
mmod = 15;




%% Simulations

tic 

path = '/Users/katarina/Desktop/Research/WDM/2011_Vieletal/Matteo-TABLES/';

simsize = '25_512';

nulls = '000';

check = Inf;
kvalssims = zeros(length(masses),10000,length(zchoose));
pkvalssims = kvalssims;
ratios = kvalssims;


for zz = 1:length(zchoose)
    
    redshiftno = find(z_snaps==zs(zchoose(zz)))-1;
    extrazeros = nulls(1:(3-length(num2str(redshiftno))));
    
    for ii = 1:length(models)
        
        filename = [path,models{ii},'_',simsize,'/powerspec_',extrazeros,num2str(redshiftno),'.txt'];
        
        [data header] = ppowerread(filename);
        [kvals pkvals noise] = ppowergetspec(data,tbin,mmod);
        
        if length(kvals.B)<check
            check = length(kvals.B);
            
            kvalssims = kvalssims(:,1:check);
            pkvalssims = pkvalssims(:,1:check);
            ratios = ratios(:,1:check);
        end
        
        kvalssims(ii,:) = kvals.B;
        tmp = pkvals.B;
        
        if sum(kvalssims(ii,:)~=kvalssims(1,:))>0
            pkvalssims(ii,:,zz) = interp1(kvalssims(ii,:),tmp,kvalssims(1,:));
        else
            pkvalssims(ii,:,zz) = tmp;
        end
        
        ratios(ii,:,zz) = pkvalssims(ii,:,zz)./pkvalssims(1,:,zz)-1;
        
    end
    
    
end

toc


%% Theory

k_vals = logspace(-2,2,1000);

cospars = setEUCLIDpars;
cospars.omega_wdm = cospars.omega_dm;

Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin_cdm_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);

pk_lin_wdm = zeros(length(masses),length(k_vals),length(zchoose));
rat_lin = pk_lin_wdm;
pk_1 = pk_lin_wdm; rat_1 = pk_lin_wdm;
pk_2 = pk_lin_wdm; rat_2 = pk_lin_wdm;
pk_3 = pk_lin_wdm; rat_3 = pk_lin_wdm;

for zz = 1:length(zchoose)
    
    redshiftno = find(z_snaps==zs(zchoose(zz)))-1;
    z_vals = z_snaps(redshiftno+1);
    
    for ii = 1:length(masses) % clf
        
        cospars.m_wdm = 1000*masses(ii);
        
        pk_lin_wdm(ii,:,zz) = pk_wdmtransf(cospars,z_vals,k_vals,pk_lin_cdm_0); % [Mpc^3/h^3]
        rat_lin(ii,:,zz) = pk_lin_wdm(ii,:,zz)./pk_lin_wdm(1,:,zz) - 1;
        
        pk_1(ii,:,zz) = pk_nlin_get(cospars,k_vals,z_vals, type1);
        rat_1(ii,:,zz) = pk_1(ii,:,zz)./pk_1(1,:,zz) - 1;
        
        pk_2(ii,:,zz) = pk_nlin_get(cospars,k_vals,z_vals, type2);
        rat_2(ii,:,zz) = pk_2(ii,:,zz)./pk_2(1,:,zz) - 1;
        
        pk_3(ii,:,zz) = pk_nlin_get(cospars,k_vals,z_vals, type3);
        rat_3(ii,:,zz) = pk_3(ii,:,zz)./pk_3(1,:,zz) - 1;
        
    end
    
    
end

%% Plotting all

%clf;

%cols = 'kbrcmgy';
cols = 'kbbggcy';
ftsz = 14;

for zz = 1:length(zchoose)
    
    
    for ii = 2:length(masses)
        
        plotno = (ii-1) + (zz-1)*(length(masses)-1);
        subplot(length(zchoose),length(masses)-1,plotno)
        
        semilogx(k_vals,0*k_vals,'k--'); hold on
        
        if ii~=1
            c = semilogx(k_vals,100*rat_2(ii,:,zz),...
                [cols(ii+length(zchoose)) '-']);
            e = semilogx(k_vals,100*rat_3(ii,:,zz),...
                [cols(ii+length(zchoose)) '--']);
            a = semilogx(kvalssims(ii,:),100*ratios(ii,:,zz),...
                [cols(ii) 'd']);
            b = semilogx(k_vals,100*rat_1(ii,:,zz),[cols(ii) '-']);
        end
        d = semilogx(k_vals,100*rat_lin(ii,:,zz),['k' ':']); hold on
        
        if ii ~= 1
            set([a b c e],'linewidth',1.2)
            set(gca,'fontsize',ftsz)
        end
        
        if zz==1 && ii==2
            legend([a b c e d],'Simulations',typename1,typename2,...
                typename3,'Linear',3)
        end
        
        text(0.13,-16,[num2str(masses(ii)) ' keV'],'fontsize',ftsz)
        text(0.13,-14,['z = ' num2str(zs(zchoose(zz)))],'fontsize',ftsz)
        axis([0.1 max(k_vals) -30 2]);
        xlabel('k[h/Mpc]')
        ylabel('100(P_W_D_M(k)-P_C_D_M(k))/P_C_D_M(k)')
        
        plottight(gca,length(zchoose),length(masses)-1,plotno,0.1,0.075);
        
    end
    
    
end

% Incompatible with Octave:
%screen = get(0,'ScreenSize');
%move = [200 1];
%enlarge = [(screen(3)-move(1))/screen(3) 1];
%set(gcf,'OuterPosition',screen.*[move enlarge]);