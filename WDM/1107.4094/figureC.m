% figureC 
%
% parent: lenshalo_tomo_demo_vielfit, 04.07.2011 KM
% grandparent: lenshalo_tomo_demo, 15.10.09 KM
%
% 07.07.2011 KM


clear
%clf

tic

cospars = setEUCLIDpars;
cospars.omega_wdm = cospars.omega_dm;

mvals = [0 1000 500];

dz = 0.1; z_vals = 0.0001:dz:2.0001;

l_vals = logspace(1,4,20);
k_vals = logspace(-4,4,1000);

%nzpars = setnzpars([0.1 100*(60*180/pi)^2 2],{'fsky','ng','nbin','vielfit'});
nzpars = setnzpars([8 0.5],{'nbin','zsmin','vielfit'});
%nzpars.zsmin = 0.5;

tmp = min(z_vals):0.01:max(z_vals);
[n_z_mat niz] = get_nofz(nzpars,tmp);
bincents = 1:nzpars.nbin;
for ii = 1:nzpars.nbin
    bincents(ii) = tmp(max(n_z_mat(:,ii))==n_z_mat(:,ii));
end
%figure; plot(tmp,n_z_mat); hold on; plot(tmp,sum(n_z_mat,2),'k')
ib = [5 5 8]; jb = [5 8 8];
zi = bincents(ib);
zj = bincents(jb);

forleg=''; forlegw = '';

C_l_ij = NaN;
for mm = 1:length(mvals)
    cospars.m_wdm = mvals(mm);
    
    if mm==2
        previous_C_l_ij = C_l_ij;
    end
    
    [C_l_ij tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
    
    if 0
        for ii = 1:length(ibin)
            n = noise(ibin(ii),ibin(ii),:);
            [h pf] = plotlens(l_vals,squeeze((C_l_ij(ibin(ii),ibin(ii),:))),col(mm),1); hold on
            loglog(l_vals,squeeze((C_l_ij(ibin(ii),ibin(ii),:))),col(mm)); hold on
            plot(l_vals,pf'.*squeeze(n),[col(mm) '--']);
            plot(l_vals,squeeze(n),[col(mm) '--']);
            axis([10 3000 1e-6 1e-3])
        end
    end
    
    % Ratios:
    if mm~=1
        ra = (C_l_ij)./ (previous_C_l_ij) - 1; % Within 1%
        ratio = (C_l_ij+noise)./ (previous_C_l_ij+noise) - 1; % Within 1%
        ns = - ra .* noise./ (previous_C_l_ij + noise); % Within 1%
    %else
        % Work out the error bars for Euclid
        CC = covmat(l_vals,C_l_ij+noise,nzpars.fsky);
    end
    %end
    
    toc
    
    
    %% Plotting
    
    if mm~=1
        
        subplot(1,length(mvals)-1,mm-1)
        
        if mm==2
            %figure(1); clf
            col = 'bcg';
        elseif mm==3
            %figure(2); clf
            col = 'bcg';
            %col = 'yrm';
        end
        
        semilogx(1:1e4:1e5,0*(1:1e4:1e5),'k--'); hold on % clf
        %[p pf] = plotlens(l_vals,squeeze(C_l_ij(1,1,:)),'w--',1);
        for ii=1:length(ib)
            
            errors = squeeze(sqrt(CC(ib(ii),jb(ii),ib(ii),jb(ii),:)));
            p_spectr = squeeze(previous_C_l_ij(ib(ii),jb(ii),:));
            spectr = squeeze(C_l_ij(ib(ii),jb(ii),:));
            nois = squeeze(noise(ib(ii),jb(ii),:));
            
            %semilogx(l_vals,squeeze(ratio(ib(ii),jb(ii),:))*100,col(ii));
            %errorbar(l_vals,squeeze(ra(ib(ii),jb(ii),:))*100,errors./spectr,col(ii));
            
            if mm==2
                r(ii) = errorbar(l_vals,squeeze(ratio(ib(ii),jb(ii),:))*100,...
                    errors./spectr,[col(ii) '-']);
                set(r,'linewidth',1.05)
            elseif mm==3
                p(ii) = errorbar(l_vals,squeeze(ratio(ib(ii),jb(ii),:))*100,...
                    errors./spectr,[col(ii) '-']);
                set(p,'linewidth',1.2)
            end
            %semilogx(l_vals,-squeeze(ns(ib(ii),jb(ii),:))*100,[col(ii) ':']);
            
            %errorbar(l_vals,zeros(size(l_vals)),errors./spectr,'kx');
            
            %errorbar(l_vals,pf'.*spectr,pf'.*errors,pf'.*errors,col(ii));
            %p(ii) = plotlens(l_vals,spectr,col(ii));
            %errorbar(l_vals,pf'.*p_spectr,pf'.*errors,pf'.*errors,'k');
            %plotlens(l_vals,spectr+errors,[col(ii) ':']);
            %plotlens(l_vals,spectr-errors,[col(ii) ':']);
            %semilogx(l_vals,pf'.*nois,[col(ii) '--']);
            
            fontsz = 14;
            if mm==2
                %forlegw{ii} = [num2str(cospars.m_wdm/1000) ...
                %    ' keV: bin ' num2str(ib(ii)) '-' num2str(jb(ii))...
                %    ', z ~' num2str(zi(ii)) '-' num2str(zj(ii))];
                %forlegw{ii} = ['bin ' num2str(ib(ii)) '-' num2str(jb(ii))];
                forleg{ii} = ['z = (' num2str(round2dp(zi(ii),1),'%1.1f') ','...
                    num2str(round2dp(zj(ii),1),'%1.1f') ')'];
                legend(r,forleg,3)
                axis([l_vals(1) l_vals(end) -0.5 0.05])
                ylabel('100(C_\kappa_,_i_j^C^D^M-C_\kappa_,_i_j^W^D^M)/C_\kappa_,_i_j^C^D^M')
                xlabel('multipole l')
                %set(gca,'PlotBoxAspectRatioMode','manual')
                %axis fill
                %text(10,-0.36,...
                %    ['bins at  z = ' num2str(round2dp(zj(1),1)) ' & ' ...
                %    num2str(round2dp(zj(3),1))],'fontsize',fontsz)
                text(10,-0.4,'1 keV','fontsize',fontsz)
            elseif mm==3
                %forleg{ii} = [num2str(cospars.m_wdm/1000) ...
                %    ' keV: bin ' num2str(ib(ii)) '-' num2str(jb(ii))...
                %    ', z ~' num2str(zi(ii)) '-' num2str(zj(ii))];
                %forleg{ii} = ['bin ' num2str(ib(ii)) '-' num2str(jb(ii))];
                %forleg{ii} = ['z = (' num2str(round2dp(zi(ii),1),'%1.1f') ','...
                %    num2str(round2dp(zj(ii),1),'%1.1f') ')'];
                %legend(p,forleg,3)
                axis([l_vals(1) l_vals(end) -0.5 0.05])
                ylabel('100(C_\kappa_,_i_j^C^D^M-C_\kappa_,_i_j^W^D^M)/C_\kappa_,_i_j^C^D^M')
                xlabel('multipole l')
                %set(gca,'PlotBoxAspectRatioMode','manual')
                %axis fill
                %text(10,-0.36,...
                %    ['bins at  z = ' num2str(round2dp(zj(1),1)) ' & ' ...
                %    num2str(round2dp(zj(3),1))])%,'fontsize',fontsz)
                text(10,-0.4,'0.5 keV','fontsize',fontsz)
            end
            
            set(gca,'fontsize',fontsz);
        end
        
        plottight(gca,1,length(mvals)-1,mm-1,0.1,0.075);
        
        
    end
    
end

screen = get(0,'ScreenSize'); 
move = [200 1]; 
enlarge = [(screen(3)-move(1))/screen(3) 0.7];
set(gcf,'OuterPosition',screen.*[move enlarge]);
%get(gcf,'outerposition')
%saveas(gcf,'figureA','eps')

%legend([r p],[forlegw forleg],3)
%axis([l_vals(1) l_vals(end) -0.5 0.05])
%ylabel('100(C_\kappa_,_i_j^C^D^M-C_\kappa_,_i_j^W^D^M)/C_\kappa_,_i_j^C^D^M')
%xlabel('multipole, l')