% Compare the WDM simulations ran by Matteo Viel and me with the halo
% model, halofit and hopefully the new WDM halo model by Smith & Markovic.
%
% Takes more than a minute to run.
%
% 23.05.2011 KMarkovic
% 19.09.2012 - Redoing stuff for a talk

clear all; clf

type1 = 'vielfit';%'readsmith'; %'halo';
type2 = 'smith2'; 'readsmith';
type3 = 'warmhalo';

specbool = 0;

docooling = 1;

% Redshifts
zchoose = 3;
zs = [5,2,1,0];
z_snaps = [10 5 3 2.5 2 1.8 1.6 1.4 1.2 1 0.9:-0.1:0];
redshiftno = find(z_snaps==zs(zchoose))-1;

modelnos = [1 4];

posmasses = [0,4,2,1,0.5];
posmodels = {'LCDM';'WDM4';'WDM2';'WDM1';'WDM0.5'};

masses = posmasses(modelnos);
models = posmodels(modelnos);

tbin = 30;
mmod = 15;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulations

tic

%path = '/Users/katarina/Desktop/Research/WDM/2011_Vieletal/Matteo-TABLES/';
path = '/Users/dida/Desktop/Science/WDM/2012_Vieletal/Matteo-TABLES/';

simsize = '25_512';

nulls = '000';
extrazeros = nulls(1:(3-length(num2str(redshiftno))));

check = Inf;
kvalssims = zeros(length(masses),10000);
pkvalssims = kvalssims;
ratios = kvalssims;
for ii = 1:length(models)
    
    filename = [path,models{ii},'_',simsize,'/powerspec_',extrazeros,num2str(redshiftno),'.txt'];
    
    [data header] = ppowerread(filename);
    [kvals pkvals noise] = ppowergetspec(data,tbin,mmod);
    
    %     if length(kvals.total)<check
    %         check = length(kvals.total);
    if length(kvals.B)<check
        check = length(kvals.B);
        
        kvalssims = kvalssims(:,1:check);
        pkvalssims = pkvalssims(:,1:check);
        ratios = ratios(:,1:check);
    end
    
    %     kvalssims(ii,:) = kvals.total;
    %     tmp = pkvals.total;
    kvalssims(ii,:) = kvals.B;
    tmp = pkvals.B;
    
    if sum(kvalssims(ii,:)~=kvalssims(1,:))>0
        pkvalssims(ii,:) = interp1(kvalssims(ii,:),tmp,kvalssims(1,:));
    else
        pkvalssims(ii,:) = tmp;
    end
    
    ratios(ii,:) = pkvalssims(ii,:)./pkvalssims(1,:)-1;
    
end
%semilogx(kvalssims(1,:),ratios); hold on

toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulations with cooling

if docooling
    
    warning('Make sure you have the right pars for cooling!')
    
    tic
    
    %path = '/Users/katarina/Desktop/Research/WDM-sims/Matteo-TABLES/';
    
    simsize = '25_512';
    
    nulls = '000';
    extrazeros = nulls(1:(3-length(num2str(redshiftno))));
    
    check = Inf;
    kvalssims_c = zeros(length(masses),10000);
    pkvalssims_c = kvalssims_c;
    ratios_c = kvalssims_c;
    for ii = 1:length(models)
        
        filename = [path,'power_',models{ii},'_',simsize,'_B_',extrazeros,num2str(redshiftno),'.txt'];
        
        [data header] = ppowerread(filename);
        [kvals pkvals noise] = ppowergetspec(data,tbin,mmod);
        
        %     if length(kvals.total)<check
        %         check = length(kvals.total);
        if length(kvals.B)<check
            check = length(kvals.B);
            
            kvalssims_c = kvalssims_c(:,1:check);
            pkvalssims_c = pkvalssims_c(:,1:check);
            ratios_c = ratios_c(:,1:check);
        end
        
        %     kvalssims(ii,:) = kvals.total;
        %     tmp = pkvals.total;
        kvalssims_c(ii,:) = kvals.B;
        tmp = pkvals.B;
        
        if sum(kvalssims_c(ii,:)~=kvalssims_c(1,:))>0
            pkvalssims_c(ii,:) = interp1(kvalssims_c(ii,:),tmp,kvalssims_c(1,:));
        else
            pkvalssims_c(ii,:) = tmp;
        end
        
        ratios_c(ii,:) = pkvalssims_c(ii,:)./pkvalssims_c(1,:)-1;
        
    end
    %semilogx(kvalssims(1,:),ratios); hold on
    
    toc
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Theory

tic

k_vals = logspace(-2,2,1000);

cospars = setEUCLIDpars;
cospars.omega_wdm = cospars.omega_dm;

z_vals = z_snaps(redshiftno+1);

Gamma = exp(-2*cospars.omega_b*cospars.h) *cospars.omega_m *cospars.h;
pk_lin_cdm_0 = pkf(Gamma,cospars.sigma8,k_vals,cospars.ns,cospars.n_run);

pk_lin_cdm_0 = pk_lin_cdm_0 * growth_factor(cospars,z_vals)^2;

pk_lin_wdm = zeros(size(pk_lin_cdm_0)); rat_lin = pk_lin_wdm;
pk_1 = pk_lin_wdm; rat_1 = pk_lin_wdm;
pk_2 = pk_lin_wdm; rat_2 = pk_lin_wdm;
pk_3 = pk_lin_wdm; rat_3 = pk_lin_wdm;
for ii = 1:length(masses) % clf
    
    cospars.m_wdm = 1000*masses(ii);
    
    pk_lin_wdm(ii,:) = pk_wdmtransf(cospars,z_vals,k_vals,pk_lin_cdm_0); % [Mpc^3/h^3]
    rat_lin(ii,:) = pk_lin_wdm(ii,:)./pk_lin_wdm(1,:) - 1;
    
    pk_1(ii,:) = pk_nlin_get(cospars,k_vals,z_vals, type1);
    rat_1(ii,:) = pk_1(ii,:)./pk_1(1,:) - 1;
    
    pk_2(ii,:) = pk_nlin_get(cospars,k_vals,z_vals, type2);
    rat_2(ii,:) = pk_2(ii,:)./pk_2(1,:) - 1;
    
    %pk_3(ii,:) = pk_nlin_get(cospars,k_vals,z_vals, type3);
    %rat_3(ii,:) = pk_3(ii,:)./pk_3(1,:) - 1; 
	%figure; loglog(k_vals,pk_3(2,:),'r--'); hold on
    
end

% Put it into physical units - EEEK???
%k_vals = k_vals/cospars.h;
%pk_lin_wdm_0 = pk_lin_wdm_0*cospars.h^3;
%pk_halo = pk_halo*cospars.h^3;
%pk_smith = pk_smith*cospars.h^3;

toc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plotting CDM

%clf
%loglog(kvalssims(1,:),pkvalssims(1,:),'k.'); hold on
%loglog(k_vals,pk_smith(1,:),'k');
%xlabel('k[h/Mpc]')
%ylabel('(P(k)')
%% Plotting all

clf;

%subplot(2,2,zchoose);

%cols = 'kbcgrmy';
cols = 'krbm';

%semilogx(k_vals,rat_halo); hold on

for ii = 1:length(masses)
    if ~specbool
        if ii~=1
            if docooling
                e = semilogx(kvalssims_c(ii,:),100*ratios_c(ii,:),[cols(ii+1) 'o']);
			else 
				c = semilogx(k_vals,100*rat_2(ii,:),[cols(ii+2) '-']); hold on
			end
            a = semilogx(kvalssims(ii,:),100*ratios(ii,:),[cols(ii) 'd']); hold on
            b = semilogx(k_vals,100*rat_1(ii,:),[cols(ii) '-']); hold on
            %e = semilogx(k_vals,rat_3(ii,:),['y' '-.']); hold on
        end
        d = semilogx(k_vals,100*rat_lin(ii,:),[cols(ii) ':']); hold on
    elseif specbool==2
        d2sims = kvalssims(ii,:).^3.*pkvalssims(ii,:)/2*pi^2 /cospars.h^3;%????
        %d23 = k_vals.^3.*pk_3(ii,:)/2*pi^2;
        d22 = k_vals.^3.*pk_2(ii,:)/2*pi^2;
        d21 = k_vals.^3.*pk_1(ii,:)/2*pi^2;
        d2lin = k_vals.^3.*pk_lin_wdm(ii,:)/2*pi^2;
        loglog(kvalssims(ii,:),d2sims,[cols(ii) 'o']); hold on
        %loglog(k_vals,d23,cols(ii)); hold on
        loglog(k_vals,d22,[cols(ii) '-']); hold on
        loglog(k_vals,d21,[cols(ii) '--']); hold on
        loglog(k_vals,d2lin,[cols(ii) '-.']); hold on
    else
        a = loglog(kvalssims(ii,:),pkvalssims(ii,:),[cols(ii) 'd']); hold on
		if docooling
			e = loglog(kvalssims_c(ii,:),pkvalssims_c(ii,:),[cols(ii) 'o']); 
		else
			c = loglog(k_vals,pk_2(ii,:),[cols(ii+2) '-']); hold on
		end 
		b = loglog(k_vals,pk_1(ii,:),[cols(ii) '-']); hold on
        %loglog(k_vals,pk_3(ii,:),[cols(ii) '-.']); hold on
		d = loglog(k_vals,pk_lin_wdm(ii,:),[cols(ii) '-']); hold on
    end
    
    if ii ~= 1
        if docooling
            set([a b e],'linewidth',2)
        else
            set([a b],'linewidth',2)
        end
        set(gca,'fontsize',20)
    end
    
    if ii==2 %&& zchoose==3
        if docooling
            %legend([a b c d e],'Simulations',['Theory: ' type1],['Theory: ' type2],'Theory: Linear','Simulations with cooling',3)
			legend([a e b d],'Sims','Cool','NLin fit','Linear',"location","southwest")
        else
            %legend([a b c e d],'Simulations',['Theory: ' type1],['Theory: ' type2],['Theory: ' type3],'Theory: Linear',3)
            %legend([a b c d],'Simulations',['Theory: ' type1],['Theory: ' type2],'Theory: Linear',3)
			legend([a b c d],'Sims','NLin fit','Lin fit','Linear',"location","southwest")
        end
    end
    
end
if ~specbool
    %axis([min(k_vals) max(k_vals) -0.50 0.05]);
    axis([0.1 max(k_vals) -50 2]);
	ylabel('100(Pk_W_D_M-Pk_C_D_M)/Pk_C_D_M')
elseif specbool==2
    axis([0.1 100 1e-1 1e6]);
	ylabel('\Delta(k)')
else
    %axis([min(kvalssims(1,:)) max(kvalssims(1,:)) 1e-5 1e5]);
    axis([min(k_vals) max(k_vals) 1e-5 1e4/z_vals]);
	ylabel('P(k)')
end
titlestring = ['z = ' num2str(z_vals) ' ; m_W_D_M = ' num2str(masses(2:end)) ' keV'];
title(titlestring)
xlabel('k[h/Mpc]')


% For Octave:
h=gcf;
set (h,'papertype', '<custom>')
set (h,'paperunits','centimeters');
set (h,'papersize',[3 2.5])
set (h,'paperposition', [0,0,[3 2.5]])
set (h,'defaultaxesposition', [0.15, 0.15, 0.75, 0.75])
set (0,'defaultaxesfontsize', 20)
