% figureB
%
% parent : figureC, 07.07.2011 KM
% grandparent: lenshalo_tomo_demo_vielfit, 04.07.2011 KM
% greatgrandparent: lenshalo_tomo_demo, 15.10.09 KM
%
% 07.07.2011 KM


clear

meth{1} = 'vielfit';
methname{1} = 'Fit to simulations';
sty{1,1} = 'b-'; sty{1,2} = 'b-';%'r-';

meth{2} = 'readsmith';
methname{2} = 'Modified halo model';
sty{2,1} = 'g-'; sty{2,2} = 'g-';%'m-';

meth{3} = 'smith2';
methname{3} = 'Halofit';
sty{3,1} = 'g--'; sty{3,2} = 'g--';%'m--';

meth{4} = 'linear';
methname{4} = 'Linear';
sty{4,1} = 'b:'; sty{4,2} = 'b:';%'r:';

cospars = setEUCLIDpars;
cospars.omega_wdm = cospars.omega_dm;

mvals = [0 1000 500];

dz = 0.1; z_vals = 0.0001:dz:2.0001;

l_vals = logspace(1,4,20);

nzpars = setnzpars([8 0.5],{'nbin','zsmin'});

tmp = min(z_vals):0.01:max(z_vals);
[n_z_mat niz] = get_nofz(nzpars,tmp);
bincents = 1:nzpars.nbin;
for ii = 1:nzpars.nbin
    bincents(ii) = tmp(max(n_z_mat(:,ii))==n_z_mat(:,ii));
end
ib = 5; jb = 8;
zi = bincents(ib);
zj = bincents(jb);

tic

C_l_ij = zeros(nzpars.nbin,nzpars.nbin,length(l_vals),4);
cold_C_l_ij = zeros(nzpars.nbin,nzpars.nbin,length(l_vals),4);
ratio = zeros(4,length(l_vals));
for mm = 1:length(mvals)
    
    cospars.m_wdm = mvals(mm);
    
    
    for jj=1:4
        
        nzpars.meth = meth{jj};
        
        p = zeros(3,1);
        
        [C_l_ij(:,:,:,jj) tmp noise] = lens_ps_get(cospars,nzpars,z_vals,l_vals);
        
        if mm==1
            cold_C_l_ij(:,:,:,jj) = C_l_ij(:,:,:,jj);
        end
        
        % Ratios:
        ii = 1;
        if mm~=1
            ratio(jj,:) = (C_l_ij(ib(ii),jb(ii),:,jj)+noise(ib(ii),jb(ii),:))./ ...
                (cold_C_l_ij(ib(ii),jb(ii),:,jj)+noise(ib(ii),jb(ii),:)) - 1; % Within 1% ???
        end
        
    end
    

    % Plotting
    ftsz = 14;
    if mm~=1
        
        subplot(1,length(mvals)-1,mm-1)
        
        for jj=1:4
            
            if jj==1
                semilogx(1:1e4:1e5,0*(1:1e4:1e5),'k--'); hold on % clf
            end
            
            p(jj) = semilogx(l_vals,squeeze(ratio(jj,:))*100,sty{jj,mm-1});
            
            axis([l_vals(1) l_vals(end) -10 1])
            ylabel('100(C_\kappa_,_i_j^C^D^M-C_\kappa_,_i_j^W^D^M)/C_\kappa_,_i_j^C^D^M')
            xlabel('multipole l')
            %text(10,-7.0,['bin ' num2str(ib) '-' num2str(jb)])
            text(10,-6.0,['z = (' num2str(round2dp(zi,1),'%1.1f') ',' ...
                num2str(round2dp(zj,1),'%1.1f') ')'],'fontsize',ftsz)
            text(10,-6.7,[num2str(mvals(mm)/1000) ' keV'],'fontsize',ftsz)
            
            plottight(gca,1,length(mvals)-1,mm-1,0.05,0.075);
            
            set(p(jj),'linewidth',1.2)
            set(gca,'fontsize',ftsz);

        end
        
        if mm==2
            legend(p,methname,3)
        end
        
    end
    
    
end

toc

screen = get(0,'ScreenSize');
move = [200 1];
enlarge = [(screen(3)-move(1))/screen(3) 0.7];
set(gcf,'OuterPosition',screen.*[move enlarge]);