# README #

In order to use this code, one should have Octave or Matlab pre-installed. The files and functions are somewhat documented and so, it may be self-explanatory. As this is Matlab, no compilation neccessary. This is execute-explore, not edit-compile-run.

### What is this repository for? ###

* This code can do cosmological calculations, mostly in order to calculate large-scale structure power spectra (matter, lensing) in the Warm Dark Matter scenario. It can use the halo model or halofit. There is Fisher matrix code included for parameter estimation.
* Mostly for personal use of Dida Markovic for now.
* Version 2.0
* The useful parts are likely to be re-written in Python.
* This is officially LEGACY CODE!