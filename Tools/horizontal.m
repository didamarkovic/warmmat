function finvec = horizontal(inivec)
% HORIZONTAL(vector) makes vector horizontal if it already isn't.
%
%  version 1
% 02.06.2011 KMarkovic

% clear; inivec(:,:,2) = [1 2;2 3;3 4;4 5;5 6;6 7]; bob = horizontal(inivec)

dims = size(inivec);

if length(dims)>2 || sum(dims==1) == 0
    error('Input must be a vector!');
end

if dims(1) ~= 1
    finvec = inivec';
else
    finvec = inivec;
end