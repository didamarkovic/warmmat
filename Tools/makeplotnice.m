function h = makeplotnice(handle)
% Octave and AquaTerm make stuff look ugly, so I'm fixing it.
% See also mu comp-notes.
% 17.04.2013 KMarkovic

if ~exist('handle','var')
	handle = gcf;
end

set (handle,'papertype', '<custom>')
set (handle,'paperunits','centimeters');
set (handle,'papersize',[3 2.5])
set (handle,'paperposition', [0,0,[3 2.5]])
set (handle,'defaultaxesposition', [0.15, 0.15, 0.75, 0.75])
set (0,'defaultaxesfontsize', 14)
set (0,'defaultlinelinewidth',2)