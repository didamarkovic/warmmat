function invmat = safeinv(mat)
% SAFEINV(mat) inverts a badly scaled Fisher matrix by multiplying the
% controversial row and column(s) by an appropriate factor of 10.
%
% This only works if only one row & column are bad in the same way 
% compared to the others!
% 
% The point is here that, as far as I can tell, for bad scaling you need a
% whole row or a whole column to be bad. But I can't find the info anywhere
% on how Matlab tests for bad scaling!
%
% First, must test that matrix is square and how big it is of course.
%
% Then will have to test the matrix for bad scaling. This means
% probably the best way is to divide all the colmns and rows between each
% other and make sure that they don't differ by too many orders of
% magnitude.  Probably first it's best to just divide one column through
% all the others and if none of the sums (sum through column) of the 
% factors are greater than let's say 10^5, then don't have to test anymore
% and can just use normal inverse.
% But if it is bigger than 100, then should do more testing. Even though a
% factor of 10^5 isn't much, a factor of 10^10 between two of them can be
% too much.
%
% Then we'll have to take the offending columns & rows and multiply them by
% the right factor.
%
% The right factor I'll find by taking log10 and rounding to the nearest
% integer!
% 
% version 2
% This shit does not work!
% 17.06.2010 KMarkovic

% clear; clc;
% mat = [2*10^(1) 3*10^(-10) 7*10^(1); 6*10^(-10) 9*10^(-20) 2*10^(-10); 1*10^(1) 7*10^(-10) 5*10^(1)];

[nrows ncols] = size(mat);

if nrows~=ncols; 
    fprintf('Error: safeinv: Need a square matrix!')
    invmat = NaN; 
    return; 
elseif nrows > 10
    fprintf('Error: safeinv: I dont think it works for such matrices!')
    invmat = NaN;
    return;
end

testcols = zeros(1,ncols); testrows = testcols;
for icol = 2:ncols
    
    tmp = log10( abs(mat(:,icol)./mat(:,1)) );
    
    testcols(icol) = tmp(1)/abs(tmp(1))*min(abs(tmp));
        
end

if sum( abs(testcols)>5 ) > 0
    
    indexcol = abs(testcols)>5 ;    
    factor = 10.^round(testcols(indexcol));
    
    mat(:,indexcol) = mat(:,indexcol) ./ sum(factor);

    mat(indexcol,:) = mat(indexcol,:) ./ sum(factor);        

end

invmat = inv(mat);

if sum( abs(testcols)>5 ) > 0
    invmat(indexcol,:) = invmat(indexcol,:) ./ factor;
    invmat(:,indexcol) = invmat(:,indexcol) ./ factor;
end