function rounded = round2sf(mynumber,setnosf)
% ROUND2SF(mynumber,nosf) rounds to significant figures.
%   It accepts arrays. It tries to round so that all elements in the array
%   are unique. It does this through recursion. This recursion is turned on
%   by setting nosf = NaN. Maximum order of magnitude is 10.
%
%   *mynumber - number or array to be rounded
%   *nosf - how many significant figures - default is 2
%
%
%   version 1
%   12.07.2010 KMarkovic

% clear; mynumber = [0.0234 0.0231]; nosf = NaN;

if ~exist('setnosf','var') || isnan(setnosf)
    nosf = 2;
else
    nosf = setnosf;
end

% Find order of magnitude:
oom = floor(log10(mynumber));

if oom>10
    warning('Cannot use order of magnitude > 10!');
    return
end

% Make it into an integer and round
intfact = 10.^(oom-nosf+1);
myint = round(mynumber./intfact);

rounded = myint.*intfact;


%% Now let' do recursion if neccessarry

% Need to make sure recursion doesn't run away:
if ~sum(size(unique(mynumber))<size(mynumber))
    
    % Now make sure elements dont become equal after rounding
    if sum(size(unique(rounded))<size(rounded))
        
        rounded = round2sf(mynumber,nosf+1);
        
    end
    
end
