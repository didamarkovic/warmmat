function rounded = round2dp(mynumber,setnodp)
% ROUND2DP(mynumber,nodp) rounds to decimal places.
%   It accepts arrays.
%
%   *mynumber - number or array to be rounded
%   *nodp - how many decimal places - default is 2
%
%
%   version 1, parent: round2sf
%    07.07.2011 KMarkovic

% clear; mynumber = [0.0234 0.0231];

if ~exist('setnodp','var') || isnan(setnodp)
    nodp = 2;
else
    nodp = setnodp;
end

% Make it into an integer and round
intfact = 10.^(nodp);
myint = round(mynumber.*intfact);

rounded = myint./intfact;
