function matx = scell2mat(cellarr)
% matx = scell2num(cellarr)
%   Converts a cell array of numbers as strings
%   to matrix of numbers.
%   If the original cell array contains non-numbers
%   you wil get a weird result.
%
%   01.08.2013 KMarkovic

matx = zeros(size(cellarr));
for int = 1:length(cellarr);

    matx(int) = str2num(cellarr{int});

end


endfunction