function [Si Ci x] = sincosint(x_max,acc,dx,type,x_min)
% SINCOSINT(x_max,acc,dx,type,x_min) calculates the sinx/x & cosx/x integrals to 
%   up to 6 terms.
%
%   type - asymptotic (good for Re(x)>>1 or convergent series (good for |x|<1)
%             see Wikipedia for expressions ('asym' or 'conv')
%           - or can just do it fully numerically - only real option: 'nume'
%   acc - to how many terms
%
%   function [Si Ci x] = sincosint(x,acc,dx,type)
%
%   Returns a cumulatively summed arrays, so the place in the Si array
%   corresponding to the wanted x value in the x array is the Si (duh).
%
%   18/12/08 KMarkovic
%
%   version 3?: Downloaded from SLB's page and modified a little.
%   06.06.2011 KM

gamma = 0.577215664901532860; % Euler's constant

if exist('type','var') && strcmp(type,'asym')
    fprintf('Cant do asym calcs for sin/cos integral!\nMaking type = nume!\n')
    type = 'nume';
elseif ~exist('type','var') type = 'nume';
end

if ~exist('acc','var') acc = 6; end
if ~exist('dx','var') dx = x_max/1000; end
if ~exist('x_min','var') x_min = dx; end

x = x_min:dx:x_max;

if strcmp(type,'nume')
    dSi = sin(x)./x;

    Ci_1stbit = gamma + log(x);
    dCin = (cos(x)-1)./x;

    Si = cumsum(dSi);
    Ci = Ci_1stbit + cumsum(dCin);
    
    % slb guessing corrections:
    Ci=Ci*dx;
    Si=Si*dx;

elseif strcmp(type,'conv')
    Ci = log(x);
    Si = x;
    for int = 2:acc
        power = int*2;
        powers = power+1;
        Ci = Ci + (-1)^(powers) * x.^power /(power*factorial(power));
        Si = Si + (-1)^(powers) * x.^powers /(powers*factorial(powers));
    end
    
    % slb guessing corrections:
    Ci=Ci*dx;
    Si=Si*dx;

elseif strcmp(type,'fullnumint')
    % full numerical integral

    % Ci(x) = - int_x^{\inf} cos(t) / t dt
    dCi=-cos(x)./x *dx;
    Ci=zeros(size(x));
    for ix=1:length(x)
        Ci(ix)=sum(dCi(ix:end));
    end

    % Si(x) = int_0^{x} sin(t) / t dt
    dSi=sin(x)./x *dx;
    Si=zeros(size(x));
    for ix=1:length(x)
        Si(ix)=sum(dSi(1:ix));
    end

elseif strcmp(type,'fulllogint')
    fprintf(1,'Sorry, cant do fulllogint, changing to fullnumint!\n')
    type = 'fullnumint';
    [Si Ci x] = sincosint(x_max,acc,dx,type,x_min);
else 
    error('Type %s not recognised!',type);
end