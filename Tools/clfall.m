function figs = clfall();
% Clears all open figures
%   01.08.2013 KMarkovic

figs = findall(0,'type','figure');

for int = 1:length(figs)
    clf(figs(int));
end