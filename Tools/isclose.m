function [inds vals mindiff] = isclose(vector,values,closeness)
% ISCLOSE(vector,values) finds the entries of 'vector', closest in value to
%   'values'. 'mindiff' is just abs(vals-values). 'vals' is just
%   vector(inds).
%
%   [inds vals mindiff] = isclose(vector,values,closeness)
%
%   'closeness' is an optional input that sets a maximum difference between
%   the two tested values. If the difference is larger, the corresponding
%   entries of inds and vals are set to NaN. This option is a little
%   useless. It is better to just look at mindiff and decide whether they
%   are small enough.
%
%   This function doesn't work if there are several entries of 'vector'
%   that are equally close to a 'value'.
%
%   version 1
%   04.07.2011 KMarkovic


inds = 0*values;
mindiff = inds;

for ii=1:length(values)
    
    diffs = abs(vector-values(ii));
    mindiff(ii) = min(diffs);
    inds(ii)= find( mindiff(ii) == diffs );
    
end

vals = vector(inds);


% Now optionally test:

if exist('closeness','var')
    toolarge = mindiff > closeness;
    inds(toolarge) = NaN;
    vals(toolarge) = NaN;
end