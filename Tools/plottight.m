function [gca pos] = plottight(gca,nov,noh,seq,widen,margin)%,xfrac,yfrac)
% PLOTTIGHT(novert,nohorizont,nosequence,widen,margin) is an
%   implementation of subplot with overlapping axes. Usage of the first
%   three inputs is same as subplot, i.e. nohorizont is the
%   number of plots in the x direction, novert is the number of plots in
%   the y direction. 
%
%   [gca position] = plottight(novert,nohorizont,nosequence,margin) 
%   calculates plot position, so must set is like so: 
%           set(gca,'position',position)
%   
%
%   widen - how much to widen axes to avoid overlapping tickmarks
%   margin - optional to set the margin in plot window
%
%   version 1
%   06.07.2011 KMarkovic

if ~exist('margin','var') margin = 0.1; end
if ~exist('widen','var') widen = [0.2 0.1]; end

% Calculate size for each individual plot
sizeh   = (1 - 2*margin)/noh;
sizev   = (1 - 2*margin)/nov;
sizeall = [sizeh sizev];

% First plot's position
marginv = margin - margin*0.5;
place0  = [margin 1-(marginv+sizev)]; % Since start bottom left.

% Where to place plot
jj    = ceil(seq/noh);
ii    = seq - (jj-1)*noh;
coords  = [(ii-1) (1-jj)];    
    
placethis = place0 + sizeall.*coords;

pos = [placethis sizeall];

% Place the plot
set(gca,'position',pos)


% Erase axis labels
if jj~=nov  set(gca,'xticklabel',''); xlabel('');   end
if ii~=1    set(gca,'yticklabel',''); ylabel('');   end


% Widen the axes by to avoid tick overlap
fonts = get(gca,'fontsize');

% Check if its log:
xsc = get(gca,'xscale');
ysc = get(gca,'yscale');

axvect = axis;

lengthofaxes = (axvect([2 4]) - axvect([1 3]));
if strcmp(xsc,'log')
    lengthofaxes(1) = log10(lengthofaxes(1));
end
if strcmp(ysc,'log')
    lengthofaxes(2) = log10(lengthofaxes(2));
end

extra = sizeall.*widen .* lengthofaxes;
axextra = [-extra(1) extra(1) -extra(2) extra(2)]*fonts/10;

axis(axvect + axextra)
if strcmp(xsc,'log')
    xlim(axvect(1:2) .* 10.^axextra(1:2))
end
if strcmp(ysc,'log')
    ylim(axvect(3:4) .* 10.^axextra(3:4))
end

return

%% Now test this function
clear
%clf
x = 0:0.01:10;
y(1,:) = sin(x);
y(2,:) = cos(x);
y(3,:) = tan(x);
y(4,:) = tanh(x);
y(5,:) = sinh(x);
y(6,:) = cosh(x);
    
noplots = size(y,1);
noh = 3;
nov = 2;

for ii = 1:(noh*nov)
    subplot(nov,noh,ii)
    plot(x,y(ii,:))
    axis([min(x) max(x) -1 1])
    xlabel('x'); ylabel('y')
    plottight(gca,nov,noh,ii);
end