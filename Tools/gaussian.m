function figid = gaussian(sigma,mean,linsty,nosteps,nosig)
% GAUSSIAN(sigma,mean,linsty,nosteps,nosig) plots a Gaussian distribution
% with st.dev sigma and mean.
% if no input, a unit gaussian is plotted
%
% find a vector for x from -nosig*sig to +nosig*sig and then plot gaussian over it
%
% version 1
% 15.06.2010 KMarkovic

if ~exist('sigma','var') sigma = 1; end
if ~exist('mean','var') mean = 0; end
if ~exist('linsty','var') linsty = 'k-'; end
if ~exist('nosteps','var') nosteps = 500/pi; end
if ~exist('nosig','var') nosig = 3; end

dx = 2*nosig*sigma/nosteps;

x = [-((nosteps/2):-1:0) (1:(nosteps/2))]*dx;

%fishprobs = 1/sqrt(2*pi)/sigma*exp(-x.^2 /2/sigma^2);
fishprobs = exp(-x.^2 /2/sigma^2);

if ischar(linsty)
    figid = plot(x+mean,fishprobs,linsty); hold on;
else
    figid = plot(x+mean,fishprobs); hold on;
    set(figid,'Color',linsty);
end
%axis([mean-3*sigma mean+3*sigma 0 1.1])

%line([mean-sigma mean-sigma],[0 interp1(x+mean,fishprobs,mean-sigma)]);
%line([mean+sigma mean+sigma],[0 interp1(x+mean,fishprobs,mean+sigma)]);