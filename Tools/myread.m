function [data header datamat jinx] = myread(filename,nheads,myform,header,delim)
% MYREAD(filename, nheads, format, headers, delimiters) 
%	reads from the file "filename" into a structure of data with 
%	"format" colums and as many rows as there are lines in the 
%	file. 
%
%	The inputs:
%		- "filename" is a string containing the name and extension
%			of the file to be read
%		- "nheads" is a scalar giving the number of header lines.
%			If it is not given it takes it to be zero!
%			* If 'nheads' has 2 elements, the second is taken to be 
%			the number of non-header lines to be read. After these 
%			all is discarded.
%			* If it has 3 elements, the third is taken to be which 
%			header line you want returned. Set the second element to 
%			Inf if want all the lines. "nheads(3) = Inf" means take 
%			last line of header. This is defaut. To return none of
%			them, set to 0.
%           * If a 'header' is input too (see below), it is not
%           expected to be read from the file head.
%		- "format"="numcols" is a scalar giving the number of columns.
%			If it is not given, it takes the number of words in the 
%			header row, separated with spaces (also if it is NaN). 
%			* If "format" is a 1D array of numbers, they signify 
%			the first byte of each individual column. If this input 
%			is found, this guides the width and number of the columns.
%		- "headers" is a cell array of headers for the different 
%			columns in case they are not included in the file.
%			If this is just a string, it is taken to be the 'ignore'
%			symbol. Make it an empty string, i.e. '', if you don't 
%			want to set it, but want to input delimiters!
%		- "delimiters" are 1 or 2 characters which delimit the file
%			entries. Default is a space.
%			* the first is the delimiter for the data, the second
%			is for the header entries
%			* if only one is given it is assumed that the header
%			and the data delimiters are the same
%			N.B. must be given as a character array if contains 
%			spaces!!! E.g. [',';' '] or char(',',' ').
%			
%
%   The outputs:
%		- "data" is in the form of a matrix if there aren't any 
%			headers. It is a structure if given structure field
%			names in the header. These fields contain vectors, which 
%			contain the columns in the file. For example 
%			data.headerword1 = vectorcolumn1;
%		- "header" is an array containing different header lines 
%			in the form of cell arrays, with each cell representing 
%			a word in the header.
%		- "datamat" is either an array or cell array, depending 
%			on whether you have only numbers or also strings
%		- "jinx" tells you which columns do not contain only no.s
%
% TODO: merge myform and header into one input!
%
% version 1 - 25.06.2010 KMarkovic
% version 2 - 08.01.2013 - not all entries are numbers
% version 3 - 21.03.2013 - fixed crazyness a bit
% version 4 - 10.04.2013 - changing the format input to first byte
%      v4.1 - 14.06.2013 - fixed some defaults to make it more general

% Tests:
%fclose('all'); clear; filename = 'DATA-Smith/Pow_rk_aa.0.dat.Katarina';  nheads = 1; myform = 100; header = ' ';
%filename = '/Users/katarina/Documents/nicaea_2.3/Demo/output/HOD'; nheads = 1; myform = NaN; header = '#';
%filename = 'LCDM_pk_nl.dat'; nheads = 4; myform = 2;
%nheads = 21; myform = 7; header = {'l';'lmin';'lmax';'TT';'errtot';'err';'cvar'}; filename = 'wmap_binned_tt_spectrum_9yr_v5.txt';
%nheads = 0; myform = 4; header = {'l','TT','EE','TE'}; filename = '/Users/katarina/Documents/camb/Outputs/michiPlanck_scalCls.dat';
%filename = 'planck_cl.dat'; nheads = 7; myform = 2; header = '#';
%filename = formSN; nheads = [38 58 37]; myform = [2 12 18 24 35];
%filename = fSN; nheads = [38 58 37]; myform = [2 12 18 24 35];
%filename = fC; nheads = 1;
%filename = fV2; nheads = 1; myform = NaN; header = '#'; delim = [' ';','];


% See if 'header' exists, if not, makeit an empty string
if ~exist('header','var')
    header = '';
end
% If there is no header then output is simply an array, which will
%	go horribly wrong if the data is alphanumeric!


% Determine how many header rows must be read:
if ~exist('nheads','var'); 
	nheads = [0 0 Inf]; 
elseif length(nheads)<3 && ~iscell(header)
	nheads(3) = Inf; 
	% Sets non-existent entries to 0 & inf. Will count them below.
else
    nheads(3) = 0;
    % If giving header, don't need to record header lines.'
end
headno = nheads(3); 
nentries = nheads(2);
nheads = nheads(1);


% See if delimiters are set:
if ~exist('delim','var')
	delimh = ' ';
	delim = ' ';
end
if length(delim)==1
	delimh = delim;
else
	delimh = delim(2);
	delim = delim(1);
end



% Open file and count lines if needed
fid = fopen(filename);
myline = 'empty';
if nentries==0
	while myline ~= -1
		myline = fgetl(fid);
		nentries = nentries + 1;
	end
	nentries = nentries - 1 - nheads;
end
frewind(fid);

% Read header:
if nheads>0 && headno~=0 && length(header)>1
	error('You have to decide whether you want the header read or input!')
end
for headint = 1:nheads

	if strcmp(delimh,' ')
		%myline = strtrim(strread(strtrim(fgetl(fid)),'%s'));
		myline = fgetl(fid);
	else
		myline = strtrim(strread(fgetl(fid),'%s','delimiter', delimh));
	end
	if headint == min(headno,nheads) && headno~=0
		tmp = myline; % Save only one line
		if length(header)~=0
            %ttmp = strjoin('-',tmp);
			%tmp = strread(strtrim(regexprep(ttmp,header,'')),'%s','delimiter', '-');
			tmp = strread(strtrim(regexprep(tmp,header,'')),'%s','delimiter', delimh);
			header = tmp;
		else
            header = tmp;
        end
	end
end


% Make sure the header is what we expected % TODO: improve implementation!
if nheads==0 && ~exist('myform','var')
    myline = fgetl(fid);
    ncols = length(str2num(myline));
    frewind(fid);
    if isempty(myline)
        error('Must give ncols if no header and no format!');
    else
    	warning('Should give ncols if no header and no format!');
    end
elseif exist('myform','var') && length(myform)==1
	if isnan(myform) && nheads>0
		ncols = length(header); % Count columns in header
	elseif isnan(myform) && nheads==0
        myline = fgetl(fid);
        ncols = length(str2num(myline));
        frewind(fid);
        if isempty(myline)
            error('Must give ncols if no header and no format!');
        else
            warning('Should give ncols if no header and no format!');
        end
	else
		ncols = myform;
	end
elseif exist('myform','var')
	ncols = length(myform);
	if headno==0
		error('if inputting format manually, must provide headers!');
	end
else
	ncols = length(header); % Count columns in header
end
clear myline headint


% Initialise the big matrices:
datamat = cell(nentries,ncols);
jinx = zeros(ncols,1);
% This jinxes a column to being filled with strings if there is 
%	only one non-num

myline = fgetl(fid);
if sum(isnan(strread(myline)))>0; notnum = 1; end 
% Test if there are non-numbers


% Read the data:
if ~exist('notnum','var')

	datamat = cell2mat(datamat);
	
	% If only number it's simple:
	for lineint = 1:nentries
		numline = sscanf(myline,'%f');
		if (ncols-length(numline))>0 % in case some rows don't have max no of cols
			fill = zeros([1,ncols])*NaN;
			fill(1,1:length(numline)) = numline;
			numline = fill;
		end
		datamat(lineint,:) = numline;
		myline = fgetl(fid); % Fetch new line
	end
	
elseif ~exist('myform','var')
    myline
	error('Can''t handle alphanumeric files without a format yet.');

else
		
	% Find where columns end
	bitends = [(myform(2:ncols)-1) Inf];

	% Read alphanumeric line
	for lineint = 1:nentries	

		for colint = 1:ncols
					
			tableentry = myline(myform(colint):min(end,bitends(colint)));
			
			% If only space, insert empty element into cell array, delete later
			if sum(isspace(tableentry)) ~= (bitends(colint)-myform(colint)+1)
				datamat{lineint,colint} = strtrim(tableentry);
			else
				datamat{lineint,colint} = '';
			end

			% Mark non-number non-empty entries in line:
			if ~isempty(datamat{lineint,colint})
				jinx(colint) = jinx(colint) + isnan(str2double(datamat{lineint,colint}));
			end
									
		end
		
		myline = fgetl(fid);		
	end

	% Convert purely numeric columns into doubles:
	jinx = jinx > 0*jinx;
	datamat(:,~jinx) = num2cell(str2double(datamat(:,~jinx)));
		
end


if sum(strcmp(header,''))==1
	data = datamat;

elseif sum(strcmp(header,''))>1
	error('header has empty entries!')	

elseif ncols ~= length(header)
    warning('header does not match number of columns: giving you datamat')
    data = datamat;

else
	%fprintf('Columns containing strings:\n');
	for colint = 1:ncols
		sttitle = genvarname(strtrim(header{colint}));
		if jinx(colint)
			data.(sttitle) = datamat(:,colint); % If strings
			%fprintf('- %s\n',strtrim(header{colint}));
		elseif iscell(datamat)
			data.(sttitle) = cell2mat(datamat(:,colint)); % If numbers
		else
			data.(sttitle) = datamat(:,colint);
		end
	end
end

fclose(fid);