function [Si Ci] = sincosint(x)%,stepno)

% Calculates the Si(x) and Ci(x) integrals, where
% Si(x) = int(six(t)/t,0,x,over t) & Ci(x) = - int(cos(x)/x,x,inf, over t).
%
% [Si Ci dx_vect] = sincosint(x,stepno)
%
% version 2
% 19/12/08 KMarkovic

gamma = 0.577215664901532860; % Euler's constant

%if ~exist('stepno','var') stepno = 100000; end

if x(1)==0
    warning('Cannot calculate at zero, make it close to zero!')
    x(1) = (x(2)-x(3))/10^10;
end

for ix = 1:length(x)
    
    if 0
    if x(ix)>1000
        if x(ix)>15000
            if x(ix)>20000
                if x(ix)>30000
                    if x(ix)>40000
                        if x(ix)>50000
                            if x(ix)>70000
                                if x(ix)>80000
                                    if x(ix)>90000
                                        if x(ix)>100000
                                            mic = 15000;
                                        else mic = 14000;
                                        end
                                    else mic = 12000;
                                    end
                                else mic = 10000;
                                end
                            else mic = 8000;
                            end
                        else mic = 7000;
                        end
                    else mic = 5000;
                    end
                else mic = 4000;
                end
            else mic = 3000;
            end
        else mic = 2000;
        end
    else mic = 650;
    end
    else
    mic = 650;
    end
    
    %dt = x(ix)/stepno;
    %t = dt:dt:x(ix);
    
    %dSi = sin(t)./t * dt;
    Si_fn = @(t) sin(t)./t; % ??
    
    Ci_1stbit = gamma + log(x(ix));
    %dCin = (cos(t)-1)./t * dt;
    Cin_fn = @(t) (cos(t) - 1)./t;
    
    
    %Si(ix) = sum(dSi);
    %Ci(ix) = Ci_1stbit + sum(dCin);
    
    
    Si(ix) = quadgk(Si_fn,0,x(ix),'MaxIntervalCount',mic);
    Ci(ix) = Ci_1stbit + quadgk(Cin_fn,0,x(ix),'MaxIntervalCount',mic);
end


% Testing:
if 0
    %%
    clear; clc; clf
    
    x = 1:100:10000;
    [Si1 Ci1] = sincosint(x);
    
    %figure
    plot(x,Si1,'g--'); hold on
    plot(x,Ci1,'m--');
    hold on
    
    axis([x(1) max(x) -0.5 2])
end