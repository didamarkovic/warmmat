function cellarr = mat2scell(mat)
% matx = scell2num(cellarr)
%   Converts a vector of numbers to a cell array
%   of strings.
%   This is the reverse of scell2mat.
%
%   01.08.2013 KMarkovic

cellarr = num2cell(mat);
for int = 1:length(mat);

    cellarr{int} = num2str(mat(int));

end


endfunction