function stringarr = genleg(arg1,arg2)
% A function to generate the string entries for a legend based on the
%   numeric values stored in the input vector. The prefix or suffix is
%   determined by the other input argument. If string argument is first,
%   get prefix. You get it now.
%
%   stringarray = genleg(arg1,arg2)
%
% version 1
% 17.07.2013 KMarkovic

%arg2 = 'bob is'; arg1 = [2 4 5];

if ~exist('pos','var') pos = 1; end
if ~exist('arg2','var') arg2 = ''; end
if ~exist('arg1','var') error('Need input!'); end


if ischar(arg1)
    noentries = length(arg2);

    vec = arg2;
    str = arg1;

    vecsecond = 1;
elseif ischar(arg2)
    noentries = length(arg1);

    vec = arg1;
    str = arg2;

    vecsecond = 0;
else
    error('Must input string!');
end


for int = 1:noentries

    if vecsecond
        stringarr{int} = [str ' ' num2str(vec(int))];
    else
        stringarr{int} = [num2str(vec(int)) ' ' str];
    end

end