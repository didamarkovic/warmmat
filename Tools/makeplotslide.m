function h = makeplotslide(h)
% Octave and AquaTerm make stuff look ugly, so I'm fixing it.
% See also mu comp-notes.
% 01.08.2013 KMarkovic

if ~exist('h','var')
	h = gcf;
end

set (h,'papertype', '<custom>')
set (h,'paperunits','centimeters');
set (h,'papersize',[3 2.5])
set (h,'paperposition', [0,0,[3 2.5]])
set (h,'defaultaxesposition', [0.15, 0.15, 0.75, 0.75])
set (h,'defaultaxesfontsize', 18)